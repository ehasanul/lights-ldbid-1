USE master
go
IF EXISTS(SELECT name FROM sys.databases
     WHERE name = 'LDBID')
     DROP DATABASE LDBID
GO

CREATE DATABASE LDBID
GO
USE LDBID
GO

CREATE TABLE [Project](
	[ProjectID] [int] IDENTITY(1,1) NOT NULL,
	[SourceID] [int] NULL,
	[ProjectDate] [datetime] NULL,
	[ClientID] [int] NULL,
	[Description] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [char](2) NULL,
	[Zip] [char](10) NULL,
	[ProjectContact] [varchar](50) NULL,
	[ClientPM] [varchar](50) NULL,
	[ClientMobilePhone] [varchar](14) NULL,
	[LDPM] [varchar](50) NULL,
	[Notes] [varchar](250) NULL,
	[LDEstimator] [varchar](50) NULL,
	[BluePrintDate] [datetime] NULL,
	[RevDate] [datetime] NULL,
	[SalesTaxRate] [float] NULL,
	[AuxLabel] [varchar](25) NULL,
	[ContractDate] [datetime] NULL,
	[ContractAmount] [money] NULL,
	[ContractBalance] [money] NULL,
	[ProjectType] [varchar](25) NULL,
	[Closed] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProjectID] ASC
)
)
GO

CREATE TABLE [Unit](
	[UnitID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[BldgID] [int] NULL,
	[UnitName] [nvarchar](50) NULL,
	[UnitType] [char](10) NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_j_Units] PRIMARY KEY CLUSTERED 
(
	[UnitID] ASC
)
)
GO

CREATE TABLE [CatalogSO](
	[ProductID] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[VendorCode] [nvarchar](255) NULL,
	[VendorPartNo] [nvarchar](255) NULL,
	[AlternateVendor] [nvarchar](255) NULL,
	[AlternatePartNum] [nvarchar](255) NULL,
	[AverageCost] [float] NULL,
	[StandardCost] [float] NULL,
	[AvailableUnits] [float] NULL,
	[OnOrderUnits] [float] NULL,
	[CommittedUnits] [float] NULL,
	[LeadTime] [nvarchar](255) NULL,
	[Notes] [nvarchar](1000) NULL,
 CONSTRAINT [PK_CatalogSO] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)
)
GO

CREATE TABLE [CatalogMain](
	[ProductID] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[VendorCode] [nvarchar](255) NULL,
	[VendorPartNo] [nvarchar](255) NULL,
	[AlternateVendor] [nvarchar](255) NULL,
	[AlternatePartNum] [nvarchar](255) NULL,
	[AverageCost] [float] NULL,
	[StandardCost] [float] NULL,
	[AvailableUnits] [float] NULL,
	[OnOrderUnits] [float] NULL,
	[CommittedUnits] [float] NULL,
	[LeadTime] [nvarchar](255) NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_CatalogMain] ON [CatalogMain] 
(
	[ProductID] ASC
)
GO

CREATE TABLE [Bulbs](
	[BulbNum] [char](25) NOT NULL,
	[Desc] [char](50) NULL,
	[Cost] [money] NOT NULL,
	[Vendor] [char](25) NULL,
 CONSTRAINT [PK_Bulbs] PRIMARY KEY CLUSTERED 
(
	[BulbNum] ASC
)
)
GO

CREATE TABLE [Building](
	[BldgID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[BldgNumber] [int] NULL,
	[Type] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[Location] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Notes] [nvarchar](250) NULL,
	[shipdate] [datetime] NULL,
	[shipsequence] [int] NULL,
	[Shipped] [bit] NULL,
	[ShippedR] [bit] NULL,
	[ShipdateR] [datetime] NULL,
	[BldgName] [varchar](50) NULL,
 CONSTRAINT [PK_Building] PRIMARY KEY CLUSTERED 
(
	[BldgID] ASC
)
)
GO

CREATE TABLE [Alternates](
	[AltID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[OrgProductID] [nvarchar](255) NOT NULL,
	[OrgDescription] [nvarchar](255) NOT NULL,
	[OrgCost] [money] NOT NULL,
	[OrgMargin] [int] NOT NULL,
	[OrgSell] [money] NOT NULL,
	[AltProductID] [nvarchar](255) NOT NULL,
	[AltDescription] [nvarchar](255) NOT NULL,
	[AltCost] [money] NOT NULL,
	[AltMargin] [int] NOT NULL,
	[AltSell] [money] NOT NULL,
	[AltRecordSource] [char](10) NULL,
 CONSTRAINT [PK_Alternates] PRIMARY KEY CLUSTERED 
(
	[AltID] ASC
)
)
GO

CREATE TABLE [Bulb2Fixture](
	[PartNumber] [nchar](255) NULL,
	[BulbNum] [char](25) NULL,
	[Quan] [int] NULL,
	[Included] [bit] NULL
)
GO



CREATE TABLE [Clients](
	[ClientID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[State] [char](2) NULL,
	[Zip] [char](10) NULL,
	[Phone1] [char](15) NULL,
	[Phone2] [char](15) NULL,
	[FAX] [char](15) NULL,
	[email] [nvarchar](50) NULL,
	[contact] [nvarchar](50) NULL,
	[taxexempt] [bit] NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)
)
GO

CREATE TABLE [ChangeOrder](
	[COID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[CODate] [datetime] NOT NULL,
	[EnteredBy] [char](10) NULL,
	[ApprovedBy] [char](50) NULL,
	[CODesc] [nvarchar](1000) NULL,
	[Misc] [nvarchar](1000) NULL,
	[PriorBalance] [money] NULL,
	[NewBalance] [money] NULL
)
GO

CREATE TABLE [Comment](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[Author] [nvarchar](30) NULL,
	[Comment] [nvarchar](1000) NULL,
 CONSTRAINT [PK_j_Comments] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)
)
GO

CREATE TABLE [LineItem](
	[ItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[BldgID] [int] NULL,
	[UnitID] [int] NOT NULL,
	[RecordSource] [char](10) NULL,
	[PartNumber] [char](25) NULL,
	[RT] [char](1) NULL,
	[Description] [nvarchar](50) NULL,
	[Cost] [money] NULL,
	[Margin] [int] NULL,
	[Sell] [money] NULL,
	[Quantity] [int] NULL,
	[Location] [nvarchar](50) NULL,
	[Type] [nvarchar](10) NULL,
	[Shipped] [bit] NULL,
	[ShipDate] [datetime] NULL,
	[ShipQuan] [int] NULL,
	[QuanBO] [int] NULL
)
GO

CREATE TABLE [BldgUnitCount](
	[BldgID] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[Quan] [int] NOT NULL
)
GO

CREATE PROCEDURE [CatMainLineItem]
@ProductID nvarchar(255)
AS
SELECT    
	 ProductID, 
	ISNULL(Description, ' ') AS Description,
	ISNULL(StandardCost, ' ') AS Cost
FROM  .CatalogMain
WHERE
	ProductID = @ProductID
GO

CREATE PROCEDURE [JobsReport]   
	@ProjectType varchar(15)   
AS
SELECT Project.ProjectID, Project.SourceID, Project.ContractDate, Project.Description, Project.Location, Project.Closed, Clients.Name, 
	SUM(LineItem.Quantity * LineItem.Cost) AS Cost, SUM(LineItem.Quantity * LineItem.Sell) AS Sell
FROM Project INNER JOIN
	Clients ON Project.ClientID = Clients.ClientID INNER JOIN
	LineItem ON Project.ProjectID = LineItem.ProjectID
where ProjectType = @ProjectType
GROUP BY Project.ProjectID, Project.SourceID, Project.ContractDate, Project.Description, Project.Location, Project.Closed, Clients.Name
ORDER BY Project.ProjectID
GO

CREATE PROCEDURE [ClubHouseSummaryExp]
	@ProjectID int,
	@Order char(2) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (se * Quantity) as Total
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'C' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Type, LineItem.location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order BY ( 
case
	when @Order = 'PN' then pn
	when @Order = 'T' then ty
	when @Order = 'L'  then lo
	END
	)
GO

CREATE PROCEDURE [ClubHouseSummary]
	@ProjectID int,
	@Order char(2) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (co * Quantity) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'C'
	group by LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
ORDER BY ( CASE 
	WHEN @Order = 'PN' THEN pn
	WHEN @Order = 'T' THEN  ty
	WHEN @Order = 'L'  THEN lo
	END
	)
GO

/* frmBid - LoadCatMainLineItem, LoadCatBERLineItem */
CREATE PROCEDURE [CatSOLineItem]
	@ProductID nvarchar(255)
AS
SELECT    
	ProductID, ISNULL(Description, ' ') AS Description,	ISNULL(StandardCost, ' ') AS Cost
FROM  CatalogSO
WHERE ProductID = @ProductID
GO

CREATE PROCEDURE [CatSOItemsSP]
	@ProductID nvarchar (255)
AS
SELECT Description, VendorCode, VendorPartNO, AlternateVendor,  AlternatePartNum,
	AverageCost, StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits,
	LeadTime, Notes
FROM CatalogSO
WHERE ProductID = @ProductID
GO

/* frmBid - LoadCatalogMainCBO, LoadBECatalogCBO */
CREATE PROCEDURE [CatSOItem] AS
SELECT ProductID, RTRIM(ProductID) + '  - ' + ISNULL(Description, ' ') AS Item
FROM CatalogSO
ORDER BY ProductID
GO

CREATE PROCEDURE [ClientDetails] 
	@ClientID int
AS
SELECT    
	Name, 
	ISNULL(Address1, ' ') AS Address1,
	ISNULL(Address2, ' ') AS Address2, 
	ISNULL(city, ' ') AS City, 
	ISNULL(State, ' ') AS State,
	ISNULL(Zip, ' ') AS Zip, 
	ISNULL(Phone1, ' ') AS Phone1,
	ISNULL(Phone2, ' ') AS Phone2, 
	ISNULL(FAX, ' ') AS FAX,
	ISNULL(email, ' ') AS email , 
	ISNULL(contact, ' ') AS Contact
FROM Clients
WHERE clientid = @ClientID
GO

CREATE PROCEDURE [Client] 
AS
SELECT ClientID, Name + ' - ' + ISNULL(city, ' ') + '  ' + ISNULL(State, ' ') AS Client
FROM Clients
ORDER BY Client
GO

CREATE PROCEDURE [ChangeOrders]
	@ProjectID int
AS
SELECT COID, CODate, EnteredBy, ApprovedBy, CODesc, Misc, PriorBalance, NewBalance
FROM         dbo.ChangeOrder
WHERE     (ProjectID = @ProjectID)
GO

/* frmBid - LoadCatalogMainCBO, LoadBECatalogCBO */
CREATE PROCEDURE [CatMainItems] AS
SELECT ProductID, 
	ISNULL(RTRIM(ProductID), ' ') + ' - ' + ISNULL(RTRIM(Description), ' ') AS Item
FROM CatalogMain
ORDER BY ProductID
GO

CREATE PROCEDURE [CatMainItem]
	@ProductID nvarchar (255)
AS
SELECT Description, VendorCode, VendorPartNO, 
	AlternateVendor,  AlternatePartNum,
	StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, LeadTime
FROM CatalogMain
WHERE ProductID = @ProductID
GO

/* frmBid - LoadCatMainLineItem */
CREATE PROCEDURE [CatLineItem]
	@ProductID char(25)
AS
SELECT    
	 PartNumber AS ProductID,
	ISNULL(Description, ' ') AS Description,
	Cost
FROM  LineItem
WHERE
	PartNumber = @ProductID
GO

/* frmBid - LoadBEBidItemCBO, LoadBidItemsCBO */
CREATE PROCEDURE [CatItems]
	@ProjectID int
AS
SELECT Distinct PartNumber AS ProductID, 
	ISNULL(RTRIM(PartNumber), ' ') + ' - ' + ISNULL(RTRIM(Description), ' ') AS Item
FROM LineItem
WHERE ProjectID = @ProjectID
ORDER BY PartNumber
GO

/* frmBid - LoadBldgs */
CREATE PROCEDURE [Buildings]
	@ProjectID int
AS SELECT BldgID, 
	RTRIM(
	CASE WHEN(LEN(ISNULL(RTRIM(CAST(BldgNumber AS CHAR)), '-')) = 1) 
		Then '0' + RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR)), '-')) 
		Else RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-'))
	END) + ' ' + Type + ' ' + Description + ' -  ' + Location AS BLDG
FROM dbo.Building
WHERE (ProjectID = @ProjectID)
ORDER BY Bldg
GO

CREATE PROCEDURE [Alternate]
	@ProjectID int,
	@OrgProductID nvarchar(255),
	@OrgDescription nvarchar(255),
	@OrgCost money,
	@OrgMargin money,
	@OrgSell money,
	@AltProductID nvarchar(255),
	@AltDescription nvarchar(255),
	@AltCost money,
	@AltMargin money,
	@AltSell money,
	@AltRecordSource char(10)
AS
INSERT INTO Alternates
	(ProjectID, OrgProductID, OrgDescription, OrgCost, OrgMargin, OrgSell,
	AltProductID, AltDescription, AltCost, AltMargin, AltSell, AltRecordSource)
VALUES
	(@ProjectID, @OrgProductID, @OrgDescription, @OrgCost, @OrgMargin, @OrgSell,
	@AltProductID, @AltDescription, @AltCost, @AltMargin, @AltSell, @AltRecordsource)
GO

CREATE PROCEDURE [AdjustMargins]
	@ProjectID int,
	@Delta int
AS 
Update LineItem
SET Sell = (Cost/(Cast((100-(Margin + @Delta)) AS float)/100)),
	Margin = Margin + @Delta
WHERE ProjectID = @ProjectID

Update Alternates
SET  
 	OrgSell = (orgCost/(Cast((100-(orgMargin + @Delta)) AS float)/100)),
	OrgMargin = OrgMargin + @Delta,
	AltSell = (AltCost/(Cast((100-(AltMargin + @Delta)) AS float)/100)),
	AltMargin = AltMargin + @Delta
WHERE ProjectID = @ProjectID
GO

CREATE PROCEDURE [AddUnitLineItem] 
	@RecordSource char(15),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money
AS 
INSERT INTO LineItem
	(RecordSource, PartNumber, RT, Description,
	 Cost, Margin, Sell)
VALUES
	(@RecordSource, @PartNumber, @RT, @Description,
	 @Cost, @Margin, @Sell)
GO

/* frmBid - AddNewUnit, frmCopyUnit */
CREATE PROCEDURE [AddUnit]
	@ProjectID int,
	@BldgID int = null,
	@UnitName nvarchar(50) = null,
	@UnitType char(10),
	@Description nvarchar(50),
	@NewUnitID int OUTPUT
AS
INSERT INTO Unit
	(ProjectID, BldgId, UnitName, UnitType, Description)
VALUES
	(@ProjectID, @BldgID, @UnitName, @UnitType, @Description);
SELECT @NewUnitID = @@IDENTITY
RETURN @NewUnitID
GO

CREATE PROCEDURE [AddProject]
	@SourceID int = null,
	@ClientID int,
	@ProjectDate datetime,
	@ContractDate datetime = null,
	@ContractAmount money = null,
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@City nvarchar(50),
	@State char(2),
	@Zip char(10),
	@ProjectContact nvarchar(50) = null,
	@ClientPM nvarchar (50) = null,
	@ClientMobilePhone char (14) = null,
	@LDPM nvarchar (50) = null,
	@Notes nvarchar(250),
	@LDEstimator nvarchar(50),
	@BluePrintDate datetime,
	@RevDate datetime,
	@SalesTaxRate float,
	@Closed bit = 0,
	@AuxLabel char(25) = null,
	@ProjectType varchar(25),
	@NewProjectID int OUTPUT
 AS
 INSERT INTO 
	Project (SourceID, ClientID, ContractDate, ProjectDate, ContractAmount,  Description, Location, Address, 
	City, State, Zip, ProjectContact, ClientPM, ClientMobilePhone, LDPM, 
	Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, Closed, AuxLabel, ProjectType)
 VALUES 
	(@SourceID, @ClientID, @ContractDate, @ProjectDate, @ContractAmount, @Description, @Location, 
	@Address, @City, @State, @Zip, @ProjectContact, @ClientPM, @ClientMobilePhone, @LDPM,
	@Notes, @LDEstimator, @BluePrintDate, @RevDate, @SalesTaxRate, @Closed, @AuxLabel, @ProjectType) ;

 SELECT @NewProjectID = @@IDENTITY
 RETURN @NewProjectID
GO

CREATE PROCEDURE [AddLIUnitWC]
	@ProjectID int,
	@UnitTypeWC varchar(10),
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@Quantity int,
	@Location nvarchar(50),
	@Type nvarchar(10)
	
AS
	INSERT INTO LineItem (ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description, Cost,
	Margin, Sell, Quantity, Location, Type, Shipped)
	(SELECT ProjectID, BldgID, UnitID, @RecordSource, @PartNumber, @RT, @Description, 
	@Cost, @Margin, @Sell, @Quantity, @Location, @Type, 0
FROM dbo.Unit
WHERE (ProjectID = @ProjectID) AND (UnitType LIKE (@UnitTypeWC+'%')))
GO

CREATE PROCEDURE [AddLineItem] 
	@ProjectID int,
	@BldgID int = NULL,
	@UnitID int,
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@Quantity int,
	@Location nvarchar(50),
	@Type nvarchar(10),
	@Shipped bit = 0,
	@ShipDate datetime=NULL,
	@shipQuan int = 0,
	@QuanBO int= 0
AS 
INSERT INTO LineItem
	(ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description,
	 Cost, Margin, Sell, Quantity, Location, Type, Shipped, Shipdate, ShipQuan, QuanBO)
VALUES
	(@ProjectID, @BldgID, @UnitID,@RecordSource, @PartNumber, @RT,  @Description,
	 @Cost, @Margin, @Sell, @Quantity, @Location, @Type, @Shipped, @Shipdate, @ShipQuan, @QuanBO)
GO

/*frmBid - AddNewBid*/
CREATE PROCEDURE [AddComment]
	@ProjectID int,
	@EntryDate datetime,
	@Author nvarchar(30),
	@Comment nvarchar(1000)
 AS
	
INSERT INTO 
	Comment (ProjectID, EntryDate, Author, Comment)
VALUES 
	(@ProjectID, @EntryDate, @Author, @Comment)
GO

CREATE PROCEDURE [AddChangeOrder]
	@ProjectID int,
	@CODate datetime,
	@EnteredBy char(10),
	@ApprovedBy char(50),
	@CODesc nvarchar(1000),
	@Misc nvarchar(1000),
	@PriorBalance money,
	@NewBalance money
AS
INSERT INTO ChangeOrder (ProjectID, CODate, EnteredBy, ApprovedBy, CODesc, Misc, PriorBalance, NewBalance)
VALUES (@ProjectID, @CODate, @EnteredBy, @ApprovedBy, @CODesc, @Misc, @PriorBalance, @NewBalance)
GO

/* frmBid - AddToSO */
CREATE PROCEDURE [AddCatalogSO]
	@ProdID nvarchar (255),
	@Desc	nvarchar (255),
	@Cost	float
AS
INSERT INTO CatalogSO (ProductID, Description, StandardCost)
VALUES (@ProdID, @Desc, @Cost)
GO

/* frmBid - btnAddBldg_Click */
CREATE PROCEDURE [AddBldg]
	@ProjectID int,
	@BldgNumber int,
	@BldgName varchar(50)=null,
	@Type nvarchar(50),
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@Notes nvarchar(250),
	@shipdate datetime=NULL,
	@shipsequence int=NULL,
	@Shipped bit = NULL,
	@ShippedR bit = NULL,
	@NewBldgID int OUTPUT
AS INSERT INTO Building
	(ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, Shipdate, shipsequence, Shipped, ShippedR)
VALUES
	(@ProjectID, @BldgNumber, @BldgName, @Type, @Description, @Location, @Address, @Notes, @Shipdate, @shipsequence, @Shipped, @ShippedR);
SELECT @NewBldgID = @@IDENTITY
RETURN @NewBldgID
GO

CREATE PROCEDURE [BldgUnitCnt]
	@BldgID int,
	@ProjectID int
AS
select COUNT(UnitID) Quantity, BldgID, UnitType, Description from Unit where bldgID = @BldgID and ProjectId = @ProjectID group by BldgID, UnitType, Description
ORDER BY 
	Unit.UnitType
GO

CREATE PROCEDURE [BldgShipByDate]
	@StartDate DateTime,
	@EndDate DateTime, 
	@ProjectType varchar(25)
AS
SELECT Project.ProjectID as BidID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate,
	LineItem.RT, SUM(LineItem.Sell*LineItem.Quantity) AS Total 
FROM Building INNER JOIN LineItem ON Building.BldgID = LineItem.BldgID INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID
WHERE ShipDateR Between @StartDate And @EndDate AND RT = 'R' AND Building.ShippedR = 0 and ProjectType = @ProjectType
GROUP BY Project.ProjectID, Building.BldgNumber, Building.BldgName, Building.shipdateR,
	Building.shipdate,LineItem.RT
UNION ALL
SELECT Project.ProjectID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate,
	LineItem.RT, SUM(LineItem.Sell*LineItem.Quantity) AS Total 
From Building INNER JOIN LineItem ON Building.BldgID = LineItem.BldgID INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID
WHERE Building.ShipDate Between @StartDate And @EndDate AND RT = 'T' AND Building.Shipped = 0 and ProjectType = @ProjectType
GROUP BY Project.ProjectID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate,
	LineItem.RT
ORDER BY Building.ShipDate
GO

CREATE PROCEDURE [Bldgs]
	@ProjectID int
AS
SELECT     
	BldgID, 
	RTRIM(CASE WHEN (LEN(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-'))=1) 
	Then 
		'0' + RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-') ) 
	Else 
		RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-') )  END) 
	 + ' ' + Type + ' ' + Description + ' -  ' 
	+ Location AS BLDG
FROM         Building
WHERE     (ProjectID = @ProjectID)
ORDER BY BLDG
GO

CREATE PROCEDURE [BldgBOMSP]
	@BldgID int
AS
Select Distinct TQ.Quantity Quan, TQ.Pn PartNumber, TQ.De Description, LineItem.Cost, TQ.CO TotalCost, LineItem.Sell, TQ.SE TotalSell From LineItem inner join 
(Select Sum(Quantity) as Quantity, PartNumber pn, Description De, SUM(Cost * Quantity ) as CO, SUM(Sell * Quantity ) as SE from LineItem where BldgID = @BldgID
Group BY PartNumber, Description) TQ on LineItem.PartNumber = TQ.pn where LineItem.BldgID = @BldgID
GO

CREATE PROCEDURE [BldgBOMExp]
	@BldgID int
AS
select PartNumber pn, Description,  COUNT(ItemID) Quan, sell as Price, SUM(sell) As Total 
from LineItem where BldgID = @BldgID 
group by PartNumber, Description, Sell
order by PartNumber
GO

CREATE PROCEDURE [BulbList]
	@PartNumber char(255)	
AS
SELECT CatalogMain.ProductID, CatalogMain.ProductID + ':' + CatalogMain.Description AS Bulb
FROM Bulb2Fixture INNER JOIN
     CatalogMain ON Bulb2Fixture.BulbNum = CatalogMain.ProductID
WHERE Bulb2Fixture.PartNumber = @PartNumber
GO

CREATE PROCEDURE [BulbDetails]
	@PartNumber char (255),
	@BulbID char (255)
AS
SELECT Bulb2Fixture.Quan, CatalogMain.StandardCost, CatalogMain.Description
FROM Bulb2Fixture INNER JOIN
	CatalogMain ON Bulb2Fixture.BulbNum = CatalogMain.ProductID
WHERE Bulb2Fixture.BulbNum = @BulbID 
	AND Bulb2Fixture.PartNumber = @PartNumber
GO

CREATE PROCEDURE [BELIbyType]
	@ProjectID int,
	@Type nvarchar (10)
AS
SELECT DISTINCT Type, PartNumber, Description, RT, Cost, Margin, Sell
FROM LineItem
WHERE (ProjectID = @ProjectID) AND
	(NOT (Type IS NULL)) AND 
	(Type <> '') AND 
  	(Type = @Type)
GO

/* frmBid - LoadBELIByPart */
CREATE PROCEDURE [BELIbyPart]
	@ProjectID int,
	@PartNumber char (25)
AS
SELECT DISTINCT PartNumber, Description, RT, Cost, Margin, Sell
FROM LineItem
WHERE ProjectID = @ProjectID AND PartNumber = @PartNumber
GO

CREATE PROCEDURE [BeByType]
	@ProjectID int,
	@Type nvarchar(10),
	@PartNumber char(25),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money
AS	
UPDATE LineItem SET
 	Partnumber = @PartNumber, 
	Description = @Description, 
	Cost = @Cost, 
	Margin = @Margin, 
	Sell = @Sell 
Where Type = @Type
AND ProjectID = @ProjectID
AND Shipped = 0
GO

CREATE PROCEDURE [BeByPartNumber]
	@ProjectID int,
	@OldPartNumber char(25),
	@PartNumber char(25),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money
AS	
UPDATE LineItem SET
 	Partnumber = @PartNumber, 
	Description = @Description, 
	Cost = @Cost, 
	Margin = @Margin, 
	Sell = @Sell 
Where PartNumber = @OldPartNumber 
AND ProjectID = @ProjectID
AND Shipped = 0
GO

CREATE PROCEDURE [TypeSummaryNoType]
	@ProjectID int
AS
Select type, PartNumber, Description, sum(Quantity) AS quan, Cost, Sell
	from LineItem
	where ProjectID = @ProjectID AND Type = '' And BldgID is not null
	group by PartNumber, Type, Description, Cost, Margin, Sell

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitSummaryL]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitSummaryL
END
GO

CREATE PROCEDURE [TypeSummary]
	@ProjectID int
AS
Select type, PartNumber, Description, sum(Quantity) AS quan, Cost, Sell
	from LineItem
	where ProjectID = @ProjectID AND Type <> '' And BldgID is not null
	group by PartNumber, Type, Description, Cost, Margin, Sell
GO

CREATE PROCEDURE [SiteSummaryExp]
	@ProjectID int,
	@OrderBy varchar(15) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (Quantity * se) as Total
from(
	select LineItem.Description Descr, LineItem.PartNumber pn, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty, LineItem.Location lo
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'S' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Sell
) tempTable
ORDER BY(
	CASE 
	WHEN @OrderBy = 'PN' THEN pn
	WHEN @OrderBy = 'T' THEN Ty	
	WHEN @OrderBy = 'L'  THEN lo
	END
)
GO

CREATE PROCEDURE [ShipDates]
	@ProjectID int
AS
SELECT     
	Building.BldgName, Building.BldgNumber, Building.Type, Building.Description, Building.shipdate,
	Building.shipdater, Building.Shipped, Building.Shippedr, SUM(LineItem.Cost * LineItem.Quantity) AS Cost, 
	SUM(LineItem.Sell * LineItem.Quantity) AS Sell
FROM Building INNER JOIN
	LineItem ON Building.BldgID = LineItem.BldgID
WHERE (Building.ProjectID = @ProjectID)
GROUP BY 
	Building.BldgName, Building.BldgNumber, Building.Type, 
	Building.Description, Building.shipdate, shipdater, Building.Shipped, shippedr
ORDER BY Building.shipdate
GO

CREATE PROCEDURE [SetShipDateR]
          @ProjectID int
AS
Update Building 
	SET ShipDateR = ShipDate - 60,
	ShippedR = 0
	WHERE ProjectID = @ProjectID

Update LineItem
	SET ShipDate = ShipDate - 60,
	Shipped = 0
	WHERE ProjectID = @ProjectID And RT = 'R'
GO

CREATE PROCEDURE [SetBldgShipDateR]
	@BldgID int
AS
Update Building 
	SET ShipDateR = ShipDate - 60,
	ShippedR = 0
	WHERE BldgID = @BldgID

Update LineItem
	SET ShipDate = ShipDate - 60,
	Shipped = 0
	WHERE BldgID = @BldgID And RT = 'R'
GO

CREATE PROCEDURE [rptUnitDetailPerBldg]
	@ProjectID int
AS
select Building.Type, Building.Description AS [Building Type], count(UnitType) as Quan, Unit.UnitType, Unit.Description Description
from Building inner join Unit 
on Building.BldgID = Unit.BldgID 
where Building.ProjectID =@ProjectID and LEFT(Building.Type, 1) = 'B'
group by Building.Type, Building.Description, UnitType, Unit.Description
order by Type, UnitType
GO

CREATE PROCEDURE [rptUnitCount]
	@ProjectID int
AS
	SELECT COUNT(UNITID) AS Units FROM Unit WHERE ProjectID = @ProjectID  
	AND (SUBSTRING(UnitType, 1, 1) = 'U') and BldgID is not null
GO

CREATE PROCEDURE [rptTypeDef]
	@ProjectID int
AS
select Distinct Type, PartNumber, Description, Cost, Margin, Sell
from LineItem where ProjectID = @ProjectID and TYPE<>''
order by type
GO

CREATE PROCEDURE [rptSalesTaxRate]
	@ProjectID int
AS
	SELECT SalesTaxRate FROM Project WHERE ProjectID = @ProjectID
GO

Create PROCEDURE [rptQuantityDetails]
	@ProjectID int,
	@Filter Char
AS
select distinct UnitType, Unit.Description AS UnitDesc, Quantity, PartNumber, LineItem.Description Description, Location,
RT, Type, Cost, Margin, Sell from LineItem inner join Unit
on LineItem.UnitID = Unit.UnitID where lineitem.ProjectID = @ProjectID
and ((@Filter is null) OR (LEFT(UnitType, 1) = @Filter)) and Unit.BldgID is not null order by Location, UnitType
GO

CREATE PROCEDURE [rptJobSummaryTBD]
	@ProjectID int
AS
SELECT Partnumber, SUM(Quantity) AS Quan , Cost, Margin, Sell, SUM(Quantity)* Sell AS Total, SUM(shipquan) AS Shipped, Sum(QuanBO) AS BO  
From LineItem Where ProjectID = @ProjectID AND LEFT(PartNumber,3) ='TBD'
Group By Partnumber, Cost, Margin, Sell
Order By Partnumber
GO

CREATE PROCEDURE [rptJobSummary]
	@ProjectID int
AS
SELECT Partnumber, SUM(Quantity) AS Quan , Cost, Margin, Sell, SUM(Quantity)* Sell AS Total, SUM(shipquan) AS Shipped, Sum(QuanBO) AS BO  
From LineItem Where ProjectID = @ProjectID
Group By Partnumber, Cost, Margin, Sell
Order By Partnumber
GO

CREATE PROCEDURE [rptIntro]
	@ProjectID int
AS
SELECT Project.ProjectID,  Project.ContractDate, Project.Description, Project.Location, Project.Address, Project.City, Project.State, Project.Zip, 
	Project.ProjectContact, Project.ClientPM, Project.ClientMobilePhone, Project.LDPM, Project.LDEstimator, Project.BluePrintDate, 
	Project.RevDate, Project.SalesTaxRate, Project.Closed, Project.Notes, Clients.Name, Clients.Address1, Clients.Address2, 
	Clients.city AS CCity, Clients.State AS CState, Clients.Zip AS CZip, Clients.Phone1, Clients.Phone2, Clients.FAX, 
	Clients.email, Clients.contact, Clients.taxexempt, Project.ProjectDate, Project.ProjectContact AS Contact
FROM Project INNER JOIN
	Clients ON Project.ClientID = Clients.ClientID
WHERE (Project.ProjectID = @ProjectID)
GO

CREATE PROCEDURE [rptComments]
	@ProjectID int
AS
SELECT EntryDate, Author, Comment 
FROM Comment
WHERE ProjectID = @ProjectID
ORDER BY EntryDate
GO

CREATE PROCEDURE [rptBOMIntro]
	@BldgID int
AS
SELECT Building.BldgName, Building.BldgNumber, Building.Type, Building.Description, Building.Location, Building.Address, 
	Building.shipdate, Building.shipsequence, Project.ProjectID, Project.ContractDate, Project.Description AS JobDesc, 
    Project.Location AS JobLocation, Clients.Name
FROM Building INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID INNER JOIN
    Clients ON Project.ClientID = Clients.ClientID
WHERE (Building.BldgID = @BldgID)
GO

CREATE PROCEDURE [rptBldgTypeCount]
	@ProjectID int
AS
SELECT Type, COUNT(BldgNumber) AS BldgCount
FROM dbo.Building
WHERE (ProjectID = @ProjectID) AND (SUBSTRING(dbo.Building.Type, 1, 1) = 'B')
GROUP BY Type
GO

CREATE PROCEDURE [rptBldgCount]
	@ProjectID int
AS
	SELECT COUNT(BldgID) AS Buildings FROM Building 
	WHERE ProjectID = @ProjectID  
	AND (SUBSTRING(Type, 1, 1) = 'B')
GO

CREATE PROCEDURE [rptAllUnitDetails]
	@ProjectID int
as
select distinct unit.UnitType, unit.description as UnitDesc, LineItem.Quantity, LineItem.PartNumber, LineItem.Description,
	LineItem.Location, LineItem.RT, LineItem.Type, LineItem.Cost, LineItem.Margin, LineItem.Sell
from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID where LineItem.ProjectID = @ProjectID and LineItem.BldgID is not null
order by UnitType, Location, PartNumber
GO

CREATE PROCEDURE [r_UnitsPerBldg]
	@ProjectID int
AS
SELECT DISTINCT Building.Type, Unit.UnitType, COUNT(Unit.UnitType) AS Quan, 
	Unit.Description, Building.Description AS BldDesc
FROM Building INNER JOIN
	Unit ON Building.BldgID = Unit.BldgID
WHERE (Building.ProjectID = @ProjectID) AND (SUBSTRING(Unit.UnitType, 1, 1) = 'U')
GROUP BY Building.BldgName, Unit.UnitType, Unit.Description, Building.Description, Building.Type
GO

CREATE PROCEDURE [r_main]
	@ProjectID int
AS
SELECT Project.ProjectID, Project.SourceID, Project.ContractDate, Project.ProjectDate, Project.Description, Project.Location, Project.Address, 
	Project.City, Project.State, Project.Zip, Project.ProjectContact, Project.ClientPM, Project.ClientMobilePhone, Project.LDPM, 
    Project.Notes, Project.LDEstimator, Project.BluePrintDate, Project.RevDate, Project.SalesTaxRate, Project.ContractAmount, 
    Project.ContractBalance, Clients.Name, Clients.Address1, Clients.Address2, Clients.city AS cCity, Clients.State AS cState, 
    Clients.Zip AS cZip, Clients.Phone1, Clients.Phone2, Clients.FAX, Clients.email, Clients.contact, 
    Clients.taxexempt
FROM Clients INNER JOIN
	Project ON Clients.ClientID = Project.ClientID
WHERE (Project.ProjectID = @ProjectID)
GO

CREATE PROCEDURE [r_BldgTypes]
	@ProjectID int
AS
SELECT Type, Description, COUNT(Type) AS Quan
FROM dbo.Building
WHERE (ProjectID = @ProjectID) AND (SUBSTRING(Type, 1, 1) = 'B')
GROUP BY Type, Description
ORDER BY Type
GO

/* frmBid - LoadUnitCount */
CREATE PROCEDURE [ProjectUnitCount]
	@ProjectID int
AS
SELECT UnitType, Description, Count(UnitID) AS QUAN
FROM Unit
WHERE ProjectID = @ProjectID AND
	LEFT(UnitType, 1) = 'U' And BldgID is not null
GROUP BY UnitType, Description
ORDER BY UnitType
GO

CREATE PROCEDURE [ProjectList]
	@ProjectType varchar(25)
AS
SELECT ProjectID,'LD-'+ RTRIM(CAST(ProjectID AS char))+ ': '+ISNULL(Description, '')+' '+ISNull(Location, '') AS Proj
FROM Project
Where ProjectType = @ProjectType

UNION ALL
SELECT 0 AS ProjectID, ' - Please Select A '+ @ProjectType +' - ' AS Proj

ORDER BY Proj
GO

CREATE PROCEDURE [PartsMaxMin]
	@StartDate datetime,
	@EndDate datetime,
	@ProjectType varchar(15)
AS
SELECT PartNumber, max(Cost) AS Max, min(cost) AS Min, sum(Quantity) AS Total, 
SUM((Quantity*Cost))/ sum(Quantity) AS Average
from LineItem 
WHERE shipdate between @StartDate and  @EndDate and shipped = 0 and BldgID is not null and quantity != 0 and ProjectID in
(select ProjectID from Project where ProjectType = @ProjectType)
Group By Partnumber
Order By PartNumber
GO

CREATE PROCEDURE [PartsFind]
	@PartNumber char (25),
	@ProjectType varchar (15),
	@Criteria char (1)
AS
IF @Criteria = 'N' BEGIN
(SELECT LineItem.PartNumber, SUM(LineItem.Quantity) AS Quantity, LineItem.Shipped, Project.ProjectID, Project.Description
FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID
WHERE (LineItem.PartNumber = @PartNumber) and ProjectType = @ProjectType
GROUP BY LineItem.PartNumber, LineItem.Shipped, Project.Description, Project.ProjectID
HAVING (LineItem.Shipped = 0))
END

IF @Criteria = 'S' BEGIN
(SELECT LineItem.PartNumber, SUM(LineItem.Quantity) AS Quantity, LineItem.Shipped, Project.ProjectID, Project.Description
FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID
WHERE LineItem.PartNumber = @PartNumber and ProjectType = @ProjectType
GROUP BY LineItem.PartNumber, LineItem.Shipped, Project.Description, Project.ProjectID
HAVING LineItem.Shipped = 1)
END

IF @Criteria = 'U' BEGIN
(SELECT LineItem.PartNumber, SUM(LineItem.Quantity) AS Quantity, LineItem.Shipped, Project.ProjectID, Project.Description
FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID
WHERE LineItem.PartNumber = @PartNumber and ProjectType = @ProjectType
GROUP BY LineItem.PartNumber, LineItem.Shipped, Project.Description, Project.ProjectID)
END
GO

CREATE PROCEDURE [PartsByDateRangeJN]
	@StartDate datetime,
	@EndDate datetime,
	@ProjectType varchar(15)
AS

SELECT Project.ProjectID as BidID, LineItem.PartNumber, LineItem.Description, SUM(Cost * Quantity) AS Cost, SUM(Sell * Quantity) AS Sell, SUM(Quantity) AS Quan
FROM Project INNER JOIN
	dbo.LineItem ON dbo.Project.ProjectID = dbo.LineItem.ProjectID
WHERE (ShipDate BETWEEN @StartDate AND @EndDate) AND shipped = 0 and ProjectType = @ProjectType and BldgID is not null
GROUP BY Project.ProjectID, PartNumber, LineItem.Description
ORDER BY PartNumber
GO

CREATE PROCEDURE [PartsByDateRangeInv]
	@StartDate as datetime,
	@EndDate as datetime,
	@ProjectType as varchar(15)
AS

SELECT PartNumber, LineItem.Description, VendorCode, CatalogMain.VendorPartNo,
	CatalogMain.AvailableUnits, CatalogMain.OnOrderUnits, CatalogMain.CommittedUnits,		
	SUM(Cost * Quantity) AS Cost, SUM(Sell * Quantity) AS Sell, SUM(Quantity) AS Quan
FROM CatalogMain RIGHT OUTER JOIN
	LineItem ON CatalogMain.ProductID = LineItem.PartNumber
WHERE (ShipDate BETWEEN @StartDate AND @EndDate) AND shipped = 0 And BldgID is not null and ProjectID in 
(select ProjectID from Project where ProjectType = @ProjectType)
GROUP BY PartNumber, LineItem.Description, VendorCode, CatalogMain.VendorPartNo, CatalogMain.AvailableUnits, 
	CatalogMain.OnOrderUnits, CatalogMain.CommittedUnits
ORDER BY PartNumber
GO

CREATE PROCEDURE [PartsByDateRange]
	@StartDate as datetime,
	@EndDate as datetime,
	@ProjectType as varchar(15)
AS
SELECT PartNumber, Description, SUM(Cost * Quantity) AS Cost, SUM(Sell * Quantity) AS Sell, SUM(Quantity) AS Quan
FROM LineItem 
WHERE (ShipDate BETWEEN @StartDate AND @EndDate) AND shipped = 0 and BldgID is not null and ProjectID in 
	(select ProjectID from Project where ProjectType = @ProjectType)
GROUP BY PartNumber, Description
ORDER BY PartNumber
GO

CREATE PROCEDURE [PartCount]
	@ProjectID int,
	@ProjectType varchar(15),
	@PartNumber CHAR(25)
AS

select distinct PartNumber, sum(quantity) Quantity from LineItem where ProjectID = @ProjectID and PartNumber = @PartNumber and 
	ProjectID in (Select ProjectID from Project where ProjectType= @ProjectType) group by PartNumber
GO

CREATE PROCEDURE [NewCatalogSO]
	@ProductID nvarchar (255),
	@Description	nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@AverageCost float,
	@StandardCost	float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255),
	@Notes nvarchar(1000)
 AS
INSERT INTO CatalogSO
	(ProductID, Description, VendorCode, VendorPartNo, AlternateVendor, AlternatePartNum, 
	 AverageCost, StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, Leadtime, Notes)
VALUES
	(@ProductID, @Description, @VendorCode, @VendorPartNo, @AlternateVendor, @AlternatePartNum, 
	 @AverageCost, @StandardCost, @AvailableUnits, @OnOrderUnits, @CommittedUnits, @Leadtime, @Notes)
GO

CREATE PROCEDURE [NewCatalogMain]
	@ProductID nvarchar (255),
	@Description	nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@StandardCost	float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255)
 AS
INSERT INTO CatalogMain
	(ProductID, Description, VendorCode, VendorPartNo, AlternateVendor, AlternatePartNum, 
	 StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, Leadtime)
VALUES
	(@ProductID, @Description, @VendorCode, @VendorPartNo, @AlternateVendor, @AlternatePartNum, 
	 @StandardCost, @AvailableUnits, @OnOrderUnits, @CommittedUnits, @Leadtime)
GO

CREATE PROCEDURE [Margins]
	@ProjectID int
AS
select UnitType as Type, SUM(cost) as TotalCost, SUM(sell) as TotalSell from(
select LEFT(UnitType,1) as UnitType, sum (se) as sell, sum(co) as Cost from (
select UnitType, sum(sell * Quantity) as se, sum(cost * Quantity) as co from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID where LineItem.ProjectID = @ProjectID 
and LineItem.BldgID is not null
group by UnitType) tq
group by UnitType) tq2
group by UnitType
GO

/* frmBid - LoadBETypeCBO */
CREATE PROCEDURE [LineItemTypes]
	@ProjectID int
AS
select distinct type, Type + ' - ' + Partnumber As Item 
from LineItem
where ProjectID = @ProjectID and type is not null and type <> ''
Order BY Type
GO

CREATE PROCEDURE [LineItemSummaryExp]
	@ProjectID int
 AS
Select pn PartNumber, descr Description, Quantity, co Cost, se Price, (Quantity * se) as [Total Sell]
from(
	select Description Descr, PartNumber pn, Cost co, Sell se, sum(Quantity) AS Quantity
	from LineItem
	where ProjectID = @ProjectID and BldgID is not null 
	group by PartNumber , Description, Cost, Sell
) tempTable
order by PartNumber
GO

CREATE PROCEDURE [LineItemSummary]
	@ProjectID int
 AS
Select pn PartNumber, descr Description, Quantity, co Cost, se Sell, (Quantity * Co) as [Total Cost],
(Quantity * Se) as [Total Sell], (Quantity * Se) - (Quantity * Co) as Net
from(
	select Description Descr, PartNumber pn, Cost co, Margin ma, Sell se, sum(Quantity) AS Quantity, RT
	from LineItem
	where ProjectID = @ProjectID and BldgID is not null
	group by RT, PartNumber , Description, Cost, Margin, Sell
) tempTable
order by RT, PartNumber
GO

CREATE PROCEDURE [EditTShipped]
	@BldgID int,
	@shipped bit = 1
AS
	Update Building Set Shipped = @shipped Where bldgid = @BldgID
	Update LineItem Set Shipped = @shipped Where bldgid = @BldgID AND RT = 'T'
GO

CREATE PROCEDURE [EditShipped]
	@BldgID int,
	@shipped bit = 1
AS
	Update Building Set Shipped = @shipped,	ShippedR = @Shipped
	Where bldgid = @BldgID
	Update LineItem Set Shipped = @shipped Where bldgid = @BldgID
GO

CREATE PROCEDURE [EditShipDate]
	@BldgID int,
	@shipdate datetime,
	@shipdateR datetime
AS
	Update Building 
		Set Shipdate = @shipdate,
		ShipdateR = @ShipdateR
		Where bldgid = @BldgID 

	Update LineItem Set Shipdate = @shipdate Where bldgid = @BldgID AND RT = 'T'
	Update LineItem Set Shipdate = @shipdateR Where bldgid = @BldgID AND RT = 'R'
GO

CREATE PROCEDURE [EditRShipped]
	@BldgID int,
	@shipped bit = 1
AS
	Update Building Set ShippedR = @shipped Where bldgid = @BldgID
	Update LineItem Set Shipped = @shipped Where bldgid = @BldgID AND RT = 'R'
GO

CREATE PROCEDURE [EditProject]
	@ProjectID int,
	@ContractAmount money = NULL,
	@Description nvarchar(50)= NULL,
	@Location nvarchar(50)= NULL,
	@Address nvarchar(50)= NULL,
	@City nvarchar(50)= NULL,
	@State char(2)= NULL,
	@Zip char(10)= NULL,
	@ProjectContact nvarchar(50)= NULL,
	@ClientPM nvarchar(50)= NULL,
	@ClientMobilePhone char(14)=NULL,
	@LDPM nvarchar(50)=NULL,
	@Notes nvarchar(250)= NULL,
	@LDEstimator nvarchar(50)= NULL,
	@BluePrintDate datetime= NULL,
	@RevDate datetime= NULL,
	@SalesTaxRate float= NULL
AS
UPDATE Project
SET
	ContractAmount = @ContractAmount,
	Description = @Description, 
	Location = @Location, 
	Address = @Address, 
	City = @City, 
	State = @State,
	Zip = @Zip,
	ClientPM = @ClientPM,
	ClientMobilePhone = @ClientMobilePhone,
	LDPM = @LDPM,
	Notes = @Notes,
	LDEstimator = @LDEstimator,
	BluePrintDate = @BluePrintDate,
	RevDate = @RevDate,
	SalesTaxRate = @SalesTaxRate,
	ProjectContact = @ProjectContact
WHERE
	ProjectID = @ProjectID
GO

CREATE PROCEDURE [EditLineItem]
	@ItemID int,
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@Quantity int,
	@Location nvarchar(50),
	@Type nvarchar(10),
	@Shipped bit,
	@shipdate datetime,
	@shipQuan int,
	@QuanBO int
AS 
UPDATE LineItem
	SET 
	RecordSource = @RecordSource,
	PartNumber = @PartNumber,
	RT = @RT,
	Description = @Description,
	Cost = @Cost,
	Margin = @Margin,
	Sell = @Sell,
	Quantity = @Quantity,
	Location = @Location,
	Type = @Type,
	Shipped = @Shipped,
	ShipDate = @shipdate,
	ShipQuan = @shipQuan,
	QuanBo = @QuanBO
WHERE ItemID = @ItemID
GO

CREATE PROCEDURE [EditChangeOrder]
	@COID int,
	@CODate datetime,
	@EnteredBy char(10),
	@ApprovedBy char(50),
	@CODesc nvarchar(1000),
	@Misc nvarchar(1000),
	@PriorBalance money,
	@NewBalance money
AS
Update  ChangeOrder SET	
	CODate = @CODate,
	EnteredBy = @EnteredBy,
	ApprovedBy = @ApprovedBy,
	CoDesc = @CODesc,
	Misc = @Misc,
	PriorBalance = @PriorBalance,
	NewBalance = @NewBalance
WHERE COID = @COID
GO

CREATE PROCEDURE [EditCatalogSO]
	@ProductID nvarchar (255),
	@Description	nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@AverageCost float,
	@StandardCost	float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255),
	@Notes nvarchar(1000)
AS
UPDATE CatalogSO SET
	Description = @Description,
	VendorCode = @VendorCode,
	VendorPartNo = @VendorPartNo,
	AlternateVendor = @AlternateVendor,
	AlternatePartNum = @AlternatePartNum,
	AverageCost = @AverageCost,
	StandardCost = @StandardCost,
	AvailableUnits = @AvailableUnits,
	OnOrderUnits = @OnOrderUnits,
	CommittedUnits = @CommittedUnits,
	LeadTime = @LeadTime,
	Notes = @notes
WHERE ProductID = @ProductID
GO

CREATE PROCEDURE [EditCatalogMain]
	@ProductID nvarchar (255),
	@Description nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@StandardCost float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255)
AS
UPDATE CatalogMain SET
	Description = @Description,
	VendorCode = @VendorCode,
	VendorPartNo = @VendorPartNo,
	AlternateVendor = @AlternateVendor,
	AlternatePartNum = @AlternatePartNum,
	StandardCost = @StandardCost,
	AvailableUnits = @AvailableUnits,
	OnOrderUnits = @OnOrderUnits,
	CommittedUnits = @CommittedUnits,
	LeadTime = @LeadTime
WHERE ProductID = @ProductID
GO

CREATE PROCEDURE [EditBldg]
	@BldgID int,
	@ProjectID int,
	@BldgNumber int=NULL,
	@Type nvarchar(50),
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@Notes nvarchar(250),
	@shipdate datetime=NULL,
	@shipsequence int=NULL,
	@Shipped bit=NULL,
	@ShippedR bit=NULL,
	@ShipdateR datetime=null,
	@BldgName varchar(50)=Null
 AS 
UPDATE Building 
	SET
	ProjectID = @ProjectID,
	BldgNumber = @BldgNumber,
	Type = @Type,
	Description = @Description,
	Location = @Location,
	Address = @Address,
	Notes = @Notes,
	shipdate = @shipdate,
	shipsequence = @shipsequence,
	Shipped = @Shipped,
	ShippedR = @ShippedR,
	ShipdateR = @ShipdateR,
	BldgName = @BldgName
	
WHERE BldgID = @bldgID
GO

CREATE PROCEDURE [CTPartVSUType]
	@ProjectID int
AS
select PartNumber, Location, Type, UnitType, SUM(Quantity) As Quan
FROM LineItem inner join Unit
on LineItem.UnitID = Unit.UnitID
where LineItem.ProjectID = @ProjectID
group by PartNumber, Location, TYPE, UnitType
GO

CREATE PROCEDURE [CTBldgVSUCnt]
	@ProjectID int
AS
select Building.BldgNumber, Building.Description, Building.Type, Count(Unit.UnitID) from Building inner join Unit
on Unit.BldgID = Building.BldgID where Building.ProjectID = @ProjectID
group by Building.BldgNumber, Building.Description, Building.Type
GO

CREATE PROCEDURE [CopyLineItemDefinition]
    @ProjectID int,
	@NewUnitID int,
	@OldUnitID int
AS
insert into LineItem (ProjectID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell)
Select @ProjectID, @NewUnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell From LineItem Where
UnitID = @OldUnitID
GO

CREATE PROCEDURE [CopyLineItemDef] 
	@ProjectID int,
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@margin int,
	@Sell money,
	@NewItemID int OUTPUT
AS
INSERT INTO LineItem (ProjectID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell)
VALUES (@ProjectID, @RecordSource, @PartNumber, @RT, @Description, @Cost, @Margin, @Sell);
SELECT @NewItemID = @@IDENTITY
RETURN @NewItemID
GO

CREATE PROCEDURE [CommonSummaryExp]
	@ProjectID int,
	@Order char(2) = 'pn'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (se * Quantity) as Total
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'X' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order BY ( 
case
	when @Order = 'PN' then pn
	when @Order = 'T' then ty
	when @Order = 'L'  then lo
	END
	)
GO

CREATE PROCEDURE [CommonSummary]
	@ProjectID int,
	@Order char(2) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (co * Quantity) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'X'
	group by LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
ORDER BY ( CASE 
	WHEN @Order = 'PN' THEN pn
	WHEN @Order = 'T' THEN  ty
	WHEN @Order = 'L'  THEN lo
	END
	),
	Type
GO

CREATE PROCEDURE [DeleteLineItems]
	@ProjectID int,
	@Partnumber char (25)
AS
Delete from LineItem where PartNumber = @Partnumber AND ProjectID = @ProjectID
GO

/* frmBid - btnBEDeleteByType_Click */
CREATE PROCEDURE [DeleteLIByType]
	@ProjectID int,
	@Type char (10)
AS
DELETE FROM LineItem WHERE ProjectID = @ProjectID And Type = @Type
GO

CREATE PROCEDURE [DeleteCatalogSO]
	@ProductID nvarchar (255)
AS
DELETE FROM CatalogSO WHERE
	ProductID = @ProductID
GO

CREATE PROCEDURE [DeleteCatalogMain]
	@ProductID nvarchar (255)
AS
DELETE FROM CatalogMain WHERE
ProductID = @ProductID
GO

CREATE PROCEDURE [UpdateShipping]
	@ProjectID int,
	@DateChange int
AS
UPDATE Building
SET shipdateR = ShipdateR + @DateChange
WHERE ProjectID = @ProjectID and ShippedR = 0

UPDATE Building
SET shipdate = Shipdate + @DateChange
WHERE ProjectID = @ProjectID and Shipped = 0

UpDate LineItem
SET ShipDate = ShipDate + @DateChange
WHERE ProjectID = @ProjectID and Shipped = 0
GO

CREATE PROCEDURE [UpdateProjectItemDesc]
	@ProjectID int
AS
UPDATE LineItem
SET LineItem.Description = catalogmain.Description
FROM LineItem INNER JOIN
	CatalogMain ON LineItem.PartNumber = CatalogMain.ProductID 
WHERE LineItem.ProjectID = @ProjectID
GO

CREATE PROCEDURE [UpdateProjectCost]
	@ProjectID int
AS
UPDATE LineItem
SET Cost = CatalogMain.standardcost, 
	Sell = (CatalogMain.standardcost/(Cast((100-Margin) AS float)/100))
FROM LineItem INNER JOIN
	CatalogMain ON LineItem.PartNumber = CatalogMain.ProductID 
WHERE LineItem.ProjectID = @ProjectID
GO

/* frmBid - btnAdjustSell_Click */
CREATE PROCEDURE [UpdatePrice]
	@ProjectID int,
	@PartNumber char (25),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@RT char (1)
AS
UPDATE LineItem
SET Cost = @Cost,
	Margin = @Margin,
	Sell = @Sell,
	Description = @Description,
	RT = @RT
WHERE 
	ProjectID = @ProjectID AND
	PartNumber = @Partnumber AND
	shipped = 0
GO

/* frmBid -btnBERByType_Click */
CREATE PROCEDURE [UpdateLineItemByType]
	@Type nvarchar (10),
	@ProjectID int,
	@RecordSource char (10),
	@PartNumber char (25),
	@RT char (1),
	@Description nvarchar (50),
	@Cost money,
	@Margin int,
	@Sell money,
	@OldPartNumber char (25)
AS
Update LineItem Set RecordSource = @RecordSource, PartNumber = @PartNumber, RT = @RT, Description = @Description, Cost = @Cost,
	Margin = @Margin, Sell = @Sell where ProjectID = @ProjectID and PartNumber = @OldPartNumber
GO

CREATE PROCEDURE [UnitTypes]
	@ProjectID int
AS
SELECT TOP 100 PERCENT UnitType, Description, COUNT(UnitType) AS Quan
FROM Unit
WHERE ProjectID = @ProjectID AND SUBSTRING(UnitType, 1, 1) = 'U'
GROUP BY UnitType, Description
ORDER BY UnitType
GO

CREATE PROCEDURE [UnitSummaryExp]
	@ProjectID int,
	@OrderBy varchar(15) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (Quantity * se) as Total
from(
	select LineItem.Description Descr, LineItem.PartNumber pn, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty, LineItem.Location lo
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'U' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Sell
) tempTable
ORDER BY(
	CASE 
	WHEN @OrderBy = 'PN' THEN pn
	WHEN @OrderBy = 'T' THEN Ty	
	WHEN @OrderBy = 'L'  THEN lo
	END
)
GO

CREATE PROCEDURE [UnitSummary]
	@ProjectID int,
	@OrderBy varchar(15),
	@UnitType varchar(15)
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (Quantity * co) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = @UnitType
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order by Location
GO

/* frmBid - LoadUnitsPerBldg */
CREATE PROCEDURE [UnitsPerAllBuilding]
	@ProjectID int
 AS
select Building.BldgID bldgID, Building.Type, Building.BldgNumber, Building.Description, Building.Location, Building.Address, sq.ut UnitType, sq.cnt Quan
from Building inner join 
	(select UnitType ut, BldgID, count(*) cnt from Unit where left (UnitType,1 ) = 'U' group by BldgID, UnitType) sq 
	on Building.BldgID = sq.BldgID
where (Building.ProjectID = @ProjectID)
Order by Type, bldgNumber
GO

CREATE PROCEDURE [UnitsBOM]
	@BldgID int
AS
SELECT     COUNT(Unit.UnitType) AS [Unit Count], Unit.UnitType, LineItem.Quantity, LineItem.PartNumber, 
                      LineItem.Description, LineItem.Location, LineItem.Sell, LineItem.Quantity * LineItem.Sell AS TotalSell
FROM         Unit INNER JOIN
                      LineItem ON Unit.UnitID = LineItem.UnitID
WHERE     (Unit.BldgID = @BldgID)
GROUP BY Unit.UnitType, LineItem.Quantity, LineItem.PartNumber, LineItem.Description, LineItem.Location, 
                      LineItem.Sell, LineItem.Quantity * LineItem.Sell
ORDER BY Location
GO

/* frmBid - LoadUnits, LoadBldgUnits */
CREATE PROCEDURE [Units]
	@ProjectID int
AS
Select Distinct UnitType, UnitType + ': ' + Description As U
From Unit
Where ProjectID = @ProjectID
ORDER BY U
GO

CREATE PROCEDURE [UnitDetail]
	@ProjectID int,
	@UnitType char
AS
SELECT DISTINCT  Unit.UnitType, Unit.Description, LineItem.PartNumber, LineItem.Description AS Expr1, 
                      LineItem.Location, LineItem.RT, LineItem.Type, LineItem.Quantity, LineItem.Sell, LineItem.Cost
FROM         Unit INNER JOIN
                      LineItem ON Unit.UnitID = LineItem.UnitID
WHERE     (Unit.ProjectID = @ProjectID) AND (SUBSTRING(Unit.UnitType, 1, 1) = @UnitType)
ORDER BY Location,  LineItem.Type
GO

CREATE PROCEDURE [AuxillarySummaryExp]
	@ProjectID int,
	@Order varchar = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (Quantity * se) as Total
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'A' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order by(
case
	when @Order = 'PN' then pn
	when @Order = 'T' then ty
	when @Order = 'L'  then lo
end
)
GO

CREATE PROCEDURE [AuxillarySummary]
	@ProjectID int,
	@Order varchar = 'PN'
AS
SELECT PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, 
	Cost, Sell, SUM(LineItem.Quantity) AS Quantity
FROM Unit INNER JOIN
	LineItem ON Unit.UnitID = LineItem.UnitID
WHERE (LineItem.ProjectID = @ProjectID) AND (SUBSTRING(Unit.UnitType, 1, 1) = 'A')
GROUP BY LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, 
	LineItem.Cost, LineItem.Sell
ORDER BY 
	( CASE 
	WHEN @Order = 'PN' THEN  PartNumber
	WHEN @Order = 'T' THEN  Type
	WHEN @Order = 'L'  THEN Location
	END
	), Type
GO

CREATE PROCEDURE [AlternateSummary]
	@ProjectID int
 AS
Select AltID, OrgProductID, OrgDescription, Sell, SUM(Quan) QUAN, AltProductID, AltDescription, AltSell
from (select sum(LineItem.Quantity) AS Quan, PartNumber, Sell
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID
	group by LineItem.PartNumber, Sell
) tempTable inner join Alternates on tempTable.PartNumber = Alternates.OrgProductID
Where ProjectID = @ProjectID
GROUP BY AltID, OrgProductID, OrgDescription,Sell, AltProductID, AltDescription, AltSell
GO

CREATE PROCEDURE [DeleteBldg]
	@BldgID int
AS
DELETE Building WHERE BldgID = @BldgID
DELETE BldgUnitCount WHERE BldgID = @BldgID
GO

ALTER TABLE [Bulb2Fixture] ADD  CONSTRAINT [DF_Fixture2Bulb_Quan]  DEFAULT (0) FOR [Quan]
GO
ALTER TABLE [Bulbs] ADD  CONSTRAINT [DF_Bulbs_Cost]  DEFAULT (0) FOR [Cost]
GO
ALTER TABLE [BldgUnitCount]  WITH NOCHECK ADD  CONSTRAINT [FK_BldgUnitCount_Building] FOREIGN KEY([BldgID])
REFERENCES [Building] ([BldgID])
ON DELETE CASCADE
GO
ALTER TABLE [BldgUnitCount] CHECK CONSTRAINT [FK_BldgUnitCount_Building]
GO
