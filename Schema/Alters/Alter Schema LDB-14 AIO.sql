--Drop contraints for ease of data transition. These are restored at the end.

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_Units_j_Bldg]') and parent_object_id = object_id(N'[dbo].[j_Units]'))
	alter table j_Units drop constraint FK_j_Units_j_Bldg

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_Unit_Bldg]') and parent_object_id = object_id(N'[dbo].[j_Units]'))
	alter table j_Units drop constraint FK_Unit_Bldg
	
if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_Bldg_j_Jobs]') and parent_object_id = object_id(N'[dbo].[j_Bldg]'))
	alter table j_bldg drop constraint FK_j_Bldg_j_Jobs

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_LineItems_j_Units]') and parent_object_id = object_id(N'[dbo].[j_LineItems]'))
	alter table j_LineItems drop constraint FK_j_LineItems_j_Units

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_Comments_j_Jobs]') AND parent_object_id = OBJECT_ID(N'[dbo].[j_Comments]'))
	alter table j_Comments drop constraint FK_j_Comments_j_Jobs

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_Comments_Projects]') AND parent_object_id = OBJECT_ID(N'[dbo].[j_Comments]'))
	alter table j_Comments drop constraint FK_Comments_Projects
go
-- Create a series of tables that are used to transition the data
-- These are used in a couple of different way. One is to help transform
-- them as they make their way into the new tables. The other is to help
-- record a lookup of their former identities for restoring foreign keys

-- Used in a join to create a sequenced column
if Object_id('tempdb..#ordinals','u')is not null begin
	drop table #ordinals
end
create table #ordinals(
	ordernum int primary key identity,
	tempNumber int
)

-- A couple of temp table is needed to help with flattening the Unit relationship
if Object_id('tempdb..#bidconversion','u')is not null begin
	Drop table #bidconversion
end
create table #bidconversion(
	bcID int identity(1,1),
	BidId int,
	UnitID int,
	UnitType varchar(10),
	[Description] varchar(50),
	Quan int,
	BldgID int
)
if Object_id('tempdb..#units','u')is not null begin
	Drop table #units
end
create table #units(
	newID int identity,
	UnitID int,
	BidID int,
	UnitType varchar(10),
	[Description] varchar(50),
	BldgID int,
	UnitName varchar(20)
)

-- Used to maintain the former building relationship
if Object_id('tempdb..#Building','u')is not null begin
	Drop Table #Building
end
Create table #Building (
	NewBuildingID int NOT NULL Primary Key IDENTITY,
	OldBuildingID int,
	ProjectID int,
	BldgNumber int,
	Type nvarchar(50),
	Description nvarchar(50),
	location nvarchar(50),
	Address nvarchar(50),
	Notes nvarchar(250),
	shipdate datetime,
	shipsequence int,
	Shipped bit,
	ShippedR bit,
	ShipdateR datetime,
	BldgName varchar(50)
)

-- There was overlap in the Bid and Job table they needed to be condensed and their former relationships saved
if Object_id('tempdb..#Project','u')is not null begin
	Drop Table #Project
end

Create table #Project (
	NewProjectID int NOT NULL Primary Key IDENTITY,
	OldProjectID int,
	LDID int NOT NULL,
	SourceID int,
	ProjectDate datetime,
	ClientID int,
	Description varchar(50),
	Location varchar(50),
	Address varchar(50),
	City varchar(50),
	State char(2),
	Zip char(10),
	BidContact varchar(50),
	ClientPM varchar(50),
	ClientMobilePhone varchar(14),
	LDPM varchar(50),
	Notes varchar(250),
	LDEstimator varchar(50),
	BluePrintDate datetime,
	RevDate datetime,
	SalesTaxRate float,
	AuxLabel varchar(25),
	ContractDate datetime,
	ContractAmount money,
	ContractBalance	money,
	ProjectType	varchar(25),
	Closed bit
)

--Used to hold the flattening of the line item object and help reestablish the foregin keys
if Object_id('tempdb..#Itemconversion','u')is not null begin
	Drop table #Itemconversion
end
create table #Itemconversion(
	icID int identity(1,1),
	ItemID int,
	ProjectID int,
	BldgID int,
	UnitID int,
	RecordSource varchar(10),
	PartNumber varchar(25),
	RT char(1),
	[Description] varchar(50),
	Cost money,
	Margin int,
	Sell money,
	Quantity int,
	Location varchar(50),
	Type varchar(10),
	Shipped bit,
	ShipDate datetime,
	ShipQuan int,
	QuanBO int,
	UnitType varchar(25)
)
go

-- The new Project table that will house bids, jobs and verbals
if exists (Select * from sysobjects where id = object_id(N'[dbo].[Project]'))
	Drop Table Project

Create table Project (
	ProjectID int NOT NULL Primary Key IDENTITY,
	SourceID int,
	LDID int NOT NULL,
	ProjectDate datetime,
	ClientID int,
	Description varchar(50),
	Location varchar(50),
	Address varchar(50),
	City varchar(50),
	State char(2),
	Zip char(10),
	ProjectContact varchar(50),
	ClientPM varchar(50),
	ClientMobilePhone varchar(14),
	LDPM varchar(50),
	Notes varchar(250),
	LDEstimator varchar(50),
	BluePrintDate datetime,
	RevDate datetime,
	SalesTaxRate float,
	AuxLabel varchar(25),
	ContractDate datetime,
	ContractAmount money,
	ContractBalance money,
	ProjectType varchar(25),
	Closed bit
)
go

--Migrate Jobs from j_Jobs table
Insert into #Project (OldProjectID, SourceID, LDID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ContractDate, ContractAmount, ContractBalance, Closed, ProjectType) Select JobID, BidID, BidID, JobDate, ClientID,
	Description, Location, Address, City, State, Zip, BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ContractDate, ContractAmount, ContractBalance, Closed, 'Job' from j_Jobs
	
--Migrate Bids from Bids table
Insert into #Project (OldProjectID, LDID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ProjectType) Select BidID, BidID, EntryDate, ClientID,	Description, Location, Address, City, State, Zip, 
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	'Bid' from Bids
go

--Update the source ID of "Job" projects 
update p1 set SourceID = p2.NewProjectID from #Project p2 inner join #Project p1 on p1.SourceID = p2.OldProjectID where p1.ProjectType = 'Job' and p2.ProjectType = 'Bid'

--Place them into the newly formed table
set identity_insert Project on
insert into Project(ProjectID, SourceID, LDID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	ProjectContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ContractDate, ContractAmount, ContractBalance, ProjectType, Closed) select NewProjectID, SourceID, LDID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel, ContractDate, ContractAmount, ContractBalance,
	ProjectType, Closed from #Project

--Restore the identity
set identity_insert Project off
Declare @NEXTPROJECTID int
Select @NEXTPROJECTID = MAX(ProjectID) from Project
DBCC CHECKIDENT('Project', RESEED, @NEXTPROJECTID)

-- Update any bid/job reference to the new project reference so when they are brought over they will already be referencing the correct
-- thing
update Building set BidID = #Project.NewProjectID from #Project inner join Building on #Project.OldProjectID = Building.BidId where #Project.ProjectType = 'Bid'
update j_Bldg set JobID = #Project.NewProjectID from #Project inner join j_Bldg on #Project.OldProjectID = j_Bldg.JobID where #Project.ProjectType ='Job'
update j_LineItems set JobID = #Project.NewProjectID from #Project inner join j_LineItems on #Project.OldProjectID = j_LineItems.JobID where #Project.ProjectType ='Job'
update j_Units set JobID = #Project.NewProjectID from #Project inner join j_Units on #Project.OldProjectID = j_Units.JobID where #Project.ProjectType ='Job'
update BidUnitDef set BidID = #Project.NewProjectID from #Project inner join BidUnitDef on #Project.OldProjectID = BidUnitDef.BidId where #Project.ProjectType = 'Bid'
update BidUnitItemQuan set BidID = #Project.NewProjectID from #Project inner join BidUnitItemQuan on #Project.OldProjectID = BidUnitItemQuan.BidId where #Project.ProjectType = 'Bid'
update bidUnitLineItemDef set BidID = #Project.NewProjectID from #Project inner join bidUnitLineItemDef on #Project.OldProjectID = bidUnitLineItemDef.BidId where #Project.ProjectType = 'Bid'
update Alternates set BidID = #Project.NewProjectID from #Project inner join Alternates on #Project.OldProjectID = Alternates.BidId where #Project.ProjectType = 'Bid'
update bidComments set BidID = #Project.NewProjectID from #Project inner join bidComments on #Project.OldProjectID = bidComments.BidId where #Project.ProjectType = 'Bid'
update j_ChangeOrder set JobID = #Project.NewProjectID from #Project inner join j_ChangeOrder on #Project.OldProjectID = j_ChangeOrder.JobID where #Project.ProjectType ='Job'
update j_Comments set JobID = #Project.NewProjectID from #Project inner join j_Comments on #Project.OldProjectID = j_Comments.JobID where #Project.ProjectType ='Job'

--The Building table needs to be modified to hold both kinds of buildings
if not exists(select * from sys.columns where Name = N'Shipped' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add Shipped bit
end
if not exists(select * from sys.columns where Name = N'ShippedR' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add ShippedR bit
end
if not exists(select * from sys.columns where Name = N'ShipdateR' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add ShipdateR datetime
end
if not exists(select * from sys.columns where Name = N'BldgName' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add BldgName varchar(50)
end
if exists(select * from sys.columns where Name = N'BidID' and Object_ID = Object_ID(N'Building'))begin
	exec sp_rename 'Building.BidID', 'ProjectID', 'Column'
end
go

--Prepare #Building identity
Declare @NEXTBUILDINGID int
Select @NEXTBUILDINGID = MAX(BldgID)+1 from Building
DBCC CHECKIDENT('#Building', RESEED, @NEXTBUILDINGID)

--Move the job buildings over into the temp table so we can save the relationship for later
insert into #Building (OldBuildingID, ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR) select BldgID, JobID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR from j_Bldg
go

--Move the job buildings into Building
set identity_insert Building on
insert into Building(BldgID, ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR) select NewBuildingID, ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR from #Building
set identity_insert Building off

go

--Update any Job, Bldg references
update j_Units set BldgID = #Building.NewBuildingID from #Building inner join j_Units on #Building.OldBuildingID = j_Units.BldgID
update j_LineItems set BldgID = #Building.NewBuildingID from #Building inner Join j_LineItems on #Building.OldBuildingID = j_LineItems.BldgID

--rename the job unit table to get it ready for generic use
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_Units]')) begin
	exec sp_rename 'j_Units', 'Unit'
end
if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'Unit'))begin
	exec sp_rename 'Unit.JobID', 'ProjectID', 'Column'
end
Alter Table Unit Alter Column BldgID int NULL

--Prepare the ordinals table for use
insert into #ordinals(tempNumber) select top 1000 UnitID from Unit
go

--We need to start the units number outside of range of the unit table
--We also have to maintain the history to restablish the relationships
declare @NEXTUNITID int
select @NEXTUNITID = MAX(UnitID) + 1000 from Unit
DBCC CHECKIDENT('#Units', RESEED,@NEXTUNITID)

--Start piecing together bid units
insert into #bidconversion(UnitID, BidId, UnitType, Description, Quan, BldgID)
Select BidUnitDef.UnitID uid, BidID, UnitType, Description, Quan, BLdgID from BidUnitDef inner join BldgUnitCount on BidUnitDef.UnitID = BldgUnitCount.UnitID
group by BidUnitDef.UnitID, BidID, UnitType, Description, Quan, BldgID

--the following represents units that were created but not added to a building yet
insert into #units(UnitID, BidID, UnitType, Description) 
select UnitId, BidId, UnitType,Description from BidUnitDef where UnitID not in (select UnitID from BldgUnitCount)

insert into #units(UnitID, BidID, UnitType, Description, BldgID, UnitName)
select UnitID, BidID, UnitType, Description, BldgID, rtrim(UnitType) + '-' + convert(varchar,ordernum)
from #bidconversion inner join #ordinals on ordernum <= Quan 
go

--prepare the unit identity
set identity_insert Unit on

--Move Bid Units over to the standard table
insert into Unit(UnitID, ProjectID, BldgID, UnitName, UnitType, Description) 
select NEWID, BidID, BldgID, UnitName, UnitType, Description
from #units 
set identity_insert Unit off

--restablish unit identity
declare @NEXTUNITID int
select @NEXTUNITID = MAX(UnitID)+1000 from Unit
DBCC CHECKIDENT('Unit', RESEED,@NEXTUNITID)
go

--Update All Bid Unit references that are NOT part of the unit definition
--The trick is this is only in the line item and we have to wait until we construct it to update it

--Rename the job line item table to get it ready for generic use
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_LineItems]')) BEGIN
	exec sp_rename 'j_LineItems', 'LineItem'
end
if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'LineItem'))begin
	exec sp_rename 'LineItem.JobID', 'ProjectID', 'Column'
end
go

--Prepare itemconversion Identity for Bid line Items
declare @NEXTITEMID int
select @NEXTITEMID = MAX(ItemID) + 1000 from LineItem
DBCC CHECKIDENT('#Itemconversion', RESEED,@NEXTITEMID)

-- Grab a flattening of line items
--Because we are joining on the unit conversion table we are able to grab the new unit id directly
-- no need to adjust later

insert into #Itemconversion(ItemID, ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell,
	Quantity, Location, Type) 
select uid, tqBidID, BldgID, #units.newID, RecordSource, PartNumber, RT, De, Cost, Margin, Sell, Quantity, Location, Type
from (select bidUnitLineItemDef.ItemID uid, bidUnitLineItemDef.BidID tqBidID, BldgID tqBldgID, BidUnitItemQuan.UnitID tqUnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description De, Cost, Margin, Sell, Quantity, Location,BidUnitDef.UnitType tqUnitType,
	Type from bidUnitLineItemDef inner join BidUnitItemQuan on bidUnitLineItemDef.ItemID = BidUnitItemQuan.ItemID and
	bidUnitLineItemDef.BidID = BidUnitItemQuan.BidID inner join BidUnitDef on BidUnitDef.UnitID = BidUnitItemQuan.UnitID and
	BidUnitDef.BidID = BidUnitItemQuan.BidID inner join BldgUnitCount on BldgUnitCount.UnitID = BidUnitItemQuan.UnitID
	group by bidUnitLineItemDef.ItemID, bidUnitLineItemDef.BidID, BldgID, BidUnitItemQuan.UnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description, Cost, Margin, Sell, Quantity, Location, BidUnitDef.UnitType,
	Type)tq inner join #units on tqUnitType = #units.UnitType and tqBidID = #units.BidID and tqBldgID =
	#units.BldgID
	
--LineItems not yet belonging to buildings
insert into #Itemconversion(ItemID, ProjectID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell,
	Quantity, Location, Type)
select uid, tqBidID, #units.newID, RecordSource, PartNumber, RT, De, Cost, Margin, Sell, Quantity, Location, Type
from (select bidUnitLineItemDef.ItemID uid, bidUnitLineItemDef.BidID tqBidID, BldgID tqBldgID, BidUnitItemQuan.UnitID tqUnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description De, Cost, Margin, Sell, Quantity, Location,BidUnitDef.UnitType tqUnitType,
	Type from bidUnitLineItemDef inner join BidUnitItemQuan on bidUnitLineItemDef.ItemID = BidUnitItemQuan.ItemID and
	bidUnitLineItemDef.BidID = BidUnitItemQuan.BidID inner join BidUnitDef on BidUnitDef.UnitID = BidUnitItemQuan.UnitID and
	BidUnitDef.BidID = BidUnitItemQuan.BidID inner join BldgUnitCount on BldgUnitCount.UnitID = BidUnitItemQuan.UnitID
	group by bidUnitLineItemDef.ItemID, bidUnitLineItemDef.BidID, BldgID, BidUnitItemQuan.UnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description, Cost, Margin, Sell, Quantity, Location, BidUnitDef.UnitType,
	Type)tq inner join #units on tqUnitType = #units.UnitType and tqBidID = #units.BidID where BldgID is null
go

--Prepare lineitem identity for new bid lineitems
declare @NEXTITEMID int
select @NEXTITEMID = MAX(ItemID) + 1000 from LineItem
DBCC CHECKIDENT('LineItem', RESEED,@NEXTITEMID) 
Set identity_insert LineItem on

--The new structure allows bid line items to have a null bldgid
Alter table LineItem Alter Column BldgID int NULL

--Move Bid Line Items over to the standard table
insert into LineItem(ItemId, ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description,
	Cost, Margin, Sell, Quantity, Location, Type)
select icid, projectid, bldgid, unitid, RecordSource, PartNumber, RT, Description, Cost,
	Margin, Sell, Quantity, Location, Type from #Itemconversion

--Restore lineitem identity
Select @NEXTITEMID = MAX(ItemID)+1000 From LineItem
Set identity_insert LineItem off
DBCC CHECKIDENT('LineItem', RESEED,@NEXTITEMID) 

--rename the job comment table to get it ready for generic use
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_Comments]')) begin
	exec sp_rename 'j_Comments', 'Comment'
end
if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'Comment'))begin
	exec sp_rename 'Comment.JobID', 'ProjectID', 'Column'
end

--Restore the relationship for the comments int the bid and job tables before combining them
Update bidComments set BidID = #Project.NewProjectID from #Project inner join bidComments on
#Project.OldProjectID = bidComments.BidID where #Project.ProjectType = 'bid'

Update Comment set ProjectID = #Project.NewProjectID from #Project inner join Comment on
#Project.OldProjectID = Comment.ProjectID where #Project.ProjectType = 'job' 

--combine the comment tables
insert into Comment(ProjectID, EntryDate, Author, Comment)
select BidID, EntryDate, Author, Comment from bidComments

--All of the data has been moved. For tables whose data is not being moved rename any appropriate columns
if exists(select * from sys.columns where Name = N'BidID' and Object_ID = Object_ID(N'Alternates'))begin
	exec sp_rename 'Alternates.BidId', 'ProjectID', 'Column'
end
go
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_ChangeOrder]')) begin
	exec sp_rename 'j_ChangeOrder', 'ChangeOrder'
end

if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'ChangeOrder'))begin
	exec sp_rename 'ChangeOrder.JobID', 'ProjectID', 'Column'
end


if exists (select * from sysobjects where id = object_id(N'[dbo].[b_AddBldg]') AND type in (N'P', N'PC'))
begin
	drop procedure dbo.b_AddBldg
end
if exists (select * from sysobjects where id = object_id(N'[dbo].[j_AddBldg]') AND type in (N'P', N'PC'))
begin
	drop procedure dbo.j_AddBldg
end
if exists (select * from sysobjects where id = object_id(N'[dbo].[AddBldg]') AND type in (N'P', N'PC'))
begin
	drop procedure dbo.AddBldg
end
go
/* frmBid - btnAddBldg_Click */
CREATE PROCEDURE AddBldg
	@ProjectID int,
	@BldgNumber int,
	@BldgName varchar(50)=null,
	@Type nvarchar(50),
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@Notes nvarchar(250),
	@shipdate datetime=NULL,
	@shipsequence int=NULL,
	@Shipped bit = NULL,
	@ShippedR bit = NULL,
	@NewBldgID int OUTPUT
AS INSERT INTO Building
	(ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, Shipdate, shipsequence, Shipped, ShippedR)
VALUES
	(@ProjectID, @BldgNumber, @BldgName, @Type, @Description, @Location, @Address, @Notes, @Shipdate, @shipsequence, @Shipped, @ShippedR);
SELECT @NewBldgID = @@IDENTITY
RETURN @NewBldgID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Buildings]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Buildings
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Buildings]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Buildings
END
GO
/* frmBid - LoadBldgs */
CREATE PROCEDURE Buildings
	@ProjectID int
AS SELECT BldgID, 
	RTRIM(
	CASE WHEN(LEN(ISNULL(RTRIM(CAST(BldgNumber AS CHAR)), '-')) = 1) 
		Then '0' + RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR)), '-')) 
		Else RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-'))
	END) + ' ' + Type + ' ' + Description + ' -  ' + Location AS BLDG
FROM dbo.Building
WHERE (ProjectID = @ProjectID)
ORDER BY Bldg
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_BuildingDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_BuildingDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BuildingDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BuildingDetails
END
GO
CREATE PROCEDURE BuildingDetails
	@BldgID int
AS SELECT Type, Description, Location, Address, Notes, shipdate
FROM dbo.Building
WHERE BldgID = @bldgid
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptBldgTypeCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptBldgTypeCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptBldgTypeCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptBldgTypeCount
END
GO
CREATE PROCEDURE rptBldgTypeCount
	@ProjectID int
AS
SELECT Type, COUNT(BldgNumber) AS BldgCount
FROM dbo.Building
WHERE (ProjectID = @ProjectID) AND (SUBSTRING(dbo.Building.Type, 1, 1) = 'B')
GROUP BY Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptBldgCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptBldgCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptBldgCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptBldgCount
END
GO
CREATE PROCEDURE rptBldgCount
	@ProjectID int
AS
SELECT Count(bldgid) AS Buildings from Building where ProjectID = @ProjectID
AND (SUBSTRING(dbo.Building.Type, 1, 1) = 'B')
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitsPerBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitsPerBuilding
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitsPerBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitsPerBuilding
END
GO
/* frmBid - LoadUnitsPerBldg */
CREATE PROCEDURE UnitsPerBuilding
	@ProjectID int
AS
  SELECT CONVERT(Varchar,BldgNumber) + Type + Building.Description as BldgType , Building.Description des, Unit.UnitType unittype, COUNT(UnitID) As Quantity from Building inner join Unit on Building.BldgID =
	Unit.BldgID where Building.ProjectID = @ProjectID and Unit.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'U'
	Group BY CONVERT(Varchar,BldgNumber) + Type + Building.Description , Building.Description, Unit.UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitsPerAllBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitsPerAllBuilding
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitsPerAllBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitsPerAllBuilding
END
GO
CREATE PROCEDURE UnitsPerAllBuilding
	@ProjectID int
AS
select building.BldgID, Building.Type, Building.BldgNumber, Building.Description, Building.Location,
	Building.Address, unit.UnitType, COUNT(unit.unitid) from Building inner join Unit on
	Building.BldgID = unit.BldgID where Unit.ProjectID = @ProjectID group by Building.BldgID, Building.Type, Building.BldgNumber, Building.Description, Building.Location,
	Building.Address, unit.UnitType 
ORDER BY building.Type, bldgNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptUnitDetailPerBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptUnitDetailPerBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptUnitDetailPerBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptUnitDetailPerBldg
END
GO
CREATE PROCEDURE rptUnitDetailPerBldg
	@ProjectID int
AS
select building.type, building.Description as [Buliding Type], count(unit.unitid) as Quan, unit.unittype,
	unit.Description from building inner join unit on building.BldgID = unit.BldgID
where Building.ProjectID = @projectid and SUBSTRING(building.type,1,1) = 'B'
Group by building.type, building.Description, unit.unittype, unit.Description
Order by building.type, unit.unittype
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptUnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptUnitCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptUnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptUnitCount
END
GO
CREATE PROCEDURE rptUnitCount
	@ProjectID int
AS
SELECT COUNT(*)  AS Units from Unit inner join Building on Unit.BldgID = Building.BldgID
WHERE (Unit.ProjectID = @ProjectID) AND (SUBSTRING(Building.Type, 1, 1) = 'B') AND (SUBSTRING(UnitType, 1, 1) = 'U')
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Margins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Margins
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Margins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Margins
END
GO
CREATE PROCEDURE Margins
	@ProjectID int
AS
select SUBSTRING(Unit.UnitType,1,1) As Type, SUM(Quantity * Cost) as TotalCost, SUM(Quantity * Sell) as TotalSell from Unit 
inner join LineItem on unit.UnitID = LineItem.UnitID where Unit.projectID = @projectID group by SUBSTRING(UnitType,1,1)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_DeleteBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_DeleteBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[DeleteBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.DeleteBldg
END
GO
CREATE PROCEDURE DeleteBldg
	@BldgID int
AS
DELETE Building WHERE BldgID = @BldgID
DELETE BldgUnitCount WHERE BldgID = @BldgID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CTBldgVSUCnt]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CTBldgVSUCnt
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CTBldgVSUCnt]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CTBldgVSUCnt
END
GO
CREATE PROCEDURE CTBldgVSUCnt
	@ProjectID int
AS
select Building.BldgNumber, Building.Description, Building.Type, Count(Unit.UnitID) from Building inner join Unit
on Unit.BldgID = Building.BldgID where Building.ProjectID = @ProjectID
group by Building.BldgNumber, Building.Description, Building.Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CTPartVSUType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CTPartVSUType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CTPartVSUType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CTPartVSUType
END
GO
CREATE PROCEDURE CTPartVSUType
	@ProjectID int
AS
Select PartNumber, Location, Type, UnitType, SUM(Quantity) AS Quan
from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID where LineItem.BldgID is not null
GROUP BY PartNumber, Location, Type, UnitType

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptBOMIntro]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptBOMIntro
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptBOMIntro]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptBOMIntro
END
GO
CREATE PROCEDURE rptBOMIntro
	@BldgID int
AS
SELECT Building.BldgName, Building.BldgNumber, Building.Type, Building.Description, Building.Location, Building.Address, 
	Building.shipdate, Building.shipsequence, Project.ProjectID, Project.ContractDate, Project.Description AS JobDesc, 
    Project.Location AS JobLocation, Clients.Name
FROM Building INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID INNER JOIN
    Clients ON Project.ClientID = Clients.ClientID
WHERE (Building.BldgID = @BldgID)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptBldgCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptBldgCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptBldgCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptBldgCount
END
GO
CREATE PROCEDURE rptBldgCount
	@ProjectID int
AS
	SELECT COUNT(BldgID) AS Buildings FROM Building 
	WHERE ProjectID = @ProjectID  
	AND (SUBSTRING(Type, 1, 1) = 'B')
GO


IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitsBOM]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitsBOM
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_UnitsBOM]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_UnitsBOM
END
GO
CREATE PROCEDURE UnitsBOM
	@BldgID int
AS
SELECT     COUNT(Unit.UnitType) AS [Unit Count], Unit.UnitType, LineItem.Quantity, LineItem.PartNumber, 
                      LineItem.Description, LineItem.Location, LineItem.Sell, LineItem.Quantity * LineItem.Sell AS TotalSell
FROM         Unit INNER JOIN
                      LineItem ON Unit.UnitID = LineItem.UnitID
WHERE     (Unit.BldgID = @BldgID)
GROUP BY Unit.UnitType, LineItem.Quantity, LineItem.PartNumber, LineItem.Description, LineItem.Location, 
                      LineItem.Sell, LineItem.Quantity * LineItem.Sell
ORDER BY Location
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_BldgTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_BldgTypes
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[r_BldgTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.r_BldgTypes
END
GO
CREATE PROCEDURE r_BldgTypes
	@ProjectID int
AS
SELECT Type, Description, COUNT(Type) AS Quan
FROM dbo.Building
WHERE (ProjectID = @ProjectID) AND (SUBSTRING(Type, 1, 1) = 'B')
GROUP BY Type, Description
ORDER BY Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditBldg
END
GO
CREATE PROCEDURE EditBldg
	@BldgID int,
	@BldgName nvarchar(50)=NULL,
	@BldgNumber int=NULL,
	@Type nvarchar(50),
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@Notes nvarchar(250),
	@shipdate datetime=NULL,
	@shipdateR datetime=Null,
	@shipsequence int=NULL,
	@ShippedR bit=NULL,
	@shipped bit=NULL
AS 
UPDATE Building 
	SET
	BldgName = @BldgName,
	BldgNumber = @BldgNumber,
	Type = @Type,
	Description = @Description,
	Location = @Location,
	Address = @Address,
	Notes = @Notes,
	shipdate = @shipdate,
	shipdateR = @shipdateR,
	shipsequence = @shipsequence,
	shipped = @Shipped,
	shippedR = @ShippedR
WHERE BldgID = @bldgID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_UnitsPerBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_UnitsPerBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[r_UnitsPerBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.r_UnitsPerBldg
END
GO
CREATE PROCEDURE r_UnitsPerBldg
	@ProjectID int
AS
SELECT DISTINCT Building.Type, Unit.UnitType, COUNT(Unit.UnitType) AS Quan, 
	Unit.Description, Building.Description AS BldDesc
FROM Building INNER JOIN
	Unit ON Building.BldgID = Unit.BldgID
WHERE (Building.ProjectID = @ProjectID) AND (SUBSTRING(Unit.UnitType, 1, 1) = 'U')
GROUP BY Building.BldgName, Unit.UnitType, Unit.Description, Building.Description, Building.Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_BldgBOM]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_BldgBOM
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BldgBOMSP]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BldgBOMSP
END
GO
CREATE PROCEDURE BldgBOMSP
	@BldgID int
AS
Select Distinct TQ.Quantity Quan, TQ.Pn PartNumber, TQ.De Description, LineItem.Cost, TQ.CO TotalCost, LineItem.Sell, TQ.SE TotalSell From LineItem inner join 
(Select Sum(Quantity) as Quantity, PartNumber pn, Description De, SUM(Cost * Quantity ) as CO, SUM(Sell * Quantity ) as SE from LineItem where BldgID = @BldgID
Group BY PartNumber, Description) TQ on LineItem.PartNumber = TQ.pn where LineItem.BldgID = @BldgID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_UpdateShipping]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_UpdateShipping
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UpdateShipping]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UpdateShipping
END
GO
CREATE PROCEDURE UpdateShipping
	@ProjectID int,
	@DateChange int
AS
UPDATE Building
SET shipdateR = ShipdateR + @DateChange
WHERE ProjectID = @ProjectID and ShippedR = 0

UPDATE Building
SET shipdate = Shipdate + @DateChange
WHERE ProjectID = @ProjectID and Shipped = 0

UpDate LineItem
SET ShipDate = ShipDate + @DateChange
WHERE ProjectID = @ProjectID and Shipped = 0
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BuildingBOM]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BuildingBOM
END
GO
CREATE PROCEDURE BuildingBOM
	@BldgID int
AS
SELECT SUM(Quantity) AS Quan, PartNumber, Description, Cost, Sell, SUM(Quantity) * Round(Cost,2) AS TotalCost, SUM(Quantity) * Round(Sell,2) AS TotalSell
FROM LineItem
WHERE (BldgID = @BldgID)
GROUP BY PartNumber, Description, Cost, Sell
ORDER BY PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_ShipDates]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_ShipDates
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ShipDates]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ShipDates
END
GO
CREATE PROCEDURE ShipDates
	@ProjectID int
AS
SELECT     
	Building.BldgName, Building.BldgNumber, Building.Type, Building.Description, Building.shipdate,
	Building.shipdater, Building.Shipped, Building.Shippedr, SUM(LineItem.Cost * LineItem.Quantity) AS Cost, 
	SUM(LineItem.Sell * LineItem.Quantity) AS Sell
FROM Building INNER JOIN
	LineItem ON Building.BldgID = LineItem.BldgID
WHERE (Building.ProjectID = @ProjectID)
GROUP BY 
	Building.BldgName, Building.BldgNumber, Building.Type, 
	Building.Description, Building.shipdate, shipdater, Building.Shipped, shippedr
ORDER BY Building.shipdate
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_SetShipDateR]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_SetShipDateR
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[SetShipDateR]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.SetShipDateR
END
GO
CREATE PROCEDURE SetShipDateR
          @ProjectID int
AS
Update Building 
	SET ShipDateR = ShipDate - 60,
	ShippedR = 0
	WHERE ProjectID = @ProjectID

Update LineItem
	SET ShipDate = ShipDate - 60,
	Shipped = 0
	WHERE ProjectID = @ProjectID And RT = 'R'
GO


IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_SetBldgShipDateR]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_SetBldgShipDateR
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[SetBldgShipDateR]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.SetBldgShipDateR
END
GO
CREATE PROCEDURE SetBldgShipDateR
	@BldgID int
AS
Update Building 
	SET ShipDateR = ShipDate - 60,
	ShippedR = 0
	WHERE BldgID = @BldgID

Update LineItem
	SET ShipDate = ShipDate - 60,
	Shipped = 0
	WHERE BldgID = @BldgID And RT = 'R'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditTShipped]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditTShipped
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditTShipped]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditTShipped
END
GO
CREATE PROCEDURE EditTShipped
	@BldgID int,
	@shipped bit = 1
AS
	Update Building Set Shipped = @shipped Where bldgid = @BldgID
	Update LineItem Set Shipped = @shipped Where bldgid = @BldgID AND RT = 'T'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditShipped]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditShipped
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditShipped]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditShipped
END
GO
CREATE PROCEDURE EditShipped
	@BldgID int,
	@shipped bit = 1
AS
	Update Building Set Shipped = @shipped,	ShippedR = @Shipped
	Where bldgid = @BldgID
	Update LineItem Set Shipped = @shipped Where bldgid = @BldgID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditShipDate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditShipDate
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditShipDate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditShipDate
END
GO
CREATE PROCEDURE EditShipDate
	@BldgID int,
	@shipdate datetime,
	@shipdateR datetime
AS
	Update Building 
		Set Shipdate = @shipdate,
		ShipdateR = @ShipdateR
		Where bldgid = @BldgID 

	Update LineItem Set Shipdate = @shipdate Where bldgid = @BldgID AND RT = 'T'
	Update LineItem Set Shipdate = @shipdateR Where bldgid = @BldgID AND RT = 'R'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditRShipped]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditRShipped
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditRShipped]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditRShipped
END
GO
CREATE PROCEDURE EditRShipped
	@BldgID int,
	@shipped bit = 1
AS
	Update Building Set ShippedR = @shipped Where bldgid = @BldgID
	Update LineItem Set Shipped = @shipped Where bldgid = @BldgID AND RT = 'R'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BuildingShipByDate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BuildingShipByDate
END
GO
CREATE PROCEDURE BuildingShipByDate
	@StartDate DateTime,
	@EndDate DateTime
AS
SELECT Project.ProjectID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate, LineItem.RT, 
	SUM(LineItem.Sell*LineItem.Quantity) AS Total 
FROM Building INNER JOIN
	LineItem ON Building.BldgID = LineItem.BldgID INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID
WHERE ShipDateR Between @StartDate And @EndDate AND RT = 'R' AND Building.ShippedR = 0
GROUP BY Project.ProjectID, Building.BldgNumber, Building.BldgName, Building.shipdateR,  Building.shipdate,LineItem.RT
UNION ALL
SELECT Project.ProjectID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate, LineItem.RT, 
	SUM(LineItem.Sell*LineItem.Quantity) AS Total 
FROM Building INNER JOIN
	LineItem ON Building.BldgID = LineItem.BldgID INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID
WHERE Building.ShipDate Between @StartDate And @EndDate AND RT = 'T' AND Building.Shipped = 0
GROUP BY Project.ProjectID, Building.BldgNumber, Building.BldgName,  Building.shipdateR, Building.shipdate, LineItem.RT
ORDER BY Building.ShipDate
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatMainLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatMainLineItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatMainLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatMainLineItem
END
GO
/* frmBid - LoadCatMainLineItem, LoadCatBERLineItem */
CREATE PROCEDURE CatMainLineItem
	@ProductID nvarchar(255)
AS
SELECT    
	ProductID, ISNULL(Description, ' ') AS Description,	ISNULL(StandardCost, ' ') AS Cost
FROM  CatalogMain
WHERE ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatSOLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatSOLineItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatSOLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatSOLineItem
END
GO
/* frmBid - LoadCatMainLineItem, LoadCatBERLineItem */
CREATE PROCEDURE CatSOLineItem
	@ProductID nvarchar(255)
AS
SELECT    
	ProductID, ISNULL(Description, ' ') AS Description,	ISNULL(StandardCost, ' ') AS Cost
FROM  CatalogSO
WHERE ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatBidLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatBidLineItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatLineItem
END
GO
/* frmBid - LoadCatMainLineItem */
CREATE PROCEDURE CatLineItem
	@ProductID char(25)
AS
SELECT    
	 PartNumber AS ProductID,
	ISNULL(Description, ' ') AS Description,
	Cost
FROM  LineItem
WHERE
	PartNumber = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_AddJob]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_AddJob
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddProject]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddProject
END
GO
CREATE PROCEDURE AddProject
	@SourceID int = null,
	@ClientID int,
	@LDID int,
	@ProjectDate datetime,
	@ContractDate datetime = null,
	@ContractAmount money = null,
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@City nvarchar(50),
	@State char(2),
	@Zip char(10),
	@ProjectContact nvarchar(50) = null,
	@ClientPM nvarchar (50) = null,
	@ClientMobilePhone char (14) = null,
	@LDPM nvarchar (50) = null,
	@Notes nvarchar(250),
	@LDEstimator nvarchar(50),
	@BluePrintDate datetime,
	@RevDate datetime,
	@SalesTaxRate float,
	@Closed bit = 0,
	@AuxLabel char(25) = null,
	@ProjectType varchar(25),
	@NewProjectID int OUTPUT
 AS
 INSERT INTO 
	Project (SourceID, ClientID, LDID, ContractDate, ProjectDate, ContractAmount,  Description, Location, Address, 
	City, State, Zip, ProjectContact, ClientPM, ClientMobilePhone, LDPM, 
	Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, Closed, AuxLabel, ProjectType)
 VALUES 
	(@SourceID, @ClientID, @LDID, @ContractDate, @ProjectDate, @ContractAmount, @Description, @Location, 
	@Address, @City, @State, @Zip, @ProjectContact, @ClientPM, @ClientMobilePhone, @LDPM,
	@Notes, @LDEstimator, @BluePrintDate, @RevDate, @SalesTaxRate, @Closed, @AuxLabel, @ProjectType) ;

 SELECT @NewProjectID = @@IDENTITY
 RETURN @NewProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_ChangeOrders]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_ChangeOrders
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ChangeOrders]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ChangeOrders
END
GO
CREATE PROCEDURE ChangeOrders
	@ProjectID int
AS
SELECT COID, CODate, EnteredBy, ApprovedBy, CODesc, Misc, PriorBalance, NewBalance
FROM         dbo.ChangeOrder
WHERE     (ProjectID = @ProjectID)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditJob]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditJob
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditProject]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditProject
END
GO
CREATE PROCEDURE EditProject
	@ProjectID int,
	@ContractAmount money = NULL,
	@Description nvarchar(50)= NULL,
	@Location nvarchar(50)= NULL,
	@Address nvarchar(50)= NULL,
	@City nvarchar(50)= NULL,
	@State char(2)= NULL,
	@Zip char(10)= NULL,
	@ProjectContact nvarchar(50)= NULL,
	@ClientPM nvarchar(50)= NULL,
	@ClientMobilePhone char(14)=NULL,
	@LDPM nvarchar(50)=NULL,
	@Notes nvarchar(250)= NULL,
	@LDEstimator nvarchar(50)= NULL,
	@BluePrintDate datetime= NULL,
	@RevDate datetime= NULL,
	@SalesTaxRate float= NULL
AS
UPDATE Project
SET
	ContractAmount = @ContractAmount,
	Description = @Description, 
	Location = @Location, 
	Address = @Address, 
	City = @City, 
	State = @State,
	Zip = @Zip,
	ClientPM = @ClientPM,
	ClientMobilePhone = @ClientMobilePhone,
	LDPM = @LDPM,
	Notes = @Notes,
	LDEstimator = @LDEstimator,
	BluePrintDate = @BluePrintDate,
	RevDate = @RevDate,
	SalesTaxRate = @SalesTaxRate,
	ProjectContact = @ProjectContact
WHERE
	ProjectID = @ProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditChangeOrder]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditChangeOrder
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditChangeOrder]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditChangeOrder
END
GO
CREATE PROCEDURE EditChangeOrder
	@COID int,
	@CODate datetime,
	@EnteredBy char(10),
	@ApprovedBy char(50),
	@CODesc nvarchar(1000),
	@Misc nvarchar(1000),
	@PriorBalance money,
	@NewBalance money
AS
Update  ChangeOrder SET	
	CODate = @CODate,
	EnteredBy = @EnteredBy,
	ApprovedBy = @ApprovedBy,
	CoDesc = @CODesc,
	Misc = @Misc,
	PriorBalance = @PriorBalance,
	NewBalance = @NewBalance
WHERE COID = @COID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_main]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_main
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[r_main]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.r_main
END
GO
CREATE PROCEDURE r_main
	@ProjectID int
AS
SELECT Project.ProjectID, Project.LDID, Project.SourceID, Project.ContractDate, Project.ProjectDate, Project.Description, Project.Location, Project.Address, 
	Project.City, Project.State, Project.Zip, Project.ProjectContact, Project.ClientPM, Project.ClientMobilePhone, Project.LDPM, 
    Project.Notes, Project.LDEstimator, Project.BluePrintDate, Project.RevDate, Project.SalesTaxRate, Project.ContractAmount, 
    Project.ContractBalance, Clients.Name, Clients.Address1, Clients.Address2, Clients.city AS cCity, Clients.State AS cState, 
    Clients.Zip AS cZip, Clients.Phone1, Clients.Phone2, Clients.FAX, Clients.email, Clients.contact, 
    Clients.taxexempt
FROM Clients INNER JOIN
	Project ON Clients.ClientID = Project.ClientID
WHERE (Project.ProjectID = @ProjectID)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptSalesTaxRate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptSalesTaxRate
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptSalesTaxRate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptSalesTaxRate
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptSalesTaxRate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptSalesTaxRate
END
GO
CREATE PROCEDURE rptSalesTaxRate
	@ProjectID int
AS
	SELECT SalesTaxRate FROM Project WHERE ProjectID = @ProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptJobIntro]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptJobIntro
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptJobIntro]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptBidIntro
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptIntro]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptIntro
END
GO
CREATE PROCEDURE rptIntro
	@ProjectID int
AS
SELECT Project.ProjectID, Project.LDID, Project.ContractDate, Project.Description, Project.Location, Project.Address, Project.City, Project.State, Project.Zip, 
	Project.ProjectContact, Project.ClientPM, Project.ClientMobilePhone, Project.LDPM, Project.LDEstimator, Project.BluePrintDate, 
	Project.RevDate, Project.SalesTaxRate, Project.Closed, Project.Notes, Clients.Name, Clients.Address1, Clients.Address2, 
	Clients.city AS CCity, Clients.State AS CState, Clients.Zip AS CZip, Clients.Phone1, Clients.Phone2, Clients.FAX, 
	Clients.email, Clients.contact, Clients.taxexempt, Project.ProjectDate, Project.ProjectContact AS Contact
FROM Project INNER JOIN
	Clients ON Project.ClientID = Clients.ClientID
WHERE (Project.ProjectID = @ProjectID)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_Job]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_Job
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_Bids]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_Bids
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ProjectList]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ProjectList
END
Go
CREATE PROCEDURE ProjectList
	@ProjectType varchar(25)
AS
SELECT ProjectID,'LD-'+ RTRIM(CAST(LDID AS char))+ ': '+ISNULL(Description, '')+' '+ISNull(Location, '') AS Proj
FROM Project
Where ProjectType = @ProjectType

UNION ALL
SELECT 0 AS ProjectID, ' - Please Select A '+ @ProjectType +' - ' AS Proj

ORDER BY Proj
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddBidUnitLIneItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddBidUnitLIneItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddUnitLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddUnitLineItem
END
Go
CREATE PROCEDURE AddUnitLineItem 
	@RecordSource char(15),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money
AS 
INSERT INTO LineItem
	(RecordSource, PartNumber, RT, Description,
	 Cost, Margin, Sell)
VALUES
	(@RecordSource, @PartNumber, @RT, @Description,
	 @Cost, @Margin, @Sell)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddBidComment]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddBidComment
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddBidComment]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_AddComment
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddComment]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddComment
END
GO
/*frmBid - AddNewBid*/
CREATE PROCEDURE AddComment
	@ProjectID int,
	@EntryDate datetime,
	@Author nvarchar(30),
	@Comment nvarchar(1000)
 AS
	
INSERT INTO 
	Comment (ProjectID, EntryDate, Author, Comment)
VALUES 
	(@ProjectID, @EntryDate, @Author, @Comment)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Alternate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Alternate
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Alternate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Alternate
END
GO
CREATE PROCEDURE Alternate
	@ProjectID int,
	@OrgProductID nvarchar(255),
	@OrgDescription nvarchar(255),
	@OrgCost money,
	@OrgMargin money,
	@OrgSell money,
	@AltProductID nvarchar(255),
	@AltDescription nvarchar(255),
	@AltCost money,
	@AltMargin money,
	@AltSell money,
	@AltRecordSource char(10)
AS
INSERT INTO Alternates
	(ProjectID, OrgProductID, OrgDescription, OrgCost, OrgMargin, OrgSell,
	AltProductID, AltDescription, AltCost, AltMargin, AltSell, AltRecordSource)
VALUES
	(@ProjectID, @OrgProductID, @OrgDescription, @OrgCost, @OrgMargin, @OrgSell,
	@AltProductID, @AltDescription, @AltCost, @AltMargin, @AltSell, @AltRecordsource)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AdjustMargins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AdjustMargins
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AdjustMargins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AdjustMargins
END
GO
CREATE PROCEDURE AdjustMargins
	@ProjectID int,
	@Delta int
AS 
Update LineItem
SET Sell = (Cost/(Cast((100-(Margin + @Delta)) AS float)/100)),
	Margin = Margin + @Delta
WHERE ProjectID = @ProjectID

Update Alternates
SET  
 	OrgSell = (orgCost/(Cast((100-(orgMargin + @Delta)) AS float)/100)),
	OrgMargin = OrgMargin + @Delta,
	AltSell = (AltCost/(Cast((100-(AltMargin + @Delta)) AS float)/100)),
	AltMargin = AltMargin + @Delta
WHERE ProjectID = @ProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_BELIbyType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_BELIbyType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ELIbyType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ELIbyType
END
Go
CREATE PROCEDURE ELIbyType
	@ProjectID int,
	@Type nvarchar (10)
AS
SELECT DISTINCT Type, PartNumber, Description, RT, Cost, Margin, Sell
FROM LineItem
WHERE   
	ProjectID = @ProjectID AND Type = @Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_BELIbyPart]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_BELIbyPart
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_BELIbyPart]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_BELIbyPart
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BELIbyPart]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BELIbyPart
END
Go
/* frmBid - LoadBELIByPart */
CREATE PROCEDURE BELIbyPart
	@ProjectID int,
	@PartNumber char (25)
AS
SELECT DISTINCT PartNumber, Description, RT, Cost, Margin, Sell
FROM LineItem
WHERE ProjectID = @ProjectID AND PartNumber = @PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatBidItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatBidItems
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_CatJobItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_CatJobItems
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatItems
END
GO
/* frmBid - LoadBEBidItemCBO, LoadBidItemsCBO */
CREATE PROCEDURE CatItems
	@ProjectID int
AS
SELECT Distinct PartNumber AS ProductID, 
	ISNULL(RTRIM(PartNumber), ' ') + ' - ' + ISNULL(RTRIM(Description), ' ') AS Item
FROM LineItem
WHERE ProjectID = @ProjectID
ORDER BY PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatalogItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatalogItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatalogItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatalogItem
END
GO
CREATE PROCEDURE CatalogItem 
AS
SELECT ProductID, RTRIM(ProductID) + '  - ' + ISNULL(Description, ' ') AS Item
FROM CatalogMain
ORDER BY ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Bulbs]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Bulbs
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BulbList]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BulbList
END
GO
CREATE PROCEDURE BulbList
	@PartNumber char(255)	
AS
SELECT CatalogMain.ProductID, CatalogMain.ProductID + ':' + CatalogMain.Description AS Bulb
FROM Bulb2Fixture INNER JOIN
     CatalogMain ON Bulb2Fixture.BulbNum = CatalogMain.ProductID
WHERE Bulb2Fixture.PartNumber = @PartNumber
GO


IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_BulbDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_BulbDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BulbDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BulbDetails
END
GO
CREATE PROCEDURE BulbDetails
	@PartNumber char (255),
	@BulbID char (255)
AS
SELECT Bulb2Fixture.Quan, CatalogMain.StandardCost, CatalogMain.Description
FROM Bulb2Fixture INNER JOIN
	CatalogMain ON Bulb2Fixture.BulbNum = CatalogMain.ProductID
WHERE Bulb2Fixture.BulbNum = @BulbID 
	AND Bulb2Fixture.PartNumber = @PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_ClientDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_ClientDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ClientDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ClientDetails
END
GO
CREATE PROCEDURE ClientDetails 
	@ClientID int
AS
SELECT    
	Name, 
	ISNULL(Address1, ' ') AS Address1,
	ISNULL(Address2, ' ') AS Address2, 
	ISNULL(city, ' ') AS City, 
	ISNULL(State, ' ') AS State,
	ISNULL(Zip, ' ') AS Zip, 
	ISNULL(Phone1, ' ') AS Phone1,
	ISNULL(Phone2, ' ') AS Phone2, 
	ISNULL(FAX, ' ') AS FAX,
	ISNULL(email, ' ') AS email , 
	ISNULL(contact, ' ') AS Contact
FROM Clients
WHERE clientid = @ClientID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Client]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Client
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Client]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Client
END
GO
CREATE PROCEDURE Client 
AS
SELECT ClientID, Name + ' - ' + ISNULL(city, ' ') + '  ' + ISNULL(State, ' ') AS Client
FROM Clients
ORDER BY Client
GO

--Hoping to be able to delete this
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_ClearTemp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_ClearTemp
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatSOItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatSOItems
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatSOItemsSP]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatSOItemsSP
END
GO
CREATE PROCEDURE CatSOItemsSP
	@ProductID nvarchar (255)
AS
SELECT Description, VendorCode, VendorPartNO, AlternateVendor,  AlternatePartNum,
	AverageCost, StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits,
	LeadTime, Notes
FROM CatalogSO
WHERE ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatSOItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatSOItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatSOItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatSOItem
END
GO
/* frmBid - LoadCatalogMainCBO, LoadBECatalogCBO */
CREATE PROCEDURE CatSOItem AS
SELECT ProductID, RTRIM(ProductID) + '  - ' + ISNULL(Description, ' ') AS Item
FROM CatalogSO
ORDER BY ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatMainItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatMainItems
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatMainItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatMainItems
END
GO
/* frmBid - LoadCatalogMainCBO, LoadBECatalogCBO */
CREATE PROCEDURE CatMainItems AS
SELECT ProductID, 
	ISNULL(RTRIM(ProductID), ' ') + ' - ' + ISNULL(RTRIM(Description), ' ') AS Item
FROM CatalogMain
ORDER BY ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatMainLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatMainLineItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatMainLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatMainLineItem
END
GO
CREATE PROCEDURE CatMainLineItem
@ProductID nvarchar(255)
AS
SELECT    
	 ProductID, 
	ISNULL(Description, ' ') AS Description,
	ISNULL(StandardCost, ' ') AS Cost
FROM  .CatalogMain
WHERE
	ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CatMainItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CatMainItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CatMainItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CatMainItem
END
GO
CREATE PROCEDURE CatMainItem
	@ProductID nvarchar (255)
AS
SELECT Description, VendorCode, VendorPartNO, 
	AlternateVendor,  AlternatePartNum,
	StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, LeadTime
FROM CatalogMain
WHERE ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddUnitDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddUnitDef
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_AddUnit]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_AddUnit
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddUnit]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddUnit
END
GO
/* frmBid - AddNewUnit, frmCopyUnit */
CREATE PROCEDURE AddUnit
	@ProjectID int,
	@BldgID int = null,
	@UnitName nvarchar(50) = null,
	@UnitType char(10),
	@Description nvarchar(50),
	@NewUnitID int OUTPUT
AS
INSERT INTO Unit
	(ProjectID, BldgId, UnitName, UnitType, Description)
VALUES
	(@ProjectID, @BldgID, @UnitName, @UnitType, @Description);
SELECT @NewUnitID = @@IDENTITY
RETURN @NewUnitID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CopyUnitLineItemDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CopyUnitLineItemDef
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CopyLineItemDefinition]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CopyLineItemDefinition
END
GO
CREATE PROCEDURE CopyLineItemDefinition
    @ProjectID int,
	@NewUnitID int,
	@OldUnitID int
AS
insert into LineItem (ProjectID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell)
Select @ProjectID, @NewUnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell From LineItem Where
UnitID = @OldUnitID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddCatalogSO
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddCatalogSO
END
GO
/* frmBid - AddToSO */
CREATE PROCEDURE AddCatalogSO
	@ProdID nvarchar (255),
	@Desc	nvarchar (255),
	@Cost	float
AS
INSERT INTO CatalogSO (ProductID, Description, StandardCost)
VALUES (@ProdID, @Desc, @Cost)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CopyUnitDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CopyUnitDef
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CopyUnitDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CopyUnitDef
END
GO
CREATE PROCEDURE CopyUnitDef
	@NewProjectID int,
	@OldProjectID int
AS
INSERT INTO Unit (ProjectID, UnitType, Description)
SELECT @NewProjectID, UnitType, Description FROM Unit WHERE ProjectID = @OldProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CopyLineItemDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CopyLineItemDef
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CopyLineItemDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CopyLineItemDef
END
GO
CREATE PROCEDURE CopyLineItemDef 
	@ProjectID int,
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@margin int,
	@Sell money,
	@NewItemID int OUTPUT
AS
INSERT INTO LineItem (ProjectID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell)
VALUES (@ProjectID, @RecordSource, @PartNumber, @RT, @Description, @Cost, @Margin, @Sell);
SELECT @NewItemID = @@IDENTITY
RETURN @NewItemID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_DeleteCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_DeleteCatalogSO
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[DeleteCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.DeleteCatalogSO
END
GO
CREATE PROCEDURE DeleteCatalogSO
	@ProductID nvarchar (255)
AS
DELETE FROM CatalogSO WHERE
	ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_DeleteCatalogMain]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_DeleteCatalogMain
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[DeleteCatalogMain]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.DeleteCatalogMain
END
GO
CREATE PROCEDURE DeleteCatalogMain
	@ProductID nvarchar (255)
AS
DELETE FROM CatalogMain WHERE
ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_EditCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_EditCatalogSO
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditCatalogSO
END
GO
CREATE PROCEDURE EditCatalogSO
	@ProductID nvarchar (255),
	@Description	nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@AverageCost float,
	@StandardCost	float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255),
	@Notes nvarchar(1000)
AS
UPDATE CatalogSO SET
	Description = @Description,
	VendorCode = @VendorCode,
	VendorPartNo = @VendorPartNo,
	AlternateVendor = @AlternateVendor,
	AlternatePartNum = @AlternatePartNum,
	AverageCost = @AverageCost,
	StandardCost = @StandardCost,
	AvailableUnits = @AvailableUnits,
	OnOrderUnits = @OnOrderUnits,
	CommittedUnits = @CommittedUnits,
	LeadTime = @LeadTime,
	Notes = @notes
WHERE ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_EditCatalogMain]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_EditCatalogMain
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditCatalogMain]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditCatalogMain
END
GO
CREATE PROCEDURE EditCatalogMain
	@ProductID nvarchar (255),
	@Description nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@StandardCost float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255)
AS
UPDATE CatalogMain SET
	Description = @Description,
	VendorCode = @VendorCode,
	VendorPartNo = @VendorPartNo,
	AlternateVendor = @AlternateVendor,
	AlternatePartNum = @AlternatePartNum,
	StandardCost = @StandardCost,
	AvailableUnits = @AvailableUnits,
	OnOrderUnits = @OnOrderUnits,
	CommittedUnits = @CommittedUnits,
	LeadTime = @LeadTime
WHERE ProductID = @ProductID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_NewCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_NewCatalogSO
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[NewCatalogSO]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.NewCatalogSO
END
GO
CREATE PROCEDURE NewCatalogSO
	@ProductID nvarchar (255),
	@Description	nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@AverageCost float,
	@StandardCost	float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255),
	@Notes nvarchar(1000)
 AS
INSERT INTO CatalogSO
	(ProductID, Description, VendorCode, VendorPartNo, AlternateVendor, AlternatePartNum, 
	 AverageCost, StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, Leadtime, Notes)
VALUES
	(@ProductID, @Description, @VendorCode, @VendorPartNo, @AlternateVendor, @AlternatePartNum, 
	 @AverageCost, @StandardCost, @AvailableUnits, @OnOrderUnits, @CommittedUnits, @Leadtime, @Notes)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_NewCatalogMain]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_NewCatalogMain
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[NewCatalogMain]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.NewCatalogMain
END
GO
CREATE PROCEDURE NewCatalogMain
	@ProductID nvarchar (255),
	@Description	nvarchar (255),
	@VendorCode nvarchar(255),
	@VendorPartNo nvarchar(255),
	@AlternateVendor nvarchar(255),
	@AlternatePartNum nvarchar(255),
	@StandardCost	float,
	@AvailableUnits float,
	@OnOrderUnits float,
	@CommittedUnits float,
	@LeadTime nvarchar(255)
 AS
INSERT INTO CatalogMain
	(ProductID, Description, VendorCode, VendorPartNo, AlternateVendor, AlternatePartNum, 
	 StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, Leadtime)
VALUES
	(@ProductID, @Description, @VendorCode, @VendorPartNo, @AlternateVendor, @AlternatePartNum, 
	 @StandardCost, @AvailableUnits, @OnOrderUnits, @CommittedUnits, @Leadtime)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptBidComments]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptBidComments
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptBidComments]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptJobComments
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptComments]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptComments
END
GO
CREATE PROCEDURE rptComments
	@ProjectID int
AS
SELECT EntryDate, Author, Comment 
FROM Comment
WHERE ProjectID = @ProjectID
ORDER BY EntryDate
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UpdatePrice]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UpdatePrice
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_UpdatePrice]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_UpdatePrice
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UpdatePrice]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UpdatePrice
END
GO
/* frmBid - btnAdjustSell_Click */
CREATE PROCEDURE UpdatePrice
	@ProjectID int,
	@PartNumber char (25),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@RT char (1)
AS
UPDATE LineItem
SET Cost = @Cost,
	Margin = @Margin,
	Sell = @Sell,
	Description = @Description,
	RT = @RT
WHERE 
	ProjectID = @ProjectID AND
	PartNumber = @Partnumber AND
	shipped = 0
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UpdateLineItemByPartNum]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UpdateLineItemByPartNum
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UpdateLineItemByPartNum]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UpdateLineItemByPartNum
END
GO
/* frmBid - btnBERByPartNumber_Click */
CREATE PROCEDURE UpdateLineItemByPartNum
	@ProjectID int,
	@OrigPartNumber char(25),
	@RecordSource char (10),
	@PartNumber char (25),
	@RT char (1),
	@Description nvarchar (50),
	@Cost money,
	@Margin int,
	@Sell Money
AS
DECLARE @ItemID int

SELECT @ItemID = (Select ItemID FROM LineItem 
WHERE ProjectID = @ProjectID AND
	PartNumber = @OrigPartNumber)
UPDATE LineItem SET
	RecordSource = @RecordSource,
	PartNumber = @PartNumber,
	RT = @RT,
	Description = @Description,
	Cost = @Cost,
	Margin = @Margin,
	Sell = @Sell
WHERE
	ItemID = @ItemID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UpdateBidItemDesc]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UpdateBidItemDesc
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UpdateProjectItemDesc]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UpdateProjectItemDesc
END
GO
CREATE PROCEDURE UpdateProjectItemDesc
	@ProjectID int
AS
UPDATE LineItem
SET LineItem.Description = catalogmain.Description
FROM LineItem INNER JOIN
	CatalogMain ON LineItem.PartNumber = CatalogMain.ProductID 
WHERE LineItem.ProjectID = @ProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UpdateBidCost]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UpdateBidCost
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UpdateProjectCost]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UpdateProjectCost
END
GO
CREATE PROCEDURE UpdateProjectCost
	@ProjectID int
AS
UPDATE LineItem
SET Cost = CatalogMain.standardcost, 
	Sell = (CatalogMain.standardcost/(Cast((100-Margin) AS float)/100))
FROM LineItem INNER JOIN
	CatalogMain ON LineItem.PartNumber = CatalogMain.ProductID 
WHERE LineItem.ProjectID = @ProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitLineItemID]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitLineItemID
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitLineItemID]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitLineItemID
END
GO
CREATE PROCEDURE UnitLineItemID
	@ProjectID int,
	@PartNum char (25)
AS
SELECT ItemID FROM LineItem
WHERE ProjectID = @ProjectID AND PartNumber = @PartNum
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Units]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Units
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Units]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Units
END
GO
/* frmBid - LoadUnits, LoadBldgUnits */
CREATE PROCEDURE Units
	@ProjectID int
AS
Select Distinct UnitType, UnitType + ': ' + Description As U
From Unit
Where ProjectID = @ProjectID
ORDER BY U
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_AddChangeOrder]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_AddChangeOrder
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddChangeOrder]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddChangeOrder
END
GO
CREATE PROCEDURE AddChangeOrder
	@ProjectID int,
	@CODate datetime,
	@EnteredBy char(10),
	@ApprovedBy char(50),
	@CODesc nvarchar(1000),
	@Misc nvarchar(1000),
	@PriorBalance money,
	@NewBalance money
AS
INSERT INTO ChangeOrder (ProjectID, CODate, EnteredBy, ApprovedBy, CODesc, Misc, PriorBalance, NewBalance)
VALUES (@ProjectID, @CODate, @EnteredBy, @ApprovedBy, @CODesc, @Misc, @PriorBalance, @NewBalance)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitRptDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitRptDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitRptDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitRptDetail
END
GO
CREATE PROCEDURE UnitRptDetail 
	@ProjectID int
AS
SELECT Unit.UnitType, Unit.Description, LineItem.PartNumber, LineItem.RT, 
	LineItem.Description AS [Item Description], LineItem.Sell, LineItem.Quantity, 
	LineItem.Location, LineItem.Type
From Unit INNER JOIN
	LineItem ON Unit.UnitID = LineItem.UnitID
Where Unit.ProjectID = @ProjectID
ORDER BY Unit.UnitType, LineItem.PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitLineItemDG]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitLineItemDG
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitLineItemDG]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE UnitLineItemDG
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ProjectUnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ProjectUnitCount
END
GO
/* frmBid - LoadUnitCount */
CREATE PROCEDURE ProjectUnitCount
	@ProjectID int
AS
SELECT UnitType, Description, Count(UnitID) AS QUAN
FROM Unit
WHERE ProjectID = @ProjectID AND
	LEFT(UnitType, 1) = 'U' And BldgID is not null
GROUP BY UnitType, Description
ORDER BY UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_TypeSummaryNoType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_TypeSummaryNoType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_TypeSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_TypeSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_TypeSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_TypeSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[TypeSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.TypeSummary
END
GO
CREATE PROCEDURE TypeSummary
	@ProjectID int
AS
Select type, PartNumber, Description, sum(Quantity) AS quan, Cost, Sell
	from LineItem
	where ProjectID = @ProjectID AND Type <> '' And BldgID is not null
	group by PartNumber, Type, Description, Cost, Margin, Sell
go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_TypeSummaryNoType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_TypeSummaryNoType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[TypeSummaryNoType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.TypeSummaryNoType
END
go
CREATE PROCEDURE TypeSummaryNoType
	@ProjectID int
AS
Select type, PartNumber, Description, sum(Quantity) AS quan, Cost, Sell
	from LineItem
	where ProjectID = @ProjectID AND Type = '' And BldgID is not null
	group by PartNumber, Type, Description, Cost, Margin, Sell

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitSummaryL]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitSummaryL
END
go
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitSummaryLSP]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitSummaryLSP
END
GO
CREATE PROCEDURE UnitSummaryLSP
	@ProjectID int,
	@OrderBy varchar(15)
AS
Select pn PartNumber, lo Location, ty [Type], descr Description, Quantity, co Cost, se Sell, (Quantity * co) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'U'
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitSummaryExp
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitSummaryExp
END
GO
CREATE PROCEDURE UnitSummaryExp
	@ProjectID int,
	@OrderBy varchar(15) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (Quantity * se) as Total
from(
	select LineItem.Description Descr, LineItem.PartNumber pn, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty, LineItem.Location lo
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'U' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Sell
) tempTable
ORDER BY(
	CASE 
	WHEN @OrderBy = 'PN' THEN pn
	WHEN @OrderBy = 'T' THEN Ty	
	WHEN @OrderBy = 'L'  THEN lo
	END
)
go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_SiteSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_SiteSummaryExp
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[SiteSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.SiteSummaryExp
END
GO
CREATE PROCEDURE SiteSummaryExp
	@ProjectID int,
	@OrderBy varchar(15) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (Quantity * se) as Total
from(
	select LineItem.Description Descr, LineItem.PartNumber pn, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty, LineItem.Location lo
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'S' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Sell
) tempTable
ORDER BY(
	CASE 
	WHEN @OrderBy = 'PN' THEN pn
	WHEN @OrderBy = 'T' THEN Ty	
	WHEN @OrderBy = 'L'  THEN lo
	END
)
go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_SiteSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_SiteSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_SiteSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_SiteSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_UnitSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_UnitSummary
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitSummary
END
GO
CREATE PROCEDURE UnitSummary
	@ProjectID int,
	@OrderBy varchar(15),
	@UnitType varchar(15)
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (Quantity * co) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = @UnitType
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order by Type

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitsPerBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitsPerBuilding
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitsPerBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitsPerBuilding
END
go
/* frmBid - LoadUnitsPerBldg */
CREATE PROCEDURE UnitsPerBuilding
	@ProjectID int
 AS
select Building.BldgID blID, RTRIM(Case When BldgNumber < 10 then RIGHT('0'+ CONVERT(VARCHAR,BldgNumber),2) else CAST(BldgNumber AS CHAR) end) 
	 + ' ' + Building.Type + ' ' + Building.Description + ' -  ' 
	+ Building.Location AS Type, Building.Description, Building.Location, Building.Address, sq.ut, sq.cnt 
from Building inner join 
	(select UnitType ut, BldgID, count(*) cnt from Unit where left (UnitType,1 ) = 'U' group by BldgID, UnitType) sq 
	on Building.BldgID = sq.BldgID
where (Building.ProjectID = @ProjectID) and (Left(sq.ut,1) = 'U')
Order by blID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UnitsPerAllBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UnitsPerAllBuilding
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitsPerAllBuilding]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitsPerAllBuilding
END
GO
/* frmBid - LoadUnitsPerBldg */
CREATE PROCEDURE UnitsPerAllBuilding
	@ProjectID int
 AS
select Building.BldgID bldgID, Building.Type, Building.BldgNumber, Building.Description, Building.Location, Building.Address, sq.ut UnitType, sq.cnt Quan
from Building inner join 
	(select UnitType ut, BldgID, count(*) cnt from Unit where left (UnitType,1 ) = 'U' group by BldgID, UnitType) sq 
	on Building.BldgID = sq.BldgID
where (Building.ProjectID = @ProjectID)
Order by Type, bldgNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptUnitDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptUnitDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptCommonDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptCommonDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_CommonDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_CommonDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptSiteDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptSiteDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_SiteDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_SiteDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptClubHouseDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptClubHouseDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_ClubHouseDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_ClubHouseDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptAuxDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptAuxDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_AuxDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_AuxDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptQuantityDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptQuantityDetails
END
GO
CREATE PROCEDURE rptQuantityDetails
	@ProjectID int,
	@Filter Char
AS
select UnitType, Unit.Description AS UnitDesc, sum(Quantity) as Quantity, PartNumber, LineItem.Description Description, Location,
RT, Type, Cost, Margin, Sell
from LineItem inner join Unit
on LineItem.UnitID = Unit.UnitID 
where LineItem.ProjectID = @ProjectID 
and ((@Filter is null) OR (LEFT(UnitType, 1) = @Filter)) and Unit.BldgID is not null
Group by UnitType, Unit.Description, PartNumber, LineItem.Description, Location, Type, RT, Cost, Margin, Sell
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_ClubHouseDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_ClubHouseDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_UnitDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_UnitDetail
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitDetail]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitDetail
END
go
CREATE PROCEDURE UnitDetail
	@ProjectID int,
	@UnitType char
AS
SELECT DISTINCT  Unit.UnitType, Unit.Description, LineItem.PartNumber, LineItem.Description AS Expr1, 
                      LineItem.Location, LineItem.RT, LineItem.Type, LineItem.Quantity, LineItem.Sell, LineItem.Cost
FROM         Unit INNER JOIN
                      LineItem ON Unit.UnitID = LineItem.UnitID
WHERE     (Unit.ProjectID = @ProjectID) AND (SUBSTRING(Unit.UnitType, 1, 1) = @UnitType)
ORDER BY Location,  LineItem.Type
GO


IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptUnitDetailPerBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptUnitDetailPerBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptUnitDetailPerBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptUnitDetailPerBldg
END
go
CREATE PROCEDURE rptUnitDetailPerBldg
	@ProjectID int
AS
select Building.Type, Building.Description AS [Building Type], count(UnitType) as Quan, Unit.UnitType, Unit.Description Description
from Building inner join Unit 
on Building.BldgID = Unit.BldgID 
where Building.ProjectID =@ProjectID and LEFT(Building.Type, 1) = 'B'
group by Building.Type, Building.Description, UnitType, Unit.Description
order by Type, UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptUnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptUnitCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptUnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptUnitCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptUnitCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptUnitCount
END
go
CREATE PROCEDURE rptUnitCount
	@ProjectID int
AS
	SELECT COUNT(UNITID) AS Units FROM Unit WHERE ProjectID = @ProjectID  
	AND (SUBSTRING(UnitType, 1, 1) = 'U') and BldgID is not null
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptTypeDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptTypeDef
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptTypeDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptTypeDef
END
go
CREATE PROCEDURE rptTypeDef
	@ProjectID int
AS
select Distinct Type, PartNumber, Description, Cost, Margin, Sell
from LineItem where ProjectID = @ProjectID and TYPE<>''
order by type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_UpdateLineItemByType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_UpdateLineItemByType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UpdateLineItemByType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UpdateLineItemByType
END
go
/* frmBid -btnBERByType_Click */
CREATE PROCEDURE UpdateLineItemByType
	@Type nvarchar (10),
	@ProjectID int,
	@RecordSource char (10),
	@PartNumber char (25),
	@RT char (1),
	@Description nvarchar (50),
	@Cost money,
	@Margin int,
	@Sell money,
	@OldPartNumber char (25)
AS
Update LineItem Set RecordSource = @RecordSource, PartNumber = @PartNumber, RT = @RT, Description = @Description, Cost = @Cost,
	Margin = @Margin, Sell = @Sell where ProjectID = @ProjectID and PartNumber = @OldPartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CopyUnitItemQuan]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CopyUnitItemQuan
END
Go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_PartCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_PartCount
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[PartCount]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.PartCount
END
go
CREATE PROCEDURE PartCount
	@ProjectID int,
	@ProjectType varchar(15),
	@PartNumber CHAR(25)
AS

select distinct PartNumber, sum(quantity) Quantity from LineItem where ProjectID = @ProjectID and PartNumber = @PartNumber and 
	ProjectID in (Select ProjectID from Project where ProjectType= @ProjectType) group by PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_Margins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_Margins
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_Margins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_Margins
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Margins]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Margins
END
go
CREATE PROCEDURE Margins
	@ProjectID int
AS
select UnitType as Type, SUM(cost) as TotalCost, SUM(sell) as TotalSell from(
select LEFT(UnitType,1) as UnitType, sum (se) as sell, sum(co) as Cost from (
select UnitType, sum(sell * Quantity) as se, sum(cost * Quantity) as co from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID where LineItem.ProjectID = @ProjectID 
and LineItem.BldgID is not null
group by UnitType) tq
group by UnitType) tq2
group by UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_LineItemTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_LineItemTypes
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_LineItemTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_LineItemTypes
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[LineItemTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.LineItemTypes
END
GO
/* frmBid - LoadBETypeCBO */
CREATE PROCEDURE LineItemTypes
	@ProjectID int
AS
select distinct type, Type + ' - ' + Partnumber As Item 
from LineItem
where ProjectID = @ProjectID and type is not null and type <> ''
Order BY Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[LineItemTBD]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.LineItemTBD
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_LineItemTBD]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_LineItemTBD
END
GO
CREATE PROCEDURE LineItemTBD
	@ProjectID int
AS
Select pn PartNumber, descr Description, RT, Quantity, co Cost, se Sell, (Quantity * co) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select Description Descr, PartNumber pn, Cost co, Margin ma, Sell se, sum(Quantity) AS Quantity, RT
	from LineItem
	where ProjectID = @ProjectID AND Type <> ''
	group by RT, PartNumber , Description, Cost, Margin, Sell
) tempTable
order by RT, PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_LineItemSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_LineItemSummaryExp
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[LineItemSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.LineItemSummaryExp
END
GO
CREATE PROCEDURE LineItemSummaryExp
	@ProjectID int
 AS
Select pn PartNumber, descr Description, Quantity, co Cost, se Price, (Quantity * se) as [Total Sell]
from(
	select Description Descr, PartNumber pn, Cost co, Sell se, sum(Quantity) AS Quantity
	from LineItem
	where ProjectID = @ProjectID and BldgID is not null 
	group by PartNumber , Description, Cost, Sell
) tempTable
order by PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_LineItemSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_LineItemSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[LineItemSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.LineItemSummary
END

GO
CREATE PROCEDURE LineItemSummary
	@ProjectID int
 AS
Select pn PartNumber, descr Description, Quantity, co Cost, se Sell, (Quantity * Co) as [Total Cost],
(Quantity * Se) as [Total Sell], (Quantity * Se) - (Quantity * Co) as Net
from(
	select Description Descr, PartNumber pn, Cost co, Margin ma, Sell se, sum(Quantity) AS Quantity, RT
	from LineItem
	where ProjectID = @ProjectID and BldgID is not null
	group by RT, PartNumber , Description, Cost, Margin, Sell
) tempTable
order by RT, PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_DeleteOrphanLIDef]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_DeleteOrphanLIDef
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_DeleteLIByType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_DeleteLIByType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[DeleteLIByType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.DeleteLIByType
END
GO
/* frmBid - btnBEDeleteByType_Click */
CREATE PROCEDURE DeleteLIByType
	@ProjectID int,
	@Type char (10)
AS
DELETE FROM LineItem WHERE ProjectID = @ProjectID And Type = @Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_DeleteBidLineItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_DeleteBidLineItems
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_DeleteJobLineItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_DeleteJobLineItems
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[DeleteLineItems]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.DeleteLineItems
END
Go
CREATE PROCEDURE DeleteLineItems
	@ProjectID int,
	@Partnumber char (25)
AS
Delete from LineItem where PartNumber = @Partnumber AND ProjectID = @ProjectID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CTPartVSUType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CTPartVSUType
END
GO
CREATE PROCEDURE CTPartVSUType
	@ProjectID int
AS
select PartNumber, Location, Type, UnitType, SUM(Quantity) As Quan
FROM LineItem inner join Unit
on LineItem.UnitID = Unit.UnitID
where LineItem.ProjectID = @ProjectID
group by PartNumber, Location, TYPE, UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_EditBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_EditBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditBldg
END
GO
CREATE PROCEDURE EditBldg
	@BldgID int,
	@ProjectID int,
	@BldgNumber int=NULL,
	@Type nvarchar(50),
	@Description nvarchar(50),
	@Location nvarchar(50),
	@Address nvarchar(50),
	@Notes nvarchar(250),
	@shipdate datetime=NULL,
	@shipsequence int=NULL,
	@Shipped bit=NULL,
	@ShippedR bit=NULL,
	@ShipdateR datetime=null,
	@BldgName varchar(50)=Null
 AS 
UPDATE Building 
	SET
	ProjectID = @ProjectID,
	BldgNumber = @BldgNumber,
	Type = @Type,
	Description = @Description,
	Location = @Location,
	Address = @Address,
	Notes = @Notes,
	shipdate = @shipdate,
	shipsequence = @shipsequence,
	Shipped = @Shipped,
	ShippedR = @ShippedR,
	ShipdateR = @ShipdateR,
	BldgName = @BldgName
	
WHERE BldgID = @bldgID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CopyBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CopyBldg
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CopyBldg]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CopyBldg
END
GO
CREATE PROCEDURE CopyBldg
	@NewBldgID int,
	@OldBldgID int
AS
	INSERT INTO Building
	 (BldgID, ProjectID, BldgName, BldgNumber, Type, Description, Location, Address, Notes, shipdateR, shipdate, shipsequence,
	 Shipped, ShippedR)
	(SELECT @NewBldgID, ProjectID, BldgName, BldgNumber, Type, Description, Location, Address, Notes, shipdateR, shipdate, shipsequence,
	 Shipped, ShippedR from Building WHERE BldgID = @OldBldgID)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CommonSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CommonSummaryExp
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CommonSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CommonSummaryExp
END
GO
CREATE PROCEDURE CommonSummaryExp
	@ProjectID int,
	@Order char(2) = 'pn'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (se * Quantity) as Total
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'X' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order BY ( 
case
	when @Order = 'PN' then pn
	when @Order = 'T' then ty
	when @Order = 'L'  then lo
	END
	)
go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_CommonSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_CommonSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_CommonSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_CommonSummary
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[CommonSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.CommonSummary
END
GO
CREATE PROCEDURE CommonSummary
	@ProjectID int,
	@Order char(2) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (co * Quantity) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'X'
	group by LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
ORDER BY ( CASE 
	WHEN @Order = 'PN' THEN pn
	WHEN @Order = 'T' THEN  ty
	WHEN @Order = 'L'  THEN lo
	END
	),
	Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_ClubHouseSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_ClubHouseSummaryExp
END
Go
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ClubHouseSummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ClubHouseSummaryExp
END
GO
CREATE PROCEDURE ClubHouseSummaryExp
	@ProjectID int,
	@Order char(2) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (se * Quantity) as Total
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'C' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Type, LineItem.location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order BY ( 
case
	when @Order = 'PN' then pn
	when @Order = 'T' then ty
	when @Order = 'L'  then lo
	END
	)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_ClubHouseSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_ClubHouseSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_ClubHouseSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_ClubHouseSummary
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[ClubHouseSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.ClubHouseSummary
END
GO
CREATE PROCEDURE ClubHouseSummary
	@ProjectID int,
	@Order char(2) = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (co * Quantity) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'C'
	group by LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
ORDER BY ( CASE 
	WHEN @Order = 'PN' THEN pn
	WHEN @Order = 'T' THEN  ty
	WHEN @Order = 'L'  THEN lo
	END
	)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddBldgUnitCnt]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddBldgUnitCnt
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_BldgUnitCnt]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_BldgUnitCnt
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BldgUnitCnt]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BldgUnitCnt
END
GO
CREATE PROCEDURE BldgUnitCnt
	@BldgID int,
	@ProjectID int
AS
select COUNT(UnitID) Quantity, BldgID, UnitType, Description from Unit where bldgID = @BldgID and ProjectId = @ProjectID group by BldgID, UnitType, Description
ORDER BY 
	Unit.UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_BldgBOMExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_BldgBOMExp
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BldgBOMExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BldgBOMExp
END
GO
CREATE PROCEDURE BldgBOMExp
	@BldgID int
AS
select PartNumber pn, Description,  COUNT(ItemID) Quan, sell as Price, SUM(sell) As Total 
from LineItem where BldgID = @BldgID 
group by PartNumber, Description, Sell
order by PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AuxillarySummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AuxillarySummaryExp
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AuxillarySummaryExp]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AuxillarySummaryExp
END
GO
CREATE PROCEDURE AuxillarySummaryExp
	@ProjectID int,
	@Order varchar = 'PN'
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, se Price, (Quantity * se) as Total
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = 'A' and LineItem.BldgID is not null
	group by LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order by(
case
	when @Order = 'PN' then pn
	when @Order = 'T' then ty
	when @Order = 'L'  then lo
end
)
go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AuxillarySummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AuxillarySummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_AuxSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_AuxSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AuxillarySummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AuxillarySummary
END
GO
CREATE PROCEDURE AuxillarySummary
	@ProjectID int,
	@Order varchar = 'PN'
AS
SELECT PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, 
	Cost, Sell, SUM(LineItem.Quantity) AS Quantity
FROM Unit INNER JOIN
	LineItem ON Unit.UnitID = LineItem.UnitID
WHERE (LineItem.ProjectID = @ProjectID) AND (SUBSTRING(Unit.UnitType, 1, 1) = 'A')
GROUP BY LineItem.PartNumber, LineItem.Type, LineItem.Location, LineItem.Description, 
	LineItem.Cost, LineItem.Sell
ORDER BY 
	( CASE 
	WHEN @Order = 'PN' THEN  PartNumber
	WHEN @Order = 'T' THEN  Type
	WHEN @Order = 'L'  THEN Location
	END
	), Type
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AltLineItemSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AltLineItemSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AltLineItemSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AltLineItemSummary
END
GO
CREATE PROCEDURE AltLineItemSummary
	@ProjectID int
AS   
select COUNT(ItemID) Quan, PartNumber, Description, Cost, Margin, Sell
from LineItem where ProjectID = @ProjectID
group by PartNumber, Description, Cost, Margin, Sell
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AlternateSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AlternateSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AlternateSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AlternateSummary
END
GO
CREATE PROCEDURE AlternateSummary
	@ProjectID int
 AS
Select AltID, OrgProductID, OrgDescription, Sell, SUM(Quan) QUAN, AltProductID, AltDescription, AltSell
from (select sum(LineItem.Quantity) AS Quan, PartNumber, Sell
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID
	group by LineItem.PartNumber, Sell
) tempTable inner join Alternates on tempTable.PartNumber = Alternates.OrgProductID
Where ProjectID = @ProjectID
GROUP BY AltID, OrgProductID, OrgDescription,Sell, AltProductID, AltDescription, AltSell
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddUnitLineItemFromCatalog]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddUnitLineItemFromCatalog
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddUnitLineItemFromCatalog]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddUnitLineItemFromCatalog
END
GO
CREATE PROCEDURE AddUnitLineItemFromCatalog
	@ProjectID int,
	@UnitID int,
	@Quantity int,
	@RecordSource char (15),
	@PartNumber char (25),
	@RT	char (1),
	@Description nvarchar (50),
	@Cost money,
	@Margin int,
	@Sell money
 AS
INSERT INTO LineItem
	(ProjectID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell)
VALUES
	(@ProjectID,  @RecordSource, @PartNumber, @RT, @Description, @Cost, @Margin, @Sell);
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddUnitLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddUnitLineItem
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_AddUnitItemQuan]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_AddUnitItemQuan
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[jr_UnitTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.jr_UnitTypes
END
GO
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitTypes]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitTypes
END
GO
CREATE PROCEDURE UnitTypes
	@ProjectID int
AS
SELECT TOP 100 PERCENT UnitType, Description, COUNT(UnitType) AS Quan
FROM Unit
WHERE ProjectID = @ProjectID AND SUBSTRING(UnitType, 1, 1) = 'U'
GROUP BY UnitType, Description
ORDER BY UnitType
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitBOM]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitBOM
END
GO
CREATE PROCEDURE UnitBOM
	@BldgID int
AS
SELECT COUNT(UnitType) AS [Unit Count], UnitType, LineItem.Quantity, PartNumber, 
	LineItem.Description, LineItem.Location, Sell, LineItem.Quantity * Sell AS TotalSell
FROM Unit INNER JOIN
	LineItem ON Unit.UnitID = LineItem.UnitID
WHERE (Unit.BldgID = @BldgID)
GROUP BY UnitType, LineItem.Quantity, PartNumber, LineItem.Description, LineItem.Location, 
	Sell, LineItem.Quantity * Sell
ORDER BY Location
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptJobSummaryTBD]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptJobSummaryTBD
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptJobSummaryTBD]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptJobSummaryTBD
END
GO
CREATE PROCEDURE rptJobSummaryTBD
	@ProjectID int
AS
SELECT Partnumber, SUM(Quantity) AS Quan , Cost, Margin, Sell, SUM(Quantity)* Sell AS Total, SUM(shipquan) AS Shipped, Sum(QuanBO) AS BO  
From LineItem Where ProjectID = @ProjectID AND LEFT(PartNumber,3) ='TBD'
Group By Partnumber, Cost, Margin, Sell
Order By Partnumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_rptJobSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_rptJobSummary
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptJobSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptJobSummary
END
Go
CREATE PROCEDURE rptJobSummary
	@ProjectID int
AS
SELECT Partnumber, SUM(Quantity) AS Quan , Cost, Margin, Sell, SUM(Quantity)* Sell AS Total, SUM(shipquan) AS Shipped, Sum(QuanBO) AS BO  
From LineItem Where ProjectID = @ProjectID
Group By Partnumber, Cost, Margin, Sell
Order By Partnumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_PartsMaxMin]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_PartsMaxMin
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[PartsMaxMin]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.PartsMaxMin
END
Go
CREATE PROCEDURE PartsMaxMin
	@StartDate datetime,
	@EndDate datetime,
	@ProjectType varchar(15)
AS
SELECT PartNumber, max(Cost) AS Max, min(cost) AS Min, sum(Quantity) AS Total, 
SUM((Quantity*Cost))/ sum(Quantity) AS Average
from LineItem 
WHERE shipdate between @StartDate and  @EndDate and shipped = 0 and BldgID is not null and quantity != 0 and ProjectID in
(select ProjectID from Project where ProjectType = @ProjectType)
Group By Partnumber
Order By PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_PartsFind]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_PartsFind
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[PartsFind]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.PartsFind
END
GO
CREATE PROCEDURE PartsFind
	@PartNumber char (25),
	@ProjectType varchar (15),
	@Criteria char (1)
AS
IF @Criteria = 'N' BEGIN
(SELECT LineItem.PartNumber, SUM(LineItem.Quantity) AS Quantity, LineItem.Shipped, Project.LDID, Project.Description
FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID
WHERE (LineItem.PartNumber = @PartNumber) and ProjectType = @ProjectType
GROUP BY LineItem.PartNumber, LineItem.Shipped, Project.Description, Project.LDID
HAVING (LineItem.Shipped = 0))
END

IF @Criteria = 'S' BEGIN
(SELECT LineItem.PartNumber, SUM(LineItem.Quantity) AS Quantity, LineItem.Shipped, Project.LDID, Project.Description
FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID
WHERE LineItem.PartNumber = @PartNumber and ProjectType = @ProjectType
GROUP BY LineItem.PartNumber, LineItem.Shipped, Project.Description, Project.LDID
HAVING LineItem.Shipped = 1)
END

IF @Criteria = 'U' BEGIN
(SELECT LineItem.PartNumber, SUM(LineItem.Quantity) AS Quantity, LineItem.Shipped, Project.LDID, Project.Description
FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID
WHERE LineItem.PartNumber = @PartNumber and ProjectType = @ProjectType
GROUP BY LineItem.PartNumber, LineItem.Shipped, Project.Description, Project.LDID)
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_PartsByDateRangeJN]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_PartsByDateRangeJN
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[PartsByDateRangeJN]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.PartsByDateRangeJN
END
go
CREATE PROCEDURE PartsByDateRangeJN
	@StartDate datetime,
	@EndDate datetime,
	@ProjectType varchar(15)
AS

SELECT Project.LDID as BidID, LineItem.PartNumber, LineItem.Description, SUM(Cost * Quantity) AS Cost, SUM(Sell * Quantity) AS Sell, SUM(Quantity) AS Quan
FROM Project INNER JOIN
	dbo.LineItem ON dbo.Project.ProjectID = dbo.LineItem.ProjectID
WHERE (ShipDate BETWEEN @StartDate AND @EndDate) AND shipped = 0 and ProjectType = @ProjectType and BldgID is not null
GROUP BY Project.LDID, PartNumber, LineItem.Description
ORDER BY PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_PartsByDateRangeInv]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_PartsByDateRangeInv
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[PartsByDateRangeInv]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.PartsByDateRangeInv
END
GO
CREATE PROCEDURE PartsByDateRangeInv
	@StartDate as datetime,
	@EndDate as datetime,
	@ProjectType as varchar(15)
AS

SELECT PartNumber, LineItem.Description, VendorCode, CatalogMain.VendorPartNo,
	CatalogMain.AvailableUnits, CatalogMain.OnOrderUnits, CatalogMain.CommittedUnits,		
	SUM(Cost * Quantity) AS Cost, SUM(Sell * Quantity) AS Sell, SUM(Quantity) AS Quan
FROM CatalogMain RIGHT OUTER JOIN
	LineItem ON CatalogMain.ProductID = LineItem.PartNumber
WHERE (ShipDate BETWEEN @StartDate AND @EndDate) AND shipped = 0 And BldgID is not null and ProjectID in 
(select ProjectID from Project where ProjectType = @ProjectType)
GROUP BY PartNumber, LineItem.Description, VendorCode, CatalogMain.VendorPartNo, CatalogMain.AvailableUnits, 
	CatalogMain.OnOrderUnits, CatalogMain.CommittedUnits
ORDER BY PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_PartsByDateRange]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_PartsByDateRange
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[PartsByDateRange]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.PartsByDateRange
END
GO
CREATE PROCEDURE PartsByDateRange
	@StartDate as datetime,
	@EndDate as datetime,
	@ProjectType as varchar(15)
AS
SELECT PartNumber, Description, SUM(Cost * Quantity) AS Cost, SUM(Sell * Quantity) AS Sell, SUM(Quantity) AS Quan
FROM LineItem 
WHERE (ShipDate BETWEEN @StartDate AND @EndDate) AND shipped = 0 and BldgID is not null and ProjectID in 
	(select ProjectID from Project where ProjectType = @ProjectType)
GROUP BY PartNumber, Description
ORDER BY PartNumber
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_EditLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_EditLineItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[EditLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.EditLineItem
END
GO
CREATE PROCEDURE EditLineItem
	@ItemID int,
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@Quantity int,
	@Location nvarchar(50),
	@Type nvarchar(10),
	@Shipped bit,
	@shipdate datetime,
	@shipQuan int,
	@QuanBO int
AS 
UPDATE LineItem
	SET 
	RecordSource = @RecordSource,
	PartNumber = @PartNumber,
	RT = @RT,
	Description = @Description,
	Cost = @Cost,
	Margin = @Margin,
	Sell = @Sell,
	Quantity = @Quantity,
	Location = @Location,
	Type = @Type,
	Shipped = @Shipped,
	ShipDate = @shipdate,
	ShipQuan = @shipQuan,
	QuanBo = @QuanBO
WHERE ItemID = @ItemID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_JobsReport]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_JobsReport
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[JobsReport]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.JobsReport
END
Go
CREATE PROCEDURE JobsReport   
	@ProjectType varchar(15)   
AS
SELECT Project.ProjectID, Project.LDID, Project.SourceID, Project.ContractDate, Project.Description, Project.Location, Project.Closed, Clients.Name, 
	SUM(LineItem.Quantity * LineItem.Cost) AS Cost, SUM(LineItem.Quantity * LineItem.Sell) AS Sell
FROM Project INNER JOIN
	Clients ON Project.ClientID = Clients.ClientID INNER JOIN
	LineItem ON Project.ProjectID = LineItem.ProjectID
where ProjectType = @ProjectType
GROUP BY Project.ProjectID, Project.LDID, Project.SourceID, Project.ContractDate, Project.Description, Project.Location, Project.Closed, Clients.Name
ORDER BY Project.LDID
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_AddLIUnitWC]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_AddLIUnitWC
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddLIUnitWC]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddLIUnitWC
END
Go
CREATE PROCEDURE AddLIUnitWC
	@ProjectID int,
	@UnitTypeWC varchar(10),
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@Quantity int,
	@Location nvarchar(50),
	@Type nvarchar(10)
	
AS
	INSERT INTO LineItem (ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description, Cost,
	Margin, Sell, Quantity, Location, Type, Shipped)
	(SELECT ProjectID, BldgID, UnitID, @RecordSource, @PartNumber, @RT, @Description, 
	@Cost, @Margin, @Sell, @Quantity, @Location, @Type, 0
FROM dbo.Unit
WHERE (ProjectID = @ProjectID) AND (UnitType LIKE (@UnitTypeWC+'%')))
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_AddLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_AddLineItem
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[AddLineItem]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.AddLineItem
END
Go
CREATE PROCEDURE AddLineItem 
	@ProjectID int,
	@BldgID int = NULL,
	@UnitID int,
	@RecordSource char(10),
	@PartNumber char(25),
	@RT char(1),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money,
	@Quantity int,
	@Location nvarchar(50),
	@Type nvarchar(10),
	@Shipped bit = 0,
	@ShipDate datetime=NULL,
	@shipQuan int = 0,
	@QuanBO int= 0
AS 
INSERT INTO LineItem
	(ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description,
	 Cost, Margin, Sell, Quantity, Location, Type, Shipped, Shipdate, ShipQuan, QuanBO)
VALUES
	(@ProjectID, @BldgID, @UnitID,@RecordSource, @PartNumber, @RT,  @Description,
	 @Cost, @Margin, @Sell, @Quantity, @Location, @Type, @Shipped, @Shipdate, @ShipQuan, @QuanBO)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_BELIbyType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_BELIbyType
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BELIbyType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BELIbyType
END
go
CREATE PROCEDURE BELIbyType
	@ProjectID int,
	@Type nvarchar (10)
AS
SELECT DISTINCT Type, PartNumber, Description, RT, Cost, Margin, Sell
FROM LineItem
WHERE (ProjectID = @ProjectID) AND
	(NOT (Type IS NULL)) AND 
	(Type <> '') AND 
  	(Type = @Type)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_BeByType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_BeByType
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BeByType]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BeByType
END
go
CREATE PROCEDURE BeByType
	@ProjectID int,
	@Type nvarchar(10),
	@PartNumber char(25),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money
AS	
UPDATE LineItem SET
 	Partnumber = @PartNumber, 
	Description = @Description, 
	Cost = @Cost, 
	Margin = @Margin, 
	Sell = @Sell 
Where Type = @Type
AND ProjectID = @ProjectID
AND Shipped = 0
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_BeByPartNumber]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_BeByPartNumber
END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BeByPartNumber]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BeByPartNumber
END
go
CREATE PROCEDURE BeByPartNumber
	@ProjectID int,
	@OldPartNumber char(25),
	@PartNumber char(25),
	@Description nvarchar(50),
	@Cost money,
	@Margin int,
	@Sell money
AS	
UPDATE LineItem SET
 	Partnumber = @PartNumber, 
	Description = @Description, 
	Cost = @Cost, 
	Margin = @Margin, 
	Sell = @Sell 
Where PartNumber = @OldPartNumber 
AND ProjectID = @ProjectID
AND Shipped = 0
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_Bldgs]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_Bldgs
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Bldgs]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.Bldgs
END
go
CREATE PROCEDURE Bldgs
	@ProjectID int
AS
SELECT     
	BldgID, 
	RTRIM(CASE WHEN (LEN(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-'))=1) 
	Then 
		'0' + RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-') ) 
	Else 
		RTRIM(ISNULL(RTRIM(CAST(BldgNumber AS CHAR) ), '-') )  END) 
	 + ' ' + Type + ' ' + Description + ' -  ' 
	+ Location AS BLDG
FROM         Building
WHERE     (ProjectID = @ProjectID)
ORDER BY BLDG
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[j_BldgShipByDate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.j_BldgShipByDate
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[BldgShipByDate]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.BldgShipByDate
END
go
CREATE PROCEDURE BldgShipByDate
	@StartDate DateTime,
	@EndDate DateTime, 
	@ProjectType varchar(25)
AS
SELECT Project.ProjectID as BidID, Project.LDID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate,
	LineItem.RT, SUM(LineItem.Sell*LineItem.Quantity) AS Total 
FROM Building INNER JOIN LineItem ON Building.BldgID = LineItem.BldgID INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID
WHERE ShipDateR Between @StartDate And @EndDate AND RT = 'R' AND Building.ShippedR = 0 and ProjectType = @ProjectType
GROUP BY Project.ProjectID, Project.LDID, Building.BldgNumber, Building.BldgName, Building.shipdateR,
	Building.shipdate,LineItem.RT
UNION ALL
SELECT Project.ProjectID, Project.LDID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate,
	LineItem.RT, SUM(LineItem.Sell*LineItem.Quantity) AS Total 
From Building INNER JOIN LineItem ON Building.BldgID = LineItem.BldgID INNER JOIN
	Project ON Building.ProjectID = Project.ProjectID
WHERE Building.ShipDate Between @StartDate And @EndDate AND RT = 'T' AND Building.Shipped = 0 and ProjectType = @ProjectType
GROUP BY Project.ProjectID, Project.LDID, Building.BldgNumber, Building.BldgName, Building.shipdateR, Building.shipdate,
	LineItem.RT
ORDER BY Building.ShipDate
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptAllUnitDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptAllUnitDetails
END
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[b_rptAllUnitDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.b_rptAllUnitDetails
END
go
CREATE PROCEDURE rptAllUnitDetails
	@ProjectID int
as
select distinct unit.UnitType, unit.description as UnitDesc, LineItem.Quantity, LineItem.PartNumber, LineItem.Description,
	LineItem.Location, LineItem.RT, LineItem.Type, LineItem.Cost, LineItem.Margin, LineItem.Sell
from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID where LineItem.ProjectID = @ProjectID and LineItem.BldgID is not null
order by UnitType, Location, PartNumber
GO

IF exists (Select * from sysobjects where id = object_id(N'[dbo].[_BidBulbItemQuan]'))
	Drop Table _BidBulbItemQuan
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[_BidBulbLineItemDef]'))
	Drop Table _BidBulbLineItemDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[_BldgDef]'))
	Drop Table _BldgDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[b_tmpUnitCount]'))
	Drop Table b_tmpUnitCount
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[bidComments]'))
	Drop Table bidComments
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[Bids]'))
	Drop Table Bids
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[BidUnitDef]'))
	Drop Table BidUnitDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[BidUnitItemQuan]'))
	Drop Table BidUnitItemQuan
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[BidUnitLineItemDef]'))
	Drop Table BidUnitLineItemDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[j_Bldg]'))
	Drop Table j_Bldg
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[j_Jobs]'))
	Drop Table j_Jobs
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[tmpBldgID]'))
	Drop Table tmpBldgID
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[tmpItemID]'))
	Drop Table tmpItemID
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[tmpUnitID]'))
	Drop Table tmpUnitID
GO