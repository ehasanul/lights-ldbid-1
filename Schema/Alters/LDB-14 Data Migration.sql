--Drop contraints for ease of data transition. These are restored at the end.

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_Units_j_Bldg]') and parent_object_id = object_id(N'[dbo].[j_Units]'))
	alter table j_Units drop constraint FK_j_Units_j_Bldg

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_Unit_Bldg]') and parent_object_id = object_id(N'[dbo].[j_Units]'))
	alter table j_Units drop constraint FK_Unit_Bldg
	
if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_Bldg_j_Jobs]') and parent_object_id = object_id(N'[dbo].[j_Bldg]'))
	alter table j_bldg drop constraint FK_j_Bldg_j_Jobs

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_LineItems_j_Units]') and parent_object_id = object_id(N'[dbo].[j_LineItems]'))
	alter table j_LineItems drop constraint FK_j_LineItems_j_Units

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_j_Comments_j_Jobs]') AND parent_object_id = OBJECT_ID(N'[dbo].[j_Comments]'))
	alter table j_Comments drop constraint FK_j_Comments_j_Jobs

if  exists (select * from sys.foreign_keys where object_id = object_id(N'[dbo].[FK_Comments_Projects]') AND parent_object_id = OBJECT_ID(N'[dbo].[j_Comments]'))
	alter table j_Comments drop constraint FK_Comments_Projects
go
-- Create a series of tables that are used to transition the data
-- These are used in a couple of different way. One is to help transform
-- them as they make their way into the new tables. The other is to help
-- record a lookup of their former identities for restoring foreign keys

-- Used in a join to create a sequenced column
if Object_id('tempdb..#ordinals','u')is not null begin
	drop table #ordinals
end
create table #ordinals(
	ordernum int primary key identity,
	tempNumber int
)

-- A couple of temp table is needed to help with flattening the Unit relationship
if Object_id('tempdb..#bidconversion','u')is not null begin
	Drop table #bidconversion
end
create table #bidconversion(
	bcID int identity(1,1),
	BidId int,
	UnitID int,
	UnitType varchar(10),
	[Description] varchar(50),
	Quan int,
	BldgID int
)
if Object_id('tempdb..#units','u')is not null begin
	Drop table #units
end
create table #units(
	newID int identity,
	UnitID int,
	BidID int,
	UnitType varchar(10),
	[Description] varchar(50),
	BldgID int,
	UnitName varchar(20)
)

-- Used to maintain the former building relationship
if Object_id('tempdb..#Building','u')is not null begin
	Drop Table #Building
end
Create table #Building (
	NewBuildingID int NOT NULL Primary Key IDENTITY,
	OldBuildingID int,
	ProjectID int,
	BldgNumber int,
	Type nvarchar(50),
	Description nvarchar(50),
	location nvarchar(50),
	Address nvarchar(50),
	Notes nvarchar(250),
	shipdate datetime,
	shipsequence int,
	Shipped bit,
	ShippedR bit,
	ShipdateR datetime,
	BldgName varchar(50)
)

-- There was overlap in the Bid and Job table they needed to be condensed and their former relationships saved
if Object_id('tempdb..#Project','u')is not null begin
	Drop Table #Project
end

Create table #Project (
	NewProjectID int NOT NULL Primary Key IDENTITY,
	OldProjectID int,
	SourceID int,
	ProjectDate datetime,
	ClientID int,
	Description varchar(50),
	Location varchar(50),
	Address varchar(50),
	City varchar(50),
	State char(2),
	Zip char(10),
	BidContact varchar(50),
	ClientPM varchar(50),
	ClientMobilePhone varchar(14),
	LDPM varchar(50),
	Notes varchar(250),
	LDEstimator varchar(50),
	BluePrintDate datetime,
	RevDate datetime,
	SalesTaxRate float,
	AuxLabel varchar(25),
	ContractDate datetime,
	ContractAmount money,
	ContractBalance	money,
	ProjectType	varchar(25),
	Closed bit
)

--Used to hold the flattening of the line item object and help reestablish the foregin keys
if Object_id('tempdb..#Itemconversion','u')is not null begin
	Drop table #Itemconversion
end
create table #Itemconversion(
	icID int identity(1,1),
	ItemID int,
	ProjectID int,
	BldgID int,
	UnitID int,
	RecordSource varchar(10),
	PartNumber varchar(25),
	RT char(1),
	[Description] varchar(50),
	Cost money,
	Margin int,
	Sell money,
	Quantity int,
	Location varchar(50),
	Type varchar(10),
	Shipped bit,
	ShipDate datetime,
	ShipQuan int,
	QuanBO int,
	UnitType varchar(25)
)
go

-- The new Project table that will house bids, jobs and verbals
if exists (Select * from sysobjects where id = object_id(N'[dbo].[Project]'))
	Drop Table Project

Create table Project (
	ProjectID int NOT NULL Primary Key IDENTITY,
	SourceID int,
	ProjectDate datetime,
	ClientID int,
	Description varchar(50),
	Location varchar(50),
	Address varchar(50),
	City varchar(50),
	State char(2),
	Zip char(10),
	ProjectContact varchar(50),
	ClientPM varchar(50),
	ClientMobilePhone varchar(14),
	LDPM varchar(50),
	Notes varchar(250),
	LDEstimator varchar(50),
	BluePrintDate datetime,
	RevDate datetime,
	SalesTaxRate float,
	AuxLabel varchar(25),
	ContractDate datetime,
	ContractAmount money,
	ContractBalance money,
	ProjectType varchar(25),
	Closed bit
)
go

--Migrate Jobs from j_Jobs table
Insert into #Project (OldProjectID, SourceID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ContractDate, ContractAmount, ContractBalance, Closed, ProjectType) Select JobID, BidID, JobDate, ClientID,
	Description, Location, Address, City, State, Zip, BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ContractDate, ContractAmount, ContractBalance, Closed, 'Job' from j_Jobs
	
--Migrate Bids from Bids table
Insert into #Project (OldProjectID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ProjectType) Select BidID, EntryDate, ClientID,	Description, Location, Address, City, State, Zip, 
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	'Bid' from Bids
go

--Update the source ID of "Job" projects 
update p1 set SourceID = p2.NewProjectID from #Project p2 inner join #Project p1 on p1.SourceID = p2.OldProjectID where p1.ProjectType = 'job'

--Place them into the newly formed table
set identity_insert Project on
insert into Project(ProjectID, SourceID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	ProjectContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel,
	ContractDate, ContractAmount, ContractBalance, ProjectType, Closed) select NewProjectID, SourceID, ProjectDate, ClientID, Description, Location, Address, City, State, Zip,
	BidContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, AuxLabel, ContractDate, ContractAmount, ContractBalance,
	ProjectType, Closed from #Project

--Restore the identity
set identity_insert Project off
Declare @NEXTPROJECTID int
Select @NEXTPROJECTID = MAX(ProjectID) from Project
DBCC CHECKIDENT('Project', RESEED, @NEXTPROJECTID)

-- Update any bid/job reference to the new project reference so when they are brought over they will already be referencing the correct
-- thing
update Building set BidID = #Project.NewProjectID from #Project inner join Building on #Project.OldProjectID = Building.BidId where #Project.ProjectType = 'Bid'
update j_Bldg set JobID = #Project.NewProjectID from #Project inner join j_Bldg on #Project.OldProjectID = j_Bldg.JobID where #Project.ProjectType ='Job'
update j_LineItems set JobID = #Project.NewProjectID from #Project inner join j_LineItems on #Project.OldProjectID = j_LineItems.JobID where #Project.ProjectType ='Job'
update j_Units set JobID = #Project.NewProjectID from #Project inner join j_Units on #Project.OldProjectID = j_Units.JobID where #Project.ProjectType ='Job'
update BidUnitDef set BidID = #Project.NewProjectID from #Project inner join BidUnitDef on #Project.OldProjectID = BidUnitDef.BidId where #Project.ProjectType = 'Bid'
update BidUnitItemQuan set BidID = #Project.NewProjectID from #Project inner join BidUnitItemQuan on #Project.OldProjectID = BidUnitItemQuan.BidId where #Project.ProjectType = 'Bid'
update bidUnitLineItemDef set BidID = #Project.NewProjectID from #Project inner join bidUnitLineItemDef on #Project.OldProjectID = bidUnitLineItemDef.BidId where #Project.ProjectType = 'Bid'
update Alternates set BidID = #Project.NewProjectID from #Project inner join Alternates on #Project.OldProjectID = Alternates.BidId where #Project.ProjectType = 'Bid'
update bidComments set BidID = #Project.NewProjectID from #Project inner join bidComments on #Project.OldProjectID = bidComments.BidId where #Project.ProjectType = 'Bid'
update j_ChangeOrder set JobID = #Project.NewProjectID from #Project inner join j_ChangeOrder on #Project.OldProjectID = j_ChangeOrder.JobID where #Project.ProjectType ='Job'
update j_Comments set JobID = #Project.NewProjectID from #Project inner join j_Comments on #Project.OldProjectID = j_Comments.JobID where #Project.ProjectType ='Job'

--The Building table needs to be modified to hold both kinds of buildings
if not exists(select * from sys.columns where Name = N'Shipped' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add Shipped bit
end
if not exists(select * from sys.columns where Name = N'ShippedR' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add ShippedR bit
end
if not exists(select * from sys.columns where Name = N'ShipdateR' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add ShipdateR datetime
end
if not exists(select * from sys.columns where Name = N'BldgName' and Object_ID = Object_ID(N'Building'))begin
	Alter table Building add BldgName varchar(50)
end
if exists(select * from sys.columns where Name = N'BidID' and Object_ID = Object_ID(N'Building'))begin
	exec sp_rename 'Building.BidID', 'ProjectID', 'Column'
end
go

--Prepare #Building identity
Declare @NEXTBUILDINGID int
Select @NEXTBUILDINGID = MAX(BldgID)+1 from Building
DBCC CHECKIDENT('#Building', RESEED, @NEXTBUILDINGID)

--Move the job buildings over into the temp table so we can save the relationship for later
insert into #Building (OldBuildingID, ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR) select BldgID, JobID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR from j_Bldg
go

--Move the job buildings into Building
set identity_insert Building on
insert into Building(BldgID, ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR) select NewBuildingID, ProjectID, BldgNumber, BldgName, Type, Description, Location, Address, Notes, shipdate, shipsequence,
	Shipped, ShippedR, ShipdateR from #Building
set identity_insert Building off

go

--Update any Job, Bldg references
update j_Units set BldgID = #Building.NewBuildingID from #Building inner join j_Units on #Building.OldBuildingID = j_Units.BldgID
update j_LineItems set BldgID = #Building.NewBuildingID from #Building inner Join j_LineItems on #Building.OldBuildingID = j_LineItems.BldgID

--rename the job unit table to get it ready for generic use
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_Units]')) begin
	exec sp_rename 'j_Units', 'Unit'
end
if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'Unit'))begin
	exec sp_rename 'Unit.JobID', 'ProjectID', 'Column'
end
Alter Table Unit Alter Column BldgID int NULL

--Prepare the ordinals table for use
insert into #ordinals(tempNumber) select top 1000 UnitID from Unit
go

--We need to start the units number outside of range of the unit table
--We also have to maintain the history to restablish the relationships
declare @NEXTUNITID int
select @NEXTUNITID = MAX(UnitID) + 1000 from Unit
DBCC CHECKIDENT('#Units', RESEED,@NEXTUNITID)

--Start piecing together bid units
insert into #bidconversion(UnitID, BidId, UnitType, Description, Quan, BldgID)
Select BidUnitDef.UnitID uid, BidID, UnitType, Description, Quan, BLdgID from BidUnitDef inner join BldgUnitCount on BidUnitDef.UnitID = BldgUnitCount.UnitID
group by BidUnitDef.UnitID, BidID, UnitType, Description, Quan, BldgID

--the following represents units that were created but not added to a building yet
insert into #units(UnitID, BidID, UnitType, Description) 
select UnitId, BidId, UnitType,Description from BidUnitDef where UnitID not in (select UnitID from BldgUnitCount)

insert into #units(UnitID, BidID, UnitType, Description, BldgID, UnitName)
select UnitID, BidID, UnitType, Description, BldgID, rtrim(UnitType) + '-' + convert(varchar,ordernum)
from #bidconversion inner join #ordinals on ordernum <= Quan 
go

--prepare the unit identity
set identity_insert Unit on

--Move Bid Units over to the standard table
insert into Unit(UnitID, ProjectID, BldgID, UnitName, UnitType, Description) 
select NEWID, BidID, BldgID, UnitName, UnitType, Description
from #units 
set identity_insert Unit off

--restablish unit identity
declare @NEXTUNITID int
select @NEXTUNITID = MAX(UnitID)+1000 from Unit
DBCC CHECKIDENT('Unit', RESEED,@NEXTUNITID)
go

--Update All Bid Unit references that are NOT part of the unit definition
--The trick is this is only in the line item and we have to wait until we construct it to update it

--Rename the job line item table to get it ready for generic use
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_LineItems]')) BEGIN
	exec sp_rename 'j_LineItems', 'LineItem'
end
if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'LineItem'))begin
	exec sp_rename 'LineItem.JobID', 'ProjectID', 'Column'
end
go

--Prepare itemconversion Identity for Bid line Items
declare @NEXTITEMID int
select @NEXTITEMID = MAX(ItemID) + 1000 from LineItem
DBCC CHECKIDENT('#Itemconversion', RESEED,@NEXTITEMID)

-- Grab a flattening of line items
--Because we are joining on the unit conversion table we are able to grab the new unit id directly
-- no need to adjust later

insert into #Itemconversion(ItemID, ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell,
	Quantity, Location, Type) 
select uid, tqBidID, BldgID, #units.newID, RecordSource, PartNumber, RT, De, Cost, Margin, Sell, Quantity, Location, Type
from (select bidUnitLineItemDef.ItemID uid, bidUnitLineItemDef.BidID tqBidID, BldgID tqBldgID, BidUnitItemQuan.UnitID tqUnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description De, Cost, Margin, Sell, Quantity, Location,BidUnitDef.UnitType tqUnitType,
	Type from bidUnitLineItemDef inner join BidUnitItemQuan on bidUnitLineItemDef.ItemID = BidUnitItemQuan.ItemID and
	bidUnitLineItemDef.BidID = BidUnitItemQuan.BidID inner join BidUnitDef on BidUnitDef.UnitID = BidUnitItemQuan.UnitID and
	BidUnitDef.BidID = BidUnitItemQuan.BidID inner join BldgUnitCount on BldgUnitCount.UnitID = BidUnitItemQuan.UnitID
	group by bidUnitLineItemDef.ItemID, bidUnitLineItemDef.BidID, BldgID, BidUnitItemQuan.UnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description, Cost, Margin, Sell, Quantity, Location, BidUnitDef.UnitType,
	Type)tq inner join #units on tqUnitType = #units.UnitType and tqBidID = #units.BidID and tqBldgID =
	#units.BldgID
	
--LineItems not yet belonging to buildings
insert into #Itemconversion(ItemID, ProjectID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, Sell,
	Quantity, Location, Type)
select uid, tqBidID, #units.newID, RecordSource, PartNumber, RT, De, Cost, Margin, Sell, Quantity, Location, Type
from (select bidUnitLineItemDef.ItemID uid, bidUnitLineItemDef.BidID tqBidID, BldgID tqBldgID, BidUnitItemQuan.UnitID tqUnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description De, Cost, Margin, Sell, Quantity, Location,BidUnitDef.UnitType tqUnitType,
	Type from bidUnitLineItemDef inner join BidUnitItemQuan on bidUnitLineItemDef.ItemID = BidUnitItemQuan.ItemID and
	bidUnitLineItemDef.BidID = BidUnitItemQuan.BidID inner join BidUnitDef on BidUnitDef.UnitID = BidUnitItemQuan.UnitID and
	BidUnitDef.BidID = BidUnitItemQuan.BidID inner join BldgUnitCount on BldgUnitCount.UnitID = BidUnitItemQuan.UnitID
	group by bidUnitLineItemDef.ItemID, bidUnitLineItemDef.BidID, BldgID, BidUnitItemQuan.UnitID, RecordSource, PartNumber, RT, 
	BidUnitLineItemDef.Description, Cost, Margin, Sell, Quantity, Location, BidUnitDef.UnitType,
	Type)tq inner join #units on tqUnitType = #units.UnitType and tqBidID = #units.BidID where BldgID is null
go

--Prepare lineitem identity for new bid lineitems
declare @NEXTITEMID int
select @NEXTITEMID = MAX(ItemID) + 1000 from LineItem
DBCC CHECKIDENT('LineItem', RESEED,@NEXTITEMID) 
Set identity_insert LineItem on

--The new structure allows bid line items to have a null bldgid
Alter table LineItem Alter Column BldgID int NULL

--Move Bid Line Items over to the standard table
insert into LineItem(ItemId, ProjectID, BldgID, UnitID, RecordSource, PartNumber, RT, Description,
	Cost, Margin, Sell, Quantity, Location, Type)
select icid, projectid, bldgid, unitid, RecordSource, PartNumber, RT, Description, Cost,
	Margin, Sell, Quantity, Location, Type from #Itemconversion

--Restore lineitem identity
Select @NEXTITEMID = MAX(ItemID)+1000 From LineItem
Set identity_insert LineItem off
DBCC CHECKIDENT('LineItem', RESEED,@NEXTITEMID) 

--rename the job comment table to get it ready for generic use
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_Comments]')) begin
	exec sp_rename 'j_Comments', 'Comment'
end
if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'Comment'))begin
	exec sp_rename 'Comment.JobID', 'ProjectID', 'Column'
end

--Restore the relationship for the comments int the bid and job tables before combining them
Update bidComments set BidID = #Project.NewProjectID from #Project inner join bidComments on
#Project.OldProjectID = bidComments.BidID where #Project.ProjectType = 'bid'

Update Comment set ProjectID = #Project.NewProjectID from #Project inner join Comment on
#Project.OldProjectID = Comment.ProjectID where #Project.ProjectType = 'job' 

--combine the comment tables
insert into Comment(ProjectID, EntryDate, Author, Comment)
select BidID, EntryDate, Author, Comment from bidComments

--All of the data has been moved. For tables whose data is not being moved rename any appropriate columns
if exists(select * from sys.columns where Name = N'BidID' and Object_ID = Object_ID(N'Alternates'))begin
	exec sp_rename 'Alternates.BidId', 'ProjectID', 'Column'
end
go
if exists (select * from sysobjects where id = OBJECT_ID(N'[dbo].[j_ChangeOrder]')) begin
	exec sp_rename 'j_ChangeOrder', 'ChangeOrder'
end

if exists(select * from sys.columns where Name = N'JobID' and Object_ID = Object_ID(N'ChangeOrder'))begin
	exec sp_rename 'ChangeOrder.JobID', 'ProjectID', 'Column'
end