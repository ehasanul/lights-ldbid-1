if OBJECT_ID('dbo.AddUnitLineItemFromCatalog') is not null
  drop procedure dbo.AddUnitLineItemFromCatalog
go

if OBJECT_ID('dbo.AltLineItemSummary') is not null
  drop procedure dbo.AltLineItemSummary
go

if OBJECT_ID('dbo.b_AltSummary') is not null
  drop procedure dbo.b_AltSummary
go

if OBJECT_ID('dbo.b_Bids') is not null
  drop procedure dbo.b_Bids
go

if OBJECT_ID('dbo.b_EditBid') is not null
  drop procedure dbo.b_EditBid
go

if OBJECT_ID('dbo.b_rptBidIntro') is not null
  drop procedure dbo.b_rptBidIntro
go

if OBJECT_ID('dbo.b_UnitSummaryExp') is not null
  drop procedure dbo.b_UnitSummaryExp
go

if OBJECT_ID('dbo.BuildingBOM') is not null
  drop procedure dbo.BuildingBOM
go

if OBJECT_ID('dbo.b_BldgBOM') is not null
  drop procedure dbo.b_BldgBOM
go

if OBJECT_ID('dbo.BuildingDetails') is not null
  drop procedure dbo.BuildingDetails
go

if OBJECT_ID('dbo.BuildingShipByDate') is not null
  drop procedure dbo.BuildingShipByDate
go

if OBJECT_ID('dbo.CatalogItem') is not null
  drop procedure dbo.CatalogItem
go

if OBJECT_ID('dbo.CatLineItems') is not null
  drop procedure dbo.CatLineItems
go

if OBJECT_ID('dbo.ClubHouseDetail') is not null
  drop procedure dbo.ClubHouseDetail
go

if OBJECT_ID('dbo.CopyBldg') is not null
  drop procedure dbo.CopyBldg
go

if OBJECT_ID('dbo.CopyUnitDef') is not null
  drop procedure dbo.CopyUnitDef
go

if OBJECT_ID('dbo.DeleteOrphans') is not null
  drop procedure dbo.DeleteOrphans
go

if OBJECT_ID('dbo.ELIbyType') is not null
  drop procedure dbo.ELIbyType
go

if OBJECT_ID('dbo.j_AddComment') is not null
  drop procedure dbo.j_AddComment
go

if OBJECT_ID('dbo.j_BldgBOM') is not null
  drop procedure dbo.j_BldgBOM
go

if OBJECT_ID('dbo.j_rptBOMIntro') is not null
  drop procedure dbo.j_rptBOMIntro
go

if OBJECT_ID('dbo.j_rptJobComments') is not null
  drop procedure dbo.j_rptJobComments
go

if OBJECT_ID('dbo.LineItemTBD') is not null
  drop procedure dbo.LineItemTBD
go

if OBJECT_ID('dbo.UnitBOM') is not null
  drop procedure dbo.UnitBOM
go

if OBJECT_ID('dbo.UnitLineItemID') is not null
  drop procedure dbo.UnitLineItemID
go

if OBJECT_ID('dbo.UnitRptDetail') is not null
  drop procedure dbo.UnitRptDetail
go

if OBJECT_ID('dbo.UnitsPerBuilding') is not null
  drop procedure dbo.UnitsPerBuilding
go

if OBJECT_ID('dbo.UnitSummaryLSP') is not null
  drop procedure dbo.UnitSummaryLSP
go

if OBJECT_ID('dbo.UpdateLineItemByPartNum') is not null
  drop procedure dbo.UpdateLineItemByPartNum
go

if OBJECT_ID('dbo.vw_LIDG') is not null
  drop view dbo.vw_LIDG
go

if OBJECT_ID('dbo.vw-UnitCount') is not null
  drop view dbo.[vw-UnitCount]
go

if OBJECT_ID('dbo.vw-LineItemQuan') is not null
  drop view dbo.[vw-LineItemQuan]
go

if OBJECT_ID('dbo.vw-BldgUnits') is not null
  drop view dbo.[vw-BldgUnits]
go

if OBJECT_ID('dbo.BldgBOM') is not null
  drop view dbo.BldgBOM
go



