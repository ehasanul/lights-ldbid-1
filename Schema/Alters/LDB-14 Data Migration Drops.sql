IF exists (Select * from sysobjects where id = object_id(N'[dbo].[_BidBulbItemQuan]'))
	Drop Table _BidBulbItemQuan
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[_BidBulbLineItemDef]'))
	Drop Table _BidBulbLineItemDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[_BldgDef]'))
	Drop Table _BldgDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[b_tmpUnitCount]'))
	Drop Table b_tmpUnitCount
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[bidComments]'))
	Drop Table bidComments
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[Bids]'))
	Drop Table Bids
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[BidUnitDef]'))
	Drop Table BidUnitDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[BidUnitItemQuan]'))
	Drop Table BidUnitItemQuan
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[BidUnitLineItemDef]'))
	Drop Table BidUnitLineItemDef
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[j_Bldg]'))
	Drop Table j_Bldg
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[j_Jobs]'))
	Drop Table j_Jobs
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[tmpBldgID]'))
	Drop Table tmpBldgID
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[tmpItemID]'))
	Drop Table tmpItemID
GO
IF exists (Select * from sysobjects where id = object_id(N'[dbo].[tmpUnitID]'))
	Drop Table tmpUnitID
GO