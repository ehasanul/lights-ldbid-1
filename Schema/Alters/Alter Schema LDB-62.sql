IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[UnitSummary]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.UnitSummary
END
GO
CREATE PROCEDURE UnitSummary
	@ProjectID int,
	@OrderBy varchar(15),
	@UnitType varchar(15)
AS
Select pn PartNumber, lo Location, ty Type, descr Description, Quantity, co Cost, se Sell, (Quantity * co) as [Total Cost],
	(Quantity * se) as [Total Sell], (Quantity * se - Quantity * co) As Net
from(
	select LineItem.Description Descr, LineItem.location lo, LineItem.PartNumber pn, LineItem.Cost co, LineItem.Margin ma, LineItem.Sell se,
	sum(LineItem.Quantity) AS Quantity, LineItem.TYPE ty
	from LineItem inner join Unit on LineItem.UnitID = Unit.UnitID
	where LineItem.ProjectID = @ProjectID AND LEFT(Unit.UnitType,1) = @UnitType
	group by LineItem.PartNumber, LineItem.Location, LineItem.Type, LineItem.Description, LineItem.Cost, LineItem.Margin, LineItem.Sell
) tempTable
order by Location
Go

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[rptQuantityDetails]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE dbo.rptQuantityDetails
END
go

Create PROCEDURE rptQuantityDetails
	@ProjectID int,
	@Filter Char
AS
select distinct UnitType, Unit.Description AS UnitDesc, Quantity, PartNumber, LineItem.Description Description, Location,
RT, Type, Cost, Margin, Sell from LineItem inner join Unit
on LineItem.UnitID = Unit.UnitID where lineitem.ProjectID = @ProjectID
and ((@Filter is null) OR (LEFT(UnitType, 1) = @Filter)) and Unit.BldgID is not null order by Location, UnitType
Go