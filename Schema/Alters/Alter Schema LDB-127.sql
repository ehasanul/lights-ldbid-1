--DO NOT APPLY THIS TO A PRODUCTION SYSTEM
--At the moment the catalog import system does not understanding key constraints
--For now this should be used to cleanup a development database.
--Cleanup any rows that will violate foreign keys
delete from Bulb2Fixture where BulbNum not in (select BulbNum from Bulbs);
delete from Comment where ProjectID not in (select ProjectID from Project);
delete from unit where ProjectID not in (select ProjectID from Project);
Update LineItem set BldgID = null where BldgID not in (select BldgID from Building);
Update Unit set BldgID = null where BldgID not in (select BldgID from Building);
delete from Alternates where OrgProductID not in(select ProductID from CatalogMain);
delete from Alternates where AltProductID not in(select ProductID from CatalogMain);
alter table Bulb2fixture alter column PartNumber nvarchar(255) not null;

--Put in place primary keys--
if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Primary key'
and table_name='Bulb2Fixture' and table_schema = 'dbo') begin

	delete from Bulb2Fixture where PartNumber is null or BulbNum is null;
	delete from Bulb2Fixture where ltrim(PartNumber) = '' or ltrim(bulbNum) = '';
	
	--There are currently duplicate key entries in the system. The system is ignoring them by only selecting the top item
	--when they do occur. To place the keys we need to remove the rest and save the first

	select PartNumber, BulbNum, count(*) Totalcount into #DuplicateBulbs
	from Bulb2Fixture
	group by PartNumber, BulbNum
	having count(*) > 1
	order by count(*) desc;

	--Save one of the duplicates to put back in the table later
	
	select distinct PartNumber, BulbNum, (select top 1 Quan from Bulb2Fixture where PartNumber = #DuplicateBulbs.PartNumber
		and BulbNum = #DuplicateBulbs.BulbNum order by Quan) Quan, (select top 1 Included from Bulb2Fixture 
		where PartNumber = #DuplicateBulbs.PartNumber and BulbNum = #DuplicateBulbs.BulbNum order by Quan) Included
	into #SavedDuplicates
	from #DuplicateBulbs;

	--Remove the duplicates
	delete Bulb2Fixture	from Bulb2Fixture, #DuplicateBulbs where Bulb2Fixture.PartNumber = #DuplicateBulbs.PartNumber 
		and Bulb2Fixture.BulbNum = #DuplicateBulbs.BulbNum;

	--put the saved entry back
	insert Bulb2Fixture select * from #SavedDuplicates;

	drop table #DuplicateBulbs;
	drop table #SavedDuplicates;

	alter table Bulb2Fixture alter column BulbNum char(25) not null;
end
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Primary key'
and table_name='Bulb2Fixture' and table_schema = 'dbo') begin
	alter table Bulb2Fixture add constraint Bulb2Fixture_PK primary key clustered (BulbNum, PartNumber);
end
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Primary key' 
	and table_name='CatalogMain' and table_schema = 'dbo') begin
	if exists (select name from sysindexes where name = 'IX_CatalogMain')
		drop index CatalogMain.IX_CatalogMain
	alter table CatalogMain alter column ProductID nvarchar(255) not null
end
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Primary key' 
	and table_name='CatalogMain' and table_schema = 'dbo') begin

	--we have duplicates in this table so we need to do the same editing as we did in the bulb table
	--Find duplicate keys and save them

	select ProductID, count(*) Totalcount into #SavedDuplicates
	from CatalogMain
	group by ProductID
	having count(*) > 1
	order by count(*) desc

	--Save one of the duplicates to put back in the table later
	
	select distinct ProductID, 
		(select top 1 Description, VendorCode, VendorPartNo, AlternatePartNum, AverageCost, StandardCost, AvailableUnits, 
		OnOrderUnits, CommittedUnits, LeadTime, AlternateVendor from CatalogMain where ProductID = #SavedDuplicates.ProductID Order By Description)
	into #DuplicateCatItems
	from #SavedDuplicates

	--Remove the duplicates
	delete CatalogMain from CatalogMain, #SavedDuplicates where CatalogMain.ProductID = #SavedDuplicates.ProductID;

	--put the saved entry back
	insert CatalogMain select * from #DuplicateCatItems;

	alter table CatalogMain add constraint CatalogMain_PK primary key nonclustered (ProductID);

	drop table #SavedDuplicates;
	drop table #DuplicateCatItems;
end
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Primary key' 
	and table_name='ChangeOrder' and table_schema = 'dbo') 
	alter table ChangeOrder add constraint ChangeOrder_PK primary key nonclustered (COID);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Primary key' 
	and table_name='LineItem' and table_schema = 'dbo')
	alter table LineItem add constraint LineItem_PK primary key nonclustered (ItemID );
go

--Put in foreign keys
if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Alternates' and table_schema = 'dbo' and constraint_name = 'Alternates_Project_FK') begin
	alter table Alternates add constraint Alternates_Project_FK foreign key
	(ProjectID)
	references Project
	(ProjectID)
	on delete cascade;
end
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Alternates' and table_schema = 'dbo' and constraint_name = 'Alternates_CatalogMain_Org_FK')
	alter table Alternates add constraint Alternates_CatalogMain_Org_FK foreign key
	(OrgProductID)
	references CatalogMain
	(ProductID);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Alternates' and table_schema = 'dbo' and constraint_name = 'Alternates_AltCatalogMain_FK')
	alter table Alternates add constraint Alternates_AltCatalogMain_FK foreign key
	(AltProductID)
	references CatalogMain
	(ProductID);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Building' and table_schema = 'dbo' and constraint_name = 'Building_Project_FK')
	alter table Building add constraint Building_Project_FK foreign key
	(ProjectID)
	references Project
	(ProjectID)
	on delete cascade;
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Bulb2Fixture' and table_schema = 'dbo' and constraint_name = 'Bulb2Fixture_Bulbs_FK')
	alter table Bulb2Fixture add constraint Bulb2Fixture_Bulbs_FK foreign key
	(BulbNum)
	references Bulbs
	(BulbNum);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='ChangeOrder' and table_schema = 'dbo' and constraint_name = 'ChangeOrder_Project_FK')
	alter table ChangeOrder add constraint ChangeOrder_Project_FK foreign key
	(ProjectID)
	references Project
	(ProjectID)
	on delete cascade;
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Comment' and table_schema = 'dbo' and constraint_name = 'Comment_Project_FK')
	alter table Comment add constraint Comment_Project_FK foreign key
	(ProjectID)
	references Project
	(ProjectID)
	on delete cascade;
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='LineItem' and table_schema = 'dbo' and constraint_name = 'LineItem_Project_FK')
	alter table LineItem add constraint LineItem_Project_FK foreign key
	(ProjectID)
	references Project
	(ProjectID);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='LineItem' and table_schema = 'dbo' and constraint_name = 'LineItem_Building_FK')
	alter table LineItem add constraint LineItem_Building_FK foreign key
	(BldgID)
	references Building
	(BldgID);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='LineItem' and table_schema = 'dbo' and constraint_name = 'LineItem_Unit_FK')
	alter table LineItem add constraint LineItem_Unit_FK foreign key
	(UnitID)
	references Unit
	(UnitID)
	on delete cascade;
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Project' and table_schema = 'dbo' and constraint_name = 'Project_Clients_FK')
	alter table Project add constraint Project_Clients_FK foreign key
	(ClientID)
	references Clients
	(ClientID);
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Unit' and table_schema = 'dbo' and constraint_name = 'Unit_Project_FK')
	alter table Unit add constraint Unit_Project_FK foreign key
	(ProjectID)
	references Project
	(ProjectID)
	on delete cascade;
go

if not exists (select constraint_name from information_schema.table_constraints where constraint_type = 'Foreign key'
	and table_name='Unit' and table_schema = 'dbo' and constraint_name = 'Unit_Building_FK')
	alter table Unit add constraint Unit_Building_FK foreign key
	(BldgID)
	references Building
	(BldgID);
go
