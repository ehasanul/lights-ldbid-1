Imports System.Data
Imports System.Data.SqlClient
Public Class frmj_PartsSearch
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim blnStartParts As Boolean
    Private projectType As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgPartsDetails As System.Windows.Forms.DataGrid
    Friend WithEvents cboParts As System.Windows.Forms.ComboBox
    Friend WithEvents rbnN As System.Windows.Forms.RadioButton
    Friend WithEvents rbnS As System.Windows.Forms.RadioButton
    Friend WithEvents rbnU As System.Windows.Forms.RadioButton
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents dgsJPartsSearch As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgPSPartNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgPSQuantity As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgPSBidID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgPSDexcription As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgPSShip As System.Windows.Forms.DataGridBoolColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgPartsDetails = New System.Windows.Forms.DataGrid()
        Me.dgsJPartsSearch = New System.Windows.Forms.DataGridTableStyle()
        Me.dgPSPartNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgPSQuantity = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgPSBidID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgPSDexcription = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgPSShip = New System.Windows.Forms.DataGridBoolColumn()
        Me.cboParts = New System.Windows.Forms.ComboBox()
        Me.rbnN = New System.Windows.Forms.RadioButton()
        Me.rbnS = New System.Windows.Forms.RadioButton()
        Me.rbnU = New System.Windows.Forms.RadioButton()
        Me.btnLoad = New System.Windows.Forms.Button()
        CType(Me.dgPartsDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(0, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(228, 20)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Select A Part From This List"
        '
        'dgPartsDetails
        '
        Me.dgPartsDetails.DataMember = ""
        Me.dgPartsDetails.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgPartsDetails.Location = New System.Drawing.Point(4, 72)
        Me.dgPartsDetails.Name = "dgPartsDetails"
        Me.dgPartsDetails.Size = New System.Drawing.Size(592, 260)
        Me.dgPartsDetails.TabIndex = 4
        Me.dgPartsDetails.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgsJPartsSearch})
        '
        'dgsJPartsSearch
        '
        Me.dgsJPartsSearch.DataGrid = Me.dgPartsDetails
        Me.dgsJPartsSearch.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgPSPartNumber, Me.dgPSQuantity, Me.dgPSBidID, Me.dgPSDexcription, Me.dgPSShip})
        Me.dgsJPartsSearch.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgsJPartsSearch.MappingName = "LineItems"
        '
        'dgPSPartNumber
        '
        Me.dgPSPartNumber.Format = ""
        Me.dgPSPartNumber.FormatInfo = Nothing
        Me.dgPSPartNumber.HeaderText = "Part Number"
        Me.dgPSPartNumber.MappingName = "PartNumber"
        Me.dgPSPartNumber.Width = 125
        '
        'dgPSQuantity
        '
        Me.dgPSQuantity.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgPSQuantity.Format = ""
        Me.dgPSQuantity.FormatInfo = Nothing
        Me.dgPSQuantity.HeaderText = "Quan"
        Me.dgPSQuantity.MappingName = "Quantity"
        Me.dgPSQuantity.Width = 75
        '
        'dgPSBidID
        '
        Me.dgPSBidID.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgPSBidID.Format = ""
        Me.dgPSBidID.FormatInfo = Nothing
        Me.dgPSBidID.HeaderText = "Job ID"
        Me.dgPSBidID.MappingName = "LDID"
        Me.dgPSBidID.NullText = ""
        Me.dgPSBidID.Width = 50
        '
        'dgPSDexcription
        '
        Me.dgPSDexcription.Format = ""
        Me.dgPSDexcription.FormatInfo = Nothing
        Me.dgPSDexcription.HeaderText = "Job Description"
        Me.dgPSDexcription.MappingName = "Description"
        Me.dgPSDexcription.NullText = ""
        Me.dgPSDexcription.Width = 225
        '
        'dgPSShip
        '
        Me.dgPSShip.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgPSShip.HeaderText = "Shipped"
        Me.dgPSShip.MappingName = "shipped"
        Me.dgPSShip.NullText = ""
        Me.dgPSShip.Width = 50
        '
        'cboParts
        '
        Me.cboParts.Location = New System.Drawing.Point(4, 40)
        Me.cboParts.Name = "cboParts"
        Me.cboParts.Size = New System.Drawing.Size(232, 21)
        Me.cboParts.TabIndex = 3
        '
        'rbnN
        '
        Me.rbnN.Checked = True
        Me.rbnN.Location = New System.Drawing.Point(248, 36)
        Me.rbnN.Name = "rbnN"
        Me.rbnN.Size = New System.Drawing.Size(92, 24)
        Me.rbnN.TabIndex = 6
        Me.rbnN.TabStop = True
        Me.rbnN.Text = "Not Shipped"
        '
        'rbnS
        '
        Me.rbnS.Location = New System.Drawing.Point(344, 36)
        Me.rbnS.Name = "rbnS"
        Me.rbnS.Size = New System.Drawing.Size(80, 24)
        Me.rbnS.TabIndex = 7
        Me.rbnS.Text = "Shipped"
        '
        'rbnU
        '
        Me.rbnU.Location = New System.Drawing.Point(424, 36)
        Me.rbnU.Name = "rbnU"
        Me.rbnU.Size = New System.Drawing.Size(52, 24)
        Me.rbnU.TabIndex = 8
        Me.rbnU.Text = "All"
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(496, 36)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(76, 20)
        Me.btnLoad.TabIndex = 9
        Me.btnLoad.Text = "Load Grid"
        '
        'frmj_PartsSearch
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(604, 362)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.rbnU)
        Me.Controls.Add(Me.rbnS)
        Me.Controls.Add(Me.rbnN)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgPartsDetails)
        Me.Controls.Add(Me.cboParts)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmj_PartsSearch"
        Me.Text = "Parts Search From Jobs"
        CType(Me.dgPartsDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmj_PartsSearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        blnStartParts = True
        If frmMain.comingFromVerbal Then
            projectType = "verbal"
        Else
            projectType = "job"
        End If
        LoadParts()
    End Sub

    Private Sub LoadParts()
        'frmBid
        cnn.ConnectionString = frmMain.strConnect
        Dim strParts As String
        strParts = "SELECT DISTINCT PartNumber FROM LineItem where projectid in (select projectid from project where " + _
            "ProjectType = '" + projectType + "' ) ORDER BY partnumber"
        Dim dsParts As New DataSet
        dsParts = ExecuteSQL(strParts, cnn, "Parts")
        cboParts.DataSource = dsParts.Tables("Parts")
        cboParts.DisplayMember = "PartNumber"
        blnStartParts = False
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        cnn.ConnectionString = frmMain.strConnect
        Dim strSQL As String
        strSQL = "PartsFind"

        Dim cmdPartsDetails As New SqlCommand(strSQL, cnn)
        cmdPartsDetails.CommandType = CommandType.StoredProcedure

        Dim prmPartNumber As SqlParameter = cmdPartsDetails.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
        prmPartNumber.Value = cboParts.Text
        Dim prmCriteria As SqlParameter = cmdPartsDetails.Parameters.Add("@Criteria", SqlDbType.Char, 1)
        If rbnN.Checked Then
            prmCriteria.Value = "N"
        ElseIf rbnS.Checked Then
            prmCriteria.Value = "S"
        ElseIf rbnU.Checked Then
            prmCriteria.Value = "U"
        End If
        Dim prmProjectType As SqlParameter = cmdPartsDetails.Parameters.Add("@ProjectType", SqlDbType.VarChar, 15)
        prmProjectType.Value = projectType

        Dim daPartsDetails As New SqlDataAdapter(cmdPartsDetails)
        Dim dsPartsDetails As New DataSet
        Try
            daPartsDetails.Fill(dsPartsDetails, "LineItems")
            dgPartsDetails.DataSource = dsPartsDetails.Tables("LineItems")
        Catch ex As Exception
            MsgBox("Error in LoadAltDB")
        End Try
    End Sub
End Class
