Imports System.Data
Imports System.Data.SqlClient

Public Class frmUnit
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim strFunction As String
    Dim strOldUnitDesc As String
    'Dim intNewUnitID As Integer
    Dim OldUnitType As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents txtBUnitType As System.Windows.Forms.TextBox
    Friend WithEvents txtBUnitDescription As System.Windows.Forms.TextBox
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitType As System.Windows.Forms.Label
    Friend WithEvents lblUnitDesc As System.Windows.Forms.Label
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.txtBUnitType = New System.Windows.Forms.TextBox
        Me.txtBUnitDescription = New System.Windows.Forms.TextBox
        Me.lblUnitType = New System.Windows.Forms.Label
        Me.lblUnitDesc = New System.Windows.Forms.Label
        Me.lblUnit = New System.Windows.Forms.Label
        Me.btnDelete = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboUnit
        '
        Me.cboUnit.Location = New System.Drawing.Point(16, 28)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(180, 21)
        Me.cboUnit.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(288, 24)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(52, 20)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Save"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(340, 24)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(52, 20)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Add"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Copy"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Edit"
        '
        'txtBUnitType
        '
        Me.txtBUnitType.Location = New System.Drawing.Point(12, 104)
        Me.txtBUnitType.Name = "txtBUnitType"
        Me.txtBUnitType.Size = New System.Drawing.Size(136, 20)
        Me.txtBUnitType.TabIndex = 3
        Me.txtBUnitType.Text = ""
        '
        'txtBUnitDescription
        '
        Me.txtBUnitDescription.Location = New System.Drawing.Point(176, 104)
        Me.txtBUnitDescription.Name = "txtBUnitDescription"
        Me.txtBUnitDescription.Size = New System.Drawing.Size(216, 20)
        Me.txtBUnitDescription.TabIndex = 4
        Me.txtBUnitDescription.Text = ""
        '
        'lblUnitType
        '
        Me.lblUnitType.Location = New System.Drawing.Point(16, 80)
        Me.lblUnitType.Name = "lblUnitType"
        Me.lblUnitType.Size = New System.Drawing.Size(108, 16)
        Me.lblUnitType.TabIndex = 5
        Me.lblUnitType.Text = "Unit Type"
        '
        'lblUnitDesc
        '
        Me.lblUnitDesc.Location = New System.Drawing.Point(176, 84)
        Me.lblUnitDesc.Name = "lblUnitDesc"
        Me.lblUnitDesc.Size = New System.Drawing.Size(156, 20)
        Me.lblUnitDesc.TabIndex = 6
        Me.lblUnitDesc.Text = "Unit Description"
        '
        'lblUnit
        '
        Me.lblUnit.Location = New System.Drawing.Point(20, 8)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(100, 20)
        Me.lblUnit.TabIndex = 7
        Me.lblUnit.Text = "Units In Bid"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(236, 24)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(52, 20)
        Me.btnDelete.TabIndex = 8
        Me.btnDelete.Text = "Delete"
        '
        'frmUnit
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(400, 181)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblUnit)
        Me.Controls.Add(Me.lblUnitDesc)
        Me.Controls.Add(Me.lblUnitType)
        Me.Controls.Add(Me.txtBUnitDescription)
        Me.Controls.Add(Me.txtBUnitType)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cboUnit)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmUnit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add / Edit Units"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadUnits()
        If Not cnn.State = ConnectionState.Open Then
            cnn.ConnectionString = frmMain.strConnect
        End If
        Dim cmdLoadUnits As New SqlCommand("Units", cnn)
        cmdLoadUnits.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadUnits.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daUnits As New SqlDataAdapter(cmdLoadUnits)
        Dim dsUnits As New DataSet
        daUnits.Fill(dsUnits, "Units")
        cboUnit.DataSource = dsUnits.Tables("Units")
        cboUnit.DisplayMember = "U"
        cboUnit.ValueMember = "UnitType"
    End Sub

    Private Sub AddNewUnit()
        cnn.Close()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAddNewUnit As New SqlCommand("AddUnit", cnn)
        cmdAddNewUnit.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAddNewUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmUnitType As SqlParameter = cmdAddNewUnit.Parameters.Add("@UnitType", SqlDbType.Char, 10)
        prmUnitType.Value = txtBUnitType.Text

        Dim prmDescription As SqlParameter = cmdAddNewUnit.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtBUnitDescription.Text

        Dim prmBldgID As SqlParameter = cmdAddNewUnit.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = DBNull.Value

        Dim prmUnitName As SqlParameter = cmdAddNewUnit.Parameters.Add("@UnitName", SqlDbType.NVarChar, 50)
        prmUnitName.Value = DBNull.Value

        Dim prmNewUnitID As SqlParameter = cmdAddNewUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
        prmNewUnitID.Direction = ParameterDirection.Output

        Dim intUnitID As Integer
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            cmdAddNewUnit.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If

        End Try
        ' Gets New Unit ID From @@Identity
        intUnitID = System.Convert.ToInt32(prmNewUnitID.Value)
        frmMain.intUnitID = intUnitID
    End Sub

    Private Sub CopyUnit()
        If txtBUnitType.Text = "" Then
            MsgBox("You Must Enter A Unit Type!")
            Exit Sub
        End If

        If txtBUnitDescription.Text = "" Then
            MsgBox("You Must Enter A Unit Description!")
            Exit Sub
        End If
       
        Dim strLineItems As String

        strLineItems = "Insert into LineItem(ProjectID, UnitID, RecordSource, PartNumber, RT, Description, Cost, Margin, " + _
            "Sell, Quantity, Location, Type, Shipped, ShipDate, ShipQuan, QuanBO) Select distinct " + frmMain.intBidID.ToString + _
            ", " + frmMain.intUnitID.ToString + ", RecordSource, PartNumber, RT, Description, Cost, Margin, Sell, Quantity, Location, " + _
            "Type, Shipped, ShipDate, ShipQuan, QuanBO from lineitem where unitid in (select unitid from unit where unittype = '" + OldUnitType + "')"
        ExecuteQueryText(strLineItems, cnn)
    End Sub

    Private Sub frmUnit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HideAll()
    End Sub

    Private Sub HideAll()
        cboUnit.Visible = False
        txtBUnitType.Visible = False
        txtBUnitDescription.Visible = False
        lblUnit.Visible = False
        lblUnitType.Visible = False
        lblUnitDesc.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False
        btnDelete.Visible = False
    End Sub

    Private Sub ClearAll()
        txtBUnitType.Text = ""
        txtBUnitDescription.Text = ""
    End Sub

    Private Sub ShowAll()
        txtBUnitType.Visible = True
        txtBUnitDescription.Visible = True
        lblUnitType.Visible = True
        lblUnitDesc.Visible = True
        btnSave.Visible = True
        btnCancel.Visible = True
    End Sub

    Private Sub LoadUnitDetails()
        'Bldg
        cnn.ConnectionString = frmMain.strConnect

        Dim strUnitInfo As String
        strUnitInfo = "SELECT UnitType, Description FROM Unit WHERE UnitType = '"
        strUnitInfo = strUnitInfo & cboUnit.SelectedValue.ToString + "'"
        Dim cmdUnitInfo As New SqlCommand(strUnitInfo, cnn)
        cmdUnitInfo.CommandType = CommandType.Text
        Dim drUnitInfo As SqlDataReader
        cnn.Open()
        drUnitInfo = cmdUnitInfo.ExecuteReader
        drUnitInfo.Read()
        txtBUnitType.Text = drUnitInfo.Item("UnitType").ToString
        txtBUnitDescription.Text = drUnitInfo.Item("Description").ToString
        strOldUnitDesc = txtBUnitDescription.Text
        drUnitInfo.Close()
        cnn.Close()
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        HideAll()
        EnableMenu()
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        strFunction = "New"
        ClearAll()
        ShowAll()
        DisableMenu()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        strFunction = "Copy"
        ClearAll()
        LoadUnits()
        cboUnit.Visible = True
        lblUnit.Visible = True
        DisableMenu()
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        strFunction = "Edit"
        ClearAll()
        LoadUnits()
        cboUnit.Visible = True
        lblUnit.Visible = True
        txtBUnitType.Visible = False
        lblUnitType.Visible = False
        DisableMenu()
    End Sub

    Private Sub DisableMenu()
        MenuItem1.Visible = False
        MenuItem2.Visible = False
        MenuItem3.Visible = False
    End Sub

    Private Sub EnableMenu()
        MenuItem1.Visible = True
        MenuItem2.Visible = True
        MenuItem3.Visible = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strSample As String
        strSample = txtBUnitType.Text.Substring(0, 1)
        If (System.Convert.ToDouble(strSample.IndexOfAny("UCXSA".ToCharArray)) = -1) Then
            MsgBox("The Unit Type Must Begin With either U,C,X,S or A")
            Exit Sub
        End If
        Dim cnn3 As New SqlConnection
        cnn3.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "SELECT * FROM Unit WHERE ProjectID = " & frmMain.intBidID
        strSqlCommand = strSqlCommand & " AND UnitType = '" & txtBUnitType.Text & "'"
        Dim cmdUnits As New SqlCommand(strSqlCommand, cnn3)
        Dim drUnits As SqlDataReader

        Select Case strFunction
            Case "New"
                cnn3.Open()
                drUnits = cmdUnits.ExecuteReader
                If drUnits.HasRows Then
                    MsgBox("You must specify a Unit Type Unique to This Bid!")
                    drUnits.Close()
                    cnn3.Close()
                    Exit Sub
                Else
                    AddNewUnit()
                    drUnits.Close()
                    cnn3.Close()
                End If
                
            Case "Edit"
                EditUnit()
            Case "Copy"
                cnn3.Open()
                drUnits = cmdUnits.ExecuteReader
                If drUnits.HasRows Then
                    MsgBox("You must specify a Unit Type Unique to This Bid!")
                    Exit Sub
                Else
                    AddNewUnit()
                    OldUnitType = cboUnit.SelectedValue.ToString
                    'MsgBox(intOldUnitID.ToString)
                    CopyUnit()
                End If
                drUnits.Close()
                cnn3.Close()
        End Select
        EnableMenu()
        HideAll()
        ClearAll()
        frmBid.blnUnitReturn = True
    End Sub

    Private Sub EditUnit()

        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "UPDATE Unit SET "
        'strSqlCommand = strSqlCommand & Trim(txtBUnitType.Text)
        strSqlCommand = strSqlCommand & "Description = '" & Trim(txtBUnitDescription.Text)
        strSqlCommand = strSqlCommand & "' WHERE UnitID = " & cboUnit.SelectedValue.ToString
        Dim cmdItems As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdItems.ExecuteNonQuery()
            frmBid.blnUnitCopy = True
            frmBid.strActiveUnit = txtBUnitType.Text
        Catch ex As Exception
            MsgBox("There was an error modifying this record!")
        Finally
            cnn.Close()
        End Try

    End Sub

    Private Sub DeleteUnits()
        If cnn.State <> ConnectionState.Closed Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "DELETE FROM LineItem WHERE LineItem.UnitId in (select unitid from unit where UnitType = '" + cboUnit.SelectedValue.ToString + "' and ProjectID = " + _
            frmMain.intBidID.ToString + ")"
        Dim cmdDeleteUnit As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdDeleteUnit.ExecuteNonQuery()
            frmBid.blnUnitCopy = False
        Catch ex As Exception
            MsgBox("There was an error Deleting The Quantities: " + ex.ToString)
        Finally
            cnn.Close()
        End Try

        strSqlCommand = "DELETE FROM Unit WHERE UnitType = '" & cboUnit.SelectedValue.ToString + "' and ProjectID = " + _
            frmMain.intBidID.ToString
        Dim cmdDeleteUnitDef As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdDeleteUnitDef.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error Deleting the Unit: " + ex.ToString)
        Finally
            cnn.Close()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteUnits()
        EnableMenu()
        HideAll()
        ClearAll()
        frmBid.blnUnitReturn = True
        frmBid.blnUnitDelete = True
    End Sub

    Private Sub cboUnit_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectionChangeCommitted
        If strFunction = "Edit" Then
            btnDelete.Visible = True
        Else
            lblUnitType.Visible = True
            txtBUnitType.Visible = True
        End If
        btnSave.Visible = True
        btnCancel.Visible = True
        lblUnitDesc.Visible = True
        txtBUnitDescription.Visible = True
        LoadUnitDetails()
    End Sub
End Class
