Imports System.Data
Imports System.Data.SqlClient
Public Class frmj_LI
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim strFunction As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents txtPartNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtMargin As System.Windows.Forms.TextBox
    Friend WithEvents txtRecordSource As System.Windows.Forms.TextBox
    Friend WithEvents txtRT As System.Windows.Forms.TextBox
    Friend WithEvents txtSell As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtShipDate As System.Windows.Forms.TextBox
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents txtQuanBO As System.Windows.Forms.TextBox
    Friend WithEvents cbxShipped As System.Windows.Forms.CheckBox
    Friend WithEvents txtShipQuan As System.Windows.Forms.TextBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuEdit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents rbMain As System.Windows.Forms.RadioButton
    Friend WithEvents rbSO As System.Windows.Forms.RadioButton
    Friend WithEvents cboCatalogMain As System.Windows.Forms.ComboBox
    Friend WithEvents mnuAdd As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtQuanBO = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.cbxShipped = New System.Windows.Forms.CheckBox
        Me.txtQuantity = New System.Windows.Forms.TextBox
        Me.txtPartNumber = New System.Windows.Forms.TextBox
        Me.txtShipQuan = New System.Windows.Forms.TextBox
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.txtLocation = New System.Windows.Forms.TextBox
        Me.txtMargin = New System.Windows.Forms.TextBox
        Me.txtRecordSource = New System.Windows.Forms.TextBox
        Me.txtRT = New System.Windows.Forms.TextBox
        Me.txtSell = New System.Windows.Forms.TextBox
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtShipDate = New System.Windows.Forms.TextBox
        Me.txtType = New System.Windows.Forms.TextBox
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mnuEdit = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuAdd = New System.Windows.Forms.MenuItem
        Me.rbMain = New System.Windows.Forms.RadioButton
        Me.rbSO = New System.Windows.Forms.RadioButton
        Me.cboCatalogMain = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'txtQuanBO
        '
        Me.txtQuanBO.Location = New System.Drawing.Point(256, 148)
        Me.txtQuanBO.Name = "txtQuanBO"
        Me.txtQuanBO.Size = New System.Drawing.Size(44, 20)
        Me.txtQuanBO.TabIndex = 13
        Me.txtQuanBO.Text = ""
        Me.txtQuanBO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtQuanBO.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(144, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 16)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Record Source"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Part Number"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(28, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "RT"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(208, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 16)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Description"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(420, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(28, 16)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Cost"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(460, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 16)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Margin"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(552, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(24, 16)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Sell"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(300, 84)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 16)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Quantity"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(428, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(84, 16)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Location"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(64, 84)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(32, 16)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Type"
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(128, 132)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Ship Date"
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(188, 132)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(68, 16)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Shipped Quan"
        Me.Label12.Visible = False
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(256, 132)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(48, 16)
        Me.Label13.TabIndex = 25
        Me.Label13.Text = "Quan BO"
        Me.Label13.Visible = False
        '
        'cbxShipped
        '
        Me.cbxShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxShipped.Location = New System.Drawing.Point(20, 148)
        Me.cbxShipped.Name = "cbxShipped"
        Me.cbxShipped.Size = New System.Drawing.Size(68, 20)
        Me.cbxShipped.TabIndex = 10
        Me.cbxShipped.Text = "Shipped"
        '
        'txtQuantity
        '
        Me.txtQuantity.Location = New System.Drawing.Point(304, 100)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(32, 20)
        Me.txtQuantity.TabIndex = 6
        Me.txtQuantity.Text = ""
        Me.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPartNumber
        '
        Me.txtPartNumber.Location = New System.Drawing.Point(20, 56)
        Me.txtPartNumber.MaxLength = 25
        Me.txtPartNumber.Name = "txtPartNumber"
        Me.txtPartNumber.Size = New System.Drawing.Size(184, 20)
        Me.txtPartNumber.TabIndex = 0
        Me.txtPartNumber.Text = ""
        '
        'txtShipQuan
        '
        Me.txtShipQuan.Location = New System.Drawing.Point(200, 148)
        Me.txtShipQuan.Name = "txtShipQuan"
        Me.txtShipQuan.Size = New System.Drawing.Size(40, 20)
        Me.txtShipQuan.TabIndex = 12
        Me.txtShipQuan.Text = ""
        Me.txtShipQuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtShipQuan.Visible = False
        '
        'txtCost
        '
        Me.txtCost.Location = New System.Drawing.Point(352, 100)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.TabIndex = 7
        Me.txtCost.Text = ""
        Me.txtCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLocation
        '
        Me.txtLocation.Location = New System.Drawing.Point(428, 56)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(148, 20)
        Me.txtLocation.TabIndex = 2
        Me.txtLocation.Text = ""
        '
        'txtMargin
        '
        Me.txtMargin.Location = New System.Drawing.Point(452, 100)
        Me.txtMargin.Name = "txtMargin"
        Me.txtMargin.Size = New System.Drawing.Size(48, 20)
        Me.txtMargin.TabIndex = 8
        Me.txtMargin.Text = ""
        Me.txtMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRecordSource
        '
        Me.txtRecordSource.Location = New System.Drawing.Point(144, 100)
        Me.txtRecordSource.Name = "txtRecordSource"
        Me.txtRecordSource.Size = New System.Drawing.Size(148, 20)
        Me.txtRecordSource.TabIndex = 5
        Me.txtRecordSource.Text = ""
        '
        'txtRT
        '
        Me.txtRT.Location = New System.Drawing.Point(20, 100)
        Me.txtRT.Name = "txtRT"
        Me.txtRT.Size = New System.Drawing.Size(36, 20)
        Me.txtRT.TabIndex = 3
        Me.txtRT.Text = ""
        Me.txtRT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSell
        '
        Me.txtSell.Location = New System.Drawing.Point(504, 100)
        Me.txtSell.Name = "txtSell"
        Me.txtSell.Size = New System.Drawing.Size(68, 20)
        Me.txtSell.TabIndex = 9
        Me.txtSell.Text = ""
        Me.txtSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(208, 56)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(216, 20)
        Me.txtDescription.TabIndex = 1
        Me.txtDescription.Text = ""
        '
        'txtShipDate
        '
        Me.txtShipDate.Location = New System.Drawing.Point(92, 148)
        Me.txtShipDate.Name = "txtShipDate"
        Me.txtShipDate.Size = New System.Drawing.Size(88, 20)
        Me.txtShipDate.TabIndex = 11
        Me.txtShipDate.Text = ""
        Me.txtShipDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtType
        '
        Me.txtType.Location = New System.Drawing.Point(60, 100)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(84, 20)
        Me.txtType.TabIndex = 4
        Me.txtType.Text = ""
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(440, 152)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(44, 16)
        Me.btnDelete.TabIndex = 14
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(528, 152)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(44, 16)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(484, 152)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(44, 16)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "Save"
        Me.btnSave.Visible = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEdit, Me.mnuCopy, Me.mnuAdd})
        '
        'mnuEdit
        '
        Me.mnuEdit.Index = 0
        Me.mnuEdit.Text = "Edit"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 1
        Me.mnuCopy.Text = "Copy"
        '
        'mnuAdd
        '
        Me.mnuAdd.Index = 2
        Me.mnuAdd.Text = "Add"
        '
        'rbMain
        '
        Me.rbMain.Checked = True
        Me.rbMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbMain.Location = New System.Drawing.Point(128, 8)
        Me.rbMain.Name = "rbMain"
        Me.rbMain.Size = New System.Drawing.Size(56, 20)
        Me.rbMain.TabIndex = 26
        Me.rbMain.TabStop = True
        Me.rbMain.Text = "Catalog"
        Me.rbMain.Visible = False
        '
        'rbSO
        '
        Me.rbSO.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbSO.Location = New System.Drawing.Point(188, 6)
        Me.rbSO.Name = "rbSO"
        Me.rbSO.Size = New System.Drawing.Size(80, 24)
        Me.rbSO.TabIndex = 27
        Me.rbSO.Text = "Special Order"
        Me.rbSO.Visible = False
        '
        'cboCatalogMain
        '
        Me.cboCatalogMain.Location = New System.Drawing.Point(276, 8)
        Me.cboCatalogMain.Name = "cboCatalogMain"
        Me.cboCatalogMain.Size = New System.Drawing.Size(300, 21)
        Me.cboCatalogMain.TabIndex = 28
        Me.cboCatalogMain.Visible = False
        '
        'frmj_LI
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(600, 197)
        Me.Controls.Add(Me.cboCatalogMain)
        Me.Controls.Add(Me.rbSO)
        Me.Controls.Add(Me.rbMain)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cbxShipped)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtQuanBO)
        Me.Controls.Add(Me.txtQuantity)
        Me.Controls.Add(Me.txtPartNumber)
        Me.Controls.Add(Me.txtShipQuan)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.txtMargin)
        Me.Controls.Add(Me.txtRecordSource)
        Me.Controls.Add(Me.txtRT)
        Me.Controls.Add(Me.txtSell)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.txtShipDate)
        Me.Controls.Add(Me.txtType)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmj_LI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Line Item"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub LoadDetails()
        'Bldg
        cnn.ConnectionString = frmMain.strConnect

        Dim strLIInfo As String
        strLIInfo = "SELECT RecordSource, PartNumber, RT, Description, Cost, Margin, Sell, " & _
        "Quantity, Location, Type, Shipped, ShipDate, ShipQuan, QuanBO FROM LineItem WHERE ItemID = "
        strLIInfo = strLIInfo & frmJob.intItemID.ToString
        Dim cmdLIInfo As New SqlCommand(strLIInfo, cnn)
        cmdLIInfo.CommandType = CommandType.Text
        Dim drLIInfo As SqlDataReader
        cnn.Open()
        drLIInfo = cmdLIInfo.ExecuteReader
        drLIInfo.Read()
        txtRecordSource.Text = drLIInfo.Item("REcordSource").ToString
        txtPartNumber.Text = drLIInfo.Item("PartNumber").ToString
        txtRT.Text = drLIInfo.Item("RT").ToString
        txtDescription.Text = drLIInfo.Item("Description").ToString
        txtCost.Text = Format(drLIInfo.Item("Cost"), "C")
        txtMargin.Text = drLIInfo.Item("Margin").ToString
        txtSell.Text = Format(drLIInfo.Item("Sell"), "C")
        txtQuantity.Text = drLIInfo.Item("Quantity").ToString
        txtLocation.Text = drLIInfo.Item("Location").ToString
        txtType.Text = drLIInfo.Item("Type").ToString
        cbxShipped.Checked = System.Convert.ToBoolean(drLIInfo.Item("Shipped"))
        txtShipDate.Text = Format(drLIInfo.Item("ShipDate"), "d")
        txtShipQuan.Text = drLIInfo.Item("ShipQuan").ToString
        txtQuanBO.Text = drLIInfo.Item("QuanBO").ToString
        drLIInfo.Close()
        cnn.Close()
    End Sub

    Private Sub ClearDetails()
        txtRecordSource.Text = ""
        txtPartNumber.Text = ""
        txtRT.Text = ""
        txtDescription.Text = ""
        txtCost.Text = "0"
        txtMargin.Text = "0"
        txtSell.Text = "0"
        txtQuantity.Text = "0"
        txtLocation.Text = ""
        txtType.Text = ""
        cbxShipped.Checked = False
        'txtShipDate.Text = Format(drLIInfo.Item("ShipDate"), "d")
        txtShipQuan.Text = "0"
        txtQuanBO.Text = "0"
    End Sub

    Private Sub EditLI()
        'MsgBox("Start Edit and Pass To CO Form")
        'Dim CO As New frmCO
        'CO.ShowDialog()
        'MsgBox("Return From From CO")
        'Me.Cursor = Cursors.Default
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdEditLI As New SqlCommand("EditLineItem", cnn)
        cmdEditLI.CommandType = CommandType.StoredProcedure

        Dim prmItemID As SqlParameter = cmdEditLI.Parameters.Add("@ItemID", SqlDbType.Int)
        prmItemID.Value = frmJob.intItemID

        Dim prmRecordSource As SqlParameter = cmdEditLI.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
        prmRecordSource.Value = txtRecordSource.Text

        Dim prmPartNumber As SqlParameter = cmdEditLI.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
        prmPartNumber.Value = txtPartNumber.Text

        Dim prmRT As SqlParameter = cmdEditLI.Parameters.Add("@RT", SqlDbType.Char, 1)
        prmRT.Value = txtRT.Text

        Dim prmDescription As SqlParameter = cmdEditLI.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtDescription.Text

        Dim prmCost As SqlParameter = cmdEditLI.Parameters.Add("@Cost", SqlDbType.Money)
        prmCost.Value = CDbl(txtCost.Text)

        Dim prmMargin As SqlParameter = cmdEditLI.Parameters.Add("@Margin", SqlDbType.Int)
        prmMargin.Value = CInt(txtMargin.Text)

        Dim prmSell As SqlParameter = cmdEditLI.Parameters.Add("@Sell", SqlDbType.Money)
        prmSell.Value = CDbl(txtSell.Text)

        Dim prmQuantity As SqlParameter = cmdEditLI.Parameters.Add("@Quantity", SqlDbType.Int)
        prmQuantity.Value = CInt(txtQuantity.Text)

        Dim prmLocation As SqlParameter = cmdEditLI.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtLocation.Text

        Dim prmType As SqlParameter = cmdEditLI.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
        prmType.Value = txtType.Text

        Dim prmShipped As SqlParameter = cmdEditLI.Parameters.Add("@Shipped", SqlDbType.Bit)
        prmShipped.Value = cbxShipped.Checked

        Dim prmShipDate As SqlParameter = cmdEditLI.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtShipDate.Text) Then
            prmShipDate.Value = txtShipDate.Text
        End If

        Dim prmShipQuan As SqlParameter = cmdEditLI.Parameters.Add("@shipQuan", SqlDbType.Int)
        If IsNumeric(txtShipQuan.Text) Then
            prmShipQuan.Value = CInt(txtShipQuan.Text)
        End If

        Dim prmQuanBO As SqlParameter = cmdEditLI.Parameters.Add("@QuanBO", SqlDbType.Int)
        If IsNumeric(txtQuanBO.Text) Then
            prmQuanBO.Value = CInt(txtQuanBO.Text)
        End If



        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdEditLI.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try


        'blnStartBldg = True

        'LoadBldgs()
    End Sub

    Private Sub frmj_LI_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadDetails()
        LoadCatalogMaincbo()
    End Sub

    Private Sub DeleteLineItem()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "DELETE FROM LineItem WHERE ItemID = " & frmJob.intItemID
        Dim cmdDeleteLI As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdDeleteLI.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error Deleting This Line Item!")
        Finally
            cnn.Close()
        End Try



    End Sub

    Private Sub CopyLI()

        MsgBox("This function is not implemented at this time")
        'If txtBUnitType.Text = "" Then
        '    MsgBox("You Must Enter A Unit Type!")
        '    Exit Sub
        'End If

        'If txtBUnitDescription.Text = "" Then
        '    MsgBox("You Must Enter A Unit Description!")
        '    Exit Sub
        'End If

        'Dim strNewUnitDesc As String
        ''Dim intOldUnitId As Integer
        ''Dim intNewUnitID As Integer
        'intOldUnitId = cboUnit.SelectedValue

        'strNewUnitDesc = txtBUnitDescription.Text
        ''If strNewUnitDesc = strOldUnitDesc Then
        ''    MsgBox("You must enter a Description different than: " & strOldUnitDesc & "!")
        ''    Exit Sub
        ''End If


        ''---------------------------
        ''Run b_CopyUnitLineItemDef



        ''Dim cmdCopyUnit As New SqlCommand("b_CopyUnitLineItemDef", cnn)
        ''cmdCopyUnit.CommandType = CommandType.StoredProcedure

        ''Dim prmBidID As SqlParameter = cmdCopyUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
        ''prmBidID.Value = frmMain.intBidID

        ''Dim prmNewUnitID As SqlParameter = cmdCopyUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
        ''prmNewUnitID.Value = frmMain.intUnitID

        ''Dim prmOldUnitID As SqlParameter = cmdCopyUnit.Parameters.Add("@OldUnitID", SqlDbType.Int)
        ''prmOldUnitID.Value = intOldUnitId
        ''Try
        ''    If cnn.State = ConnectionState.Closed Then
        ''        cnn.Open()
        ''    End If
        ''    cmdCopyUnit.ExecuteNonQuery()
        ''    cnn.Close()
        ''Catch ex As Exception
        ''    MsgBox("There was an error copying this record!!")
        ''Finally
        ''    If cnn.State = ConnectionState.Open Then
        ''        cnn.Close()
        ''    End If
        ''End Try
        ''----------- Line Item Definitions Added ----------------

        'cnn.ConnectionString = frmMain.strConnect
        'Dim strSqlCommand As String
        'strSqlCommand = "SELECT bidUnitLineItemdef.ItemID, PartNumber, RT, Location FROM bidUnitLineItemDef "
        'strSqlCommand = strSqlCommand & "INNER JOIN bidunititemquan ON bidunitlineitemdef.itemid = bidunititemquan.itemid WHERE bidunititemquan.UnitID = " & intOldUnitID
        'Dim cmdItems As New SqlCommand(strSqlCommand, cnn)
        'Dim drItems As SqlDataReader
        'cnn.Open()
        'drItems = cmdItems.ExecuteReader

        'While drItems.Read
        '    'MsgBox(drItems("PartNumber").ToString)
        '    ' New connection because cnn is still open
        '    Dim cnn1 As New SqlConnection
        '    cnn1.ConnectionString = frmMain.strConnect
        '    Dim intNewItemID As Integer
        '    Dim intOldItemID As Integer
        '    Dim strPartNumber As String
        '    Dim strRT As String
        '    intNewItemID = drItems("ItemId")
        '    strPartNumber = drItems("PartNumber")
        '    strRT = drItems("RT")
        '    ' Get Old ItemID 
        '    Dim strSqlCommand1 As String
        '    strSqlCommand1 = "SELECT ItemID FROM bidUnitLineItemDef "
        '    strSqlCommand1 = strSqlCommand1 & "WHERE BidID = " & frmMain.intBidID
        '    strSqlCommand1 = strSqlCommand1 & " AND PartNumber = '" & Trim(strPartNumber)
        '    strSqlCommand1 = strSqlCommand1 & "' AND RT = '" & Trim(strRT) & "'"
        '    Dim cmdOldItemID As New SqlCommand(strSqlCommand1, cnn1)
        '    Dim drOldItemID As SqlDataReader
        '    cnn1.Open()
        '    drOldItemID = cmdOldItemID.ExecuteReader
        '    drOldItemID.Read()
        '    intOldItemID = drOldItemID("ItemID")
        '    drOldItemID.Close()
        '    cnn1.Close()
        '    ' All Variables defined to call b_CopyUnitItemQuan

        '    Dim cmdCopyLIQuan As New SqlCommand("b_CopyUnitItemQuan", cnn1)
        '    cmdCopyLIQuan.CommandType = CommandType.StoredProcedure

        '    Dim prmNewItemID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@NewItemID", SqlDbType.Int)
        '    prmNewItemID1.Value = intNewItemID

        '    Dim prmBidID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@ProjectID", SqlDbType.Int)
        '    prmBidID1.Value = frmMain.intBidID

        '    Dim prmNewUnitID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@NewUnitID", SqlDbType.Int)
        '    prmNewUnitID1.Value = frmMain.intUnitID

        '    Dim prmOldItemID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@OldItemID", SqlDbType.Int)
        '    prmOldItemID1.Value = intOldItemID

        '    Dim prmOldUnitID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@OldUnitID", SqlDbType.Int)
        '    prmOldUnitID1.Value = intOldUnitID

        '    Dim prmLocation As SqlParameter = cmdCopyLIQuan.Parameters.Add("@Location", SqlDbType.VarChar, 50)
        '    prmLocation.Value = drItems("Location")
        '    Try
        '        If cnn1.State = ConnectionState.Closed Then
        '            cnn1.Open()
        '        End If
        '        cmdCopyLIQuan.ExecuteNonQuery()
        '        cnn1.Close()
        '    Catch ex As Exception
        '        MsgBox("There was an error copying this record!!")
        '    Finally
        '        If cnn1.State = ConnectionState.Open Then
        '            cnn1.Close()
        '        End If
        '    End Try
        'End While
        'drItems.Close()
        'cnn.Close()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        strFunction = "Copy"
        'ClearAll()
        'LoadUnits()
        'cboUnit.Visible = True
        'lblUnit.Visible = True
        DisableMenu()
        ShowButtons()
        btnDelete.Visible = False
    End Sub

    Private Sub DisableMenu()
        mnuCopy.Visible = False
        mnuEdit.Visible = False

    End Sub

    Private Sub EnableMenu()
        mnuCopy.Visible = True
        mnuEdit.Visible = True

    End Sub

    Private Sub mnuEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEdit.Click
        strFunction = "Edit"

        DisableMenu()
        ShowButtons()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteLineItem()
        Me.Close()
        EnableMenu()
    End Sub

    Private Sub ShowButtons()
        btnSave.Visible = True
        btnCancel.Visible = True
        If strFunction <> "New" Then
            btnDelete.Visible = True
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub



    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case strFunction
            Case "New"
                AddLI()
                ClearDetails()
            Case "Edit"
                EditLI()
                Me.Close()
            Case "Copy"
                Me.Close()
        End Select
        EnableMenu()

        'HideAll()
        'ClearAll()
        'frmBid.blnStartUnit = True
        'frmBid.blnUnitReturn = True
    End Sub

    Sub LoadCatalogMaincbo()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String = Nothing
        If rbMain.Checked = True Then
            strCatalogMain = "CatMainItems"
        ElseIf rbSO.Checked = True Then
            strCatalogMain = "CatSOItem"
            'ElseIf rbBid.Checked = True Then
            '    LoadBidItemcbo()
            '    Exit Sub
        End If

        Dim daCatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading CatalogMain CBO")
        End Try
        cboCatalogMain.DataSource = dsCatalogMain.Tables("Catalog")
        cboCatalogMain.DisplayMember = "Item"
        cboCatalogMain.ValueMember = "ProductID"
    End Sub

    Private Sub rbMain_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbMain.CheckedChanged
        ClearDetails()
        LoadCatalogMaincbo()
    End Sub

    Private Sub rbSO_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSO.CheckedChanged
        ClearDetails()
        LoadCatalogMaincbo()
    End Sub

    Private Sub mnuAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAdd.Click
        rbSO.Visible = True
        rbMain.Visible = True
        cboCatalogMain.Visible = True
        strFunction = "New"
        ClearDetails()
        DisableMenu()
        ShowButtons()
    End Sub

    Sub LoadCatMainLineItem()
        'Units
        If cboCatalogMain.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboCatalogMain.SelectedValue.ToString

        Dim spCatMainLineItem As String = "CatMainLineItem"
        If rbMain.Checked = True Then
            spCatMainLineItem = "CatMainLineItem"
        ElseIf rbSO.Checked = True Then
            spCatMainLineItem = "CatSOLineItem"
            'ElseIf rbBid.Checked = True Then
            '    spCatMainLineItem = "b_CatBidLineItem"
        End If

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboCatalogMain.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            txtPartNumber.Text = drCatMainLineItem.Item("ProductID").ToString
            txtDescription.Text = drCatMainLineItem.Item("Description").ToString
            txtCost.Text = drCatMainLineItem.Item("Cost").ToString
        Catch ex As Exception
            MsgBox("Error Loading CatMainLineItem")
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try
        txtQuantity.Text = "1"
        txtMargin.Text = "30"
        txtSell.Text = Format((CDbl(txtCost.Text) / (1 - (CDbl(txtMargin.Text) / 100))), "$###,###.##")
        txtRT.Text = "T"
        If rbMain.Checked Then
            txtRecordSource.Text = "CatalogMain"
        End If
        If rbSO.Checked Then
            txtRecordSource.Text = "SOCatalog"
        End If
    End Sub

    Private Sub cboCatalogMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCatalogMain.SelectedIndexChanged
        LoadCatMainLineItem()
    End Sub

    Private Sub AddLI()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAddLI As New SqlCommand("AddLineItem", cnn)
        cmdAddLI.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdAddLI.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim prmBldgID As SqlParameter = cmdAddLI.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = frmJob.intBldgID

        Dim prmUnitID As SqlParameter = cmdAddLI.Parameters.Add("@UnitID", SqlDbType.Int)
        prmUnitID.Value = frmJob.intUnitID

        Dim prmRecordSource As SqlParameter = cmdAddLI.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
        prmRecordSource.Value = txtRecordSource.Text

        Dim prmPartNumber As SqlParameter = cmdAddLI.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
        prmPartNumber.Value = txtPartNumber.Text

        Dim prmRT As SqlParameter = cmdAddLI.Parameters.Add("@RT", SqlDbType.Char, 1)
        prmRT.Value = txtRT.Text

        Dim prmDescription As SqlParameter = cmdAddLI.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtDescription.Text

        Dim prmCost As SqlParameter = cmdAddLI.Parameters.Add("@Cost", SqlDbType.Money)
        If IsNumeric(txtCost.Text) Then
            prmCost.Value = CDbl(txtCost.Text)
        Else
            prmCost.Value = 0
        End If


        Dim prmMargin As SqlParameter = cmdAddLI.Parameters.Add("@Margin", SqlDbType.Int)
        If IsNumeric(txtMargin.Text) Then
            prmMargin.Value = CInt(txtMargin.Text)
        Else
            prmMargin.Value = 0
        End If

        Dim prmSell As SqlParameter = cmdAddLI.Parameters.Add("@Sell", SqlDbType.Money)
        If IsNumeric(txtSell.Text) Then
            prmSell.Value = CDbl(txtSell.Text)
        Else
            prmSell.Value = 0
        End If

        Dim prmQuantity As SqlParameter = cmdAddLI.Parameters.Add("@Quantity", SqlDbType.Int)
        If IsNumeric(txtQuantity.Text) Then
            prmQuantity.Value = CInt(txtQuantity.Text)
        Else
            prmQuantity.Value = 1
        End If

        Dim prmLocation As SqlParameter = cmdAddLI.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtLocation.Text

        Dim prmType As SqlParameter = cmdAddLI.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
        prmType.Value = txtType.Text

        'Dim prmShipped As SqlParameter = cmdAddLI.Parameters.Add("@Shipped", SqlDbType.Bit)
        'prmShipped.Value = cbxShipped.Checked

        Dim prmShipDate As SqlParameter = cmdAddLI.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtShipDate.Text) Then
            prmShipDate.Value = txtShipDate.Text
        End If

        Dim prmShipQuan As SqlParameter = cmdAddLI.Parameters.Add("@shipQuan", SqlDbType.Int)
        If IsNumeric(txtShipQuan.Text) Then
            prmShipQuan.Value = CInt(txtShipQuan.Text)
        End If

        Dim prmQuanBO As SqlParameter = cmdAddLI.Parameters.Add("@QuanBO", SqlDbType.Int)
        If IsNumeric(txtQuanBO.Text) Then
            prmQuanBO.Value = CInt(txtQuanBO.Text)
        End If



        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddLI.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub txtPartNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPartNumber.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtDescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtLocation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLocation.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtType_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtType.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub
End Class
