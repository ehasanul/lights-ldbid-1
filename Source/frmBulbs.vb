Imports System.Data
Imports System.Data.SqlClient
Public Class frmBulbs
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim blnIsLoading As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblPartNumber As System.Windows.Forms.Label
    Friend WithEvents lblItemSelected As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents lblRT As System.Windows.Forms.Label
    Friend WithEvents cboBulbs As System.Windows.Forms.ComboBox
    Friend WithEvents txtQuan As System.Windows.Forms.TextBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents txtMargin As System.Windows.Forms.TextBox
    Friend WithEvents txtSell As System.Windows.Forms.TextBox
    Friend WithEvents lblQuan As System.Windows.Forms.Label
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents lblCost As System.Windows.Forms.Label
    Friend WithEvents lblMargin As System.Windows.Forms.Label
    Friend WithEvents lblSell As System.Windows.Forms.Label
    Friend WithEvents txtLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtRT As System.Windows.Forms.TextBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblPartNumber = New System.Windows.Forms.Label
        Me.lblLocation = New System.Windows.Forms.Label
        Me.lblRT = New System.Windows.Forms.Label
        Me.lblItemSelected = New System.Windows.Forms.Label
        Me.cboBulbs = New System.Windows.Forms.ComboBox
        Me.txtQuan = New System.Windows.Forms.TextBox
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.txtMargin = New System.Windows.Forms.TextBox
        Me.txtSell = New System.Windows.Forms.TextBox
        Me.lblQuan = New System.Windows.Forms.Label
        Me.lblDesc = New System.Windows.Forms.Label
        Me.lblCost = New System.Windows.Forms.Label
        Me.lblMargin = New System.Windows.Forms.Label
        Me.lblSell = New System.Windows.Forms.Label
        Me.txtLocation = New System.Windows.Forms.TextBox
        Me.txtRT = New System.Windows.Forms.TextBox
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblPartNumber
        '
        Me.lblPartNumber.Location = New System.Drawing.Point(84, 24)
        Me.lblPartNumber.Name = "lblPartNumber"
        Me.lblPartNumber.Size = New System.Drawing.Size(320, 20)
        Me.lblPartNumber.TabIndex = 0
        Me.lblPartNumber.Text = "PartNum"
        '
        'lblLocation
        '
        Me.lblLocation.Location = New System.Drawing.Point(20, 128)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(100, 16)
        Me.lblLocation.TabIndex = 2
        Me.lblLocation.Text = "Location"
        '
        'lblRT
        '
        Me.lblRT.Location = New System.Drawing.Point(332, 76)
        Me.lblRT.Name = "lblRT"
        Me.lblRT.Size = New System.Drawing.Size(40, 16)
        Me.lblRT.TabIndex = 3
        Me.lblRT.Text = "RT"
        '
        'lblItemSelected
        '
        Me.lblItemSelected.Location = New System.Drawing.Point(20, 24)
        Me.lblItemSelected.Name = "lblItemSelected"
        Me.lblItemSelected.Size = New System.Drawing.Size(52, 16)
        Me.lblItemSelected.TabIndex = 4
        Me.lblItemSelected.Text = "For Item:"
        '
        'cboBulbs
        '
        Me.cboBulbs.Location = New System.Drawing.Point(20, 48)
        Me.cboBulbs.Name = "cboBulbs"
        Me.cboBulbs.Size = New System.Drawing.Size(388, 21)
        Me.cboBulbs.TabIndex = 5
        '
        'txtQuan
        '
        Me.txtQuan.Enabled = False
        Me.txtQuan.Location = New System.Drawing.Point(20, 100)
        Me.txtQuan.Name = "txtQuan"
        Me.txtQuan.Size = New System.Drawing.Size(52, 20)
        Me.txtQuan.TabIndex = 7
        Me.txtQuan.Text = ""
        Me.txtQuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDesc
        '
        Me.txtDesc.Enabled = False
        Me.txtDesc.Location = New System.Drawing.Point(80, 100)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(248, 20)
        Me.txtDesc.TabIndex = 8
        Me.txtDesc.Text = ""
        '
        'txtCost
        '
        Me.txtCost.Enabled = False
        Me.txtCost.Location = New System.Drawing.Point(193, 148)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.Size = New System.Drawing.Size(68, 20)
        Me.txtCost.TabIndex = 9
        Me.txtCost.Text = ""
        Me.txtCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMargin
        '
        Me.txtMargin.Enabled = False
        Me.txtMargin.Location = New System.Drawing.Point(264, 148)
        Me.txtMargin.Name = "txtMargin"
        Me.txtMargin.Size = New System.Drawing.Size(68, 20)
        Me.txtMargin.TabIndex = 10
        Me.txtMargin.Text = ""
        Me.txtMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSell
        '
        Me.txtSell.Enabled = False
        Me.txtSell.Location = New System.Drawing.Point(336, 148)
        Me.txtSell.Name = "txtSell"
        Me.txtSell.Size = New System.Drawing.Size(68, 20)
        Me.txtSell.TabIndex = 11
        Me.txtSell.Text = ""
        Me.txtSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblQuan
        '
        Me.lblQuan.Location = New System.Drawing.Point(20, 76)
        Me.lblQuan.Name = "lblQuan"
        Me.lblQuan.Size = New System.Drawing.Size(48, 16)
        Me.lblQuan.TabIndex = 12
        Me.lblQuan.Text = "Quantity"
        '
        'lblDesc
        '
        Me.lblDesc.Location = New System.Drawing.Point(80, 76)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(108, 16)
        Me.lblDesc.TabIndex = 13
        Me.lblDesc.Text = "Description"
        '
        'lblCost
        '
        Me.lblCost.Location = New System.Drawing.Point(196, 128)
        Me.lblCost.Name = "lblCost"
        Me.lblCost.Size = New System.Drawing.Size(36, 16)
        Me.lblCost.TabIndex = 14
        Me.lblCost.Text = "Cost"
        '
        'lblMargin
        '
        Me.lblMargin.Location = New System.Drawing.Point(268, 128)
        Me.lblMargin.Name = "lblMargin"
        Me.lblMargin.Size = New System.Drawing.Size(52, 16)
        Me.lblMargin.TabIndex = 15
        Me.lblMargin.Text = "Margin"
        '
        'lblSell
        '
        Me.lblSell.Location = New System.Drawing.Point(340, 128)
        Me.lblSell.Name = "lblSell"
        Me.lblSell.Size = New System.Drawing.Size(36, 16)
        Me.lblSell.TabIndex = 16
        Me.lblSell.Text = "Sell"
        '
        'txtLocation
        '
        Me.txtLocation.Enabled = False
        Me.txtLocation.Location = New System.Drawing.Point(20, 148)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(172, 20)
        Me.txtLocation.TabIndex = 17
        Me.txtLocation.Text = ""
        '
        'txtRT
        '
        Me.txtRT.Enabled = False
        Me.txtRT.Location = New System.Drawing.Point(332, 100)
        Me.txtRT.Name = "txtRT"
        Me.txtRT.Size = New System.Drawing.Size(72, 20)
        Me.txtRT.TabIndex = 18
        Me.txtRT.Text = ""
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(316, 184)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(52, 23)
        Me.btnAdd.TabIndex = 19
        Me.btnAdd.Text = "Add"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(368, 184)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(48, 23)
        Me.btnCancel.TabIndex = 20
        Me.btnCancel.Text = "Cancel"
        '
        'frmBulbs
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(428, 230)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.txtRT)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.lblSell)
        Me.Controls.Add(Me.lblMargin)
        Me.Controls.Add(Me.lblCost)
        Me.Controls.Add(Me.lblDesc)
        Me.Controls.Add(Me.lblQuan)
        Me.Controls.Add(Me.txtSell)
        Me.Controls.Add(Me.txtMargin)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.txtDesc)
        Me.Controls.Add(Me.txtQuan)
        Me.Controls.Add(Me.cboBulbs)
        Me.Controls.Add(Me.lblItemSelected)
        Me.Controls.Add(Me.lblRT)
        Me.Controls.Add(Me.lblLocation)
        Me.Controls.Add(Me.lblPartNumber)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBulbs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bulbs"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmBulbs_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblPartNumber().Text = frmMain.strBlbPartNumber
        'lblQuanMsg.Text = frmMain.intBlbQuan
        blnIsLoading = True
        LoadBulbs()

    End Sub

    Sub LoadBulbs()

        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadBulbs As New SqlCommand("BulbList", cnn)
        cmdLoadBulbs.CommandType = CommandType.StoredProcedure

        Dim prmPartNumber As SqlParameter = cmdLoadBulbs.Parameters.Add("@PartNumber", SqlDbType.Char, 255)
        prmPartNumber.Value = frmMain.strBlbPartNumber

        Dim daBulbs As New SqlDataAdapter(cmdLoadBulbs)


        Dim dsBulbs As New DataSet

        daBulbs.Fill(dsBulbs, "Bulbs")
        If dsBulbs.Tables(0).Rows.Count = 1 Then

            cboBulbs.DataSource = dsBulbs.Tables("Bulbs")
            cboBulbs.DisplayMember = "Bulb"
            cboBulbs.ValueMember = "ProductID"
            blnIsLoading = False
            cboBulbs.SelectedIndex = 0
            LoadBulbDetails()
            AddBulbsFromCatalog()
            MsgBox("Bulbs have been added!", MsgBoxStyle.OkOnly, "It Saves Time")
            Me.Close()
            Exit Sub
        End If


        Dim dtBulbs As DataTable = dsBulbs.Tables("Bulbs")
        Dim dr As DataRow = dtBulbs.NewRow
        'Initialize DataSet First Row
        dr("Bulb") = " - Please Select An Item"
        dr("ProductID") = "---"
        dtBulbs.Rows.InsertAt(dr, 0)

        cboBulbs.DataSource = dsBulbs.Tables("Bulbs")
        cboBulbs.DisplayMember = "Bulb"
        cboBulbs.ValueMember = "ProductID"
        blnIsLoading = False

    End Sub

    Private Sub AddBulbsFromCatalog()

        If Not IsNumeric(txtCost.Text) Then
            txtCost.Text = "0"
        End If

        If Not IsNumeric(txtMargin.Text) Then
            txtMargin.Text = "0"
        End If

        If Not IsNumeric(txtSell.Text) Then
            txtSell.Text = "0"
        End If
        AddLineItemToUnitType(frmMain.UnitType, frmMain.intBidID, CInt(txtQuan.Text), txtLocation.Text, _
                                 "CatMain", CStr(cboBulbs.SelectedValue), txtRT.Text, frmMain.strBlbType, _
                                 txtDesc.Text, txtCost.Text, txtSell.Text, cnn, _
                                 txtMargin.Text)
    End Sub

    Private Sub LoadBulbDetails()
        If blnIsLoading = True Then
            Exit Sub
        End If
        Dim strPartNumber As String
        strPartNumber = frmMain.strBlbPartNumber
        Dim strBulbID As String
        strBulbID = cboBulbs.SelectedValue.ToString
        'Populate TextBoxes From Stored Proecedure And DataReader

        Dim cmdBulbDetails As New SqlCommand("BulbDetails", cnn)
        cmdBulbDetails.CommandType = CommandType.StoredProcedure

        Dim prmPartNumber As SqlParameter = cmdBulbDetails.Parameters.Add("@PartNumber", SqlDbType.Char, 255)
        prmPartNumber.Value = frmMain.strBlbPartNumber

        Dim prmBulbID As SqlParameter = cmdBulbDetails.Parameters.Add("@BulbID", SqlDbType.Char, 255)
        prmBulbID.Value = cboBulbs.SelectedValue

        Dim drBulbDetails As SqlDataReader
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        drBulbDetails = cmdBulbDetails.ExecuteReader
        drBulbDetails.Read()
        txtQuan.Text = (CInt(drBulbDetails.Item("Quan")) * frmMain.intBlbQuan).ToString
        txtCost.Text = Format(CDbl(drBulbDetails.Item("StandardCost")), "C")
        txtDesc.Text = drBulbDetails.Item("Description").ToString

        txtRT.Text = frmMain.strBlbRT
        txtLocation.Text = frmMain.strBlbLocation
        txtMargin.Text = "30"
        txtSell.Text = Format((CDbl(txtCost.Text) / 0.7), "C")
        drBulbDetails.Close()
        cnn.Close()
    End Sub



    Private Sub cboBulbs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBulbs.SelectedIndexChanged
        LoadBulbDetails()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        AddBulbsFromCatalog()
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class
