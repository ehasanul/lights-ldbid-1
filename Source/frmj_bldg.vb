Imports System.Data
Imports System.Data.SqlClient
Public Class frmj_bldg
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim blnStartBldg As Boolean  'Tests Startup Condition in LoadBldgs
    Dim strFunction As String
    Dim strOldBldgDesc As String
    Dim intOldBldgNum As Integer
    Public Shared strOldShipDate As String
    Public Shared strOldShipDateR As String
    Public Shared blnShipped As Boolean
    Public Shared blnRShipped As Boolean
    Public Shared dtShipDate As Date
    Public Shared dtshipdateR As Date
    Dim intNewBldgID As Integer
    Dim intOldUnitID As Integer
    Dim intNewUnitID As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblBldgNotes As System.Windows.Forms.Label
    Friend WithEvents lblBldgShipSeq As System.Windows.Forms.Label
    Friend WithEvents lblbldgShipDate As System.Windows.Forms.Label
    Friend WithEvents lblBldgAddress As System.Windows.Forms.Label
    Friend WithEvents lblBldgLocation As System.Windows.Forms.Label
    Friend WithEvents lblBldgNum As System.Windows.Forms.Label
    Friend WithEvents lblBldgDesc As System.Windows.Forms.Label
    Friend WithEvents lblbldgType As System.Windows.Forms.Label
    Friend WithEvents txtbldgNotes As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgShipSeq As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgShipDate As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgNum As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgType As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents cbxShipped As System.Windows.Forms.CheckBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuEdit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuShip As System.Windows.Forms.MenuItem
    Friend WithEvents txtBldgShipDateR As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxRShipped As System.Windows.Forms.CheckBox
    Friend WithEvents mnuHot As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblBldgNotes = New System.Windows.Forms.Label
        Me.lblBldgShipSeq = New System.Windows.Forms.Label
        Me.lblbldgShipDate = New System.Windows.Forms.Label
        Me.lblBldgAddress = New System.Windows.Forms.Label
        Me.lblBldgLocation = New System.Windows.Forms.Label
        Me.lblBldgNum = New System.Windows.Forms.Label
        Me.lblBldgDesc = New System.Windows.Forms.Label
        Me.lblbldgType = New System.Windows.Forms.Label
        Me.txtbldgNotes = New System.Windows.Forms.TextBox
        Me.txtbldgShipSeq = New System.Windows.Forms.TextBox
        Me.txtbldgShipDate = New System.Windows.Forms.TextBox
        Me.txtbldgAddress = New System.Windows.Forms.TextBox
        Me.txtbldgLocation = New System.Windows.Forms.TextBox
        Me.txtbldgNum = New System.Windows.Forms.TextBox
        Me.txtbldgDesc = New System.Windows.Forms.TextBox
        Me.txtbldgType = New System.Windows.Forms.TextBox
        Me.txtBldgName = New System.Windows.Forms.TextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.cbxShipped = New System.Windows.Forms.CheckBox
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mnuEdit = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuShip = New System.Windows.Forms.MenuItem
        Me.txtBldgShipDateR = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbxRShipped = New System.Windows.Forms.CheckBox
        Me.mnuHot = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.SuspendLayout()
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(464, 236)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(44, 16)
        Me.btnDelete.TabIndex = 40
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(552, 236)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(44, 16)
        Me.btnCancel.TabIndex = 39
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(508, 236)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(44, 16)
        Me.btnSave.TabIndex = 38
        Me.btnSave.Text = "Save"
        Me.btnSave.Visible = False
        '
        'lblBldgNotes
        '
        Me.lblBldgNotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgNotes.Location = New System.Drawing.Point(8, 128)
        Me.lblBldgNotes.Name = "lblBldgNotes"
        Me.lblBldgNotes.Size = New System.Drawing.Size(40, 16)
        Me.lblBldgNotes.TabIndex = 37
        Me.lblBldgNotes.Text = "Notes"
        '
        'lblBldgShipSeq
        '
        Me.lblBldgShipSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgShipSeq.Location = New System.Drawing.Point(376, 30)
        Me.lblBldgShipSeq.Name = "lblBldgShipSeq"
        Me.lblBldgShipSeq.Size = New System.Drawing.Size(44, 16)
        Me.lblBldgShipSeq.TabIndex = 36
        Me.lblBldgShipSeq.Text = "Ship Seq"
        '
        'lblbldgShipDate
        '
        Me.lblbldgShipDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbldgShipDate.Location = New System.Drawing.Point(128, 32)
        Me.lblbldgShipDate.Name = "lblbldgShipDate"
        Me.lblbldgShipDate.Size = New System.Drawing.Size(56, 12)
        Me.lblbldgShipDate.TabIndex = 35
        Me.lblbldgShipDate.Text = "T Ship Date"
        '
        'lblBldgAddress
        '
        Me.lblBldgAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgAddress.Location = New System.Drawing.Point(248, 94)
        Me.lblBldgAddress.Name = "lblBldgAddress"
        Me.lblBldgAddress.Size = New System.Drawing.Size(56, 16)
        Me.lblBldgAddress.TabIndex = 34
        Me.lblBldgAddress.Text = "Address"
        '
        'lblBldgLocation
        '
        Me.lblBldgLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgLocation.Location = New System.Drawing.Point(8, 96)
        Me.lblBldgLocation.Name = "lblBldgLocation"
        Me.lblBldgLocation.Size = New System.Drawing.Size(60, 12)
        Me.lblBldgLocation.TabIndex = 33
        Me.lblBldgLocation.Text = "Location"
        '
        'lblBldgNum
        '
        Me.lblBldgNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgNum.Location = New System.Drawing.Point(80, 30)
        Me.lblBldgNum.Name = "lblBldgNum"
        Me.lblBldgNum.Size = New System.Drawing.Size(36, 16)
        Me.lblBldgNum.TabIndex = 32
        Me.lblBldgNum.Text = "#"
        '
        'lblBldgDesc
        '
        Me.lblBldgDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgDesc.Location = New System.Drawing.Point(252, 72)
        Me.lblBldgDesc.Name = "lblBldgDesc"
        Me.lblBldgDesc.Size = New System.Drawing.Size(52, 20)
        Me.lblBldgDesc.TabIndex = 31
        Me.lblBldgDesc.Text = "Description"
        '
        'lblbldgType
        '
        Me.lblbldgType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbldgType.Location = New System.Drawing.Point(464, 72)
        Me.lblbldgType.Name = "lblbldgType"
        Me.lblbldgType.Size = New System.Drawing.Size(32, 16)
        Me.lblbldgType.TabIndex = 30
        Me.lblbldgType.Text = "Type"
        '
        'txtbldgNotes
        '
        Me.txtbldgNotes.Location = New System.Drawing.Point(76, 120)
        Me.txtbldgNotes.Multiline = True
        Me.txtbldgNotes.Name = "txtbldgNotes"
        Me.txtbldgNotes.Size = New System.Drawing.Size(520, 104)
        Me.txtbldgNotes.TabIndex = 29
        Me.txtbldgNotes.Text = ""
        '
        'txtbldgShipSeq
        '
        Me.txtbldgShipSeq.Location = New System.Drawing.Point(376, 48)
        Me.txtbldgShipSeq.Name = "txtbldgShipSeq"
        Me.txtbldgShipSeq.Size = New System.Drawing.Size(36, 20)
        Me.txtbldgShipSeq.TabIndex = 28
        Me.txtbldgShipSeq.Text = ""
        '
        'txtbldgShipDate
        '
        Me.txtbldgShipDate.Enabled = False
        Me.txtbldgShipDate.Location = New System.Drawing.Point(124, 48)
        Me.txtbldgShipDate.Name = "txtbldgShipDate"
        Me.txtbldgShipDate.Size = New System.Drawing.Size(76, 20)
        Me.txtbldgShipDate.TabIndex = 27
        Me.txtbldgShipDate.Text = ""
        '
        'txtbldgAddress
        '
        Me.txtbldgAddress.Location = New System.Drawing.Point(304, 92)
        Me.txtbldgAddress.Name = "txtbldgAddress"
        Me.txtbldgAddress.Size = New System.Drawing.Size(288, 20)
        Me.txtbldgAddress.TabIndex = 26
        Me.txtbldgAddress.Text = ""
        '
        'txtbldgLocation
        '
        Me.txtbldgLocation.Location = New System.Drawing.Point(76, 92)
        Me.txtbldgLocation.Name = "txtbldgLocation"
        Me.txtbldgLocation.Size = New System.Drawing.Size(172, 20)
        Me.txtbldgLocation.TabIndex = 25
        Me.txtbldgLocation.Text = ""
        '
        'txtbldgNum
        '
        Me.txtbldgNum.Location = New System.Drawing.Point(76, 48)
        Me.txtbldgNum.Name = "txtbldgNum"
        Me.txtbldgNum.Size = New System.Drawing.Size(44, 20)
        Me.txtbldgNum.TabIndex = 24
        Me.txtbldgNum.Text = ""
        '
        'txtbldgDesc
        '
        Me.txtbldgDesc.Location = New System.Drawing.Point(304, 72)
        Me.txtbldgDesc.Name = "txtbldgDesc"
        Me.txtbldgDesc.Size = New System.Drawing.Size(156, 20)
        Me.txtbldgDesc.TabIndex = 23
        Me.txtbldgDesc.Text = ""
        '
        'txtbldgType
        '
        Me.txtbldgType.Location = New System.Drawing.Point(496, 70)
        Me.txtbldgType.Name = "txtbldgType"
        Me.txtbldgType.Size = New System.Drawing.Size(96, 20)
        Me.txtbldgType.TabIndex = 22
        Me.txtbldgType.Text = ""
        '
        'txtBldgName
        '
        Me.txtBldgName.Location = New System.Drawing.Point(76, 72)
        Me.txtBldgName.Name = "txtBldgName"
        Me.txtBldgName.Size = New System.Drawing.Size(172, 20)
        Me.txtBldgName.TabIndex = 41
        Me.txtBldgName.Text = ""
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 72)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(60, 20)
        Me.lblName.TabIndex = 42
        Me.lblName.Text = "Name"
        '
        'cbxShipped
        '
        Me.cbxShipped.Enabled = False
        Me.cbxShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxShipped.Location = New System.Drawing.Point(516, 46)
        Me.cbxShipped.Name = "cbxShipped"
        Me.cbxShipped.Size = New System.Drawing.Size(68, 24)
        Me.cbxShipped.TabIndex = 43
        Me.cbxShipped.Text = "Trim Shpd"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEdit, Me.mnuCopy, Me.mnuShip, Me.mnuHot})
        '
        'mnuEdit
        '
        Me.mnuEdit.Index = 0
        Me.mnuEdit.Text = "Edit"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 1
        Me.mnuCopy.Text = "Copy"
        '
        'mnuShip
        '
        Me.mnuShip.Index = 2
        Me.mnuShip.Text = "Ship"
        '
        'txtBldgShipDateR
        '
        Me.txtBldgShipDateR.Enabled = False
        Me.txtBldgShipDateR.Location = New System.Drawing.Point(212, 48)
        Me.txtBldgShipDateR.Name = "txtBldgShipDateR"
        Me.txtBldgShipDateR.Size = New System.Drawing.Size(72, 20)
        Me.txtBldgShipDateR.TabIndex = 44
        Me.txtBldgShipDateR.Text = ""
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(216, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 16)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "R Ship Date"
        '
        'cbxRShipped
        '
        Me.cbxRShipped.Enabled = False
        Me.cbxRShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRShipped.Location = New System.Drawing.Point(428, 44)
        Me.cbxRShipped.Name = "cbxRShipped"
        Me.cbxRShipped.Size = New System.Drawing.Size(76, 24)
        Me.cbxRShipped.TabIndex = 46
        Me.cbxRShipped.Text = "Rough Shpd"
        '
        'mnuHot
        '
        Me.mnuHot.Index = 3
        Me.mnuHot.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        Me.mnuHot.Text = "Hot"
        Me.mnuHot.Visible = False
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Shortcut = System.Windows.Forms.Shortcut.CtrlZ
        Me.MenuItem1.Text = "Z"
        '
        'frmj_bldg
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(604, 261)
        Me.Controls.Add(Me.cbxRShipped)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBldgShipDateR)
        Me.Controls.Add(Me.cbxShipped)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.txtBldgName)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblBldgNotes)
        Me.Controls.Add(Me.lblBldgShipSeq)
        Me.Controls.Add(Me.lblbldgShipDate)
        Me.Controls.Add(Me.lblBldgAddress)
        Me.Controls.Add(Me.lblBldgLocation)
        Me.Controls.Add(Me.lblBldgNum)
        Me.Controls.Add(Me.lblBldgDesc)
        Me.Controls.Add(Me.lblbldgType)
        Me.Controls.Add(Me.txtbldgNotes)
        Me.Controls.Add(Me.txtbldgShipSeq)
        Me.Controls.Add(Me.txtbldgShipDate)
        Me.Controls.Add(Me.txtbldgAddress)
        Me.Controls.Add(Me.txtbldgLocation)
        Me.Controls.Add(Me.txtbldgNum)
        Me.Controls.Add(Me.txtbldgDesc)
        Me.Controls.Add(Me.txtbldgType)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmj_bldg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Building Details"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmj_bldg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadBldgDetails()
    End Sub
    Private Sub LoadBldgDetails()
        
        cnn.ConnectionString = frmMain.strConnect
        'MsgBox(frmJob.intBldgID.ToString)

        Dim strBldgInfo As String
        strBldgInfo = "SELECT BldgName, BldgNumber, Type, Description, Location, Address, Notes, shipdate, shipdateR, shipsequence, Shipped, ShippedR FROM Building WHERE BldgID = "
        strBldgInfo = strBldgInfo & frmJob.intBldgID.ToString
        Dim cmdBldgInfo As New SqlCommand(strBldgInfo, cnn)
        cmdBldgInfo.CommandType = CommandType.Text
        Dim drBldgInfo As SqlDataReader
        cnn.Open()
        drBldgInfo = cmdBldgInfo.ExecuteReader
        drBldgInfo.Read()
        txtBldgName.Text = drBldgInfo.Item("BldgName").ToString
        txtbldgType.Text = drBldgInfo.Item("Type").ToString
        txtbldgDesc.Text = drBldgInfo.Item("Description").ToString
        strOldBldgDesc = txtbldgDesc.Text
        txtbldgNum.Text = drBldgInfo.Item("BldgNumber").ToString
        intOldBldgNum = CInt(drBldgInfo.Item("BldgNumber").ToString)
        txtbldgLocation.Text = drBldgInfo.Item("Location").ToString
        txtbldgAddress.Text = drBldgInfo.Item("Address").ToString
        txtbldgNotes.Text = drBldgInfo.Item("Notes").ToString

        If IsDBNull(drBldgInfo.Item("shipdate")) Then
            txtbldgShipDate.Text = ""
            strOldShipDate = ""
        Else
            txtbldgShipDate.Text = FormatDateTime(System.Convert.ToDateTime(drBldgInfo.Item("shipdate")), DateFormat.ShortDate)
            strOldShipDate = txtbldgShipDate.Text
        End If

        If IsDBNull(drBldgInfo.Item("shipdateR")) Then
            txtBldgShipDateR.Text = ""
            strOldShipDateR = ""
        Else
            txtBldgShipDateR.Text = FormatDateTime(System.Convert.ToDateTime(drBldgInfo.Item("shipdateR")), DateFormat.ShortDate)
            strOldShipDateR = txtBldgShipDateR.Text
        End If
        txtbldgShipSeq.Text = drBldgInfo.Item("shipsequence").ToString
        'If drBldgInfo.Item("Shipped")  = null Then
        'MsgBox(drBldgInfo.Item("Shipped").ToString)
        If drBldgInfo.Item("Shipped").ToString = "" Then
            cbxShipped.Checked = False

        Else
            'If drBldgInfo.Item("Shipped").ToString = "TRUE" Then
            '    cbxShipped.Checked = True
            'Else
            '    cbxShipped.Checked = False
            'End If
            cbxShipped.Checked = System.Convert.ToBoolean(drBldgInfo.Item("Shipped"))
            cbxRShipped.Checked = System.Convert.ToBoolean(drBldgInfo.Item("ShippedR"))
            blnShipped = cbxShipped.Checked
            blnRShipped = cbxRShipped.Checked
        End If

        'End If
        drBldgInfo.Close()
        cnn.Close()
    End Sub

    Private Sub DeleteBldg()
        cnn.ConnectionString = frmMain.strConnect

        Dim strDeleteBldg As String
        strDeleteBldg = "DELETE Building WHERE BldgID = " & frmJob.intBldgID.ToString

        Dim cmdDeleteBldg As New SqlCommand(strDeleteBldg, cnn)
        cmdDeleteBldg.CommandType = CommandType.Text

        'Dim prmBldgID As SqlParameter = cmdDeleteBldg.Parameters.Add("@BldgID", SqlDbType.Int)
        'prmBldgID.Value = cboBldg.SelectedValue


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdDeleteBldg.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error deleting this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        'EnableMenu()
        'HideAll()
        'ClearAll()

        'LoadBldgs()
    End Sub

    

    Private Sub EditBldg()
        If txtbldgType.Text = "" Then
            MsgBox("You must enter a Building Type!")
            Exit Sub
        End If
        If txtbldgDesc.Text = "" Then
            MsgBox("You must enter a Building Description!")
            Exit Sub
        End If
        If txtbldgNum.Text = "" Then
            MsgBox("You must enter a Building Number")
            Exit Sub
        End If
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAddBldg As New SqlCommand("EditBldg", cnn)
        cmdAddBldg.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdAddBldg.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = frmJob.intBldgID

        'Dim prmBidID As SqlParameter = cmdAddBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        'prmBidID.Value = frmMain.intBidID

        Dim prmName As SqlParameter = cmdAddBldg.Parameters.Add("@BldgName", SqlDbType.NVarChar, 50)
        prmName.Value = txtBldgName.Text

        Dim prmBldgNumber As SqlParameter = cmdAddBldg.Parameters.Add("@BldgNumber", SqlDbType.Int)
        If IsNumeric(txtbldgNum.Text) Then
            prmBldgNumber.Value = CInt(txtbldgNum.Text)
        End If

        Dim prmType As SqlParameter = cmdAddBldg.Parameters.Add("@Type", SqlDbType.NVarChar, 50)
        prmType.Value = txtbldgType.Text

        Dim prmDescription As SqlParameter = cmdAddBldg.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbldgDesc.Text

        Dim prmLocation As SqlParameter = cmdAddBldg.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtbldgLocation.Text

        Dim prmAddress As SqlParameter = cmdAddBldg.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtbldgAddress.Text

        Dim prmNotes As SqlParameter = cmdAddBldg.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtbldgNotes.Text

        Dim prmShipDate As SqlParameter = cmdAddBldg.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtbldgShipDate.Text) Then
            prmShipDate.Value = txtbldgShipDate.Text
        End If

        Dim prmShipSequence As SqlParameter = cmdAddBldg.Parameters.Add("@shipsequence", SqlDbType.Int)
        If IsNumeric(txtbldgShipSeq.Text) Then
            prmShipSequence.Value = CInt(txtbldgShipSeq.Text)

        End If

        Dim prmShipped As SqlParameter = cmdAddBldg.Parameters.Add("@Shipped", SqlDbType.Bit)
        prmShipped.Value = cbxShipped.Checked

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddBldg.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try

        Dim strUpdateShipDates As String
        strUpdateShipDates = "Update LineItem Set ShipDate = '" & _
        txtbldgShipDate.Text & "' WHERE BldgID = " & frmJob.intBldgID

        Dim cmdUpdateShipDates As New SqlCommand(strUpdateShipDates, cnn)
        cmdUpdateShipDates.CommandType = CommandType.Text
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdUpdateShipDates.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error Updating the Shipping Dates!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        blnStartBldg = True

        'LoadBldgs()
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '    EditBldg()
        'End Sub
        Dim strSample As String
        strSample = txtbldgType.Text.Substring(0, 1)
        If (System.Convert.ToDouble(strSample.IndexOfAny("BCSA".ToCharArray)) = -1) Then
            MsgBox("The Unit Type Must Begin With either B,C,S or A")
            Exit Sub
        End If

        Select Case strFunction
            Case "Edit"
                EditBldg()
            Case "Copy"
                If CInt(txtbldgNum.Text) = intOldBldgNum Then
                    MsgBox("You Must Enter A New Building Number")
                    Exit Sub
                End If
                CopyBldg()
                Me.Close()
        End Select
        EnableMenu()
        frmJob.blnUpdateBldg = True
        Me.Close()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        strFunction = "Copy"
        'ClearAll()
        'LoadUnits()
        'cboUnit.Visible = True
        'lblUnit.Visible = True
        DisableMenu()
        ShowButtons()
        btnDelete.Visible = False
    End Sub

    Private Sub DisableMenu()
        mnuCopy.Visible = False
        mnuEdit.Visible = False
        mnuShip.Visible = False

    End Sub

    Private Sub EnableMenu()
        mnuCopy.Visible = True
        mnuEdit.Visible = True
        mnuShip.Visible = True
    End Sub

    Private Sub mnuEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEdit.Click
        strFunction = "Edit"

        DisableMenu()
        ShowButtons()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteBldg()
        Me.Close()
        EnableMenu()
    End Sub

    Private Sub ShowButtons()
        btnSave.Visible = True
        btnCancel.Visible = True
        btnDelete.Visible = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub CopyBldg()

       
        Dim cnnB2 As New SqlConnection
        cnnB2.ConnectionString = frmMain.strConnect

            ''Bldg

        Dim cmdAddBldg As New SqlCommand("AddBldg", cnnB2)
        cmdAddBldg.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdAddBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim prmBldgName As SqlParameter = cmdAddBldg.Parameters.Add("@BldgName", SqlDbType.NVarChar, 50)
        'strBldgName = drOldBldg.Item("BldgNumber").ToString & "-" & drOldBldg.Item("Type")
        prmBldgName.Value = txtBldgName.Text

        Dim prmBldgNumber As SqlParameter = cmdAddBldg.Parameters.Add("@BldgNumber", SqlDbType.Int)
        prmBldgNumber.Value = CInt(txtbldgNum.Text)

        Dim prmType As SqlParameter = cmdAddBldg.Parameters.Add("@Type", SqlDbType.NVarChar, 50)
        prmType.Value = txtbldgType.Text

        Dim prmDescription As SqlParameter = cmdAddBldg.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbldgDesc.Text

        Dim prmLocation As SqlParameter = cmdAddBldg.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtbldgLocation.Text

        Dim prmAddress As SqlParameter = cmdAddBldg.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtbldgAddress.Text

        Dim prmNotes As SqlParameter = cmdAddBldg.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = ""

        Dim prmShipDate As SqlParameter = cmdAddBldg.Parameters.Add("@shipdate", SqlDbType.DateTime)
        prmShipDate.Value = txtbldgShipDate.Text

        Dim prmShipSequence As SqlParameter = cmdAddBldg.Parameters.Add("@shipsequence", SqlDbType.Int)
        If IsNumeric(txtbldgShipSeq.Text) Then
            prmShipSequence.Value = CInt(txtbldgShipSeq.Text)
        Else
            MsgBox("The Ship Sequence must be entered as an Integer")
            Exit Sub
        End If


        Dim prmNewBldgID As SqlParameter = cmdAddBldg.Parameters.Add("@NewBldgID", SqlDbType.Int)
        prmNewBldgID.Direction = ParameterDirection.Output

        Try
            cnnB2.Open()

            cmdAddBldg.ExecuteNonQuery()
            cnnB2.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this Building!!")
        Finally
            If cnnB2.State = ConnectionState.Open Then
                cnnB2.Close()
            End If
        End Try
        intNewBldgID = System.Convert.ToInt32(prmNewBldgID.Value)

        '    '---------- Enter Units --------------

        Dim strUnits As String
        strUnits = "SELECT UnitID, UnitName, UnitType, Description FROM Unit WHERE BldgID = " & frmJob.intBldgID
        Dim cnnU1 As New SqlConnection
        cnnU1.ConnectionString = frmMain.strConnect

        Dim cmdUnits As New SqlCommand(strUnits, cnnU1)
        cmdUnits.CommandType = CommandType.Text

        Dim drUnits As SqlDataReader
        cnnU1.Open()

        drUnits = cmdUnits.ExecuteReader
        While drUnits.Read
            intOldUnitID = System.Convert.ToInt32(drUnits.Item("UnitID"))
            Dim cnnU2 As New SqlConnection
            cnnU2.ConnectionString = frmMain.strConnect

            ''Unit

            Dim cmdAddUnit As New SqlCommand("AddUnit", cnnU2)
            cmdAddUnit.CommandType = CommandType.StoredProcedure

            Dim prmUJobID As SqlParameter = cmdAddUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmUJobID.Value = frmMain.intJobID

            Dim prmBldgID As SqlParameter = cmdAddUnit.Parameters.Add("@BldgID", SqlDbType.Int)
            prmBldgID.Value = intNewBldgID

            Dim prmUnitName As SqlParameter = cmdAddUnit.Parameters.Add("@UnitName", SqlDbType.NVarChar, 50)
            Dim strUnitName As String
            strUnitName = drUnits.Item("UnitName").ToString
            prmUnitName.Value = strUnitName


            Dim prmUType As SqlParameter = cmdAddUnit.Parameters.Add("@UnitType", SqlDbType.Char, 10)
            prmUType.Value = drUnits.Item("UnitType")

            Dim prmUDescription As SqlParameter = cmdAddUnit.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
            prmUDescription.Value = drUnits.Item("Description")


            Dim prmNewUnitID As SqlParameter = cmdAddUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
            prmNewUnitID.Direction = ParameterDirection.Output

            Try
                cnnU2.Open()

                cmdAddUnit.ExecuteNonQuery()
                cnnU2.Close()
            Catch ex As Exception
                MsgBox("There was an error adding this Unit!!")
            Finally
                If cnnU2.State = ConnectionState.Open Then
                    cnnU2.Close()
                End If
            End Try
            intNewUnitID = System.Convert.ToInt32(prmNewUnitID.Value)

            'Add Unit Line Items

            Dim strLineItems As String
            strLineItems = "SELECT RecordSource, PartNumber, RT, Description," & _
                "Cost, Margin, Sell, Quantity, Location, Type, Shipdate " & _
                "FROM LineItem Where UnitID = " & intOldUnitID
            Dim cnnL1 As New SqlConnection
            cnnL1.ConnectionString = frmMain.strConnect

            Dim cmdLineItems As New SqlCommand(strLineItems, cnnL1)
            cmdLineItems.CommandType = CommandType.Text

            Dim drLineItems As SqlDataReader
            cnnL1.Open()

            drLineItems = cmdLineItems.ExecuteReader
            While drLineItems.Read

                Dim cnnL2 As New SqlConnection
                cnnL2.ConnectionString = frmMain.strConnect

                ''LineItem

                Dim cmdAddLineItem As New SqlCommand("AddLineItem", cnnL2)
                cmdAddLineItem.CommandType = CommandType.StoredProcedure

                Dim prmLJobID As SqlParameter = cmdAddLineItem.Parameters.Add("@ProjectID", SqlDbType.Int)
                prmLJobID.Value = frmMain.intJobID

                Dim prmLBldgID As SqlParameter = cmdAddLineItem.Parameters.Add("@BldgID", SqlDbType.Int)
                prmLBldgID.Value = intNewBldgID

                Dim prmLUnitID As SqlParameter = cmdAddLineItem.Parameters.Add("@UnitID", SqlDbType.Int)
                prmLUnitID.Value = intNewUnitID

                Dim prmRecordSource As SqlParameter = cmdAddLineItem.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
                prmRecordSource.Value = drLineItems.Item("RecordSource")

                Dim prmPartNumber As SqlParameter = cmdAddLineItem.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
                prmPartNumber.Value = drLineItems.Item("PartNumber")

                Dim prmRT As SqlParameter = cmdAddLineItem.Parameters.Add("@RT", SqlDbType.Char, 1)
                prmRT.Value = drLineItems.Item("RT")

                Dim prmLDescription As SqlParameter = cmdAddLineItem.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
                prmLDescription.Value = drLineItems.Item("Description")

                Dim prmCost As SqlParameter = cmdAddLineItem.Parameters.Add("@Cost", SqlDbType.Money)
                prmCost.Value = drLineItems.Item("Cost")

                Dim prmMargin As SqlParameter = cmdAddLineItem.Parameters.Add("@Margin", SqlDbType.Int)
                prmMargin.Value = drLineItems.Item("Margin")

                Dim prmSell As SqlParameter = cmdAddLineItem.Parameters.Add("@Sell", SqlDbType.Money)
                prmSell.Value = drLineItems.Item("Sell")

                Dim prmQuantity As SqlParameter = cmdAddLineItem.Parameters.Add("@Quantity", SqlDbType.Int)
                prmQuantity.Value = drLineItems.Item("Quantity")

                Dim prmLLocation As SqlParameter = cmdAddLineItem.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
                prmLLocation.Value = drLineItems.Item("Location")

                Dim prmLType As SqlParameter = cmdAddLineItem.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
                prmLType.Value = drLineItems.Item("Type")

                Dim prmLShipDate As SqlParameter = cmdAddLineItem.Parameters.Add("@ShipDate", SqlDbType.DateTime)
                prmLShipDate.Value = drLineItems.Item("ShipDate")

                Try
                    cnnL2.Open()

                    cmdAddLineItem.ExecuteNonQuery()
                    cnnL2.Close()
                Catch ex As Exception
                    MsgBox("There was an error adding this LineItems!!")
                Finally
                    If cnnL2.State = ConnectionState.Open Then
                        cnnL2.Close()
                    End If
                End Try

            End While
            drLineItems.Close()
            cnnL1.Close()
        End While
        drUnits.Close()
        cnnU1.Close()
        SetBldgShipDateR()

    End Sub

    Private Sub DeleteUnits()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "DELETE FROM Building WHERE BldgID = " & frmJob.intBldgID
        Dim cmdDeleteUnit As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdDeleteUnit.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error Deleting This Building!")
        Finally
            cnn.Close()
        End Try



    End Sub

    Private Sub mnuShip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShip.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim EditShipDate As New frmjb_Ship
        EditShipDate.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub frmj_bldg_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        LoadBldgDetails()
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        MsgBox("Got It")
    End Sub
    Private Sub SetBldgShipDateR()
        Dim cmdSetShipDateR As New SqlCommand("SetBldgShipDateR", cnn)
        cmdSetShipDateR.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdSetShipDateR.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = intNewBldgID
        Try
            cnn.Open()
            cmdSetShipDateR.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error Setting the R Ship Dates")
        Finally
            cnn.Close()
        End Try


    End Sub
End Class
