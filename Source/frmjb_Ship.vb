Imports System.Data
Imports System.Data.SqlClient
Public Class frmjb_Ship
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtOrigShipDate As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNewShipDate As System.Windows.Forms.TextBox
    Friend WithEvents cbxShipped As System.Windows.Forms.CheckBox
    Friend WithEvents btnEditShipDate As System.Windows.Forms.Button
    Friend WithEvents btnShipped As System.Windows.Forms.Button
    Friend WithEvents txtOrigShipDateR As System.Windows.Forms.TextBox
    Friend WithEvents txtNewShipDateR As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRShipped As System.Windows.Forms.Button
    Friend WithEvents cbxRShipped As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtOrigShipDate = New System.Windows.Forms.TextBox
        Me.txtNewShipDate = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cbxShipped = New System.Windows.Forms.CheckBox
        Me.btnEditShipDate = New System.Windows.Forms.Button
        Me.btnShipped = New System.Windows.Forms.Button
        Me.txtOrigShipDateR = New System.Windows.Forms.TextBox
        Me.txtNewShipDateR = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cbxRShipped = New System.Windows.Forms.CheckBox
        Me.btnRShipped = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtOrigShipDate
        '
        Me.txtOrigShipDate.Enabled = False
        Me.txtOrigShipDate.Location = New System.Drawing.Point(76, 32)
        Me.txtOrigShipDate.Name = "txtOrigShipDate"
        Me.txtOrigShipDate.Size = New System.Drawing.Size(136, 20)
        Me.txtOrigShipDate.TabIndex = 0
        Me.txtOrigShipDate.Text = ""
        Me.txtOrigShipDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNewShipDate
        '
        Me.txtNewShipDate.Location = New System.Drawing.Point(76, 56)
        Me.txtNewShipDate.Name = "txtNewShipDate"
        Me.txtNewShipDate.Size = New System.Drawing.Size(136, 20)
        Me.txtNewShipDate.TabIndex = 0
        Me.txtNewShipDate.Text = ""
        Me.txtNewShipDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Original"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "New"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(76, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Trim Ship Date"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxShipped
        '
        Me.cbxShipped.Enabled = False
        Me.cbxShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxShipped.Location = New System.Drawing.Point(76, 80)
        Me.cbxShipped.Name = "cbxShipped"
        Me.cbxShipped.Size = New System.Drawing.Size(80, 20)
        Me.cbxShipped.TabIndex = 3
        Me.cbxShipped.Text = "Trim Shipped"
        '
        'btnEditShipDate
        '
        Me.btnEditShipDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditShipDate.Location = New System.Drawing.Point(360, 56)
        Me.btnEditShipDate.Name = "btnEditShipDate"
        Me.btnEditShipDate.Size = New System.Drawing.Size(104, 20)
        Me.btnEditShipDate.TabIndex = 2
        Me.btnEditShipDate.Text = "Change Ship Date"
        '
        'btnShipped
        '
        Me.btnShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShipped.Location = New System.Drawing.Point(76, 104)
        Me.btnShipped.Name = "btnShipped"
        Me.btnShipped.Size = New System.Drawing.Size(136, 20)
        Me.btnShipped.TabIndex = 4
        Me.btnShipped.Text = "Mark Trim As Shipped"
        '
        'txtOrigShipDateR
        '
        Me.txtOrigShipDateR.Enabled = False
        Me.txtOrigShipDateR.Location = New System.Drawing.Point(220, 32)
        Me.txtOrigShipDateR.Name = "txtOrigShipDateR"
        Me.txtOrigShipDateR.Size = New System.Drawing.Size(136, 20)
        Me.txtOrigShipDateR.TabIndex = 8
        Me.txtOrigShipDateR.Text = ""
        Me.txtOrigShipDateR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNewShipDateR
        '
        Me.txtNewShipDateR.Location = New System.Drawing.Point(220, 56)
        Me.txtNewShipDateR.Name = "txtNewShipDateR"
        Me.txtNewShipDateR.Size = New System.Drawing.Size(136, 20)
        Me.txtNewShipDateR.TabIndex = 1
        Me.txtNewShipDateR.Text = ""
        Me.txtNewShipDateR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(220, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 20)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Rough Ship Date"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxRShipped
        '
        Me.cbxRShipped.Enabled = False
        Me.cbxRShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRShipped.Location = New System.Drawing.Point(220, 80)
        Me.cbxRShipped.Name = "cbxRShipped"
        Me.cbxRShipped.Size = New System.Drawing.Size(92, 20)
        Me.cbxRShipped.TabIndex = 11
        Me.cbxRShipped.Text = "Rough Shipped"
        '
        'btnRShipped
        '
        Me.btnRShipped.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRShipped.Location = New System.Drawing.Point(220, 104)
        Me.btnRShipped.Name = "btnRShipped"
        Me.btnRShipped.Size = New System.Drawing.Size(136, 20)
        Me.btnRShipped.TabIndex = 12
        Me.btnRShipped.Text = "Mark Rough As Shipped"
        '
        'frmjb_Ship
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(476, 130)
        Me.Controls.Add(Me.btnRShipped)
        Me.Controls.Add(Me.cbxRShipped)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtNewShipDateR)
        Me.Controls.Add(Me.txtOrigShipDateR)
        Me.Controls.Add(Me.btnShipped)
        Me.Controls.Add(Me.btnEditShipDate)
        Me.Controls.Add(Me.cbxShipped)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNewShipDate)
        Me.Controls.Add(Me.txtOrigShipDate)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmjb_Ship"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Ship Status"
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub frmjb_Ship_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtOrigShipDate.Text = frmj_bldg.strOldShipDate
        txtOrigShipDateR.Text = frmj_bldg.strOldShipDateR
        cbxShipped.Checked = frmj_bldg.blnShipped
        cbxRShipped.Checked = frmj_bldg.blnRShipped
        If frmj_bldg.blnShipped = True Then
            btnShipped.Text = "Change Trim to Not Shpd!"
        End If
        If frmj_bldg.blnRShipped = True Then
            btnRShipped.Text = "Change Rough to Not Shpd!"
        End If
    End Sub

    Private Sub btnEditShipDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditShipDate.Click
        
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdEditShipDate As New SqlCommand("EditShipDate", cnn)
        cmdEditShipDate.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdEditShipDate.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = frmJob.intBldgID

        Dim prmShipDate As SqlParameter = cmdEditShipDate.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtNewShipDate.Text) Then
            prmShipDate.Value = txtNewShipDate.Text
        Else
            prmShipDate.Value = txtOrigShipDate.Text
        End If

        Dim prmShipDateR As SqlParameter = cmdEditShipDate.Parameters.Add("@shipdateR", SqlDbType.DateTime)
        If IsDate(txtNewShipDateR.Text) Then
            prmShipDateR.Value = txtNewShipDateR.Text
        Else
            prmShipDateR.Value = txtOrigShipDateR.Text
        End If


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdEditShipDate.ExecuteNonQuery()
            'cnn.Close()
        Catch ex As Exception
            '    MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        Me.Close()
        
    End Sub

    Private Sub btnShipped_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShipped.Click
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdEditShipped As New SqlCommand("EditTShipped", cnn)
        cmdEditShipped.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdEditShipped.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = frmJob.intBldgID

        Dim prmShipped As SqlParameter = cmdEditShipped.Parameters.Add("@shipped", SqlDbType.Bit)
        If frmj_bldg.blnShipped = True Then
            prmShipped.Value = 0
        Else
            prmShipped.Value = 1
        End If


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdEditShipped.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        frmj_bldg.blnShipped = True
        Me.Close()
    End Sub

    
    Private Sub btnRShipped_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRShipped.Click
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdEditShipped As New SqlCommand("EditRShipped", cnn)
        cmdEditShipped.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdEditShipped.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = frmJob.intBldgID

        Dim prmShipped As SqlParameter = cmdEditShipped.Parameters.Add("@shipped", SqlDbType.Bit)
        If frmj_bldg.blnRShipped = True Then
            prmShipped.Value = 0
        Else
            prmShipped.Value = 1
        End If


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdEditShipped.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        frmj_bldg.blnRShipped = True
        Me.Close()
    End Sub

End Class
