Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmRptUnitDetails
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(528, 298)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'frmRptUnitDetails
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(528, 298)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.MinimizeBox = False
        Me.Name = "frmRptUnitDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRptUnitDetails"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRptUnitDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadReport()
    End Sub

    Private Sub LoadReport()
        cnn.ConnectionString = "workstation id=""CO-TECH-LT-003"";user id=SA;data source=""CO-TECH-LT-003"";initial catalog=LD-BID;password=VW9110"

        Dim strUnitDetail As String
        strUnitDetail = "rptQuantityDetails"
        Dim cmdUnitDetail As New SqlCommand
        cmdUnitDetail.Connection = cnn
        cmdUnitDetail.CommandText = strUnitDetail
        cmdUnitDetail.CommandType = CommandType.StoredProcedure
        Dim prmUnitDetailBidID As SqlParameter = cmdUnitDetail.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitDetailBidID.Value = frmMain.intBidID
        Dim daUnitDetail As New SqlDataAdapter(cmdUnitDetail)
        Dim dsUnitDetail As New DataSet
        Try
            daUnitDetail.Fill(dsUnitDetail, "rptQuantityDetails")
        Catch ex As Exception
            MsgBox("Failed to Fill rptQuantityDetails")
        End Try

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        myReport = New rptUnitDetails
        myReport.SetDataSource(dsUnitDetail.Tables("rptQuantityDetails"))
        Dim myFieldSell As CrystalDecisions.CrystalReports.Engine.ReportObject
        myFieldSell = myReport.ReportDefinition.ReportObjects.Item("Field9")
        myFieldSell.ObjectFormat.EnableSuppress = True

        Dim myFieldTotalSell As CrystalDecisions.CrystalReports.Engine.ReportObject
        myFieldTotalSell = myReport.ReportDefinition.ReportObjects.Item("Field18")
        myFieldTotalSell.ObjectFormat.EnableSuppress = True

        Dim myFieldGTSell As CrystalDecisions.CrystalReports.Engine.ReportObject
        myFieldGTSell = myReport.ReportDefinition.ReportObjects.Item("Field19")
        myFieldGTSell.ObjectFormat.EnableSuppress = True

        Dim myTextSell As CrystalDecisions.CrystalReports.Engine.ReportObject
        myTextSell = myReport.ReportDefinition.ReportObjects.Item("Text10")
        myTextSell.ObjectFormat.EnableSuppress = True

        Dim myTextTotSell As CrystalDecisions.CrystalReports.Engine.ReportObject
        myTextTotSell = myReport.ReportDefinition.ReportObjects.Item("Text11")
        myTextTotSell.ObjectFormat.EnableSuppress = True



        CrystalReportViewer1.ReportSource = myReport
    End Sub

End Class
