Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmRptSSMain
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim strUnitsCost As String = "0"
    Dim strUnitsMargin As String = "0"
    Dim strUnits As String = "0"
    Dim strSiteCost As String = "0"
    Dim strSiteMargin As String = "0"
    Dim strSite As String = "0"
    Dim strClubhouseCost As String = "0"
    Dim StrClubhouseMargin As String = "0"
    Dim strClubhouse As String = "0"
    Dim strCommonCost As String = "0"
    Dim StrCommonMargin As String = "0"
    Dim strAux As String = "0"
    Dim strAuxCost As String = "0"
    Dim strAuxMargin As String = "0"
    Dim strCommon As String = "0"
    Dim strPreTaxTotal As String = "0"
    Dim strTaxRate As String = "0"
    Dim strTaxTotal As String = "0"
    Dim strTotalCost As String = "0"
    Dim strGrandTotal As String = "0"
    Dim strTotalMargin As String = "0"
    Dim strrptUnitCount As String
    Dim strrptBldgCount As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(580, 266)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'frmRptSSMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(580, 266)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.MinimizeBox = False
        Me.Name = "frmRptSSMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRptSSMain"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRptSSMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, CrystalReportViewer1.Load
        LoadSubTotals()
        LoadCounts()
        LoadReport()
    End Sub

    Private Sub LoadSubTotals()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdMargins As New SqlCommand("Margins", cnn)
        cmdMargins.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drMargins As SqlDataReader
        cnn.Open()
        drMargins = cmdMargins.ExecuteReader

        While drMargins.Read
            If drMargins.GetString(0) = "U" Then
                strUnitsCost = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strUnits = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                strUnitsMargin = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2), 2))
            ElseIf drMargins.GetString(0) = "C" Then
                strClubhouseCost = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strClubhouse = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                StrClubhouseMargin = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2), 2))
            ElseIf drMargins.GetString(0) = "S" Then
                strSiteCost = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strSite = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                strSiteMargin = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2), 2))
            ElseIf drMargins.GetString(0) = "X" Then
                strCommonCost = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strCommon = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                StrCommonMargin = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2), 2))
            ElseIf drMargins.GetString(0) = "A" Then
                strAuxCost = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strAux = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                strAuxMargin = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2), 2))
            End If

        End While
        drMargins.Close()
        cnn.Close()


        strPreTaxTotal = Format(CDbl(strUnits) + CDbl(strSite) + CDbl(strClubhouse) + CDbl(strCommon) + CDbl(strAux), "C")
        strTotalCost = Format(CDbl(strUnitsCost) + CDbl(strSiteCost) + CDbl(strClubhouseCost) + CDbl(strCommonCost) + CDbl(strAuxCost), "C")
        strTotalMargin = CStr(FormatPercent(CDbl(1 - CDbl(strTotalCost) / CDbl(strPreTaxTotal)), 2))

        Dim cmdSalesTaxRate As New SqlCommand("rptSalesTaxRate", cnn)
        cmdSalesTaxRate.CommandType = CommandType.StoredProcedure
        Dim prmSalesTaxBidID As SqlParameter = cmdSalesTaxRate.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSalesTaxBidID.Value = frmMain.intBidID
        Dim drSalesTaxRate As SqlDataReader = Nothing
        cnn.Open()
        Try
            drSalesTaxRate = cmdSalesTaxRate.ExecuteReader
            drSalesTaxRate.Read()
            strTaxRate = drSalesTaxRate.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drSalesTaxRate.Close()
            cnn.Close()
        End Try


        strTaxTotal = Format(CDbl(strPreTaxTotal) * CDbl(strTaxRate), "C")
        strGrandTotal = Format(CDbl(strPreTaxTotal) + CDbl(strTaxTotal), "C")

    End Sub

    Private Sub LoadCounts()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdUnitCount As New SqlCommand("rptUnitCount", cnn)
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drUnitCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drUnitCount = cmdUnitCount.ExecuteReader
            drUnitCount.Read()
            strrptUnitCount = drUnitCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drUnitCount.Close()
            cnn.Close()
        End Try

        Dim cmdBldgCount As New SqlCommand("rptBldgCount", cnn)
        cmdBldgCount.CommandType = CommandType.StoredProcedure
        Dim prmBldgBidID As SqlParameter = cmdBldgCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBldgBidID.Value = frmMain.intBidID
        Dim drBldgCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drBldgCount = cmdBldgCount.ExecuteReader
            drBldgCount.Read()
            strrptBldgCount = drBldgCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drBldgCount.Close()
            cnn.Close()
        End Try

    End Sub

    Private Sub LoadReport()

        Dim curRpt As String
        Dim LineItemPrimary As String

        LineItemPrimary = "Select pn PartNumber, type, descr Description, Quantity, co Cost, se Sell, " + _
            "(Quantity * Co) as [Total Cost], (Quantity * Se) as [Total Sell], " + _
            "(Quantity * Se) - (Quantity * Co) as Net "

        Dim LineItemSecondary As String
        LineItemSecondary = "from(select type, Description Descr, PartNumber pn, Cost co, Margin ma, Sell se," + _
            " sum(Quantity) AS Quantity, RT from LineItem where ProjectID = " + frmMain.intBidID.ToString

        Dim LineItemThird As String
        LineItemThird = " and BldgID is not null group by type, RT, PartNumber , Description, Cost, Margin, Sell" + _
            " ) tempTable "

        Dim OrderBy As String

        curRpt = "rptIntro"
        Dim dsBid As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "Bids")

        OrderBy = "Order By PartNumber"
        LineItemPrimary = "Select pn PartNumber, Type, descr Description, Quantity, co Cost, se Sell, " + _
            "(Quantity * Co) as [Total Cost], (Quantity * Se) as [Total Sell], " + _
            "(Quantity * Se) - (Quantity * Co) as Net "
        curRpt = LineItemPrimary + LineItemSecondary + LineItemThird + OrderBy
        Dim dsSummary As DataSet = ExecuteSQL(curRpt, cnn, "b_LineItemSummary")

        curRpt = "rptBldgTypeCount"
        Dim dsBldgTypeCount As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptBldgTypeCount")

        curRpt = "rptUnitDetailPerBldg"
        Dim dsUnitDetailPerBldg As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptUnitDetailPerBldg")

        curRpt = "ProjectUnitCount"
        Dim dsUnitCount As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_UnitCount")

        curRpt = "rptAllUnitDetails"
        Dim dsUnitDetail As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptAllUnitDetails")

        curRpt = "UnitsPerAllBuilding"
        Dim dsUnitsPerAllBuilding As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_UnitsPerAllBuilding")

        curRpt = "select bldgNumber, building.Type, building.Description as BldgDesc, Building.Location, Address, Building.Shipdate, Building.shipsequence," + _
            " lineitem.PartNumber, SUM(Lineitem.Quantity) As Total, LineItem.Description, LineItem.Cost, LineItem.Sell from Building inner join LineItem" + _
            " on building.BldgID = LineItem.BldgID where Building.ProjectID = " + frmMain.intBidID.ToString + " group by bldgNumber, building.Type," + _
            " building.Description, Building.Location, Address, Building.Shipdate, Building.shipsequence, lineitem.PartNumber, LineItem.Description," + _
            " LineItem.Cost, LineItem.Sell order by bldgNumber desc"
        Dim dsBldgBOM As DataSet = ExecuteSQL(curRpt, cnn, "b_BldgBOM")

        Dim strUnitSummary As String
        strUnitSummary = "UnitSummary"
        Dim cmdUnitSummary As New SqlCommand
        cmdUnitSummary.Connection = cnn
        cmdUnitSummary.CommandText = strUnitSummary
        cmdUnitSummary.CommandType = CommandType.StoredProcedure

        Dim prmUnitSummaryBidID As SqlParameter = cmdUnitSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitSummaryBidID.Value = frmMain.intBidID
        Dim prmUnitType As SqlParameter
        prmUnitType = cmdUnitSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        prmUnitType.Value = "U"

        Dim prmUnitOrder As SqlParameter = cmdUnitSummary.Parameters.Add("@OrderBy", SqlDbType.Char, 2)
        prmUnitOrder.Value = "PartNumber"

        Dim daUnitSummary As New SqlDataAdapter(cmdUnitSummary)
        Dim dsUnitSummary As New DataSet
        Try
            daUnitSummary.Fill(dsUnitSummary, "b_UnitSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_UnitSummary")
        End Try

        Dim strCommonSummary As String
        strCommonSummary = "CommonSummary"
        Dim cmdCommonSummary As New SqlCommand
        cmdCommonSummary.Connection = cnn
        cmdCommonSummary.CommandText = strCommonSummary
        cmdCommonSummary.CommandType = CommandType.StoredProcedure
        Dim prmCommonSummaryBidID As SqlParameter = cmdCommonSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmCommonSummaryBidID.Value = frmMain.intBidID

        Dim prmCommonOrder As SqlParameter = cmdCommonSummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        prmCommonOrder.Value = "PartNumber"

        Dim daCommonSummary As New SqlDataAdapter(cmdCommonSummary)
        Dim dsCommonSummary As New DataSet
        Try
            daCommonSummary.Fill(dsCommonSummary, "b_CommonSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_CommonSummary")
        End Try

        Dim strClubHouseSummary As String
        strClubHouseSummary = "ClubHouseSummary"
        Dim cmdClubHouseSummary As New SqlCommand
        cmdClubHouseSummary.Connection = cnn
        cmdClubHouseSummary.CommandText = strClubHouseSummary
        cmdClubHouseSummary.CommandType = CommandType.StoredProcedure
        Dim prmClubHouseSummaryBidID As SqlParameter = cmdClubHouseSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmClubHouseSummaryBidID.Value = frmMain.intBidID

        Dim prmCllubHouseOrder As SqlParameter = cmdClubHouseSummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        prmCllubHouseOrder.Value = "PartNumber"

        Dim daClubHouseSummary As New SqlDataAdapter(cmdClubHouseSummary)
        Dim dsClubHouseSummary As New DataSet
        Try
            daClubHouseSummary.Fill(dsClubHouseSummary, "b_ClubHouseSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_ClubHouseSummary")
        End Try

        Dim strAuxiliarySummary As String
        strAuxiliarySummary = "AuxillarySummary"
        Dim cmdAuxiliarySummary As New SqlCommand
        cmdAuxiliarySummary.Connection = cnn
        cmdAuxiliarySummary.CommandText = strAuxiliarySummary
        cmdAuxiliarySummary.CommandType = CommandType.StoredProcedure
        Dim prmAuxiliarySummaryBidID As SqlParameter = cmdAuxiliarySummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmAuxiliarySummaryBidID.Value = frmMain.intBidID

        Dim prmAuxiliaryOrder As SqlParameter = cmdAuxiliarySummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        prmAuxiliaryOrder.Value = "PartNumber"

        Dim daAuxiliarySummary As New SqlDataAdapter(cmdAuxiliarySummary)
        Dim dsAuxiliarySummary As New DataSet
        Try
            daAuxiliarySummary.Fill(dsAuxiliarySummary, "b_AuxillarySummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_AuxiliarySummary")
        End Try

        Dim strSiteSummary As String
        strSiteSummary = "UnitSummary"
        Dim cmdSiteSummary As New SqlCommand
        cmdSiteSummary.Connection = cnn
        cmdSiteSummary.CommandText = strSiteSummary
        cmdSiteSummary.CommandType = CommandType.StoredProcedure
        Dim prmSiteSummaryBidID As SqlParameter = cmdSiteSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSiteSummaryBidID.Value = frmMain.intBidID

        prmUnitType = cmdSiteSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        prmUnitType.Value = "S"

        Dim prmSiteOrder As SqlParameter = cmdSiteSummary.Parameters.Add("@OrderBy", SqlDbType.VarChar, 15)
        prmSiteOrder.Value = "PartNumber"

        Dim daSiteSummary As New SqlDataAdapter(cmdSiteSummary)
        Dim dsSiteSummary As New DataSet
        Try
            daSiteSummary.Fill(dsSiteSummary, "b_SiteSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_SiteSummary")
        End Try

        curRpt = "AlternateSummary"
        Dim dsAlternates As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_AlternateSummary")

        curRpt = "rptComments"
        Dim dsBidComments As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptBidComments")

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Try
            myReport = New rptSSMain
            myReport.SetDataSource(dsBid)
            myReport.OpenSubreport("rptBidComments.rpt").SetDataSource(dsBidComments)
            myReport.OpenSubreport("rptBldgTypeCount.rpt").SetDataSource(dsBldgTypeCount)
            myReport.OpenSubreport("rptUnitCount.rpt").SetDataSource(dsUnitCount)
            myReport.OpenSubreport("rptUnitDetailPerBldg1.rpt").SetDataSource(dsUnitDetailPerBldg)
            myReport.OpenSubreport("rptUnitsPerAllBldg.rpt").SetDataSource(dsUnitsPerAllBuilding)
            myReport.OpenSubreport("rptBldgBOM.rpt").SetDataSource(dsBldgBOM)
            myReport.OpenSubreport("rptSSAllUnitDetails.rpt").SetDataSource(dsUnitDetail)
            myReport.OpenSubreport("rptUnitSummary.rpt").SetDataSource(dsUnitSummary)
            myReport.OpenSubreport("rptCommonSummary.rpt").SetDataSource(dsCommonSummary)
            myReport.OpenSubreport("rptClubHouseSummary.rpt").SetDataSource(dsClubHouseSummary)
            myReport.OpenSubreport("rptAuxiliarySummary.rpt").SetDataSource(dsAuxiliarySummary)
            myReport.OpenSubreport("rptSiteSummary.rpt").SetDataSource(dsSiteSummary)
            myReport.OpenSubreport("rptSSLineItemSummary.rpt").SetDataSource(dsSummary)
            myReport.OpenSubreport("rptBidAlternates.rpt").SetDataSource(dsAlternates)
        Catch ex As Exception
            MsgBox("Failed to Open Subreports")
        End Try

        Dim UnitsCostText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitsCostText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnitsCost"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitsCostText.Text = strUnitsCost

        Dim UnitsText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitsText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnits"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitsText.Text = strUnits

        Dim UnitsMarginText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitsMarginText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnitsMargin"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitsMarginText.Text = strUnitsMargin

        Dim SiteCostText As CrystalDecisions.CrystalReports.Engine.TextObject
        SiteCostText = CType(myReport.ReportDefinition.ReportObjects.Item("txtSiteCost"), CrystalDecisions.CrystalReports.Engine.TextObject)
        SiteCostText.Text = strSiteCost

        Dim SiteText As CrystalDecisions.CrystalReports.Engine.TextObject
        SiteText = CType(myReport.ReportDefinition.ReportObjects.Item("txtSite"), CrystalDecisions.CrystalReports.Engine.TextObject)
        SiteText.Text = strSite

        Dim SiteMarginText As CrystalDecisions.CrystalReports.Engine.TextObject
        SiteMarginText = CType(myReport.ReportDefinition.ReportObjects.Item("txtSiteMargin"), CrystalDecisions.CrystalReports.Engine.TextObject)
        SiteMarginText.Text = strSiteMargin

        Dim ClubhouseCostText As CrystalDecisions.CrystalReports.Engine.TextObject
        ClubhouseCostText = CType(myReport.ReportDefinition.ReportObjects.Item("txtClubhouseCost"), CrystalDecisions.CrystalReports.Engine.TextObject)
        ClubhouseCostText.Text = strClubhouseCost

        Dim ClubhouseText As CrystalDecisions.CrystalReports.Engine.TextObject
        ClubhouseText = CType(myReport.ReportDefinition.ReportObjects.Item("txtClubhouse"), CrystalDecisions.CrystalReports.Engine.TextObject)
        ClubhouseText.Text = strClubhouse

        Dim ClubhouseMarginText As CrystalDecisions.CrystalReports.Engine.TextObject
        ClubhouseMarginText = CType(myReport.ReportDefinition.ReportObjects.Item("txtClubhouseMargin"), CrystalDecisions.CrystalReports.Engine.TextObject)
        ClubhouseMarginText.Text = StrClubhouseMargin

        Dim CommonCostText As CrystalDecisions.CrystalReports.Engine.TextObject
        CommonCostText = CType(myReport.ReportDefinition.ReportObjects.Item("txtCommonCost"), CrystalDecisions.CrystalReports.Engine.TextObject)
        CommonCostText.Text = strCommonCost

        Dim CommonText As CrystalDecisions.CrystalReports.Engine.TextObject
        CommonText = CType(myReport.ReportDefinition.ReportObjects.Item("txtCommon"), CrystalDecisions.CrystalReports.Engine.TextObject)
        CommonText.Text = strCommon

        Dim CommonMarginText As CrystalDecisions.CrystalReports.Engine.TextObject
        CommonMarginText = CType(myReport.ReportDefinition.ReportObjects.Item("txtCommonMargin"), CrystalDecisions.CrystalReports.Engine.TextObject)
        CommonMarginText.Text = StrCommonMargin

        Dim AuxCostText As CrystalDecisions.CrystalReports.Engine.TextObject
        AuxCostText = CType(myReport.ReportDefinition.ReportObjects.Item("txtAuxCost"), CrystalDecisions.CrystalReports.Engine.TextObject)
        AuxCostText.Text = strAuxCost

        Dim AuxText As CrystalDecisions.CrystalReports.Engine.TextObject
        AuxText = CType(myReport.ReportDefinition.ReportObjects.Item("txtAuxSell"), CrystalDecisions.CrystalReports.Engine.TextObject)
        AuxText.Text = strAux

        Dim AuxMarginText As CrystalDecisions.CrystalReports.Engine.TextObject
        AuxMarginText = CType(myReport.ReportDefinition.ReportObjects.Item("txtAuxMargin"), CrystalDecisions.CrystalReports.Engine.TextObject)
        AuxMarginText.Text = strAuxMargin

        Dim PreTaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        PreTaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtPreTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        PreTaxTotalText.Text = strPreTaxTotal

        Dim TaxRateText As CrystalDecisions.CrystalReports.Engine.TextObject
        TaxRateText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxRate"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TaxRateText.Text = FormatPercent(CDbl(strTaxRate), 2)

        Dim TaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        TaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TaxTotalText.Text = strTaxTotal

        Dim TotalCostText As CrystalDecisions.CrystalReports.Engine.TextObject
        TotalCostText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTotalCost"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TotalCostText.Text = strTotalCost

        Dim GrandTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        GrandTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtGrandTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        GrandTotalText.Text = strGrandTotal

        Dim TotalMarginText As CrystalDecisions.CrystalReports.Engine.TextObject
        TotalMarginText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTotalMargin"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TotalMarginText.Text = strTotalMargin

        Dim UnitCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnitCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitCountText.Text = strrptUnitCount

        Dim BldgCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        BldgCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtBldgCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        BldgCountText.Text = strrptBldgCount

        If dsBidComments.Tables(0).Rows.Count = 0 Then
            Dim MySectionComment As CrystalDecisions.CrystalReports.Engine.Section
            MySectionComment = myReport.ReportDefinition.Sections.Item("Section17")
            MySectionComment.SectionFormat.EnableSuppress = True
        End If

        CrystalReportViewer1.ReportSource = myReport
    End Sub

End Class
