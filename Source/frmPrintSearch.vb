Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmPrintSearch
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(508, 266)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'frmPrintSearch
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(508, 266)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmPrintSearch"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Search Results"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadReport()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSearch As String
        strSearch = "SELECT distinct LineItem.PartNumber, LineItem.Quantity, LineItem.Location, "
        strSearch = strSearch & "LineItem.Type, Unit.UnitType, Unit.Description "
        strSearch = strSearch & " FROM Unit INNER JOIN LineITem ON Unit.UnitID = LineItem.UnitID "
        strSearch = strSearch & " WHERE (LineItem.ProjectID = " & frmMain.intBidID.ToString & ")"
        If frmBid.strSPartNumber <> "" Then
            strSearch = strSearch & " AND (Lineitem.PartNumber = '" & frmBid.strSPartNumber & "')"
        End If
        If frmBid.strSType <> "" Then
            strSearch = strSearch & " AND (Lineitem.Type = '" & frmBid.strSType & "')"
        End If
        If frmBid.strSLocation <> "" Then
            strSearch = strSearch & " AND (Lineitem.Location = '" & frmBid.strSLocation & "')"
        End If

        Dim cmdSearch As New SqlCommand(strSearch, cnn)
        cmdSearch.CommandType = CommandType.Text

        Dim daSearch As New SqlDataAdapter(cmdSearch)
        Dim dsSearch As New DataSet
        Try
            daSearch.Fill(dsSearch, "b_SearchPrint")
        Catch ex As Exception
            MsgBox("Error Loading Search DataGrid: " + ex.ToString)
        End Try

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        myReport = New rptPrintSearch
        myReport.SetDataSource(dsSearch.Tables("b_SearchPrint"))
        CrystalReportViewer1.ReportSource = myReport
    End Sub

    Private Sub frmPrintSearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadReport()
    End Sub
End Class
