Imports System.Data
Imports System.Data.SqlClient
Public Class frmProductSearch
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim blnStartParts As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboParts As System.Windows.Forms.ComboBox
    Friend WithEvents dgPartsDetails As System.Windows.Forms.DataGrid
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgPartSearchStyle As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents pBidId As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents pPartNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents pDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents pCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents pMargin As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents pSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents lblBid As System.Windows.Forms.Label
    Friend WithEvents lblPart As System.Windows.Forms.Label
    Friend WithEvents lblQuan As System.Windows.Forms.Label
    Friend WithEvents lblBidl As System.Windows.Forms.Label
    Friend WithEvents lblPartl As System.Windows.Forms.Label
    Friend WithEvents lblQuanl As System.Windows.Forms.Label
    Friend WithEvents pEntryDate As System.Windows.Forms.DataGridTextBoxColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboParts = New System.Windows.Forms.ComboBox()
        Me.dgPartsDetails = New System.Windows.Forms.DataGrid()
        Me.dgPartSearchStyle = New System.Windows.Forms.DataGridTableStyle()
        Me.pPartNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.pDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.pCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.pMargin = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.pSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.pBidId = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.pEntryDate = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblBid = New System.Windows.Forms.Label()
        Me.lblPart = New System.Windows.Forms.Label()
        Me.lblQuan = New System.Windows.Forms.Label()
        Me.lblBidl = New System.Windows.Forms.Label()
        Me.lblPartl = New System.Windows.Forms.Label()
        Me.lblQuanl = New System.Windows.Forms.Label()
        CType(Me.dgPartsDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboParts
        '
        Me.cboParts.Location = New System.Drawing.Point(12, 36)
        Me.cboParts.Name = "cboParts"
        Me.cboParts.Size = New System.Drawing.Size(360, 21)
        Me.cboParts.TabIndex = 1
        '
        'dgPartsDetails
        '
        Me.dgPartsDetails.DataMember = ""
        Me.dgPartsDetails.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgPartsDetails.Location = New System.Drawing.Point(12, 84)
        Me.dgPartsDetails.Name = "dgPartsDetails"
        Me.dgPartsDetails.Size = New System.Drawing.Size(708, 260)
        Me.dgPartsDetails.TabIndex = 0
        Me.dgPartsDetails.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgPartSearchStyle})
        '
        'dgPartSearchStyle
        '
        Me.dgPartSearchStyle.DataGrid = Me.dgPartsDetails
        Me.dgPartSearchStyle.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.pBidId, Me.pPartNumber, Me.pDesc, Me.pCost, Me.pMargin, Me.pSell, Me.pEntryDate})
        Me.dgPartSearchStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgPartSearchStyle.MappingName = "LineItems"
        '
        'pPartNumber
        '
        Me.pPartNumber.Format = ""
        Me.pPartNumber.FormatInfo = Nothing
        Me.pPartNumber.HeaderText = "Part Number"
        Me.pPartNumber.MappingName = "PartNumber"
        Me.pPartNumber.NullText = ""
        Me.pPartNumber.Width = 125
        '
        'pDesc
        '
        Me.pDesc.Format = ""
        Me.pDesc.FormatInfo = Nothing
        Me.pDesc.HeaderText = "Description"
        Me.pDesc.MappingName = "Description"
        Me.pDesc.NullText = ""
        Me.pDesc.Width = 175
        '
        'pCost
        '
        Me.pCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.pCost.Format = "C"
        Me.pCost.FormatInfo = Nothing
        Me.pCost.HeaderText = "Cost -"
        Me.pCost.MappingName = "Cost"
        Me.pCost.NullText = ""
        Me.pCost.Width = 75
        '
        'pMargin
        '
        Me.pMargin.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.pMargin.Format = ""
        Me.pMargin.FormatInfo = Nothing
        Me.pMargin.HeaderText = "Margin"
        Me.pMargin.MappingName = "Margin"
        Me.pMargin.NullText = ""
        Me.pMargin.Width = 75
        '
        'pSell
        '
        Me.pSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.pSell.Format = "C"
        Me.pSell.FormatInfo = Nothing
        Me.pSell.HeaderText = "Sell -"
        Me.pSell.MappingName = "Sell"
        Me.pSell.NullText = ""
        Me.pSell.Width = 75
        '
        'pBidId
        '
        Me.pBidId.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.pBidId.Format = ""
        Me.pBidId.FormatInfo = Nothing
        Me.pBidId.HeaderText = "Bid"
        Me.pBidId.MappingName = "ProjectID"
        Me.pBidId.Width = 50
        '
        'pEntryDate
        '
        Me.pEntryDate.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.pEntryDate.Format = "d"
        Me.pEntryDate.FormatInfo = Nothing
        Me.pEntryDate.HeaderText = "Entry Date"
        Me.pEntryDate.MappingName = "ProjectDate"
        Me.pEntryDate.Width = 75
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(228, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select A Part From This List"
        '
        'lblBid
        '
        Me.lblBid.Location = New System.Drawing.Point(28, 368)
        Me.lblBid.Name = "lblBid"
        Me.lblBid.Size = New System.Drawing.Size(96, 20)
        Me.lblBid.TabIndex = 3
        '
        'lblPart
        '
        Me.lblPart.Location = New System.Drawing.Point(144, 368)
        Me.lblPart.Name = "lblPart"
        Me.lblPart.Size = New System.Drawing.Size(160, 20)
        Me.lblPart.TabIndex = 4
        '
        'lblQuan
        '
        Me.lblQuan.Location = New System.Drawing.Point(348, 368)
        Me.lblQuan.Name = "lblQuan"
        Me.lblQuan.Size = New System.Drawing.Size(100, 20)
        Me.lblQuan.TabIndex = 5
        '
        'lblBidl
        '
        Me.lblBidl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBidl.Location = New System.Drawing.Point(28, 348)
        Me.lblBidl.Name = "lblBidl"
        Me.lblBidl.Size = New System.Drawing.Size(84, 12)
        Me.lblBidl.TabIndex = 6
        Me.lblBidl.Text = "Bid"
        '
        'lblPartl
        '
        Me.lblPartl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartl.Location = New System.Drawing.Point(144, 348)
        Me.lblPartl.Name = "lblPartl"
        Me.lblPartl.Size = New System.Drawing.Size(76, 12)
        Me.lblPartl.TabIndex = 7
        Me.lblPartl.Text = "Part"
        '
        'lblQuanl
        '
        Me.lblQuanl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuanl.Location = New System.Drawing.Point(348, 348)
        Me.lblQuanl.Name = "lblQuanl"
        Me.lblQuanl.Size = New System.Drawing.Size(80, 12)
        Me.lblQuanl.TabIndex = 8
        Me.lblQuanl.Text = "Quantity"
        '
        'frmProductSearch
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(732, 402)
        Me.Controls.Add(Me.lblQuanl)
        Me.Controls.Add(Me.lblPartl)
        Me.Controls.Add(Me.lblBidl)
        Me.Controls.Add(Me.lblQuan)
        Me.Controls.Add(Me.lblPart)
        Me.Controls.Add(Me.lblBid)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgPartsDetails)
        Me.Controls.Add(Me.cboParts)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProductSearch"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Search For Parts Used In Bids"
        CType(Me.dgPartsDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmProductSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        blnStartParts = True
        LoadParts()
    End Sub

    Private Sub LoadParts()
        'frmBid
        cnn.ConnectionString = frmMain.strConnect
        Dim strParts As String
        strParts = "SELECT DISTINCT PartNumber FROM LineItem where projectid in (select projectid from project where " + _
            "ProjectType = 'bid' ) ORDER BY partnumber"
        Dim dsParts As New DataSet
        dsParts = ExecuteSQL(strParts, cnn, "Parts")
        cboParts.DataSource = dsParts.Tables("Parts")
        cboParts.DisplayMember = "PartNumber"
        blnStartParts = False
    End Sub

    Private Sub LoadPartDetails()
        If blnStartParts = True Then
            Exit Sub
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim strSQL As String
        strSQL = "SELECT LineItem.ProjectID, PartNumber, LineItem.Description, Cost, Margin, Sell, Project.ProjectDate " + _
        "FROM LineItem INNER JOIN Project ON LineItem.ProjectID = Project.ProjectID WHERE partnumber = '" + cboParts.Text & "' and ProjectType = 'bid' " + _
        "Group by LineItem.ProjectID, PartNumber, LineItem.Description, Cost, Margin, Sell, Project.ProjectDate Order by LineItem.ProjectID"

        Dim dsPartsDetails As New DataSet
        dsPartsDetails = ExecuteSQL(strSQL, cnn, "LineItems")
        dgPartsDetails.DataSource = dsPartsDetails.Tables("LineItems")
        
    End Sub

    Private Sub dgPartsDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgPartsDetails.Click
        lblBid.Text = "LD-" & dgPartsDetails.Item(dgPartsDetails.CurrentRowIndex, 0).ToString
        lblPart.Text = dgPartsDetails.Item(dgPartsDetails.CurrentRowIndex, 1).ToString
        lblBidl.Visible = True
        lblPartl.Visible = True
        lblQuanl.Visible = True
        Dim spPartCount As String = "PartCount"
        Dim cmdPartCount As New SqlCommand(spPartCount, cnn)
        cmdPartCount.CommandType = CommandType.StoredProcedure
        cmdPartCount.Parameters.AddWithValue("@ProjectID", CInt(dgPartsDetails.Item(dgPartsDetails.CurrentRowIndex, 0).ToString))
        cmdPartCount.Parameters.AddWithValue("@PartNumber", lblPart.Text)
        cmdPartCount.Parameters.AddWithValue("@ProjectType", "Bid")
        Dim drPartCount As SqlDataReader
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        drPartCount = cmdPartCount.ExecuteReader
        If drPartCount.Read() Then
            lblQuan.Text = drPartCount.Item("Quantity").ToString
        Else
            lblQuan.Text = "0"
        End If


        drPartCount.Close()
        cnn.Close()

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub SelectedPartChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParts.SelectionChangeCommitted
        LoadPartDetails()
        lblBidl.Visible = False
        lblPartl.Visible = False
        lblQuanl.Visible = False
        lblBid.Text = ""
        lblPart.Text = ""
        lblQuan.Text = ""
    End Sub
End Class
