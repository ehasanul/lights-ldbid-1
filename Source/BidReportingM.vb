﻿Imports System.Data.SqlClient
'Bid Reporting Module
'Eventually this will get boiled down and combined with the job module
'and become a reporting class
Module BidReportingM
    Public Sub LoadSubTotals(ByRef unitCount As String, ByRef clubHouseCount As String, ByRef siteCount As String, ByRef commonCount As String,
                             ByRef auxCount As String, ByRef preTaxTotal As String, ByRef taxRate As String, ByRef taxTotal As String, ByRef grandTotal As String,
                             ByVal cnn As SqlConnection)
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdMargins As New SqlCommand("Margins", cnn)
        cmdMargins.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drMargins As SqlDataReader
        cnn.Open()
        drMargins = cmdMargins.ExecuteReader

        unitCount = "0"
        clubHouseCount = "0"
        siteCount = "0"
        commonCount = "0"
        auxCount = "0"

        While drMargins.Read
            If drMargins.GetString(0) = "U" Then
                unitCount = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "C" Then
                clubHouseCount = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "S" Then
                siteCount = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "X" Then
                commonCount = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "A" Then
                auxCount = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            End If
        End While

        drMargins.Close()
        cnn.Close()


        preTaxTotal = Format(CDbl(unitCount) + CDbl(siteCount) + CDbl(clubHouseCount) + CDbl(commonCount) + CDbl(auxCount), "C")

        Dim cmdSalesTaxRate As New SqlCommand("rptSalesTaxRate", cnn)
        cmdSalesTaxRate.CommandType = CommandType.StoredProcedure
        Dim prmSalesTaxBidID As SqlParameter = cmdSalesTaxRate.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSalesTaxBidID.Value = frmMain.intBidID
        Dim drSalesTaxRate As SqlDataReader = Nothing
        cnn.Open()
        taxRate = "0"
        Try
            drSalesTaxRate = cmdSalesTaxRate.ExecuteReader
            drSalesTaxRate.Read()
            taxRate = drSalesTaxRate.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drSalesTaxRate.Close()
            cnn.Close()
        End Try

        taxTotal = Format(CDbl(preTaxTotal) * CDbl(taxRate), "C")
        grandTotal = Format(CDbl(preTaxTotal) + CDbl(taxTotal), "C")

    End Sub

End Module
