Imports System.Data
Imports System.Data.SqlClient
Public Class frmJob
    Inherits System.Windows.Forms.Form

    Dim cnn As New SqlConnection ' Primary Connection
    Dim intClientID As Integer
    Dim dsBids As New DataSet
    Public Shared blnStartUnit As Boolean 'Tests Startup Condition in LoadUnits
    Dim blnStartBldg As Boolean 'Tests Startup Condition in LoadBldgs

    Public Shared intBldgID As Integer
    Public Shared intBOMBldgID As Integer
    Public Shared intItemID As Integer
    Public Shared intUnitID As Integer
    Public Shared blnUnitReturn As Boolean
    Public Shared blnShowPrice As Boolean
    Public Shared blnUpdateBldg As Boolean
    Public Shared blnUpdateUnit As Boolean
    Public Shared intOrigBidID As Integer
    Dim intUnitEditing As Integer
    Dim intBldgDGRowCount As Integer
    Dim intDGLineItemsActiveRow As Integer
    Dim intUnitDGRowCount As Integer
    Dim intShippingDGRowCount As Integer


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tabJob As System.Windows.Forms.TabControl
    Friend WithEvents tpgJob As System.Windows.Forms.TabPage
    Friend WithEvents tpgDetails As System.Windows.Forms.TabPage
    Friend WithEvents tpgSummary As System.Windows.Forms.TabPage
    Friend WithEvents txtBBidContact As System.Windows.Forms.TextBox
    Friend WithEvents lblBContact As System.Windows.Forms.Label
    Friend WithEvents lblSalesTaxRate As System.Windows.Forms.Label
    Friend WithEvents txtSalesTaxRate As System.Windows.Forms.TextBox
    Friend WithEvents txtRevDate As System.Windows.Forms.TextBox
    Friend WithEvents txtBluePrintDate As System.Windows.Forms.TextBox
    Friend WithEvents txtLDEstimator As System.Windows.Forms.TextBox
    Friend WithEvents LBLRevDate As System.Windows.Forms.Label
    Friend WithEvents lblBluePrintDate As System.Windows.Forms.Label
    Friend WithEvents lblLDEstimator As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtbDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtBNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCLemail As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLFAX As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLZip As System.Windows.Forms.TextBox
    Friend WithEvents txtCLState As System.Windows.Forms.TextBox
    Friend WithEvents txtCLCity As System.Windows.Forms.TextBox
    Friend WithEvents txtCLAddr2 As System.Windows.Forms.TextBox
    Friend WithEvents txtClAddr1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLContact As System.Windows.Forms.TextBox
    Friend WithEvents txtCLName As System.Windows.Forms.TextBox
    Friend WithEvents txtBZip As System.Windows.Forms.TextBox
    Friend WithEvents txtBAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtBCity As System.Windows.Forms.TextBox
    Friend WithEvents txtBState As System.Windows.Forms.TextBox
    Friend WithEvents txtBLocation As System.Windows.Forms.TextBox
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents dgUnits As System.Windows.Forms.DataGrid
    Friend WithEvents dgLineItems As System.Windows.Forms.DataGrid
    Friend WithEvents dgBldg As System.Windows.Forms.DataGrid
    Friend WithEvents tpgComments As System.Windows.Forms.TabPage
    Friend WithEvents tpgChangeOrders As System.Windows.Forms.TabPage
    Friend WithEvents dgSummary As System.Windows.Forms.DataGrid
    Friend WithEvents btnSaveComment As System.Windows.Forms.Button
    Friend WithEvents lbCAuthor As System.Windows.Forms.Label
    Friend WithEvents lblCComment As System.Windows.Forms.Label
    Friend WithEvents txtCComment As System.Windows.Forms.TextBox
    Friend WithEvents txtCAuthor As System.Windows.Forms.TextBox
    Friend WithEvents dgComments As System.Windows.Forms.DataGrid
    Friend WithEvents btnCommentDelete As System.Windows.Forms.Button
    Friend WithEvents btnCommentEDit As System.Windows.Forms.Button
    Friend WithEvents stySummary As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgPN As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSQuan As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSMargin As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSShipped As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSBO As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSTotal As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents styBldg As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgBBldgID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents styUnits As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgUUnitID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgUName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgUType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgUDescription As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents styLI As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgLItemID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLPN As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLRT As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLLocation As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLQuan As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLMargin As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgLShipDate As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtLDPM As System.Windows.Forms.TextBox
    Friend WithEvents txtClientMobilePhone As System.Windows.Forms.TextBox
    Friend WithEvents txtClientPM As System.Windows.Forms.TextBox
    Friend WithEvents txtContractDate As System.Windows.Forms.TextBox
    Friend WithEvents btnEditJob As System.Windows.Forms.Button
    Friend WithEvents cbxClosed As System.Windows.Forms.CheckBox
    Friend WithEvents btnEditBldg As System.Windows.Forms.Button
    Friend WithEvents btnEditUnit As System.Windows.Forms.Button
    Friend WithEvents btnEditLI As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tpgBulkEdits As System.Windows.Forms.TabPage
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents btnUpdatePrice As System.Windows.Forms.Button
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtBERSell As System.Windows.Forms.TextBox
    Friend WithEvents txtBERMargin As System.Windows.Forms.TextBox
    Friend WithEvents txtBERCost As System.Windows.Forms.TextBox
    Friend WithEvents txtBERType As System.Windows.Forms.TextBox
    Friend WithEvents txtBERRT As System.Windows.Forms.TextBox
    Friend WithEvents txtBERDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtBERProdID As System.Windows.Forms.TextBox
    Friend WithEvents btnBEAdjustSell As System.Windows.Forms.Button
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents btnBERByType As System.Windows.Forms.Button
    Friend WithEvents btnBERByPartNum As System.Windows.Forms.Button
    Friend WithEvents btnBEDelete As System.Windows.Forms.Button
    Friend WithEvents cboBEType As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cbxFSell As System.Windows.Forms.CheckBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtBEBMargin As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblBEBType As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtBEBSell As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBCost As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBType As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBRT As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBProdID As System.Windows.Forms.TextBox
    Friend WithEvents rbBESO As System.Windows.Forms.RadioButton
    Friend WithEvents rbBEMain As System.Windows.Forms.RadioButton
    Friend WithEvents cboBECatalog As System.Windows.Forms.ComboBox
    Friend WithEvents cboBEBidItems As System.Windows.Forms.ComboBox
    Friend WithEvents btnDeleteJob As System.Windows.Forms.Button
    Friend WithEvents tpgBuilding As System.Windows.Forms.TabPage
    Friend WithEvents cboBldg As System.Windows.Forms.ComboBox
    Friend WithEvents dgBldgSummary As System.Windows.Forms.DataGrid
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents tbsQuan As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tbsPartNum As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tbsDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tbsCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tbsSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tbsTotalCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tbsTotalSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents cmdBOM As System.Windows.Forms.Button
    Friend WithEvents cbxShowPrice As System.Windows.Forms.CheckBox
    Friend WithEvents lblprices As System.Windows.Forms.Label
    Friend WithEvents txtJobDate As System.Windows.Forms.TextBox
    Friend WithEvents txtContractAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnShipBldgs As System.Windows.Forms.Button
    Friend WithEvents btnUpdateLIQuan As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReports As System.Windows.Forms.MenuItem
    Friend WithEvents mnurptSummary As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditAuxTitle As System.Windows.Forms.MenuItem
    Friend WithEvents dgChangeOrders As System.Windows.Forms.DataGrid
    Friend WithEvents txtCODate As System.Windows.Forms.TextBox
    Friend WithEvents txtCOEnteredBy As System.Windows.Forms.TextBox
    Friend WithEvents txtCOApprovedBy As System.Windows.Forms.TextBox
    Friend WithEvents txtCOMisc As System.Windows.Forms.TextBox
    Friend WithEvents txtCODesc As System.Windows.Forms.TextBox
    Friend WithEvents txtCOPriorBalance As System.Windows.Forms.TextBox
    Friend WithEvents txtCONewBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents btnCOSave As System.Windows.Forms.Button
    Friend WithEvents tsChangeOrder As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgDate As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgCOID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgApprovedBy As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgPriorBalance As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgNewBalance As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgEnteredBy As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgCOMisc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents btnCOCancel As System.Windows.Forms.Button
    Friend WithEvents btnCODelete As System.Windows.Forms.Button
    Friend WithEvents rbnRough As System.Windows.Forms.RadioButton
    Friend WithEvents rbnTrim As System.Windows.Forms.RadioButton
    Friend WithEvents rbnBoth As System.Windows.Forms.RadioButton
    Friend WithEvents dgShipSummary As System.Windows.Forms.DataGrid
    Friend WithEvents tsShipDates As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgBldgNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgShippedR As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents dgbShippedT As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents dgShpDateR As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgShpDateT As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBldgName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtUC As System.Windows.Forms.TextBox
    Friend WithEvents txtUS As System.Windows.Forms.TextBox
    Friend WithEvents txtSC As System.Windows.Forms.TextBox
    Friend WithEvents txtSS As System.Windows.Forms.TextBox
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents txtCS As System.Windows.Forms.TextBox
    Friend WithEvents txtCOC As System.Windows.Forms.TextBox
    Friend WithEvents txtCOS As System.Windows.Forms.TextBox
    Friend WithEvents txtAC As System.Windows.Forms.TextBox
    Friend WithEvents txtAS As System.Windows.Forms.TextBox
    Friend WithEvents txtTotSell As System.Windows.Forms.TextBox
    Friend WithEvents txtTotCost As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents mnuShipDates As System.Windows.Forms.MenuItem
    Friend WithEvents txtUnitWC As System.Windows.Forms.TextBox
    Friend WithEvents btnAddUnitWC As System.Windows.Forms.Button
    Friend WithEvents txtBERQuan As System.Windows.Forms.TextBox
    Friend WithEvents lblBERQuan As System.Windows.Forms.Label
    Friend WithEvents lblBERLocation As System.Windows.Forms.Label
    Friend WithEvents txtBERLocation As System.Windows.Forms.TextBox
    Friend WithEvents btnAddToUnits As System.Windows.Forms.Button
    Friend WithEvents lblBerType As System.Windows.Forms.Label
    Friend WithEvents btnUpdateShipDates As System.Windows.Forms.Button
    Friend WithEvents tpgShipping As System.Windows.Forms.TabPage
    Friend WithEvents dg_ShipDates As System.Windows.Forms.DataGrid
    Friend WithEvents dgShipDatesSty As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgsBldgID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsBldgNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsBldgName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsShipSequence As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsShipDateR As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsShipDate As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents btnUpdateShipping As System.Windows.Forms.Button
    Friend WithEvents tpgBidComments As System.Windows.Forms.TabPage
    Friend WithEvents dgBidComments As System.Windows.Forms.DataGrid
    Friend WithEvents tsBidComments As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents tsBCEntryDate As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents tsBCAuthor As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBCComment As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents txtBidComment As System.Windows.Forms.TextBox
    Friend WithEvents lblBidComments As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tabJob = New System.Windows.Forms.TabControl()
        Me.tpgJob = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtContractAmount = New System.Windows.Forms.TextBox()
        Me.txtJobDate = New System.Windows.Forms.TextBox()
        Me.btnDeleteJob = New System.Windows.Forms.Button()
        Me.cbxClosed = New System.Windows.Forms.CheckBox()
        Me.btnEditJob = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtLDPM = New System.Windows.Forms.TextBox()
        Me.txtClientMobilePhone = New System.Windows.Forms.TextBox()
        Me.txtClientPM = New System.Windows.Forms.TextBox()
        Me.txtContractDate = New System.Windows.Forms.TextBox()
        Me.txtBBidContact = New System.Windows.Forms.TextBox()
        Me.lblBContact = New System.Windows.Forms.Label()
        Me.lblSalesTaxRate = New System.Windows.Forms.Label()
        Me.txtSalesTaxRate = New System.Windows.Forms.TextBox()
        Me.txtRevDate = New System.Windows.Forms.TextBox()
        Me.txtBluePrintDate = New System.Windows.Forms.TextBox()
        Me.txtLDEstimator = New System.Windows.Forms.TextBox()
        Me.LBLRevDate = New System.Windows.Forms.Label()
        Me.lblBluePrintDate = New System.Windows.Forms.Label()
        Me.lblLDEstimator = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtbDescription = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtBNotes = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCLemail = New System.Windows.Forms.TextBox()
        Me.txtCLPhone2 = New System.Windows.Forms.TextBox()
        Me.txtCLFAX = New System.Windows.Forms.TextBox()
        Me.txtCLPhone1 = New System.Windows.Forms.TextBox()
        Me.txtCLZip = New System.Windows.Forms.TextBox()
        Me.txtCLState = New System.Windows.Forms.TextBox()
        Me.txtCLCity = New System.Windows.Forms.TextBox()
        Me.txtCLAddr2 = New System.Windows.Forms.TextBox()
        Me.txtClAddr1 = New System.Windows.Forms.TextBox()
        Me.txtCLContact = New System.Windows.Forms.TextBox()
        Me.txtCLName = New System.Windows.Forms.TextBox()
        Me.txtBZip = New System.Windows.Forms.TextBox()
        Me.txtBAddress = New System.Windows.Forms.TextBox()
        Me.txtBCity = New System.Windows.Forms.TextBox()
        Me.txtBState = New System.Windows.Forms.TextBox()
        Me.txtBLocation = New System.Windows.Forms.TextBox()
        Me.tpgDetails = New System.Windows.Forms.TabPage()
        Me.rbnBoth = New System.Windows.Forms.RadioButton()
        Me.rbnTrim = New System.Windows.Forms.RadioButton()
        Me.rbnRough = New System.Windows.Forms.RadioButton()
        Me.btnUpdateLIQuan = New System.Windows.Forms.Button()
        Me.btnShipBldgs = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnEditLI = New System.Windows.Forms.Button()
        Me.btnEditUnit = New System.Windows.Forms.Button()
        Me.btnEditBldg = New System.Windows.Forms.Button()
        Me.dgLineItems = New System.Windows.Forms.DataGrid()
        Me.styLI = New System.Windows.Forms.DataGridTableStyle()
        Me.dgLItemID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLPN = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLRT = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLLocation = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLQuan = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLMargin = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgLShipDate = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgUnits = New System.Windows.Forms.DataGrid()
        Me.styUnits = New System.Windows.Forms.DataGridTableStyle()
        Me.dgUUnitID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgUName = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgUType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgUDescription = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBldg = New System.Windows.Forms.DataGrid()
        Me.styBldg = New System.Windows.Forms.DataGridTableStyle()
        Me.dgBBldgID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBName = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tpgBuilding = New System.Windows.Forms.TabPage()
        Me.lblprices = New System.Windows.Forms.Label()
        Me.cbxShowPrice = New System.Windows.Forms.CheckBox()
        Me.cmdBOM = New System.Windows.Forms.Button()
        Me.dgBldgSummary = New System.Windows.Forms.DataGrid()
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle()
        Me.tbsQuan = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tbsPartNum = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tbsDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tbsCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tbsTotalCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tbsSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tbsTotalSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.cboBldg = New System.Windows.Forms.ComboBox()
        Me.tpgShipping = New System.Windows.Forms.TabPage()
        Me.btnUpdateShipping = New System.Windows.Forms.Button()
        Me.dg_ShipDates = New System.Windows.Forms.DataGrid()
        Me.dgShipDatesSty = New System.Windows.Forms.DataGridTableStyle()
        Me.dgsBldgID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsBldgNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsBldgName = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsShipSequence = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsShipDateR = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsShipDate = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tpgSummary = New System.Windows.Forms.TabPage()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtTotCost = New System.Windows.Forms.TextBox()
        Me.txtTotSell = New System.Windows.Forms.TextBox()
        Me.txtAS = New System.Windows.Forms.TextBox()
        Me.txtAC = New System.Windows.Forms.TextBox()
        Me.txtCOS = New System.Windows.Forms.TextBox()
        Me.txtCOC = New System.Windows.Forms.TextBox()
        Me.txtCS = New System.Windows.Forms.TextBox()
        Me.txtCC = New System.Windows.Forms.TextBox()
        Me.txtSS = New System.Windows.Forms.TextBox()
        Me.txtSC = New System.Windows.Forms.TextBox()
        Me.txtUS = New System.Windows.Forms.TextBox()
        Me.txtUC = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.dgShipSummary = New System.Windows.Forms.DataGrid()
        Me.tsShipDates = New System.Windows.Forms.DataGridTableStyle()
        Me.dgBldgNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBldgName = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgShippedR = New System.Windows.Forms.DataGridBoolColumn()
        Me.dgbShippedT = New System.Windows.Forms.DataGridBoolColumn()
        Me.dgShpDateR = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgShpDateT = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSummary = New System.Windows.Forms.DataGrid()
        Me.stySummary = New System.Windows.Forms.DataGridTableStyle()
        Me.dgPN = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSQuan = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSMargin = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSTotal = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSShipped = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgSBO = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tpgBulkEdits = New System.Windows.Forms.TabPage()
        Me.btnUpdateShipDates = New System.Windows.Forms.Button()
        Me.btnAddToUnits = New System.Windows.Forms.Button()
        Me.txtBERLocation = New System.Windows.Forms.TextBox()
        Me.lblBERLocation = New System.Windows.Forms.Label()
        Me.lblBERQuan = New System.Windows.Forms.Label()
        Me.txtBERQuan = New System.Windows.Forms.TextBox()
        Me.btnAddUnitWC = New System.Windows.Forms.Button()
        Me.txtUnitWC = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.btnUpdatePrice = New System.Windows.Forms.Button()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.lblBerType = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtBERSell = New System.Windows.Forms.TextBox()
        Me.txtBERMargin = New System.Windows.Forms.TextBox()
        Me.txtBERCost = New System.Windows.Forms.TextBox()
        Me.txtBERType = New System.Windows.Forms.TextBox()
        Me.txtBERRT = New System.Windows.Forms.TextBox()
        Me.txtBERDesc = New System.Windows.Forms.TextBox()
        Me.txtBERProdID = New System.Windows.Forms.TextBox()
        Me.btnBEAdjustSell = New System.Windows.Forms.Button()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.btnBERByType = New System.Windows.Forms.Button()
        Me.btnBERByPartNum = New System.Windows.Forms.Button()
        Me.btnBEDelete = New System.Windows.Forms.Button()
        Me.cboBEType = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cbxFSell = New System.Windows.Forms.CheckBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtBEBMargin = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblBEBType = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtBEBSell = New System.Windows.Forms.TextBox()
        Me.txtBEBCost = New System.Windows.Forms.TextBox()
        Me.txtBEBType = New System.Windows.Forms.TextBox()
        Me.txtBEBRT = New System.Windows.Forms.TextBox()
        Me.txtBEBDesc = New System.Windows.Forms.TextBox()
        Me.txtBEBProdID = New System.Windows.Forms.TextBox()
        Me.rbBESO = New System.Windows.Forms.RadioButton()
        Me.rbBEMain = New System.Windows.Forms.RadioButton()
        Me.cboBECatalog = New System.Windows.Forms.ComboBox()
        Me.cboBEBidItems = New System.Windows.Forms.ComboBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.tpgComments = New System.Windows.Forms.TabPage()
        Me.btnCommentDelete = New System.Windows.Forms.Button()
        Me.btnCommentEDit = New System.Windows.Forms.Button()
        Me.btnSaveComment = New System.Windows.Forms.Button()
        Me.lbCAuthor = New System.Windows.Forms.Label()
        Me.lblCComment = New System.Windows.Forms.Label()
        Me.txtCComment = New System.Windows.Forms.TextBox()
        Me.txtCAuthor = New System.Windows.Forms.TextBox()
        Me.dgComments = New System.Windows.Forms.DataGrid()
        Me.tpgChangeOrders = New System.Windows.Forms.TabPage()
        Me.btnCODelete = New System.Windows.Forms.Button()
        Me.btnCOCancel = New System.Windows.Forms.Button()
        Me.btnCOSave = New System.Windows.Forms.Button()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCONewBalance = New System.Windows.Forms.TextBox()
        Me.txtCOPriorBalance = New System.Windows.Forms.TextBox()
        Me.txtCODesc = New System.Windows.Forms.TextBox()
        Me.txtCOMisc = New System.Windows.Forms.TextBox()
        Me.txtCOApprovedBy = New System.Windows.Forms.TextBox()
        Me.txtCOEnteredBy = New System.Windows.Forms.TextBox()
        Me.txtCODate = New System.Windows.Forms.TextBox()
        Me.dgChangeOrders = New System.Windows.Forms.DataGrid()
        Me.tsChangeOrder = New System.Windows.Forms.DataGridTableStyle()
        Me.dgCOID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgDate = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgEnteredBy = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgApprovedBy = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgCOMisc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgPriorBalance = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgNewBalance = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tpgBidComments = New System.Windows.Forms.TabPage()
        Me.lblBidComments = New System.Windows.Forms.Label()
        Me.txtBidComment = New System.Windows.Forms.TextBox()
        Me.dgBidComments = New System.Windows.Forms.DataGrid()
        Me.tsBidComments = New System.Windows.Forms.DataGridTableStyle()
        Me.tsBCEntryDate = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.tsBCAuthor = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBCComment = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.cboJob = New System.Windows.Forms.ComboBox()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuShipDates = New System.Windows.Forms.MenuItem()
        Me.mnuReports = New System.Windows.Forms.MenuItem()
        Me.mnurptSummary = New System.Windows.Forms.MenuItem()
        Me.mnuEditAuxTitle = New System.Windows.Forms.MenuItem()
        Me.tabJob.SuspendLayout()
        Me.tpgJob.SuspendLayout()
        Me.tpgDetails.SuspendLayout()
        CType(Me.dgLineItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgUnits, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgBldg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgBuilding.SuspendLayout()
        CType(Me.dgBldgSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgShipping.SuspendLayout()
        CType(Me.dg_ShipDates, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgSummary.SuspendLayout()
        CType(Me.dgShipSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgBulkEdits.SuspendLayout()
        Me.tpgComments.SuspendLayout()
        CType(Me.dgComments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgChangeOrders.SuspendLayout()
        CType(Me.dgChangeOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgBidComments.SuspendLayout()
        CType(Me.dgBidComments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabJob
        '
        Me.tabJob.Controls.Add(Me.tpgJob)
        Me.tabJob.Controls.Add(Me.tpgDetails)
        Me.tabJob.Controls.Add(Me.tpgBuilding)
        Me.tabJob.Controls.Add(Me.tpgShipping)
        Me.tabJob.Controls.Add(Me.tpgSummary)
        Me.tabJob.Controls.Add(Me.tpgBulkEdits)
        Me.tabJob.Controls.Add(Me.tpgComments)
        Me.tabJob.Controls.Add(Me.tpgChangeOrders)
        Me.tabJob.Controls.Add(Me.tpgBidComments)
        Me.tabJob.Location = New System.Drawing.Point(20, 56)
        Me.tabJob.Name = "tabJob"
        Me.tabJob.SelectedIndex = 0
        Me.tabJob.Size = New System.Drawing.Size(720, 428)
        Me.tabJob.TabIndex = 0
        Me.tabJob.Visible = False
        '
        'tpgJob
        '
        Me.tpgJob.Controls.Add(Me.Label10)
        Me.tpgJob.Controls.Add(Me.Label9)
        Me.tpgJob.Controls.Add(Me.txtContractAmount)
        Me.tpgJob.Controls.Add(Me.txtJobDate)
        Me.tpgJob.Controls.Add(Me.btnDeleteJob)
        Me.tpgJob.Controls.Add(Me.cbxClosed)
        Me.tpgJob.Controls.Add(Me.btnEditJob)
        Me.tpgJob.Controls.Add(Me.Label1)
        Me.tpgJob.Controls.Add(Me.Label5)
        Me.tpgJob.Controls.Add(Me.Label6)
        Me.tpgJob.Controls.Add(Me.Label7)
        Me.tpgJob.Controls.Add(Me.txtLDPM)
        Me.tpgJob.Controls.Add(Me.txtClientMobilePhone)
        Me.tpgJob.Controls.Add(Me.txtClientPM)
        Me.tpgJob.Controls.Add(Me.txtContractDate)
        Me.tpgJob.Controls.Add(Me.txtBBidContact)
        Me.tpgJob.Controls.Add(Me.lblBContact)
        Me.tpgJob.Controls.Add(Me.lblSalesTaxRate)
        Me.tpgJob.Controls.Add(Me.txtSalesTaxRate)
        Me.tpgJob.Controls.Add(Me.txtRevDate)
        Me.tpgJob.Controls.Add(Me.txtBluePrintDate)
        Me.tpgJob.Controls.Add(Me.txtLDEstimator)
        Me.tpgJob.Controls.Add(Me.LBLRevDate)
        Me.tpgJob.Controls.Add(Me.lblBluePrintDate)
        Me.tpgJob.Controls.Add(Me.lblLDEstimator)
        Me.tpgJob.Controls.Add(Me.Label25)
        Me.tpgJob.Controls.Add(Me.Label24)
        Me.tpgJob.Controls.Add(Me.Label23)
        Me.tpgJob.Controls.Add(Me.Label22)
        Me.tpgJob.Controls.Add(Me.Label21)
        Me.tpgJob.Controls.Add(Me.Label20)
        Me.tpgJob.Controls.Add(Me.Label11)
        Me.tpgJob.Controls.Add(Me.txtbDescription)
        Me.tpgJob.Controls.Add(Me.Label14)
        Me.tpgJob.Controls.Add(Me.txtBNotes)
        Me.tpgJob.Controls.Add(Me.Label4)
        Me.tpgJob.Controls.Add(Me.Label3)
        Me.tpgJob.Controls.Add(Me.Label2)
        Me.tpgJob.Controls.Add(Me.txtCLemail)
        Me.tpgJob.Controls.Add(Me.txtCLPhone2)
        Me.tpgJob.Controls.Add(Me.txtCLFAX)
        Me.tpgJob.Controls.Add(Me.txtCLPhone1)
        Me.tpgJob.Controls.Add(Me.txtCLZip)
        Me.tpgJob.Controls.Add(Me.txtCLState)
        Me.tpgJob.Controls.Add(Me.txtCLCity)
        Me.tpgJob.Controls.Add(Me.txtCLAddr2)
        Me.tpgJob.Controls.Add(Me.txtClAddr1)
        Me.tpgJob.Controls.Add(Me.txtCLContact)
        Me.tpgJob.Controls.Add(Me.txtCLName)
        Me.tpgJob.Controls.Add(Me.txtBZip)
        Me.tpgJob.Controls.Add(Me.txtBAddress)
        Me.tpgJob.Controls.Add(Me.txtBCity)
        Me.tpgJob.Controls.Add(Me.txtBState)
        Me.tpgJob.Controls.Add(Me.txtBLocation)
        Me.tpgJob.Location = New System.Drawing.Point(4, 22)
        Me.tpgJob.Name = "tpgJob"
        Me.tpgJob.Size = New System.Drawing.Size(712, 402)
        Me.tpgJob.TabIndex = 0
        Me.tpgJob.Text = "Project"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(208, 368)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(76, 16)
        Me.Label10.TabIndex = 116
        Me.Label10.Text = "Contract Amount"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(8, 368)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 16)
        Me.Label9.TabIndex = 115
        Me.Label9.Text = "Job Date"
        '
        'txtContractAmount
        '
        Me.txtContractAmount.Location = New System.Drawing.Point(296, 366)
        Me.txtContractAmount.Name = "txtContractAmount"
        Me.txtContractAmount.Size = New System.Drawing.Size(88, 20)
        Me.txtContractAmount.TabIndex = 114
        '
        'txtJobDate
        '
        Me.txtJobDate.Location = New System.Drawing.Point(92, 366)
        Me.txtJobDate.Name = "txtJobDate"
        Me.txtJobDate.Size = New System.Drawing.Size(108, 20)
        Me.txtJobDate.TabIndex = 113
        '
        'btnDeleteJob
        '
        Me.btnDeleteJob.Location = New System.Drawing.Point(540, 364)
        Me.btnDeleteJob.Name = "btnDeleteJob"
        Me.btnDeleteJob.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteJob.TabIndex = 111
        Me.btnDeleteJob.Text = "Delete Job"
        '
        'cbxClosed
        '
        Me.cbxClosed.Location = New System.Drawing.Point(620, 236)
        Me.cbxClosed.Name = "cbxClosed"
        Me.cbxClosed.Size = New System.Drawing.Size(64, 16)
        Me.cbxClosed.TabIndex = 110
        Me.cbxClosed.Text = "Closed"
        '
        'btnEditJob
        '
        Me.btnEditJob.Location = New System.Drawing.Point(608, 364)
        Me.btnEditJob.Name = "btnEditJob"
        Me.btnEditJob.Size = New System.Drawing.Size(80, 24)
        Me.btnEditJob.TabIndex = 109
        Me.btnEditJob.Text = "Edit"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(480, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 20)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Phone"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 172)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 20)
        Me.Label5.TabIndex = 107
        Me.Label5.Text = "LD PM"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(256, 152)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 20)
        Me.Label6.TabIndex = 106
        Me.Label6.Text = "Client PM"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 152)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 20)
        Me.Label7.TabIndex = 105
        Me.Label7.Text = "Contract Date"
        '
        'txtLDPM
        '
        Me.txtLDPM.Location = New System.Drawing.Point(84, 172)
        Me.txtLDPM.Name = "txtLDPM"
        Me.txtLDPM.Size = New System.Drawing.Size(156, 20)
        Me.txtLDPM.TabIndex = 104
        '
        'txtClientMobilePhone
        '
        Me.txtClientMobilePhone.Location = New System.Drawing.Point(520, 152)
        Me.txtClientMobilePhone.Name = "txtClientMobilePhone"
        Me.txtClientMobilePhone.Size = New System.Drawing.Size(168, 20)
        Me.txtClientMobilePhone.TabIndex = 103
        '
        'txtClientPM
        '
        Me.txtClientPM.Location = New System.Drawing.Point(304, 152)
        Me.txtClientPM.Name = "txtClientPM"
        Me.txtClientPM.Size = New System.Drawing.Size(152, 20)
        Me.txtClientPM.TabIndex = 102
        '
        'txtContractDate
        '
        Me.txtContractDate.Location = New System.Drawing.Point(84, 152)
        Me.txtContractDate.Name = "txtContractDate"
        Me.txtContractDate.Size = New System.Drawing.Size(156, 20)
        Me.txtContractDate.TabIndex = 101
        '
        'txtBBidContact
        '
        Me.txtBBidContact.Location = New System.Drawing.Point(520, 172)
        Me.txtBBidContact.Name = "txtBBidContact"
        Me.txtBBidContact.Size = New System.Drawing.Size(168, 20)
        Me.txtBBidContact.TabIndex = 76
        '
        'lblBContact
        '
        Me.lblBContact.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBContact.Location = New System.Drawing.Point(460, 172)
        Me.lblBContact.Name = "lblBContact"
        Me.lblBContact.Size = New System.Drawing.Size(56, 20)
        Me.lblBContact.TabIndex = 75
        Me.lblBContact.Text = "Bid Contact"
        '
        'lblSalesTaxRate
        '
        Me.lblSalesTaxRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalesTaxRate.Location = New System.Drawing.Point(8, 232)
        Me.lblSalesTaxRate.Name = "lblSalesTaxRate"
        Me.lblSalesTaxRate.Size = New System.Drawing.Size(72, 20)
        Me.lblSalesTaxRate.TabIndex = 74
        Me.lblSalesTaxRate.Text = "Sales Tax Rate"
        '
        'txtSalesTaxRate
        '
        Me.txtSalesTaxRate.Location = New System.Drawing.Point(84, 232)
        Me.txtSalesTaxRate.Name = "txtSalesTaxRate"
        Me.txtSalesTaxRate.Size = New System.Drawing.Size(100, 20)
        Me.txtSalesTaxRate.TabIndex = 73
        Me.txtSalesTaxRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRevDate
        '
        Me.txtRevDate.Location = New System.Drawing.Point(408, 232)
        Me.txtRevDate.Name = "txtRevDate"
        Me.txtRevDate.Size = New System.Drawing.Size(100, 20)
        Me.txtRevDate.TabIndex = 72
        Me.txtRevDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBluePrintDate
        '
        Me.txtBluePrintDate.Location = New System.Drawing.Point(264, 232)
        Me.txtBluePrintDate.Name = "txtBluePrintDate"
        Me.txtBluePrintDate.Size = New System.Drawing.Size(92, 20)
        Me.txtBluePrintDate.TabIndex = 71
        Me.txtBluePrintDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLDEstimator
        '
        Me.txtLDEstimator.Location = New System.Drawing.Point(304, 172)
        Me.txtLDEstimator.Name = "txtLDEstimator"
        Me.txtLDEstimator.Size = New System.Drawing.Size(152, 20)
        Me.txtLDEstimator.TabIndex = 70
        '
        'LBLRevDate
        '
        Me.LBLRevDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLRevDate.Location = New System.Drawing.Point(360, 232)
        Me.LBLRevDate.Name = "LBLRevDate"
        Me.LBLRevDate.Size = New System.Drawing.Size(48, 20)
        Me.LBLRevDate.TabIndex = 69
        Me.LBLRevDate.Text = "Rev Date"
        '
        'lblBluePrintDate
        '
        Me.lblBluePrintDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBluePrintDate.Location = New System.Drawing.Point(188, 236)
        Me.lblBluePrintDate.Name = "lblBluePrintDate"
        Me.lblBluePrintDate.Size = New System.Drawing.Size(72, 16)
        Me.lblBluePrintDate.TabIndex = 68
        Me.lblBluePrintDate.Text = "Blue Print Date"
        '
        'lblLDEstimator
        '
        Me.lblLDEstimator.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLDEstimator.Location = New System.Drawing.Point(240, 172)
        Me.lblLDEstimator.Name = "lblLDEstimator"
        Me.lblLDEstimator.Size = New System.Drawing.Size(68, 20)
        Me.lblLDEstimator.TabIndex = 67
        Me.lblLDEstimator.Text = "LD Estimator"
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(320, 132)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(116, 16)
        Me.Label25.TabIndex = 66
        Me.Label25.Text = "----------Project-----------"
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(592, 212)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(20, 20)
        Me.Label24.TabIndex = 65
        Me.Label24.Text = "ZIP"
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(532, 216)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(24, 20)
        Me.Label23.TabIndex = 64
        Me.Label23.Text = "ST"
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(360, 212)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(28, 20)
        Me.Label22.TabIndex = 63
        Me.Label22.Text = "City"
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(8, 212)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(48, 20)
        Me.Label21.TabIndex = 62
        Me.Label21.Text = "Address"
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(360, 194)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(48, 16)
        Me.Label20.TabIndex = 61
        Me.Label20.Text = "Location"
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(8, 194)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 16)
        Me.Label11.TabIndex = 60
        Me.Label11.Text = "Desc"
        '
        'txtbDescription
        '
        Me.txtbDescription.Location = New System.Drawing.Point(84, 192)
        Me.txtbDescription.Name = "txtbDescription"
        Me.txtbDescription.Size = New System.Drawing.Size(272, 20)
        Me.txtbDescription.TabIndex = 50
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(8, 252)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 15)
        Me.Label14.TabIndex = 58
        Me.Label14.Text = "Notes"
        '
        'txtBNotes
        '
        Me.txtBNotes.Location = New System.Drawing.Point(84, 252)
        Me.txtBNotes.Multiline = True
        Me.txtBNotes.Name = "txtBNotes"
        Me.txtBNotes.Size = New System.Drawing.Size(604, 100)
        Me.txtBNotes.TabIndex = 59
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(408, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 16)
        Me.Label4.TabIndex = 53
        Me.Label4.Text = "e-mail"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(408, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 16)
        Me.Label3.TabIndex = 51
        Me.Label3.Text = "FAX"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(408, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "Phone"
        '
        'txtCLemail
        '
        Me.txtCLemail.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLemail.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLemail.Location = New System.Drawing.Point(464, 101)
        Me.txtCLemail.Name = "txtCLemail"
        Me.txtCLemail.ReadOnly = True
        Me.txtCLemail.Size = New System.Drawing.Size(216, 20)
        Me.txtCLemail.TabIndex = 48
        Me.txtCLemail.TabStop = False
        '
        'txtCLPhone2
        '
        Me.txtCLPhone2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone2.Location = New System.Drawing.Point(576, 61)
        Me.txtCLPhone2.Name = "txtCLPhone2"
        Me.txtCLPhone2.ReadOnly = True
        Me.txtCLPhone2.Size = New System.Drawing.Size(104, 20)
        Me.txtCLPhone2.TabIndex = 46
        Me.txtCLPhone2.TabStop = False
        '
        'txtCLFAX
        '
        Me.txtCLFAX.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLFAX.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLFAX.Location = New System.Drawing.Point(464, 81)
        Me.txtCLFAX.Name = "txtCLFAX"
        Me.txtCLFAX.ReadOnly = True
        Me.txtCLFAX.Size = New System.Drawing.Size(112, 20)
        Me.txtCLFAX.TabIndex = 47
        Me.txtCLFAX.TabStop = False
        '
        'txtCLPhone1
        '
        Me.txtCLPhone1.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone1.Location = New System.Drawing.Point(464, 61)
        Me.txtCLPhone1.Name = "txtCLPhone1"
        Me.txtCLPhone1.ReadOnly = True
        Me.txtCLPhone1.Size = New System.Drawing.Size(112, 20)
        Me.txtCLPhone1.TabIndex = 45
        Me.txtCLPhone1.TabStop = False
        '
        'txtCLZip
        '
        Me.txtCLZip.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLZip.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLZip.Location = New System.Drawing.Point(260, 101)
        Me.txtCLZip.Name = "txtCLZip"
        Me.txtCLZip.ReadOnly = True
        Me.txtCLZip.Size = New System.Drawing.Size(80, 20)
        Me.txtCLZip.TabIndex = 43
        Me.txtCLZip.TabStop = False
        '
        'txtCLState
        '
        Me.txtCLState.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLState.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLState.Location = New System.Drawing.Point(220, 101)
        Me.txtCLState.Name = "txtCLState"
        Me.txtCLState.ReadOnly = True
        Me.txtCLState.Size = New System.Drawing.Size(40, 20)
        Me.txtCLState.TabIndex = 42
        Me.txtCLState.TabStop = False
        '
        'txtCLCity
        '
        Me.txtCLCity.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLCity.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLCity.Location = New System.Drawing.Point(84, 101)
        Me.txtCLCity.Name = "txtCLCity"
        Me.txtCLCity.ReadOnly = True
        Me.txtCLCity.Size = New System.Drawing.Size(136, 20)
        Me.txtCLCity.TabIndex = 41
        Me.txtCLCity.TabStop = False
        '
        'txtCLAddr2
        '
        Me.txtCLAddr2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLAddr2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLAddr2.Location = New System.Drawing.Point(84, 81)
        Me.txtCLAddr2.Name = "txtCLAddr2"
        Me.txtCLAddr2.ReadOnly = True
        Me.txtCLAddr2.Size = New System.Drawing.Size(256, 20)
        Me.txtCLAddr2.TabIndex = 40
        Me.txtCLAddr2.TabStop = False
        '
        'txtClAddr1
        '
        Me.txtClAddr1.BackColor = System.Drawing.SystemColors.Info
        Me.txtClAddr1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtClAddr1.Location = New System.Drawing.Point(84, 61)
        Me.txtClAddr1.Name = "txtClAddr1"
        Me.txtClAddr1.ReadOnly = True
        Me.txtClAddr1.Size = New System.Drawing.Size(256, 20)
        Me.txtClAddr1.TabIndex = 39
        Me.txtClAddr1.TabStop = False
        '
        'txtCLContact
        '
        Me.txtCLContact.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLContact.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLContact.Location = New System.Drawing.Point(464, 41)
        Me.txtCLContact.Name = "txtCLContact"
        Me.txtCLContact.ReadOnly = True
        Me.txtCLContact.Size = New System.Drawing.Size(216, 20)
        Me.txtCLContact.TabIndex = 44
        Me.txtCLContact.TabStop = False
        '
        'txtCLName
        '
        Me.txtCLName.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLName.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLName.Location = New System.Drawing.Point(84, 41)
        Me.txtCLName.Name = "txtCLName"
        Me.txtCLName.ReadOnly = True
        Me.txtCLName.Size = New System.Drawing.Size(256, 20)
        Me.txtCLName.TabIndex = 38
        Me.txtCLName.TabStop = False
        '
        'txtBZip
        '
        Me.txtBZip.Location = New System.Drawing.Point(616, 212)
        Me.txtBZip.Name = "txtBZip"
        Me.txtBZip.Size = New System.Drawing.Size(72, 20)
        Me.txtBZip.TabIndex = 57
        '
        'txtBAddress
        '
        Me.txtBAddress.Location = New System.Drawing.Point(84, 212)
        Me.txtBAddress.Name = "txtBAddress"
        Me.txtBAddress.Size = New System.Drawing.Size(272, 20)
        Me.txtBAddress.TabIndex = 54
        '
        'txtBCity
        '
        Me.txtBCity.Location = New System.Drawing.Point(408, 212)
        Me.txtBCity.Name = "txtBCity"
        Me.txtBCity.Size = New System.Drawing.Size(124, 20)
        Me.txtBCity.TabIndex = 55
        '
        'txtBState
        '
        Me.txtBState.Location = New System.Drawing.Point(556, 212)
        Me.txtBState.Name = "txtBState"
        Me.txtBState.Size = New System.Drawing.Size(32, 20)
        Me.txtBState.TabIndex = 56
        '
        'txtBLocation
        '
        Me.txtBLocation.Location = New System.Drawing.Point(408, 192)
        Me.txtBLocation.Name = "txtBLocation"
        Me.txtBLocation.Size = New System.Drawing.Size(280, 20)
        Me.txtBLocation.TabIndex = 52
        '
        'tpgDetails
        '
        Me.tpgDetails.Controls.Add(Me.rbnBoth)
        Me.tpgDetails.Controls.Add(Me.rbnTrim)
        Me.tpgDetails.Controls.Add(Me.rbnRough)
        Me.tpgDetails.Controls.Add(Me.btnUpdateLIQuan)
        Me.tpgDetails.Controls.Add(Me.btnShipBldgs)
        Me.tpgDetails.Controls.Add(Me.Label8)
        Me.tpgDetails.Controls.Add(Me.btnEditLI)
        Me.tpgDetails.Controls.Add(Me.btnEditUnit)
        Me.tpgDetails.Controls.Add(Me.btnEditBldg)
        Me.tpgDetails.Controls.Add(Me.dgLineItems)
        Me.tpgDetails.Controls.Add(Me.dgUnits)
        Me.tpgDetails.Controls.Add(Me.dgBldg)
        Me.tpgDetails.Location = New System.Drawing.Point(4, 22)
        Me.tpgDetails.Name = "tpgDetails"
        Me.tpgDetails.Size = New System.Drawing.Size(712, 402)
        Me.tpgDetails.TabIndex = 1
        Me.tpgDetails.Text = "Details"
        '
        'rbnBoth
        '
        Me.rbnBoth.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbnBoth.Location = New System.Drawing.Point(184, 8)
        Me.rbnBoth.Name = "rbnBoth"
        Me.rbnBoth.Size = New System.Drawing.Size(52, 16)
        Me.rbnBoth.TabIndex = 11
        Me.rbnBoth.Text = "Both"
        '
        'rbnTrim
        '
        Me.rbnTrim.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbnTrim.Location = New System.Drawing.Point(132, 8)
        Me.rbnTrim.Name = "rbnTrim"
        Me.rbnTrim.Size = New System.Drawing.Size(52, 16)
        Me.rbnTrim.TabIndex = 10
        Me.rbnTrim.Text = "Trim"
        '
        'rbnRough
        '
        Me.rbnRough.Checked = True
        Me.rbnRough.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbnRough.Location = New System.Drawing.Point(80, 8)
        Me.rbnRough.Name = "rbnRough"
        Me.rbnRough.Size = New System.Drawing.Size(52, 16)
        Me.rbnRough.TabIndex = 9
        Me.rbnRough.TabStop = True
        Me.rbnRough.Text = "Rough"
        '
        'btnUpdateLIQuan
        '
        Me.btnUpdateLIQuan.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateLIQuan.Location = New System.Drawing.Point(416, 212)
        Me.btnUpdateLIQuan.Name = "btnUpdateLIQuan"
        Me.btnUpdateLIQuan.Size = New System.Drawing.Size(48, 16)
        Me.btnUpdateLIQuan.TabIndex = 8
        Me.btnUpdateLIQuan.Text = "Update Quan"
        '
        'btnShipBldgs
        '
        Me.btnShipBldgs.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShipBldgs.Location = New System.Drawing.Point(12, 4)
        Me.btnShipBldgs.Name = "btnShipBldgs"
        Me.btnShipBldgs.Size = New System.Drawing.Size(60, 20)
        Me.btnShipBldgs.TabIndex = 7
        Me.btnShipBldgs.Text = "Ship Bldgs"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(560, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(24, 12)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Edit"
        '
        'btnEditLI
        '
        Me.btnEditLI.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditLI.Location = New System.Drawing.Point(660, 4)
        Me.btnEditLI.Name = "btnEditLI"
        Me.btnEditLI.Size = New System.Drawing.Size(36, 16)
        Me.btnEditLI.TabIndex = 5
        Me.btnEditLI.Text = "Item"
        '
        'btnEditUnit
        '
        Me.btnEditUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditUnit.Location = New System.Drawing.Point(624, 4)
        Me.btnEditUnit.Name = "btnEditUnit"
        Me.btnEditUnit.Size = New System.Drawing.Size(36, 16)
        Me.btnEditUnit.TabIndex = 4
        Me.btnEditUnit.Text = "Unit"
        '
        'btnEditBldg
        '
        Me.btnEditBldg.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditBldg.Location = New System.Drawing.Point(588, 4)
        Me.btnEditBldg.Name = "btnEditBldg"
        Me.btnEditBldg.Size = New System.Drawing.Size(36, 16)
        Me.btnEditBldg.TabIndex = 3
        Me.btnEditBldg.Text = "Bldg"
        '
        'dgLineItems
        '
        Me.dgLineItems.CaptionText = "Line Items"
        Me.dgLineItems.DataMember = ""
        Me.dgLineItems.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgLineItems.Location = New System.Drawing.Point(16, 208)
        Me.dgLineItems.Name = "dgLineItems"
        Me.dgLineItems.Size = New System.Drawing.Size(688, 168)
        Me.dgLineItems.TabIndex = 2
        Me.dgLineItems.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.styLI})
        '
        'styLI
        '
        Me.styLI.DataGrid = Me.dgLineItems
        Me.styLI.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgLItemID, Me.dgLPN, Me.dgLDesc, Me.dgLRT, Me.dgLLocation, Me.dgLType, Me.dgLQuan, Me.dgLCost, Me.dgLMargin, Me.dgLSell, Me.dgLShipDate})
        Me.styLI.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.styLI.MappingName = "LineItems"
        '
        'dgLItemID
        '
        Me.dgLItemID.Format = ""
        Me.dgLItemID.FormatInfo = Nothing
        Me.dgLItemID.MappingName = "ItemID"
        Me.dgLItemID.Width = 0
        '
        'dgLPN
        '
        Me.dgLPN.Format = ""
        Me.dgLPN.FormatInfo = Nothing
        Me.dgLPN.HeaderText = "Part Number"
        Me.dgLPN.MappingName = "PartNumber"
        Me.dgLPN.ReadOnly = True
        Me.dgLPN.Width = 90
        '
        'dgLDesc
        '
        Me.dgLDesc.Format = ""
        Me.dgLDesc.FormatInfo = Nothing
        Me.dgLDesc.HeaderText = "Description"
        Me.dgLDesc.MappingName = "Description"
        Me.dgLDesc.NullText = ""
        Me.dgLDesc.ReadOnly = True
        Me.dgLDesc.Width = 125
        '
        'dgLRT
        '
        Me.dgLRT.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgLRT.Format = ""
        Me.dgLRT.FormatInfo = Nothing
        Me.dgLRT.HeaderText = "RT"
        Me.dgLRT.MappingName = "RT"
        Me.dgLRT.NullText = ""
        Me.dgLRT.ReadOnly = True
        Me.dgLRT.Width = 25
        '
        'dgLLocation
        '
        Me.dgLLocation.Format = ""
        Me.dgLLocation.FormatInfo = Nothing
        Me.dgLLocation.HeaderText = "Location"
        Me.dgLLocation.MappingName = "Location"
        Me.dgLLocation.NullText = ""
        Me.dgLLocation.ReadOnly = True
        Me.dgLLocation.Width = 75
        '
        'dgLType
        '
        Me.dgLType.Format = ""
        Me.dgLType.FormatInfo = Nothing
        Me.dgLType.HeaderText = "Type"
        Me.dgLType.MappingName = "Type"
        Me.dgLType.NullText = ""
        Me.dgLType.ReadOnly = True
        Me.dgLType.Width = 50
        '
        'dgLQuan
        '
        Me.dgLQuan.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgLQuan.Format = ""
        Me.dgLQuan.FormatInfo = Nothing
        Me.dgLQuan.HeaderText = "Quan"
        Me.dgLQuan.MappingName = "Quantity"
        Me.dgLQuan.NullText = ""
        Me.dgLQuan.Width = 40
        '
        'dgLCost
        '
        Me.dgLCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgLCost.Format = "C"
        Me.dgLCost.FormatInfo = Nothing
        Me.dgLCost.HeaderText = "Cost-"
        Me.dgLCost.MappingName = "Cost"
        Me.dgLCost.NullText = ""
        Me.dgLCost.ReadOnly = True
        Me.dgLCost.Width = 60
        '
        'dgLMargin
        '
        Me.dgLMargin.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgLMargin.Format = ""
        Me.dgLMargin.FormatInfo = Nothing
        Me.dgLMargin.HeaderText = "Margin"
        Me.dgLMargin.MappingName = "Margin"
        Me.dgLMargin.NullText = ""
        Me.dgLMargin.ReadOnly = True
        Me.dgLMargin.Width = 40
        '
        'dgLSell
        '
        Me.dgLSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgLSell.Format = "C"
        Me.dgLSell.FormatInfo = Nothing
        Me.dgLSell.HeaderText = "Sell-"
        Me.dgLSell.MappingName = "Sell"
        Me.dgLSell.NullText = ""
        Me.dgLSell.ReadOnly = True
        Me.dgLSell.Width = 70
        '
        'dgLShipDate
        '
        Me.dgLShipDate.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgLShipDate.Format = "d"
        Me.dgLShipDate.FormatInfo = Nothing
        Me.dgLShipDate.HeaderText = "Ship Date"
        Me.dgLShipDate.MappingName = "ShipDate"
        Me.dgLShipDate.NullText = ""
        Me.dgLShipDate.ReadOnly = True
        Me.dgLShipDate.Width = 75
        '
        'dgUnits
        '
        Me.dgUnits.CaptionText = "Units"
        Me.dgUnits.DataMember = ""
        Me.dgUnits.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgUnits.Location = New System.Drawing.Point(388, 24)
        Me.dgUnits.Name = "dgUnits"
        Me.dgUnits.Size = New System.Drawing.Size(316, 176)
        Me.dgUnits.TabIndex = 1
        Me.dgUnits.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.styUnits})
        '
        'styUnits
        '
        Me.styUnits.DataGrid = Me.dgUnits
        Me.styUnits.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgUUnitID, Me.dgUName, Me.dgUType, Me.dgUDescription})
        Me.styUnits.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.styUnits.MappingName = "Units"
        '
        'dgUUnitID
        '
        Me.dgUUnitID.Format = ""
        Me.dgUUnitID.FormatInfo = Nothing
        Me.dgUUnitID.MappingName = "UnitID"
        Me.dgUUnitID.Width = 0
        '
        'dgUName
        '
        Me.dgUName.Format = ""
        Me.dgUName.FormatInfo = Nothing
        Me.dgUName.HeaderText = "Name"
        Me.dgUName.MappingName = "UnitName"
        Me.dgUName.NullText = ""
        Me.dgUName.ReadOnly = True
        Me.dgUName.Width = 75
        '
        'dgUType
        '
        Me.dgUType.Format = ""
        Me.dgUType.FormatInfo = Nothing
        Me.dgUType.HeaderText = "Type"
        Me.dgUType.MappingName = "UnitType"
        Me.dgUType.NullText = ""
        Me.dgUType.ReadOnly = True
        Me.dgUType.Width = 60
        '
        'dgUDescription
        '
        Me.dgUDescription.Format = ""
        Me.dgUDescription.FormatInfo = Nothing
        Me.dgUDescription.HeaderText = "Description"
        Me.dgUDescription.MappingName = "Description"
        Me.dgUDescription.ReadOnly = True
        Me.dgUDescription.Width = 125
        '
        'dgBldg
        '
        Me.dgBldg.CaptionText = "Buildings"
        Me.dgBldg.DataMember = ""
        Me.dgBldg.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBldg.Location = New System.Drawing.Point(12, 24)
        Me.dgBldg.Name = "dgBldg"
        Me.dgBldg.Size = New System.Drawing.Size(364, 176)
        Me.dgBldg.TabIndex = 0
        Me.dgBldg.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.styBldg})
        '
        'styBldg
        '
        Me.styBldg.DataGrid = Me.dgBldg
        Me.styBldg.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgBBldgID, Me.dgBNumber, Me.dgBName, Me.dgBType, Me.dgBDesc})
        Me.styBldg.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.styBldg.MappingName = "Bldgs"
        '
        'dgBBldgID
        '
        Me.dgBBldgID.Format = ""
        Me.dgBBldgID.FormatInfo = Nothing
        Me.dgBBldgID.MappingName = "BldgID"
        Me.dgBBldgID.Width = 0
        '
        'dgBNumber
        '
        Me.dgBNumber.Format = ""
        Me.dgBNumber.FormatInfo = Nothing
        Me.dgBNumber.HeaderText = "Num"
        Me.dgBNumber.MappingName = "bldgNumber"
        Me.dgBNumber.NullText = ""
        Me.dgBNumber.ReadOnly = True
        Me.dgBNumber.Width = 40
        '
        'dgBName
        '
        Me.dgBName.Format = ""
        Me.dgBName.FormatInfo = Nothing
        Me.dgBName.HeaderText = "Name"
        Me.dgBName.MappingName = "BldgName"
        Me.dgBName.NullText = ""
        Me.dgBName.ReadOnly = True
        Me.dgBName.Width = 75
        '
        'dgBType
        '
        Me.dgBType.Format = ""
        Me.dgBType.FormatInfo = Nothing
        Me.dgBType.HeaderText = "Type"
        Me.dgBType.MappingName = "Type"
        Me.dgBType.NullText = ""
        Me.dgBType.ReadOnly = True
        Me.dgBType.Width = 50
        '
        'dgBDesc
        '
        Me.dgBDesc.Format = ""
        Me.dgBDesc.FormatInfo = Nothing
        Me.dgBDesc.HeaderText = "Description"
        Me.dgBDesc.MappingName = "Description"
        Me.dgBDesc.ReadOnly = True
        Me.dgBDesc.Width = 140
        '
        'tpgBuilding
        '
        Me.tpgBuilding.Controls.Add(Me.lblprices)
        Me.tpgBuilding.Controls.Add(Me.cbxShowPrice)
        Me.tpgBuilding.Controls.Add(Me.cmdBOM)
        Me.tpgBuilding.Controls.Add(Me.dgBldgSummary)
        Me.tpgBuilding.Controls.Add(Me.cboBldg)
        Me.tpgBuilding.Location = New System.Drawing.Point(4, 22)
        Me.tpgBuilding.Name = "tpgBuilding"
        Me.tpgBuilding.Size = New System.Drawing.Size(712, 402)
        Me.tpgBuilding.TabIndex = 6
        Me.tpgBuilding.Text = "Buildings"
        '
        'lblprices
        '
        Me.lblprices.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblprices.Location = New System.Drawing.Point(536, 4)
        Me.lblprices.Name = "lblprices"
        Me.lblprices.Size = New System.Drawing.Size(128, 12)
        Me.lblprices.TabIndex = 4
        Me.lblprices.Text = "Print Shipping Manifest"
        Me.lblprices.Visible = False
        '
        'cbxShowPrice
        '
        Me.cbxShowPrice.Checked = True
        Me.cbxShowPrice.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxShowPrice.Location = New System.Drawing.Point(600, 20)
        Me.cbxShowPrice.Name = "cbxShowPrice"
        Me.cbxShowPrice.Size = New System.Drawing.Size(80, 24)
        Me.cbxShowPrice.TabIndex = 3
        Me.cbxShowPrice.Text = "Show Price"
        Me.cbxShowPrice.Visible = False
        '
        'cmdBOM
        '
        Me.cmdBOM.Location = New System.Drawing.Point(536, 20)
        Me.cmdBOM.Name = "cmdBOM"
        Me.cmdBOM.Size = New System.Drawing.Size(60, 20)
        Me.cmdBOM.TabIndex = 2
        Me.cmdBOM.Text = "Print"
        Me.cmdBOM.Visible = False
        '
        'dgBldgSummary
        '
        Me.dgBldgSummary.DataMember = ""
        Me.dgBldgSummary.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBldgSummary.Location = New System.Drawing.Point(8, 80)
        Me.dgBldgSummary.Name = "dgBldgSummary"
        Me.dgBldgSummary.Size = New System.Drawing.Size(696, 300)
        Me.dgBldgSummary.TabIndex = 1
        Me.dgBldgSummary.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle1})
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.DataGrid = Me.dgBldgSummary
        Me.DataGridTableStyle1.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.tbsQuan, Me.tbsPartNum, Me.tbsDesc, Me.tbsCost, Me.tbsTotalCost, Me.tbsSell, Me.tbsTotalSell})
        Me.DataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle1.MappingName = "BldgBOM"
        '
        'tbsQuan
        '
        Me.tbsQuan.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.tbsQuan.Format = ""
        Me.tbsQuan.FormatInfo = Nothing
        Me.tbsQuan.HeaderText = "Quan"
        Me.tbsQuan.MappingName = "Quan"
        Me.tbsQuan.ReadOnly = True
        Me.tbsQuan.Width = 40
        '
        'tbsPartNum
        '
        Me.tbsPartNum.Format = ""
        Me.tbsPartNum.FormatInfo = Nothing
        Me.tbsPartNum.HeaderText = "Part Number"
        Me.tbsPartNum.MappingName = "PartNumber"
        Me.tbsPartNum.NullText = ""
        Me.tbsPartNum.ReadOnly = True
        Me.tbsPartNum.Width = 150
        '
        'tbsDesc
        '
        Me.tbsDesc.Format = ""
        Me.tbsDesc.FormatInfo = Nothing
        Me.tbsDesc.HeaderText = "Description"
        Me.tbsDesc.MappingName = "Description"
        Me.tbsDesc.NullText = ""
        Me.tbsDesc.ReadOnly = True
        Me.tbsDesc.Width = 150
        '
        'tbsCost
        '
        Me.tbsCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.tbsCost.Format = "C"
        Me.tbsCost.FormatInfo = Nothing
        Me.tbsCost.HeaderText = "Cost-"
        Me.tbsCost.MappingName = "Cost"
        Me.tbsCost.NullText = ""
        Me.tbsCost.ReadOnly = True
        Me.tbsCost.Width = 75
        '
        'tbsTotalCost
        '
        Me.tbsTotalCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.tbsTotalCost.Format = "C"
        Me.tbsTotalCost.FormatInfo = Nothing
        Me.tbsTotalCost.HeaderText = "Total Cost-"
        Me.tbsTotalCost.MappingName = "TotalCost"
        Me.tbsTotalCost.NullText = ""
        Me.tbsTotalCost.ReadOnly = True
        Me.tbsTotalCost.Width = 75
        '
        'tbsSell
        '
        Me.tbsSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.tbsSell.Format = "C"
        Me.tbsSell.FormatInfo = Nothing
        Me.tbsSell.HeaderText = "Sell-"
        Me.tbsSell.MappingName = "Sell"
        Me.tbsSell.NullText = ""
        Me.tbsSell.ReadOnly = True
        Me.tbsSell.Width = 75
        '
        'tbsTotalSell
        '
        Me.tbsTotalSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.tbsTotalSell.Format = "C"
        Me.tbsTotalSell.FormatInfo = Nothing
        Me.tbsTotalSell.HeaderText = "Total Sell-"
        Me.tbsTotalSell.MappingName = "TotalSell"
        Me.tbsTotalSell.NullText = ""
        Me.tbsTotalSell.ReadOnly = True
        Me.tbsTotalSell.Width = 75
        '
        'cboBldg
        '
        Me.cboBldg.Location = New System.Drawing.Point(8, 20)
        Me.cboBldg.Name = "cboBldg"
        Me.cboBldg.Size = New System.Drawing.Size(360, 21)
        Me.cboBldg.TabIndex = 0
        '
        'tpgShipping
        '
        Me.tpgShipping.Controls.Add(Me.btnUpdateShipping)
        Me.tpgShipping.Controls.Add(Me.dg_ShipDates)
        Me.tpgShipping.Location = New System.Drawing.Point(4, 22)
        Me.tpgShipping.Name = "tpgShipping"
        Me.tpgShipping.Size = New System.Drawing.Size(712, 402)
        Me.tpgShipping.TabIndex = 7
        Me.tpgShipping.Text = "Ship Dates"
        '
        'btnUpdateShipping
        '
        Me.btnUpdateShipping.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateShipping.Location = New System.Drawing.Point(32, 44)
        Me.btnUpdateShipping.Name = "btnUpdateShipping"
        Me.btnUpdateShipping.Size = New System.Drawing.Size(92, 16)
        Me.btnUpdateShipping.TabIndex = 1
        Me.btnUpdateShipping.Text = "Update"
        '
        'dg_ShipDates
        '
        Me.dg_ShipDates.DataMember = ""
        Me.dg_ShipDates.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dg_ShipDates.Location = New System.Drawing.Point(24, 40)
        Me.dg_ShipDates.Name = "dg_ShipDates"
        Me.dg_ShipDates.Size = New System.Drawing.Size(408, 240)
        Me.dg_ShipDates.TabIndex = 0
        Me.dg_ShipDates.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgShipDatesSty})
        '
        'dgShipDatesSty
        '
        Me.dgShipDatesSty.DataGrid = Me.dg_ShipDates
        Me.dgShipDatesSty.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgsBldgID, Me.dgsBldgNumber, Me.dgsBldgName, Me.dgsShipSequence, Me.dgsShipDateR, Me.dgsShipDate})
        Me.dgShipDatesSty.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgShipDatesSty.MappingName = "ShipDates"
        '
        'dgsBldgID
        '
        Me.dgsBldgID.Format = ""
        Me.dgsBldgID.FormatInfo = Nothing
        Me.dgsBldgID.MappingName = "BldgID"
        Me.dgsBldgID.Width = 0
        '
        'dgsBldgNumber
        '
        Me.dgsBldgNumber.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgsBldgNumber.Format = ""
        Me.dgsBldgNumber.FormatInfo = Nothing
        Me.dgsBldgNumber.HeaderText = "Bldg Number"
        Me.dgsBldgNumber.MappingName = "BldgNumber"
        Me.dgsBldgNumber.ReadOnly = True
        Me.dgsBldgNumber.Width = 75
        '
        'dgsBldgName
        '
        Me.dgsBldgName.Format = ""
        Me.dgsBldgName.FormatInfo = Nothing
        Me.dgsBldgName.HeaderText = "Bldg Name"
        Me.dgsBldgName.MappingName = "BldgName"
        Me.dgsBldgName.ReadOnly = True
        Me.dgsBldgName.Width = 125
        '
        'dgsShipSequence
        '
        Me.dgsShipSequence.Format = ""
        Me.dgsShipSequence.FormatInfo = Nothing
        Me.dgsShipSequence.HeaderText = "Ship Sequence"
        Me.dgsShipSequence.MappingName = "ShipSequence"
        Me.dgsShipSequence.Width = 0
        '
        'dgsShipDateR
        '
        Me.dgsShipDateR.Format = "d"
        Me.dgsShipDateR.FormatInfo = Nothing
        Me.dgsShipDateR.HeaderText = "Ship Date R"
        Me.dgsShipDateR.MappingName = "ShipDateR"
        Me.dgsShipDateR.NullText = ""
        Me.dgsShipDateR.Width = 75
        '
        'dgsShipDate
        '
        Me.dgsShipDate.Format = "d"
        Me.dgsShipDate.FormatInfo = Nothing
        Me.dgsShipDate.HeaderText = "Ship Date T"
        Me.dgsShipDate.MappingName = "ShipDate"
        Me.dgsShipDate.Width = 75
        '
        'tpgSummary
        '
        Me.tpgSummary.Controls.Add(Me.Label50)
        Me.tpgSummary.Controls.Add(Me.txtTotCost)
        Me.tpgSummary.Controls.Add(Me.txtTotSell)
        Me.tpgSummary.Controls.Add(Me.txtAS)
        Me.tpgSummary.Controls.Add(Me.txtAC)
        Me.tpgSummary.Controls.Add(Me.txtCOS)
        Me.tpgSummary.Controls.Add(Me.txtCOC)
        Me.tpgSummary.Controls.Add(Me.txtCS)
        Me.tpgSummary.Controls.Add(Me.txtCC)
        Me.tpgSummary.Controls.Add(Me.txtSS)
        Me.tpgSummary.Controls.Add(Me.txtSC)
        Me.tpgSummary.Controls.Add(Me.txtUS)
        Me.tpgSummary.Controls.Add(Me.txtUC)
        Me.tpgSummary.Controls.Add(Me.Label49)
        Me.tpgSummary.Controls.Add(Me.Label48)
        Me.tpgSummary.Controls.Add(Me.Label47)
        Me.tpgSummary.Controls.Add(Me.Label46)
        Me.tpgSummary.Controls.Add(Me.Label45)
        Me.tpgSummary.Controls.Add(Me.Label44)
        Me.tpgSummary.Controls.Add(Me.Label43)
        Me.tpgSummary.Controls.Add(Me.dgShipSummary)
        Me.tpgSummary.Controls.Add(Me.dgSummary)
        Me.tpgSummary.Location = New System.Drawing.Point(4, 22)
        Me.tpgSummary.Name = "tpgSummary"
        Me.tpgSummary.Size = New System.Drawing.Size(712, 402)
        Me.tpgSummary.TabIndex = 2
        Me.tpgSummary.Text = "Summary"
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(520, 12)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(72, 16)
        Me.Label50.TabIndex = 21
        Me.Label50.Text = "Totals"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotCost
        '
        Me.txtTotCost.BackColor = System.Drawing.SystemColors.Control
        Me.txtTotCost.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotCost.Location = New System.Drawing.Point(516, 36)
        Me.txtTotCost.Name = "txtTotCost"
        Me.txtTotCost.Size = New System.Drawing.Size(76, 13)
        Me.txtTotCost.TabIndex = 20
        Me.txtTotCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotSell
        '
        Me.txtTotSell.BackColor = System.Drawing.SystemColors.Control
        Me.txtTotSell.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotSell.Location = New System.Drawing.Point(516, 60)
        Me.txtTotSell.Name = "txtTotSell"
        Me.txtTotSell.Size = New System.Drawing.Size(76, 13)
        Me.txtTotSell.TabIndex = 19
        Me.txtTotSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAS
        '
        Me.txtAS.BackColor = System.Drawing.SystemColors.Control
        Me.txtAS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAS.Location = New System.Drawing.Point(432, 60)
        Me.txtAS.Name = "txtAS"
        Me.txtAS.Size = New System.Drawing.Size(76, 13)
        Me.txtAS.TabIndex = 18
        Me.txtAS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAC
        '
        Me.txtAC.BackColor = System.Drawing.SystemColors.Control
        Me.txtAC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAC.Location = New System.Drawing.Point(432, 36)
        Me.txtAC.Name = "txtAC"
        Me.txtAC.Size = New System.Drawing.Size(76, 13)
        Me.txtAC.TabIndex = 17
        Me.txtAC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCOS
        '
        Me.txtCOS.BackColor = System.Drawing.SystemColors.Control
        Me.txtCOS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCOS.Location = New System.Drawing.Point(348, 60)
        Me.txtCOS.Name = "txtCOS"
        Me.txtCOS.Size = New System.Drawing.Size(76, 13)
        Me.txtCOS.TabIndex = 16
        Me.txtCOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCOC
        '
        Me.txtCOC.BackColor = System.Drawing.SystemColors.Control
        Me.txtCOC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCOC.Location = New System.Drawing.Point(348, 36)
        Me.txtCOC.Name = "txtCOC"
        Me.txtCOC.Size = New System.Drawing.Size(76, 13)
        Me.txtCOC.TabIndex = 15
        Me.txtCOC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCS
        '
        Me.txtCS.BackColor = System.Drawing.SystemColors.Control
        Me.txtCS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCS.Location = New System.Drawing.Point(264, 60)
        Me.txtCS.Name = "txtCS"
        Me.txtCS.Size = New System.Drawing.Size(76, 13)
        Me.txtCS.TabIndex = 14
        Me.txtCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCC
        '
        Me.txtCC.BackColor = System.Drawing.SystemColors.Control
        Me.txtCC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCC.Location = New System.Drawing.Point(264, 36)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(76, 13)
        Me.txtCC.TabIndex = 13
        Me.txtCC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSS
        '
        Me.txtSS.BackColor = System.Drawing.SystemColors.Control
        Me.txtSS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSS.Location = New System.Drawing.Point(180, 60)
        Me.txtSS.Name = "txtSS"
        Me.txtSS.Size = New System.Drawing.Size(76, 13)
        Me.txtSS.TabIndex = 12
        Me.txtSS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSC
        '
        Me.txtSC.BackColor = System.Drawing.SystemColors.Control
        Me.txtSC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSC.Location = New System.Drawing.Point(180, 36)
        Me.txtSC.Name = "txtSC"
        Me.txtSC.Size = New System.Drawing.Size(76, 13)
        Me.txtSC.TabIndex = 11
        Me.txtSC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUS
        '
        Me.txtUS.BackColor = System.Drawing.SystemColors.Control
        Me.txtUS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUS.Location = New System.Drawing.Point(96, 60)
        Me.txtUS.Name = "txtUS"
        Me.txtUS.Size = New System.Drawing.Size(76, 13)
        Me.txtUS.TabIndex = 10
        Me.txtUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUC
        '
        Me.txtUC.BackColor = System.Drawing.SystemColors.Control
        Me.txtUC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUC.Location = New System.Drawing.Point(96, 36)
        Me.txtUC.Name = "txtUC"
        Me.txtUC.Size = New System.Drawing.Size(76, 13)
        Me.txtUC.TabIndex = 9
        Me.txtUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(432, 12)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(72, 16)
        Me.Label49.TabIndex = 8
        Me.Label49.Text = "Aux"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label48
        '
        Me.Label48.Location = New System.Drawing.Point(352, 12)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(72, 16)
        Me.Label48.TabIndex = 7
        Me.Label48.Text = "Common"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label47
        '
        Me.Label47.Location = New System.Drawing.Point(264, 12)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(76, 16)
        Me.Label47.TabIndex = 6
        Me.Label47.Text = "Clubhouse"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(180, 12)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(76, 16)
        Me.Label46.TabIndex = 5
        Me.Label46.Text = "Site"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label45
        '
        Me.Label45.Location = New System.Drawing.Point(96, 12)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(76, 16)
        Me.Label45.TabIndex = 4
        Me.Label45.Text = "Units"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label44
        '
        Me.Label44.Location = New System.Drawing.Point(24, 60)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(60, 16)
        Me.Label44.TabIndex = 3
        Me.Label44.Text = "Sell"
        '
        'Label43
        '
        Me.Label43.Location = New System.Drawing.Point(24, 38)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(56, 16)
        Me.Label43.TabIndex = 2
        Me.Label43.Text = "Cost"
        '
        'dgShipSummary
        '
        Me.dgShipSummary.CaptionText = "Building Summary"
        Me.dgShipSummary.DataMember = ""
        Me.dgShipSummary.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgShipSummary.Location = New System.Drawing.Point(16, 84)
        Me.dgShipSummary.Name = "dgShipSummary"
        Me.dgShipSummary.ReadOnly = True
        Me.dgShipSummary.Size = New System.Drawing.Size(672, 140)
        Me.dgShipSummary.TabIndex = 1
        Me.dgShipSummary.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.tsShipDates})
        '
        'tsShipDates
        '
        Me.tsShipDates.DataGrid = Me.dgShipSummary
        Me.tsShipDates.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgBldgNumber, Me.dgBldgName, Me.dgType, Me.dgShippedR, Me.dgbShippedT, Me.dgShpDateR, Me.dgShpDateT, Me.dgCost, Me.dgSell})
        Me.tsShipDates.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.tsShipDates.MappingName = "ShipDates"
        '
        'dgBldgNumber
        '
        Me.dgBldgNumber.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgBldgNumber.Format = ""
        Me.dgBldgNumber.FormatInfo = Nothing
        Me.dgBldgNumber.HeaderText = "Bldg #"
        Me.dgBldgNumber.MappingName = "BldgNumber"
        Me.dgBldgNumber.Width = 50
        '
        'dgBldgName
        '
        Me.dgBldgName.Format = ""
        Me.dgBldgName.FormatInfo = Nothing
        Me.dgBldgName.HeaderText = "Name"
        Me.dgBldgName.MappingName = "BldgName"
        Me.dgBldgName.Width = 105
        '
        'dgType
        '
        Me.dgType.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgType.Format = ""
        Me.dgType.FormatInfo = Nothing
        Me.dgType.HeaderText = "Type"
        Me.dgType.MappingName = "Type"
        Me.dgType.Width = 60
        '
        'dgShippedR
        '
        Me.dgShippedR.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgShippedR.HeaderText = "R Shpd"
        Me.dgShippedR.MappingName = "ShippedR"
        Me.dgShippedR.Width = 50
        '
        'dgbShippedT
        '
        Me.dgbShippedT.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgbShippedT.HeaderText = "T Shpd"
        Me.dgbShippedT.MappingName = "shipped"
        Me.dgbShippedT.Width = 50
        '
        'dgShpDateR
        '
        Me.dgShpDateR.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgShpDateR.Format = "d"
        Me.dgShpDateR.FormatInfo = Nothing
        Me.dgShpDateR.HeaderText = "R Shp Date"
        Me.dgShpDateR.MappingName = "ShipDateR"
        Me.dgShpDateR.Width = 75
        '
        'dgShpDateT
        '
        Me.dgShpDateT.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgShpDateT.Format = "d"
        Me.dgShpDateT.FormatInfo = Nothing
        Me.dgShpDateT.HeaderText = "T Shp Date"
        Me.dgShpDateT.MappingName = "ShipDate"
        Me.dgShpDateT.Width = 75
        '
        'dgCost
        '
        Me.dgCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgCost.Format = "C"
        Me.dgCost.FormatInfo = Nothing
        Me.dgCost.HeaderText = "Cost-"
        Me.dgCost.MappingName = "Cost"
        Me.dgCost.Width = 75
        '
        'dgSell
        '
        Me.dgSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgSell.Format = "C"
        Me.dgSell.FormatInfo = Nothing
        Me.dgSell.HeaderText = "Sell-"
        Me.dgSell.MappingName = "Sell"
        Me.dgSell.Width = 75
        '
        'dgSummary
        '
        Me.dgSummary.CaptionText = "BOM Summary"
        Me.dgSummary.DataMember = ""
        Me.dgSummary.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgSummary.Location = New System.Drawing.Point(16, 224)
        Me.dgSummary.Name = "dgSummary"
        Me.dgSummary.ReadOnly = True
        Me.dgSummary.Size = New System.Drawing.Size(672, 168)
        Me.dgSummary.TabIndex = 0
        Me.dgSummary.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.stySummary})
        '
        'stySummary
        '
        Me.stySummary.DataGrid = Me.dgSummary
        Me.stySummary.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgPN, Me.dgSQuan, Me.dgSCost, Me.dgSMargin, Me.dgSSell, Me.dgSTotal, Me.dgSShipped, Me.dgSBO})
        Me.stySummary.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.stySummary.MappingName = "Summary"
        '
        'dgPN
        '
        Me.dgPN.Format = ""
        Me.dgPN.FormatInfo = Nothing
        Me.dgPN.HeaderText = "Part Number"
        Me.dgPN.MappingName = "PartNumber"
        Me.dgPN.ReadOnly = True
        Me.dgPN.Width = 200
        '
        'dgSQuan
        '
        Me.dgSQuan.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgSQuan.Format = ""
        Me.dgSQuan.FormatInfo = Nothing
        Me.dgSQuan.HeaderText = "Quan"
        Me.dgSQuan.MappingName = "Quan"
        Me.dgSQuan.NullText = ""
        Me.dgSQuan.ReadOnly = True
        Me.dgSQuan.Width = 50
        '
        'dgSCost
        '
        Me.dgSCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgSCost.Format = "C"
        Me.dgSCost.FormatInfo = Nothing
        Me.dgSCost.HeaderText = "Cost-"
        Me.dgSCost.MappingName = "Cost"
        Me.dgSCost.NullText = ""
        Me.dgSCost.Width = 105
        '
        'dgSMargin
        '
        Me.dgSMargin.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgSMargin.Format = ""
        Me.dgSMargin.FormatInfo = Nothing
        Me.dgSMargin.HeaderText = "Margin"
        Me.dgSMargin.MappingName = "Margin"
        Me.dgSMargin.NullText = ""
        Me.dgSMargin.ReadOnly = True
        Me.dgSMargin.Width = 50
        '
        'dgSSell
        '
        Me.dgSSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgSSell.Format = "C"
        Me.dgSSell.FormatInfo = Nothing
        Me.dgSSell.HeaderText = "Sell-"
        Me.dgSSell.MappingName = "Sell"
        Me.dgSSell.ReadOnly = True
        Me.dgSSell.Width = 105
        '
        'dgSTotal
        '
        Me.dgSTotal.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgSTotal.Format = "C"
        Me.dgSTotal.FormatInfo = Nothing
        Me.dgSTotal.HeaderText = "Total-"
        Me.dgSTotal.MappingName = "Total"
        Me.dgSTotal.NullText = ""
        Me.dgSTotal.Width = 105
        '
        'dgSShipped
        '
        Me.dgSShipped.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgSShipped.Format = ""
        Me.dgSShipped.FormatInfo = Nothing
        Me.dgSShipped.MappingName = "Shipped"
        Me.dgSShipped.NullText = ""
        Me.dgSShipped.ReadOnly = True
        Me.dgSShipped.Width = 0
        '
        'dgSBO
        '
        Me.dgSBO.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgSBO.Format = ""
        Me.dgSBO.FormatInfo = Nothing
        Me.dgSBO.MappingName = "BO"
        Me.dgSBO.ReadOnly = True
        Me.dgSBO.Width = 0
        '
        'tpgBulkEdits
        '
        Me.tpgBulkEdits.Controls.Add(Me.btnUpdateShipDates)
        Me.tpgBulkEdits.Controls.Add(Me.btnAddToUnits)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERLocation)
        Me.tpgBulkEdits.Controls.Add(Me.lblBERLocation)
        Me.tpgBulkEdits.Controls.Add(Me.lblBERQuan)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERQuan)
        Me.tpgBulkEdits.Controls.Add(Me.btnAddUnitWC)
        Me.tpgBulkEdits.Controls.Add(Me.txtUnitWC)
        Me.tpgBulkEdits.Controls.Add(Me.Label42)
        Me.tpgBulkEdits.Controls.Add(Me.Label41)
        Me.tpgBulkEdits.Controls.Add(Me.btnUpdatePrice)
        Me.tpgBulkEdits.Controls.Add(Me.Label36)
        Me.tpgBulkEdits.Controls.Add(Me.Label35)
        Me.tpgBulkEdits.Controls.Add(Me.Label34)
        Me.tpgBulkEdits.Controls.Add(Me.lblBerType)
        Me.tpgBulkEdits.Controls.Add(Me.Label32)
        Me.tpgBulkEdits.Controls.Add(Me.Label31)
        Me.tpgBulkEdits.Controls.Add(Me.Label30)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERSell)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERMargin)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERCost)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERType)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERRT)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERDesc)
        Me.tpgBulkEdits.Controls.Add(Me.txtBERProdID)
        Me.tpgBulkEdits.Controls.Add(Me.btnBEAdjustSell)
        Me.tpgBulkEdits.Controls.Add(Me.Label29)
        Me.tpgBulkEdits.Controls.Add(Me.btnBERByType)
        Me.tpgBulkEdits.Controls.Add(Me.btnBERByPartNum)
        Me.tpgBulkEdits.Controls.Add(Me.btnBEDelete)
        Me.tpgBulkEdits.Controls.Add(Me.cboBEType)
        Me.tpgBulkEdits.Controls.Add(Me.Label28)
        Me.tpgBulkEdits.Controls.Add(Me.cbxFSell)
        Me.tpgBulkEdits.Controls.Add(Me.Label27)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBMargin)
        Me.tpgBulkEdits.Controls.Add(Me.Label26)
        Me.tpgBulkEdits.Controls.Add(Me.Label19)
        Me.tpgBulkEdits.Controls.Add(Me.lblBEBType)
        Me.tpgBulkEdits.Controls.Add(Me.Label17)
        Me.tpgBulkEdits.Controls.Add(Me.Label15)
        Me.tpgBulkEdits.Controls.Add(Me.Label13)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBSell)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBCost)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBType)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBRT)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBDesc)
        Me.tpgBulkEdits.Controls.Add(Me.txtBEBProdID)
        Me.tpgBulkEdits.Controls.Add(Me.rbBESO)
        Me.tpgBulkEdits.Controls.Add(Me.rbBEMain)
        Me.tpgBulkEdits.Controls.Add(Me.cboBECatalog)
        Me.tpgBulkEdits.Controls.Add(Me.cboBEBidItems)
        Me.tpgBulkEdits.Controls.Add(Me.TextBox8)
        Me.tpgBulkEdits.Controls.Add(Me.TextBox9)
        Me.tpgBulkEdits.Location = New System.Drawing.Point(4, 22)
        Me.tpgBulkEdits.Name = "tpgBulkEdits"
        Me.tpgBulkEdits.Size = New System.Drawing.Size(712, 402)
        Me.tpgBulkEdits.TabIndex = 5
        Me.tpgBulkEdits.Text = "Bulk Edits"
        '
        'btnUpdateShipDates
        '
        Me.btnUpdateShipDates.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateShipDates.Location = New System.Drawing.Point(512, 248)
        Me.btnUpdateShipDates.Name = "btnUpdateShipDates"
        Me.btnUpdateShipDates.Size = New System.Drawing.Size(120, 20)
        Me.btnUpdateShipDates.TabIndex = 92
        Me.btnUpdateShipDates.Text = "Update Ship Dates"
        '
        'btnAddToUnits
        '
        Me.btnAddToUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddToUnits.Location = New System.Drawing.Point(388, 248)
        Me.btnAddToUnits.Name = "btnAddToUnits"
        Me.btnAddToUnits.Size = New System.Drawing.Size(124, 20)
        Me.btnAddToUnits.TabIndex = 27
        Me.btnAddToUnits.Text = "Add To Units Via Wildcard"
        '
        'txtBERLocation
        '
        Me.txtBERLocation.Location = New System.Drawing.Point(348, 348)
        Me.txtBERLocation.Name = "txtBERLocation"
        Me.txtBERLocation.Size = New System.Drawing.Size(92, 20)
        Me.txtBERLocation.TabIndex = 20
        Me.txtBERLocation.Visible = False
        '
        'lblBERLocation
        '
        Me.lblBERLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBERLocation.Location = New System.Drawing.Point(292, 350)
        Me.lblBERLocation.Name = "lblBERLocation"
        Me.lblBERLocation.Size = New System.Drawing.Size(48, 16)
        Me.lblBERLocation.TabIndex = 91
        Me.lblBERLocation.Text = "Location"
        Me.lblBERLocation.Visible = False
        '
        'lblBERQuan
        '
        Me.lblBERQuan.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBERQuan.Location = New System.Drawing.Point(184, 350)
        Me.lblBERQuan.Name = "lblBERQuan"
        Me.lblBERQuan.Size = New System.Drawing.Size(44, 16)
        Me.lblBERQuan.TabIndex = 90
        Me.lblBERQuan.Text = "Quan"
        Me.lblBERQuan.Visible = False
        '
        'txtBERQuan
        '
        Me.txtBERQuan.Location = New System.Drawing.Point(232, 348)
        Me.txtBERQuan.Name = "txtBERQuan"
        Me.txtBERQuan.Size = New System.Drawing.Size(52, 20)
        Me.txtBERQuan.TabIndex = 19
        Me.txtBERQuan.Visible = False
        '
        'btnAddUnitWC
        '
        Me.btnAddUnitWC.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddUnitWC.Location = New System.Drawing.Point(508, 348)
        Me.btnAddUnitWC.Name = "btnAddUnitWC"
        Me.btnAddUnitWC.Size = New System.Drawing.Size(112, 20)
        Me.btnAddUnitWC.TabIndex = 22
        Me.btnAddUnitWC.Text = "Add To All Units LIke"
        Me.btnAddUnitWC.Visible = False
        '
        'txtUnitWC
        '
        Me.txtUnitWC.Location = New System.Drawing.Point(620, 348)
        Me.txtUnitWC.Name = "txtUnitWC"
        Me.txtUnitWC.Size = New System.Drawing.Size(68, 20)
        Me.txtUnitWC.TabIndex = 21
        Me.txtUnitWC.Visible = False
        '
        'Label42
        '
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(16, 279)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(200, 16)
        Me.Label42.TabIndex = 86
        Me.Label42.Text = "With This One -------------"
        '
        'Label41
        '
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(16, 103)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(256, 23)
        Me.Label41.TabIndex = 85
        Me.Label41.Text = "Replace This Item -----------"
        '
        'btnUpdatePrice
        '
        Me.btnUpdatePrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdatePrice.Location = New System.Drawing.Point(672, 143)
        Me.btnUpdatePrice.Name = "btnUpdatePrice"
        Me.btnUpdatePrice.Size = New System.Drawing.Size(24, 16)
        Me.btnUpdatePrice.TabIndex = 11
        Me.btnUpdatePrice.Text = "Calc"
        '
        'Label36
        '
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(632, 303)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(24, 16)
        Me.Label36.TabIndex = 83
        Me.Label36.Text = "Sell"
        '
        'Label35
        '
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(512, 303)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(48, 16)
        Me.Label35.TabIndex = 82
        Me.Label35.Text = "Margin"
        '
        'Label34
        '
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(456, 303)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(40, 16)
        Me.Label34.TabIndex = 81
        Me.Label34.Text = "Cost"
        '
        'lblBerType
        '
        Me.lblBerType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBerType.Location = New System.Drawing.Point(408, 303)
        Me.lblBerType.Name = "lblBerType"
        Me.lblBerType.Size = New System.Drawing.Size(40, 16)
        Me.lblBerType.TabIndex = 80
        Me.lblBerType.Text = "Type"
        Me.lblBerType.Visible = False
        '
        'Label32
        '
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(368, 303)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(40, 16)
        Me.Label32.TabIndex = 79
        Me.Label32.Text = "R/T"
        '
        'Label31
        '
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(160, 303)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(88, 16)
        Me.Label31.TabIndex = 78
        Me.Label31.Text = "Description"
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(16, 303)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(104, 16)
        Me.Label30.TabIndex = 77
        Me.Label30.Text = "Product ID"
        '
        'txtBERSell
        '
        Me.txtBERSell.Location = New System.Drawing.Point(632, 319)
        Me.txtBERSell.Name = "txtBERSell"
        Me.txtBERSell.Size = New System.Drawing.Size(56, 20)
        Me.txtBERSell.TabIndex = 18
        '
        'txtBERMargin
        '
        Me.txtBERMargin.Location = New System.Drawing.Point(512, 320)
        Me.txtBERMargin.Name = "txtBERMargin"
        Me.txtBERMargin.Size = New System.Drawing.Size(48, 20)
        Me.txtBERMargin.TabIndex = 17
        '
        'txtBERCost
        '
        Me.txtBERCost.Location = New System.Drawing.Point(456, 320)
        Me.txtBERCost.Name = "txtBERCost"
        Me.txtBERCost.Size = New System.Drawing.Size(56, 20)
        Me.txtBERCost.TabIndex = 16
        '
        'txtBERType
        '
        Me.txtBERType.Location = New System.Drawing.Point(408, 320)
        Me.txtBERType.Name = "txtBERType"
        Me.txtBERType.Size = New System.Drawing.Size(48, 20)
        Me.txtBERType.TabIndex = 15
        Me.txtBERType.Visible = False
        '
        'txtBERRT
        '
        Me.txtBERRT.Location = New System.Drawing.Point(368, 320)
        Me.txtBERRT.Name = "txtBERRT"
        Me.txtBERRT.Size = New System.Drawing.Size(40, 20)
        Me.txtBERRT.TabIndex = 14
        '
        'txtBERDesc
        '
        Me.txtBERDesc.Location = New System.Drawing.Point(160, 320)
        Me.txtBERDesc.Name = "txtBERDesc"
        Me.txtBERDesc.Size = New System.Drawing.Size(208, 20)
        Me.txtBERDesc.TabIndex = 13
        '
        'txtBERProdID
        '
        Me.txtBERProdID.Location = New System.Drawing.Point(16, 320)
        Me.txtBERProdID.MaxLength = 25
        Me.txtBERProdID.Name = "txtBERProdID"
        Me.txtBERProdID.Size = New System.Drawing.Size(144, 20)
        Me.txtBERProdID.TabIndex = 12
        '
        'btnBEAdjustSell
        '
        Me.btnBEAdjustSell.Location = New System.Drawing.Point(496, 183)
        Me.btnBEAdjustSell.Name = "btnBEAdjustSell"
        Me.btnBEAdjustSell.Size = New System.Drawing.Size(160, 23)
        Me.btnBEAdjustSell.TabIndex = 26
        Me.btnBEAdjustSell.Text = "Adjust Selling Price"
        '
        'Label29
        '
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(392, 63)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(120, 16)
        Me.Label29.TabIndex = 76
        Me.Label29.Text = "Types Included In This Job"
        '
        'btnBERByType
        '
        Me.btnBERByType.Location = New System.Drawing.Point(336, 183)
        Me.btnBERByType.Name = "btnBERByType"
        Me.btnBERByType.Size = New System.Drawing.Size(160, 23)
        Me.btnBERByType.TabIndex = 25
        Me.btnBERByType.Text = "Replace By Type"
        '
        'btnBERByPartNum
        '
        Me.btnBERByPartNum.Location = New System.Drawing.Point(176, 183)
        Me.btnBERByPartNum.Name = "btnBERByPartNum"
        Me.btnBERByPartNum.Size = New System.Drawing.Size(160, 23)
        Me.btnBERByPartNum.TabIndex = 24
        Me.btnBERByPartNum.Text = "Replace By Part Number"
        '
        'btnBEDelete
        '
        Me.btnBEDelete.Location = New System.Drawing.Point(16, 183)
        Me.btnBEDelete.Name = "btnBEDelete"
        Me.btnBEDelete.Size = New System.Drawing.Size(160, 23)
        Me.btnBEDelete.TabIndex = 23
        Me.btnBEDelete.Text = "Delete"
        '
        'cboBEType
        '
        Me.cboBEType.Location = New System.Drawing.Point(392, 79)
        Me.cboBEType.Name = "cboBEType"
        Me.cboBEType.Size = New System.Drawing.Size(288, 21)
        Me.cboBEType.TabIndex = 1
        '
        'Label28
        '
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(16, 63)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(100, 16)
        Me.Label28.TabIndex = 74
        Me.Label28.Text = "Current Items"
        '
        'cbxFSell
        '
        Me.cbxFSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFSell.Location = New System.Drawing.Point(566, 143)
        Me.cbxFSell.Name = "cbxFSell"
        Me.cbxFSell.Size = New System.Drawing.Size(48, 24)
        Me.cbxFSell.TabIndex = 9
        Me.cbxFSell.Text = "Fixed Sell"
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(512, 127)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(40, 16)
        Me.Label27.TabIndex = 71
        Me.Label27.Text = "Margin"
        '
        'txtBEBMargin
        '
        Me.txtBEBMargin.Location = New System.Drawing.Point(512, 143)
        Me.txtBEBMargin.Name = "txtBEBMargin"
        Me.txtBEBMargin.Size = New System.Drawing.Size(48, 20)
        Me.txtBEBMargin.TabIndex = 8
        Me.txtBEBMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(615, 127)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(40, 16)
        Me.Label26.TabIndex = 68
        Me.Label26.Text = "Sell"
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(456, 127)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(32, 16)
        Me.Label19.TabIndex = 65
        Me.Label19.Text = "Cost"
        '
        'lblBEBType
        '
        Me.lblBEBType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBEBType.Location = New System.Drawing.Point(408, 127)
        Me.lblBEBType.Name = "lblBEBType"
        Me.lblBEBType.Size = New System.Drawing.Size(40, 16)
        Me.lblBEBType.TabIndex = 63
        Me.lblBEBType.Text = "Type"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(368, 127)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 16)
        Me.Label17.TabIndex = 61
        Me.Label17.Text = "R/T"
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(160, 127)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 59
        Me.Label15.Text = "Description"
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(16, 127)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 58
        Me.Label13.Text = "Product ID"
        '
        'txtBEBSell
        '
        Me.txtBEBSell.Location = New System.Drawing.Point(615, 143)
        Me.txtBEBSell.Name = "txtBEBSell"
        Me.txtBEBSell.Size = New System.Drawing.Size(56, 20)
        Me.txtBEBSell.TabIndex = 10
        Me.txtBEBSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBEBCost
        '
        Me.txtBEBCost.Location = New System.Drawing.Point(456, 143)
        Me.txtBEBCost.Name = "txtBEBCost"
        Me.txtBEBCost.Size = New System.Drawing.Size(56, 20)
        Me.txtBEBCost.TabIndex = 7
        Me.txtBEBCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBEBType
        '
        Me.txtBEBType.Enabled = False
        Me.txtBEBType.Location = New System.Drawing.Point(408, 143)
        Me.txtBEBType.Name = "txtBEBType"
        Me.txtBEBType.Size = New System.Drawing.Size(48, 20)
        Me.txtBEBType.TabIndex = 6
        Me.txtBEBType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtBEBRT
        '
        Me.txtBEBRT.Location = New System.Drawing.Point(368, 143)
        Me.txtBEBRT.Name = "txtBEBRT"
        Me.txtBEBRT.Size = New System.Drawing.Size(40, 20)
        Me.txtBEBRT.TabIndex = 5
        '
        'txtBEBDesc
        '
        Me.txtBEBDesc.Enabled = False
        Me.txtBEBDesc.Location = New System.Drawing.Point(160, 143)
        Me.txtBEBDesc.Name = "txtBEBDesc"
        Me.txtBEBDesc.Size = New System.Drawing.Size(208, 20)
        Me.txtBEBDesc.TabIndex = 4
        '
        'txtBEBProdID
        '
        Me.txtBEBProdID.Enabled = False
        Me.txtBEBProdID.Location = New System.Drawing.Point(16, 143)
        Me.txtBEBProdID.Name = "txtBEBProdID"
        Me.txtBEBProdID.Size = New System.Drawing.Size(144, 20)
        Me.txtBEBProdID.TabIndex = 3
        '
        'rbBESO
        '
        Me.rbBESO.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBESO.Location = New System.Drawing.Point(112, 223)
        Me.rbBESO.Name = "rbBESO"
        Me.rbBESO.Size = New System.Drawing.Size(72, 16)
        Me.rbBESO.TabIndex = 49
        Me.rbBESO.Text = "SO Catalog"
        '
        'rbBEMain
        '
        Me.rbBEMain.Checked = True
        Me.rbBEMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBEMain.Location = New System.Drawing.Point(16, 223)
        Me.rbBEMain.Name = "rbBEMain"
        Me.rbBEMain.Size = New System.Drawing.Size(80, 16)
        Me.rbBEMain.TabIndex = 47
        Me.rbBEMain.TabStop = True
        Me.rbBEMain.Text = "Main Catalog"
        '
        'cboBECatalog
        '
        Me.cboBECatalog.Location = New System.Drawing.Point(16, 247)
        Me.cboBECatalog.Name = "cboBECatalog"
        Me.cboBECatalog.Size = New System.Drawing.Size(352, 21)
        Me.cboBECatalog.TabIndex = 2
        '
        'cboBEBidItems
        '
        Me.cboBEBidItems.Location = New System.Drawing.Point(16, 79)
        Me.cboBEBidItems.Name = "cboBEBidItems"
        Me.cboBEBidItems.Size = New System.Drawing.Size(344, 21)
        Me.cboBEBidItems.TabIndex = 0
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(420, 320)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(48, 20)
        Me.TextBox8.TabIndex = 64
        Me.TextBox8.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(464, 320)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(56, 20)
        Me.TextBox9.TabIndex = 66
        '
        'tpgComments
        '
        Me.tpgComments.Controls.Add(Me.btnCommentDelete)
        Me.tpgComments.Controls.Add(Me.btnCommentEDit)
        Me.tpgComments.Controls.Add(Me.btnSaveComment)
        Me.tpgComments.Controls.Add(Me.lbCAuthor)
        Me.tpgComments.Controls.Add(Me.lblCComment)
        Me.tpgComments.Controls.Add(Me.txtCComment)
        Me.tpgComments.Controls.Add(Me.txtCAuthor)
        Me.tpgComments.Controls.Add(Me.dgComments)
        Me.tpgComments.Location = New System.Drawing.Point(4, 22)
        Me.tpgComments.Name = "tpgComments"
        Me.tpgComments.Size = New System.Drawing.Size(712, 402)
        Me.tpgComments.TabIndex = 3
        Me.tpgComments.Text = "Comments"
        '
        'btnCommentDelete
        '
        Me.btnCommentDelete.Location = New System.Drawing.Point(616, 372)
        Me.btnCommentDelete.Name = "btnCommentDelete"
        Me.btnCommentDelete.Size = New System.Drawing.Size(84, 20)
        Me.btnCommentDelete.TabIndex = 13
        Me.btnCommentDelete.Text = "Delete"
        Me.btnCommentDelete.Visible = False
        '
        'btnCommentEDit
        '
        Me.btnCommentEDit.Location = New System.Drawing.Point(532, 372)
        Me.btnCommentEDit.Name = "btnCommentEDit"
        Me.btnCommentEDit.Size = New System.Drawing.Size(84, 20)
        Me.btnCommentEDit.TabIndex = 12
        Me.btnCommentEDit.Text = "Edit"
        Me.btnCommentEDit.Visible = False
        '
        'btnSaveComment
        '
        Me.btnSaveComment.Location = New System.Drawing.Point(584, 20)
        Me.btnSaveComment.Name = "btnSaveComment"
        Me.btnSaveComment.Size = New System.Drawing.Size(112, 24)
        Me.btnSaveComment.TabIndex = 11
        Me.btnSaveComment.Text = "Save Comment"
        '
        'lbCAuthor
        '
        Me.lbCAuthor.Location = New System.Drawing.Point(12, 25)
        Me.lbCAuthor.Name = "lbCAuthor"
        Me.lbCAuthor.Size = New System.Drawing.Size(64, 16)
        Me.lbCAuthor.TabIndex = 10
        Me.lbCAuthor.Text = "Entered By"
        '
        'lblCComment
        '
        Me.lblCComment.Location = New System.Drawing.Point(12, 44)
        Me.lblCComment.Name = "lblCComment"
        Me.lblCComment.Size = New System.Drawing.Size(128, 12)
        Me.lblCComment.TabIndex = 9
        Me.lblCComment.Text = "Comment"
        '
        'txtCComment
        '
        Me.txtCComment.Location = New System.Drawing.Point(12, 61)
        Me.txtCComment.MaxLength = 1000
        Me.txtCComment.Multiline = True
        Me.txtCComment.Name = "txtCComment"
        Me.txtCComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCComment.Size = New System.Drawing.Size(684, 64)
        Me.txtCComment.TabIndex = 8
        '
        'txtCAuthor
        '
        Me.txtCAuthor.Location = New System.Drawing.Point(80, 25)
        Me.txtCAuthor.Name = "txtCAuthor"
        Me.txtCAuthor.Size = New System.Drawing.Size(60, 20)
        Me.txtCAuthor.TabIndex = 7
        '
        'dgComments
        '
        Me.dgComments.DataMember = ""
        Me.dgComments.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgComments.Location = New System.Drawing.Point(8, 141)
        Me.dgComments.Name = "dgComments"
        Me.dgComments.Size = New System.Drawing.Size(692, 224)
        Me.dgComments.TabIndex = 6
        '
        'tpgChangeOrders
        '
        Me.tpgChangeOrders.Controls.Add(Me.btnCODelete)
        Me.tpgChangeOrders.Controls.Add(Me.btnCOCancel)
        Me.tpgChangeOrders.Controls.Add(Me.btnCOSave)
        Me.tpgChangeOrders.Controls.Add(Me.Label40)
        Me.tpgChangeOrders.Controls.Add(Me.Label39)
        Me.tpgChangeOrders.Controls.Add(Me.Label38)
        Me.tpgChangeOrders.Controls.Add(Me.Label37)
        Me.tpgChangeOrders.Controls.Add(Me.Label18)
        Me.tpgChangeOrders.Controls.Add(Me.Label16)
        Me.tpgChangeOrders.Controls.Add(Me.Label12)
        Me.tpgChangeOrders.Controls.Add(Me.txtCONewBalance)
        Me.tpgChangeOrders.Controls.Add(Me.txtCOPriorBalance)
        Me.tpgChangeOrders.Controls.Add(Me.txtCODesc)
        Me.tpgChangeOrders.Controls.Add(Me.txtCOMisc)
        Me.tpgChangeOrders.Controls.Add(Me.txtCOApprovedBy)
        Me.tpgChangeOrders.Controls.Add(Me.txtCOEnteredBy)
        Me.tpgChangeOrders.Controls.Add(Me.txtCODate)
        Me.tpgChangeOrders.Controls.Add(Me.dgChangeOrders)
        Me.tpgChangeOrders.Location = New System.Drawing.Point(4, 22)
        Me.tpgChangeOrders.Name = "tpgChangeOrders"
        Me.tpgChangeOrders.Size = New System.Drawing.Size(712, 402)
        Me.tpgChangeOrders.TabIndex = 4
        Me.tpgChangeOrders.Text = "Change Orders"
        '
        'btnCODelete
        '
        Me.btnCODelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCODelete.Location = New System.Drawing.Point(644, 4)
        Me.btnCODelete.Name = "btnCODelete"
        Me.btnCODelete.Size = New System.Drawing.Size(52, 16)
        Me.btnCODelete.TabIndex = 8
        Me.btnCODelete.Text = "Delete"
        Me.btnCODelete.Visible = False
        '
        'btnCOCancel
        '
        Me.btnCOCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCOCancel.Location = New System.Drawing.Point(596, 4)
        Me.btnCOCancel.Name = "btnCOCancel"
        Me.btnCOCancel.Size = New System.Drawing.Size(52, 16)
        Me.btnCOCancel.TabIndex = 7
        Me.btnCOCancel.Text = "Cancel"
        Me.btnCOCancel.Visible = False
        '
        'btnCOSave
        '
        Me.btnCOSave.Location = New System.Drawing.Point(596, 24)
        Me.btnCOSave.Name = "btnCOSave"
        Me.btnCOSave.Size = New System.Drawing.Size(100, 20)
        Me.btnCOSave.TabIndex = 6
        Me.btnCOSave.Text = "Add"
        '
        'Label40
        '
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(8, 64)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(32, 16)
        Me.Label40.TabIndex = 14
        Me.Label40.Text = "Desc"
        '
        'Label39
        '
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(12, 116)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(32, 48)
        Me.Label39.TabIndex = 13
        Me.Label39.Text = "Misc- LD Notes Only"
        '
        'Label38
        '
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(516, 4)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(60, 16)
        Me.Label38.TabIndex = 12
        Me.Label38.Text = "New Balance"
        '
        'Label37
        '
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(432, 4)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(64, 16)
        Me.Label37.TabIndex = 11
        Me.Label37.Text = "Prior Balance"
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(208, 4)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(120, 16)
        Me.Label18.TabIndex = 10
        Me.Label18.Text = "Approved By (Client)"
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(140, 4)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(52, 16)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Entered By"
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(52, 4)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(48, 16)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Date"
        '
        'txtCONewBalance
        '
        Me.txtCONewBalance.Location = New System.Drawing.Point(496, 24)
        Me.txtCONewBalance.Name = "txtCONewBalance"
        Me.txtCONewBalance.Size = New System.Drawing.Size(84, 20)
        Me.txtCONewBalance.TabIndex = 3
        Me.txtCONewBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCOPriorBalance
        '
        Me.txtCOPriorBalance.Location = New System.Drawing.Point(412, 24)
        Me.txtCOPriorBalance.Name = "txtCOPriorBalance"
        Me.txtCOPriorBalance.Size = New System.Drawing.Size(84, 20)
        Me.txtCOPriorBalance.TabIndex = 2
        Me.txtCOPriorBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCODesc
        '
        Me.txtCODesc.Location = New System.Drawing.Point(48, 56)
        Me.txtCODesc.Multiline = True
        Me.txtCODesc.Name = "txtCODesc"
        Me.txtCODesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCODesc.Size = New System.Drawing.Size(648, 48)
        Me.txtCODesc.TabIndex = 4
        '
        'txtCOMisc
        '
        Me.txtCOMisc.Location = New System.Drawing.Point(48, 116)
        Me.txtCOMisc.Multiline = True
        Me.txtCOMisc.Name = "txtCOMisc"
        Me.txtCOMisc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCOMisc.Size = New System.Drawing.Size(652, 52)
        Me.txtCOMisc.TabIndex = 5
        '
        'txtCOApprovedBy
        '
        Me.txtCOApprovedBy.Location = New System.Drawing.Point(200, 24)
        Me.txtCOApprovedBy.Name = "txtCOApprovedBy"
        Me.txtCOApprovedBy.Size = New System.Drawing.Size(212, 20)
        Me.txtCOApprovedBy.TabIndex = 1
        '
        'txtCOEnteredBy
        '
        Me.txtCOEnteredBy.Location = New System.Drawing.Point(136, 24)
        Me.txtCOEnteredBy.Name = "txtCOEnteredBy"
        Me.txtCOEnteredBy.Size = New System.Drawing.Size(64, 20)
        Me.txtCOEnteredBy.TabIndex = 0
        '
        'txtCODate
        '
        Me.txtCODate.Location = New System.Drawing.Point(48, 24)
        Me.txtCODate.Name = "txtCODate"
        Me.txtCODate.Size = New System.Drawing.Size(88, 20)
        Me.txtCODate.TabIndex = 0
        Me.txtCODate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dgChangeOrders
        '
        Me.dgChangeOrders.DataMember = ""
        Me.dgChangeOrders.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgChangeOrders.Location = New System.Drawing.Point(20, 176)
        Me.dgChangeOrders.Name = "dgChangeOrders"
        Me.dgChangeOrders.Size = New System.Drawing.Size(680, 208)
        Me.dgChangeOrders.TabIndex = 9
        Me.dgChangeOrders.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.tsChangeOrder})
        '
        'tsChangeOrder
        '
        Me.tsChangeOrder.DataGrid = Me.dgChangeOrders
        Me.tsChangeOrder.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgCOID, Me.dgDate, Me.dgEnteredBy, Me.dgApprovedBy, Me.dgDesc, Me.dgCOMisc, Me.dgPriorBalance, Me.dgNewBalance})
        Me.tsChangeOrder.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.tsChangeOrder.MappingName = "ChangeOrders"
        '
        'dgCOID
        '
        Me.dgCOID.Format = ""
        Me.dgCOID.FormatInfo = Nothing
        Me.dgCOID.MappingName = "COID"
        Me.dgCOID.Width = 0
        '
        'dgDate
        '
        Me.dgDate.Format = "d"
        Me.dgDate.FormatInfo = Nothing
        Me.dgDate.HeaderText = "Date"
        Me.dgDate.MappingName = "CODate"
        Me.dgDate.NullText = ""
        Me.dgDate.Width = 75
        '
        'dgEnteredBy
        '
        Me.dgEnteredBy.Format = ""
        Me.dgEnteredBy.FormatInfo = Nothing
        Me.dgEnteredBy.MappingName = "EnteredBy"
        Me.dgEnteredBy.Width = 0
        '
        'dgApprovedBy
        '
        Me.dgApprovedBy.Format = ""
        Me.dgApprovedBy.FormatInfo = Nothing
        Me.dgApprovedBy.HeaderText = "Approved By"
        Me.dgApprovedBy.MappingName = "ApprovedBy"
        Me.dgApprovedBy.NullText = ""
        Me.dgApprovedBy.Width = 75
        '
        'dgDesc
        '
        Me.dgDesc.Format = ""
        Me.dgDesc.FormatInfo = Nothing
        Me.dgDesc.HeaderText = "Description"
        Me.dgDesc.MappingName = "CODesc"
        Me.dgDesc.NullText = ""
        Me.dgDesc.Width = 350
        '
        'dgCOMisc
        '
        Me.dgCOMisc.Format = ""
        Me.dgCOMisc.FormatInfo = Nothing
        Me.dgCOMisc.MappingName = "Misc"
        Me.dgCOMisc.Width = 0
        '
        'dgPriorBalance
        '
        Me.dgPriorBalance.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgPriorBalance.Format = "c"
        Me.dgPriorBalance.FormatInfo = Nothing
        Me.dgPriorBalance.HeaderText = "Prior Balance"
        Me.dgPriorBalance.MappingName = "PriorBalance"
        Me.dgPriorBalance.NullText = ""
        Me.dgPriorBalance.Width = 75
        '
        'dgNewBalance
        '
        Me.dgNewBalance.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgNewBalance.Format = "c"
        Me.dgNewBalance.FormatInfo = Nothing
        Me.dgNewBalance.HeaderText = "New Balance"
        Me.dgNewBalance.MappingName = "NewBalance"
        Me.dgNewBalance.NullText = ""
        Me.dgNewBalance.Width = 75
        '
        'tpgBidComments
        '
        Me.tpgBidComments.Controls.Add(Me.lblBidComments)
        Me.tpgBidComments.Controls.Add(Me.txtBidComment)
        Me.tpgBidComments.Controls.Add(Me.dgBidComments)
        Me.tpgBidComments.Location = New System.Drawing.Point(4, 22)
        Me.tpgBidComments.Name = "tpgBidComments"
        Me.tpgBidComments.Size = New System.Drawing.Size(712, 402)
        Me.tpgBidComments.TabIndex = 8
        Me.tpgBidComments.Text = "Bid Comments"
        '
        'lblBidComments
        '
        Me.lblBidComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBidComments.Location = New System.Drawing.Point(16, 4)
        Me.lblBidComments.Name = "lblBidComments"
        Me.lblBidComments.Size = New System.Drawing.Size(556, 12)
        Me.lblBidComments.TabIndex = 3
        Me.lblBidComments.Text = "These Comments Were Entered In The Origianl Bid   - Click On The Left Column Of A" & _
            " Comment To View The Entire Comment"
        '
        'txtBidComment
        '
        Me.txtBidComment.Location = New System.Drawing.Point(12, 24)
        Me.txtBidComment.Multiline = True
        Me.txtBidComment.Name = "txtBidComment"
        Me.txtBidComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBidComment.Size = New System.Drawing.Size(684, 60)
        Me.txtBidComment.TabIndex = 2
        '
        'dgBidComments
        '
        Me.dgBidComments.DataMember = ""
        Me.dgBidComments.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBidComments.Location = New System.Drawing.Point(10, 89)
        Me.dgBidComments.Name = "dgBidComments"
        Me.dgBidComments.Size = New System.Drawing.Size(692, 295)
        Me.dgBidComments.TabIndex = 1
        Me.dgBidComments.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.tsBidComments})
        Me.dgBidComments.TabStop = False
        '
        'tsBidComments
        '
        Me.tsBidComments.DataGrid = Me.dgBidComments
        Me.tsBidComments.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.tsBCEntryDate, Me.tsBCAuthor, Me.dgBCComment})
        Me.tsBidComments.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.tsBidComments.MappingName = "Comments"
        Me.tsBidComments.ReadOnly = True
        '
        'tsBCEntryDate
        '
        Me.tsBCEntryDate.Format = "d"
        Me.tsBCEntryDate.FormatInfo = Nothing
        Me.tsBCEntryDate.HeaderText = "Entered"
        Me.tsBCEntryDate.MappingName = "EntryDate"
        Me.tsBCEntryDate.ReadOnly = True
        Me.tsBCEntryDate.Width = 75
        '
        'tsBCAuthor
        '
        Me.tsBCAuthor.Format = ""
        Me.tsBCAuthor.FormatInfo = Nothing
        Me.tsBCAuthor.HeaderText = "BY"
        Me.tsBCAuthor.MappingName = "Author"
        Me.tsBCAuthor.Width = 25
        '
        'dgBCComment
        '
        Me.dgBCComment.Format = ""
        Me.dgBCComment.FormatInfo = Nothing
        Me.dgBCComment.HeaderText = "Comment"
        Me.dgBCComment.MappingName = "Comment"
        Me.dgBCComment.ReadOnly = True
        Me.dgBCComment.Width = 550
        '
        'cboJob
        '
        Me.cboJob.Location = New System.Drawing.Point(24, 20)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(328, 21)
        Me.cboJob.TabIndex = 2
        Me.cboJob.Text = "Select Project"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuReports})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuShipDates})
        Me.mnuFile.Text = "File"
        Me.mnuFile.Visible = False
        '
        'mnuShipDates
        '
        Me.mnuShipDates.Index = 0
        Me.mnuShipDates.Text = "Update Ship Dates"
        '
        'mnuReports
        '
        Me.mnuReports.Index = 1
        Me.mnuReports.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnurptSummary, Me.mnuEditAuxTitle})
        Me.mnuReports.Text = "Reports"
        Me.mnuReports.Visible = False
        '
        'mnurptSummary
        '
        Me.mnurptSummary.Index = 0
        Me.mnurptSummary.Text = "Project Summary"
        '
        'mnuEditAuxTitle
        '
        Me.mnuEditAuxTitle.Index = 1
        Me.mnuEditAuxTitle.Text = "Edt Aux Label"
        '
        'frmJob
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(764, 498)
        Me.Controls.Add(Me.tabJob)
        Me.Controls.Add(Me.cboJob)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "frmJob"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Project Details"
        Me.tabJob.ResumeLayout(False)
        Me.tpgJob.ResumeLayout(False)
        Me.tpgJob.PerformLayout()
        Me.tpgDetails.ResumeLayout(False)
        CType(Me.dgLineItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgUnits, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgBldg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgBuilding.ResumeLayout(False)
        CType(Me.dgBldgSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgShipping.ResumeLayout(False)
        CType(Me.dg_ShipDates, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgSummary.ResumeLayout(False)
        Me.tpgSummary.PerformLayout()
        CType(Me.dgShipSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgBulkEdits.ResumeLayout(False)
        Me.tpgBulkEdits.PerformLayout()
        Me.tpgComments.ResumeLayout(False)
        Me.tpgComments.PerformLayout()
        CType(Me.dgComments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgChangeOrders.ResumeLayout(False)
        Me.tpgChangeOrders.PerformLayout()
        CType(Me.dgChangeOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgBidComments.ResumeLayout(False)
        Me.tpgBidComments.PerformLayout()
        CType(Me.dgBidComments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadJobInfo()
        cnn.ConnectionString = frmMain.strConnect
        Dim strBidInfo As String
        strBidInfo = "SELECT ProjectID, LDID, SourceID, ClientID, ContractDate, ProjectDate, ContractAmount, Description, Location, Address, City, State, Zip, ProjectContact, ClientPM, ClientMobilePhone, LDPM, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, Closed FROM Project WHERE ProjectID = " + _
        cboJob.SelectedValue.ToString
        Dim cmdClientID As New SqlCommand(strBidInfo, cnn)
        cmdClientID.CommandType = CommandType.Text
        Dim drBid As SqlDataReader = Nothing
        Dim JobNum As String = Nothing
        Try
            cnn.Open()
            drBid = cmdClientID.ExecuteReader
            If drBid.Read() Then
                intClientID = System.Convert.ToInt32(drBid.Item("ClientID"))
                If Not IsDBNull(drBid.Item("ContractDate")) Then
                    txtContractDate.Text = Format(drBid.Item("ContractDate"), "d")
                End If
                If Not IsDBNull(drBid.Item("ProjectDate")) Then
                    txtJobDate.Text = Format(drBid.Item("ProjectDate"), "d")
                End If
                If Not IsDBNull(drBid.Item("ContractAmount")) Then
                    txtContractAmount.Text = Format(drBid.Item("ContractAmount"), "c")
                End If
                If Not IsDBNull(drBid.Item("Description")) Then
                    txtbDescription.Text = drBid.Item("Description").ToString
                End If
                If Not IsDBNull(drBid.Item("Location")) Then
                    txtBLocation.Text = drBid.Item("Location").ToString
                End If
                If Not IsDBNull(drBid.Item("Address")) Then
                    txtBAddress.Text = drBid.Item("Address").ToString
                End If
                If Not IsDBNull(drBid.Item("City")) Then
                    txtBCity.Text = drBid.Item("City").ToString
                End If
                If Not IsDBNull(drBid.Item("State")) Then
                    txtBState.Text = drBid.Item("State").ToString
                End If
                If Not IsDBNull(drBid.Item("Zip")) Then
                    txtBZip.Text = drBid.Item("Zip").ToString
                End If
                If Not IsDBNull(drBid.Item("ProjectContact")) Then
                    txtBBidContact.Text = drBid.Item("ProjectContact").ToString
                End If
                If Not IsDBNull(drBid.Item("ClientPM")) Then
                    txtClientPM.Text = drBid.Item("ClientPM").ToString
                End If
                If Not IsDBNull(drBid.Item("ClientMobilePhone")) Then
                    txtClientMobilePhone.Text = drBid.Item("ClientMobilePhone").ToString
                End If
                If Not IsDBNull(drBid.Item("LDPM")) Then
                    txtLDPM.Text = drBid.Item("LDPM").ToString
                End If
                If Not IsDBNull(drBid.Item("Notes")) Then
                    txtBNotes.Text = drBid.Item("Notes").ToString
                End If
                If Not IsDBNull(drBid.Item("LDEstimator")) Then
                    txtLDEstimator.Text = drBid.Item("LDEstimator").ToString
                End If
                If Not IsDBNull(drBid.Item("BluePrintDate")) Then
                    txtBluePrintDate.Text = drBid.Item("BluePrintDate").ToString
                End If
                If Not IsDBNull(drBid.Item("RevDate")) Then
                    txtRevDate.Text = Format(drBid.Item("RevDate"), "d")
                End If
                If Not IsDBNull(drBid.Item("SalesTaxRate")) Then
                    txtSalesTaxRate.Text = CStr(drBid.Item("SalesTaxRate"))
                End If
                If Not IsDBNull(drBid.Item("Closed")) Then
                    cbxClosed.Checked = System.Convert.ToBoolean(drBid.Item("Closed"))
                End If
                JobNum = "LD-" & Format(drBid.Item("ContractDate"), "yy")
                JobNum = JobNum & "-" & drBid.Item("LDID").ToString
                If Not IsDBNull(drBid.Item("SourceID")) Then
                    intOrigBidID = System.Convert.ToInt32(drBid.Item("SourceID"))
                Else
                    'TODO Handle Parameterized SQL and value returns in a seperate module
                    intOrigBidID = -1
                End If

                LoadClientDetails()
                Me.Text = "Lights Direct Job  -----    " & JobNum
            End If
        Catch ex As Exception
            MsgBox("Error Loading Job Info: " + ex.ToString)
        Finally
            drBid.Close()
            cnn.Close()
        End Try

    End Sub

    Private Sub LoadJobs()
        Dim ProjType As String
        If frmMain.comingFromVerbal = True Then
            ProjType = "verbal"
        Else
            ProjType = "job"
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim strBids As String
        strBids = "SELECT ProjectID,'LD-'+ RTRIM(CAST(LDID AS char))+ ': '+ISNULL(Description, '')+' '+ISNull(Location, " + _
            "'') AS Proj FROM Project Where ProjectType = '" + ProjType + "' order by LDID"
        Dim daBids As New SqlDataAdapter(strBids, cnn)
        Dim dsBids As New DataSet
        daBids.Fill(dsBids, "Project")
        cboJob.DisplayMember = "Proj"
        cboJob.ValueMember = "ProjectID"
        cboJob.DataSource = dsBids.Tables("Project")
    End Sub

    Private Sub LoadBldgs()

        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadBldgs As New SqlCommand("Bldgs", cnn)
        cmdLoadBldgs.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdLoadBldgs.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim daBldgs As New SqlDataAdapter(cmdLoadBldgs)
        Dim dsBldgs As New DataSet
        Try
            daBldgs.Fill(dsBldgs, "Bldgs")
            'Populate First Row of CBO with Select Item prompt
            Dim dtBldgs As DataTable = dsBldgs.Tables("Building")
        Catch ex As Exception
            MsgBox("Error Loading Buildings: " + ex.ToString)
        End Try

        cboBldg.DataSource = dsBldgs.Tables("Bldgs")
        cboBldg.DisplayMember = "BLDG"
        cboBldg.ValueMember = "BldgID"
    End Sub

    Private Sub frmJob_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadJobs()
    End Sub

    Sub LoadClientDetails()
        Dim cnnclient As New SqlConnection
        cnnclient.ConnectionString = frmMain.strConnect
        Dim strClientID As String
        strClientID = intClientID.ToString
        Dim spClientDetails As String = "ClientDetails"
        Dim cmdClientDetails As New SqlCommand(spClientDetails, cnnclient)
        cmdClientDetails.CommandType = CommandType.StoredProcedure
        cmdClientDetails.Parameters.AddWithValue("@ClientID", intClientID)
        Dim drClientDetails As SqlDataReader = Nothing
        If cnnclient.State = ConnectionState.Closed Then
            cnnclient.Open()
        End If

        Try
            drClientDetails = cmdClientDetails.ExecuteReader
            drClientDetails.Read()
            txtCLName.Text = drClientDetails.Item("Name").ToString
            txtClAddr1.Text = drClientDetails.Item("Address1").ToString
            txtCLAddr2.Text = drClientDetails.Item("Address2").ToString
            txtCLCity.Text = drClientDetails.Item("City").ToString
            txtCLState.Text = drClientDetails.Item("State").ToString
            txtCLZip.Text = drClientDetails.Item("Zip").ToString
            txtCLPhone1.Text = drClientDetails.Item("Phone1").ToString
            txtCLPhone2.Text = drClientDetails.Item("Phone2").ToString
            txtCLFAX.Text = drClientDetails.Item("FAX").ToString
            txtCLemail.Text = drClientDetails.Item("email").ToString
            txtCLContact.Text = drClientDetails.Item("contact").ToString

        Catch ex As Exception
            MsgBox("Error Loading Client Details")
        Finally
            drClientDetails.Close()
            cnnclient.Close()
        End Try
    End Sub
    Private Sub LoadBldgDG()

        cnn.ConnectionString = frmMain.strConnect
        Dim strBldg As String
        strBldg = "SELECT BldgID, BldgName, BldgNumber, Type, Description FROM Building WHERE ProjectID = " & cboJob.SelectedValue.ToString
        Dim cmdBLDG As New SqlCommand(strBldg, cnn)
        cmdBLDG.CommandType = CommandType.Text
        Dim daBLDGs As New SqlDataAdapter(cmdBLDG)
        Dim dsBLDGs As New DataSet
        Try
            daBLDGs.Fill(dsBLDGs, "Bldgs")
        Catch ex As Exception
            MsgBox("Error Loading Unit Line Item Datagrid")
        End Try
        dgBldg.DataSource = dsBLDGs.Tables("Bldgs")
        intBldgDGRowCount = dsBLDGs.Tables("Bldgs").Rows.Count


    End Sub

    Private Sub LoadUnitsDG()
        cnn.ConnectionString = frmMain.strConnect
        Dim strUnit As String
        strUnit = "SELECT UnitID, UnitName, UnitType, Description FROM Unit WHERE BldgID = " & dgBldg.Item(dgBldg.CurrentRowIndex, 0).ToString _
            + " and ProjectID = " + frmMain.intJobID.ToString
        Dim cmdUnit As New SqlCommand(strUnit, cnn)
        cmdUnit.CommandType = CommandType.Text
        Dim daUnits As New SqlDataAdapter(cmdUnit)
        Dim dsUnits As New DataSet
        Try
            daUnits.Fill(dsUnits, "Units")
        Catch ex As Exception
            MsgBox("Error Loading Unit Line Item Datagrid")
        End Try
        dgUnits.DataSource = dsUnits.Tables("Units")
    End Sub

    Private Sub LoadLIDG()
        If (dgUnits.CurrentRowIndex) = -1 Then
            dgLineItems.DataSource = Nothing
            Exit Sub
        End If
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim strLineItem As String
        strLineItem = "SELECT ItemID, PartNumber, Description, RT, Location, Type,Quantity, Cost, Margin, Sell, ShipDate, ShipQuan, QuanBO FROM LineItem WHERE UnitID = " & dgUnits.Item(dgUnits.CurrentRowIndex, 0).ToString
        Dim cmdLineItem As New SqlCommand(strLineItem, cnn)
        cmdLineItem.CommandType = CommandType.Text
        Dim daLineItems As New SqlDataAdapter(cmdLineItem)
        Dim dsLineItems As New DataSet
        Try
            daLineItems.Fill(dsLineItems, "LineItems")
        Catch ex As Exception
            MsgBox("Error Loading LineItem Line Item Datagrid")
        End Try
        dgLineItems.DataSource = dsLineItems.Tables("LineItems")
        dgLineItems.CurrentRowIndex = intDGLineItemsActiveRow
        intUnitDGRowCount = dsLineItems.Tables("LineItems").Rows.Count
    End Sub

    Private Sub LoadShipDatesDG()
        cnn.ConnectionString = frmMain.strConnect
        Dim strShipdates As String
        strShipdates = "SELECT BldgID, BldgNUmber, BldgName,ShipSequence, ShipDateR, ShipDate FROM Building WHERE ProjectID = " & frmMain.intJobID.ToString
        strShipdates = strShipdates & " Order By bldgNumber"
        Dim cmdShipdates As New SqlCommand(strShipdates, cnn)
        cmdShipdates.CommandType = CommandType.Text
        Dim daShipdates As New SqlDataAdapter(cmdShipdates)
        Dim dsShipdates As New DataSet
        Try
            daShipdates.Fill(dsShipdates, "Shipdates")
        Catch ex As Exception
            MsgBox("Error Loading Shipdates Line Item Datagrid")
        End Try
        dg_ShipDates.DataSource = dsShipdates.Tables("Shipdates")
        intShippingDGRowCount = dsShipdates.Tables("ShipDates").Rows.Count
    End Sub

    Private Sub LoaddgSummary()

        cnn.ConnectionString = frmMain.strConnect
        Dim strSummary As String
        strSummary = "SELECT Partnumber, SUM(Quantity) AS Quan , Cost, Margin, Sell, SUM(Quantity)* Sell AS Total, SUM(shipquan) AS Shipped, Sum(QuanBO) AS BO  From LineItem Where ProjectID = " & _
        cboJob.SelectedValue.ToString & " Group By Partnumber, Cost, Margin, Sell"
        Dim cmdSummary As New SqlCommand(strSummary, cnn)
        cmdSummary.CommandType = CommandType.Text
        Dim daSummary As New SqlDataAdapter(cmdSummary)
        Dim dsSummary As New DataSet
        Try
            daSummary.Fill(dsSummary, "Summary")
        Catch ex As Exception
            MsgBox("Error Loading Summary Line Item Datagrid")
        End Try
        dgSummary.DataSource = dsSummary.Tables("Summary")

        Dim strShipDates As String
        strShipDates = "ShipDates"
        Dim cmdShipDates As New SqlCommand
        cmdShipDates.Connection = cnn
        cmdShipDates.CommandText = strShipDates
        cmdShipDates.CommandType = CommandType.StoredProcedure
        Dim prmShipDatesJobID As SqlParameter = cmdShipDates.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmShipDatesJobID.Value = frmMain.intJobID
        Dim daShipDates As New SqlDataAdapter(cmdShipDates)
        Dim dsShipDates As New DataSet
        daShipDates.Fill(dsShipDates, "ShipDates")

        dgShipSummary.DataSource = dsShipDates.Tables("ShipDates")

        Dim cmdMargins As New SqlCommand("Margins", cnn)
        cmdMargins.CommandType = CommandType.StoredProcedure
        Dim prmJobID As SqlParameter = cmdMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID
        Dim drMargins As SqlDataReader
        cnn.Open()
        drMargins = cmdMargins.ExecuteReader

        While drMargins.Read
            If drMargins.GetString(0) = "U" Then
                txtUC.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtUS.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'txtMarUnitMargin.Text = CStr(FormatPercent(CDbl(1 - (txtMarUnitCost.Text) / CDbl(txtMarUnitSell.Text)), 2))
            ElseIf drMargins.GetString(0) = "C" Then
                txtCC.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtCS.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'txtMarCHMargin.Text = CStr(FormatPercent(CDbl(1 - (txtMarCHCost.Text) / CDbl(txtMarCHSell.Text)), 2))
            ElseIf drMargins.GetString(0) = "S" Then
                txtSC.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtSS.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'txtMarSiteMargin.Text = CStr(FormatPercent(CDbl(1 - (txtMarSiteCost.Text) / CDbl(txtMarSiteSell.Text)), 2))
            ElseIf drMargins.GetString(0) = "X" Then
                txtCOC.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtCOS.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'StrCommonMargin = CStr(FormatPercent(CDbl(1 - (strCommonCost) / CDbl(strCommon)), 2))
            ElseIf drMargins.GetString(0) = "A" Then
                txtAC.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                If IsNumeric(drMargins.GetDecimal(2)) Then
                    txtAS.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                Else
                    txtAS.Text = "0"
                End If

                'StrCommonMargin = CStr(FormatPercent(CDbl(1 - (strCommonCost) / CDbl(strCommon)), 2))
            End If



        End While
        Dim dblUS As Double
        Dim dblSS As Double
        Dim dblCS As Double
        Dim dblCOS As Double
        Dim dblAS As Double
        If IsNumeric(txtUS.Text) Then
            dblUS = CDbl(txtUS.Text)
        Else
            dblUS = 0
        End If

        If IsNumeric(txtSS.Text) Then
            dblSS = CDbl(txtSS.Text)
        Else
            dblSS = 0
        End If

        If IsNumeric(txtCS.Text) Then
            dblCS = CDbl(txtCS.Text)
        Else
            dblCS = 0
        End If

        If IsNumeric(txtCOS.Text) Then
            dblCOS = CDbl(txtCOS.Text)
        Else
            dblCOS = 0
        End If

        If IsNumeric(txtAS.Text) Then
            dblAS = CDbl(txtAS.Text)
        Else
            dblAS = 0
        End If

        txtTotSell.Text = Format(dblUS + dblSS + dblCS + dblCOS + dblAS, "C")

        Dim dblUC As Double
        Dim dblSC As Double
        Dim dblCC As Double
        Dim dblCOC As Double
        Dim dblAC As Double
        If IsNumeric(txtUC.Text) Then
            dblUC = CDbl(txtUC.Text)
        Else
            dblUC = 0
        End If

        If IsNumeric(txtSC.Text) Then
            dblSC = CDbl(txtSC.Text)
        Else
            dblSC = 0
        End If

        If IsNumeric(txtCC.Text) Then
            dblCC = CDbl(txtCC.Text)
        Else
            dblCC = 0
        End If

        If IsNumeric(txtCOC.Text) Then
            dblCOC = CDbl(txtCOC.Text)
        Else
            dblCOC = 0
        End If

        If IsNumeric(txtAC.Text) Then
            dblAC = CDbl(txtAC.Text)
        Else
            dblAC = 0
        End If

        txtTotCost.Text = Format(dblUC + dblSC + dblCC + dblCOC + dblAC, "C")
        drMargins.Close()
        cnn.Close()


    End Sub

    Private Sub dgUnits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgUnits.Click
        LoadLIDG()
    End Sub

    Private Sub dgBldg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgBldg.Click
        LoadUnitsDG()
        LoadLIDG()
    End Sub

    Private Sub LoadComments()
        Dim strSQL As String
        strSQL = "SELECT CommentID, EntryDate, Author, Comment FROM Comment WHERE ProjectId = " & _
        frmMain.intJobID
        Dim cmdComments As New SqlCommand(strSQL, cnn)
        cmdComments.CommandType = CommandType.Text
        Dim daComments As New SqlDataAdapter(cmdComments)
        Dim dsComments As New DataSet
        Try
            daComments.Fill(dsComments, "Comments")
        Catch ex As Exception
            MsgBox("Error Loading Bldg Units DataGrid")
        End Try

        dgComments.DataSource = dsComments.Tables("Comments")
    End Sub

    Private Sub LoadChangeOrderDetails()
        txtCODate.Text = Format(dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 1), "d")
        txtCOEnteredBy.Text = dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 2).ToString
        txtCOApprovedBy.Text = dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 3).ToString
        txtCODesc.Text = dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 4).ToString
        txtCOMisc.Text = dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 5).ToString
        txtCOPriorBalance.Text = Format(dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 6), "C")
        txtCONewBalance.Text = Format(dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 7), "C")
        btnCOSave.Text = "Update"
        btnCOCancel.Visible = True
        btnCODelete.Visible = True

    End Sub

    Private Sub ClearChangeOrderDetails()
        txtCODate.Text = Format(Now(), "d")
        txtCOEnteredBy.Text = ""
        txtCOApprovedBy.Text = ""
        txtCODesc.Text = ""
        txtCOMisc.Text = ""
        txtCOPriorBalance.Text = ""
        txtCONewBalance.Text = ""
        btnCOSave.Text = "Save"
        btnCOCancel.Visible = False
        btnCODelete.Visible = False
    End Sub

    Private Sub btnCommentEDit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommentEDit.Click
        btnSaveComment.Text = "Update Comment"
        txtCComment.Text = dgComments.Item(dgComments.CurrentRowIndex, 3).ToString

    End Sub

    Private Sub btnSaveComment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveComment.Click
        If btnSaveComment.Text = "Save Comment" Then
            If txtCAuthor.Text = "" Then
                MsgBox("You must enter the initials of the Author of this comment!!")
                Exit Sub
            End If
            Dim AddJobComment As New SqlCommand("AddComment", cnn)
            AddJobComment.CommandType = CommandType.StoredProcedure

            Dim prmJobID As SqlParameter = AddJobComment.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmJobID.Value = frmMain.intJobID

            Dim prmEntryDate As SqlParameter = AddJobComment.Parameters.Add("@EntryDate", SqlDbType.DateTime)
            prmEntryDate.Value = Now()

            Dim prmAuthor As SqlParameter = AddJobComment.Parameters.Add("@Author", SqlDbType.NVarChar, 30)
            prmAuthor.Value = txtCAuthor.Text

            Dim prmComment As SqlParameter = AddJobComment.Parameters.Add("@Comment", SqlDbType.NVarChar, 1000)
            prmComment.Value = txtCComment.Text


            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                AddJobComment.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                MsgBox("There was an error adding this record: " + ex.ToString)
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
            End Try

        ElseIf btnSaveComment.Text = "Update Comment" Then
            Dim strEditSql As String
            strEditSql = "Update Comment Set Comment = '" & txtCComment.Text & _
            "' WHERE CommentID = " & dgComments.Item(dgComments.CurrentRowIndex, 0).ToString
            Dim EditJobComment As New SqlCommand(strEditSql.ToString, cnn)
            EditJobComment.CommandType = CommandType.Text


            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                EditJobComment.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                MsgBox("There was an error editing this record!!")
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
                btnSaveComment.Text = "Save Comment"
                btnCommentEDit.Visible = False
                btnCommentDelete.Visible = False
            End Try
        End If

        txtCAuthor.Text = ""
        txtCComment.Text = ""
        LoadComments()
    End Sub

    Private Sub btnCommentDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommentDelete.Click
        Dim strDeleteSql As String
        strDeleteSql = "DELETE FROM Comment " & _
        "WHERE CommentID = " & dgComments.Item(dgComments.CurrentRowIndex, 0).ToString
        Dim DeleteJobComment As New SqlCommand(strDeleteSql.ToString, cnn)
        DeleteJobComment.CommandType = CommandType.Text


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            DeleteJobComment.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error editing this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If

        End Try

        btnCommentEDit.Visible = False
        btnCommentDelete.Visible = False
        btnSaveComment.Text = "Save Comment"
        txtCAuthor.Text = ""
        txtCComment.Text = ""
        LoadComments()
    End Sub

    Private Sub dgComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgComments.Click
        btnCommentEDit.Visible = True
        btnCommentDelete.Visible = True
    End Sub

    Private Sub btnEditJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditJob.Click
        If btnEditJob.Text = "Save" Then
            EditJob()
            btnEditJob.Text = "Edit"
        Else
            btnEditJob.Text = "Save"
        End If
    End Sub

    Private Sub EditJob()
        If txtbDescription.Text = "" Then
            MsgBox("You Must Enter A Bid Description!")
            txtbDescription.Focus()
            Exit Sub
        End If

        If txtBLocation.Text = "" Then
            MsgBox("You Must Enter A Bid Location!")
            txtBLocation.Focus()
            Exit Sub
        End If

        If Not IsDate(txtBluePrintDate.Text) Then
            MsgBox("You must enter a valid Blue Print Date")
            Exit Sub
        End If

        If Not IsDate(txtJobDate.Text) Then
            MsgBox("You must enter a valid Job Date")
            Exit Sub
        End If

        If Not IsNumeric(txtContractAmount.Text) Then
            MsgBox("You must enter a valid ContractAmount")
            Exit Sub
        End If

        Dim EditBid As New SqlCommand("EditProject", cnn)
        EditBid.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = EditBid.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim prmContractDate As SqlParameter = EditBid.Parameters.Add("@ContractDate", SqlDbType.DateTime)
        prmContractDate.Value = txtContractDate.Text

        Dim prmJobDate As SqlParameter = EditBid.Parameters.Add("@JobDate", SqlDbType.DateTime)
        prmJobDate.Value = txtJobDate.Text

        Dim prmContractAmount As SqlParameter = EditBid.Parameters.Add("@ContractAmount", SqlDbType.Money)
        prmContractAmount.Value = CDbl(txtContractAmount.Text)

        Dim prmDescription As SqlParameter = EditBid.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbDescription.Text

        Dim prmLocation As SqlParameter = EditBid.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtBLocation.Text

        Dim prmAddress As SqlParameter = EditBid.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtBAddress.Text

        Dim prmCity As SqlParameter = EditBid.Parameters.Add("@City", SqlDbType.NVarChar, 50)
        prmCity.Value = txtBCity.Text

        Dim prmState As SqlParameter = EditBid.Parameters.Add("@State", SqlDbType.Char, 2)
        prmState.Value = txtBState.Text

        Dim prmZip As SqlParameter = EditBid.Parameters.Add("@Zip", SqlDbType.Char, 10)
        prmZip.Value = txtBZip.Text

        Dim prmBidContact As SqlParameter = EditBid.Parameters.Add("@BidContact", SqlDbType.NVarChar, 50)
        prmBidContact.Value = txtBBidContact.Text

        Dim prmClientPM As SqlParameter = EditBid.Parameters.Add("@ClientPM", SqlDbType.NVarChar, 50)
        prmClientPM.Value = txtClientPM.Text

        Dim prmClientMobilePhone As SqlParameter = EditBid.Parameters.Add("@ClientMobilePhone", SqlDbType.Char, 14)
        prmClientMobilePhone.Value = txtClientMobilePhone.Text

        Dim prmLDPM As SqlParameter = EditBid.Parameters.Add("@LDPM", SqlDbType.NVarChar, 50)
        prmLDPM.Value = txtLDPM.Text

        Dim prmNotes As SqlParameter = EditBid.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtBNotes.Text

        Dim prmLDEstimator As SqlParameter = EditBid.Parameters.Add("@LDEstimator", SqlDbType.NVarChar, 50)
        prmLDEstimator.Value = txtLDEstimator.Text

        Dim prmBluePrintDate As SqlParameter = EditBid.Parameters.Add("@BluePrintDate", SqlDbType.DateTime)
        prmBluePrintDate.Value = txtBluePrintDate.Text

        Dim prmRevDate As SqlParameter = EditBid.Parameters.Add("@RevDate", SqlDbType.DateTime)
        If Not IsDate(txtRevDate.Text) Then
            prmRevDate.Value = Now()
        Else
            prmRevDate.Value = txtRevDate.Text
        End If

        Dim prmSalesTaxRate As SqlParameter = EditBid.Parameters.Add("@SalesTaxRate", SqlDbType.Float)
        If IsNumeric(txtSalesTaxRate.Text) Then
            prmSalesTaxRate.Value = CDbl(txtSalesTaxRate.Text)
        Else
            prmSalesTaxRate.Value = 0
        End If

        Dim prmClosed As SqlParameter = EditBid.Parameters.Add("@Closed", SqlDbType.Bit)
        If cbxClosed.Checked = True Then
            prmClosed.Value = 1
        Else
            prmClosed.Value = 0
        End If


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            EditBid.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try


    End Sub

    Private Sub SaveChangeOrder()
        If btnCOSave.Text = "Add" Then
            If txtCOEnteredBy.Text = "" Then
                MsgBox("You must enter the initials of the Author of this comment!!")
                Exit Sub
            End If
            Dim AddChangeOrder As New SqlCommand("AddChangeOrder", cnn)
            AddChangeOrder.CommandType = CommandType.StoredProcedure

            Dim prmJobID As SqlParameter = AddChangeOrder.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmJobID.Value = frmMain.intJobID

            Dim prmCODate As SqlParameter = AddChangeOrder.Parameters.Add("@CODate", SqlDbType.DateTime)
            prmCODate.Value = Now()

            Dim prmEnteredBy As SqlParameter = AddChangeOrder.Parameters.Add("@EnteredBy", SqlDbType.NVarChar, 10)
            prmEnteredBy.Value = txtCOEnteredBy.Text

            Dim prmApprovedBy As SqlParameter = AddChangeOrder.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 50)
            prmApprovedBy.Value = txtCOApprovedBy.Text

            Dim prmCODesc As SqlParameter = AddChangeOrder.Parameters.Add("@CODesc", SqlDbType.NVarChar, 1000)
            prmCODesc.Value = txtCODesc.Text

            Dim prmMIsc As SqlParameter = AddChangeOrder.Parameters.Add("@Misc", SqlDbType.NVarChar, 1000)
            prmMIsc.Value = txtCOMisc.Text

            Dim prmPriorBalance As SqlParameter = AddChangeOrder.Parameters.Add("@PriorBalance", SqlDbType.Money)
            If IsNumeric(txtCOPriorBalance.Text) Then
                prmPriorBalance.Value = CDbl(txtCOPriorBalance.Text)
            Else
                MsgBox("You Must Enter a Numeric Value for the Prior Balance")
                Exit Sub
            End If

            Dim prmNewBalance As SqlParameter = AddChangeOrder.Parameters.Add("@NewBalance", SqlDbType.Money)
            If IsNumeric(txtCONewBalance.Text) Then
                prmNewBalance.Value = CDbl(txtCONewBalance.Text)
            Else
                MsgBox("You Must Enter a Numeric Value for the New Balance")
                Exit Sub
            End If

            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                AddChangeOrder.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                MsgBox("There was an error adding this record: " + ex.ToString)
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
            End Try

        ElseIf btnCOSave.Text = "Update" Then
            Dim EditChangeOrder As New SqlCommand("EditChangeOrder", cnn)
            EditChangeOrder.CommandType = CommandType.StoredProcedure

            Dim prmCOID As SqlParameter = EditChangeOrder.Parameters.Add("@COID", SqlDbType.Int)
            prmCOID.Value = dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 0)

            Dim prmEnteredBy As SqlParameter = EditChangeOrder.Parameters.Add("@EnteredBy", SqlDbType.NVarChar, 10)
            prmEnteredBy.Value = txtCOEnteredBy.Text

            Dim prmApprovedBy As SqlParameter = EditChangeOrder.Parameters.Add("@ApprovedBy", SqlDbType.NVarChar, 50)
            prmApprovedBy.Value = txtCOApprovedBy.Text

            Dim prmCODesc As SqlParameter = EditChangeOrder.Parameters.Add("@CODesc", SqlDbType.NVarChar, 1000)
            prmCODesc.Value = txtCODesc.Text

            Dim prmMIsc As SqlParameter = EditChangeOrder.Parameters.Add("@Misc", SqlDbType.NVarChar, 1000)
            prmMIsc.Value = txtCOMisc.Text

            Dim prmPriorBalance As SqlParameter = EditChangeOrder.Parameters.Add("@PriorBalance", SqlDbType.Money)
            If IsNumeric(txtCOPriorBalance.Text) Then
                prmPriorBalance.Value = CDbl(txtCOPriorBalance.Text)
            Else
                MsgBox("You Must Enter a Numeric Value for the Prior Balance")
                Exit Sub
            End If

            Dim prmNewBalance As SqlParameter = EditChangeOrder.Parameters.Add("@NewBalance", SqlDbType.Money)
            If IsNumeric(txtCONewBalance.Text) Then
                prmNewBalance.Value = CDbl(txtCONewBalance.Text)
            Else
                MsgBox("You Must Enter a Numeric Value for the New Balance")
                Exit Sub
            End If

            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                EditChangeOrder.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                MsgBox("There was an error editing this record!!")
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
            End Try
        End If
        ClearChangeOrderDetails()
        LoadChangeOrders()

    End Sub

    Private Sub DeleteChangeOrder()
        Dim strSql As String
        strSql = "DELETE ChangeOrder WHERE COID = " & dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 0).ToString
        Dim DeleteChangeOrder As New SqlCommand(strSql, cnn)

        DeleteChangeOrder.CommandType = CommandType.Text
        'Dim prmCOID As SqlParameter = EditChangeOrder.Parameters.Add("@COID", SqlDbType.Int)
        'prmCOID.Value = dgChangeOrders.Item(dgChangeOrders.CurrentRowIndex, 0)



        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            DeleteChangeOrder.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub btnEditBldg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBldg.Click
        intBldgID = System.Convert.ToInt32(dgBldg.Item(dgBldg.CurrentRowIndex, 0))
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim EditBldg As New frmj_bldg
        EditBldg.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnEditUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditUnit.Click
        intUnitID = System.Convert.ToInt32(dgUnits.Item(dgUnits.CurrentRowIndex, 0))
        intBldgID = System.Convert.ToInt32(dgBldg.Item(dgBldg.CurrentRowIndex, 0))
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim EditBldg As New frmj_units
        EditBldg.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnEditLI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditLI.Click
        intItemID = System.Convert.ToInt32(dgLineItems.Item(dgLineItems.CurrentRowIndex, 0))
        intBldgID = System.Convert.ToInt32(dgBldg.Item(dgBldg.CurrentRowIndex, 0))
        intUnitID = System.Convert.ToInt32(dgUnits.Item(dgUnits.CurrentRowIndex, 0))
        intDGLineItemsActiveRow = dgLineItems.CurrentRowIndex
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim EditLI As New frmj_LI
        EditLI.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub LoadBEJobItemcbo()
        'Bulk Edit
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadJobItems As New SqlCommand("CatItems", cnn)
        cmdLoadJobItems.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdLoadJobItems.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim daBECatalogMain As New SqlDataAdapter(cmdLoadJobItems)

        Dim dsBECatalogMain As New DataSet

        Try
            daBECatalogMain.Fill(dsBECatalogMain, "Catalog")
            'Populate First Row of CBO with Select Item prompt
            Dim dtCatalog As DataTable = dsBECatalogMain.Tables("Catalog")
            Dim dr As DataRow = dtCatalog.NewRow
            dr("Item") = " - Please Select An Item"
            dr("ProductID") = "---"
            dtCatalog.Rows.InsertAt(dr, 0)
        Catch ex As Exception
            MsgBox("Error Loading BEJobItemCBO")
        End Try

        cboBEBidItems.DataSource = dsBECatalogMain.Tables("Catalog")
        cboBEBidItems.DisplayMember = "Item"
        cboBEBidItems.ValueMember = "ProductID"

    End Sub

    Private Sub LoadBELIbyPart()
        'Bulk Edit
        'Replace All Items in Units By Part Number
        If cboBEBidItems.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboBEBidItems.SelectedValue.ToString

        Dim spCatMainLineItem As String = "BELIbyPart"
        Dim cmdBEMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdBEMainLineItem.CommandType = CommandType.StoredProcedure
        cmdBEMainLineItem.Parameters.AddWithValue("@ProjectID", frmMain.intJobID)
        cmdBEMainLineItem.Parameters.AddWithValue("@PartNumber", cboBEBidItems.SelectedValue.ToString)
        Dim drBEMainLineItem As SqlDataReader = Nothing
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            drBEMainLineItem = cmdBEMainLineItem.ExecuteReader
            drBEMainLineItem.Read()
            txtBEBProdID.Text = drBEMainLineItem.Item("PartNumber").ToString
            txtBEBDesc.Text = drBEMainLineItem.Item("Description").ToString
            txtBEBRT.Text = drBEMainLineItem.Item("RT").ToString
            txtBEBCost.Text = CDbl(drBEMainLineItem.Item("Cost").ToString).ToString("C")
            txtBEBMargin.Text = drBEMainLineItem.Item("Margin").ToString
            txtBEBSell.Text = CDbl(drBEMainLineItem.Item("Sell").ToString).ToString("C")
        Catch ex As Exception
            MsgBox("Error in LoadBELIByPart")
        Finally
            drBEMainLineItem.Close()
            cnn.Close()
        End Try

        lblBEBType.Visible = False
        txtBEBType.Visible = False
        cboBEType.SelectedIndex = 0

    End Sub

    Private Sub LoadBECatalogCBO()
        'Bulk Edits
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String = Nothing
        If rbBEMain.Checked = True Then
            strCatalogMain = "CatMainItems"
        ElseIf rbBESO.Checked = True Then
            strCatalogMain = "CatSOItem"
            'ElseIf rbBid.Checked = True Then
            '    LoadBidItemcbo()
            '    Exit Sub
        End If

        Dim daBECatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsBECatalogMain As New DataSet
        Try
            daBECatalogMain.Fill(dsBECatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading BECatalog")
        End Try

        cboBECatalog.DataSource = dsBECatalogMain.Tables("Catalog")
        cboBECatalog.DisplayMember = "Item"
        cboBECatalog.ValueMember = "ProductID"
    End Sub

    Private Sub rbBEMain_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbBEMain.CheckedChanged
        LoadBECatalogCBO()
    End Sub

    Private Sub rbBESO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBESO.CheckedChanged
        LoadBECatalogCBO()
    End Sub
    Private Sub LoadCatBERLineItem()
        'Bulk Edit
        If cboBECatalog.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboBECatalog.SelectedValue.ToString

        Dim spCatMainLineItem As String = Nothing
        If rbBEMain.Checked = True Then
            spCatMainLineItem = "CatMainLineItem"
        ElseIf rbBESO.Checked = True Then
            spCatMainLineItem = "CatSOLineItem"
        End If

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboBECatalog.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            txtBERProdID.Text = drCatMainLineItem.Item("ProductID").ToString
            txtBERDesc.Text = drCatMainLineItem.Item("Description").ToString
            txtBERCost.Text = drCatMainLineItem.Item("Cost").ToString
        Catch ex As Exception
            MsgBox("Error In LoadCatBERLineItem")
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try
        'txtLIQuan.Text = 1
        txtBERMargin.Text = "30"
        txtBERSell.Text = Format((CDbl(txtBERCost.Text) / (1 - (CDbl(txtBERMargin.Text) / 100))), "$###,###.##")
        txtBERRT.Text = "T"
    End Sub

    Private Sub cboBECatalog_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBECatalog.SelectedIndexChanged
        LoadCatBERLineItem()
    End Sub
    Private Sub LoadBETypeCBO()
        'Bulk Edit
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadBidType As New SqlCommand("LineItemTypes", cnn)
        cmdLoadBidType.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdLoadBidType.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID
        Dim daBEBidType As New SqlDataAdapter(cmdLoadBidType)

        Dim dsBEBidType As New DataSet

        Try
            daBEBidType.Fill(dsBEBidType, "Type")
            Dim dtType As DataTable = dsBEBidType.Tables("Type")
            Dim dr As DataRow = dtType.NewRow
            dr("Item") = " - Please Select An Item"
            dr("Type") = "---"
            dtType.Rows.InsertAt(dr, 0)
        Catch ex As Exception
            MsgBox("Error Loading BETypeCBO")
        End Try

        cboBEType.DataSource = dsBEBidType.Tables("Type")
        cboBEType.DisplayMember = "Item"
        cboBEType.ValueMember = "Type"
        cboBECatalog.SelectedIndex = 0
    End Sub

    Private Sub LoadBELIbyType()
        'Bulk Edit
        'Replace All Items in Units By Type
        If cboBEType.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strType As String
        strType = cboBEType.SelectedValue.ToString

        Dim spCatMainLineItem As String = "BELIbyType"

        Dim cmdBEMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdBEMainLineItem.CommandType = CommandType.StoredProcedure
        cmdBEMainLineItem.Parameters.AddWithValue("@ProjectID", frmMain.intJobID)
        cmdBEMainLineItem.Parameters.AddWithValue("@Type", cboBEType.SelectedValue.ToString)
        Dim drBEMainLineItem As SqlDataReader = Nothing
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            drBEMainLineItem = cmdBEMainLineItem.ExecuteReader
            drBEMainLineItem.Read()
            txtBEBProdID.Text = drBEMainLineItem.Item("PartNumber").ToString
            txtBEBDesc.Text = drBEMainLineItem.Item("Description").ToString
            txtBEBRT.Text = drBEMainLineItem.Item("RT").ToString
            txtBEBType.Text = drBEMainLineItem.Item("Type").ToString
            txtBEBCost.Text = drBEMainLineItem.Item("Cost").ToString
            txtBEBMargin.Text = drBEMainLineItem.Item("Margin").ToString
            txtBEBSell.Text = drBEMainLineItem.Item("Sell").ToString
        Catch ex As Exception
            MsgBox("Error In LoadBELIByType")
        Finally
            drBEMainLineItem.Close()
            cnn.Close()
        End Try
        lblBEBType.Visible = True
        txtBEBType.Visible = True
        cboBEBidItems.SelectedIndex = 0
    End Sub

    Private Sub cboBEType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBEType.SelectedIndexChanged
        LoadBELIbyType()
    End Sub
    Private Sub btnBEDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBEDelete.Click
        'Bulk Edit
        Dim cmdBEDelete As New SqlCommand("DeleteLineItems", cnn)
        cmdBEDelete.CommandType = CommandType.StoredProcedure
        cmdBEDelete.Parameters.AddWithValue("@ProjectID", frmMain.intJobID)
        cmdBEDelete.Parameters.AddWithValue("@PartNumber", txtBEBProdID.Text.ToString)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            cmdBEDelete.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error In Deleting Records.")
        Finally
            cnn.Close()

        End Try

        LoadBEJobItemcbo()
        LoadBETypeCBO()
        LoadLIDG()
        LoaddgSummary()
        ClearBulkEditFields()

    End Sub

    Private Sub ClearBulkEditFields()
        'Bulk Edit
        txtBEBProdID.Text = ""
        txtBEBDesc.Text = ""
        txtBEBRT.Text = ""
        txtBEBType.Text = ""
        txtBEBCost.Text = ""
        txtBEBMargin.Text = ""
        txtBEBSell.Text = ""
        txtBERProdID.Text = ""
        txtBERDesc.Text = ""
        txtBERRT.Text = ""
        txtBERType.Text = ""
        txtBERCost.Text = ""
        txtBERMargin.Text = ""
        txtBERSell.Text = ""
        txtBERQuan.Text = ""
        txtBERLocation.Text = ""
        txtUnitWC.Text = ""
        cbxFSell.Checked = False
        cboBEBidItems.SelectedIndex = 0
        cboBEType.SelectedIndex = 0
        cboBECatalog.SelectedIndex = 0
    End Sub

    Private Sub btnBERByPartNum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBERByPartNum.Click
        Dim intJobID As Integer
        Dim strOPartNumber As String
        Dim strNPartNumber As String
        Dim strNDescription As String
        Dim dblNCost As Double
        Dim intNMargin As Integer
        Dim dblNSell As Double

        ' Initialize all basic variables
        cnn.ConnectionString = frmMain.strConnect

        intJobID = frmMain.intJobID
        strOPartNumber = txtBEBProdID.Text
        strNPartNumber = txtBERProdID.Text
        strNDescription = txtBERDesc.Text
        dblNCost = CDbl(txtBERCost.Text)
        intNMargin = CInt(txtBERMargin.Text)
        If IsNumeric(txtBERSell.Text) Then
            dblNSell = CDbl(txtBERSell.Text)
        Else
            dblNSell = 0
        End If


        ' Get the Original Part Number Item ID

        Dim cmdReplaceByPartNumber As New SqlCommand("BeByPartNumber", cnn)
        cmdReplaceByPartNumber.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        'MsgBox(CStr(cboUnit.SelectedIndex))
        Dim prmOldPartNumber As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@OldPartNumber", SqlDbType.Char, 25)
        prmOldPartNumber.Value = txtBEBProdID.Text

        Dim prmPartNumber As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
        prmPartNumber.Value = txtBERProdID.Text

        Dim prmDescription As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtBERDesc.Text

        Dim prmCost As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@Cost", SqlDbType.Money)
        If IsNumeric(txtBERCost.Text) Then
            prmCost.Value = CDbl(txtBERCost.Text)
        Else
            MsgBox("You must enter a Number for the Cost")
            Exit Sub
        End If


        Dim prmMargin As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@Margin", SqlDbType.Int)
        If IsNumeric(txtBERMargin.Text) Then
            prmMargin.Value = CInt(txtBERMargin.Text)
        Else
            MsgBox("You must enter a Number for the Margin")
            Exit Sub
        End If

        Dim prmSell As SqlParameter = cmdReplaceByPartNumber.Parameters.Add("@Sell", SqlDbType.Money)
        If IsNumeric(txtBERSell.Text) Then
            prmSell.Value = CDbl(txtBERSell.Text)
        Else
            MsgBox("You must enter a Number for the Selling Price")
            Exit Sub
        End If



        Try
            cnn.Open()
            cmdReplaceByPartNumber.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error in Replacing this Part Number!")
        Finally
            cnn.Close()
        End Try



        'Check for (And Get) New Part Number Item ID

        LoadBEJobItemcbo()
        LoadBETypeCBO()
        LoadLIDG()
        LoaddgSummary()
        ClearBulkEditFields()
    End Sub

    Private Sub btnBERByType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBERByType.Click
        Dim intJobID As Integer
        Dim strOPartNumber As String
        Dim strNPartNumber As String
        Dim strNDescription As String
        Dim dblNCost As Double
        Dim intNMargin As Integer
        Dim dblNSell As Double

        ' Initialize all basic variables
        cnn.ConnectionString = frmMain.strConnect

        ' Check for Dupe Parts of Same Type
        Dim strSQL As String
        'Populate Client TextBoxes From Stored Proecedure And DataReader
        strSQL = "SELECT Distinct PartNumber, Type From LineItem " & _
            "where Projectid = " & frmMain.intJobID & " AND Type = '" & txtBEBType.Text & "'"
        Dim cmdDupeType As New SqlCommand(strSQL, cnn)
        cmdDupeType.CommandType = CommandType.Text
        Dim daDupeType As New SqlDataAdapter(cmdDupeType)
        Dim dsDupeType As New DataSet
        daDupeType.Fill(dsDupeType, "Types")
        If dsDupeType.Tables(0).Rows.Count > 1 Then
            MsgBox("This Type refers to multiple Part Numbers in this bid. It cannot be used in this procedure!!")
            Exit Sub
        End If

        intJobID = frmMain.intJobID
        strOPartNumber = txtBEBProdID.Text
        strNPartNumber = txtBERProdID.Text
        strNDescription = txtBERDesc.Text
        dblNCost = CDbl(txtBERCost.Text)
        intNMargin = CInt(txtBERMargin.Text)
        If IsNumeric(txtBERSell.Text) Then
            dblNSell = CDbl(txtBERSell.Text)
        Else
            dblNSell = 0
        End If


        ' Get the Original Part Number Item ID

        Dim cmdReplaceByType As New SqlCommand("BeByType", cnn)
        cmdReplaceByType.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdReplaceByType.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim prmType As SqlParameter = cmdReplaceByType.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
        prmType.Value = cboBEType.SelectedValue

        'MsgBox(CStr(cboUnit.SelectedIndex))
        Dim prmPartNumber As SqlParameter = cmdReplaceByType.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
        prmPartNumber.Value = txtBERProdID.Text

        Dim prmDescription As SqlParameter = cmdReplaceByType.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtBERDesc.Text

        Dim prmCost As SqlParameter = cmdReplaceByType.Parameters.Add("@Cost", SqlDbType.Money)
        If IsNumeric(txtBERCost.Text) Then
            prmCost.Value = CDbl(txtBERCost.Text)
        Else
            MsgBox("You must enter a Number for the Cost")
            Exit Sub
        End If


        Dim prmMargin As SqlParameter = cmdReplaceByType.Parameters.Add("@Margin", SqlDbType.Int)
        If IsNumeric(txtBERMargin.Text) Then
            prmMargin.Value = CInt(txtBERMargin.Text)
        Else
            MsgBox("You must enter a Number for the Margin")
            Exit Sub
        End If

        Dim prmSell As SqlParameter = cmdReplaceByType.Parameters.Add("@Sell", SqlDbType.Money)
        If IsNumeric(txtBERSell.Text) Then
            prmSell.Value = CDbl(txtBERSell.Text)
        Else
            MsgBox("You must enter a Number for the Selling Price")
            Exit Sub
        End If



        Try
            cnn.Open()
            cmdReplaceByType.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error in Replacing this Part Number!")
        Finally
            cnn.Close()
        End Try



        'Check for (And Get) New Part Number Item ID

        LoadBEJobItemcbo()
        LoadBETypeCBO()
        LoadLIDG()
        LoaddgSummary()
        ClearBulkEditFields()
    End Sub
    Private Sub BECalcSell()
        'Bulk Edit
        If txtBEBSell.Text = "" Then
            Exit Sub
        End If
        If txtBEBCost.Text = "" Then
            Exit Sub
        End If
        If cbxFSell.Checked = True Then
            txtBEBMargin.Text = CInt((1 - (CDbl(txtBEBCost.Text) / CDbl(txtBEBSell.Text))) * 100).ToString
        Else
            txtBEBSell.Text = Format((CDbl(txtBEBCost.Text) / (1 - (CDbl(txtBEBMargin.Text) / 100))), "$###,###.##")
        End If
    End Sub

    Private Sub btnBEAdjustSell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBEAdjustSell.Click
        'Bulk Edit
        Dim cmdBEUpdatePrice As New SqlCommand("UpdatePrice", cnn)
        cmdBEUpdatePrice.CommandType = CommandType.StoredProcedure
        cmdBEUpdatePrice.Parameters.AddWithValue("@ProjectID", frmMain.intJobID)
        cmdBEUpdatePrice.Parameters.AddWithValue("@PartNumber", txtBEBProdID.Text.ToString)

        Dim prmRT As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@RT", SqlDbType.Char, 1)
        prmRT.Value = txtBEBRT.Text

        Dim prmCost As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Cost", SqlDbType.Money)
        If IsNumeric(txtBEBCost.Text) Then
            prmCost.Value = CDbl(txtBEBCost.Text)
        Else
            MsgBox("You must enter a Number For Cost!!")
            Exit Sub
        End If

        Dim prmMargin As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Margin", SqlDbType.Int)
        If IsNumeric(txtBEBMargin.Text) Then
            prmMargin.Value = CInt(txtBEBMargin.Text)
        Else
            MsgBox("You must enter a Whole Number For Margin!!")
            Exit Sub
        End If

        Dim prmSell As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Sell", SqlDbType.Money)
        If IsNumeric(txtBEBSell.Text) Then
            prmSell.Value = CDbl(txtBEBSell.Text)
        Else
            MsgBox("You must enter a Whole Number For Margin!!")
            Exit Sub
        End If


        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            cmdBEUpdatePrice.ExecuteNonQuery()
        Catch ex As Exception
        Finally
            cnn.Close()
        End Try
        ClearBulkEditFields()
        LoadLIDG()
        LoaddgSummary()
    End Sub

    Private Sub btnUpdatePrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePrice.Click
        BECalcSell()
    End Sub

    Private Sub btnDeleteJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteJob.Click

        If MsgBox("Are You Sure You Want To Delete This Job?", MsgBoxStyle.YesNo, "Do You Really Want To Do This?") = MsgBoxResult.Yes Then
            If MsgBox("ALL DATA ASSOCIATED WTIH THIS JOB WILL BE DESTROYED", MsgBoxStyle.YesNo, "If You Select Yes All Data For This Bid Will Be Gone!!!!") = MsgBoxResult.Yes Then
                DeleteJob()
            End If
        End If
    End Sub

    Private Sub DeleteJob()
        cnn.ConnectionString = frmMain.strConnect
        Dim strDelete As String
        strDelete = "DELETE Project WHERE ProjectID = " & frmMain.intJobID.ToString
        ExecuteQueryText(strDelete, cnn)
        strDelete = "Delete From Lineitem where ProjectID = " + frmMain.intJobID.ToString
        ExecuteQueryText(strDelete, cnn)
        strDelete = "Delete from Unit where projectid = " + frmMain.intJobID.ToString
        ExecuteQueryText(strDelete, cnn)
        strDelete = "Delete from Building where projectid = " + frmMain.intJobID.ToString
        ExecuteQueryText(strDelete, cnn)
        strDelete = "Delete from comment where projectid = " + frmMain.intJobID.ToString
        ExecuteQueryText(strDelete, cnn)
        strDelete = "Delete from alternates where projectid = " + frmMain.intJobID.ToString
        ExecuteQueryText(strDelete, cnn)

        Me.Close()
    End Sub

    Private Sub LoadBldgSummaryDG()
        intBOMBldgID = System.Convert.ToInt32(cboBldg.SelectedValue)
        cmdBOM.Visible = True
        cbxShowPrice.Visible = True
        lblprices.Visible = True

        cnn.ConnectionString = frmMain.strConnect

        Dim cmdBLDGBOM As New SqlCommand("BldgBOMSP", cnn)
        cmdBLDGBOM.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdBLDGBOM.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = cboBldg.SelectedValue
        Dim daBLDGBOM As New SqlDataAdapter(cmdBLDGBOM)
        Dim dsBLDGBOM As New DataSet
        Try
            daBLDGBOM.Fill(dsBLDGBOM, "BldgBOM")
        Catch ex As Exception
            MsgBox("Error Loading Buildign Line Item Datagrid")
        End Try
        dgBldgSummary.DataSource = dsBLDGBOM.Tables("BldgBOM")


    End Sub

    Private Sub LoadChangeOrders()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdCO As New SqlCommand("ChangeOrders", cnn)
        cmdCO.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdCO.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID
        Dim daCO As New SqlDataAdapter(cmdCO)
        Dim dsCO As New DataSet
        Try
            daCO.Fill(dsCO, "ChangeOrders")
        Catch ex As Exception
            MsgBox("Error Loading Change Order Line Item Datagrid")
        End Try
        dgChangeOrders.DataSource = dsCO.Tables("ChangeOrders")
        txtCODate.Text = Format(Now(), "d")
    End Sub

    Private Sub cmdBOM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBOM.Click
        If cbxShowPrice.Checked Then
            blnShowPrice = True
        Else
            blnShowPrice = False
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim BOM As New frmj_BldgBOM
        BOM.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub


    Private Sub txtBERProdID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBERProdID.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtBERDesc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBERDesc.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtBERType_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBERType.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub


    Private Sub btnShipBldgs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShipBldgs.Click
        Me.Cursor = Cursors.WaitCursor

        cnn.ConnectionString = frmMain.strConnect

        Dim i As Integer
        For i = 0 To intBldgDGRowCount - 1
            If dgBldg.IsSelected(i) Then
                Dim intBldgID As Integer

                If rbnBoth.Checked = True Then
                    intBldgID = System.Convert.ToInt32(dgBldg.Item(i, 0))

                    Dim cmdEditShipped As New SqlCommand("EditShipped", cnn)
                    cmdEditShipped.CommandType = CommandType.StoredProcedure

                    Dim prmBldgID As SqlParameter = cmdEditShipped.Parameters.Add("@BldgID", SqlDbType.Int)
                    prmBldgID.Value = intBldgID

                    Dim prmShipped As SqlParameter = cmdEditShipped.Parameters.Add("@shipped", SqlDbType.Bit)
                    prmShipped.Value = 1

                    Try
                        If cnn.State = ConnectionState.Closed Then
                            cnn.Open()
                        End If
                        cmdEditShipped.ExecuteNonQuery()
                        cnn.Close()
                    Catch ex As Exception
                        MsgBox("There was an error modifying this record!!")
                    Finally
                        If cnn.State = ConnectionState.Open Then
                            cnn.Close()
                        End If
                    End Try
                End If

                If rbnRough.Checked = True Then
                    intBldgID = System.Convert.ToInt32(dgBldg.Item(i, 0))

                    Dim cmdEditShipped As New SqlCommand("EditRShipped", cnn)
                    cmdEditShipped.CommandType = CommandType.StoredProcedure

                    Dim prmBldgID As SqlParameter = cmdEditShipped.Parameters.Add("@BldgID", SqlDbType.Int)
                    prmBldgID.Value = intBldgID

                    Dim prmShipped As SqlParameter = cmdEditShipped.Parameters.Add("@shipped", SqlDbType.Bit)
                    prmShipped.Value = 1

                    Try
                        If cnn.State = ConnectionState.Closed Then
                            cnn.Open()
                        End If
                        cmdEditShipped.ExecuteNonQuery()
                        cnn.Close()
                    Catch ex As Exception
                        MsgBox("There was an error modifying this record!!")
                    Finally
                        If cnn.State = ConnectionState.Open Then
                            cnn.Close()
                        End If
                    End Try
                End If

                If rbnTrim.Checked = True Then
                    intBldgID = System.Convert.ToInt32(dgBldg.Item(i, 0))

                    Dim cmdEditShipped As New SqlCommand("EditTShipped", cnn)
                    cmdEditShipped.CommandType = CommandType.StoredProcedure

                    Dim prmBldgID As SqlParameter = cmdEditShipped.Parameters.Add("@BldgID", SqlDbType.Int)
                    prmBldgID.Value = intBldgID

                    Dim prmShipped As SqlParameter = cmdEditShipped.Parameters.Add("@shipped", SqlDbType.Bit)
                    prmShipped.Value = 1

                    Try
                        If cnn.State = ConnectionState.Closed Then
                            cnn.Open()
                        End If
                        cmdEditShipped.ExecuteNonQuery()
                        cnn.Close()
                    Catch ex As Exception
                        MsgBox("There was an error modifying this record!!")
                    Finally
                        If cnn.State = ConnectionState.Open Then
                            cnn.Close()
                        End If
                    End Try
                End If




            End If


        Next i
        LoaddgSummary()
        Me.Cursor = Cursors.Default
        MsgBox("The Selected Buildings Have Been Marked As Shipped.", MsgBoxStyle.OKOnly, "They Shipped")
    End Sub

    Private Sub btnUpdateLIQuan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateLIQuan.Click
        cnn.ConnectionString = frmMain.strConnect

        Dim i As Integer
        For i = 0 To intUnitDGRowCount - 1
            'If dgUnitLineItems.IsSelected(i) Then
            '    MsgBox(dgUnitLineItems.Item(i, 2).ToString & " " & dgUnitLineItems.Item(i, 3).ToString)
            'End If
            Dim intItemID As Int32

            intItemID = System.Convert.ToInt32(dgLineItems.Item(i, 0))
            'intUnitID = dgLineItems.Item(i, 1)
            'strLocation = dgLineItems.Item(i, 5)
            'strType = dgLineItems.Item(i, 7)


            Dim cmdUpdateLineItemQuan As New SqlCommand
            Dim strCommandText As String
            strCommandText = "Update LineItem Set Quantity = "
            strCommandText = strCommandText & dgLineItems.Item(i, 6).ToString
            strCommandText = strCommandText & " WHERE (ProjectID = "
            strCommandText = strCommandText & frmMain.intJobID
            strCommandText = strCommandText & ") AND (ItemID = "
            strCommandText = strCommandText & intItemID
            'strCommandText = strCommandText & ") AND (UnitID = "
            'strCommandText = strCommandText & intUnitID
            'strCommandText = strCommandText & ") AND (Location = '"
            'strCommandText = strCommandText & strLocation
            'strCommandText = strCommandText & "') AND (Type = '"
            'strCommandText = strCommandText & strType
            strCommandText = strCommandText & ")"

            cmdUpdateLineItemQuan.CommandText = strCommandText
            cmdUpdateLineItemQuan.Connection = cnn
            'MsgBox(strCommandText)
            Try
                cnn.Open()
                cmdUpdateLineItemQuan.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Error in btnUpdateLIQuan")
            Finally
                cnn.Close()
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
            End Try

        Next
        'HideUnitEdit()
        'LoadLineItemDG()
    End Sub

    Private Sub mnurptSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnurptSummary.Click
        Me.Cursor = Cursors.WaitCursor
        Dim JobReport As New frmJobReportMain
        JobReport.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuEditAuxTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditAuxTitle.Click
        Me.Cursor = Cursors.WaitCursor
        Dim JobEditAux As New frmj_EditAuxLabel
        JobEditAux.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnCOSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCOSave.Click
        SaveChangeOrder()
    End Sub

    Private Sub dgChangeOrders_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgChangeOrders.Click
        LoadChangeOrderDetails()
    End Sub

    Private Sub btnCODelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCODelete.Click
        DeleteChangeOrder()
        ClearChangeOrderDetails()
        LoadChangeOrders()
    End Sub

    Private Sub btnCOCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCOCancel.Click
        ClearChangeOrderDetails()
    End Sub


    Private Sub txtBEBRT_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBEBRT.KeyPress
        Dim ch As Char
        ch = e.KeyChar
        ch = Char.ToUpper(ch)
        Select Case ch
            Case "R"c
                txtBEBRT.Text = ch
            Case "T"c
                txtBEBRT.Text = ch
        End Select
        'If ch.IsLetter(ch) Then
        '    ch.ToUpper(ch)
        '    txtLIRT.Text = ch
        'End If
        'txtLIRT.Text = e.KeyChar.ToUpper(e.KeyChar)
        e.Handled = True
    End Sub

    Private Sub txtBERRT_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBERRT.KeyPress
        Dim ch As Char
        ch = e.KeyChar
        ch = Char.ToUpper(ch)
        Select Case ch
            Case "R"c
                txtBERRT.Text = ch
            Case "T"c
                txtBERRT.Text = ch
        End Select
        'If ch.IsLetter(ch) Then
        '    ch.ToUpper(ch)
        '    txtLIRT.Text = ch
        'End If
        'txtLIRT.Text = e.KeyChar.ToUpper(e.KeyChar)
        e.Handled = True
    End Sub


    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Dim strSample As String
    '    strSample = " "
    '    MsgBox(strSample.IndexOfAny("UCXSA".ToCharArray).ToString())

    'End Sub

    Private Sub mnuDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 1
    End Sub

    Private Sub mnuJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 0
    End Sub

    Private Sub mnuComments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 5
    End Sub

    Private Sub mnuCO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 6
    End Sub

    Private Sub mnuBldgs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 2
    End Sub

    Private Sub mnuSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 3
    End Sub

    Private Sub mnuBE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabJob.SelectedIndex = 4
    End Sub


    Private Sub mnuShipDates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShipDates.Click
        Me.Cursor = Cursors.WaitCursor
        Dim JobUpdateShipDates As New frmj_UpdateShipping
        JobUpdateShipDates.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnAddUnitWC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddUnitWC.Click
        If cboBECatalog.SelectedIndex = 0 Then
            MsgBox("You must select an item!")
            Exit Sub
        End If
        Dim intJobID As Integer
        Dim strRecordSource As String
        Dim strPartNumber As String
        Dim strDescription As String
        Dim dblCost As Double
        Dim intMargin As Integer
        Dim dblSell As Double
        Dim intQuan As Integer

        '' Initialize all basic variables
        'cnn.ConnectionString = frmMain.strConnect

        intJobID = frmMain.intJobID

        If txtBERType.Text <> "" Then
            Dim strSQL As String
            strSQL = "SELECT ProjectID, PartNumber, Type FROM LineItems"
            strSQL = strSQL & " WHERE (ProjectID = " & intJobID.ToString & ""
            strSQL = strSQL & ") AND (Type = '" & txtBERType.Text & "')"
            strSQL = strSQL & "AND (PartNumber <> '" & txtBERProdID.Text & "')"


            Dim cmdTypeSearch As New SqlCommand(strSQL, cnn)
            cmdTypeSearch.CommandType = CommandType.Text
            Dim drTypeSearch As SqlDataReader
            cnn.Open()
            drTypeSearch = cmdTypeSearch.ExecuteReader
            If drTypeSearch.HasRows Then
                MsgBox("The Type you have selected for this part has already been used in this job!")
                drTypeSearch.Close()
                cnn.Close()
                Exit Sub
            End If
            drTypeSearch.Close()
            cnn.Close()
        End If


        If rbBEMain.Checked Then
            strRecordSource = "Main"
        Else
            strRecordSource = "SO"
        End If

        If Not IsNumeric(txtBERQuan.Text) Then
            MsgBox("You must enter a quantity!")
            Exit Sub
        End If
        intQuan = CInt(txtBERQuan.Text)
        If txtBERLocation.Text = "" Then
            MsgBox("You must enter a Location!")
            Exit Sub
        End If

        strPartNumber = txtBERProdID.Text
        strDescription = txtBERDesc.Text
        dblCost = CDbl(txtBERCost.Text)
        intMargin = CInt(txtBERMargin.Text)
        If IsNumeric(txtBERSell.Text) Then
            dblSell = CDbl(txtBERSell.Text)
        Else
            dblSell = 0
        End If

        If txtUnitWC.Text = "" Then
            MsgBox("You must enter a Unit Type Prefix!!")
            Exit Sub
        End If


        Dim cmdAddUnitWC As New SqlCommand("AddLIUnitWC", cnn)
        cmdAddUnitWC.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdAddUnitWC.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim prmUnitTypeWC As SqlParameter = cmdAddUnitWC.Parameters.Add("@UnitTypeWC", SqlDbType.VarChar, 10)
        prmUnitTypeWC.Value = Trim(txtUnitWC.Text)

        Dim prmRecordSource As SqlParameter = cmdAddUnitWC.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
        prmRecordSource.Value = strRecordSource

        Dim prmPartNumber As SqlParameter = cmdAddUnitWC.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
        prmPartNumber.Value = strPartNumber

        Dim prmRT As SqlParameter = cmdAddUnitWC.Parameters.Add("@RT", SqlDbType.Char, 1)
        prmRT.Value = txtBERRT.Text

        Dim prmDescription As SqlParameter = cmdAddUnitWC.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = strDescription

        Dim prmCost As SqlParameter = cmdAddUnitWC.Parameters.Add("@Cost", SqlDbType.Money)
        If IsNumeric(txtBERCost.Text) Then
            prmCost.Value = dblCost
        Else
            MsgBox("You must enter a Number for the Cost")
            Exit Sub
        End If


        Dim prmMargin As SqlParameter = cmdAddUnitWC.Parameters.Add("@Margin", SqlDbType.Int)
        If IsNumeric(txtBERMargin.Text) Then
            prmMargin.Value = intMargin
        Else
            MsgBox("You must enter a Number for the Margin")
            Exit Sub
        End If

        Dim prmSell As SqlParameter = cmdAddUnitWC.Parameters.Add("@Sell", SqlDbType.Money)
        If IsNumeric(txtBERSell.Text) Then
            prmSell.Value = dblSell
        Else
            MsgBox("You must enter a Number for the Selling Price")
            Exit Sub
        End If

        Dim prmQuantity As SqlParameter = cmdAddUnitWC.Parameters.Add("@Quantity", SqlDbType.Int)
        If IsNumeric(txtBERQuan.Text) Then
            prmQuantity.Value = CInt((txtBERQuan.Text))
        Else
            MsgBox("You must enter a Number for the Quantity")
            Exit Sub
        End If

        Dim prmLocation As SqlParameter = cmdAddUnitWC.Parameters.Add("@Location", SqlDbType.Char, 50)
        prmLocation.Value = txtBERLocation.Text

        Dim prmType As SqlParameter = cmdAddUnitWC.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
        prmType.Value = txtBERType.Text
        Dim strGO As String
        Dim strCaption As String
        strCaption = "You are about to add Part " & txtBERProdID.Text & vbCrLf
        strCaption = strCaption & "To all Units with the Type beginning with " & txtUnitWC.Text & vbCrLf & vbCrLf
        strCaption = strCaption & "ARE YOU SURE YOU WANT TO DO THIS???"
        strGO = MsgBox(strCaption, MsgBoxStyle.YesNo, "SLOW DOWN AND THINK ABOUT THIS!!").ToString
        If strGO = "6" Then
            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If

                cmdAddUnitWC.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Error in Adding this Part Number!")
            Finally
                cnn.Close()
            End Try
        End If

        LoadBEJobItemcbo()
        LoadBETypeCBO()
        LoadLIDG()
        LoaddgSummary()
        ClearBulkEditFields()
        lblBERQuan.Visible = False
        lblBERLocation.Visible = False
        lblBerType.Visible = False
        txtBERQuan.Visible = False
        txtBERLocation.Visible = False
        txtBERType.Visible = False
        btnAddUnitWC.Visible = False
        txtUnitWC.Visible = False
        btnAddToUnits.Visible = True
    End Sub

    Private Sub btnAddToUnits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddToUnits.Click
        lblBERQuan.Visible = True
        lblBERLocation.Visible = True
        lblBerType.Visible = True
        txtBERQuan.Visible = True
        txtBERLocation.Visible = True
        txtBERType.Visible = True
        btnAddUnitWC.Visible = True
        txtUnitWC.Visible = True
        btnAddToUnits.Visible = False

    End Sub

    Private Sub btnUpdateShipDates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateShipDates.Click
        If MsgBox("You are about to reset all Shipping Dates for this job", MsgBoxStyle.OkCancel, "Slow down and think about this!") = MsgBoxResult.Ok Then

            Dim intBldgID As Integer
            Dim dtShipDate As Date
            Dim dtShipDateR As Date
            Dim strBldgs As String
            Dim strUpdateShipping As String
            Dim strUpdateShippingR As String
            strBldgs = "SELECT BldgID, ProjectID, shipdateR, shipdate FROM Building WHERE  (ProjectID  =" & frmMain.intJobID & ")"
            Dim cmdBldgs As New SqlCommand(strBldgs, cnn)
            cmdBldgs.CommandType = CommandType.Text

            Dim drBldgs As SqlDataReader
            cnn.Open()

            drBldgs = cmdBldgs.ExecuteReader

            While drBldgs.Read()

                intBldgID = System.Convert.ToInt32(drBldgs.Item("BldgID"))
                dtShipDate = System.Convert.ToDateTime(drBldgs.Item("ShipDate"))
                If IsDBNull(drBldgs.Item("ShipDateR")) Then
                    dtShipDateR = dtShipDate.AddDays(-60)
                Else
                    dtShipDateR = System.Convert.ToDateTime(drBldgs.Item("ShipDateR"))
                End If

                Dim cnn2 As New SqlConnection
                cnn2.ConnectionString = frmMain.strConnect

                strUpdateShipping = "Update LineItem Set ShipDate = '" & dtShipDate & "' Where BldgID = " & intBldgID & " and RT = 'T'"
                Dim cmdUpdate1 As New SqlCommand(strUpdateShipping, cnn2)
                cmdUpdate1.CommandType = CommandType.Text
                Try
                    If cnn2.State = ConnectionState.Closed Then
                        cnn2.Open()
                    End If
                    cmdUpdate1.ExecuteNonQuery()
                    cnn2.Close()
                Catch ex As Exception
                    MsgBox("There was an error Editing the Trim Date!!")
                Finally
                    If cnn2.State = ConnectionState.Open Then
                        cnn2.Close()
                    End If
                End Try

                strUpdateShippingR = "Update LineItem Set ShipDate = '" & dtShipDateR & "' Where BldgID = " & intBldgID & " and (RT = 'R' OR RT = '')"
                Dim cmdUpdate2 As New SqlCommand(strUpdateShippingR, cnn2)
                cmdUpdate2.CommandType = CommandType.Text
                Try
                    If cnn2.State = ConnectionState.Closed Then
                        cnn2.Open()
                    End If
                    cmdUpdate2.ExecuteNonQuery()
                    cnn2.Close()
                Catch ex As Exception
                    MsgBox("There was an error Editing The Rough Date!!")
                Finally
                    If cnn2.State = ConnectionState.Open Then
                        cnn2.Close()
                    End If
                End Try
            End While

            drBldgs.Close()
            cnn.Close()
            Dim strPrompt2 As String
            strPrompt2 = "All Line Item Shipping Dates have been reset to the Building Ship Dates for R ant T items."
            strPrompt2 = strPrompt2 & vbCrLf & vbCrLf & "If any Line Items were set separately you will need to reset these manually!!"
            MsgBox(strPrompt2, MsgBoxStyle.Exclamation, "READ THIS!")
        End If
    End Sub

    Private Sub btnUpdateShipping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateShipping.Click
        cnn.ConnectionString = frmMain.strConnect
        Dim i As Integer
        For i = 0 To intShippingDGRowCount - 1

            Dim intBldgID As Integer

            intBldgID = System.Convert.ToInt32(dg_ShipDates.Item(i, 0))
            cnn.ConnectionString = frmMain.strConnect

            Dim cmdEditShipDate As New SqlCommand("EditShipDate", cnn)
            cmdEditShipDate.CommandType = CommandType.StoredProcedure

            Dim prmBldgID As SqlParameter = cmdEditShipDate.Parameters.Add("@BldgID", SqlDbType.Int)
            prmBldgID.Value = intBldgID

            Dim prmShipDate As SqlParameter = cmdEditShipDate.Parameters.Add("@shipdate", SqlDbType.DateTime)
            If IsDate(dg_ShipDates.Item(i, 5)) Then
                prmShipDate.Value = dg_ShipDates.Item(i, 5)
            Else
                Exit For
            End If

            Dim prmShipDateR As SqlParameter = cmdEditShipDate.Parameters.Add("@shipdateR", SqlDbType.DateTime)
            If IsDate(dg_ShipDates.Item(i, 4)) Then
                prmShipDateR.Value = dg_ShipDates.Item(i, 4)
            Else
                Exit For
            End If


            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                cmdEditShipDate.ExecuteNonQuery()
                'cnn.Close()
            Catch ex As Exception
                '    MsgBox("There was an error modifying this record!!")
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
            End Try

        Next
        LoadShipDatesDG()
        LoadLIDG()
        LoaddgSummary()
    End Sub

    Private Sub LoadBidComments()
        Dim strSQL As String
        strSQL = "SELECT CommentID, EntryDate, Author, Comment FROM Comment WHERE ProjectId = " & _
        intOrigBidID
        Dim cmdBidComments As New SqlCommand(strSQL, cnn)
        cmdBidComments.CommandType = CommandType.Text
        Dim daBidComments As New SqlDataAdapter(cmdBidComments)
        Dim dsBidComments As New DataSet
        Try
            daBidComments.Fill(dsBidComments, "Comments")
        Catch ex As Exception
            MsgBox("Error Loading Bid Comments DataGrid")
        End Try

        dgBidComments.DataSource = dsBidComments.Tables("Comments")
    End Sub

    Private Sub dgBidComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgBidComments.Click
        txtBidComment.Text = dgBidComments.Item(dgBidComments.CurrentRowIndex, 2).ToString
    End Sub

    Private Sub cboJob_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectionChangeCommitted
        frmMain.intJobID = CInt(cboJob.SelectedValue)

        tabJob.Visible = True
        LoadJobInfo()
        mnuFile.Visible = True
        mnuReports.Visible = True
        cboJob.Visible = False
    End Sub

    Private Sub tpgJob_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgJob.Enter
        LoadJobInfo()
    End Sub

    Private Sub tpgDetails_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgDetails.Enter
        LoadBldgDG()
    End Sub

    Private Sub tpgBuilding_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgBuilding.Enter
        LoadBldgs()
    End Sub

    Private Sub cboBldg_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBldg.SelectionChangeCommitted
        LoadBldgSummaryDG()
    End Sub

    Sub LoadEditVerbalForm()
    End Sub

    Private Sub tpgShipping_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgShipping.Enter
        LoadShipDatesDG()
    End Sub

    Private Sub tpgSummary_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgSummary.Enter
        LoaddgSummary()
        LoaddgSummary()
    End Sub

    Private Sub tpgBulkEdits_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgBulkEdits.Enter
        LoadBEJobItemcbo()
        LoadBETypeCBO()
        LoadBECatalogCBO()
    End Sub

    Private Sub cboBEBidItems_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBEBidItems.SelectionChangeCommitted
        LoadBELIbyPart()
    End Sub

    Private Sub tpgComments_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgComments.Enter
        LoadComments()
    End Sub

    Private Sub tpgBidComments_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tpgBidComments.Enter
        LoadBidComments()
    End Sub
End Class
