Public Class frmEditCatSO
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents daCatSO As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents dgCatSO As System.Windows.Forms.DataGrid
    Friend WithEvents dgCatSOStyle As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents ProductID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents VendorPartNo As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Cost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Available As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents OnOrder As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Committed As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents LeadTime As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DsCatSO1 As LD_Bid.dsCatSO
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.dgCatSO = New System.Windows.Forms.DataGrid
        Me.DsCatSO1 = New LD_Bid.dsCatSO
        Me.dgCatSOStyle = New System.Windows.Forms.DataGridTableStyle
        Me.ProductID = New System.Windows.Forms.DataGridTextBoxColumn
        Me.Description = New System.Windows.Forms.DataGridTextBoxColumn
        Me.VendorPartNo = New System.Windows.Forms.DataGridTextBoxColumn
        Me.Cost = New System.Windows.Forms.DataGridTextBoxColumn
        Me.Available = New System.Windows.Forms.DataGridTextBoxColumn
        Me.OnOrder = New System.Windows.Forms.DataGridTextBoxColumn
        Me.Committed = New System.Windows.Forms.DataGridTextBoxColumn
        Me.LeadTime = New System.Windows.Forms.DataGridTextBoxColumn
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.daCatSO = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.dgCatSO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCatSO1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgCatSO
        '
        Me.dgCatSO.DataMember = ""
        Me.dgCatSO.DataSource = Me.DsCatSO1.CatalogSO
        Me.dgCatSO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgCatSO.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgCatSO.Location = New System.Drawing.Point(0, 0)
        Me.dgCatSO.Name = "dgCatSO"
        Me.dgCatSO.Size = New System.Drawing.Size(660, 398)
        Me.dgCatSO.TabIndex = 0
        Me.dgCatSO.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgCatSOStyle})
        '
        'DsCatSO1
        '
        Me.DsCatSO1.DataSetName = "dsCatSO"
        Me.DsCatSO1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'dgCatSOStyle
        '
        Me.dgCatSOStyle.DataGrid = Me.dgCatSO
        Me.dgCatSOStyle.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.ProductID, Me.Description, Me.VendorPartNo, Me.Cost, Me.Available, Me.OnOrder, Me.Committed, Me.LeadTime})
        Me.dgCatSOStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgCatSOStyle.MappingName = "CatalogSO"
        '
        'ProductID
        '
        Me.ProductID.Format = ""
        Me.ProductID.FormatInfo = Nothing
        Me.ProductID.HeaderText = "Product ID"
        Me.ProductID.MappingName = "ProductID"
        Me.ProductID.NullText = ""
        Me.ProductID.ReadOnly = True
        Me.ProductID.Width = 75
        '
        'Description
        '
        Me.Description.Format = ""
        Me.Description.FormatInfo = Nothing
        Me.Description.HeaderText = "Description"
        Me.Description.MappingName = "Description"
        Me.Description.NullText = ""
        Me.Description.Width = 150
        '
        'VendorPartNo
        '
        Me.VendorPartNo.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.VendorPartNo.Format = ""
        Me.VendorPartNo.FormatInfo = Nothing
        Me.VendorPartNo.HeaderText = "Vendor Part #"
        Me.VendorPartNo.MappingName = "VendorPartNo"
        Me.VendorPartNo.NullText = ""
        Me.VendorPartNo.Width = 75
        '
        'Cost
        '
        Me.Cost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.Cost.Format = "C"
        Me.Cost.FormatInfo = Nothing
        Me.Cost.HeaderText = "Cost"
        Me.Cost.MappingName = "StandardCost"
        Me.Cost.NullText = ""
        Me.Cost.Width = 75
        '
        'Available
        '
        Me.Available.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Available.Format = ""
        Me.Available.FormatInfo = Nothing
        Me.Available.HeaderText = "Available"
        Me.Available.MappingName = "AvailableUnits"
        Me.Available.NullText = "?"
        Me.Available.Width = 60
        '
        'OnOrder
        '
        Me.OnOrder.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.OnOrder.Format = ""
        Me.OnOrder.FormatInfo = Nothing
        Me.OnOrder.HeaderText = "On Order"
        Me.OnOrder.MappingName = "OnOrderUnits"
        Me.OnOrder.NullText = "?"
        Me.OnOrder.Width = 60
        '
        'Committed
        '
        Me.Committed.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.Committed.Format = ""
        Me.Committed.FormatInfo = Nothing
        Me.Committed.HeaderText = "Committed"
        Me.Committed.MappingName = "CommittedUnits"
        Me.Committed.NullText = "?"
        Me.Committed.Width = 60
        '
        'LeadTime
        '
        Me.LeadTime.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.LeadTime.Format = ""
        Me.LeadTime.FormatInfo = Nothing
        Me.LeadTime.HeaderText = "Lead Time"
        Me.LeadTime.MappingName = "LeadTime"
        Me.LeadTime.NullText = "?"
        Me.LeadTime.Width = 60
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(0, 0)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 20)
        Me.btnUpdate.TabIndex = 1
        Me.btnUpdate.Text = "Update"
        '
        'daCatSO
        '
        Me.daCatSO.DeleteCommand = Me.SqlDeleteCommand1
        Me.daCatSO.InsertCommand = Me.SqlInsertCommand1
        Me.daCatSO.SelectCommand = Me.SqlSelectCommand1
        Me.daCatSO.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CatalogSO", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ProductID", "ProductID"), New System.Data.Common.DataColumnMapping("Description", "Description"), New System.Data.Common.DataColumnMapping("VendorCode", "VendorCode"), New System.Data.Common.DataColumnMapping("VendorPartNo", "VendorPartNo"), New System.Data.Common.DataColumnMapping("StandardCost", "StandardCost"), New System.Data.Common.DataColumnMapping("AvailableUnits", "AvailableUnits"), New System.Data.Common.DataColumnMapping("OnOrderUnits", "OnOrderUnits"), New System.Data.Common.DataColumnMapping("CommittedUnits", "CommittedUnits"), New System.Data.Common.DataColumnMapping("LeadTime", "LeadTime")})})
        Me.daCatSO.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM CatalogSO WHERE (ProductID = @Original_ProductID) AND (AvailableUnits" & _
        " = @Original_AvailableUnits OR @Original_AvailableUnits IS NULL AND AvailableUni" & _
        "ts IS NULL) AND (CommittedUnits = @Original_CommittedUnits OR @Original_Committe" & _
        "dUnits IS NULL AND CommittedUnits IS NULL) AND (Description = @Original_Descript" & _
        "ion OR @Original_Description IS NULL AND Description IS NULL) AND (LeadTime = @O" & _
        "riginal_LeadTime OR @Original_LeadTime IS NULL AND LeadTime IS NULL) AND (OnOrde" & _
        "rUnits = @Original_OnOrderUnits OR @Original_OnOrderUnits IS NULL AND OnOrderUni" & _
        "ts IS NULL) AND (StandardCost = @Original_StandardCost OR @Original_StandardCost" & _
        " IS NULL AND StandardCost IS NULL) AND (VendorCode = @Original_VendorCode OR @Or" & _
        "iginal_VendorCode IS NULL AND VendorCode IS NULL) AND (VendorPartNo = @Original_" & _
        "VendorPartNo OR @Original_VendorPartNo IS NULL AND VendorPartNo IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProductID", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProductID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvailableUnits", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvailableUnits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CommittedUnits", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CommittedUnits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Description", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LeadTime", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LeadTime", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OnOrderUnits", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OnOrderUnits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StandardCost", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StandardCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VendorCode", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VendorCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VendorPartNo", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VendorPartNo", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""CO-DEV-WS"";packet size=4096;user id=SA;data source=""CO-TECH-LT-00" & _
        "3"";persist security info=True;initial catalog=""LD-Bid"";password=vw9110"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO CatalogSO(ProductID, Description, VendorCode, VendorPartNo, StandardC" & _
        "ost, AvailableUnits, OnOrderUnits, CommittedUnits, LeadTime) VALUES (@ProductID," & _
        " @Description, @VendorCode, @VendorPartNo, @StandardCost, @AvailableUnits, @OnOr" & _
        "derUnits, @CommittedUnits, @LeadTime); SELECT ProductID, Description, VendorCode" & _
        ", VendorPartNo, StandardCost, AvailableUnits, OnOrderUnits, CommittedUnits, Lead" & _
        "Time FROM CatalogSO WHERE (ProductID = @ProductID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProductID", System.Data.SqlDbType.NVarChar, 255, "ProductID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 255, "Description"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VendorCode", System.Data.SqlDbType.NVarChar, 255, "VendorCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VendorPartNo", System.Data.SqlDbType.NVarChar, 255, "VendorPartNo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StandardCost", System.Data.SqlDbType.Float, 8, "StandardCost"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvailableUnits", System.Data.SqlDbType.Float, 8, "AvailableUnits"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OnOrderUnits", System.Data.SqlDbType.Float, 8, "OnOrderUnits"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CommittedUnits", System.Data.SqlDbType.Float, 8, "CommittedUnits"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LeadTime", System.Data.SqlDbType.NVarChar, 255, "LeadTime"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT ProductID, Description, VendorCode, VendorPartNo, StandardCost, AvailableU" & _
        "nits, OnOrderUnits, CommittedUnits, LeadTime FROM CatalogSO"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE CatalogSO SET ProductID = @ProductID, Description = @Description, VendorCo" & _
        "de = @VendorCode, VendorPartNo = @VendorPartNo, StandardCost = @StandardCost, Av" & _
        "ailableUnits = @AvailableUnits, OnOrderUnits = @OnOrderUnits, CommittedUnits = @" & _
        "CommittedUnits, LeadTime = @LeadTime WHERE (ProductID = @Original_ProductID) AND" & _
        " (AvailableUnits = @Original_AvailableUnits OR @Original_AvailableUnits IS NULL " & _
        "AND AvailableUnits IS NULL) AND (CommittedUnits = @Original_CommittedUnits OR @O" & _
        "riginal_CommittedUnits IS NULL AND CommittedUnits IS NULL) AND (Description = @O" & _
        "riginal_Description OR @Original_Description IS NULL AND Description IS NULL) AN" & _
        "D (LeadTime = @Original_LeadTime OR @Original_LeadTime IS NULL AND LeadTime IS N" & _
        "ULL) AND (OnOrderUnits = @Original_OnOrderUnits OR @Original_OnOrderUnits IS NUL" & _
        "L AND OnOrderUnits IS NULL) AND (StandardCost = @Original_StandardCost OR @Origi" & _
        "nal_StandardCost IS NULL AND StandardCost IS NULL) AND (VendorCode = @Original_V" & _
        "endorCode OR @Original_VendorCode IS NULL AND VendorCode IS NULL) AND (VendorPar" & _
        "tNo = @Original_VendorPartNo OR @Original_VendorPartNo IS NULL AND VendorPartNo " & _
        "IS NULL); SELECT ProductID, Description, VendorCode, VendorPartNo, StandardCost," & _
        " AvailableUnits, OnOrderUnits, CommittedUnits, LeadTime FROM CatalogSO WHERE (Pr" & _
        "oductID = @ProductID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProductID", System.Data.SqlDbType.NVarChar, 255, "ProductID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 255, "Description"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VendorCode", System.Data.SqlDbType.NVarChar, 255, "VendorCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VendorPartNo", System.Data.SqlDbType.NVarChar, 255, "VendorPartNo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StandardCost", System.Data.SqlDbType.Float, 8, "StandardCost"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AvailableUnits", System.Data.SqlDbType.Float, 8, "AvailableUnits"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OnOrderUnits", System.Data.SqlDbType.Float, 8, "OnOrderUnits"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CommittedUnits", System.Data.SqlDbType.Float, 8, "CommittedUnits"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LeadTime", System.Data.SqlDbType.NVarChar, 255, "LeadTime"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ProductID", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ProductID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_AvailableUnits", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AvailableUnits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CommittedUnits", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CommittedUnits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Description", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LeadTime", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LeadTime", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OnOrderUnits", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OnOrderUnits", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_StandardCost", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "StandardCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VendorCode", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VendorCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_VendorPartNo", System.Data.SqlDbType.NVarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "VendorPartNo", System.Data.DataRowVersion.Original, Nothing))
        '
        'frmEditCatSO
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(660, 398)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.dgCatSO)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditCatSO"
        Me.Text = "Special Order Catalog"
        CType(Me.dgCatSO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCatSO1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmEditCatSO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = frmMain.strConnect
        daCatSO.Fill(DsCatSO1)
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        daCatSO.Update(DsCatSO1)
        DsCatSO1.Clear()
        daCatSO.Fill(DsCatSO1)
    End Sub
End Class
