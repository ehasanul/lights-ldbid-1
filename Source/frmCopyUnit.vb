Imports System.Data
Imports System.Data.SqlClient
Public Class frmCopyUnit
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim blnStartUnit As Boolean 'Tests Startup Condition in LoadUnits
    Dim intNewUnitID As Integer
    Dim intOldUnitID As Integer
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents txtBUnitType As System.Windows.Forms.TextBox
    Friend WithEvents txtBUnitDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnCopyUnit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.txtBUnitType = New System.Windows.Forms.TextBox
        Me.txtBUnitDescription = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnCopyUnit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboUnit
        '
        Me.cboUnit.Location = New System.Drawing.Point(20, 28)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(184, 21)
        Me.cboUnit.TabIndex = 0
        '
        'txtBUnitType
        '
        Me.txtBUnitType.Location = New System.Drawing.Point(228, 28)
        Me.txtBUnitType.Name = "txtBUnitType"
        Me.txtBUnitType.Size = New System.Drawing.Size(156, 20)
        Me.txtBUnitType.TabIndex = 1
        Me.txtBUnitType.Text = ""
        '
        'txtBUnitDescription
        '
        Me.txtBUnitDescription.Location = New System.Drawing.Point(228, 76)
        Me.txtBUnitDescription.Name = "txtBUnitDescription"
        Me.txtBUnitDescription.Size = New System.Drawing.Size(152, 20)
        Me.txtBUnitDescription.TabIndex = 2
        Me.txtBUnitDescription.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(228, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(148, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Unit Type"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(224, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(140, 20)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Unit Description"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(20, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Units In Bid"
        '
        'btnCopyUnit
        '
        Me.btnCopyUnit.Location = New System.Drawing.Point(228, 120)
        Me.btnCopyUnit.Name = "btnCopyUnit"
        Me.btnCopyUnit.Size = New System.Drawing.Size(152, 24)
        Me.btnCopyUnit.TabIndex = 6
        Me.btnCopyUnit.Text = "Copy Unit"
        '
        'frmCopyUnit
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(408, 230)
        Me.Controls.Add(Me.btnCopyUnit)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBUnitDescription)
        Me.Controls.Add(Me.txtBUnitType)
        Me.Controls.Add(Me.cboUnit)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCopyUnit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Copy Unit"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub LoadUnits()
        'Units
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadUnits As New SqlCommand("Units", cnn)
        cmdLoadUnits.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadUnits.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daUnits As New SqlDataAdapter(cmdLoadUnits)
        Dim dsUnits As New DataSet
        daUnits.Fill(dsUnits, "Units")
        Dim dtUnits As DataTable = dsUnits.Tables("Units")
        Dim dr As DataRow = dtUnits.NewRow
        dr("Unit") = " - Please Select A Unit"
        dr("UnitID") = 0
        dtUnits.Rows.InsertAt(dr, 0)
        cboUnit.DataSource = dsUnits.Tables("Units")
        cboUnit.DisplayMember = "Unit"
        cboUnit.ValueMember = "UnitID"
        blnStartUnit = False
    End Sub

    Private Sub AddNewUnit()
        Dim cmdAddNewUnit As New SqlCommand("AddUnit", cnn)
        cmdAddNewUnit.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAddNewUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmUnitType As SqlParameter = cmdAddNewUnit.Parameters.Add("@UnitType", SqlDbType.Char, 10)
        prmUnitType.Value = txtBUnitType.Text

        Dim prmDescription As SqlParameter = cmdAddNewUnit.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtBUnitDescription.Text

        Dim prmNewUnitID As SqlParameter = cmdAddNewUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
        prmNewUnitID.Direction = ParameterDirection.Output


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            cmdAddNewUnit.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        ' Gets New Unit ID From @@Identity
        intNewUnitID = System.Convert.ToInt32(prmNewUnitID.Value)
        'frmMain.intUnitID = intUnitID
        'MsgBox(intNewUnitID.ToString)
    End Sub

    Private Sub frmCopyUnit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        blnStartUnit = True
        LoadUnits()
    End Sub

    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
        If IsNumeric(cboUnit.SelectedValue) Then
            'MsgBox(cboUnit.SelectedValue.ToString)
        End If
        'MsgBox(cboUnit.SelectedValue.ToString)
    End Sub

    Private Sub btnCopyUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyUnit.Click
        AddNewUnit()
        intOldUnitID = System.Convert.ToInt32(cboUnit.SelectedValue)
        'MsgBox(intOldUnitID.ToString)
        CopyUnit()
    End Sub

    Private Sub CopyUnit()
        If txtBUnitType.Text = "" Then
            MsgBox("You Must Enter A Unit Type!")
            Exit Sub
        End If

        If txtBUnitDescription.Text = "" Then
            MsgBox("You Must Enter A Unit Description!")
            Exit Sub
        End If
        'Run b_CopyUnitLineItemDef
        Dim cmdCopyUnit As New SqlCommand("CopyLineItemDefinition", cnn)
        cmdCopyUnit.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdCopyUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmNewUnitID As SqlParameter = cmdCopyUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
        prmNewUnitID.Value = intNewUnitID

        Dim prmOldUnitID As SqlParameter = cmdCopyUnit.Parameters.Add("@OldUnitID", SqlDbType.Int)
        prmOldUnitID.Value = intOldUnitID
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdCopyUnit.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error copying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        '----------- Line Item Definitions Added ----------------

        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "SELECT ItemID, PartNumber, RT FROM LineItem "
        strSqlCommand = strSqlCommand & "WHERE UnitID = " & intNewUnitID
        Dim cmdItems As New SqlCommand(strSqlCommand, cnn)
        Dim drItems As SqlDataReader
        cnn.Open()
        drItems = cmdItems.ExecuteReader

        While drItems.Read
            'MsgBox(drItems("PartNumber").ToString)
            ' New connection because cnn is still open
            Dim cnn1 As New SqlConnection
            cnn1.ConnectionString = frmMain.strConnect
            Dim intNewItemID As Integer
            Dim intOldItemID As Integer
            Dim strPartNumber As String
            Dim strRT As String
            intNewItemID = System.Convert.ToInt32(drItems("ItemId"))
            strPartNumber = drItems("PartNumber").ToString
            strRT = drItems("RT").ToString
            ' Get Old ItemID 
            Dim strSqlCommand1 As String
            strSqlCommand1 = "SELECT ItemID FROM bidUnitLineItemDef "
            strSqlCommand1 = strSqlCommand1 & "WHERE UnitID = " & intOldUnitID
            strSqlCommand1 = strSqlCommand1 & " AND PartNumber = '" & Trim(strPartNumber)
            strSqlCommand1 = strSqlCommand1 & "' AND RT = '" & Trim(strRT) & "'"
            Dim cmdOldItemID As New SqlCommand(strSqlCommand1, cnn1)
            Dim drOldItemID As SqlDataReader
            cnn1.Open()
            drOldItemID = cmdOldItemID.ExecuteReader
            drOldItemID.Read()
            intOldItemID = System.Convert.ToInt32(drOldItemID("ItemID"))
            drOldItemID.Close()
            cnn1.Close()
            ' All Variables defined to call b_CopyUnitItemQuan

            Dim cmdCopyLIQuan As New SqlCommand("PartCount", cnn1)
            cmdCopyLIQuan.CommandType = CommandType.StoredProcedure

            Dim prmNewItemID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@NewItemID", SqlDbType.Int)
            prmNewItemID1.Value = intNewItemID

            Dim prmBidID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmBidID1.Value = frmMain.intBidID

            Dim prmNewUnitID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@NewUnitID", SqlDbType.Int)
            prmNewUnitID1.Value = intNewUnitID

            Dim prmOldItemID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@OldItemID", SqlDbType.Int)
            prmOldItemID1.Value = intOldItemID

            Dim prmOldUnitID1 As SqlParameter = cmdCopyLIQuan.Parameters.Add("@OldUnitID", SqlDbType.Int)
            prmOldUnitID1.Value = intOldUnitID
            Try
                If cnn1.State = ConnectionState.Closed Then
                    cnn1.Open()
                End If
                cmdCopyLIQuan.ExecuteNonQuery()
                cnn1.Close()
            Catch ex As Exception
                MsgBox("There was an error copying this record!!")
            Finally
                If cnn1.State = ConnectionState.Open Then
                    cnn1.Close()
                End If
            End Try




        End While
        drItems.Close()
        cnn.Close()



    End Sub
End Class
