Imports System.Data
Imports System.Data.SqlClient
Public Class frmEditCatMain
    Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents dgCatMain As System.Windows.Forms.DataGrid
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents dgCatMainStyle As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents ProductID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents VendorCode As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents VendorPartNo As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents StandardCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents daCatMain As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents Dscatmain1 As LD_Bid.dscatmain
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents ProductNum As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents AverageCost As System.Windows.Forms.DataGridTextBoxColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.dgCatMain = New System.Windows.Forms.DataGrid
        Me.Dscatmain1 = New LD_Bid.dscatmain
        Me.dgCatMainStyle = New System.Windows.Forms.DataGridTableStyle
        Me.ProductNum = New System.Windows.Forms.DataGridTextBoxColumn
        Me.ProductID = New System.Windows.Forms.DataGridTextBoxColumn
        Me.Description = New System.Windows.Forms.DataGridTextBoxColumn
        Me.VendorCode = New System.Windows.Forms.DataGridTextBoxColumn
        Me.VendorPartNo = New System.Windows.Forms.DataGridTextBoxColumn
        Me.StandardCost = New System.Windows.Forms.DataGridTextBoxColumn
        Me.AverageCost = New System.Windows.Forms.DataGridTextBoxColumn
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.daCatMain = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.dgCatMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dscatmain1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgCatMain
        '
        Me.dgCatMain.DataMember = "CatalogMain"
        Me.dgCatMain.DataSource = Me.Dscatmain1
        Me.dgCatMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgCatMain.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgCatMain.Location = New System.Drawing.Point(0, 0)
        Me.dgCatMain.Name = "dgCatMain"
        Me.dgCatMain.Size = New System.Drawing.Size(744, 422)
        Me.dgCatMain.TabIndex = 0
        Me.dgCatMain.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgCatMainStyle})
        '
        'Dscatmain1
        '
        Me.Dscatmain1.DataSetName = "dscatmain"
        Me.Dscatmain1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'dgCatMainStyle
        '
        Me.dgCatMainStyle.DataGrid = Me.dgCatMain
        Me.dgCatMainStyle.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.ProductNum, Me.ProductID, Me.Description, Me.VendorCode, Me.VendorPartNo, Me.StandardCost, Me.AverageCost})
        Me.dgCatMainStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgCatMainStyle.MappingName = "CatalogMain"
        '
        'ProductNum
        '
        Me.ProductNum.Format = ""
        Me.ProductNum.FormatInfo = Nothing
        Me.ProductNum.MappingName = "ProdNum"
        Me.ProductNum.ReadOnly = True
        Me.ProductNum.Width = 75
        '
        'ProductID
        '
        Me.ProductID.Format = ""
        Me.ProductID.FormatInfo = Nothing
        Me.ProductID.HeaderText = "ID"
        Me.ProductID.MappingName = "ProductID"
        Me.ProductID.NullText = ""
        Me.ProductID.ReadOnly = True
        Me.ProductID.Width = 90
        '
        'Description
        '
        Me.Description.Format = ""
        Me.Description.FormatInfo = Nothing
        Me.Description.HeaderText = "Description"
        Me.Description.MappingName = "Description"
        Me.Description.NullText = ""
        Me.Description.Width = 150
        '
        'VendorCode
        '
        Me.VendorCode.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.VendorCode.Format = ""
        Me.VendorCode.FormatInfo = Nothing
        Me.VendorCode.HeaderText = "Vendor Code"
        Me.VendorCode.MappingName = "VendorCode"
        Me.VendorCode.NullText = ""
        Me.VendorCode.Width = 75
        '
        'VendorPartNo
        '
        Me.VendorPartNo.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.VendorPartNo.Format = ""
        Me.VendorPartNo.FormatInfo = Nothing
        Me.VendorPartNo.HeaderText = "Vendor #"
        Me.VendorPartNo.MappingName = "VendorPartNo"
        Me.VendorPartNo.NullText = ""
        Me.VendorPartNo.Width = 75
        '
        'StandardCost
        '
        Me.StandardCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.StandardCost.Format = "C"
        Me.StandardCost.FormatInfo = Nothing
        Me.StandardCost.HeaderText = "Cost"
        Me.StandardCost.MappingName = "StandardCost"
        Me.StandardCost.NullText = "0"
        Me.StandardCost.Width = 60
        '
        'AverageCost
        '
        Me.AverageCost.Format = "C"
        Me.AverageCost.FormatInfo = Nothing
        Me.AverageCost.HeaderText = "Avg Cost"
        Me.AverageCost.MappingName = "AverageCost"
        Me.AverageCost.Width = 75
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(4, 0)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(84, 20)
        Me.btnUpdate.TabIndex = 1
        Me.btnUpdate.Text = "Update"
        '
        'daCatMain
        '
        Me.daCatMain.ContinueUpdateOnError = True
        Me.daCatMain.InsertCommand = Me.SqlInsertCommand1
        Me.daCatMain.SelectCommand = Me.SqlSelectCommand1
        Me.daCatMain.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "CatalogMain", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ProdNum", "ProdNum"), New System.Data.Common.DataColumnMapping("ProductID", "ProductID"), New System.Data.Common.DataColumnMapping("Description", "Description"), New System.Data.Common.DataColumnMapping("VendorCode", "VendorCode"), New System.Data.Common.DataColumnMapping("VendorPartNo", "VendorPartNo"), New System.Data.Common.DataColumnMapping("AverageCost", "AverageCost"), New System.Data.Common.DataColumnMapping("StandardCost", "StandardCost")})})
        Me.daCatMain.UpdateCommand = Me.SqlCommand1
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO CatalogMain(ProductID, Description, VendorCode, VendorPartNo, Average" & _
        "Cost, StandardCost) VALUES (@ProductID, @Description, @VendorCode, @VendorPartNo" & _
        ", @AverageCost, @StandardCost); SELECT ProdNum, ProductID, Description, VendorCo" & _
        "de, VendorPartNo, AverageCost, StandardCost FROM CatalogMain WHERE (ProdNum = @@" & _
        "IDENTITY) ORDER BY ProductID"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ProductID", System.Data.SqlDbType.NVarChar, 255, "ProductID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 255, "Description"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VendorCode", System.Data.SqlDbType.NVarChar, 255, "VendorCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@VendorPartNo", System.Data.SqlDbType.NVarChar, 255, "VendorPartNo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AverageCost", System.Data.SqlDbType.Float, 8, "AverageCost"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StandardCost", System.Data.SqlDbType.Float, 8, "StandardCost"))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=""CO-DEV-WS"";packet size=4096;user id=SA;data source=""CO-TECH-LT-00" & _
        "3"";persist security info=True;initial catalog=""LD-Bid"";password=vw9110"
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT ProdNum, ProductID, Description, VendorCode, VendorPartNo, AverageCost, St" & _
        "andardCost FROM CatalogMain ORDER BY ProductID"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'frmEditCatMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(744, 422)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.dgCatMain)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditCatMain"
        Me.Text = "Main Catalog"
        CType(Me.dgCatMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dscatmain1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmEditCatMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = frmMain.strConnect

        daCatMain.Fill(DsCatMain1)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        daCatMain.Update(DsCatMain1)
    End Sub
End Class
