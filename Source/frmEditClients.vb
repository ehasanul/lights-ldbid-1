Public Class frmEditClients
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents dgClients As System.Windows.Forms.DataGrid
    Friend WithEvents daClients As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DsClients1 As LD_Bid.dsClients
    Friend WithEvents dgClientsStyle As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents rwClientID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwAddress1 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwAddress2 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwCity As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwState As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwZIP As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwPhone1 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwPhone2 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwFAX As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwemail As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwContact As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwTE As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.dgClients = New System.Windows.Forms.DataGrid
        Me.DsClients1 = New LD_Bid.dsClients
        Me.dgClientsStyle = New System.Windows.Forms.DataGridTableStyle
        Me.rwClientID = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwName = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwContact = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwAddress1 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwAddress2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwCity = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwState = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwZIP = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwPhone1 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwPhone2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwFAX = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwemail = New System.Windows.Forms.DataGridTextBoxColumn
        Me.rwTE = New System.Windows.Forms.DataGridTextBoxColumn
        Me.daClients = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.btnUpdate = New System.Windows.Forms.Button
        CType(Me.dgClients, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsClients1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgClients
        '
        Me.dgClients.DataMember = "Clients"
        Me.dgClients.DataSource = Me.DsClients1
        Me.dgClients.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgClients.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgClients.Location = New System.Drawing.Point(0, 0)
        Me.dgClients.Name = "dgClients"
        Me.dgClients.Size = New System.Drawing.Size(888, 394)
        Me.dgClients.TabIndex = 0
        Me.dgClients.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgClientsStyle})
        '
        'DsClients1
        '
        Me.DsClients1.DataSetName = "dsClients"
        Me.DsClients1.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'dgClientsStyle
        '
        Me.dgClientsStyle.DataGrid = Me.dgClients
        Me.dgClientsStyle.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.rwClientID, Me.rwName, Me.rwContact, Me.rwAddress1, Me.rwAddress2, Me.rwCity, Me.rwState, Me.rwZIP, Me.rwPhone1, Me.rwPhone2, Me.rwFAX, Me.rwemail, Me.rwTE})
        Me.dgClientsStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgClientsStyle.MappingName = "Clients"
        '
        'rwClientID
        '
        Me.rwClientID.Format = ""
        Me.rwClientID.FormatInfo = Nothing
        Me.rwClientID.MappingName = "ClientID"
        Me.rwClientID.Width = 0
        '
        'rwName
        '
        Me.rwName.Format = ""
        Me.rwName.FormatInfo = Nothing
        Me.rwName.HeaderText = "Name"
        Me.rwName.MappingName = "Name"
        Me.rwName.NullText = ""
        Me.rwName.Width = 175
        '
        'rwContact
        '
        Me.rwContact.Format = ""
        Me.rwContact.FormatInfo = Nothing
        Me.rwContact.HeaderText = "Contact"
        Me.rwContact.MappingName = "contact"
        Me.rwContact.NullText = ""
        Me.rwContact.Width = 150
        '
        'rwAddress1
        '
        Me.rwAddress1.Format = ""
        Me.rwAddress1.FormatInfo = Nothing
        Me.rwAddress1.HeaderText = "Address 1"
        Me.rwAddress1.MappingName = "Address1"
        Me.rwAddress1.NullText = ""
        Me.rwAddress1.Width = 125
        '
        'rwAddress2
        '
        Me.rwAddress2.Format = ""
        Me.rwAddress2.FormatInfo = Nothing
        Me.rwAddress2.HeaderText = "Address 2"
        Me.rwAddress2.MappingName = "Address2"
        Me.rwAddress2.NullText = ""
        Me.rwAddress2.Width = 75
        '
        'rwCity
        '
        Me.rwCity.Format = ""
        Me.rwCity.FormatInfo = Nothing
        Me.rwCity.HeaderText = "City"
        Me.rwCity.MappingName = "city"
        Me.rwCity.NullText = ""
        Me.rwCity.Width = 125
        '
        'rwState
        '
        Me.rwState.Format = ""
        Me.rwState.FormatInfo = Nothing
        Me.rwState.HeaderText = "ST"
        Me.rwState.MappingName = "State"
        Me.rwState.NullText = ""
        Me.rwState.Width = 25
        '
        'rwZIP
        '
        Me.rwZIP.Format = ""
        Me.rwZIP.FormatInfo = Nothing
        Me.rwZIP.HeaderText = "ZIP"
        Me.rwZIP.MappingName = "Zip"
        Me.rwZIP.NullText = ""
        Me.rwZIP.Width = 60
        '
        'rwPhone1
        '
        Me.rwPhone1.Format = ""
        Me.rwPhone1.FormatInfo = Nothing
        Me.rwPhone1.HeaderText = "Phone 1"
        Me.rwPhone1.MappingName = "Phone1"
        Me.rwPhone1.NullText = ""
        Me.rwPhone1.Width = 125
        '
        'rwPhone2
        '
        Me.rwPhone2.Format = ""
        Me.rwPhone2.FormatInfo = Nothing
        Me.rwPhone2.HeaderText = "Phone 2"
        Me.rwPhone2.MappingName = "Phone2"
        Me.rwPhone2.NullText = ""
        Me.rwPhone2.Width = 125
        '
        'rwFAX
        '
        Me.rwFAX.Format = ""
        Me.rwFAX.FormatInfo = Nothing
        Me.rwFAX.HeaderText = "FAX"
        Me.rwFAX.MappingName = "FAX"
        Me.rwFAX.NullText = ""
        Me.rwFAX.Width = 125
        '
        'rwemail
        '
        Me.rwemail.Format = ""
        Me.rwemail.FormatInfo = Nothing
        Me.rwemail.HeaderText = "E-Mail"
        Me.rwemail.MappingName = "email"
        Me.rwemail.NullText = ""
        Me.rwemail.Width = 150
        '
        'rwTE
        '
        Me.rwTE.Format = "Yes/No"
        Me.rwTE.FormatInfo = Nothing
        Me.rwTE.HeaderText = "TE"
        Me.rwTE.MappingName = "taxexempt"
        Me.rwTE.NullText = ""
        Me.rwTE.Width = 50
        '
        'daClients
        '
        Me.daClients.DeleteCommand = Me.SqlDeleteCommand1
        Me.daClients.InsertCommand = Me.SqlInsertCommand1
        Me.daClients.SelectCommand = Me.SqlSelectCommand1
        Me.daClients.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Clients", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ClientID", "ClientID"), New System.Data.Common.DataColumnMapping("Name", "Name"), New System.Data.Common.DataColumnMapping("Address1", "Address1"), New System.Data.Common.DataColumnMapping("Address2", "Address2"), New System.Data.Common.DataColumnMapping("city", "city"), New System.Data.Common.DataColumnMapping("State", "State"), New System.Data.Common.DataColumnMapping("Zip", "Zip"), New System.Data.Common.DataColumnMapping("Phone1", "Phone1"), New System.Data.Common.DataColumnMapping("Phone2", "Phone2"), New System.Data.Common.DataColumnMapping("FAX", "FAX"), New System.Data.Common.DataColumnMapping("email", "email"), New System.Data.Common.DataColumnMapping("contact", "contact"), New System.Data.Common.DataColumnMapping("taxexempt", "taxexempt")})})
        Me.daClients.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Clients WHERE (ClientID = @Original_ClientID) AND (Address1 = @Origin" & _
        "al_Address1 OR @Original_Address1 IS NULL AND Address1 IS NULL) AND (Address2 = " & _
        "@Original_Address2 OR @Original_Address2 IS NULL AND Address2 IS NULL) AND (FAX " & _
        "= @Original_FAX OR @Original_FAX IS NULL AND FAX IS NULL) AND (Name = @Original_" & _
        "Name) AND (Phone1 = @Original_Phone1 OR @Original_Phone1 IS NULL AND Phone1 IS N" & _
        "ULL) AND (Phone2 = @Original_Phone2 OR @Original_Phone2 IS NULL AND Phone2 IS NU" & _
        "LL) AND (State = @Original_State OR @Original_State IS NULL AND State IS NULL) A" & _
        "ND (Zip = @Original_Zip OR @Original_Zip IS NULL AND Zip IS NULL) AND (city = @O" & _
        "riginal_city OR @Original_city IS NULL AND city IS NULL) AND (contact = @Origina" & _
        "l_contact OR @Original_contact IS NULL AND contact IS NULL) AND (email = @Origin" & _
        "al_email OR @Original_email IS NULL AND email IS NULL) AND (taxexempt = @Origina" & _
        "l_taxexempt OR @Original_taxexempt IS NULL AND taxexempt IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ClientID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ClientID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Address1", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Address1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Address2", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Address2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FAX", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FAX", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Name", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Phone1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Phone1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Phone2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Phone2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_State", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "State", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Zip", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Zip", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_city", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "city", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_contact", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "contact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_email", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_taxexempt", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "taxexempt", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Clients(Name, Address1, Address2, city, State, Zip, Phone1, Phone2, F" & _
        "AX, email, contact, taxexempt) VALUES (@Name, @Address1, @Address2, @city, @Stat" & _
        "e, @Zip, @Phone1, @Phone2, @FAX, @email, @contact, @taxexempt); SELECT ClientID," & _
        " Name, Address1, Address2, city, State, Zip, Phone1, Phone2, FAX, email, contact" & _
        ", taxexempt FROM Clients WHERE (ClientID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.NVarChar, 50, "Name"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Address1", System.Data.SqlDbType.NVarChar, 50, "Address1"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Address2", System.Data.SqlDbType.NVarChar, 50, "Address2"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@city", System.Data.SqlDbType.NVarChar, 50, "city"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@State", System.Data.SqlDbType.VarChar, 2, "State"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Zip", System.Data.SqlDbType.VarChar, 10, "Zip"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Phone1", System.Data.SqlDbType.VarChar, 15, "Phone1"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Phone2", System.Data.SqlDbType.VarChar, 15, "Phone2"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FAX", System.Data.SqlDbType.VarChar, 15, "FAX"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.NVarChar, 50, "email"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@contact", System.Data.SqlDbType.NVarChar, 50, "contact"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@taxexempt", System.Data.SqlDbType.Bit, 1, "taxexempt"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT ClientID, Name, Address1, Address2, city, State, Zip, Phone1, Phone2, FAX," & _
        " email, contact, taxexempt FROM Clients"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Clients SET Name = @Name, Address1 = @Address1, Address2 = @Address2, city" & _
        " = @city, State = @State, Zip = @Zip, Phone1 = @Phone1, Phone2 = @Phone2, FAX = " & _
        "@FAX, email = @email, contact = @contact, taxexempt = @taxexempt WHERE (ClientID" & _
        " = @Original_ClientID) AND (Address1 = @Original_Address1 OR @Original_Address1 " & _
        "IS NULL AND Address1 IS NULL) AND (Address2 = @Original_Address2 OR @Original_Ad" & _
        "dress2 IS NULL AND Address2 IS NULL) AND (FAX = @Original_FAX OR @Original_FAX I" & _
        "S NULL AND FAX IS NULL) AND (Name = @Original_Name) AND (Phone1 = @Original_Phon" & _
        "e1 OR @Original_Phone1 IS NULL AND Phone1 IS NULL) AND (Phone2 = @Original_Phone" & _
        "2 OR @Original_Phone2 IS NULL AND Phone2 IS NULL) AND (State = @Original_State O" & _
        "R @Original_State IS NULL AND State IS NULL) AND (Zip = @Original_Zip OR @Origin" & _
        "al_Zip IS NULL AND Zip IS NULL) AND (city = @Original_city OR @Original_city IS " & _
        "NULL AND city IS NULL) AND (contact = @Original_contact OR @Original_contact IS " & _
        "NULL AND contact IS NULL) AND (email = @Original_email OR @Original_email IS NUL" & _
        "L AND email IS NULL) AND (taxexempt = @Original_taxexempt OR @Original_taxexempt" & _
        " IS NULL AND taxexempt IS NULL); SELECT ClientID, Name, Address1, Address2, city" & _
        ", State, Zip, Phone1, Phone2, FAX, email, contact, taxexempt FROM Clients WHERE " & _
        "(ClientID = @ClientID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.NVarChar, 50, "Name"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Address1", System.Data.SqlDbType.NVarChar, 50, "Address1"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Address2", System.Data.SqlDbType.NVarChar, 50, "Address2"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@city", System.Data.SqlDbType.NVarChar, 50, "city"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@State", System.Data.SqlDbType.VarChar, 2, "State"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Zip", System.Data.SqlDbType.VarChar, 10, "Zip"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Phone1", System.Data.SqlDbType.VarChar, 15, "Phone1"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Phone2", System.Data.SqlDbType.VarChar, 15, "Phone2"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FAX", System.Data.SqlDbType.VarChar, 15, "FAX"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@email", System.Data.SqlDbType.NVarChar, 50, "email"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@contact", System.Data.SqlDbType.NVarChar, 50, "contact"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@taxexempt", System.Data.SqlDbType.Bit, 1, "taxexempt"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ClientID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ClientID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Address1", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Address1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Address2", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Address2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_FAX", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "FAX", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Name", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Phone1", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Phone1", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Phone2", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Phone2", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_State", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "State", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Zip", System.Data.SqlDbType.VarChar, 10, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Zip", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_city", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "city", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_contact", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "contact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_email", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_taxexempt", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "taxexempt", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ClientID", System.Data.SqlDbType.Int, 4, "ClientID"))
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(4, 0)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 20)
        Me.btnUpdate.TabIndex = 1
        Me.btnUpdate.Text = "Update"
        '
        'frmEditClients
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(888, 394)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.dgClients)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditClients"
        Me.Text = "Clients"
        CType(Me.dgClients, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsClients1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmEditClients_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlConnection1.ConnectionString = frmMain.strConnect
        daClients.Fill(DsClients1)
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        daClients.Update(DsClients1)
    End Sub

    Private Sub frmEditClients_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    End Sub
End Class
