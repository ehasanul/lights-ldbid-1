Imports System.Data
Imports System.Data.SqlClient
Public Class frmj_UpdateShipping
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDays As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDays = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(404, 28)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "This module will add the requested number of days to all Buildings and Line Items" & _
        " in this Job!!"
        '
        'txtDays
        '
        Me.txtDays.Location = New System.Drawing.Point(208, 52)
        Me.txtDays.Name = "txtDays"
        Me.txtDays.Size = New System.Drawing.Size(36, 20)
        Me.txtDays.TabIndex = 1
        Me.txtDays.Text = ""
        Me.txtDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(24, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(172, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Enter a Whole # of Days To Add"
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(264, 52)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(156, 24)
        Me.btnUpdate.TabIndex = 3
        Me.btnUpdate.Text = "Modify Shipping Schedules"
        '
        'frmj_UpdateShipping
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(436, 102)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDays)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmj_UpdateShipping"
        Me.Text = "Update Shipping Dates"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub UpdateShipping()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdEditShipDate As New SqlCommand("UpdateShipping", cnn)
        cmdEditShipDate.CommandType = CommandType.StoredProcedure

        Dim prmJobID As SqlParameter = cmdEditShipDate.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID

        Dim prmDateChange As SqlParameter = cmdEditShipDate.Parameters.Add("@DateChange", SqlDbType.Int)
        prmDateChange.Value = CInt(txtDays.Text)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        cmdEditShipDate.ExecuteNonQuery()

        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        'End Try
        Me.Close()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If Not IsNumeric(txtDays.Text) Then
            MsgBox("You must enter a numeric whole number of days!")
            Exit Sub
        End If
        Try
            Dim intTemp As Integer
            intTemp = CInt(txtDays.Text)
            'MsgBox(intTemp.ToString)
        Catch ex As Exception
            MsgBox("You must enter an Integer for the number of days!")
            Exit Sub
        End Try
        If MsgBox("You are about to change all shipping dates!" & vbCrLf & "Do you wish to proceed?", MsgBoxStyle.YesNo, "Change Em") = MsgBoxResult.Yes Then
            UpdateShipping()
            Me.Close()
        End If


    End Sub
End Class
