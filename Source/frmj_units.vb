Imports System.Data
Imports System.Data.SqlClient
Public Class frmj_units
    Inherits System.Windows.Forms.Form

    Dim cnn As New SqlConnection ' Primary Connection
    Dim blnStartUnit As Boolean ' Tests Startup Condition
    Dim strFunction As String
    Dim strOldUnitDesc As String
    Dim strOldUnitName As String
    Dim intNewUnitID As Integer
    Dim intOldUnitID As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents lblUnitDesc As System.Windows.Forms.Label
    Friend WithEvents lblUnitType As System.Windows.Forms.Label
    Friend WithEvents txtBUnitDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtBUnitType As System.Windows.Forms.TextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtUnitName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuEdit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnDelete = New System.Windows.Forms.Button
        Me.lblUnitDesc = New System.Windows.Forms.Label
        Me.lblUnitType = New System.Windows.Forms.Label
        Me.txtBUnitDescription = New System.Windows.Forms.TextBox
        Me.txtBUnitType = New System.Windows.Forms.TextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtUnitName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mnuEdit = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.SuspendLayout()
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(184, 112)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(44, 16)
        Me.btnDelete.TabIndex = 17
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.Visible = False
        '
        'lblUnitDesc
        '
        Me.lblUnitDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitDesc.Location = New System.Drawing.Point(28, 84)
        Me.lblUnitDesc.Name = "lblUnitDesc"
        Me.lblUnitDesc.Size = New System.Drawing.Size(64, 20)
        Me.lblUnitDesc.TabIndex = 15
        Me.lblUnitDesc.Text = "Description"
        '
        'lblUnitType
        '
        Me.lblUnitType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitType.Location = New System.Drawing.Point(28, 60)
        Me.lblUnitType.Name = "lblUnitType"
        Me.lblUnitType.Size = New System.Drawing.Size(56, 16)
        Me.lblUnitType.TabIndex = 14
        Me.lblUnitType.Text = "Type"
        '
        'txtBUnitDescription
        '
        Me.txtBUnitDescription.Location = New System.Drawing.Point(100, 80)
        Me.txtBUnitDescription.Name = "txtBUnitDescription"
        Me.txtBUnitDescription.Size = New System.Drawing.Size(216, 20)
        Me.txtBUnitDescription.TabIndex = 13
        Me.txtBUnitDescription.Text = ""
        '
        'txtBUnitType
        '
        Me.txtBUnitType.Location = New System.Drawing.Point(100, 60)
        Me.txtBUnitType.Name = "txtBUnitType"
        Me.txtBUnitType.Size = New System.Drawing.Size(136, 20)
        Me.txtBUnitType.TabIndex = 12
        Me.txtBUnitType.Text = ""
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(272, 112)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(44, 16)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(228, 112)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(44, 16)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "Save"
        Me.btnSave.Visible = False
        '
        'txtUnitName
        '
        Me.txtUnitName.Location = New System.Drawing.Point(100, 40)
        Me.txtUnitName.Name = "txtUnitName"
        Me.txtUnitName.Size = New System.Drawing.Size(136, 20)
        Me.txtUnitName.TabIndex = 18
        Me.txtUnitName.Text = ""
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(28, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 20)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Name"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEdit, Me.mnuCopy})
        '
        'mnuEdit
        '
        Me.mnuEdit.Index = 0
        Me.mnuEdit.Text = "Edit"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 1
        Me.mnuCopy.Text = "Copy"
        '
        'frmj_units
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(332, 134)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtUnitName)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblUnitDesc)
        Me.Controls.Add(Me.lblUnitType)
        Me.Controls.Add(Me.txtBUnitDescription)
        Me.Controls.Add(Me.txtBUnitType)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmj_units"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Units"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadUnitDetails()

        cnn.ConnectionString = frmMain.strConnect

        Dim strUnitInfo As String
        strUnitInfo = "SELECT UnitType, UnitName, Description FROM Unit WHERE UnitID = "
        strUnitInfo = strUnitInfo & frmJob.intUnitID.ToString
        Dim cmdUnitInfo As New SqlCommand(strUnitInfo, cnn)
        cmdUnitInfo.CommandType = CommandType.Text
        Dim drUnitInfo As SqlDataReader
        cnn.Open()
        drUnitInfo = cmdUnitInfo.ExecuteReader
        drUnitInfo.Read()
        txtUnitName.Text = drUnitInfo.Item("UnitName").ToString
        strOldUnitName = txtUnitName.Text
        txtBUnitType.Text = drUnitInfo.Item("UnitType").ToString
        txtBUnitDescription.Text = drUnitInfo.Item("Description").ToString
        strOldUnitDesc = txtBUnitDescription.Text
        drUnitInfo.Close()
        cnn.Close()
    End Sub

    Private Sub frmj_units_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadUnitDetails()
    End Sub

    Private Sub EditUnit()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "UPDATE Unit SET "
        'strSqlCommand = strSqlCommand & Trim(txtBUnitType.Text)
        strSqlCommand = strSqlCommand & "UnitName = '" & Trim(txtUnitName.Text)
        strSqlCommand = strSqlCommand & "', Description = '" & Trim(txtBUnitDescription.Text)
        strSqlCommand = strSqlCommand & "' WHERE UnitID = " & frmJob.intUnitID
        Dim cmdItems As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdItems.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!")
        Finally
            cnn.Close()

        End Try
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strSample As String
        strSample = txtBUnitType.Text.Substring(0, 1)
        If (System.Convert.ToInt32(strSample.IndexOfAny("UCXSA".ToCharArray)) = -1) Then
            MsgBox("The Unit Type Must Begin With either U,C,X,S or A")
            Exit Sub
        End If
        Select Case strFunction
            Case "New"
                
            Case "Edit"
                EditUnit()
                frmJob.blnUpdateUnit = True
            Case "Copy"
                If strOldUnitName = txtUnitName.Text Then
                    MsgBox("You must enter a new Unit Name!!")
                    Exit Sub
                End If
                CopyUnit()
                frmJob.blnUpdateUnit = True
                Me.Close()

        End Select
        EnableMenu()
        'HideAll()
        'ClearAll()
        'frmBid.blnStartUnit = True
        'frmBid.blnUnitReturn = True
    End Sub

    Private Sub DeleteUnits()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "DELETE FROM Unit WHERE UnitID = " & frmJob.intUnitID
        Dim cmdDeleteUnit As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdDeleteUnit.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error Deleting This Unit!")
        Finally
            cnn.Close()
        End Try



    End Sub

    Private Sub CopyUnit()

        intOldUnitID = frmJob.intUnitID
        Dim cnnU2 As New SqlConnection
        cnnU2.ConnectionString = frmMain.strConnect

        ''Unit

        Dim cmdAddUnit As New SqlCommand("AddUnit", cnnU2)
        cmdAddUnit.CommandType = CommandType.StoredProcedure

        Dim prmUJobID As SqlParameter = cmdAddUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUJobID.Value = frmMain.intJobID

        Dim prmBldgID As SqlParameter = cmdAddUnit.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = frmJob.intBldgID

        Dim prmUnitName As SqlParameter = cmdAddUnit.Parameters.Add("@UnitName", SqlDbType.NVarChar, 50)
        Dim strUnitName As String
        strUnitName = txtUnitName.Text
        prmUnitName.Value = strUnitName


        Dim prmUType As SqlParameter = cmdAddUnit.Parameters.Add("@UnitType", SqlDbType.Char, 10)
        prmUType.Value = txtBUnitType.Text

        Dim prmUDescription As SqlParameter = cmdAddUnit.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmUDescription.Value = txtBUnitDescription.Text


        Dim prmNewUnitID As SqlParameter = cmdAddUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
        prmNewUnitID.Direction = ParameterDirection.Output

        Try
            cnnU2.Open()

            cmdAddUnit.ExecuteNonQuery()
            cnnU2.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this Unit!!")
        Finally
            If cnnU2.State = ConnectionState.Open Then
                cnnU2.Close()
            End If
        End Try
        intNewUnitID = System.Convert.ToInt32(prmNewUnitID.Value)

        'Add Unit Line Items

        Dim strLineItems As String
        strLineItems = "SELECT RecordSource, PartNumber, RT, Description," & _
            "Cost, Margin, Sell, Quantity, Location, Type, Shipdate " & _
            "FROM LineItem Where UnitID = " & intOldUnitID
        Dim cnnL1 As New SqlConnection
        cnnL1.ConnectionString = frmMain.strConnect

        Dim cmdLineItems As New SqlCommand(strLineItems, cnnL1)
        cmdLineItems.CommandType = CommandType.Text

        Dim drLineItems As SqlDataReader
        cnnL1.Open()

        drLineItems = cmdLineItems.ExecuteReader
        While drLineItems.Read

            Dim cnnL2 As New SqlConnection
            cnnL2.ConnectionString = frmMain.strConnect

            ''LineItem

            Dim cmdAddLineItem As New SqlCommand("AddLineItem", cnnL2)
            cmdAddLineItem.CommandType = CommandType.StoredProcedure

            Dim prmLJobID As SqlParameter = cmdAddLineItem.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmLJobID.Value = frmMain.intJobID

            Dim prmLBldgID As SqlParameter = cmdAddLineItem.Parameters.Add("@BldgID", SqlDbType.Int)
            prmLBldgID.Value = frmJob.intBldgID

            Dim prmLUnitID As SqlParameter = cmdAddLineItem.Parameters.Add("@UnitID", SqlDbType.Int)
            prmLUnitID.Value = intNewUnitID

            Dim prmRecordSource As SqlParameter = cmdAddLineItem.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
            prmRecordSource.Value = drLineItems.Item("RecordSource")

            Dim prmPartNumber As SqlParameter = cmdAddLineItem.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
            prmPartNumber.Value = drLineItems.Item("PartNumber")

            Dim prmRT As SqlParameter = cmdAddLineItem.Parameters.Add("@RT", SqlDbType.Char, 1)
            prmRT.Value = drLineItems.Item("RT")

            Dim prmLDescription As SqlParameter = cmdAddLineItem.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
            prmLDescription.Value = drLineItems.Item("Description")

            Dim prmCost As SqlParameter = cmdAddLineItem.Parameters.Add("@Cost", SqlDbType.Money)
            prmCost.Value = drLineItems.Item("Cost")

            Dim prmMargin As SqlParameter = cmdAddLineItem.Parameters.Add("@Margin", SqlDbType.Int)
            prmMargin.Value = drLineItems.Item("Margin")

            Dim prmSell As SqlParameter = cmdAddLineItem.Parameters.Add("@Sell", SqlDbType.Money)
            prmSell.Value = drLineItems.Item("Sell")

            Dim prmQuantity As SqlParameter = cmdAddLineItem.Parameters.Add("@Quantity", SqlDbType.Int)
            prmQuantity.Value = drLineItems.Item("Quantity")

            Dim prmLLocation As SqlParameter = cmdAddLineItem.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
            prmLLocation.Value = drLineItems.Item("Location")

            Dim prmLType As SqlParameter = cmdAddLineItem.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
            prmLType.Value = drLineItems.Item("Type")

            Dim prmLShipDate As SqlParameter = cmdAddLineItem.Parameters.Add("@ShipDate", SqlDbType.DateTime)
            prmLShipDate.Value = drLineItems.Item("ShipDate")

            Try
                cnnL2.Open()

                cmdAddLineItem.ExecuteNonQuery()
                cnnL2.Close()
            Catch ex As Exception
                MsgBox("There was an error adding this LineItems!!")
            Finally
                If cnnL2.State = ConnectionState.Open Then
                    cnnL2.Close()
                End If
            End Try

        End While
        drLineItems.Close()
        cnnL1.Close()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        strFunction = "Copy"
        DisableMenu()
        ShowButtons()
        btnDelete.Visible = False
    End Sub

    Private Sub DisableMenu()
        mnuCopy.Visible = False
        mnuEdit.Visible = False

    End Sub

    Private Sub EnableMenu()
        mnuCopy.Visible = True
        mnuEdit.Visible = True

    End Sub

    Private Sub mnuEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEdit.Click
        strFunction = "Edit"

        DisableMenu()
        ShowButtons()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteUnits()
        Me.Close()
        EnableMenu()
    End Sub

    Private Sub ShowButtons()
        btnSave.Visible = True
        btnCancel.Visible = True
        btnDelete.Visible = True
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub
End Class
