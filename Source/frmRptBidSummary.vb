Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmRptBidSummary
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim strUnits As String
    Dim strCommon As String
    Dim strSite As String
    Dim strClubhouse As String
    Dim strAuxiliary As String
    Dim strAuxLabel As String
    Dim strPreTaxTotal As String
    Dim strTaxRate As String
    Dim strTaxTotal As String
    Dim strGrandTotal As String
    Dim strrptUnitCount As String
    Dim strrptBldgCount As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents btnShowReport As System.Windows.Forms.Button
    Friend WithEvents cbxSummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxComments As System.Windows.Forms.CheckBox
    Friend WithEvents cbxType As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUnitSummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCommonSummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClubHouseSummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSiteSummayr As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUnitDetails As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCommonDetails As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAlternates As System.Windows.Forms.CheckBox
    Friend WithEvents cbxDescriptions As System.Windows.Forms.CheckBox
    Friend WithEvents cboOrder As System.Windows.Forms.ComboBox
    Friend WithEvents lblSections As System.Windows.Forms.Label
    Friend WithEvents lblOrder As System.Windows.Forms.Label
    Friend WithEvents cbxBldgType As System.Windows.Forms.CheckBox
    Friend WithEvents cbxPrices As System.Windows.Forms.CheckBox
    Friend WithEvents lblColumns As System.Windows.Forms.Label
    Friend WithEvents cbxGrandTotals As System.Windows.Forms.CheckBox
    Friend WithEvents cbxPageTwoTotals As System.Windows.Forms.CheckBox
    Friend WithEvents lblPageTwoTotals As System.Windows.Forms.Label
    Friend WithEvents cbxAuxiliarySummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBldgBOM As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClubhouseDetails As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSiteDetails As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAuxDetails As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTBD As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.btnShowReport = New System.Windows.Forms.Button
        Me.cbxSummary = New System.Windows.Forms.CheckBox
        Me.cbxComments = New System.Windows.Forms.CheckBox
        Me.cbxType = New System.Windows.Forms.CheckBox
        Me.cbxUnitSummary = New System.Windows.Forms.CheckBox
        Me.cbxCommonSummary = New System.Windows.Forms.CheckBox
        Me.cbxClubHouseSummary = New System.Windows.Forms.CheckBox
        Me.cbxSiteSummayr = New System.Windows.Forms.CheckBox
        Me.cbxUnitDetails = New System.Windows.Forms.CheckBox
        Me.cbxCommonDetails = New System.Windows.Forms.CheckBox
        Me.cbxAlternates = New System.Windows.Forms.CheckBox
        Me.cbxDescriptions = New System.Windows.Forms.CheckBox
        Me.cboOrder = New System.Windows.Forms.ComboBox
        Me.lblSections = New System.Windows.Forms.Label
        Me.lblOrder = New System.Windows.Forms.Label
        Me.cbxBldgType = New System.Windows.Forms.CheckBox
        Me.cbxPrices = New System.Windows.Forms.CheckBox
        Me.lblColumns = New System.Windows.Forms.Label
        Me.cbxGrandTotals = New System.Windows.Forms.CheckBox
        Me.cbxPageTwoTotals = New System.Windows.Forms.CheckBox
        Me.lblPageTwoTotals = New System.Windows.Forms.Label
        Me.cbxAuxiliarySummary = New System.Windows.Forms.CheckBox
        Me.cbxBldgBOM = New System.Windows.Forms.CheckBox
        Me.cbxClubhouseDetails = New System.Windows.Forms.CheckBox
        Me.cbxSiteDetails = New System.Windows.Forms.CheckBox
        Me.cbxAuxDetails = New System.Windows.Forms.CheckBox
        Me.cbxTBD = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(448, 292)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(148, 44)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.Visible = False
        '
        'btnShowReport
        '
        Me.btnShowReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowReport.Location = New System.Drawing.Point(496, 292)
        Me.btnShowReport.Name = "btnShowReport"
        Me.btnShowReport.Size = New System.Drawing.Size(112, 20)
        Me.btnShowReport.TabIndex = 1
        Me.btnShowReport.Text = "Show Report"
        '
        'cbxSummary
        '
        Me.cbxSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSummary.Location = New System.Drawing.Point(40, 252)
        Me.cbxSummary.Name = "cbxSummary"
        Me.cbxSummary.Size = New System.Drawing.Size(156, 20)
        Me.cbxSummary.TabIndex = 2
        Me.cbxSummary.Text = "Total Bid BOM"
        '
        'cbxComments
        '
        Me.cbxComments.Checked = True
        Me.cbxComments.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxComments.Location = New System.Drawing.Point(40, 48)
        Me.cbxComments.Name = "cbxComments"
        Me.cbxComments.Size = New System.Drawing.Size(156, 20)
        Me.cbxComments.TabIndex = 3
        Me.cbxComments.Text = "Comments"
        '
        'cbxType
        '
        Me.cbxType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.Location = New System.Drawing.Point(40, 88)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Size = New System.Drawing.Size(156, 20)
        Me.cbxType.TabIndex = 4
        Me.cbxType.Text = "Type Summary"
        '
        'cbxUnitSummary
        '
        Me.cbxUnitSummary.Checked = True
        Me.cbxUnitSummary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxUnitSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUnitSummary.Location = New System.Drawing.Point(40, 108)
        Me.cbxUnitSummary.Name = "cbxUnitSummary"
        Me.cbxUnitSummary.Size = New System.Drawing.Size(156, 20)
        Me.cbxUnitSummary.TabIndex = 5
        Me.cbxUnitSummary.Text = "Unit Summary"
        '
        'cbxCommonSummary
        '
        Me.cbxCommonSummary.Checked = True
        Me.cbxCommonSummary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxCommonSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCommonSummary.Location = New System.Drawing.Point(40, 128)
        Me.cbxCommonSummary.Name = "cbxCommonSummary"
        Me.cbxCommonSummary.Size = New System.Drawing.Size(156, 20)
        Me.cbxCommonSummary.TabIndex = 6
        Me.cbxCommonSummary.Text = "Common Summary"
        '
        'cbxClubHouseSummary
        '
        Me.cbxClubHouseSummary.Checked = True
        Me.cbxClubHouseSummary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxClubHouseSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClubHouseSummary.Location = New System.Drawing.Point(40, 148)
        Me.cbxClubHouseSummary.Name = "cbxClubHouseSummary"
        Me.cbxClubHouseSummary.Size = New System.Drawing.Size(156, 20)
        Me.cbxClubHouseSummary.TabIndex = 7
        Me.cbxClubHouseSummary.Text = "Club House Summary"
        '
        'cbxSiteSummayr
        '
        Me.cbxSiteSummayr.Checked = True
        Me.cbxSiteSummayr.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxSiteSummayr.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSiteSummayr.Location = New System.Drawing.Point(40, 188)
        Me.cbxSiteSummayr.Name = "cbxSiteSummayr"
        Me.cbxSiteSummayr.Size = New System.Drawing.Size(156, 20)
        Me.cbxSiteSummayr.TabIndex = 8
        Me.cbxSiteSummayr.Text = "Site Summary"
        '
        'cbxUnitDetails
        '
        Me.cbxUnitDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUnitDetails.Location = New System.Drawing.Point(264, 48)
        Me.cbxUnitDetails.Name = "cbxUnitDetails"
        Me.cbxUnitDetails.Size = New System.Drawing.Size(156, 16)
        Me.cbxUnitDetails.TabIndex = 9
        Me.cbxUnitDetails.Text = "Unit Details"
        '
        'cbxCommonDetails
        '
        Me.cbxCommonDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCommonDetails.Location = New System.Drawing.Point(40, 208)
        Me.cbxCommonDetails.Name = "cbxCommonDetails"
        Me.cbxCommonDetails.Size = New System.Drawing.Size(156, 20)
        Me.cbxCommonDetails.TabIndex = 10
        Me.cbxCommonDetails.Text = "Common Details"
        '
        'cbxAlternates
        '
        Me.cbxAlternates.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAlternates.Location = New System.Drawing.Point(40, 272)
        Me.cbxAlternates.Name = "cbxAlternates"
        Me.cbxAlternates.Size = New System.Drawing.Size(156, 20)
        Me.cbxAlternates.TabIndex = 11
        Me.cbxAlternates.Text = "Alternates"
        '
        'cbxDescriptions
        '
        Me.cbxDescriptions.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDescriptions.Location = New System.Drawing.Point(268, 180)
        Me.cbxDescriptions.Name = "cbxDescriptions"
        Me.cbxDescriptions.Size = New System.Drawing.Size(156, 20)
        Me.cbxDescriptions.TabIndex = 12
        Me.cbxDescriptions.Text = "Descriptions"
        '
        'cboOrder
        '
        Me.cboOrder.Items.AddRange(New Object() {"Location", "Part Number", "Type"})
        Me.cboOrder.Location = New System.Drawing.Point(268, 276)
        Me.cboOrder.Name = "cboOrder"
        Me.cboOrder.Size = New System.Drawing.Size(168, 20)
        Me.cboOrder.TabIndex = 13
        '
        'lblSections
        '
        Me.lblSections.Location = New System.Drawing.Point(28, 24)
        Me.lblSections.Name = "lblSections"
        Me.lblSections.Size = New System.Drawing.Size(172, 16)
        Me.lblSections.TabIndex = 14
        Me.lblSections.Text = "Select The Report Sections To Include"
        '
        'lblOrder
        '
        Me.lblOrder.Location = New System.Drawing.Point(268, 252)
        Me.lblOrder.Name = "lblOrder"
        Me.lblOrder.Size = New System.Drawing.Size(184, 16)
        Me.lblOrder.TabIndex = 15
        Me.lblOrder.Text = "Select The Summary Report Sort Order"
        '
        'cbxBldgType
        '
        Me.cbxBldgType.Checked = True
        Me.cbxBldgType.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxBldgType.Location = New System.Drawing.Point(40, 68)
        Me.cbxBldgType.Name = "cbxBldgType"
        Me.cbxBldgType.Size = New System.Drawing.Size(156, 20)
        Me.cbxBldgType.TabIndex = 16
        Me.cbxBldgType.Text = "Building Type / Unit Count"
        '
        'cbxPrices
        '
        Me.cbxPrices.Checked = True
        Me.cbxPrices.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxPrices.Location = New System.Drawing.Point(268, 200)
        Me.cbxPrices.Name = "cbxPrices"
        Me.cbxPrices.Size = New System.Drawing.Size(92, 20)
        Me.cbxPrices.TabIndex = 17
        Me.cbxPrices.Text = "Prices"
        '
        'lblColumns
        '
        Me.lblColumns.Location = New System.Drawing.Point(264, 160)
        Me.lblColumns.Name = "lblColumns"
        Me.lblColumns.Size = New System.Drawing.Size(144, 16)
        Me.lblColumns.TabIndex = 18
        Me.lblColumns.Text = "Summary Columns To Show"
        '
        'cbxGrandTotals
        '
        Me.cbxGrandTotals.Checked = True
        Me.cbxGrandTotals.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxGrandTotals.Location = New System.Drawing.Point(292, 220)
        Me.cbxGrandTotals.Name = "cbxGrandTotals"
        Me.cbxGrandTotals.TabIndex = 19
        Me.cbxGrandTotals.Text = "Grand Totals"
        '
        'cbxPageTwoTotals
        '
        Me.cbxPageTwoTotals.Checked = True
        Me.cbxPageTwoTotals.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxPageTwoTotals.Location = New System.Drawing.Point(460, 40)
        Me.cbxPageTwoTotals.Name = "cbxPageTwoTotals"
        Me.cbxPageTwoTotals.Size = New System.Drawing.Size(132, 24)
        Me.cbxPageTwoTotals.TabIndex = 20
        Me.cbxPageTwoTotals.Text = "Show Page Two Totals"
        '
        'lblPageTwoTotals
        '
        Me.lblPageTwoTotals.Location = New System.Drawing.Point(464, 20)
        Me.lblPageTwoTotals.Name = "lblPageTwoTotals"
        Me.lblPageTwoTotals.Size = New System.Drawing.Size(160, 16)
        Me.lblPageTwoTotals.TabIndex = 21
        Me.lblPageTwoTotals.Text = "Sub Totals On Page Two"
        '
        'cbxAuxiliarySummary
        '
        Me.cbxAuxiliarySummary.Checked = True
        Me.cbxAuxiliarySummary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxAuxiliarySummary.Location = New System.Drawing.Point(40, 168)
        Me.cbxAuxiliarySummary.Name = "cbxAuxiliarySummary"
        Me.cbxAuxiliarySummary.Size = New System.Drawing.Size(160, 20)
        Me.cbxAuxiliarySummary.TabIndex = 22
        Me.cbxAuxiliarySummary.Text = "Auxiliary Summary"
        '
        'cbxBldgBOM
        '
        Me.cbxBldgBOM.Location = New System.Drawing.Point(40, 228)
        Me.cbxBldgBOM.Name = "cbxBldgBOM"
        Me.cbxBldgBOM.Size = New System.Drawing.Size(156, 24)
        Me.cbxBldgBOM.TabIndex = 23
        Me.cbxBldgBOM.Text = "Bldg BOM's"
        '
        'cbxClubhouseDetails
        '
        Me.cbxClubhouseDetails.Location = New System.Drawing.Point(264, 67)
        Me.cbxClubhouseDetails.Name = "cbxClubhouseDetails"
        Me.cbxClubhouseDetails.Size = New System.Drawing.Size(156, 16)
        Me.cbxClubhouseDetails.TabIndex = 24
        Me.cbxClubhouseDetails.Text = "Club House Details"
        '
        'cbxSiteDetails
        '
        Me.cbxSiteDetails.Location = New System.Drawing.Point(264, 86)
        Me.cbxSiteDetails.Name = "cbxSiteDetails"
        Me.cbxSiteDetails.Size = New System.Drawing.Size(156, 16)
        Me.cbxSiteDetails.TabIndex = 25
        Me.cbxSiteDetails.Text = "Site Details"
        '
        'cbxAuxDetails
        '
        Me.cbxAuxDetails.Location = New System.Drawing.Point(264, 105)
        Me.cbxAuxDetails.Name = "cbxAuxDetails"
        Me.cbxAuxDetails.Size = New System.Drawing.Size(156, 16)
        Me.cbxAuxDetails.TabIndex = 26
        Me.cbxAuxDetails.Text = "Auxilliary Details"
        '
        'cbxTBD
        '
        Me.cbxTBD.Location = New System.Drawing.Point(264, 124)
        Me.cbxTBD.Name = "cbxTBD"
        Me.cbxTBD.Size = New System.Drawing.Size(156, 16)
        Me.cbxTBD.TabIndex = 27
        Me.cbxTBD.Text = "TBD "
        '
        'frmRptBidSummary
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 11)
        Me.ClientSize = New System.Drawing.Size(636, 338)
        Me.Controls.Add(Me.cbxTBD)
        Me.Controls.Add(Me.cbxAuxDetails)
        Me.Controls.Add(Me.cbxSiteDetails)
        Me.Controls.Add(Me.cbxClubhouseDetails)
        Me.Controls.Add(Me.cbxBldgBOM)
        Me.Controls.Add(Me.cbxAuxiliarySummary)
        Me.Controls.Add(Me.lblPageTwoTotals)
        Me.Controls.Add(Me.cbxPageTwoTotals)
        Me.Controls.Add(Me.cbxGrandTotals)
        Me.Controls.Add(Me.lblColumns)
        Me.Controls.Add(Me.cbxPrices)
        Me.Controls.Add(Me.cbxBldgType)
        Me.Controls.Add(Me.lblOrder)
        Me.Controls.Add(Me.lblSections)
        Me.Controls.Add(Me.cboOrder)
        Me.Controls.Add(Me.cbxDescriptions)
        Me.Controls.Add(Me.cbxAlternates)
        Me.Controls.Add(Me.cbxCommonDetails)
        Me.Controls.Add(Me.cbxUnitDetails)
        Me.Controls.Add(Me.cbxSiteSummayr)
        Me.Controls.Add(Me.cbxClubHouseSummary)
        Me.Controls.Add(Me.cbxCommonSummary)
        Me.Controls.Add(Me.cbxUnitSummary)
        Me.Controls.Add(Me.cbxType)
        Me.Controls.Add(Me.cbxComments)
        Me.Controls.Add(Me.cbxSummary)
        Me.Controls.Add(Me.btnShowReport)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.Name = "frmRptBidSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Summary Bid"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRptBidSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadSubTotals()
        LoadCounts()
        'LoadReport()
        cboOrder.SelectedIndex = 0
    End Sub

    Private Sub LoadReport()

        Dim searchOrder As String
        Select Case cboOrder.SelectedItem.ToString
            Case "Part Number"
                searchOrder = "PN"
            Case "Type"
                searchOrder = "T"
            Case "Location"
                searchOrder = "L"
            Case Else
                searchOrder = ""
        End Select

        Dim curRpt As String
        Dim LineItemPrimary As String
        LineItemPrimary = "Select pn PartNumber, type, descr Description, Quantity, co Cost, se Sell, " + _
            "(Quantity * Co) as [Total Cost], (Quantity * Se) as [Total Sell], " + _
            "(Quantity * Se) - (Quantity * Co) as Net "

        Dim LineItemSecondary As String
        LineItemSecondary = "from(select type, Description Descr, PartNumber pn, Cost co, Margin ma, Sell se," + _
            " sum(Quantity) AS Quantity, RT from LineItem where ProjectID = " + frmMain.intBidID.ToString

        Dim LineItemThird As String
        LineItemThird = " and BldgID is not null group by type, RT, PartNumber , Description, Cost, Margin, Sell" + _
            " ) tempTable "

        Dim OrderBy As String

        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If

        cnn.ConnectionString = frmMain.strConnect

        curRpt = "rptIntro"
        Dim dsBid As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "Bids")

        curRpt = "rptBldgTypeCount"
        Dim dsBldgTypeCount As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptBldgTypeCount")

        OrderBy = "Order By PartNumber"
        LineItemPrimary = "Select pn PartNumber, Type, descr Description, Quantity, co Cost, se Sell, " + _
            "(Quantity * Co) as [Total Cost], (Quantity * Se) as [Total Sell], " + _
            "(Quantity * Se) - (Quantity * Co) as Net "
        Dim BidBom As String = "Select Partnumber, Description, Sum(Quantity) Quantity, Sell from LineItem where ProjectID = " + _
            frmMain.intBidID.ToString + " and BldgId is not null group by Partnumber, Description, Sell order by PartNumber"
        curRpt = LineItemPrimary + LineItemSecondary + LineItemThird + OrderBy
        Dim dsSummary As DataSet = ExecuteSQL(BidBom, cnn, "b_LineItemSummary")

        OrderBy = "Order By Type"
        curRpt = LineItemPrimary + LineItemSecondary + " and Type <> ''" + LineItemThird + OrderBy
        Dim dsTypeSummary As DataSet = ExecuteSQL(curRpt, cnn, "b_TypeSummary")

        OrderBy = "Order By PartNumber"
        curRpt = LineItemPrimary + LineItemSecondary + " and Type = ''" + LineItemThird + OrderBy
        Dim dsTypeSummaryNoType As DataSet = ExecuteSQL(curRpt, cnn, "b_TypeSummaryNoType")

        curRpt = "rptUnitDetailPerBldg"
        Dim dsUnitDetailPerBldg As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptUnitDetailPerBldg")

        curRpt = "ProjectUnitCount"
        Dim dsUnitCount As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_UnitCount")

        curRpt = "rptQuantityDetails"
        Dim dsUnitDetail As DataSet = CallQuantityDetailsSP(curRpt, cnn, "b_rptUnitDetails", frmMain.intBidID.ToString, "U")

        curRpt = "rptQuantityDetails"
        Dim dsCommonDetail As DataSet = CallQuantityDetailsSP(curRpt, cnn, "b_rptCommonDetails", frmMain.intBidID.ToString, "X")

        curRpt = "AlternateSummary"
        Dim dsAlternates As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_AlternateSummary")

        curRpt = "rptComments"
        Dim dsBidComments As DataSet = CallRptSP(curRpt, cnn, frmMain.intBidID.ToString, "b_rptBidComments")

        curRpt = "select bldgNumber, building.Type, building.Description as BldgDesc, Building.Location, Address, Building.ShipdateR, Building.shipsequence," + _
            " lineitem.PartNumber, SUM(Lineitem.Quantity) As Total, LineItem.Description, LineItem.Cost, LineItem.Sell from Building inner join LineItem" + _
            " on building.BldgID = LineItem.BldgID where Building.ProjectID = " + frmMain.intBidID.ToString + " group by bldgNumber, building.Type," + _
            " building.Description, Building.Location, Address, Building.ShipdateR, Building.shipsequence, lineitem.PartNumber, LineItem.Description," + _
            " LineItem.Cost, LineItem.Sell order by bldgNumber"
        Dim dsBldgBOM As DataSet = ExecuteSQL(curRpt, cnn, "b_BldgBOM")

        curRpt = "rptQuantityDetails"
        Dim dsClubHouseDetail As DataSet = CallQuantityDetailsSP(curRpt, cnn, "b_rptClubHouseDetails", frmMain.intBidID.ToString, "C")

        curRpt = "rptQuantityDetails"
        Dim dsSiteDetail As DataSet = CallQuantityDetailsSP(curRpt, cnn, "b_rptSiteDetails", frmMain.intBidID.ToString, "S")

        curRpt = "rptQuantityDetails"
        Dim dsAuxDetail As DataSet = CallQuantityDetailsSP(curRpt, cnn, "b_rptAuxDetails", frmMain.intBidID.ToString, "A")

        OrderBy = "Order by RT, PartNumber"
        curRpt = LineItemPrimary + LineItemSecondary + " and LEFT(PartNumber,3) ='TBD'" + LineItemThird + OrderBy
        Dim dsLineItemTBD As DataSet = ExecuteSQL(curRpt, cnn, "b_LineItemTBD")


        Dim strClubHouseSummary As String
        strClubHouseSummary = "ClubHouseSummary"
        Dim cmdClubHouseSummary As New SqlCommand
        cmdClubHouseSummary.Connection = cnn
        cmdClubHouseSummary.CommandText = strClubHouseSummary
        cmdClubHouseSummary.CommandType = CommandType.StoredProcedure
        Dim prmClubHouseSummaryBidID As SqlParameter = cmdClubHouseSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmClubHouseSummaryBidID.Value = frmMain.intBidID

        Dim prmCllubHouseOrder As SqlParameter = cmdClubHouseSummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        prmCllubHouseOrder.Value = searchOrder

        Dim daClubHouseSummary As New SqlDataAdapter(cmdClubHouseSummary)
        Dim dsClubHouseSummary As New DataSet
        Try
            daClubHouseSummary.Fill(dsClubHouseSummary, "b_ClubHouseSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_ClubHouseSummary")
        End Try

        Dim strSiteSummary As String
        strSiteSummary = "UnitSummary"
        Dim cmdSiteSummary As New SqlCommand
        cmdSiteSummary.Connection = cnn
        cmdSiteSummary.CommandText = strSiteSummary
        cmdSiteSummary.CommandType = CommandType.StoredProcedure
        Dim prmSiteSummaryBidID As SqlParameter = cmdSiteSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSiteSummaryBidID.Value = frmMain.intBidID

        Dim prmUnitType As SqlParameter = cmdSiteSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        prmUnitType.Value = "S"

        Dim prmSiteOrder As SqlParameter = cmdSiteSummary.Parameters.Add("@OrderBy", SqlDbType.VarChar, 15)
        prmSiteOrder.Value = searchOrder

        Dim daSiteSummary As New SqlDataAdapter(cmdSiteSummary)
        Dim dsSiteSummary As New DataSet
        Try
            daSiteSummary.Fill(dsSiteSummary, "b_SiteSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_SiteSummary")
        End Try

        Dim strUnitSummary As String
        strUnitSummary = "UnitSummary"
        Dim cmdUnitSummary As New SqlCommand
        cmdUnitSummary.Connection = cnn
        cmdUnitSummary.CommandText = strUnitSummary
        cmdUnitSummary.CommandType = CommandType.StoredProcedure

        Dim prmUnitSummaryBidID As SqlParameter = cmdUnitSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitSummaryBidID.Value = frmMain.intBidID

        prmUnitType = cmdUnitSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        prmUnitType.Value = "U"

        Dim prmUnitOrder As SqlParameter = cmdUnitSummary.Parameters.Add("@OrderBy", SqlDbType.Char, 2)
        prmUnitOrder.Value = searchOrder

        Dim daUnitSummary As New SqlDataAdapter(cmdUnitSummary)
        Dim dsUnitSummary As New DataSet
        Try
            daUnitSummary.Fill(dsUnitSummary, "b_UnitSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_UnitSummary")
        End Try

        Dim strCommonSummary As String
        strCommonSummary = "CommonSummary"
        Dim cmdCommonSummary As New SqlCommand
        cmdCommonSummary.Connection = cnn
        cmdCommonSummary.CommandText = strCommonSummary
        cmdCommonSummary.CommandType = CommandType.StoredProcedure
        Dim prmCommonSummaryBidID As SqlParameter = cmdCommonSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmCommonSummaryBidID.Value = frmMain.intBidID

        Dim prmCommonOrder As SqlParameter = cmdCommonSummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        prmCommonOrder.Value = searchOrder

        Dim daCommonSummary As New SqlDataAdapter(cmdCommonSummary)
        Dim dsCommonSummary As New DataSet
        Try
            daCommonSummary.Fill(dsCommonSummary, "b_CommonSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_CommonSummary")
        End Try

        Dim strAuxiliarySummary As String
        strAuxiliarySummary = "AuxillarySummary"
        Dim cmdAuxiliarySummary As New SqlCommand
        cmdAuxiliarySummary.Connection = cnn
        cmdAuxiliarySummary.CommandText = strAuxiliarySummary
        cmdAuxiliarySummary.CommandType = CommandType.StoredProcedure
        Dim prmAuxiliarySummaryBidID As SqlParameter = cmdAuxiliarySummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmAuxiliarySummaryBidID.Value = frmMain.intBidID

        Dim prmAuxiliaryOrder As SqlParameter = cmdAuxiliarySummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        prmAuxiliaryOrder.Value = searchOrder

        Dim daAuxiliarySummary As New SqlDataAdapter(cmdAuxiliarySummary)
        Dim dsAuxiliarySummary As New DataSet
        Try
            daAuxiliarySummary.Fill(dsAuxiliarySummary, "b_AuxillarySummary")
        Catch ex As Exception
            MsgBox("Failed to Fill b_AuxiliarySummary")
        End Try

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument

        Try
            myReport = New rptBidMain
            myReport.SetDataSource(dsBid)
            myReport.OpenSubreport("rptBidComments.rpt").SetDataSource(dsBidComments)
            myReport.OpenSubreport("rptBldgTypeCount.rpt").SetDataSource(dsBldgTypeCount)
            myReport.OpenSubreport("rptUnitCount.rpt").SetDataSource(dsUnitCount)
            myReport.OpenSubreport("rptTypeSummary.rpt").SetDataSource(dsTypeSummary)
            myReport.OpenSubreport("rptTypeSummaryNotype.rpt").SetDataSource(dsTypeSummaryNoType)
            myReport.OpenSubreport("rptUnitDetailPerBldg1.rpt").SetDataSource(dsUnitDetailPerBldg)
            myReport.OpenSubreport("rptUnitDetails.rpt").SetDataSource(dsUnitDetail)
            myReport.OpenSubreport("rptCommonDetails.rpt").SetDataSource(dsCommonDetail)
            myReport.OpenSubreport("b_rptClubHouseDetails.rpt").SetDataSource(dsClubHouseDetail)
            myReport.OpenSubreport("b_rptSiteDetails.rpt").SetDataSource(dsSiteDetail)
            myReport.OpenSubreport("b_rptAuxDetails.rpt").SetDataSource(dsAuxDetail)
            myReport.OpenSubreport("rptClubHouseSummary.rpt").SetDataSource(dsClubHouseSummary)
            myReport.OpenSubreport("rptSiteSummary.rpt").SetDataSource(dsSiteSummary)
            myReport.OpenSubreport("rptUnitSummary.rpt").SetDataSource(dsUnitSummary)
            myReport.OpenSubreport("rptCommonSummary.rpt").SetDataSource(dsCommonSummary)
            myReport.OpenSubreport("rptAuxiliarySummary.rpt").SetDataSource(dsAuxiliarySummary)
            myReport.OpenSubreport("rptBldgBOM.rpt").SetDataSource(dsBldgBOM)
            myReport.OpenSubreport("rptBidSummary.rpt").SetDataSource(dsSummary)
            myReport.OpenSubreport("b_rptTBD.rpt").SetDataSource(dsLineItemTBD)
            myReport.OpenSubreport("rptBidAlternates.rpt").SetDataSource(dsAlternates)
        Catch ex As Exception
            MsgBox("Failed to Open Subreports")
        End Try

        Dim SRAuxiliarylbl As CrystalDecisions.CrystalReports.Engine.ReportDocument
        SRAuxiliarylbl = myReport.OpenSubreport("rptAuxiliarySummary.rpt")
        Dim roAuxiliarylbl As CrystalDecisions.CrystalReports.Engine.ReportObject
        Dim foAuxiliarylbl As CrystalDecisions.CrystalReports.Engine.TextObject
        roAuxiliarylbl = SRAuxiliarylbl.ReportDefinition.ReportObjects.Item("Text8")
        foAuxiliarylbl = CType(roAuxiliarylbl, CrystalDecisions.CrystalReports.Engine.TextObject)
        foAuxiliarylbl.Text = Trim(strAuxLabel) & " Summary"

        Dim SRAuxDetaillbl As CrystalDecisions.CrystalReports.Engine.ReportDocument
        SRAuxDetaillbl = myReport.OpenSubreport("b_rptAuxDetails.rpt")
        Dim roAuxDetaillbl As CrystalDecisions.CrystalReports.Engine.ReportObject
        Dim foAuxDetaillbl As CrystalDecisions.CrystalReports.Engine.TextObject
        roAuxDetaillbl = SRAuxDetaillbl.ReportDefinition.ReportObjects.Item("Text1")
        foAuxDetaillbl = CType(roAuxDetaillbl, CrystalDecisions.CrystalReports.Engine.TextObject)
        foAuxDetaillbl.Text = Trim(strAuxLabel) & " Details"

        If cbxPageTwoTotals.Checked = True Then
            Dim UnitsText As CrystalDecisions.CrystalReports.Engine.TextObject
            UnitsText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnits"), CrystalDecisions.CrystalReports.Engine.TextObject)
            UnitsText.Text = strUnits

            Dim CommonText As CrystalDecisions.CrystalReports.Engine.TextObject
            CommonText = CType(myReport.ReportDefinition.ReportObjects.Item("txtCommon"), CrystalDecisions.CrystalReports.Engine.TextObject)
            CommonText.Text = strCommon

            Dim SiteText As CrystalDecisions.CrystalReports.Engine.TextObject
            SiteText = CType(myReport.ReportDefinition.ReportObjects.Item("txtSite"), CrystalDecisions.CrystalReports.Engine.TextObject)
            SiteText.Text = strSite

            Dim ClubhouseText As CrystalDecisions.CrystalReports.Engine.TextObject
            ClubhouseText = CType(myReport.ReportDefinition.ReportObjects.Item("txtClubhouse"), CrystalDecisions.CrystalReports.Engine.TextObject)
            ClubhouseText.Text = strClubhouse

            Dim AuxiliaryText As CrystalDecisions.CrystalReports.Engine.TextObject
            AuxiliaryText = CType(myReport.ReportDefinition.ReportObjects.Item("txtAuxiliary"), CrystalDecisions.CrystalReports.Engine.TextObject)
            AuxiliaryText.Text = strAuxiliary

            Dim Auxiliarylbl As CrystalDecisions.CrystalReports.Engine.TextObject
            Auxiliarylbl = CType(myReport.ReportDefinition.ReportObjects.Item("lblAuxiliary"), CrystalDecisions.CrystalReports.Engine.TextObject)
            Auxiliarylbl.Text = strAuxLabel

            Dim PreTaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            PreTaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtPreTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
            PreTaxTotalText.Text = strPreTaxTotal

            Dim TaxRateText As CrystalDecisions.CrystalReports.Engine.TextObject
            TaxRateText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxRate"), CrystalDecisions.CrystalReports.Engine.TextObject)
            TaxRateText.Text = FormatPercent(CDbl(strTaxRate), 2)

            Dim TaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            TaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
            TaxTotalText.Text = strTaxTotal

            Dim GrandTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            GrandTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtGrandTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
            GrandTotalText.Text = strGrandTotal
        End If

        Dim UnitCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnitCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitCountText.Text = strrptUnitCount

        Dim BldgCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        BldgCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtBldgCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        BldgCountText.Text = strrptBldgCount

        If cbxType.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section15")
            MySectionTypeDef.SectionFormat.EnableSuppress = True

            Dim MySectionNoTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionNoTypeDef = myReport.ReportDefinition.Sections.Item("Section17")
            MySectionNoTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxBldgType.Checked = False Then
            Dim MySectionBldgType As CrystalDecisions.CrystalReports.Engine.Section
            MySectionBldgType = myReport.ReportDefinition.Sections.Item("Section3")
            MySectionBldgType.SectionFormat.EnableSuppress = True
        End If

        If cbxUnitSummary.Checked = False Then
            Dim MySectionUnitSummary As CrystalDecisions.CrystalReports.Engine.Section
            MySectionUnitSummary = myReport.ReportDefinition.Sections.Item("Section12")
            MySectionUnitSummary.SectionFormat.EnableSuppress = True
        End If

        If cbxCommonSummary.Checked = False Then
            Dim MySectionCommonSummary As CrystalDecisions.CrystalReports.Engine.Section
            MySectionCommonSummary = myReport.ReportDefinition.Sections.Item("Section16")
            MySectionCommonSummary.SectionFormat.EnableSuppress = True
        End If

        If cbxAuxiliarySummary.Checked = False Then
            Dim MySectionAuxiliarySummary As CrystalDecisions.CrystalReports.Engine.Section
            MySectionAuxiliarySummary = myReport.ReportDefinition.Sections.Item("Section19")
            MySectionAuxiliarySummary.SectionFormat.EnableSuppress = True
        End If

        If cbxClubHouseSummary.Checked = False Then
            Dim MySectionClubhouseSummary As CrystalDecisions.CrystalReports.Engine.Section
            MySectionClubhouseSummary = myReport.ReportDefinition.Sections.Item("Section10")
            MySectionClubhouseSummary.SectionFormat.EnableSuppress = True
        End If

        If cbxSiteSummayr.Checked = False Then
            Dim MySectionSiteSummary As CrystalDecisions.CrystalReports.Engine.Section
            MySectionSiteSummary = myReport.ReportDefinition.Sections.Item("Section11")
            MySectionSiteSummary.SectionFormat.EnableSuppress = True
        End If

        If cbxAlternates.Checked = False Then
            Dim MySectionAlternates As CrystalDecisions.CrystalReports.Engine.Section
            MySectionAlternates = myReport.ReportDefinition.Sections.Item("Section7")
            MySectionAlternates.SectionFormat.EnableSuppress = True
        End If

        If cbxBldgBOM.Checked = False Then
            Dim MySectionBldgBOM As CrystalDecisions.CrystalReports.Engine.Section
            MySectionBldgBOM = myReport.ReportDefinition.Sections.Item("Section20")
            MySectionBldgBOM.SectionFormat.EnableSuppress = True
        End If

        If cbxUnitDetails.Checked = False Then
            Dim MySectionUnitDetails As CrystalDecisions.CrystalReports.Engine.Section
            MySectionUnitDetails = myReport.ReportDefinition.Sections.Item("Section9")
            MySectionUnitDetails.SectionFormat.EnableSuppress = True

            Dim MySectionUnitDetailsPerBldg As CrystalDecisions.CrystalReports.Engine.Section
            MySectionUnitDetailsPerBldg = myReport.ReportDefinition.Sections.Item("Section8")
            MySectionUnitDetailsPerBldg.SectionFormat.EnableSuppress = True
        End If

        If cbxCommonDetails.Checked = False Then
            Dim MySectionCommonDetails As CrystalDecisions.CrystalReports.Engine.Section
            MySectionCommonDetails = myReport.ReportDefinition.Sections.Item("Section14")
            MySectionCommonDetails.SectionFormat.EnableSuppress = True
        End If

        If cbxClubhouseDetails.Checked = False Then
            Dim MySectionClubHouseDetails As CrystalDecisions.CrystalReports.Engine.Section
            MySectionClubHouseDetails = myReport.ReportDefinition.Sections.Item("Section21")
            MySectionClubHouseDetails.SectionFormat.EnableSuppress = True
        End If

        If cbxSiteDetails.Checked = False Then
            Dim MySectionSiteDetails As CrystalDecisions.CrystalReports.Engine.Section
            MySectionSiteDetails = myReport.ReportDefinition.Sections.Item("Section23")
            MySectionSiteDetails.SectionFormat.EnableSuppress = True
        End If

        If cbxAuxDetails.Checked = False Then
            Dim MySectionAuxDetails As CrystalDecisions.CrystalReports.Engine.Section
            MySectionAuxDetails = myReport.ReportDefinition.Sections.Item("Section22")
            MySectionAuxDetails.SectionFormat.EnableSuppress = True
        End If

        If cbxSummary.Checked = False Then
            Dim MySectionBOM As CrystalDecisions.CrystalReports.Engine.Section
            MySectionBOM = myReport.ReportDefinition.Sections.Item("Section6")
            MySectionBOM.SectionFormat.EnableSuppress = True
        End If

        If cbxTBD.Checked = False Then
            Dim MySectionTBD As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTBD = myReport.ReportDefinition.Sections.Item("Section24")
            MySectionTBD.SectionFormat.EnableSuppress = True
        End If

        If cbxComments.Checked = False Then
            Dim MySectionComment As CrystalDecisions.CrystalReports.Engine.Section
            MySectionComment = myReport.ReportDefinition.Sections.Item("Section18")
            MySectionComment.SectionFormat.EnableSuppress = True
        End If

        If dsBidComments.Tables(0).Rows.Count = 0 Then
            Dim MySectionComment As CrystalDecisions.CrystalReports.Engine.Section
            MySectionComment = myReport.ReportDefinition.Sections.Item("Section18")
            MySectionComment.SectionFormat.EnableSuppress = True
        End If

        If cbxDescriptions.Checked = False Then
            Dim SRTypeSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRTypeSummary = myReport.OpenSubreport("rptTypeSummary.rpt")
            Dim roTypeField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foTypeField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roTypeField = SRTypeSummary.ReportDefinition.ReportObjects.Item("Field3")
            foTypeField = CType(roTypeField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foTypeField.ObjectFormat.EnableSuppress = True

            Dim roTypeText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foTypeText As CrystalDecisions.CrystalReports.Engine.TextObject
            roTypeText = SRTypeSummary.ReportDefinition.ReportObjects.Item("Text3")
            foTypeText = CType(roTypeText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foTypeText.ObjectFormat.EnableSuppress = True

            Dim SRNoTypeSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRNoTypeSummary = myReport.OpenSubreport("rptTypeSummaryNotype.rpt")
            Dim roNoTypeField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foNoTypeField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roNoTypeField = SRNoTypeSummary.ReportDefinition.ReportObjects.Item("Field3")
            foNoTypeField = CType(roNoTypeField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foNoTypeField.ObjectFormat.EnableSuppress = True

            Dim roNoTypeText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foNoTypeText As CrystalDecisions.CrystalReports.Engine.TextObject
            roNoTypeText = SRNoTypeSummary.ReportDefinition.ReportObjects.Item("Text3")
            foNoTypeText = CType(roNoTypeText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foNoTypeText.ObjectFormat.EnableSuppress = True

            Dim SRUnitSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRUnitSummary = myReport.OpenSubreport("rptUnitSummary.rpt")
            Dim roUnitField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foUnitField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roUnitField = SRUnitSummary.ReportDefinition.ReportObjects.Item("Field2")
            foUnitField = CType(roUnitField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foUnitField.ObjectFormat.EnableSuppress = True

            Dim roUnitText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foUnitText As CrystalDecisions.CrystalReports.Engine.TextObject
            roUnitText = SRUnitSummary.ReportDefinition.ReportObjects.Item("Text2")
            foUnitText = CType(roUnitText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foUnitText.ObjectFormat.EnableSuppress = True

            Dim SRCommonSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRCommonSummary = myReport.OpenSubreport("rptCommonSummary.rpt")
            Dim roCommonField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foCommonField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roCommonField = SRCommonSummary.ReportDefinition.ReportObjects.Item("Field2")
            foCommonField = CType(roCommonField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foCommonField.ObjectFormat.EnableSuppress = True

            Dim roCommonText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foCommonText As CrystalDecisions.CrystalReports.Engine.TextObject
            roCommonText = SRCommonSummary.ReportDefinition.ReportObjects.Item("Text2")
            foCommonText = CType(roCommonText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foCommonText.ObjectFormat.EnableSuppress = True

            Dim SRClubHouseSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRClubHouseSummary = myReport.OpenSubreport("rptClubHouseSummary.rpt")
            Dim roClubHouseField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foClubHouseField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roClubHouseField = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Field2")
            foClubHouseField = CType(roClubHouseField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foClubHouseField.ObjectFormat.EnableSuppress = True

            Dim roClubHouseText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foClubHouseText As CrystalDecisions.CrystalReports.Engine.TextObject
            roClubHouseText = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Text2")
            foClubHouseText = CType(roClubHouseText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foClubHouseText.ObjectFormat.EnableSuppress = True

            Dim SRClubHouseDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRClubHouseDetail = myReport.OpenSubreport("b_rptClubHouseDetails.rpt")
            Dim roClubHouseDetailField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foClubHouseDetailField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roClubHouseDetailField = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Field5")
            foClubHouseDetailField = CType(roClubHouseDetailField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foClubHouseDetailField.ObjectFormat.EnableSuppress = True

            Dim roClubHouseDetailText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foClubHouseDetailText As CrystalDecisions.CrystalReports.Engine.TextObject
            roClubHouseDetailText = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Text5")
            foClubHouseDetailText = CType(roClubHouseDetailText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foClubHouseDetailText.ObjectFormat.EnableSuppress = True

            Dim SRSiteDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRSiteDetail = myReport.OpenSubreport("b_rptSiteDetails.rpt")
            Dim roSiteDetailField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foSiteDetailField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roSiteDetailField = SRSiteDetail.ReportDefinition.ReportObjects.Item("Field5")
            foSiteDetailField = CType(roSiteDetailField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foSiteDetailField.ObjectFormat.EnableSuppress = True

            Dim roSiteDetailText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foSiteDetailText As CrystalDecisions.CrystalReports.Engine.TextObject
            roSiteDetailText = SRSiteDetail.ReportDefinition.ReportObjects.Item("Text6")
            foSiteDetailText = CType(roSiteDetailText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foSiteDetailText.ObjectFormat.EnableSuppress = True

            Dim SRUnitDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRUnitDetail = myReport.OpenSubreport("rptUnitDetails.rpt")
            Dim roUnitDetailField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foUnitDetailField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roUnitDetailField = SRUnitDetail.ReportDefinition.ReportObjects.Item("Field5")
            foUnitDetailField = CType(roUnitDetailField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foUnitDetailField.ObjectFormat.EnableSuppress = True

            Dim roUnitDetailText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foUnitDetailText As CrystalDecisions.CrystalReports.Engine.TextObject
            roUnitDetailText = SRUnitDetail.ReportDefinition.ReportObjects.Item("Text6")
            foUnitDetailText = CType(roUnitDetailText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foUnitDetailText.ObjectFormat.EnableSuppress = True

            Dim SRAuxDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRAuxDetail = myReport.OpenSubreport("b_rptAuxDetails.rpt")
            Dim roAuxDetailField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foAuxDetailField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roAuxDetailField = SRAuxDetail.ReportDefinition.ReportObjects.Item("Field5")
            foAuxDetailField = CType(roAuxDetailField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foAuxDetailField.ObjectFormat.EnableSuppress = True

            Dim roAuxDetailText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foAuxDetailText As CrystalDecisions.CrystalReports.Engine.TextObject
            roAuxDetailText = SRAuxDetail.ReportDefinition.ReportObjects.Item("Text5")
            foAuxDetailText = CType(roAuxDetailText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foAuxDetailText.ObjectFormat.EnableSuppress = True

            Dim SRAuxiliarySummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRAuxiliarySummary = myReport.OpenSubreport("rptAuxiliarySummary.rpt")
            Dim roAuxiliaryField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foAuxiliaryField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roAuxiliaryField = SRAuxiliarySummary.ReportDefinition.ReportObjects.Item("Field4")
            foAuxiliaryField = CType(roAuxiliaryField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foAuxiliaryField.ObjectFormat.EnableSuppress = True

            Dim roAuxiliaryText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foAuxiliaryText As CrystalDecisions.CrystalReports.Engine.TextObject
            roAuxiliaryText = SRAuxiliarySummary.ReportDefinition.ReportObjects.Item("Text4")
            foAuxiliaryText = CType(roAuxiliaryText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foAuxiliaryText.ObjectFormat.EnableSuppress = True

            Dim SRSiteSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRSiteSummary = myReport.OpenSubreport("rptSiteSummary.rpt")
            Dim roSiteField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foSiteField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roSiteField = SRSiteSummary.ReportDefinition.ReportObjects.Item("Field2")
            foSiteField = CType(roSiteField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foSiteField.ObjectFormat.EnableSuppress = True

            Dim roSiteText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foSiteText As CrystalDecisions.CrystalReports.Engine.TextObject
            roSiteText = SRSiteSummary.ReportDefinition.ReportObjects.Item("Text2")
            foSiteText = CType(roSiteText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foSiteText.ObjectFormat.EnableSuppress = True

            Dim SRBldgBOM As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRBldgBOM = myReport.OpenSubreport("rptBldgBOM.rpt")
            roSiteField = SRBldgBOM.ReportDefinition.ReportObjects.Item("Field10")
            foSiteField = CType(roSiteField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foSiteField.ObjectFormat.EnableSuppress = True

            roSiteText = SRBldgBOM.ReportDefinition.ReportObjects.Item("Text11")
            foSiteText = CType(roSiteText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foSiteText.ObjectFormat.EnableSuppress = True

            Dim SRMainSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRMainSummary = myReport.OpenSubreport("rptBidSummary.rpt")
            Dim roMainField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foMainField As CrystalDecisions.CrystalReports.Engine.FieldObject
            roMainField = SRMainSummary.ReportDefinition.ReportObjects.Item("Field4")
            foMainField = CType(roMainField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            foMainField.ObjectFormat.EnableSuppress = True

            Dim roMainText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim foMainText As CrystalDecisions.CrystalReports.Engine.TextObject
            roMainText = SRMainSummary.ReportDefinition.ReportObjects.Item("Text5")
            foMainText = CType(roMainText, CrystalDecisions.CrystalReports.Engine.TextObject)
            foMainText.ObjectFormat.EnableSuppress = True
        End If

        If cbxPrices.Checked = False Then

            Dim SRUnitSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRUnitSummary = myReport.OpenSubreport("rptUnitSummary.rpt")

            Dim rUNPriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUNPriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rUNPriceField = SRUnitSummary.ReportDefinition.ReportObjects.Item("Field3")
            fUNPriceField = CType(rUNPriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fUNPriceField.ObjectFormat.EnableSuppress = True

            Dim rUNPriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUNPriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rUNPriceText = SRUnitSummary.ReportDefinition.ReportObjects.Item("Text3")
            fUNPriceText = CType(rUNPriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fUNPriceText.ObjectFormat.EnableSuppress = True

            Dim rUNTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUNTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rUNTotalField = SRUnitSummary.ReportDefinition.ReportObjects.Item("Field5")
            fUNTotalField = CType(rUNTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fUNTotalField.ObjectFormat.EnableSuppress = True

            Dim rUNTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUNTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rUNTotalText = SRUnitSummary.ReportDefinition.ReportObjects.Item("Text5")
            fUNTotalText = CType(rUNTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fUNTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rUNGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fUNGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rUNGrandTotalField = SRUnitSummary.ReportDefinition.ReportObjects.Item("Field8")
                fUNGrandTotalField = CType(rUNGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fUNGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

            Dim SRCommonSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRCommonSummary = myReport.OpenSubreport("rptCommonSummary.rpt")

            Dim rCOMMPriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCOMMPriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rCOMMPriceField = SRCommonSummary.ReportDefinition.ReportObjects.Item("Field3")
            fCOMMPriceField = CType(rCOMMPriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fCOMMPriceField.ObjectFormat.EnableSuppress = True

            Dim rCOMMPriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCOMMPriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rCOMMPriceText = SRCommonSummary.ReportDefinition.ReportObjects.Item("Text3")
            fCOMMPriceText = CType(rCOMMPriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fCOMMPriceText.ObjectFormat.EnableSuppress = True

            Dim rCOMMTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCOMMTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rCOMMTotalField = SRCommonSummary.ReportDefinition.ReportObjects.Item("Field5")
            fCOMMTotalField = CType(rCOMMTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fCOMMTotalField.ObjectFormat.EnableSuppress = True

            Dim rCOMMTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCOMMTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rCOMMTotalText = SRCommonSummary.ReportDefinition.ReportObjects.Item("Text5")
            fCOMMTotalText = CType(rCOMMTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fCOMMTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rCOMMGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fCOMMGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rCOMMGrandTotalField = SRCommonSummary.ReportDefinition.ReportObjects.Item("Field8")
                fCOMMGrandTotalField = CType(rCOMMGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fCOMMGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

            Dim SRAuxSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRAuxSummary = myReport.OpenSubreport("rptAuxiliarySummary.rpt")

            Dim rAuxPriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxPriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rAuxPriceField = SRAuxSummary.ReportDefinition.ReportObjects.Item("Field6")
            fAuxPriceField = CType(rAuxPriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fAuxPriceField.ObjectFormat.EnableSuppress = True

            Dim rAuxPriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxPriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rAuxPriceText = SRAuxSummary.ReportDefinition.ReportObjects.Item("Text6")
            fAuxPriceText = CType(rAuxPriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fAuxPriceText.ObjectFormat.EnableSuppress = True

            Dim rAuxTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rAuxTotalField = SRAuxSummary.ReportDefinition.ReportObjects.Item("Field7")
            fAuxTotalField = CType(rAuxTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fAuxTotalField.ObjectFormat.EnableSuppress = True

            Dim rAuxTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rAuxTotalText = SRAuxSummary.ReportDefinition.ReportObjects.Item("Text7")
            fAuxTotalText = CType(rAuxTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fAuxTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rAuxGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fAuxGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rAuxGrandTotalField = SRAuxSummary.ReportDefinition.ReportObjects.Item("Field10")
                fAuxGrandTotalField = CType(rAuxGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fAuxGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

            Dim SRClubHouseSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRClubHouseSummary = myReport.OpenSubreport("rptClubHouseSummary.rpt")

            Dim rClubHPriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHPriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rClubHPriceField = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Field3")
            fClubHPriceField = CType(rClubHPriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fClubHPriceField.ObjectFormat.EnableSuppress = True

            Dim rClubHPriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHPriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rClubHPriceText = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Text3")
            fClubHPriceText = CType(rClubHPriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fClubHPriceText.ObjectFormat.EnableSuppress = True

            Dim rClubHTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rClubHTotalField = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Field5")
            fClubHTotalField = CType(rClubHTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fClubHTotalField.ObjectFormat.EnableSuppress = True

            Dim rClubHTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rClubHTotalText = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Text5")
            fClubHTotalText = CType(rClubHTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fClubHTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rClubHGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fClubHGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rClubHGrandTotalField = SRClubHouseSummary.ReportDefinition.ReportObjects.Item("Field8")
                fClubHGrandTotalField = CType(rClubHGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fClubHGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

            Dim SRCommonDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRCommonDetail = myReport.OpenSubreport("rptCommonDetails.rpt")

            Dim rCommonHPDriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCommonHPDriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rCommonHPDriceField = SRCommonDetail.ReportDefinition.ReportObjects.Item("Field9")
            fCommonHPDriceField = CType(rCommonHPDriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fCommonHPDriceField.ObjectFormat.EnableSuppress = True

            Dim rCommonHPDriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCommonHPDriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rCommonHPDriceText = SRCommonDetail.ReportDefinition.ReportObjects.Item("Text10")
            fCommonHPDriceText = CType(rCommonHPDriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fCommonHPDriceText.ObjectFormat.EnableSuppress = True

            Dim rCommonDTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCommonDTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rCommonDTotalField = SRCommonDetail.ReportDefinition.ReportObjects.Item("Field10")
            fCommonDTotalField = CType(rCommonDTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fCommonDTotalField.ObjectFormat.EnableSuppress = True

            Dim rCommonDTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCommonDTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rCommonDTotalText = SRCommonDetail.ReportDefinition.ReportObjects.Item("Text11")
            fCommonDTotalText = CType(rCommonDTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fCommonDTotalText.ObjectFormat.EnableSuppress = True

            Dim rCommonDGTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fCommonDGTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rCommonDGTotalField = SRCommonDetail.ReportDefinition.ReportObjects.Item("Field13")
            fCommonDGTotalField = CType(rCommonDGTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fCommonDGTotalField.ObjectFormat.EnableSuppress = True

            Dim SRClubHouseDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRClubHouseDetail = myReport.OpenSubreport("b_rptClubHouseDetails.rpt")

            Dim rClubHouseHPDriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHouseHPDriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rClubHouseHPDriceField = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Field9")
            fClubHouseHPDriceField = CType(rClubHouseHPDriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fClubHouseHPDriceField.ObjectFormat.EnableSuppress = True

            Dim rClubHouseHPDriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHouseHPDriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rClubHouseHPDriceText = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Text9")
            fClubHouseHPDriceText = CType(rClubHouseHPDriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fClubHouseHPDriceText.ObjectFormat.EnableSuppress = True

            Dim rClubHouseDTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHouseDTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rClubHouseDTotalField = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Field13")
            fClubHouseDTotalField = CType(rClubHouseDTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fClubHouseDTotalField.ObjectFormat.EnableSuppress = True

            Dim rClubHouseDTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHouseDTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rClubHouseDTotalText = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Text10")
            fClubHouseDTotalText = CType(rClubHouseDTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fClubHouseDTotalText.ObjectFormat.EnableSuppress = True

            Dim rClubHouseDGTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fClubHouseDGTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rClubHouseDGTotalField = SRClubHouseDetail.ReportDefinition.ReportObjects.Item("Field10")
            fClubHouseDGTotalField = CType(rClubHouseDGTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fClubHouseDGTotalField.ObjectFormat.EnableSuppress = True

            Dim SRSiteDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRSiteDetail = myReport.OpenSubreport("b_rptSiteDetails.rpt")

            Dim rSiteHPDriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteHPDriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSiteHPDriceField = SRSiteDetail.ReportDefinition.ReportObjects.Item("Field9")
            fSiteHPDriceField = CType(rSiteHPDriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSiteHPDriceField.ObjectFormat.EnableSuppress = True

            Dim rSiteHPDriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteHPDriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rSiteHPDriceText = SRSiteDetail.ReportDefinition.ReportObjects.Item("Text10")
            fSiteHPDriceText = CType(rSiteHPDriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fSiteHPDriceText.ObjectFormat.EnableSuppress = True

            Dim rSiteDTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteDTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSiteDTotalField = SRSiteDetail.ReportDefinition.ReportObjects.Item("Field18")
            fSiteDTotalField = CType(rSiteDTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSiteDTotalField.ObjectFormat.EnableSuppress = True

            Dim rSiteDTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteDTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rSiteDTotalText = SRSiteDetail.ReportDefinition.ReportObjects.Item("Text11")
            fSiteDTotalText = CType(rSiteDTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fSiteDTotalText.ObjectFormat.EnableSuppress = True

            Dim rSiteDGTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteDGTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSiteDGTotalField = SRSiteDetail.ReportDefinition.ReportObjects.Item("Field10")
            fSiteDGTotalField = CType(rSiteDGTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSiteDGTotalField.ObjectFormat.EnableSuppress = True

            Dim SRAuxDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRAuxDetail = myReport.OpenSubreport("b_rptAuxDetails.rpt")

            Dim rAuxHPDriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxHPDriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rAuxHPDriceField = SRAuxDetail.ReportDefinition.ReportObjects.Item("Field9")
            fAuxHPDriceField = CType(rAuxHPDriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fAuxHPDriceField.ObjectFormat.EnableSuppress = True

            Dim rAuxHPDriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxHPDriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rAuxHPDriceText = SRAuxDetail.ReportDefinition.ReportObjects.Item("Text9")
            fAuxHPDriceText = CType(rAuxHPDriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fAuxHPDriceText.ObjectFormat.EnableSuppress = True

            Dim rAuxDTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxDTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rAuxDTotalField = SRAuxDetail.ReportDefinition.ReportObjects.Item("Field15")
            fAuxDTotalField = CType(rAuxDTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fAuxDTotalField.ObjectFormat.EnableSuppress = True

            Dim rAuxDTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxDTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rAuxDTotalText = SRAuxDetail.ReportDefinition.ReportObjects.Item("Text10")
            fAuxDTotalText = CType(rAuxDTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fAuxDTotalText.ObjectFormat.EnableSuppress = True

            Dim rAuxDGTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fAuxDGTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rAuxDGTotalField = SRAuxDetail.ReportDefinition.ReportObjects.Item("Field10")
            fAuxDGTotalField = CType(rAuxDGTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fAuxDGTotalField.ObjectFormat.EnableSuppress = True

            Dim SRUnitDetail As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRUnitDetail = myReport.OpenSubreport("rptUnitDetails.rpt")

            Dim rUnitHPDriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUnitHPDriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rUnitHPDriceField = SRUnitDetail.ReportDefinition.ReportObjects.Item("Field9")
            fUnitHPDriceField = CType(rUnitHPDriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fUnitHPDriceField.ObjectFormat.EnableSuppress = True

            Dim rUnitHPDriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUnitHPDriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rUnitHPDriceText = SRUnitDetail.ReportDefinition.ReportObjects.Item("Text10")
            fUnitHPDriceText = CType(rUnitHPDriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fUnitHPDriceText.ObjectFormat.EnableSuppress = True

            Dim rUnitDTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUnitDTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rUnitDTotalField = SRUnitDetail.ReportDefinition.ReportObjects.Item("Field18")
            fUnitDTotalField = CType(rUnitDTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fUnitDTotalField.ObjectFormat.EnableSuppress = True

            Dim rUnitDTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUnitDTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rUnitDTotalText = SRUnitDetail.ReportDefinition.ReportObjects.Item("Text11")
            fUnitDTotalText = CType(rUnitDTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fUnitDTotalText.ObjectFormat.EnableSuppress = True

            Dim rUnitDGTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fUnitDGTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rUnitDGTotalField = SRUnitDetail.ReportDefinition.ReportObjects.Item("Field19")
            fUnitDGTotalField = CType(rUnitDGTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fUnitDGTotalField.ObjectFormat.EnableSuppress = True

            Dim SRSiteSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRSiteSummary = myReport.OpenSubreport("rptSiteSummary.rpt")

            Dim rSitePriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSitePriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSitePriceField = SRSiteSummary.ReportDefinition.ReportObjects.Item("Field3")
            fSitePriceField = CType(rSitePriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSitePriceField.ObjectFormat.EnableSuppress = True

            Dim rSitePriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSitePriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rSitePriceText = SRSiteSummary.ReportDefinition.ReportObjects.Item("Text3")
            fSitePriceText = CType(rSitePriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fSitePriceText.ObjectFormat.EnableSuppress = True

            Dim rSiteTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSiteTotalField = SRSiteSummary.ReportDefinition.ReportObjects.Item("Field5")
            fSiteTotalField = CType(rSiteTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSiteTotalField.ObjectFormat.EnableSuppress = True

            Dim rSiteTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSiteTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rSiteTotalText = SRSiteSummary.ReportDefinition.ReportObjects.Item("Text5")
            fSiteTotalText = CType(rSiteTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fSiteTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rSiteGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fSiteGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rSiteGrandTotalField = SRSiteSummary.ReportDefinition.ReportObjects.Item("Field8")
                fSiteGrandTotalField = CType(rSiteGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fSiteGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

            Dim SRBldgBOM As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRBldgBOM = myReport.OpenSubreport("rptBldgBOM.rpt")

            Dim rBldgBOMPriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fBldgBOMPriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rBldgBOMPriceField = SRBldgBOM.ReportDefinition.ReportObjects.Item("Field12")
            fBldgBOMPriceField = CType(rBldgBOMPriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fBldgBOMPriceField.ObjectFormat.EnableSuppress = True

            Dim rBldgBOMPriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fBldgBOMPriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rBldgBOMPriceText = SRBldgBOM.ReportDefinition.ReportObjects.Item("Text13")
            fBldgBOMPriceText = CType(rBldgBOMPriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fBldgBOMPriceText.ObjectFormat.EnableSuppress = True

            Dim rBldgBOMTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fBldgBOMTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rBldgBOMTotalField = SRBldgBOM.ReportDefinition.ReportObjects.Item("Field14")
            fBldgBOMTotalField = CType(rBldgBOMTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fBldgBOMTotalField.ObjectFormat.EnableSuppress = True

            Dim rBldgBOMTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fBldgBOMTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rBldgBOMTotalText = SRBldgBOM.ReportDefinition.ReportObjects.Item("Text15")
            fBldgBOMTotalText = CType(rBldgBOMTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fBldgBOMTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rBldgBOMGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fBldgBOMGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rBldgBOMGrandTotalField = SRBldgBOM.ReportDefinition.ReportObjects.Item("Field18")
                fBldgBOMGrandTotalField = CType(rBldgBOMGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fBldgBOMGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

            Dim SRTypeSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRTypeSummary = myReport.OpenSubreport("rptTypeSummary.rpt")

            Dim rTypePriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fTypePriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rTypePriceField = SRTypeSummary.ReportDefinition.ReportObjects.Item("Field5")
            fTypePriceField = CType(rTypePriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fTypePriceField.ObjectFormat.EnableSuppress = True

            Dim rTypePriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fTypePriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rTypePriceText = SRTypeSummary.ReportDefinition.ReportObjects.Item("Text5")
            fTypePriceText = CType(rTypePriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fTypePriceText.ObjectFormat.EnableSuppress = True

            Dim rTypeTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fTypeTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rTypeTotalField = SRTypeSummary.ReportDefinition.ReportObjects.Item("Field6")
            fTypeTotalField = CType(rTypeTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fTypeTotalField.ObjectFormat.EnableSuppress = True

            Dim rTypeTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fTypeTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rTypeTotalText = SRTypeSummary.ReportDefinition.ReportObjects.Item("Text6")
            fTypeTotalText = CType(rTypeTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fTypeTotalText.ObjectFormat.EnableSuppress = True

            Dim SRNoTypeSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRNoTypeSummary = myReport.OpenSubreport("rptTypeSummaryNotype.rpt")

            Dim rNoTypePriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fNoTypePriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rNoTypePriceField = SRNoTypeSummary.ReportDefinition.ReportObjects.Item("Field5")
            fNoTypePriceField = CType(rNoTypePriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fNoTypePriceField.ObjectFormat.EnableSuppress = True

            Dim rNoTypePriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fNoTypePriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rNoTypePriceText = SRNoTypeSummary.ReportDefinition.ReportObjects.Item("Text5")
            fNoTypePriceText = CType(rNoTypePriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fNoTypePriceText.ObjectFormat.EnableSuppress = True

            Dim rNoTypeTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fNoTypeTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rNoTypeTotalField = SRNoTypeSummary.ReportDefinition.ReportObjects.Item("Field6")
            fNoTypeTotalField = CType(rNoTypeTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fNoTypeTotalField.ObjectFormat.EnableSuppress = True

            Dim rNoTypeTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fNoTypeTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rNoTypeTotalText = SRNoTypeSummary.ReportDefinition.ReportObjects.Item("Text6")
            fNoTypeTotalText = CType(rNoTypeTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fNoTypeTotalText.ObjectFormat.EnableSuppress = True

            Dim SRMainSummary As CrystalDecisions.CrystalReports.Engine.ReportDocument
            SRMainSummary = myReport.OpenSubreport("rptBidSummary.rpt")

            Dim rSummPriceField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSummPriceField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSummPriceField = SRMainSummary.ReportDefinition.ReportObjects.Item("Field7")
            fSummPriceField = CType(rSummPriceField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSummPriceField.ObjectFormat.EnableSuppress = True

            Dim rSummPriceText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSummPriceText As CrystalDecisions.CrystalReports.Engine.TextObject
            rSummPriceText = SRMainSummary.ReportDefinition.ReportObjects.Item("Text8")
            fSummPriceText = CType(rSummPriceText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fSummPriceText.ObjectFormat.EnableSuppress = True

            Dim rSummTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSummTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
            rSummTotalField = SRMainSummary.ReportDefinition.ReportObjects.Item("Field22")
            fSummTotalField = CType(rSummTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
            fSummTotalField.ObjectFormat.EnableSuppress = True

            Dim rSummTotalText As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim fSummTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
            rSummTotalText = SRMainSummary.ReportDefinition.ReportObjects.Item("Text4")
            fSummTotalText = CType(rSummTotalText, CrystalDecisions.CrystalReports.Engine.TextObject)
            fSummTotalText.ObjectFormat.EnableSuppress = True

            If cbxGrandTotals.Checked = False Then
                Dim rSummGrandTotalField As CrystalDecisions.CrystalReports.Engine.ReportObject
                Dim fSummGrandTotalField As CrystalDecisions.CrystalReports.Engine.FieldObject
                rSummGrandTotalField = SRMainSummary.ReportDefinition.ReportObjects.Item("Field24")
                fSummGrandTotalField = CType(rSummGrandTotalField, CrystalDecisions.CrystalReports.Engine.FieldObject)
                fSummGrandTotalField.ObjectFormat.EnableSuppress = True
            End If

        End If

        CrystalReportViewer1.ReportSource = myReport
    End Sub

    Private Sub LoadSubTotals()
        If cbxPageTwoTotals.Checked = True Then
            BidReportingM.LoadSubTotals(strUnits, strClubhouse, strSite, strCommon, strAuxiliary, strPreTaxTotal, strTaxRate, strTaxTotal, strGrandTotal, cnn)
        End If
    End Sub

    Private Sub LoadCounts()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdUnitCount As New SqlCommand("rptUnitCount", cnn)
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drUnitCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drUnitCount = cmdUnitCount.ExecuteReader
            drUnitCount.Read()
            strrptUnitCount = drUnitCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drUnitCount.Close()
            cnn.Close()
        End Try

        Dim cmdBldgCount As New SqlCommand("rptBldgCount", cnn)
        cmdBldgCount.CommandType = CommandType.StoredProcedure
        Dim prmBldgBidID As SqlParameter = cmdBldgCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBldgBidID.Value = frmMain.intBidID
        Dim drBldgCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drBldgCount = cmdBldgCount.ExecuteReader
            drBldgCount.Read()
            strrptBldgCount = drBldgCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drBldgCount.Close()
            cnn.Close()
        End Try

        Dim strBidInfo As String
        strBidInfo = "SELECT Description, Location, Address, City, State, Zip, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, ProjectDate, ProjectContact, AuxLabel FROM Project WHERE ProjectID = "
        strBidInfo = strBidInfo & frmMain.intBidID
        Dim cmdClientID As New SqlCommand(strBidInfo, cnn)
        cmdClientID.CommandType = CommandType.Text
        Dim drBid As SqlDataReader = Nothing
        Try
            cnn.Open()
            drBid = cmdClientID.ExecuteReader
            drBid.Read()
            
            If drBid.Item("AuxLabel").ToString = "" Then
                strAuxLabel = "Auxiliary"
            Else
                strAuxLabel = drBid.Item("AuxLabel").ToString
            End If
        Catch ex As Exception
            MsgBox("Error Loading Project Info")
        Finally
            drBid.Close()
            cnn.Close()
        End Try
    End Sub

    Private Sub HideAll()
        cbxComments.Visible = False
        cbxBldgType.Visible = False
        cbxType.Visible = False
        cbxUnitSummary.Visible = False
        cbxCommonSummary.Visible = False
        cbxClubHouseSummary.Visible = False
        cbxAuxiliarySummary.Visible = False
        cbxSiteSummayr.Visible = False
        cbxUnitDetails.Visible = False
        cbxCommonDetails.Visible = False
        cbxClubhouseDetails.Visible = False
        cbxSiteDetails.Visible = False
        cbxAuxDetails.Visible = False
        cbxBldgBOM.Visible = False
        cbxSummary.Visible = False
        cbxAlternates.Visible = False
        cbxDescriptions.Visible = False
        cbxPrices.Visible = False
        cbxGrandTotals.Visible = False
        cbxPageTwoTotals.Visible = False
        cbxTBD.Visible = False
        btnShowReport.Visible = False
        cboOrder.Visible = False
        lblSections.Visible = False
        lblOrder.Visible = False
        lblColumns.Visible = False
        lblPageTwoTotals.Visible = False
    End Sub

    Private Sub btnShowReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowReport.Click
        Me.Cursor = Cursors.WaitCursor
        LoadReport()
        HideAll()
        CrystalReportViewer1.Dock = DockStyle.Fill
        CrystalReportViewer1.Visible = True
        Me.Cursor = Cursors.Default
    End Sub


    
End Class
