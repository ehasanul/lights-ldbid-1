Imports System.Data.SqlClient
'Job Reporting Module
'Eventually this will get boiled down and combined with the bid module
'and become a reporting class
Module JobReportingM

    Public Function RunUnitTypeDetailSP(ByVal UnitType As String, ByVal ProjectID As String, ByVal connection As SqlConnection,
                                        ByVal TableName As String) As DataSet
        Dim cmdUnitTypeDetail As New SqlCommand
        cmdUnitTypeDetail.Connection = connection
        cmdUnitTypeDetail.CommandText = "UnitDetail"
        cmdUnitTypeDetail.CommandType = CommandType.StoredProcedure
        Dim prmUnitTypedetailProjectID As SqlParameter = cmdUnitTypeDetail.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitTypedetailProjectID.Value = ProjectID
        Dim prmUnitTypeDetailUnitType As SqlParameter = cmdUnitTypeDetail.Parameters.Add("@UnitType", SqlDbType.Char)
        prmUnitTypeDetailUnitType.Value = UnitType
        Dim daUnitTypeDetail As New SqlDataAdapter(cmdUnitTypeDetail)
        Dim dsUnitTypeDetail As New DataSet
        daUnitTypeDetail.Fill(dsUnitTypeDetail, TableName)
        connection.Close()
        Return dsUnitTypeDetail
    End Function
End Module
