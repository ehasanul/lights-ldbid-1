Imports System.Data
Imports System.Data.SqlClient
Public Class frmBidExportBOM
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim strBidNum As String
    Dim strDate As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxBOM As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUnit As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClubhouse As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCommon As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAux As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSite As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBldg As System.Windows.Forms.CheckBox
    Friend WithEvents cboBldg As System.Windows.Forms.ComboBox
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents cbxPartVsUnit As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBldgVsUnit As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbxBOM = New System.Windows.Forms.CheckBox
        Me.cbxUnit = New System.Windows.Forms.CheckBox
        Me.cbxClubhouse = New System.Windows.Forms.CheckBox
        Me.cbxCommon = New System.Windows.Forms.CheckBox
        Me.cbxAux = New System.Windows.Forms.CheckBox
        Me.cbxSite = New System.Windows.Forms.CheckBox
        Me.cbxBldg = New System.Windows.Forms.CheckBox
        Me.cboBldg = New System.Windows.Forms.ComboBox
        Me.btnExport = New System.Windows.Forms.Button
        Me.cbxPartVsUnit = New System.Windows.Forms.CheckBox
        Me.cbxBldgVsUnit = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(20, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(392, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "The Files Generatd By This Module Will Be Located In Your C:\Export Folder "
        '
        'cbxBOM
        '
        Me.cbxBOM.Location = New System.Drawing.Point(20, 44)
        Me.cbxBOM.Name = "cbxBOM"
        Me.cbxBOM.Size = New System.Drawing.Size(156, 24)
        Me.cbxBOM.TabIndex = 1
        Me.cbxBOM.Text = "Total Bid BOM"
        '
        'cbxUnit
        '
        Me.cbxUnit.Location = New System.Drawing.Point(20, 68)
        Me.cbxUnit.Name = "cbxUnit"
        Me.cbxUnit.Size = New System.Drawing.Size(156, 24)
        Me.cbxUnit.TabIndex = 2
        Me.cbxUnit.Text = "Units BOM"
        '
        'cbxClubhouse
        '
        Me.cbxClubhouse.Location = New System.Drawing.Point(20, 92)
        Me.cbxClubhouse.Name = "cbxClubhouse"
        Me.cbxClubhouse.Size = New System.Drawing.Size(156, 24)
        Me.cbxClubhouse.TabIndex = 3
        Me.cbxClubhouse.Text = "Clubhouse BOM"
        '
        'cbxCommon
        '
        Me.cbxCommon.Location = New System.Drawing.Point(20, 116)
        Me.cbxCommon.Name = "cbxCommon"
        Me.cbxCommon.Size = New System.Drawing.Size(156, 24)
        Me.cbxCommon.TabIndex = 4
        Me.cbxCommon.Text = "Common BOM"
        '
        'cbxAux
        '
        Me.cbxAux.Location = New System.Drawing.Point(20, 140)
        Me.cbxAux.Name = "cbxAux"
        Me.cbxAux.Size = New System.Drawing.Size(156, 24)
        Me.cbxAux.TabIndex = 5
        Me.cbxAux.Text = "Auxiliary BOM"
        '
        'cbxSite
        '
        Me.cbxSite.Location = New System.Drawing.Point(20, 164)
        Me.cbxSite.Name = "cbxSite"
        Me.cbxSite.Size = New System.Drawing.Size(156, 24)
        Me.cbxSite.TabIndex = 6
        Me.cbxSite.Text = "Site BOM"
        '
        'cbxBldg
        '
        Me.cbxBldg.Location = New System.Drawing.Point(232, 44)
        Me.cbxBldg.Name = "cbxBldg"
        Me.cbxBldg.Size = New System.Drawing.Size(196, 24)
        Me.cbxBldg.TabIndex = 7
        Me.cbxBldg.Text = "Building BOM"
        '
        'cboBldg
        '
        Me.cboBldg.Location = New System.Drawing.Point(232, 68)
        Me.cboBldg.Name = "cboBldg"
        Me.cboBldg.Size = New System.Drawing.Size(196, 21)
        Me.cboBldg.TabIndex = 8
        Me.cboBldg.Visible = False
        '
        'btnExport
        '
        Me.btnExport.Location = New System.Drawing.Point(232, 160)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(196, 24)
        Me.btnExport.TabIndex = 9
        Me.btnExport.Text = "Export"
        '
        'cbxPartVsUnit
        '
        Me.cbxPartVsUnit.Location = New System.Drawing.Point(232, 100)
        Me.cbxPartVsUnit.Name = "cbxPartVsUnit"
        Me.cbxPartVsUnit.Size = New System.Drawing.Size(132, 20)
        Me.cbxPartVsUnit.TabIndex = 10
        Me.cbxPartVsUnit.Text = "CT Parts vs. Unit"
        '
        'cbxBldgVsUnit
        '
        Me.cbxBldgVsUnit.Location = New System.Drawing.Point(232, 132)
        Me.cbxBldgVsUnit.Name = "cbxBldgVsUnit"
        Me.cbxBldgVsUnit.Size = New System.Drawing.Size(136, 20)
        Me.cbxBldgVsUnit.TabIndex = 11
        Me.cbxBldgVsUnit.Text = "CT Bldg vs. Unit Cnt"
        '
        'frmBidExportBOM
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(444, 206)
        Me.Controls.Add(Me.cbxBldgVsUnit)
        Me.Controls.Add(Me.cbxPartVsUnit)
        Me.Controls.Add(Me.btnExport)
        Me.Controls.Add(Me.cboBldg)
        Me.Controls.Add(Me.cbxBldg)
        Me.Controls.Add(Me.cbxSite)
        Me.Controls.Add(Me.cbxAux)
        Me.Controls.Add(Me.cbxCommon)
        Me.Controls.Add(Me.cbxClubhouse)
        Me.Controls.Add(Me.cbxUnit)
        Me.Controls.Add(Me.cbxBOM)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "frmBidExportBOM"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Export Bid Bill Of Materials (BOM)"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmBidExportBOM_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadBldgs()
        bidTitles()
    End Sub

    Private Sub BidTitles()
        strBidNum = "C:\Export\" & frmMain.intBidID.ToString & "-"
        strDate = "-" & Month(Now) & Microsoft.VisualBasic.DateAndTime.Day(Now) & Microsoft.VisualBasic.Right(Year(Now).ToString, 2) & ".xml"

    End Sub
    Private Sub LoadBldgs()
        'bldg
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadBldg As New SqlCommand("Buildings", cnn)
        cmdLoadBldg.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBldg As New SqlDataAdapter(cmdLoadBldg)
        Dim dsBldg As New DataSet
        Try
            daBldg.Fill(dsBldg, "Bldg")
        Catch ex As Exception
            MsgBox("Error Loading Bldgs")
        End Try

        cboBldg.DataSource = dsBldg.Tables("Bldg")
        cboBldg.DisplayMember = "BLDG"
        cboBldg.ValueMember = "BldgID"
        'blnStartBldg = False
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If

    End Sub

    Private Sub ExportBidBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "LineItemSummaryExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_LineItemSummaryExp")
        Catch ex As Exception
            MsgBox("Failed to Fill LineItemSummaryExp")
        End Try
        Dim strPath As String
        strPath = strBidNum & "BidBOM" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
    End Sub

    Private Sub ExportUnitBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "UnitSummaryExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_UnitSummaryExp")
        Catch ex As Exception
            MsgBox("Failed to Fill UnitExp: " + ex.ToString)
        End Try
        Dim strPath As String
        strPath = strBidNum & "Unit" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
    End Sub

    Private Sub ExportClubhouseBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "ClubHouseSummaryExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_ClubHouseSummaryExp")
        Catch ex As Exception
            MsgBox("Failed to Fill ClubhouseExp: " + ex.ToString)
        End Try
        Dim strPath As String
        strPath = strBidNum & "CH" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
    End Sub

    Private Sub ExportCommonBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "CommonSummaryExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_CommonSummaryExp")
        Catch ex As Exception
            MsgBox("Failed to Fill CommonExp")
        End Try
        Dim strPath As String
        strPath = strBidNum & "COM" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
    End Sub

    Private Sub ExportAuxBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "AuxillarySummaryExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_AuxillarySummaryExp")
        Catch ex As Exception
            MsgBox("Failed to Fill AuxExp")
        End Try
        Dim strPath As String
        strPath = strBidNum & "Aux" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
    End Sub

    Private Sub ExportSiteBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "SiteSummaryExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_SiteSummaryExp")
        Catch ex As Exception
            MsgBox("Failed to Fill SiteExp")
        End Try
        Dim strPath As String
        strPath = strBidNum & "Site" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
    End Sub

    Private Sub ExportBldgBOM()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "BldgBOMExp"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBldgID As SqlParameter = cmdBOM.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = cboBldg.SelectedValue

        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_BldgBOMExp")
        Catch ex As Exception
            MsgBox("Failed to Fill BldgBOMExp")
        End Try
        Dim strPath As String
        strPath = strBidNum & "B-" & cboBldg.Text.ToString & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
        MsgBox("This File is saved as " & strPath)
    End Sub

    Private Sub ExportBldgCT()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "CTBldgVSUCnt"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daBOM As New SqlDataAdapter(cmdBOM)

        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_CTBldgVSUCnt")
        Catch ex As Exception
            MsgBox("Failed to Fill CTBldgVSUcnt")
        End Try
        Dim strPath As String
        strPath = strBidNum & "-CTBldg" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
        MsgBox("This File is saved as " & strPath)
    End Sub

    Private Sub ExportUnitCT()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBOM As String
        strBOM = "CTPartVSUType"
        Dim cmdBOM As New SqlCommand
        cmdBOM.Connection = cnn
        cmdBOM.CommandText = strBOM
        cmdBOM.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdBOM.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daBOM As New SqlDataAdapter(cmdBOM)


        Dim dsBOM As New DataSet
        Try
            daBOM.Fill(dsBOM, "b_CTPartVSUType")
        Catch ex As Exception
            MsgBox("Failed to Fill CTPartVSUType")
        End Try
        Dim strPath As String
        strPath = strBidNum & "-CTPart" & strDate
        CreateExportDirectory()
        dsBOM.WriteXml(strPath)
        MsgBox("This File is saved as " & strPath)
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Me.Cursor = Cursors.WaitCursor
        If cbxBOM.Checked Then
            ExportBidBOM()
        End If
        If cbxUnit.Checked Then
            ExportUnitBOM()
        End If
        If cbxClubhouse.Checked Then
            ExportClubhouseBOM()
        End If
        If cbxCommon.Checked Then
            ExportCommonBOM()
        End If
        If cbxAux.Checked Then
            ExportAuxBOM()
        End If
        If cbxSite.Checked Then
            ExportSiteBOM()
        End If
        If cbxBldg.Checked Then
            ExportBldgBOM()
        End If
        If cbxBldgVsUnit.Checked Then
            ExportBldgCT()
        End If
        If cbxPartVsUnit.Checked Then
            ExportUnitCT()
        End If
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub cbxBldg_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxBldg.CheckedChanged
        If cbxBldg.Checked Then
            cboBldg.Visible = True
        Else
            cboBldg.Visible = False
        End If
    End Sub

    Private Sub CreateExportDirectory()
        'Eventually move this to a configuration setting
        If (Not System.IO.Directory.Exists("C:\Export")) Then
            System.IO.Directory.CreateDirectory("C:\Export")
        End If
    End Sub
End Class
