Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmVwJPartsByDates
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents txtStart As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEnd As System.Windows.Forms.TextBox
    Friend WithEvents cmdLoad As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.txtStart = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtEnd = New System.Windows.Forms.TextBox
        Me.cmdLoad = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(700, 334)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'txtStart
        '
        Me.txtStart.Location = New System.Drawing.Point(360, 4)
        Me.txtStart.Name = "txtStart"
        Me.txtStart.Size = New System.Drawing.Size(76, 20)
        Me.txtStart.TabIndex = 1
        Me.txtStart.Text = ""
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(328, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Start"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(456, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "End"
        '
        'txtEnd
        '
        Me.txtEnd.Location = New System.Drawing.Point(480, 4)
        Me.txtEnd.Name = "txtEnd"
        Me.txtEnd.Size = New System.Drawing.Size(64, 20)
        Me.txtEnd.TabIndex = 4
        Me.txtEnd.Text = ""
        '
        'cmdLoad
        '
        Me.cmdLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdLoad.Location = New System.Drawing.Point(580, 4)
        Me.cmdLoad.Name = "cmdLoad"
        Me.cmdLoad.Size = New System.Drawing.Size(72, 20)
        Me.cmdLoad.TabIndex = 5
        Me.cmdLoad.Text = "Load Report"
        '
        'frmVwJPartsByDates
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(700, 334)
        Me.Controls.Add(Me.cmdLoad)
        Me.Controls.Add(Me.txtEnd)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtStart)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.MinimizeBox = False
        Me.Name = "frmVwJPartsByDates"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Parts By Date Range"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub LoadReport()
        Dim projectType As String
        If frmMain.comingFromVerbal Then
            projectType = "verbal"
        Else
            projectType = "job"
        End If

        cnn.ConnectionString = frmMain.strConnect

        Dim strCommand As String
        strCommand = "PartsByDateRange"
        Dim cmdCommand As New SqlCommand
        cmdCommand.Connection = cnn
        cmdCommand.CommandText = strCommand
        cmdCommand.CommandType = CommandType.StoredProcedure
        Dim prmStateDate As SqlParameter = cmdCommand.Parameters.Add("@StartDate", SqlDbType.DateTime)
        prmStateDate.Value = txtStart.Text
        Dim prmEndDate As SqlParameter = cmdCommand.Parameters.Add("@EndDate", SqlDbType.DateTime)
        prmEndDate.Value = txtEnd.Text
        Dim prmProjectType As SqlParameter = cmdCommand.Parameters.Add("@ProjectType", SqlDbType.VarChar, 15)
        prmProjectType.Value = projectType

        Dim daIntro As New SqlDataAdapter(cmdCommand)

        Dim dsIntro As New DataSet
        daIntro.Fill(dsIntro, "PartsByDateRange")

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        myReport = New j_rPartsByDate

        myReport.SetDataSource(dsIntro.Tables("PartsByDateRange"))

        Dim StartDate As CrystalDecisions.CrystalReports.Engine.TextObject
        StartDate = CType(myReport.ReportDefinition.ReportObjects.Item("txtStartDate"), CrystalDecisions.CrystalReports.Engine.TextObject)
        StartDate.Text = txtStart.Text

        Dim EndDate As CrystalDecisions.CrystalReports.Engine.TextObject
        EndDate = CType(myReport.ReportDefinition.ReportObjects.Item("txtEndDate"), CrystalDecisions.CrystalReports.Engine.TextObject)
        EndDate.Text = txtEnd.Text

        CrystalReportViewer1.ReportSource = myReport
    End Sub

    Private Sub cmdLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoad.Click
        If Not IsDate(txtStart.Text) Then
            MsgBox("You Must Enter a Proper Date in the Start Box! i.e. 1/15/2006")
            Exit Sub
        End If

        If Not IsDate(txtEnd.Text) Then
            MsgBox("You Must Enter a Proper Date in the End Box! i.e. 1/15/2006")
            Exit Sub
        End If
        LoadReport()
    End Sub
End Class
