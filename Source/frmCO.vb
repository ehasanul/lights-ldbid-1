Public Class frmCO
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtAuthor As System.Windows.Forms.TextBox
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtAuthor = New System.Windows.Forms.TextBox
        Me.txtNotes = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'txtAuthor
        '
        Me.txtAuthor.Location = New System.Drawing.Point(60, 64)
        Me.txtAuthor.Name = "txtAuthor"
        Me.txtAuthor.TabIndex = 0
        Me.txtAuthor.Text = ""
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(64, 108)
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.Size = New System.Drawing.Size(336, 76)
        Me.txtNotes.TabIndex = 1
        Me.txtNotes.Text = ""
        '
        'frmCO
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(468, 218)
        Me.Controls.Add(Me.txtNotes)
        Me.Controls.Add(Me.txtAuthor)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCO"
        Me.Text = "frmCO"
        Me.ResumeLayout(False)

    End Sub

#End Region

End Class
