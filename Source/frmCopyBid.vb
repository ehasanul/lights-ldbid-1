Imports System.Data
Imports System.Data.SqlClient
Public Class frmCopyBid
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim dsBids As New DataSet
    Public Shared blnStartUnit As Boolean 'Tests Startup Condition in LoadUnits
    Dim blnStartBldg As Boolean 'Tests Startup Condition in LoadBldgs
    Public Shared intUnitID As Integer
    Public Shared blnUnitReturn As Boolean
    Dim intUnitEditing As Integer
    Dim intNewBidID As Integer
    Dim intOldUnitID As Integer
    Dim intNewUnitID As Integer
    Dim intOldBldgID As Integer
    Dim intOldAltID As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboBid As System.Windows.Forms.ComboBox
    Friend WithEvents cboClients As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCLemail As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLFAX As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLZip As System.Windows.Forms.TextBox
    Friend WithEvents txtCLState As System.Windows.Forms.TextBox
    Friend WithEvents txtCLCity As System.Windows.Forms.TextBox
    Friend WithEvents txtCLAddr2 As System.Windows.Forms.TextBox
    Friend WithEvents txtClAddr1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLContact As System.Windows.Forms.TextBox
    Friend WithEvents txtCLName As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtbDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtBNotes As System.Windows.Forms.TextBox
    Friend WithEvents txtBZip As System.Windows.Forms.TextBox
    Friend WithEvents txtBAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtBCity As System.Windows.Forms.TextBox
    Friend WithEvents txtBState As System.Windows.Forms.TextBox
    Friend WithEvents txtBLocation As System.Windows.Forms.TextBox
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents lblSalesTaxRate As System.Windows.Forms.Label
    Friend WithEvents txtSalesTaxRate As System.Windows.Forms.TextBox
    Friend WithEvents txtRevDate As System.Windows.Forms.TextBox
    Friend WithEvents txtBluePrintDate As System.Windows.Forms.TextBox
    Friend WithEvents LBLRevDate As System.Windows.Forms.Label
    Friend WithEvents lblBluePrintDate As System.Windows.Forms.Label
    Friend WithEvents txtLDEstimator As System.Windows.Forms.TextBox
    Friend WithEvents lblLDEstimator As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboBid = New System.Windows.Forms.ComboBox()
        Me.cboClients = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCLemail = New System.Windows.Forms.TextBox()
        Me.txtCLPhone2 = New System.Windows.Forms.TextBox()
        Me.txtCLFAX = New System.Windows.Forms.TextBox()
        Me.txtCLPhone1 = New System.Windows.Forms.TextBox()
        Me.txtCLZip = New System.Windows.Forms.TextBox()
        Me.txtCLState = New System.Windows.Forms.TextBox()
        Me.txtCLCity = New System.Windows.Forms.TextBox()
        Me.txtCLAddr2 = New System.Windows.Forms.TextBox()
        Me.txtClAddr1 = New System.Windows.Forms.TextBox()
        Me.txtCLContact = New System.Windows.Forms.TextBox()
        Me.txtCLName = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtbDescription = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtBNotes = New System.Windows.Forms.TextBox()
        Me.txtBZip = New System.Windows.Forms.TextBox()
        Me.txtBAddress = New System.Windows.Forms.TextBox()
        Me.txtBCity = New System.Windows.Forms.TextBox()
        Me.txtBState = New System.Windows.Forms.TextBox()
        Me.txtBLocation = New System.Windows.Forms.TextBox()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.lblSalesTaxRate = New System.Windows.Forms.Label()
        Me.txtSalesTaxRate = New System.Windows.Forms.TextBox()
        Me.txtRevDate = New System.Windows.Forms.TextBox()
        Me.txtBluePrintDate = New System.Windows.Forms.TextBox()
        Me.LBLRevDate = New System.Windows.Forms.Label()
        Me.lblBluePrintDate = New System.Windows.Forms.Label()
        Me.txtLDEstimator = New System.Windows.Forms.TextBox()
        Me.lblLDEstimator = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cboBid
        '
        Me.cboBid.Location = New System.Drawing.Point(16, 8)
        Me.cboBid.Name = "cboBid"
        Me.cboBid.Size = New System.Drawing.Size(352, 21)
        Me.cboBid.TabIndex = 2
        Me.cboBid.Text = "Select Bid "
        '
        'cboClients
        '
        Me.cboClients.Location = New System.Drawing.Point(20, 272)
        Me.cboClients.Name = "cboClients"
        Me.cboClients.Size = New System.Drawing.Size(336, 21)
        Me.cboClients.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(316, 380)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 16)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "e-mail"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(316, 360)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 16)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "FAX"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(316, 332)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 16)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Phone"
        '
        'txtCLemail
        '
        Me.txtCLemail.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLemail.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLemail.Location = New System.Drawing.Point(372, 368)
        Me.txtCLemail.Name = "txtCLemail"
        Me.txtCLemail.ReadOnly = True
        Me.txtCLemail.Size = New System.Drawing.Size(216, 20)
        Me.txtCLemail.TabIndex = 25
        Me.txtCLemail.TabStop = False
        '
        'txtCLPhone2
        '
        Me.txtCLPhone2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone2.Location = New System.Drawing.Point(484, 328)
        Me.txtCLPhone2.Name = "txtCLPhone2"
        Me.txtCLPhone2.ReadOnly = True
        Me.txtCLPhone2.Size = New System.Drawing.Size(104, 20)
        Me.txtCLPhone2.TabIndex = 23
        Me.txtCLPhone2.TabStop = False
        '
        'txtCLFAX
        '
        Me.txtCLFAX.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLFAX.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLFAX.Location = New System.Drawing.Point(372, 348)
        Me.txtCLFAX.Name = "txtCLFAX"
        Me.txtCLFAX.ReadOnly = True
        Me.txtCLFAX.Size = New System.Drawing.Size(112, 20)
        Me.txtCLFAX.TabIndex = 24
        Me.txtCLFAX.TabStop = False
        '
        'txtCLPhone1
        '
        Me.txtCLPhone1.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone1.Location = New System.Drawing.Point(372, 328)
        Me.txtCLPhone1.Name = "txtCLPhone1"
        Me.txtCLPhone1.ReadOnly = True
        Me.txtCLPhone1.Size = New System.Drawing.Size(112, 20)
        Me.txtCLPhone1.TabIndex = 22
        Me.txtCLPhone1.TabStop = False
        '
        'txtCLZip
        '
        Me.txtCLZip.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLZip.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLZip.Location = New System.Drawing.Point(196, 368)
        Me.txtCLZip.Name = "txtCLZip"
        Me.txtCLZip.ReadOnly = True
        Me.txtCLZip.Size = New System.Drawing.Size(80, 20)
        Me.txtCLZip.TabIndex = 20
        Me.txtCLZip.TabStop = False
        '
        'txtCLState
        '
        Me.txtCLState.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLState.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLState.Location = New System.Drawing.Point(156, 368)
        Me.txtCLState.Name = "txtCLState"
        Me.txtCLState.ReadOnly = True
        Me.txtCLState.Size = New System.Drawing.Size(40, 20)
        Me.txtCLState.TabIndex = 19
        Me.txtCLState.TabStop = False
        '
        'txtCLCity
        '
        Me.txtCLCity.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLCity.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLCity.Location = New System.Drawing.Point(20, 368)
        Me.txtCLCity.Name = "txtCLCity"
        Me.txtCLCity.ReadOnly = True
        Me.txtCLCity.Size = New System.Drawing.Size(136, 20)
        Me.txtCLCity.TabIndex = 18
        Me.txtCLCity.TabStop = False
        '
        'txtCLAddr2
        '
        Me.txtCLAddr2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLAddr2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLAddr2.Location = New System.Drawing.Point(20, 348)
        Me.txtCLAddr2.Name = "txtCLAddr2"
        Me.txtCLAddr2.ReadOnly = True
        Me.txtCLAddr2.Size = New System.Drawing.Size(256, 20)
        Me.txtCLAddr2.TabIndex = 17
        Me.txtCLAddr2.TabStop = False
        '
        'txtClAddr1
        '
        Me.txtClAddr1.BackColor = System.Drawing.SystemColors.Info
        Me.txtClAddr1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtClAddr1.Location = New System.Drawing.Point(20, 328)
        Me.txtClAddr1.Name = "txtClAddr1"
        Me.txtClAddr1.ReadOnly = True
        Me.txtClAddr1.Size = New System.Drawing.Size(256, 20)
        Me.txtClAddr1.TabIndex = 16
        Me.txtClAddr1.TabStop = False
        '
        'txtCLContact
        '
        Me.txtCLContact.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLContact.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLContact.Location = New System.Drawing.Point(372, 308)
        Me.txtCLContact.Name = "txtCLContact"
        Me.txtCLContact.ReadOnly = True
        Me.txtCLContact.Size = New System.Drawing.Size(216, 20)
        Me.txtCLContact.TabIndex = 21
        Me.txtCLContact.TabStop = False
        '
        'txtCLName
        '
        Me.txtCLName.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLName.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLName.Location = New System.Drawing.Point(20, 308)
        Me.txtCLName.Name = "txtCLName"
        Me.txtCLName.ReadOnly = True
        Me.txtCLName.Size = New System.Drawing.Size(256, 20)
        Me.txtCLName.TabIndex = 15
        Me.txtCLName.TabStop = False
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(528, 84)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(40, 16)
        Me.Label24.TabIndex = 42
        Me.Label24.Text = "ZIP"
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(476, 84)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(24, 16)
        Me.Label23.TabIndex = 41
        Me.Label23.Text = "ST"
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(320, 84)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 40
        Me.Label22.Text = "City"
        '
        'Label21
        '
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(16, 84)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(48, 16)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "Address"
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(320, 44)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(48, 16)
        Me.Label20.TabIndex = 38
        Me.Label20.Text = "Location"
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(16, 44)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 16)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Desc"
        '
        'txtbDescription
        '
        Me.txtbDescription.Location = New System.Drawing.Point(16, 60)
        Me.txtbDescription.Name = "txtbDescription"
        Me.txtbDescription.Size = New System.Drawing.Size(280, 20)
        Me.txtbDescription.TabIndex = 29
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(16, 124)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 16)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "Notes"
        '
        'txtBNotes
        '
        Me.txtBNotes.Location = New System.Drawing.Point(16, 140)
        Me.txtBNotes.Multiline = True
        Me.txtBNotes.Name = "txtBNotes"
        Me.txtBNotes.Size = New System.Drawing.Size(576, 80)
        Me.txtBNotes.TabIndex = 35
        '
        'txtBZip
        '
        Me.txtBZip.Location = New System.Drawing.Point(512, 100)
        Me.txtBZip.Name = "txtBZip"
        Me.txtBZip.Size = New System.Drawing.Size(80, 20)
        Me.txtBZip.TabIndex = 34
        '
        'txtBAddress
        '
        Me.txtBAddress.Location = New System.Drawing.Point(16, 100)
        Me.txtBAddress.Name = "txtBAddress"
        Me.txtBAddress.Size = New System.Drawing.Size(288, 20)
        Me.txtBAddress.TabIndex = 31
        '
        'txtBCity
        '
        Me.txtBCity.Location = New System.Drawing.Point(320, 100)
        Me.txtBCity.Name = "txtBCity"
        Me.txtBCity.Size = New System.Drawing.Size(144, 20)
        Me.txtBCity.TabIndex = 32
        '
        'txtBState
        '
        Me.txtBState.Location = New System.Drawing.Point(472, 100)
        Me.txtBState.Name = "txtBState"
        Me.txtBState.Size = New System.Drawing.Size(32, 20)
        Me.txtBState.TabIndex = 33
        '
        'txtBLocation
        '
        Me.txtBLocation.Location = New System.Drawing.Point(320, 60)
        Me.txtBLocation.Name = "txtBLocation"
        Me.txtBLocation.Size = New System.Drawing.Size(272, 20)
        Me.txtBLocation.TabIndex = 30
        '
        'btnCopy
        '
        Me.btnCopy.Location = New System.Drawing.Point(28, 420)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(280, 28)
        Me.btnCopy.TabIndex = 43
        Me.btnCopy.Text = "Copy Bid To Selected Client"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(316, 420)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(272, 28)
        Me.btnDelete.TabIndex = 44
        Me.btnDelete.Text = "Delete Bid"
        '
        'lblSalesTaxRate
        '
        Me.lblSalesTaxRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalesTaxRate.Location = New System.Drawing.Point(20, 229)
        Me.lblSalesTaxRate.Name = "lblSalesTaxRate"
        Me.lblSalesTaxRate.Size = New System.Drawing.Size(72, 20)
        Me.lblSalesTaxRate.TabIndex = 50
        Me.lblSalesTaxRate.Text = "Sales Tax Rate"
        '
        'txtSalesTaxRate
        '
        Me.txtSalesTaxRate.Location = New System.Drawing.Point(120, 226)
        Me.txtSalesTaxRate.Name = "txtSalesTaxRate"
        Me.txtSalesTaxRate.Size = New System.Drawing.Size(100, 20)
        Me.txtSalesTaxRate.TabIndex = 49
        Me.txtSalesTaxRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRevDate
        '
        Me.txtRevDate.Location = New System.Drawing.Point(492, 226)
        Me.txtRevDate.Name = "txtRevDate"
        Me.txtRevDate.Size = New System.Drawing.Size(100, 20)
        Me.txtRevDate.TabIndex = 48
        Me.txtRevDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBluePrintDate
        '
        Me.txtBluePrintDate.Location = New System.Drawing.Point(320, 226)
        Me.txtBluePrintDate.Name = "txtBluePrintDate"
        Me.txtBluePrintDate.Size = New System.Drawing.Size(104, 20)
        Me.txtBluePrintDate.TabIndex = 47
        Me.txtBluePrintDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LBLRevDate
        '
        Me.LBLRevDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLRevDate.Location = New System.Drawing.Point(436, 229)
        Me.LBLRevDate.Name = "LBLRevDate"
        Me.LBLRevDate.Size = New System.Drawing.Size(48, 20)
        Me.LBLRevDate.TabIndex = 46
        Me.LBLRevDate.Text = "Rev Date"
        '
        'lblBluePrintDate
        '
        Me.lblBluePrintDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBluePrintDate.Location = New System.Drawing.Point(232, 229)
        Me.lblBluePrintDate.Name = "lblBluePrintDate"
        Me.lblBluePrintDate.Size = New System.Drawing.Size(80, 20)
        Me.lblBluePrintDate.TabIndex = 45
        Me.lblBluePrintDate.Text = "Blue Print Date"
        '
        'txtLDEstimator
        '
        Me.txtLDEstimator.Location = New System.Drawing.Point(376, 28)
        Me.txtLDEstimator.Name = "txtLDEstimator"
        Me.txtLDEstimator.Size = New System.Drawing.Size(216, 20)
        Me.txtLDEstimator.TabIndex = 52
        '
        'lblLDEstimator
        '
        Me.lblLDEstimator.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLDEstimator.Location = New System.Drawing.Point(376, 12)
        Me.lblLDEstimator.Name = "lblLDEstimator"
        Me.lblLDEstimator.Size = New System.Drawing.Size(64, 16)
        Me.lblLDEstimator.TabIndex = 51
        Me.lblLDEstimator.Text = "LD Estimator"
        '
        'frmCopyBid
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(628, 474)
        Me.Controls.Add(Me.txtLDEstimator)
        Me.Controls.Add(Me.lblLDEstimator)
        Me.Controls.Add(Me.lblSalesTaxRate)
        Me.Controls.Add(Me.txtSalesTaxRate)
        Me.Controls.Add(Me.txtRevDate)
        Me.Controls.Add(Me.txtBluePrintDate)
        Me.Controls.Add(Me.LBLRevDate)
        Me.Controls.Add(Me.lblBluePrintDate)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtbDescription)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtBNotes)
        Me.Controls.Add(Me.txtBZip)
        Me.Controls.Add(Me.txtBAddress)
        Me.Controls.Add(Me.txtBCity)
        Me.Controls.Add(Me.txtBState)
        Me.Controls.Add(Me.txtBLocation)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtCLemail)
        Me.Controls.Add(Me.txtCLPhone2)
        Me.Controls.Add(Me.txtCLFAX)
        Me.Controls.Add(Me.txtCLPhone1)
        Me.Controls.Add(Me.txtCLZip)
        Me.Controls.Add(Me.txtCLState)
        Me.Controls.Add(Me.txtCLCity)
        Me.Controls.Add(Me.txtCLAddr2)
        Me.Controls.Add(Me.txtClAddr1)
        Me.Controls.Add(Me.txtCLContact)
        Me.Controls.Add(Me.txtCLName)
        Me.Controls.Add(Me.cboClients)
        Me.Controls.Add(Me.cboBid)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCopyBid"
        Me.Text = "frmCopyBid"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub LoadBids()
        cnn.ConnectionString = frmMain.strConnect
        Dim strBids As String
        strBids = "select 'LD-'+convert(varchar,LDID)+': '+RTRIM(Description)+' '+RTRIM(Location) as bid, " + _
            "ProjectID from Project where ProjectType = 'bid'"
        Dim daBids As New SqlDataAdapter(strBids, cnn)
        Dim dsBids As New DataSet
        daBids.Fill(dsBids, "Project")
        cboBid.DataSource = dsBids.Tables("Project")
        cboBid.DisplayMember = "Bid"
        cboBid.ValueMember = "ProjectID"
    End Sub

    Sub LoadClients()
        'On Load Binds Clients to cboClients on Main Page
        cnn.ConnectionString = frmMain.strConnect

        Dim strClientSelect As String
        strClientSelect = "Client"
        Dim daClients As New SqlDataAdapter(strClientSelect, cnn)
        Dim dsClients As New DataSet
        Try
            daClients.Fill(dsClients, "ClientList")
        Catch ex As Exception
            MsgBox("Error Loading Clients")
        End Try
        cboClients.DataSource = dsClients.Tables("ClientList")
        cboClients.DisplayMember = "Client"
        cboClients.ValueMember = "ClientID"

    End Sub

    Private Sub frmCopyBid_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadBids()
    End Sub

    Private Sub LoadBidInfo()
        Dim BidNum As String = Nothing
        cnn.ConnectionString = frmMain.strConnect
        Dim strBidInfo As String
        strBidInfo = "SELECT Description, LDID, Location, Address, City, State, Zip, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, ProjectDate FROM Project WHERE ProjectID = "
        strBidInfo = strBidInfo & cboBid.SelectedValue.ToString
        Dim cmdClientID As New SqlCommand(strBidInfo, cnn)
        cmdClientID.CommandType = CommandType.Text
        Dim drBid As SqlDataReader = Nothing
        Dim dtEntryDate As Date
        Try
            cnn.Open()
            drBid = cmdClientID.ExecuteReader
            drBid.Read()
            txtbDescription.Text = drBid.Item("Description").ToString
            txtBLocation.Text = drBid.Item("Location").ToString
            txtBAddress.Text = drBid.Item("Address").ToString
            txtBCity.Text = drBid.Item("City").ToString
            txtBState.Text = drBid.Item("State").ToString
            txtBZip.Text = drBid.Item("Zip").ToString
            txtBNotes.Text = drBid.Item("Notes").ToString
            txtLDEstimator.Text = drBid.Item("LDEstimator").ToString
            txtBluePrintDate.Text = Format(drBid.Item("BluePrintDate"), "d")
            txtRevDate.Text = Format(drBid.Item("RevDate"), "d")
            txtSalesTaxRate.Text = CStr(drBid.Item("SalesTaxRate"))
            dtEntryDate = System.Convert.ToDateTime(drBid.Item("ProjectDate"))
            BidNum = "LD-" & Format(dtEntryDate, "yy")
            BidNum = BidNum & "-" & drBid.Item("LDID").ToString
        Catch ex As Exception
            MsgBox("Error Loading Bid Info")
        Finally
            drBid.Close()
            cnn.Close()
        End Try
        Me.Text = "Lights Direct Bid  -----    " & BidNum
    End Sub

    Sub LoadClientDetails()
        'Check for Integet Length 1 to 4
        If Len(cboClients.SelectedValue.ToString) > 4 Then
            Exit Sub
        End If
        Dim strClientID As String
        strClientID = cboClients.SelectedValue.ToString

        Dim spClientDetails As String = "ClientDetails"
        Dim cmdClientDetails As New SqlCommand(spClientDetails, cnn)
        cmdClientDetails.CommandType = CommandType.StoredProcedure
        cmdClientDetails.Parameters.AddWithValue("@ClientID", cboClients.SelectedValue.ToString)
        Dim drClientDetails As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        Try
            drClientDetails = cmdClientDetails.ExecuteReader
            drClientDetails.Read()
            txtCLName.Text = drClientDetails.Item("Name").ToString
            txtClAddr1.Text = drClientDetails.Item("Address1").ToString
            txtCLAddr2.Text = drClientDetails.Item("Address2").ToString
            txtCLCity.Text = drClientDetails.Item("City").ToString
            txtCLState.Text = drClientDetails.Item("State").ToString
            txtCLZip.Text = drClientDetails.Item("Zip").ToString
            txtCLPhone1.Text = drClientDetails.Item("Phone1").ToString
            txtCLPhone2.Text = drClientDetails.Item("Phone2").ToString
            txtCLFAX.Text = drClientDetails.Item("FAX").ToString
            txtCLemail.Text = drClientDetails.Item("email").ToString
            txtCLContact.Text = drClientDetails.Item("contact").ToString

        Catch ex As Exception
            MsgBox("Error Loading Client Details")
        Finally
            drClientDetails.Close()
            cnn.Close()
        End Try
    End Sub

    Private Sub cboClients_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClients.SelectedIndexChanged
        LoadClientDetails()
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Dim strprompt As String
        strprompt = "You are about to copy LD Bid --- " & vbCrLf & vbCrLf & _
        cboBid.Text & vbCrLf & vbCrLf & _
        "To  --- " & vbCrLf & vbCrLf & cboClients.Text & "!" & vbCrLf & vbCrLf & _
        "Are you sure you wish to proceed??"
        If MsgBox(strprompt, MsgBoxStyle.OkCancel, "Copy") = MsgBoxResult.Ok Then
            Me.Cursor = Cursors.WaitCursor
            CopyBid()
            CopyProjectDetails(cboBid.SelectedValue.ToString, intNewBidID)
            CopyAlternates()
            Me.Cursor = Cursors.Default
            Me.Close()
        End If
    End Sub

    Private Sub CopyBid()
        'Client
        If txtbDescription.Text = "" Then
            MsgBox("You Must Enter A Bid Description!")
            txtbDescription.Focus()
            Exit Sub
        End If

        If txtBLocation.Text = "" Then
            MsgBox("You Must Enter A Bid Location!")
            txtBLocation.Focus()
            Exit Sub
        End If

        If Not IsDate(txtBluePrintDate.Text) Then
            MsgBox("You must enter a valid Blue Print Date")
            Exit Sub
        End If

        Dim AddNewBid As New SqlCommand("AddProject", cnn)
        AddNewBid.CommandType = CommandType.StoredProcedure

        Dim prmClientID As SqlParameter = AddNewBid.Parameters.Add("@ClientID", SqlDbType.Int)
        prmClientID.Value = cboClients.SelectedValue

        Dim LDID As Integer
        Dim LDIDQuery As String = "SELECT MAX(LDID) as LDID FROM Project"
        Dim LDIDQU As New SqlCommand(LDIDQuery, cnn)
        LDIDQU.CommandType = CommandType.Text
        Dim LDIDDR As SqlDataReader = Nothing
        Try
            cnn.Open()
            LDIDDR = LDIDQU.ExecuteReader
            LDIDDR.Read()
            LDID = System.Convert.ToInt32(LDIDDR.Item("LDID")) + 1
        Catch ex As Exception
            MsgBox("Error obtaining LDID: " + ex.ToString)
        Finally
            cnn.Close()
        End Try

        AddNewBid.Parameters.AddWithValue("@LDID", LDID)

        Dim prmEntryDate As SqlParameter = AddNewBid.Parameters.Add("@ProjectDate", SqlDbType.DateTime)
        prmEntryDate.Value = Now()

        Dim prmDescription As SqlParameter = AddNewBid.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbDescription.Text

        Dim prmLocation As SqlParameter = AddNewBid.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtBLocation.Text

        Dim prmAddress As SqlParameter = AddNewBid.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtBAddress.Text

        Dim prmCity As SqlParameter = AddNewBid.Parameters.Add("@City", SqlDbType.NVarChar, 50)
        prmCity.Value = txtBCity.Text

        Dim prmState As SqlParameter = AddNewBid.Parameters.Add("@State", SqlDbType.Char, 2)
        prmState.Value = txtBState.Text

        Dim prmZip As SqlParameter = AddNewBid.Parameters.Add("@Zip", SqlDbType.Char, 10)
        prmZip.Value = txtBZip.Text

        Dim prmNotes As SqlParameter = AddNewBid.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtBNotes.Text

        Dim prmLDEstimator As SqlParameter = AddNewBid.Parameters.Add("@LDEstimator", SqlDbType.NVarChar, 50)
        prmLDEstimator.Value = txtLDEstimator.Text

        Dim prmBluePrintDate As SqlParameter = AddNewBid.Parameters.Add("@BluePrintDate", SqlDbType.DateTime)
        prmBluePrintDate.Value = txtBluePrintDate.Text

        Dim prmRevDate As SqlParameter = AddNewBid.Parameters.Add("@RevDate", SqlDbType.DateTime)
        If Not IsDate(txtRevDate.Text) Then
            prmRevDate.Value = Now()
        Else
            prmRevDate.Value = txtRevDate.Text
        End If

        Dim prmSalesTaxRate As SqlParameter = AddNewBid.Parameters.Add("@SalesTaxRate", SqlDbType.Float)
        If IsNumeric(txtSalesTaxRate.Text) Then
            prmSalesTaxRate.Value = CDbl(txtSalesTaxRate.Text)
        Else
            prmSalesTaxRate.Value = 0
        End If

        Dim prmBidContact As SqlParameter = AddNewBid.Parameters.Add("@ProjectContact", SqlDbType.NVarChar, 50)
        prmBidContact.Value = ""
        AddNewBid.Parameters.AddWithValue("@ProjectType", "Bid")

        Dim prmNewBidID As SqlParameter = AddNewBid.Parameters.Add("@NewProjectID", SqlDbType.Int)
        prmNewBidID.Direction = ParameterDirection.Output

        'Dim intBidID As Integer
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            AddNewBid.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        'Gets New Bid ID As Output Parameter
        intNewBidID = System.Convert.ToInt32(prmNewBidID.Value)

    End Sub

    Private Sub DeleteBid()
        Dim strPrompt As String
        strPrompt = "You are about to DELETE ALL DATA Associated with ---- " & vbCrLf & vbCrLf & _
        cboBid.Text & vbCrLf & vbCrLf & _
        "Proceed??"
        If MsgBox(strPrompt, MsgBoxStyle.OkCancel, "Delete Bid??") = MsgBoxResult.Ok Then
            Me.Cursor = Cursors.WaitCursor
            Dim cnn5 As New SqlConnection
            cnn5.ConnectionString = frmMain.strConnect

            Dim strDeleteBid As String
            strDeleteBid = "DELETE FROM Project where ProjectId = " & cboBid.SelectedValue.ToString

            Dim DeleteBid As New SqlCommand(strDeleteBid, cnn5)
            DeleteBid.CommandType = CommandType.Text

            Try
                If cnn5.State = ConnectionState.Closed Then
                    cnn5.Open()
                End If
                DeleteBid.ExecuteNonQuery()
                cnn5.Close()
            Catch ex As Exception
                MsgBox("There was an error deleting the Bid Data!!")
            Finally
                If cnn5.State = ConnectionState.Open Then
                    cnn5.Close()
                End If
            End Try

            Dim strDeleteUnits As String
            strDeleteUnits = "DELETE FROM Unit WHERE Projectid = " & cboBid.SelectedValue.ToString

            Dim DeleteUnits As New SqlCommand(strDeleteUnits, cnn5)
            DeleteUnits.CommandType = CommandType.Text


            Try
                If cnn5.State = ConnectionState.Closed Then
                    cnn5.Open()
                End If
                DeleteUnits.ExecuteNonQuery()
                cnn5.Close()
            Catch ex As Exception
                MsgBox("There was an error deleting the Units!!")
            Finally
                If cnn5.State = ConnectionState.Open Then
                    cnn5.Close()
                End If
            End Try

            Dim strDeleteParts As String
            strDeleteParts = "DELETE FROM LineItem WHERE Projectid = " & cboBid.SelectedValue.ToString

            Dim DeleteParts As New SqlCommand(strDeleteParts, cnn5)
            DeleteParts.CommandType = CommandType.Text

            Try
                If cnn5.State = ConnectionState.Closed Then
                    cnn5.Open()
                End If
                DeleteParts.ExecuteNonQuery()
                cnn5.Close()
            Catch ex As Exception
                MsgBox("There was an error deleting the Parts!!")
            Finally
                If cnn5.State = ConnectionState.Open Then
                    cnn5.Close()
                End If
            End Try

            Dim strDeleteBldg As String
            strDeleteBldg = "DELETE FROM Building WHERE Projectid = " & cboBid.SelectedValue.ToString

            Dim DeleteBldg As New SqlCommand(strDeleteBldg, cnn5)
            DeleteBldg.CommandType = CommandType.Text

            Try
                If cnn5.State = ConnectionState.Closed Then
                    cnn5.Open()
                End If
                DeleteBldg.ExecuteNonQuery()
                cnn5.Close()
            Catch ex As Exception
                MsgBox("There was an error deleting the Buildings!!")
            Finally
                If cnn5.State = ConnectionState.Open Then
                    cnn5.Close()
                End If
            End Try

            Dim strDeleteAlt As String
            strDeleteAlt = "DELETE FROM Alternates WHERE Projectid = " & cboBid.SelectedValue.ToString

            Dim DeleteAlt As New SqlCommand(strDeleteAlt, cnn5)
            DeleteAlt.CommandType = CommandType.Text

            Try
                If cnn5.State = ConnectionState.Closed Then
                    cnn5.Open()
                End If
                DeleteAlt.ExecuteNonQuery()
                cnn5.Close()
            Catch ex As Exception
                MsgBox("There was an error deleting the Alternates!!")
            Finally
                If cnn5.State = ConnectionState.Open Then
                    cnn5.Close()
                End If
            End Try
            Me.Cursor = Cursors.Default

            ExecuteQueryText("delete from comment where projectid = " + cboBid.SelectedValue.ToString, cnn5)
        End If


    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        DeleteBid()
        Me.Close()
    End Sub


    Private Sub CopyAlternates()
        Dim cnn3 As New SqlConnection
        cnn3.ConnectionString = frmMain.strConnect

        Dim strAlt As String
        strAlt = "SELECT Altid as OldAltID, OrgProductID, OrgDescription, " & _
        "OrgCost, OrgMargin, OrgSell, AltProductID, AltDescription, " & _
        "AltCost, AltMargin, AltSell, AltRecordSource FROM Alternates " & _
        " where ProjectId =" & cboBid.SelectedValue.ToString


        Dim cmdOldAlt As New SqlCommand(strAlt, cnn3)
        cmdOldAlt.CommandType = CommandType.Text

        Dim drOldAlt As SqlDataReader
        cnn3.Open()

        drOldAlt = cmdOldAlt.ExecuteReader

        While drOldAlt.Read()
            intOldAltID = System.Convert.ToInt32(drOldAlt.Item("OldAltID"))
            Dim cnn2 As New SqlConnection
            cnn2.ConnectionString = frmMain.strConnect

            Dim cmdAddAlternate As New SqlCommand("Alternate", cnn2)
            cmdAddAlternate.CommandType = CommandType.StoredProcedure

            Dim prmBidID As SqlParameter = cmdAddAlternate.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmBidID.Value = intNewBidID
            Dim prmOrgProductID As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgProductID", SqlDbType.NVarChar, 255)
            prmOrgProductID.Value = drOldAlt.Item("OrgProductID")

            Dim prmOrgDescription As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgDescription", SqlDbType.NVarChar, 25)
            prmOrgDescription.Value = drOldAlt.Item("OrgDescription")

            Dim prmOrgCost As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgCost", SqlDbType.Money)
            prmOrgCost.Value = drOldAlt.Item("OrgCost")

            Dim prmOrgMargin As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgMargin", SqlDbType.Int)
            prmOrgMargin.Value = drOldAlt.Item("OrgMargin")

            Dim prmOrgSell As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgSell", SqlDbType.Money)
            prmOrgSell.Value = drOldAlt.Item("OrgSell")

            Dim prmAltProductID As SqlParameter = cmdAddAlternate.Parameters.Add("@AltProductID", SqlDbType.NVarChar, 255)
            prmAltProductID.Value = drOldAlt.Item("AltProductID")

            Dim prmAltDescription As SqlParameter = cmdAddAlternate.Parameters.Add("@AltDescription", SqlDbType.NVarChar, 25)
            prmAltDescription.Value = drOldAlt.Item("AltDescription")

            Dim prmAltCost As SqlParameter = cmdAddAlternate.Parameters.Add("@AltCost", SqlDbType.Money)
            prmAltCost.Value = drOldAlt.Item("AltCost")

            Dim prmAltMargin As SqlParameter = cmdAddAlternate.Parameters.Add("@AltMargin", SqlDbType.Int)
            prmAltMargin.Value = drOldAlt.Item("AltMargin")

            Dim prmAltSell As SqlParameter = cmdAddAlternate.Parameters.Add("@AltSell", SqlDbType.Money)
            prmAltSell.Value = drOldAlt.Item("AltSell")

            Dim prmAltRecordSource As SqlParameter = cmdAddAlternate.Parameters.Add("@AltRecordSource", SqlDbType.Char, 10)
            prmAltRecordSource.Value = drOldAlt.Item("AltRecordSource")
            If cnn2.State = ConnectionState.Closed Then
                cnn2.Open()
            End If
            cmdAddAlternate.ExecuteNonQuery()
            cnn2.Close()
        End While
        drOldAlt.Close()
        cnn3.Close()
    End Sub

    Private Sub BidChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBid.SelectionChangeCommitted
        If cboBid.SelectedIndex = 0 Then
            Exit Sub
        End If
        LoadBidInfo()
        LoadClients()
    End Sub
End Class
