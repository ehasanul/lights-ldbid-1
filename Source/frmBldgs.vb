Imports System.Data
Imports System.Data.SqlClient
Public Class frmBldgs
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim blnStartBldg As Boolean  'Tests Startup Condition in LoadBldgs
    Dim strFunction As String
    Dim strOldBldgDesc As String
    Dim intOldBldgNum As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboBldg As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtbldgType As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgNum As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgShipDate As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgShipSeq As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgNotes As System.Windows.Forms.TextBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents lblbldgType As System.Windows.Forms.Label
    Friend WithEvents lblBldgNum As System.Windows.Forms.Label
    Friend WithEvents lblBldgLocation As System.Windows.Forms.Label
    Friend WithEvents lblBldgAddress As System.Windows.Forms.Label
    Friend WithEvents lblBldgShipSeq As System.Windows.Forms.Label
    Friend WithEvents lblBldgNotes As System.Windows.Forms.Label
    Friend WithEvents lblBldgDesc As System.Windows.Forms.Label
    Friend WithEvents lblbldgShipDate As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboBldg = New System.Windows.Forms.ComboBox
        Me.txtbldgType = New System.Windows.Forms.TextBox
        Me.txtbldgDesc = New System.Windows.Forms.TextBox
        Me.txtbldgNum = New System.Windows.Forms.TextBox
        Me.txtbldgLocation = New System.Windows.Forms.TextBox
        Me.txtbldgAddress = New System.Windows.Forms.TextBox
        Me.txtbldgShipDate = New System.Windows.Forms.TextBox
        Me.txtbldgShipSeq = New System.Windows.Forms.TextBox
        Me.txtbldgNotes = New System.Windows.Forms.TextBox
        Me.lblbldgType = New System.Windows.Forms.Label
        Me.lblBldgDesc = New System.Windows.Forms.Label
        Me.lblBldgNum = New System.Windows.Forms.Label
        Me.lblBldgLocation = New System.Windows.Forms.Label
        Me.lblBldgAddress = New System.Windows.Forms.Label
        Me.lblbldgShipDate = New System.Windows.Forms.Label
        Me.lblBldgShipSeq = New System.Windows.Forms.Label
        Me.lblBldgNotes = New System.Windows.Forms.Label
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.btnDelete = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboBldg
        '
        Me.cboBldg.Location = New System.Drawing.Point(20, 16)
        Me.cboBldg.Name = "cboBldg"
        Me.cboBldg.Size = New System.Drawing.Size(232, 21)
        Me.cboBldg.TabIndex = 0
        '
        'txtbldgType
        '
        Me.txtbldgType.Location = New System.Drawing.Point(24, 68)
        Me.txtbldgType.Name = "txtbldgType"
        Me.txtbldgType.Size = New System.Drawing.Size(168, 20)
        Me.txtbldgType.TabIndex = 1
        Me.txtbldgType.Text = ""
        '
        'txtbldgDesc
        '
        Me.txtbldgDesc.Location = New System.Drawing.Point(224, 68)
        Me.txtbldgDesc.Name = "txtbldgDesc"
        Me.txtbldgDesc.Size = New System.Drawing.Size(120, 20)
        Me.txtbldgDesc.TabIndex = 2
        Me.txtbldgDesc.Text = ""
        '
        'txtbldgNum
        '
        Me.txtbldgNum.Location = New System.Drawing.Point(384, 68)
        Me.txtbldgNum.Name = "txtbldgNum"
        Me.txtbldgNum.Size = New System.Drawing.Size(40, 20)
        Me.txtbldgNum.TabIndex = 3
        Me.txtbldgNum.Text = ""
        '
        'txtbldgLocation
        '
        Me.txtbldgLocation.Location = New System.Drawing.Point(24, 108)
        Me.txtbldgLocation.Name = "txtbldgLocation"
        Me.txtbldgLocation.Size = New System.Drawing.Size(168, 20)
        Me.txtbldgLocation.TabIndex = 4
        Me.txtbldgLocation.Text = ""
        '
        'txtbldgAddress
        '
        Me.txtbldgAddress.Location = New System.Drawing.Point(224, 108)
        Me.txtbldgAddress.Name = "txtbldgAddress"
        Me.txtbldgAddress.Size = New System.Drawing.Size(124, 20)
        Me.txtbldgAddress.TabIndex = 5
        Me.txtbldgAddress.Text = ""
        '
        'txtbldgShipDate
        '
        Me.txtbldgShipDate.Location = New System.Drawing.Point(360, 108)
        Me.txtbldgShipDate.Name = "txtbldgShipDate"
        Me.txtbldgShipDate.Size = New System.Drawing.Size(72, 20)
        Me.txtbldgShipDate.TabIndex = 6
        Me.txtbldgShipDate.Text = ""
        '
        'txtbldgShipSeq
        '
        Me.txtbldgShipSeq.Location = New System.Drawing.Point(464, 108)
        Me.txtbldgShipSeq.Name = "txtbldgShipSeq"
        Me.txtbldgShipSeq.Size = New System.Drawing.Size(36, 20)
        Me.txtbldgShipSeq.TabIndex = 7
        Me.txtbldgShipSeq.Text = ""
        '
        'txtbldgNotes
        '
        Me.txtbldgNotes.Location = New System.Drawing.Point(28, 148)
        Me.txtbldgNotes.Multiline = True
        Me.txtbldgNotes.Name = "txtbldgNotes"
        Me.txtbldgNotes.Size = New System.Drawing.Size(468, 104)
        Me.txtbldgNotes.TabIndex = 8
        Me.txtbldgNotes.Text = ""
        '
        'lblbldgType
        '
        Me.lblbldgType.Location = New System.Drawing.Point(24, 52)
        Me.lblbldgType.Name = "lblbldgType"
        Me.lblbldgType.Size = New System.Drawing.Size(88, 16)
        Me.lblbldgType.TabIndex = 9
        Me.lblbldgType.Text = "Type"
        '
        'lblBldgDesc
        '
        Me.lblBldgDesc.Location = New System.Drawing.Point(224, 52)
        Me.lblBldgDesc.Name = "lblBldgDesc"
        Me.lblBldgDesc.Size = New System.Drawing.Size(76, 16)
        Me.lblBldgDesc.TabIndex = 10
        Me.lblBldgDesc.Text = "Description"
        '
        'lblBldgNum
        '
        Me.lblBldgNum.Location = New System.Drawing.Point(384, 52)
        Me.lblBldgNum.Name = "lblBldgNum"
        Me.lblBldgNum.Size = New System.Drawing.Size(48, 16)
        Me.lblBldgNum.TabIndex = 11
        Me.lblBldgNum.Text = "#"
        '
        'lblBldgLocation
        '
        Me.lblBldgLocation.Location = New System.Drawing.Point(24, 91)
        Me.lblBldgLocation.Name = "lblBldgLocation"
        Me.lblBldgLocation.Size = New System.Drawing.Size(92, 12)
        Me.lblBldgLocation.TabIndex = 12
        Me.lblBldgLocation.Text = "Location"
        '
        'lblBldgAddress
        '
        Me.lblBldgAddress.Location = New System.Drawing.Point(224, 91)
        Me.lblBldgAddress.Name = "lblBldgAddress"
        Me.lblBldgAddress.Size = New System.Drawing.Size(80, 16)
        Me.lblBldgAddress.TabIndex = 13
        Me.lblBldgAddress.Text = "Address"
        '
        'lblbldgShipDate
        '
        Me.lblbldgShipDate.Location = New System.Drawing.Point(364, 91)
        Me.lblbldgShipDate.Name = "lblbldgShipDate"
        Me.lblbldgShipDate.Size = New System.Drawing.Size(56, 12)
        Me.lblbldgShipDate.TabIndex = 14
        Me.lblbldgShipDate.Text = "Ship Date"
        '
        'lblBldgShipSeq
        '
        Me.lblBldgShipSeq.Location = New System.Drawing.Point(456, 87)
        Me.lblBldgShipSeq.Name = "lblBldgShipSeq"
        Me.lblBldgShipSeq.Size = New System.Drawing.Size(56, 16)
        Me.lblBldgShipSeq.TabIndex = 15
        Me.lblBldgShipSeq.Text = "Ship Seq"
        '
        'lblBldgNotes
        '
        Me.lblBldgNotes.Location = New System.Drawing.Point(28, 132)
        Me.lblBldgNotes.Name = "lblBldgNotes"
        Me.lblBldgNotes.Size = New System.Drawing.Size(92, 16)
        Me.lblBldgNotes.TabIndex = 16
        Me.lblBldgNotes.Text = "Notes"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(400, 20)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(52, 20)
        Me.btnSave.TabIndex = 18
        Me.btnSave.Text = "Save"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(452, 20)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(52, 20)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Text = "Cancel"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "New"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Copy"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Edit"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(348, 20)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(52, 20)
        Me.btnDelete.TabIndex = 20
        Me.btnDelete.Text = "Delete"
        '
        'frmBldgs
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(540, 278)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblBldgNotes)
        Me.Controls.Add(Me.lblBldgShipSeq)
        Me.Controls.Add(Me.lblbldgShipDate)
        Me.Controls.Add(Me.lblBldgAddress)
        Me.Controls.Add(Me.lblBldgLocation)
        Me.Controls.Add(Me.lblBldgNum)
        Me.Controls.Add(Me.lblBldgDesc)
        Me.Controls.Add(Me.lblbldgType)
        Me.Controls.Add(Me.txtbldgNotes)
        Me.Controls.Add(Me.txtbldgShipSeq)
        Me.Controls.Add(Me.txtbldgShipDate)
        Me.Controls.Add(Me.txtbldgAddress)
        Me.Controls.Add(Me.txtbldgLocation)
        Me.Controls.Add(Me.txtbldgNum)
        Me.Controls.Add(Me.txtbldgDesc)
        Me.Controls.Add(Me.txtbldgType)
        Me.Controls.Add(Me.cboBldg)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmBldgs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add - Edit Buildings"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub LoadBldgs()
        'bldg
        
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadBldg As New SqlCommand("Buildings", cnn)
        cmdLoadBldg.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBldg As New SqlDataAdapter(cmdLoadBldg)
        Dim dsBldg As New DataSet
        daBldg.Fill(dsBldg, "Bldg")

        Dim dtBldg As DataTable = dsBldg.Tables("Bldg")
        Dim dr As DataRow = dtBldg.NewRow
        dr("Bldg") = " - Please Select A Building - "
        dr("BldgID") = 0
        dtBldg.Rows.InsertAt(dr, 0)
        cboBldg.DataSource = dsBldg.Tables("Bldg")
        cboBldg.DisplayMember = "BLDG"
        cboBldg.ValueMember = "BldgID"
        blnStartBldg = False
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If

    End Sub

    Private Sub SaveNew()
        If txtbldgType.Text = "" Then
            MsgBox("You must enter a Building Type!")
            Exit Sub
        End If
        If txtbldgDesc.Text = "" Then
            MsgBox("You must enter a Building Description!")
            Exit Sub
        End If
        If txtbldgNum.Text = "" Then
            MsgBox("You must enter a Building Number")
            Exit Sub
        End If
        frmBid.blnBldgCopy = True
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAddBldg As New SqlCommand("AddBldg", cnn)
        cmdAddBldg.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAddBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmBldgNumber As SqlParameter = cmdAddBldg.Parameters.Add("@BldgNumber", SqlDbType.Int)
        prmBldgNumber.Value = CInt(txtbldgNum.Text)

        Dim prmType As SqlParameter = cmdAddBldg.Parameters.Add("@Type", SqlDbType.NVarChar, 50)
        prmType.Value = txtbldgType.Text

        Dim prmDescription As SqlParameter = cmdAddBldg.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbldgDesc.Text

        Dim prmLocation As SqlParameter = cmdAddBldg.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtbldgLocation.Text

        Dim prmAddress As SqlParameter = cmdAddBldg.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtbldgAddress.Text

        Dim prmNotes As SqlParameter = cmdAddBldg.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtbldgNotes.Text

        Dim prmShipDate As SqlParameter = cmdAddBldg.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtbldgShipDate.Text) Then
            prmShipDate.Value = txtbldgShipDate.Text
        End If

        Dim prmShipSequence As SqlParameter = cmdAddBldg.Parameters.Add("@shipsequence", SqlDbType.Int)
        If IsNumeric(txtbldgShipSeq.Text) Then
            prmShipSequence.Value = CInt(txtbldgShipSeq.Text)
        End If

        Dim prmNewBldgID As SqlParameter = cmdAddBldg.Parameters.Add("@NewBldgID", SqlDbType.Int)
        prmNewBldgID.Direction = ParameterDirection.Output

        Dim intBldgID As Integer


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddBldg.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        intBldgID = System.Convert.ToInt32(prmNewBldgID.Value)
        EnableMenu()
        HideAll()
        ClearAll()

        LoadBldgs()
    End Sub

    Private Sub CopyBldg()
        If txtbldgType.Text = "" Then
            MsgBox("You must enter a Building Type!")
            Exit Sub
        End If
        If txtbldgDesc.Text = "" Then
            MsgBox("You must enter a Building Description!")
            Exit Sub
        End If
        If txtbldgNum.Text = "" Then
            MsgBox("You must enter a Building Number")
            Exit Sub
        End If

        frmBid.blnBldgCopy = True

        Dim intNewBldgNum As Integer
        Dim intOldBldgId As Integer
        Dim intNewBldgID As Integer
        intOldBldgId = System.Convert.ToInt32(cboBldg.SelectedValue)

        intNewBldgNum = CInt(txtbldgNum.Text)
        If intNewBldgNum = intOldBldgNum Then
            MsgBox("You must enter a different Building Number!")
            Exit Sub
        End If
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAddBldg As New SqlCommand("AddBldg", cnn)
        cmdAddBldg.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAddBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmBldgNumber As SqlParameter = cmdAddBldg.Parameters.Add("@BldgNumber", SqlDbType.Int)
        prmBldgNumber.Value = CInt(txtbldgNum.Text)

        Dim prmType As SqlParameter = cmdAddBldg.Parameters.Add("@Type", SqlDbType.NVarChar, 50)
        prmType.Value = txtbldgType.Text

        Dim prmDescription As SqlParameter = cmdAddBldg.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbldgDesc.Text

        Dim prmLocation As SqlParameter = cmdAddBldg.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtbldgLocation.Text

        Dim prmAddress As SqlParameter = cmdAddBldg.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtbldgAddress.Text

        Dim prmNotes As SqlParameter = cmdAddBldg.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtbldgNotes.Text

        Dim prmShipDate As SqlParameter = cmdAddBldg.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtbldgShipDate.Text) Then
            prmShipDate.Value = txtbldgShipDate.Text
        End If

        Dim prmShipSequence As SqlParameter = cmdAddBldg.Parameters.Add("@shipsequence", SqlDbType.Int)
        If IsNumeric(txtbldgShipSeq.Text) Then
            prmShipSequence.Value = CInt(txtbldgShipSeq.Text)
        End If

        Dim prmNewBldgID As SqlParameter = cmdAddBldg.Parameters.Add("@NewBldgID", SqlDbType.Int)
        prmNewBldgID.Direction = ParameterDirection.Output

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddBldg.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
            Exit Sub
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        intNewBldgID = CInt(prmNewBldgID.Value)

        CopyUnitsForBldg(intOldBldgId, intNewBldgID, frmMain.intBidID)
        EnableMenu()
        HideAll()
        ClearAll()
        blnStartBldg = True
        LoadBldgs()
    End Sub

    Private Sub EditBldg()
        If txtbldgType.Text = "" Then
            MsgBox("You must enter a Building Type!")
            Exit Sub
        End If
        If txtbldgDesc.Text = "" Then
            MsgBox("You must enter a Building Description!")
            Exit Sub
        End If
        If txtbldgNum.Text = "" Then
            MsgBox("You must enter a Building Number")
            Exit Sub
        End If
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAddBldg As New SqlCommand("EditBldg", cnn)
        cmdAddBldg.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdAddBldg.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = cboBldg.SelectedValue

        Dim prmBidID As SqlParameter = cmdAddBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmBldgNumber As SqlParameter = cmdAddBldg.Parameters.Add("@BldgNumber", SqlDbType.Int)
        If IsNumeric(txtbldgNum.Text) Then
            prmBldgNumber.Value = CInt(txtbldgNum.Text)
        End If

        Dim prmType As SqlParameter = cmdAddBldg.Parameters.Add("@Type", SqlDbType.NVarChar, 50)
        prmType.Value = txtbldgType.Text

        Dim prmDescription As SqlParameter = cmdAddBldg.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbldgDesc.Text

        Dim prmLocation As SqlParameter = cmdAddBldg.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtbldgLocation.Text

        Dim prmAddress As SqlParameter = cmdAddBldg.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtbldgAddress.Text

        Dim prmNotes As SqlParameter = cmdAddBldg.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtbldgNotes.Text

        Dim prmShipDate As SqlParameter = cmdAddBldg.Parameters.Add("@shipdate", SqlDbType.DateTime)
        If IsDate(txtbldgShipDate.Text) Then
            prmShipDate.Value = txtbldgShipDate.Text
        End If

        Dim prmShipSequence As SqlParameter = cmdAddBldg.Parameters.Add("@shipsequence", SqlDbType.Int)
        If IsNumeric(txtbldgShipSeq.Text) Then
            prmShipSequence.Value = CInt(txtbldgShipSeq.Text)

        End If

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddBldg.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        EnableMenu()
        HideAll()
        ClearAll()
        blnStartBldg = True

        LoadBldgs()
    End Sub

    Private Sub DeleteBldg()
        frmBid.blnBldgCopy = True
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdDeleteBldg As New SqlCommand("DeleteBldg", cnn)
        cmdDeleteBldg.CommandType = CommandType.StoredProcedure

        Dim prmBldgID As SqlParameter = cmdDeleteBldg.Parameters.Add("@BldgID", SqlDbType.Int)
        prmBldgID.Value = cboBldg.SelectedValue


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdDeleteBldg.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error deleting this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        EnableMenu()
        HideAll()
        ClearAll()

        LoadBldgs()
    End Sub

    Private Sub frmBldgs_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        blnStartBldg = True
        HideAll()
        'LoadBldgs()
        frmBid.blnBldgCopy = False
    End Sub

    Private Sub cboBldg_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBldg.SelectedValueChanged
        'Check to see if in Startup Mode
        If blnStartBldg = True Then
            Exit Sub
        End If
        ShowAll()
        LoadBldgDetails()
    End Sub

    Private Sub LoadBldgDetails()
        If cboBldg.SelectedIndex = 0 Then
            ClearAll()
            Exit Sub
        End If
        'Bldg
        cnn.ConnectionString = frmMain.strConnect

        Dim strBldgInfo As String
        strBldgInfo = "SELECT BldgNumber, Type, Description, Location, Address, Notes, shipdate, shipsequence FROM Building WHERE BldgID = "
        strBldgInfo = strBldgInfo & cboBldg.SelectedValue.ToString
        Dim cmdBldgInfo As New SqlCommand(strBldgInfo, cnn)
        cmdBldgInfo.CommandType = CommandType.Text
        Dim drBldgInfo As SqlDataReader
        cnn.Open()
        drBldgInfo = cmdBldgInfo.ExecuteReader
        drBldgInfo.Read()
        txtbldgType.Text = drBldgInfo.Item("Type").ToString
        txtbldgDesc.Text = drBldgInfo.Item("Description").ToString
        strOldBldgDesc = txtbldgDesc.Text
        txtbldgNum.Text = drBldgInfo.Item("BldgNumber").ToString
        intOldBldgNum = CInt(drBldgInfo.Item("BldgNumber").ToString)
        txtbldgLocation.Text = drBldgInfo.Item("Location").ToString
        txtbldgAddress.Text = drBldgInfo.Item("Address").ToString
        txtbldgNotes.Text = drBldgInfo.Item("Notes").ToString
        If IsDBNull(drBldgInfo.Item("shipdate")) Then
            txtbldgShipDate.Text = ""
        Else
            txtbldgShipDate.Text = FormatDateTime(System.Convert.ToDateTime(drBldgInfo.Item("shipdate")), DateFormat.ShortDate)
        End If
        txtbldgShipSeq.Text = drBldgInfo.Item("shipsequence").ToString
        drBldgInfo.Close()
        cnn.Close()
    End Sub

    Private Sub HideAll()
        cboBldg.Visible = False
        txtbldgType.Visible = False
        txtbldgDesc.Visible = False
        txtbldgNum.Visible = False
        txtbldgLocation.Visible = False
        txtbldgAddress.Visible = False
        txtbldgShipDate.Visible = False
        txtbldgShipSeq.Visible = False
        txtbldgNotes.Visible = False
        lblbldgType.Visible = False
        lblBldgDesc.Visible = False
        lblBldgNum.Visible = False
        lblBldgLocation.Visible = False
        lblBldgAddress.Visible = False
        lblbldgShipDate.Visible = False
        lblBldgShipSeq.Visible = False
        lblBldgNotes.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False
        btnDelete.Visible = False
    End Sub

    Private Sub ClearAll()
        txtbldgType.Text = ""
        txtbldgDesc.Text = ""
        txtbldgNum.Text = ""
        txtbldgLocation.Text = ""
        txtbldgAddress.Text = ""
        txtbldgShipDate.Text = ""
        txtbldgShipSeq.Text = ""
        txtbldgNotes.Text = ""
    End Sub

    Private Sub ShowAll()
        txtbldgType.Visible = True
        txtbldgDesc.Visible = True
        txtbldgNum.Visible = True
        txtbldgLocation.Visible = True
        txtbldgAddress.Visible = True
        txtbldgShipDate.Visible = True
        txtbldgShipSeq.Visible = True
        txtbldgNotes.Visible = True
        lblbldgType.Visible = True
        lblBldgDesc.Visible = True
        lblBldgNum.Visible = True
        lblBldgLocation.Visible = True
        lblBldgAddress.Visible = True
        lblbldgShipDate.Visible = True
        lblBldgShipSeq.Visible = True
        lblBldgNotes.Visible = True
        btnSave.Visible = True
        btnCancel.Visible = True
        If strFunction = "Edit" Then
            btnDelete.Visible = True
        End If

    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        HideAll()
        EnableMenu()
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        strFunction = "New"
        ClearAll()
        ShowAll()
        DisableMenu()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        strFunction = "Copy"
        ClearAll()
        LoadBldgs()
        HideAll()
        cboBldg.Visible = True
        DisableMenu()
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        strFunction = "Edit"
        ClearAll()
        LoadBldgs()
        HideAll()
        cboBldg.Visible = True
        DisableMenu()
    End Sub

    Private Sub DisableMenu()
        MenuItem1.Visible = False
        MenuItem2.Visible = False
        MenuItem3.Visible = False
    End Sub

    Private Sub EnableMenu()
        MenuItem1.Visible = True
        MenuItem2.Visible = True
        MenuItem3.Visible = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim strSample As String
        strSample = txtbldgType.Text.Substring(0, 1)
        If (System.Convert.ToDouble(strSample.IndexOfAny("BCSA".ToCharArray)) = -1) Then
            MsgBox("The Building Type Must Begin With either B,C,S or A")
            Exit Sub
        End If
        Select Case strFunction
            Case "New"
                SaveNew()
            Case "Edit"
                EditBldg()
            Case "Copy"
                CopyBldg()
        End Select
        'EnableMenu()
        'HideAll()
        'ClearAll()
    End Sub


    Private Sub txtbldgNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbldgNum.KeyPress
        If Not (Char.IsDigit(e.KeyChar)) Then
            e.Handled = True
        End If

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If cboBldg.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strGo As String
        strGo = MsgBox("Are You Sure You Want to Delete This Record?", MsgBoxStyle.YesNo, "Delete Record").ToString
        If strGo = "6" Then
            DeleteBldg()
            HideAll()
            EnableMenu()
        End If
    End Sub


    Private Sub txtbldgNum_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtbldgNum.KeyDown
        If e.KeyCode = Keys.Back Then
            txtbldgNum.Text = Microsoft.VisualBasic.Left(txtbldgNum.Text.ToString, Len(txtbldgNum.Text) - 1)
            e.Handled = True
        End If
    End Sub

    Private Sub txtbldgNum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbldgNum.TextChanged
        txtbldgNum.SelectionStart = Len(txtbldgNum.Text) + 1
    End Sub
End Class
