Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms

Public Class frmj_BldgBOM
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(508, 302)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'frmj_BldgBOM
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(508, 302)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.MinimizeBox = False
        Me.Name = "frmj_BldgBOM"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Building BOM"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadReport()
        cnn.ConnectionString = frmMain.strConnect

        Dim strCommand As String
        strCommand = "rptBOMIntro"
        Dim cmdCommand As New SqlCommand
        cmdCommand.Connection = cnn
        cmdCommand.CommandText = strCommand
        cmdCommand.CommandType = CommandType.StoredProcedure
        Dim prmJobID As SqlParameter = cmdCommand.Parameters.Add("@BldgID", SqlDbType.Int)
        prmJobID.Value = frmJob.intBOMBldgID
        Dim daIntro As New SqlDataAdapter(cmdCommand)
        Dim dsIntro As New DataSet
        daIntro.Fill(dsIntro, "rptBOMIntro")

        Dim strSummary As String
        strSummary = "BldgBOMSP"
        Dim cmdSummary As New SqlCommand
        cmdSummary.Connection = cnn
        cmdSummary.CommandText = strSummary
        cmdSummary.CommandType = CommandType.StoredProcedure
        Dim prmSumBldgID As SqlParameter = cmdSummary.Parameters.Add("@BldgID", SqlDbType.Int)
        prmSumBldgID.Value = frmJob.intBOMBldgID
        Dim daSummary As New SqlDataAdapter(cmdSummary)
        Dim dsSummary As New DataSet
        Try
            daSummary.Fill(dsSummary, "BldgBOMSP")
        Catch ex As Exception
            MsgBox("Failed to Fill BldgBOMSP")
        End Try

        Dim strUnitBOM As String
        strUnitBOM = "UnitsBOM"
        Dim cmdUnitBOM As New SqlCommand
        cmdUnitBOM.Connection = cnn
        cmdUnitBOM.CommandText = strUnitBOM
        cmdUnitBOM.CommandType = CommandType.StoredProcedure
        Dim prmUnitBldgID As SqlParameter = cmdUnitBOM.Parameters.Add("@BldgID", SqlDbType.Int)
        prmUnitBldgID.Value = frmJob.intBOMBldgID
        Dim daUnitBOM As New SqlDataAdapter(cmdUnitBOM)
        Dim dsUnitBOM As New DataSet
        Try
            daUnitBOM.Fill(dsUnitBOM, "UnitsBOM")
        Catch ex As Exception
            MsgBox("Failed to Fill UnitsBOM")
        End Try


        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
     
        Dim MySubReport As CrystalDecisions.CrystalReports.Engine.ReportDocument = Nothing
        Dim MySubReport1 As CrystalDecisions.CrystalReports.Engine.ReportDocument = Nothing
     
        Try
            myReport = New rpt_jBldgBOMMain
            myReport.SetDataSource(dsIntro.Tables("rptBOMIntro"))
            myReport.OpenSubreport("rpt_jBldgBOM.rpt").SetDataSource(dsSummary)
            MySubReport = myReport.OpenSubreport("rpt_jBldgBOM.rpt")
            myReport.OpenSubreport("j_rUnitsBOM.rpt").SetDataSource(dsUnitBOM)
            MySubReport1 = myReport.OpenSubreport("j_rUnitsBOM.rpt")
            'myReport.OpenSubreport("rptUnitCount.rpt").SetDataSource(dsUnitCount)
            'myReport.OpenSubreport("rptTypeSummary.rpt").SetDataSource(dsTypeSummary)
            'myReport.OpenSubreport("rptTypeSummaryNotype.rpt").SetDataSource(dsTypeSummaryNoType)
            'myReport.OpenSubreport("rptUnitDetailPerBldg1.rpt").SetDataSource(dsUnitDetailPerBldg)
            'myReport.OpenSubreport("rptUnitDetails.rpt").SetDataSource(dsUnitDetail)
            'myReport.OpenSubreport("rptCommonDetails.rpt").SetDataSource(dsCommonDetail)
            'myReport.OpenSubreport("rptClubHouseSummary.rpt").SetDataSource(dsClubHouseSummary)
            'myReport.OpenSubreport("rptSiteSummary.rpt").SetDataSource(dsSiteSummary)
            'myReport.OpenSubreport("rptUnitSummary.rpt").SetDataSource(dsUnitSummary)
            'myReport.OpenSubreport("rptCommonSummary.rpt").SetDataSource(dsCommonSummary)
            'myReport.OpenSubreport("rptBidSummary.rpt").SetDataSource(dsSummary)
            'myReport.OpenSubreport("rptBidAlternates.rpt").SetDataSource(dsAlternates)
        Catch ex As Exception
            MsgBox("Failed to Open Subreports")
        End Try

        If frmJob.blnShowPrice = False Then
            'Dim MyReportObject0 As CrystalDecisions.CrystalReports.Engine.ReportObject
            'Dim MyFieldObject0 As CrystalDecisions.CrystalReports.Engine.FieldObject
            'MyReportObject0 = mySubReport1.ReportDefinition.ReportObjects.Item("Field9")
            'MyFieldObject0 = CType(MyReportObject0, CrystalDecisions.CrystalReports.Engine.FieldObject)
            'MyFieldObject0.ObjectFormat.EnableSuppress = True

            Dim MyReportObject As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject As CrystalDecisions.CrystalReports.Engine.FieldObject
            MyReportObject = MySubReport1.ReportDefinition.ReportObjects.Item("Field7")
            MyFieldObject = CType(MyReportObject, CrystalDecisions.CrystalReports.Engine.FieldObject)
            MyFieldObject.ObjectFormat.EnableSuppress = True

            Dim MyReportObject1 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject1 As CrystalDecisions.CrystalReports.Engine.FieldObject
            MyReportObject1 = MySubReport1.ReportDefinition.ReportObjects.Item("Field8")
            MyFieldObject1 = CType(MyReportObject1, CrystalDecisions.CrystalReports.Engine.FieldObject)
            MyFieldObject1.ObjectFormat.EnableSuppress = True

            Dim MyReportObject2 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject2 As CrystalDecisions.CrystalReports.Engine.TextObject
            MyReportObject2 = MySubReport1.ReportDefinition.ReportObjects.Item("Text3")
            MyFieldObject2 = CType(MyReportObject2, CrystalDecisions.CrystalReports.Engine.TextObject)
            MyFieldObject2.ObjectFormat.EnableSuppress = True

            Dim MyReportObject3 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject3 As CrystalDecisions.CrystalReports.Engine.TextObject
            MyReportObject3 = MySubReport1.ReportDefinition.ReportObjects.Item("Text2")
            MyFieldObject3 = CType(MyReportObject3, CrystalDecisions.CrystalReports.Engine.TextObject)
            MyFieldObject3.ObjectFormat.EnableSuppress = True

            Dim MyReportObject4 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject4 As CrystalDecisions.CrystalReports.Engine.FieldObject
            MyReportObject4 = MySubReport.ReportDefinition.ReportObjects.Item("Field4")
            MyFieldObject4 = CType(MyReportObject4, CrystalDecisions.CrystalReports.Engine.FieldObject)
            MyFieldObject4.ObjectFormat.EnableSuppress = True

            Dim MyReportObject5 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject5 As CrystalDecisions.CrystalReports.Engine.FieldObject
            MyReportObject5 = MySubReport.ReportDefinition.ReportObjects.Item("Field5")
            MyFieldObject5 = CType(MyReportObject5, CrystalDecisions.CrystalReports.Engine.FieldObject)
            MyFieldObject5.ObjectFormat.EnableSuppress = True

            Dim MyReportObject6 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject6 As CrystalDecisions.CrystalReports.Engine.TextObject
            MyReportObject6 = MySubReport.ReportDefinition.ReportObjects.Item("Text4")
            MyFieldObject6 = CType(MyReportObject6, CrystalDecisions.CrystalReports.Engine.TextObject)
            MyFieldObject6.ObjectFormat.EnableSuppress = True

            Dim MyReportObject7 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject7 As CrystalDecisions.CrystalReports.Engine.TextObject
            MyReportObject7 = MySubReport.ReportDefinition.ReportObjects.Item("Text5")
            MyFieldObject7 = CType(MyReportObject7, CrystalDecisions.CrystalReports.Engine.TextObject)
            MyFieldObject7.ObjectFormat.EnableSuppress = True

            Dim MyReportObject8 As CrystalDecisions.CrystalReports.Engine.ReportObject
            Dim MyFieldObject8 As CrystalDecisions.CrystalReports.Engine.FieldObject
            MyReportObject8 = MySubReport.ReportDefinition.ReportObjects.Item("Field7")
            MyFieldObject8 = CType(MyReportObject8, CrystalDecisions.CrystalReports.Engine.FieldObject)
            MyFieldObject8.ObjectFormat.EnableSuppress = True
        End If





        CrystalReportViewer1.ReportSource = myReport
    End Sub

    Private Sub frmj_BldgBOM_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadReport()
    End Sub
End Class
