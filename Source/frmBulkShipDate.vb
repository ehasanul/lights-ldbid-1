Imports System.Data
Imports System.Data.SqlClient
Public Class frmBulkShipDate
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnEnter As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnEnter = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtDate
        '
        Me.txtDate.Location = New System.Drawing.Point(244, 44)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(124, 20)
        Me.txtDate.TabIndex = 0
        Me.txtDate.Text = ""
        Me.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(372, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "This module will set the Ship Date of all Buildings to the value you enter!!"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(28, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Enter a Date in the format of mm/dd/yy"
        '
        'btnEnter
        '
        Me.btnEnter.Location = New System.Drawing.Point(244, 76)
        Me.btnEnter.Name = "btnEnter"
        Me.btnEnter.Size = New System.Drawing.Size(124, 20)
        Me.btnEnter.TabIndex = 3
        Me.btnEnter.Text = "Enter Ship Dates"
        '
        'frmBulkShipDate
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(392, 126)
        Me.Controls.Add(Me.btnEnter)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDate)
        Me.Name = "frmBulkShipDate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bulk Ship Date Entry"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmBulkShipDate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cnn.ConnectionString = frmMain.strConnect
    End Sub

    Private Sub btnEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        If IsDate(txtDate.Text) = True Then
            If MsgBox("You are about to change all Ship Dates in the Bid! Continue?", MsgBoxStyle.YesNo, "Continue?") = MsgBoxResult.Yes Then
                Dim strSql As String
                strSql = "Update Building Set Shipdate = '" & txtDate.Text
                strSql = strSql & "' WHERE ProjectId = " & frmMain.intBidID.ToString
                Dim UpdateShipDates As New SqlCommand(strSql, cnn)
                UpdateShipDates.CommandType = CommandType.Text
                Try
                    If cnn.State = ConnectionState.Closed Then
                        cnn.Open()
                    End If
                    UpdateShipDates.ExecuteNonQuery()
                    cnn.Close()
                Catch ex As Exception
                    MsgBox("There was an error Updating The Ship Dates: " + ex.ToString)
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try
                Me.Close()
            Else
                txtDate.Text = ""
                txtDate.Focus()
            End If
        Else
            MsgBox("You must enter a proper date!! (mm/dd/yy)", MsgBoxStyle.Critical, "Enter a good date!")
            txtDate.Text = ""
            txtDate.Focus()
        End If
    End Sub
End Class
