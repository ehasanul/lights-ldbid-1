Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmRptMain
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim strUnits As String
    Dim strCommon As String
    Dim strSite As String
    Dim strClubhouse As String
    Dim strPreTaxTotal As String
    Dim strTaxRate As String
    Dim strTaxTotal As String
    Dim strGrandTotal As String
    Dim strrptUnitCount As String
    Dim strrptBldgCount As String

    Private Sub LoadReport()
        'cnn.ConnectionString = "workstation id=""CO-TECH-LT-003"";user id=SA;data source=""CO-TECH-LT-003"";initial catalog=LD-BID;password=vw9110"
        cnn.ConnectionString = frmMain.strConnect
        Dim strCommand As String
        strCommand = "rptIntro"
        Dim cmdCommand As New SqlCommand
        cmdCommand.Connection = cnn
        cmdCommand.CommandText = strCommand
        cmdCommand.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdCommand.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daBid As New SqlDataAdapter(cmdCommand)
        'daBid.SelectCommand = cmdCommand

        Dim dsBid As New DataSet
        Try
            daBid.Fill(dsBid, "Project")
        Catch ex As Exception
            MsgBox("Failed to Fill Bid Data")
        End Try


        Dim strSummary As String
        strSummary = "LineItemSummary"
        Dim cmdSummary As New SqlCommand
        cmdSummary.Connection = cnn
        cmdSummary.CommandText = strSummary
        cmdSummary.CommandType = CommandType.StoredProcedure
        Dim prmSumBidID As SqlParameter = cmdSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSumBidID.Value = frmMain.intBidID
        Dim daSummary As New SqlDataAdapter(cmdSummary)
        Dim dsSummary As New DataSet
        Try
            daSummary.Fill(dsSummary, "LineItemSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill LineItemSummary")
        End Try


        Dim strBldgTypeCount As String
        strBldgTypeCount = "rptBldgTypeCount"
        Dim cmdBldgTypeCount As New SqlCommand
        cmdBldgTypeCount.Connection = cnn
        cmdBldgTypeCount.CommandText = strBldgTypeCount
        cmdBldgTypeCount.CommandType = CommandType.StoredProcedure
        Dim prmBldgTypeCountBidID As SqlParameter = cmdBldgTypeCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBldgTypeCountBidID.Value = frmMain.intBidID
        Dim daBldgTypeCount As New SqlDataAdapter(cmdBldgTypeCount)
        Dim dsBldgTypeCount As New DataSet
        Try
            daBldgTypeCount.Fill(dsBldgTypeCount, "rptBldgTypeCount")
        Catch ex As Exception
            MsgBox("Failed to Fill rptBldgTypeCount")
        End Try

        Dim strUnitDetailPerBldg As String
        strUnitDetailPerBldg = "rptUnitDetailPerBldg"
        Dim cmdUnitDetailPerBldg As New SqlCommand
        cmdUnitDetailPerBldg.Connection = cnn
        cmdUnitDetailPerBldg.CommandText = strUnitDetailPerBldg
        cmdUnitDetailPerBldg.CommandType = CommandType.StoredProcedure
        Dim prmUnitDetailPerBldgBidID As SqlParameter = cmdUnitDetailPerBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitDetailPerBldgBidID.Value = frmMain.intBidID
        Dim daUnitDetailPerBldg As New SqlDataAdapter(cmdUnitDetailPerBldg)
        Dim dsUnitDetailPerBldg As New DataSet
        Try
            daUnitDetailPerBldg.Fill(dsUnitDetailPerBldg, "rptUnitDetailPerBldg")
        Catch ex As Exception
            MsgBox("Failed to Fill rptUnitDetailPerBldg")
        End Try

        Dim strUnitCount As String
        strUnitCount = "ProjectUnitCount"
        Dim cmdUnitCount As New SqlCommand
        cmdUnitCount.Connection = cnn
        cmdUnitCount.CommandText = strUnitCount
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmUnitCountBidID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitCountBidID.Value = frmMain.intBidID
        Dim daUnitCount As New SqlDataAdapter(cmdUnitCount)
        Dim dsUnitCount As New DataSet
        Try
            daUnitCount.Fill(dsUnitCount, "ProjectUnitCount")
        Catch ex As Exception
            MsgBox("Failed to Fill ProjectUnitCount")
        End Try

        Dim strTypeSummary As String
        strTypeSummary = "TypeSummary"
        Dim cmdTypeSummary As New SqlCommand
        cmdTypeSummary.Connection = cnn
        cmdTypeSummary.CommandText = strTypeSummary
        cmdTypeSummary.CommandType = CommandType.StoredProcedure
        Dim prmTypeSummaryBidID As SqlParameter = cmdTypeSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmTypeSummaryBidID.Value = frmMain.intBidID
        Dim daTypeSummary As New SqlDataAdapter(cmdTypeSummary)
        Dim dsTypeSummary As New DataSet
        Try
            daTypeSummary.Fill(dsTypeSummary, "TypeSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill TypeSummary")
        End Try

        Dim strTypeSummaryNoType As String
        strTypeSummaryNoType = "TypeSummary"
        Dim cmdTypeSummaryNoType As New SqlCommand
        cmdTypeSummaryNoType.Connection = cnn
        cmdTypeSummaryNoType.CommandText = strTypeSummaryNoType
        cmdTypeSummaryNoType.CommandType = CommandType.StoredProcedure
        Dim prmTypeSummaryNoTypeBidID As SqlParameter = cmdTypeSummaryNoType.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmTypeSummaryNoTypeBidID.Value = frmMain.intBidID
        Dim daTypeSummaryNoType As New SqlDataAdapter(cmdTypeSummaryNoType)
        Dim dsTypeSummaryNoType As New DataSet
        Try
            daTypeSummaryNoType.Fill(dsTypeSummaryNoType, "TypeSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill TypeSummary")
        End Try

        Dim strUnitDetail As String
        strUnitDetail = "rptQuantityDetails"
        Dim cmdUnitDetail As New SqlCommand
        cmdUnitDetail.Connection = cnn
        cmdUnitDetail.CommandText = strUnitDetail
        cmdUnitDetail.CommandType = CommandType.StoredProcedure
        Dim prmUnitDetailBidID As SqlParameter = cmdUnitDetail.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitDetailBidID.Value = frmMain.intBidID
        Dim daUnitDetail As New SqlDataAdapter(cmdUnitDetail)
        Dim dsUnitDetail As New DataSet
        Try
            daUnitDetail.Fill(dsUnitDetail, "rptQuantityDetails")
        Catch ex As Exception
            MsgBox("Failed to Fill rptQuantityDetails")
        End Try

        Dim strClubHouseSummary As String
        strClubHouseSummary = "ClubHouseSummary"
        Dim cmdClubHouseSummary As New SqlCommand
        cmdClubHouseSummary.Connection = cnn
        cmdClubHouseSummary.CommandText = strClubHouseSummary
        cmdClubHouseSummary.CommandType = CommandType.StoredProcedure
        Dim prmClubHouseSummaryBidID As SqlParameter = cmdClubHouseSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmClubHouseSummaryBidID.Value = frmMain.intBidID
        Dim daClubHouseSummary As New SqlDataAdapter(cmdClubHouseSummary)
        Dim dsClubHouseSummary As New DataSet
        Try
            daClubHouseSummary.Fill(dsClubHouseSummary, "ClubHouseSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill ClubHouseSummary")
        End Try

        Dim strCommonDetail As String
        strCommonDetail = "rptQuantityDetails"
        Dim cmdCommonDetail As New SqlCommand
        cmdCommonDetail.Connection = cnn
        cmdCommonDetail.CommandText = strCommonDetail
        cmdCommonDetail.CommandType = CommandType.StoredProcedure
        Dim prmCommonDetailBidID As SqlParameter = cmdCommonDetail.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmCommonDetailBidID.Value = frmMain.intBidID
        Dim daCommonDetail As New SqlDataAdapter(cmdCommonDetail)
        Dim dsCommonDetail As New DataSet
        Try
            daCommonDetail.Fill(dsCommonDetail, "rptQuantityDetails")
        Catch ex As Exception
            MsgBox("Failed to Fill rptQuantityDetails")
        End Try

        Dim strClubHouseDetail As String
        strClubHouseDetail = "rptQuantityDetails"
        Dim cmdClubHouseDetail As New SqlCommand
        cmdClubHouseDetail.Connection = cnn
        cmdClubHouseDetail.CommandText = strClubHouseDetail
        cmdClubHouseDetail.CommandType = CommandType.StoredProcedure
        Dim prmClubHouseDetailBidID As SqlParameter = cmdClubHouseDetail.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmClubHouseDetailBidID.Value = frmMain.intBidID
        Dim daClubHouseDetail As New SqlDataAdapter(cmdClubHouseDetail)
        Dim dsClubHouseDetail As New DataSet
        Try
            daClubHouseDetail.Fill(dsClubHouseDetail, "rptQuantityDetails")
            If dsClubHouseDetail.Tables(0).Rows.Count > 1 Then
                MsgBox("Club House Detail Set")
            End If
        Catch ex As Exception
            MsgBox("Failed to Fill rptQuantityDetails")
        End Try



        Dim strSiteSummary As String
        strSiteSummary = "UnitSummary"
        Dim cmdSiteSummary As New SqlCommand
        cmdSiteSummary.Connection = cnn
        cmdSiteSummary.CommandText = strSiteSummary
        cmdSiteSummary.CommandType = CommandType.StoredProcedure
        Dim prmSiteSummaryBidID As SqlParameter = cmdSiteSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSiteSummaryBidID.Value = frmMain.intBidID
        Dim daSiteSummary As New SqlDataAdapter(cmdSiteSummary)
        Dim dsSiteSummary As New DataSet
        Try
            daSiteSummary.Fill(dsSiteSummary, "UnitSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill UnitSummary")
        End Try



        Dim strUnitSummary As String
        strUnitSummary = "UnitSummary"
        Dim cmdUnitSummary As New SqlCommand
        cmdUnitSummary.Connection = cnn
        cmdUnitSummary.CommandText = strUnitSummary
        cmdUnitSummary.CommandType = CommandType.StoredProcedure
        Dim prmUnitSummaryBidID As SqlParameter = cmdUnitSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmUnitSummaryBidID.Value = frmMain.intBidID
        Dim daUnitSummary As New SqlDataAdapter(cmdUnitSummary)
        Dim dsUnitSummary As New DataSet
        Try
            daUnitSummary.Fill(dsUnitSummary, "UnitSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill UnitSummary")
        End Try

        Dim strCommonSummary As String
        strCommonSummary = "CommonSummary"
        Dim cmdCommonSummary As New SqlCommand
        cmdCommonSummary.Connection = cnn
        cmdCommonSummary.CommandText = strCommonSummary
        cmdCommonSummary.CommandType = CommandType.StoredProcedure
        Dim prmCommonSummaryBidID As SqlParameter = cmdCommonSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmCommonSummaryBidID.Value = frmMain.intBidID
        Dim daCommonSummary As New SqlDataAdapter(cmdCommonSummary)
        Dim dsCommonSummary As New DataSet
        Try
            daCommonSummary.Fill(dsCommonSummary, "CommonSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill CommonSummary")
        End Try


        Dim strAlternates As String
        strAlternates = "AlternateSummary"
        Dim cmdAlternates As New SqlCommand
        cmdAlternates.Connection = cnn
        cmdAlternates.CommandText = strAlternates
        cmdAlternates.CommandType = CommandType.StoredProcedure
        Dim prmAlternatesBidID As SqlParameter = cmdAlternates.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmAlternatesBidID.Value = frmMain.intBidID
        Dim daAlternates As New SqlDataAdapter(cmdAlternates)
        Dim dsAlternates As New DataSet
        Try
            daAlternates.Fill(dsAlternates, "AlternateSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill AlternateSummary")
        End Try

        Dim strBidComments As String
        strBidComments = "rptComments"
        Dim cmdBidComments As New SqlCommand
        cmdBidComments.Connection = cnn
        cmdBidComments.CommandText = strBidComments
        cmdBidComments.CommandType = CommandType.StoredProcedure
        Dim prmBidCommentsBidID As SqlParameter = cmdBidComments.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidCommentsBidID.Value = frmMain.intBidID
        Dim daBidComments As New SqlDataAdapter(cmdBidComments)
        Dim dsBidComments As New DataSet
        Try
            daBidComments.Fill(dsBidComments, "rptComments")
        Catch ex As Exception
            MsgBox("Failed to Fill rptComments")
        End Try

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Try
            myReport = New rptBidMain
            myReport.SetDataSource(dsBid)
            myReport.OpenSubreport("rptBidComments.rpt").SetDataSource(dsBidComments)
            myReport.OpenSubreport("rptBldgTypeCount.rpt").SetDataSource(dsBldgTypeCount)
            myReport.OpenSubreport("rptUnitCount.rpt").SetDataSource(dsUnitCount)
            myReport.OpenSubreport("rptTypeSummary.rpt").SetDataSource(dsTypeSummary)
            myReport.OpenSubreport("rptTypeSummaryNotype.rpt").SetDataSource(dsTypeSummaryNoType)
            myReport.OpenSubreport("rptUnitDetailPerBldg1.rpt").SetDataSource(dsUnitDetailPerBldg)
            myReport.OpenSubreport("rptUnitDetails.rpt").SetDataSource(dsUnitDetail)
            myReport.OpenSubreport("rptCommonDetails.rpt").SetDataSource(dsCommonDetail)

            myReport.OpenSubreport("rptClubHouseSummary.rpt").SetDataSource(dsClubHouseSummary)
            myReport.OpenSubreport("rptSiteSummary.rpt").SetDataSource(dsSiteSummary)
            myReport.OpenSubreport("rptUnitSummary.rpt").SetDataSource(dsUnitSummary)

            myReport.OpenSubreport("rptCommonSummary.rpt").SetDataSource(dsCommonSummary)
            myReport.OpenSubreport("rptBidSummary.rpt").SetDataSource(dsSummary)
            myReport.OpenSubreport("rptBidAlternates.rpt").SetDataSource(dsAlternates)
            myReport.OpenSubreport("rptQuantityDetails.rpt").SetDataSource(dsClubHouseDetail)
        Catch ex As Exception
            MsgBox("Failed to Open Subreports")
        End Try

        Dim UnitsText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitsText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnits"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitsText.Text = strUnits

        Dim CommonText As CrystalDecisions.CrystalReports.Engine.TextObject
        CommonText = CType(myReport.ReportDefinition.ReportObjects.Item("txtCommon"), CrystalDecisions.CrystalReports.Engine.TextObject)
        CommonText.Text = strCommon

        Dim SiteText As CrystalDecisions.CrystalReports.Engine.TextObject
        SiteText = CType(myReport.ReportDefinition.ReportObjects.Item("txtSite"), CrystalDecisions.CrystalReports.Engine.TextObject)
        SiteText.Text = strSite

        Dim ClubhouseText As CrystalDecisions.CrystalReports.Engine.TextObject
        ClubhouseText = CType(myReport.ReportDefinition.ReportObjects.Item("txtClubhouse"), CrystalDecisions.CrystalReports.Engine.TextObject)
        ClubhouseText.Text = strClubhouse

        Dim PreTaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        PreTaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtPreTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        PreTaxTotalText.Text = strPreTaxTotal

        Dim TaxRateText As CrystalDecisions.CrystalReports.Engine.TextObject
        TaxRateText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxRate"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TaxRateText.Text = FormatPercent(CDbl(strTaxRate), 2)

        Dim TaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        TaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TaxTotalText.Text = strTaxTotal

        Dim GrandTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        GrandTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtGrandTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        GrandTotalText.Text = strGrandTotal

        Dim UnitCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnitCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitCountText.Text = strrptUnitCount

        Dim BldgCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        BldgCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtBldgCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        BldgCountText.Text = strrptBldgCount

        'Dim MySectionUnitDetailsPerBldg As CrystalDecisions.CrystalReports.Engine.Section
        'MySectionUnitDetailsPerBldg = myReport.ReportDefinition.Sections.Item("Section8")
        'MySectionUnitDetailsPerBldg.SectionFormat.EnableSuppress = True

        'Dim MySectionUnitDetails As CrystalDecisions.CrystalReports.Engine.Section
        'MySectionUnitDetails = myReport.ReportDefinition.Sections.Item("Section9")
        'MySectionUnitDetails.SectionFormat.EnableSuppress = True

        'Dim MySectionBOM As CrystalDecisions.CrystalReports.Engine.Section
        'MySectionBOM = myReport.ReportDefinition.Sections.Item("Section6")
        'MySectionBOM.SectionFormat.EnableSuppress = True
        If dsBidComments.Tables(0).Rows.Count = 0 Then
            Dim MySectionComment As CrystalDecisions.CrystalReports.Engine.Section
            MySectionComment = myReport.ReportDefinition.Sections.Item("Section18")
            MySectionComment.SectionFormat.EnableSuppress = True
        End If

        CrystalReportViewer1.ReportSource = myReport
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(592, 330)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'frmRptMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(592, 330)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.MinimizeBox = False
        Me.Name = "frmRptMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bid Report"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadSubTotals()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdMargins As New SqlCommand("Margins", cnn)
        cmdMargins.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drMargins As SqlDataReader
        cnn.Open()
        drMargins = cmdMargins.ExecuteReader

        While drMargins.Read
            If drMargins.GetString(0) = "U" Then
                'txtMarUnitCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strUnits = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'txtMarUnitMargin.Text = CStr(FormatPercent(CDbl(1 - (txtMarUnitCost.Text) / CDbl(txtMarUnitSell.Text)), 2))
            ElseIf drMargins.GetString(0) = "C" Then
                'txtMarCHCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strClubhouse = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'txtMarCHMargin.Text = CStr(FormatPercent(CDbl(1 - (txtMarCHCost.Text) / CDbl(txtMarCHSell.Text)), 2))
            ElseIf drMargins.GetString(0) = "S" Then
                'txtMarSiteCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strSite = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'txtMarSiteMargin.Text = CStr(FormatPercent(CDbl(1 - (txtMarSiteCost.Text) / CDbl(txtMarSiteSell.Text)), 2))
            ElseIf drMargins.GetString(0) = "X" Then
                'strCommonCost = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                strCommon = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                'StrCommonMargin = CStr(FormatPercent(CDbl(1 - (strCommonCost) / CDbl(strCommon)), 2))
            End If

        End While
        drMargins.Close()
        cnn.Close()


        strPreTaxTotal = Format(CDbl(strUnits) + CDbl(strSite) + CDbl(strClubhouse) + CDbl(strCommon), "C")

        Dim cmdSalesTaxRate As New SqlCommand("rptSalesTaxRate", cnn)
        cmdSalesTaxRate.CommandType = CommandType.StoredProcedure
        Dim prmSalesTaxBidID As SqlParameter = cmdSalesTaxRate.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSalesTaxBidID.Value = frmMain.intBidID
        Dim drSalesTaxRate As SqlDataReader = Nothing
        cnn.Open()
        Try
            drSalesTaxRate = cmdSalesTaxRate.ExecuteReader
            drSalesTaxRate.Read()
            strTaxRate = drSalesTaxRate.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drSalesTaxRate.Close()
            cnn.Close()
        End Try


        strTaxTotal = Format(CDbl(strPreTaxTotal) * CDbl(strTaxRate), "C")
        strGrandTotal = Format(CDbl(strPreTaxTotal) + CDbl(strTaxTotal), "C")

    End Sub

    Private Sub LoadCounts()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdUnitCount As New SqlCommand("rptUnitCount", cnn)
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drUnitCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drUnitCount = cmdUnitCount.ExecuteReader
            drUnitCount.Read()
            strrptUnitCount = drUnitCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drUnitCount.Close()
            cnn.Close()
        End Try

        Dim cmdBldgCount As New SqlCommand("rptBldgCount", cnn)
        cmdBldgCount.CommandType = CommandType.StoredProcedure
        Dim prmBldgBidID As SqlParameter = cmdBldgCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBldgBidID.Value = frmMain.intBidID
        Dim drBldgCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drBldgCount = cmdBldgCount.ExecuteReader
            drBldgCount.Read()
            strrptBldgCount = drBldgCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drBldgCount.Close()
            cnn.Close()
        End Try



    End Sub

    Private Sub frmRptMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadSubTotals()
        LoadCounts()
        LoadReport()
    End Sub
End Class
