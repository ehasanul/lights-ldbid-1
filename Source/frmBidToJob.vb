Imports System.Data
Imports System.Data.SqlClient
Public Class frmBidToJob
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim dsBids As New DataSet
    Public Shared blnStartUnit As Boolean 'Tests Startup Condition in LoadUnits
    Dim blnStartBldg As Boolean 'Tests Startup Condition in LoadBldgs
    Public Shared intUnitID As Integer
    Public Shared blnUnitReturn As Boolean
    Dim intUnitEditing As Integer
    Dim intNewJobID As Integer
    Dim intOldUnitID As Integer
    Dim intNewUnitID As Integer
    Dim intOldBldgId As Integer
    Dim intNewBldgID As Integer
    Dim intOldAltID As Integer
    Dim intClientID As Integer
    Dim dtShipDate As Date
    Dim strAuxLabel As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblSalesTaxRate As System.Windows.Forms.Label
    Friend WithEvents txtSalesTaxRate As System.Windows.Forms.TextBox
    Friend WithEvents txtRevDate As System.Windows.Forms.TextBox
    Friend WithEvents txtBluePrintDate As System.Windows.Forms.TextBox
    Friend WithEvents LBLRevDate As System.Windows.Forms.Label
    Friend WithEvents lblBluePrintDate As System.Windows.Forms.Label
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents txtbDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtBNotes As System.Windows.Forms.TextBox
    Friend WithEvents txtBZip As System.Windows.Forms.TextBox
    Friend WithEvents txtBAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtBCity As System.Windows.Forms.TextBox
    Friend WithEvents txtBState As System.Windows.Forms.TextBox
    Friend WithEvents txtBLocation As System.Windows.Forms.TextBox
    Friend WithEvents cboBid As System.Windows.Forms.ComboBox
    Friend WithEvents txtClientPM As System.Windows.Forms.TextBox
    Friend WithEvents txtClientMobilePhone As System.Windows.Forms.TextBox
    Friend WithEvents txtLDPM As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblClientID As System.Windows.Forms.Label
    Friend WithEvents txtContractDate As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCLemail As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLFAX As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLZip As System.Windows.Forms.TextBox
    Friend WithEvents txtCLState As System.Windows.Forms.TextBox
    Friend WithEvents txtCLCity As System.Windows.Forms.TextBox
    Friend WithEvents txtCLAddr2 As System.Windows.Forms.TextBox
    Friend WithEvents txtClAddr1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLContact As System.Windows.Forms.TextBox
    Friend WithEvents txtCLName As System.Windows.Forms.TextBox
    Friend WithEvents txtLDEstimator As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtJobDate As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtContractAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblSalesTaxRate = New System.Windows.Forms.Label()
        Me.txtSalesTaxRate = New System.Windows.Forms.TextBox()
        Me.txtRevDate = New System.Windows.Forms.TextBox()
        Me.txtBluePrintDate = New System.Windows.Forms.TextBox()
        Me.LBLRevDate = New System.Windows.Forms.Label()
        Me.lblBluePrintDate = New System.Windows.Forms.Label()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.txtbDescription = New System.Windows.Forms.TextBox()
        Me.txtBNotes = New System.Windows.Forms.TextBox()
        Me.txtBZip = New System.Windows.Forms.TextBox()
        Me.txtBAddress = New System.Windows.Forms.TextBox()
        Me.txtBCity = New System.Windows.Forms.TextBox()
        Me.txtBState = New System.Windows.Forms.TextBox()
        Me.txtBLocation = New System.Windows.Forms.TextBox()
        Me.cboBid = New System.Windows.Forms.ComboBox()
        Me.txtContractDate = New System.Windows.Forms.TextBox()
        Me.txtClientPM = New System.Windows.Forms.TextBox()
        Me.txtClientMobilePhone = New System.Windows.Forms.TextBox()
        Me.txtLDPM = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblClientID = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCLemail = New System.Windows.Forms.TextBox()
        Me.txtCLPhone2 = New System.Windows.Forms.TextBox()
        Me.txtCLFAX = New System.Windows.Forms.TextBox()
        Me.txtCLPhone1 = New System.Windows.Forms.TextBox()
        Me.txtCLZip = New System.Windows.Forms.TextBox()
        Me.txtCLState = New System.Windows.Forms.TextBox()
        Me.txtCLCity = New System.Windows.Forms.TextBox()
        Me.txtCLAddr2 = New System.Windows.Forms.TextBox()
        Me.txtClAddr1 = New System.Windows.Forms.TextBox()
        Me.txtCLContact = New System.Windows.Forms.TextBox()
        Me.txtCLName = New System.Windows.Forms.TextBox()
        Me.txtLDEstimator = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtJobDate = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtContractAmount = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblSalesTaxRate
        '
        Me.lblSalesTaxRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalesTaxRate.Location = New System.Drawing.Point(8, 172)
        Me.lblSalesTaxRate.Name = "lblSalesTaxRate"
        Me.lblSalesTaxRate.Size = New System.Drawing.Size(72, 20)
        Me.lblSalesTaxRate.TabIndex = 90
        Me.lblSalesTaxRate.Text = "Sales Tax Rate"
        '
        'txtSalesTaxRate
        '
        Me.txtSalesTaxRate.BackColor = System.Drawing.SystemColors.Info
        Me.txtSalesTaxRate.Location = New System.Drawing.Point(84, 172)
        Me.txtSalesTaxRate.Name = "txtSalesTaxRate"
        Me.txtSalesTaxRate.ReadOnly = True
        Me.txtSalesTaxRate.Size = New System.Drawing.Size(48, 20)
        Me.txtSalesTaxRate.TabIndex = 89
        Me.txtSalesTaxRate.TabStop = False
        Me.txtSalesTaxRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRevDate
        '
        Me.txtRevDate.BackColor = System.Drawing.SystemColors.Info
        Me.txtRevDate.Location = New System.Drawing.Point(364, 172)
        Me.txtRevDate.Name = "txtRevDate"
        Me.txtRevDate.ReadOnly = True
        Me.txtRevDate.Size = New System.Drawing.Size(80, 20)
        Me.txtRevDate.TabIndex = 88
        Me.txtRevDate.TabStop = False
        Me.txtRevDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBluePrintDate
        '
        Me.txtBluePrintDate.BackColor = System.Drawing.SystemColors.Info
        Me.txtBluePrintDate.Location = New System.Drawing.Point(208, 172)
        Me.txtBluePrintDate.Name = "txtBluePrintDate"
        Me.txtBluePrintDate.ReadOnly = True
        Me.txtBluePrintDate.Size = New System.Drawing.Size(104, 20)
        Me.txtBluePrintDate.TabIndex = 87
        Me.txtBluePrintDate.TabStop = False
        Me.txtBluePrintDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LBLRevDate
        '
        Me.LBLRevDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLRevDate.Location = New System.Drawing.Point(316, 172)
        Me.LBLRevDate.Name = "LBLRevDate"
        Me.LBLRevDate.Size = New System.Drawing.Size(48, 20)
        Me.LBLRevDate.TabIndex = 86
        Me.LBLRevDate.Text = "Rev Date"
        '
        'lblBluePrintDate
        '
        Me.lblBluePrintDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBluePrintDate.Location = New System.Drawing.Point(136, 176)
        Me.lblBluePrintDate.Name = "lblBluePrintDate"
        Me.lblBluePrintDate.Size = New System.Drawing.Size(68, 16)
        Me.lblBluePrintDate.TabIndex = 85
        Me.lblBluePrintDate.Text = "Blue Print Date"
        '
        'btnCopy
        '
        Me.btnCopy.Location = New System.Drawing.Point(44, 412)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(492, 28)
        Me.btnCopy.TabIndex = 7
        Me.btnCopy.Text = "Generate Job From This Bid"
        Me.btnCopy.Visible = False
        '
        'txtbDescription
        '
        Me.txtbDescription.BackColor = System.Drawing.SystemColors.Info
        Me.txtbDescription.Location = New System.Drawing.Point(4, 68)
        Me.txtbDescription.Name = "txtbDescription"
        Me.txtbDescription.ReadOnly = True
        Me.txtbDescription.Size = New System.Drawing.Size(288, 20)
        Me.txtbDescription.TabIndex = 69
        Me.txtbDescription.TabStop = False
        '
        'txtBNotes
        '
        Me.txtBNotes.BackColor = System.Drawing.SystemColors.Info
        Me.txtBNotes.Location = New System.Drawing.Point(4, 116)
        Me.txtBNotes.Multiline = True
        Me.txtBNotes.Name = "txtBNotes"
        Me.txtBNotes.ReadOnly = True
        Me.txtBNotes.Size = New System.Drawing.Size(576, 52)
        Me.txtBNotes.TabIndex = 75
        Me.txtBNotes.TabStop = False
        '
        'txtBZip
        '
        Me.txtBZip.BackColor = System.Drawing.SystemColors.Info
        Me.txtBZip.Location = New System.Drawing.Point(500, 92)
        Me.txtBZip.Name = "txtBZip"
        Me.txtBZip.ReadOnly = True
        Me.txtBZip.Size = New System.Drawing.Size(80, 20)
        Me.txtBZip.TabIndex = 74
        Me.txtBZip.TabStop = False
        '
        'txtBAddress
        '
        Me.txtBAddress.BackColor = System.Drawing.SystemColors.Info
        Me.txtBAddress.Location = New System.Drawing.Point(4, 92)
        Me.txtBAddress.Name = "txtBAddress"
        Me.txtBAddress.ReadOnly = True
        Me.txtBAddress.Size = New System.Drawing.Size(288, 20)
        Me.txtBAddress.TabIndex = 71
        Me.txtBAddress.TabStop = False
        '
        'txtBCity
        '
        Me.txtBCity.BackColor = System.Drawing.SystemColors.Info
        Me.txtBCity.Location = New System.Drawing.Point(308, 92)
        Me.txtBCity.Name = "txtBCity"
        Me.txtBCity.ReadOnly = True
        Me.txtBCity.Size = New System.Drawing.Size(144, 20)
        Me.txtBCity.TabIndex = 72
        Me.txtBCity.TabStop = False
        '
        'txtBState
        '
        Me.txtBState.BackColor = System.Drawing.SystemColors.Info
        Me.txtBState.Location = New System.Drawing.Point(460, 92)
        Me.txtBState.Name = "txtBState"
        Me.txtBState.ReadOnly = True
        Me.txtBState.Size = New System.Drawing.Size(32, 20)
        Me.txtBState.TabIndex = 73
        Me.txtBState.TabStop = False
        '
        'txtBLocation
        '
        Me.txtBLocation.BackColor = System.Drawing.SystemColors.Info
        Me.txtBLocation.Location = New System.Drawing.Point(308, 68)
        Me.txtBLocation.Name = "txtBLocation"
        Me.txtBLocation.ReadOnly = True
        Me.txtBLocation.Size = New System.Drawing.Size(272, 20)
        Me.txtBLocation.TabIndex = 70
        Me.txtBLocation.TabStop = False
        '
        'cboBid
        '
        Me.cboBid.Location = New System.Drawing.Point(4, 12)
        Me.cboBid.Name = "cboBid"
        Me.cboBid.Size = New System.Drawing.Size(352, 21)
        Me.cboBid.TabIndex = 0
        Me.cboBid.Text = "Select Bid "
        '
        'txtContractDate
        '
        Me.txtContractDate.Location = New System.Drawing.Point(104, 340)
        Me.txtContractDate.Name = "txtContractDate"
        Me.txtContractDate.Size = New System.Drawing.Size(116, 20)
        Me.txtContractDate.TabIndex = 1
        Me.txtContractDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtClientPM
        '
        Me.txtClientPM.Location = New System.Drawing.Point(104, 360)
        Me.txtClientPM.Name = "txtClientPM"
        Me.txtClientPM.Size = New System.Drawing.Size(292, 20)
        Me.txtClientPM.TabIndex = 4
        '
        'txtClientMobilePhone
        '
        Me.txtClientMobilePhone.Location = New System.Drawing.Point(484, 364)
        Me.txtClientMobilePhone.Name = "txtClientMobilePhone"
        Me.txtClientMobilePhone.Size = New System.Drawing.Size(104, 20)
        Me.txtClientMobilePhone.TabIndex = 5
        '
        'txtLDPM
        '
        Me.txtLDPM.Location = New System.Drawing.Point(104, 380)
        Me.txtLDPM.Name = "txtLDPM"
        Me.txtLDPM.Size = New System.Drawing.Size(292, 20)
        Me.txtLDPM.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 342)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 16)
        Me.Label1.TabIndex = 97
        Me.Label1.Text = "Contract Date"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 360)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 98
        Me.Label2.Text = "Client PM"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 382)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 16)
        Me.Label3.TabIndex = 99
        Me.Label3.Text = "LD PM"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(404, 364)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 16)
        Me.Label4.TabIndex = 100
        Me.Label4.Text = "Mobile Phone"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(96, 312)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(396, 16)
        Me.Label5.TabIndex = 101
        Me.Label5.Text = "The Following Fields Must Be Filled In Prior To Generating A Job"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblClientID
        '
        Me.lblClientID.Location = New System.Drawing.Point(4, 200)
        Me.lblClientID.Name = "lblClientID"
        Me.lblClientID.Size = New System.Drawing.Size(84, 20)
        Me.lblClientID.TabIndex = 102
        Me.lblClientID.Text = "ClientID"
        Me.lblClientID.Visible = False
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(312, 284)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 16)
        Me.Label6.TabIndex = 116
        Me.Label6.Text = "e-mail"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(312, 264)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 16)
        Me.Label7.TabIndex = 115
        Me.Label7.Text = "FAX"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(312, 244)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 16)
        Me.Label8.TabIndex = 114
        Me.Label8.Text = "Phone"
        '
        'txtCLemail
        '
        Me.txtCLemail.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLemail.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLemail.Location = New System.Drawing.Point(366, 280)
        Me.txtCLemail.Name = "txtCLemail"
        Me.txtCLemail.ReadOnly = True
        Me.txtCLemail.Size = New System.Drawing.Size(216, 20)
        Me.txtCLemail.TabIndex = 113
        Me.txtCLemail.TabStop = False
        '
        'txtCLPhone2
        '
        Me.txtCLPhone2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone2.Location = New System.Drawing.Point(478, 240)
        Me.txtCLPhone2.Name = "txtCLPhone2"
        Me.txtCLPhone2.ReadOnly = True
        Me.txtCLPhone2.Size = New System.Drawing.Size(104, 20)
        Me.txtCLPhone2.TabIndex = 111
        Me.txtCLPhone2.TabStop = False
        '
        'txtCLFAX
        '
        Me.txtCLFAX.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLFAX.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLFAX.Location = New System.Drawing.Point(366, 260)
        Me.txtCLFAX.Name = "txtCLFAX"
        Me.txtCLFAX.ReadOnly = True
        Me.txtCLFAX.Size = New System.Drawing.Size(112, 20)
        Me.txtCLFAX.TabIndex = 112
        Me.txtCLFAX.TabStop = False
        '
        'txtCLPhone1
        '
        Me.txtCLPhone1.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone1.Location = New System.Drawing.Point(366, 240)
        Me.txtCLPhone1.Name = "txtCLPhone1"
        Me.txtCLPhone1.ReadOnly = True
        Me.txtCLPhone1.Size = New System.Drawing.Size(112, 20)
        Me.txtCLPhone1.TabIndex = 110
        Me.txtCLPhone1.TabStop = False
        '
        'txtCLZip
        '
        Me.txtCLZip.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLZip.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLZip.Location = New System.Drawing.Point(190, 280)
        Me.txtCLZip.Name = "txtCLZip"
        Me.txtCLZip.ReadOnly = True
        Me.txtCLZip.Size = New System.Drawing.Size(80, 20)
        Me.txtCLZip.TabIndex = 108
        Me.txtCLZip.TabStop = False
        '
        'txtCLState
        '
        Me.txtCLState.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLState.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLState.Location = New System.Drawing.Point(150, 280)
        Me.txtCLState.Name = "txtCLState"
        Me.txtCLState.ReadOnly = True
        Me.txtCLState.Size = New System.Drawing.Size(40, 20)
        Me.txtCLState.TabIndex = 107
        Me.txtCLState.TabStop = False
        '
        'txtCLCity
        '
        Me.txtCLCity.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLCity.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLCity.Location = New System.Drawing.Point(4, 280)
        Me.txtCLCity.Name = "txtCLCity"
        Me.txtCLCity.ReadOnly = True
        Me.txtCLCity.Size = New System.Drawing.Size(144, 20)
        Me.txtCLCity.TabIndex = 106
        Me.txtCLCity.TabStop = False
        '
        'txtCLAddr2
        '
        Me.txtCLAddr2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLAddr2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLAddr2.Location = New System.Drawing.Point(4, 260)
        Me.txtCLAddr2.Name = "txtCLAddr2"
        Me.txtCLAddr2.ReadOnly = True
        Me.txtCLAddr2.Size = New System.Drawing.Size(265, 20)
        Me.txtCLAddr2.TabIndex = 105
        Me.txtCLAddr2.TabStop = False
        '
        'txtClAddr1
        '
        Me.txtClAddr1.BackColor = System.Drawing.SystemColors.Info
        Me.txtClAddr1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtClAddr1.Location = New System.Drawing.Point(4, 240)
        Me.txtClAddr1.Name = "txtClAddr1"
        Me.txtClAddr1.ReadOnly = True
        Me.txtClAddr1.Size = New System.Drawing.Size(265, 20)
        Me.txtClAddr1.TabIndex = 104
        Me.txtClAddr1.TabStop = False
        '
        'txtCLContact
        '
        Me.txtCLContact.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLContact.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLContact.Location = New System.Drawing.Point(366, 220)
        Me.txtCLContact.Name = "txtCLContact"
        Me.txtCLContact.ReadOnly = True
        Me.txtCLContact.Size = New System.Drawing.Size(216, 20)
        Me.txtCLContact.TabIndex = 109
        Me.txtCLContact.TabStop = False
        '
        'txtCLName
        '
        Me.txtCLName.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLName.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLName.Location = New System.Drawing.Point(4, 220)
        Me.txtCLName.Name = "txtCLName"
        Me.txtCLName.ReadOnly = True
        Me.txtCLName.Size = New System.Drawing.Size(265, 20)
        Me.txtCLName.TabIndex = 103
        Me.txtCLName.TabStop = False
        '
        'txtLDEstimator
        '
        Me.txtLDEstimator.BackColor = System.Drawing.SystemColors.Info
        Me.txtLDEstimator.Location = New System.Drawing.Point(496, 172)
        Me.txtLDEstimator.Name = "txtLDEstimator"
        Me.txtLDEstimator.ReadOnly = True
        Me.txtLDEstimator.Size = New System.Drawing.Size(84, 20)
        Me.txtLDEstimator.TabIndex = 117
        Me.txtLDEstimator.TabStop = False
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(444, 176)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 16)
        Me.Label9.TabIndex = 118
        Me.Label9.Text = "Estimator"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(244, 40)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 16)
        Me.Label10.TabIndex = 119
        Me.Label10.Text = "Job"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(252, 196)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 20)
        Me.Label11.TabIndex = 120
        Me.Label11.Text = "Client"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtJobDate
        '
        Me.txtJobDate.Location = New System.Drawing.Point(296, 340)
        Me.txtJobDate.Name = "txtJobDate"
        Me.txtJobDate.Size = New System.Drawing.Size(100, 20)
        Me.txtJobDate.TabIndex = 2
        Me.txtJobDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(228, 344)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 12)
        Me.Label12.TabIndex = 122
        Me.Label12.Text = "Job Date"
        '
        'txtContractAmount
        '
        Me.txtContractAmount.Location = New System.Drawing.Point(484, 336)
        Me.txtContractAmount.Name = "txtContractAmount"
        Me.txtContractAmount.Size = New System.Drawing.Size(104, 20)
        Me.txtContractAmount.TabIndex = 3
        Me.txtContractAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(404, 340)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 16)
        Me.Label13.TabIndex = 124
        Me.Label13.Text = "Contract Amount"
        '
        'frmBidToJob
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(596, 458)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtContractAmount)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtJobDate)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtLDEstimator)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtCLemail)
        Me.Controls.Add(Me.txtCLPhone2)
        Me.Controls.Add(Me.txtCLFAX)
        Me.Controls.Add(Me.txtCLPhone1)
        Me.Controls.Add(Me.txtCLZip)
        Me.Controls.Add(Me.txtCLState)
        Me.Controls.Add(Me.txtCLCity)
        Me.Controls.Add(Me.txtCLAddr2)
        Me.Controls.Add(Me.txtClAddr1)
        Me.Controls.Add(Me.txtCLContact)
        Me.Controls.Add(Me.txtCLName)
        Me.Controls.Add(Me.lblClientID)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtLDPM)
        Me.Controls.Add(Me.txtClientMobilePhone)
        Me.Controls.Add(Me.txtClientPM)
        Me.Controls.Add(Me.txtContractDate)
        Me.Controls.Add(Me.lblSalesTaxRate)
        Me.Controls.Add(Me.txtSalesTaxRate)
        Me.Controls.Add(Me.txtRevDate)
        Me.Controls.Add(Me.txtBluePrintDate)
        Me.Controls.Add(Me.LBLRevDate)
        Me.Controls.Add(Me.lblBluePrintDate)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.txtbDescription)
        Me.Controls.Add(Me.txtBNotes)
        Me.Controls.Add(Me.txtBZip)
        Me.Controls.Add(Me.txtBAddress)
        Me.Controls.Add(Me.txtBCity)
        Me.Controls.Add(Me.txtBState)
        Me.Controls.Add(Me.txtBLocation)
        Me.Controls.Add(Me.cboBid)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBidToJob"
        Me.Text = "Convert Bid To A Job"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmBidToJob_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Text = "Convert " + frmMain.ConvertFrom + " to a " + frmMain.ConvertTo
        btnCopy.Text = "Generate " + frmMain.ConvertTo + " from this " + frmMain.ConvertFrom
        Label5.Text = "The Following Fields Must Be Filled In Prior To Generating A " + frmMain.ConvertTo
        Label12.Text = frmMain.ConvertTo + " Date"
        Label10.Text = frmMain.ConvertFrom

        LoadProjects()
    End Sub

    Private Sub LoadProjects()
        cnn.ConnectionString = frmMain.strConnect
        Dim projStr As String = "SELECT ProjectID,'LD-'+ RTRIM(CAST(LDID AS char))+ ': '+ISNULL(Description, '')+' '+ISNull(Location, '') AS Proj" + _
         " FROM Project Where ProjectType = '" + frmMain.ConvertFrom + "'"
        Dim daBids As New SqlDataAdapter(projStr, cnn)
        Dim dsBids As New DataSet
        daBids.Fill(dsBids, "Project")
        cboBid.DataSource = dsBids.Tables("Project")
        cboBid.DisplayMember = "Proj"
        cboBid.ValueMember = "ProjectID"
    End Sub

    Private Sub LoadBidInfo()
        Dim BidNum As String = Nothing
        cnn.ConnectionString = frmMain.strConnect
        Dim strBidInfo As String
        strBidInfo = "SELECT * FROM Project WHERE ProjectID = "
        strBidInfo = strBidInfo & cboBid.SelectedValue.ToString
        Dim cmdClientID As New SqlCommand(strBidInfo, cnn)
        cmdClientID.CommandType = CommandType.Text
        Dim drBid As SqlDataReader = Nothing
        Dim dtEntryDate As Date
        Try
            cnn.Open()
            drBid = cmdClientID.ExecuteReader
            drBid.Read()
            intClientID = System.Convert.ToInt32(drBid.Item("ClientID"))
            lblClientID.Text = intClientID.ToString
            txtbDescription.Text = drBid.Item("Description").ToString
            txtBLocation.Text = drBid.Item("Location").ToString
            txtBAddress.Text = drBid.Item("Address").ToString
            txtBCity.Text = drBid.Item("City").ToString
            txtBState.Text = drBid.Item("State").ToString
            txtBZip.Text = drBid.Item("Zip").ToString
            txtBNotes.Text = drBid.Item("Notes").ToString
            txtLDEstimator.Text = drBid.Item("LDEstimator").ToString
            txtBluePrintDate.Text = Format(drBid.Item("BluePrintDate"), "d")
            txtRevDate.Text = Format(drBid.Item("RevDate"), "d")
            txtSalesTaxRate.Text = CStr(drBid.Item("SalesTaxRate"))
            dtEntryDate = System.Convert.ToDateTime(drBid.Item("ProjectDate"))
            If frmMain.ConvertFrom = "Verbal" Then
                If Not IsDBNull(drBid.Item("ContractDate")) Then
                    txtContractDate.Text = Format(drBid.Item("ContractDate"), "d")
                End If
                If Not IsDBNull(drBid.Item("ProjectDate")) Then
                    txtJobDate.Text = Format(drBid.Item("ProjectDate"), "d")
                End If
                If Not IsDBNull(drBid.Item("ContractAmount")) Then
                    txtContractAmount.Text = drBid.Item("ContractAmount").ToString
                End If
                If Not IsDBNull(drBid.Item("ClientPM")) Then
                    txtClientPM.Text = drBid.Item("ClientPM").ToString
                End If
                If Not IsDBNull(drBid.Item("ClientMobilePhone")) Then
                    txtClientMobilePhone.Text = drBid.Item("ClientMobilePhone").ToString
                End If
                If Not IsDBNull(drBid.Item("LDPM")) Then
                    txtLDPM.Text = drBid.Item("LDPM").ToString
                End If
            End If
            If IsDBNull(drBid.Item("AuxLabel")) Then
                strAuxLabel = "Auxiliary"
            Else
                strAuxLabel = drBid.Item("AuxLabel").ToString
            End If

            BidNum = "LD-" & Format(dtEntryDate, "yy")
            BidNum = BidNum & "-" & drBid.Item("LDID").ToString
            LoadClientDetails()
        Catch ex As Exception
            MsgBox("Error Loading Bid Info")
        Finally
            drBid.Close()
            cnn.Close()
        End Try
        Me.Text = "Lights Direct Bid  -----    " & BidNum
    End Sub

    Sub LoadClientDetails()
        'Client Tab 
        'Check for Integet Length 1 to 4
        Dim cnnclient As New SqlConnection
        cnnclient.ConnectionString = frmMain.strConnect
        Dim strClientID As String
        strClientID = intClientID.ToString
        Dim spClientDetails As String = "ClientDetails"
        Dim cmdClientDetails As New SqlCommand(spClientDetails, cnnclient)
        cmdClientDetails.CommandType = CommandType.StoredProcedure
        cmdClientDetails.Parameters.AddWithValue("@ClientID", intClientID)
        Dim drClientDetails As SqlDataReader = Nothing
        If cnnclient.State = ConnectionState.Closed Then
            cnnclient.Open()
        End If

        Try
            drClientDetails = cmdClientDetails.ExecuteReader
            drClientDetails.Read()
            txtCLName.Text = drClientDetails.Item("Name").ToString
            txtClAddr1.Text = drClientDetails.Item("Address1").ToString
            txtCLAddr2.Text = drClientDetails.Item("Address2").ToString
            txtCLCity.Text = drClientDetails.Item("City").ToString
            txtCLState.Text = drClientDetails.Item("State").ToString
            txtCLZip.Text = drClientDetails.Item("Zip").ToString
            txtCLPhone1.Text = drClientDetails.Item("Phone1").ToString
            txtCLPhone2.Text = drClientDetails.Item("Phone2").ToString
            txtCLFAX.Text = drClientDetails.Item("FAX").ToString
            txtCLemail.Text = drClientDetails.Item("email").ToString
            txtCLContact.Text = drClientDetails.Item("contact").ToString

        Catch ex As Exception
            MsgBox("Error Loading Client Details")
        Finally
            drClientDetails.Close()
            cnnclient.Close()
        End Try
    End Sub

    Private Sub CopyBidToJob()
        'Copy the Primary Bid Data and Generate a New Job ID
        cnn.ConnectionString = frmMain.strConnect
        
        Dim CopyBidToJob As New SqlCommand("AddProject", cnn)
        CopyBidToJob.CommandType = CommandType.StoredProcedure

        CopyBidToJob.Parameters.AddWithValue("@SourceID", cboBid.SelectedValue)
        Dim LDID As Integer
        Dim LDIDQuery As String = "SELECT LDID FROM Project WHERE ProjectID = " + cboBid.SelectedValue.ToString
        Dim LDIDQU As New SqlCommand(LDIDQuery, cnn)
        LDIDQU.CommandType = CommandType.Text
        Dim LDIDDR As SqlDataReader = Nothing
        Try
            cnn.Open()
            LDIDDR = LDIDQU.ExecuteReader
            LDIDDR.Read()
            LDID = System.Convert.ToInt32(LDIDDR.Item("LDID"))
        Catch ex As Exception
            MsgBox("Error obtaining LDID: " + ex.ToString)
        Finally
            cnn.Close()
        End Try

        CopyBidToJob.Parameters.AddWithValue("@LDID", LDID)
        Dim prmClientID As SqlParameter = CopyBidToJob.Parameters.Add("@ClientID", SqlDbType.Int)
        prmClientID.Value = intClientID

        Dim prmContractDate As SqlParameter = CopyBidToJob.Parameters.Add("@ContractDate", SqlDbType.DateTime)
        prmContractDate.Value = txtContractDate.Text

        Dim prmJobDate As SqlParameter = CopyBidToJob.Parameters.Add("@ProjectDate", SqlDbType.DateTime)
        prmJobDate.Value = txtJobDate.Text

        Dim prmContractAmount As SqlParameter = CopyBidToJob.Parameters.Add("@ContractAmount", SqlDbType.Money)
        If IsNumeric(txtContractAmount.Text) Then
            prmContractAmount.Value = txtContractAmount.Text
        Else
            prmContractAmount.Value = 0
        End If

        Dim prmDescription As SqlParameter = CopyBidToJob.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbDescription.Text

        Dim prmLocation As SqlParameter = CopyBidToJob.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtBLocation.Text

        Dim prmAddress As SqlParameter = CopyBidToJob.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtBAddress.Text

        Dim prmCity As SqlParameter = CopyBidToJob.Parameters.Add("@City", SqlDbType.NVarChar, 50)
        prmCity.Value = txtBCity.Text

        Dim prmState As SqlParameter = CopyBidToJob.Parameters.Add("@State", SqlDbType.Char, 2)
        prmState.Value = txtBState.Text

        Dim prmZip As SqlParameter = CopyBidToJob.Parameters.Add("@Zip", SqlDbType.Char, 10)
        prmZip.Value = txtBZip.Text

        Dim prmBidContact As SqlParameter = CopyBidToJob.Parameters.Add("@ProjectContact", SqlDbType.NVarChar, 50)
        prmBidContact.Value = "Jim"

        Dim prmClientPM As SqlParameter = CopyBidToJob.Parameters.Add("@ClientPM", SqlDbType.NVarChar, 50)
        prmClientPM.Value = txtClientPM.Text

        Dim prmClientMobilePhone As SqlParameter = CopyBidToJob.Parameters.Add("@ClientMobilePhone", SqlDbType.Char, 14)
        prmClientMobilePhone.Value = txtClientMobilePhone.Text

        Dim prmLDPM As SqlParameter = CopyBidToJob.Parameters.Add("@LDPM", SqlDbType.NVarChar, 50)
        prmLDPM.Value = txtLDPM.Text

        Dim prmNotes As SqlParameter = CopyBidToJob.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtBNotes.Text

        Dim prmLDEstimator As SqlParameter = CopyBidToJob.Parameters.Add("@LDEstimator", SqlDbType.NVarChar, 50)
        prmLDEstimator.Value = txtLDEstimator.Text

        Dim prmBluePrintDate As SqlParameter = CopyBidToJob.Parameters.Add("@BluePrintDate", SqlDbType.DateTime)
        prmBluePrintDate.Value = txtBluePrintDate.Text

        Dim prmRevDate As SqlParameter = CopyBidToJob.Parameters.Add("@RevDate", SqlDbType.DateTime)
        If Not IsDate(txtRevDate.Text) Then
            prmRevDate.Value = Now()
        Else
            prmRevDate.Value = txtRevDate.Text
        End If

        Dim prmSalesTaxRate As SqlParameter = CopyBidToJob.Parameters.Add("@SalesTaxRate", SqlDbType.Float)
        If IsNumeric(txtSalesTaxRate.Text) Then
            prmSalesTaxRate.Value = CDbl(txtSalesTaxRate.Text)
        Else
            prmSalesTaxRate.Value = 0
        End If

        Dim prmAuxLabel As SqlParameter = CopyBidToJob.Parameters.Add("@AuxLabel", SqlDbType.Char, 25)
        prmAuxLabel.Value = strAuxLabel

        Dim prmProjType As SqlParameter = CopyBidToJob.Parameters.Add("@ProjectType", SqlDbType.NVarChar, 25)
        prmProjType.Value = frmMain.ConvertTo

        Dim prmNewJobID As SqlParameter = CopyBidToJob.Parameters.Add("@NewProjectID", SqlDbType.Int)
        prmNewJobID.Direction = ParameterDirection.Output
        prmNewJobID.Value = 0

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        CopyBidToJob.ExecuteNonQuery()
        cnn.Close()

        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If

        intNewJobID = System.Convert.ToInt32(prmNewJobID.Value)

    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        If Not IsDate(txtContractDate.Text) Then
            MsgBox("You must enter a valid Contract Date")
            txtContractDate.Focus()
            Exit Sub
        End If

        If Not IsDate(txtJobDate.Text) Then
            MsgBox("You must enter a valid Job Date")
            txtJobDate.Focus()
            Exit Sub
        End If

        If txtClientPM.Text = "" Then
            MsgBox("You Must Enter A Client PM!")
            txtClientPM.Focus()
            Exit Sub
        End If

        Dim cnntest As New SqlConnection
        cnntest.ConnectionString = frmMain.strConnect

        Dim strSQL As String
        strSQL = "SELECT bldgID FROM Building WHERE shipdate IS NULL AND ProjectID = " & cboBid.SelectedValue.ToString
        Dim cmdNullShipDate As New SqlCommand(strSQL, cnntest)
        Dim drNullShipDate As SqlDataReader = Nothing
        cnntest.Open()
        Try
            drNullShipDate = cmdNullShipDate.ExecuteReader

            If drNullShipDate.HasRows Then
                Dim strPrompt As String
                strPrompt = "In the Bid you have selected some Building Ship Dates are not defined!"
                strPrompt = strPrompt & vbCrLf
                strPrompt = strPrompt & vbCrLf
                strPrompt = strPrompt & "This Bid Will Not Be Copied AS IS!"
                MsgBox("In the Bid you have selected some Building Ship Dates are not defined!", MsgBoxStyle.Critical, "Ship Dates Must Be Defined")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("There was an error in checking for null Ship Dates")
        Finally
            drNullShipDate.Close()
            cnntest.Close()
            Me.Close()
        End Try

        Me.Cursor = Cursors.WaitCursor
        CopyBidToJob()
        CopyProjectDetails(cboBid.SelectedValue.ToString, intNewJobID)
        SetShipDateR()
        Me.Cursor = Cursors.Default
        Me.Close()
    End Sub

    Private Sub SetShipDateR()
        Dim cmdSetShipDateR As New SqlCommand("SetShipDateR", cnn)
        cmdSetShipDateR.CommandType = CommandType.StoredProcedure

        Dim prmLJobID As SqlParameter = cmdSetShipDateR.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmLJobID.Value = intNewJobID
        Try
            cnn.Open()
            cmdSetShipDateR.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error Setting the R Ship Dates")
        Finally
            cnn.Close()
        End Try


    End Sub

    Private Sub cboBid_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBid.SelectionChangeCommitted
        LoadBidInfo()
        btnCopy.Visible = True
    End Sub


End Class
