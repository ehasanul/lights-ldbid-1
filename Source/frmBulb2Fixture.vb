Imports System.Data
Imports System.Data.SqlClient
Public Class frmBulb2Fixture
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection ' Primary Connection
    Dim blnIsLoadingF As Boolean
    Dim blnIsLoadingB As Boolean
    Dim blnIsLoadingM As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtQuan As System.Windows.Forms.TextBox
    Friend WithEvents lblFixture As System.Windows.Forms.Label
    Friend WithEvents lblBulb As System.Windows.Forms.Label
    Friend WithEvents lblQuan As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents cboFixtures As System.Windows.Forms.ComboBox
    Friend WithEvents cboBulbs As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbxMappedBulbs As System.Windows.Forms.ListBox
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboFixtures = New System.Windows.Forms.ComboBox
        Me.cboBulbs = New System.Windows.Forms.ComboBox
        Me.txtQuan = New System.Windows.Forms.TextBox
        Me.lblFixture = New System.Windows.Forms.Label
        Me.lblBulb = New System.Windows.Forms.Label
        Me.lblQuan = New System.Windows.Forms.Label
        Me.btnAdd = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.lbxMappedBulbs = New System.Windows.Forms.ListBox
        Me.btnDelete = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboFixtures
        '
        Me.cboFixtures.Location = New System.Drawing.Point(12, 44)
        Me.cboFixtures.Name = "cboFixtures"
        Me.cboFixtures.Size = New System.Drawing.Size(212, 21)
        Me.cboFixtures.TabIndex = 1
        '
        'cboBulbs
        '
        Me.cboBulbs.Location = New System.Drawing.Point(224, 44)
        Me.cboBulbs.Name = "cboBulbs"
        Me.cboBulbs.Size = New System.Drawing.Size(212, 21)
        Me.cboBulbs.TabIndex = 2
        '
        'txtQuan
        '
        Me.txtQuan.Location = New System.Drawing.Point(440, 44)
        Me.txtQuan.Name = "txtQuan"
        Me.txtQuan.Size = New System.Drawing.Size(48, 20)
        Me.txtQuan.TabIndex = 3
        Me.txtQuan.Text = ""
        Me.txtQuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblFixture
        '
        Me.lblFixture.Location = New System.Drawing.Point(12, 28)
        Me.lblFixture.Name = "lblFixture"
        Me.lblFixture.Size = New System.Drawing.Size(60, 12)
        Me.lblFixture.TabIndex = 3
        Me.lblFixture.Text = "Fixture"
        '
        'lblBulb
        '
        Me.lblBulb.Location = New System.Drawing.Point(224, 24)
        Me.lblBulb.Name = "lblBulb"
        Me.lblBulb.Size = New System.Drawing.Size(96, 16)
        Me.lblBulb.TabIndex = 4
        Me.lblBulb.Text = "Bulb"
        '
        'lblQuan
        '
        Me.lblQuan.Location = New System.Drawing.Point(436, 24)
        Me.lblQuan.Name = "lblQuan"
        Me.lblQuan.Size = New System.Drawing.Size(48, 16)
        Me.lblQuan.TabIndex = 5
        Me.lblQuan.Text = "Quantity"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(428, 232)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(68, 20)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Add"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Bulbs Defined For This Fixture"
        '
        'lbxMappedBulbs
        '
        Me.lbxMappedBulbs.Location = New System.Drawing.Point(12, 132)
        Me.lbxMappedBulbs.Name = "lbxMappedBulbs"
        Me.lbxMappedBulbs.Size = New System.Drawing.Size(212, 121)
        Me.lbxMappedBulbs.TabIndex = 0
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(344, 232)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(72, 20)
        Me.btnDelete.TabIndex = 10
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.Visible = False
        '
        'frmBulb2Fixture
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(504, 262)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lbxMappedBulbs)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.lblQuan)
        Me.Controls.Add(Me.lblBulb)
        Me.Controls.Add(Me.lblFixture)
        Me.Controls.Add(Me.txtQuan)
        Me.Controls.Add(Me.cboBulbs)
        Me.Controls.Add(Me.cboFixtures)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBulb2Fixture"
        Me.Text = "Bulb To Fixture"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadBulbs()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadBulbs As New SqlCommand("CatMainItems", cnn)
        cmdLoadBulbs.CommandType = CommandType.StoredProcedure

        Dim daBulbs As New SqlDataAdapter(cmdLoadBulbs)
        Dim dsBulbs As New DataSet

        daBulbs.Fill(dsBulbs, "Bulbs")

        cboBulbs.DataSource = dsBulbs.Tables("Bulbs")
        cboBulbs.DisplayMember = "Item"
        cboBulbs.ValueMember = "ProductID"
        blnIsLoadingB = False

    End Sub

    Private Sub LoadFixtures()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadFixtures As New SqlCommand("CatMainItems", cnn)
        cmdLoadFixtures.CommandType = CommandType.StoredProcedure

        Dim daFixtures As New SqlDataAdapter(cmdLoadFixtures)
        Dim dsFixtures As New DataSet

        daFixtures.Fill(dsFixtures, "Fixtures")

        cboFixtures.DataSource = dsFixtures.Tables("Fixtures")
        cboFixtures.DisplayMember = "Item"
        cboFixtures.ValueMember = "ProductID"
        blnIsLoadingF = False
        blnIsLoadingM = False
    End Sub

    Private Sub frmBulb2Fixture_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        blnIsLoadingF = True
        blnIsLoadingB = True
        blnIsLoadingM = True
        LoadBulbs()
        LoadFixtures()

    End Sub

    Private Sub LoadMappedBulbs()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadMappedBulbs As New SqlCommand("BulbList", cnn)
        cmdLoadMappedBulbs.CommandType = CommandType.StoredProcedure

        Dim prmPartNumber As SqlParameter = cmdLoadMappedBulbs.Parameters.Add("@PartNumber", SqlDbType.Char, 255)
        prmPartNumber.Value = cboFixtures.SelectedValue

        Dim daBulbs As New SqlDataAdapter(cmdLoadMappedBulbs)


        Dim dsBulbs As New DataSet

        daBulbs.Fill(dsBulbs, "Bulbs")

        'Dim dtBulbs As DataTable = dsBulbs.Tables("Bulbs")
        'Dim dr As DataRow = dtBulbs.NewRow
        ''Initialize DataSet First Row
        'dr("Bulb") = " - Please Select An Item"
        'dr("ProductID") = "---"
        'dtBulbs.Rows.InsertAt(dr, 0)

        lbxMappedBulbs.DataSource = dsBulbs.Tables("Bulbs")
        lbxMappedBulbs.DisplayMember = "Bulb"
        lbxMappedBulbs.ValueMember = "ProductID"
        blnIsLoadingM = False
    End Sub

    Private Sub cboFixtures_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFixtures.SelectedIndexChanged
        If blnIsLoadingM = True Then
            Exit Sub
        End If
        LoadMappedBulbs()
    End Sub

    Private Sub txtQuan_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQuan.TextChanged

    End Sub

    Private Sub txtQuan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuan.KeyPress
        If Not (Char.IsDigit(e.KeyChar)) Then
            e.Handled = True
        End If

    End Sub

    Private Sub SaveMapping()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSQL As String
        strSQL = "INSERT INTO Bulb2Fixture (PartNumber, BulbNum, Quan)"
        strSQL = strSQL & " VALUES ('"
        strSQL = strSQL & cboFixtures.SelectedValue.ToString & "' , '"
        strSQL = strSQL & cboBulbs.SelectedValue.ToString & "' , "
        strSQL = strSQL & txtQuan.Text & ")"
        Dim cmdAddMapping As New SqlCommand(strSQL, cnn)
        cmdAddMapping.CommandType = CommandType.Text
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddMapping.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try

    End Sub

    Private Sub DeleteMapping()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSQL As String
        strSQL = "DELETE Bulb2Fixture WHERE PartNumber = '"
        strSQL = strSQL & cboFixtures.SelectedValue.ToString & "' AND BulbNum = '"
        strSQL = strSQL & lbxMappedBulbs.SelectedValue.ToString & "'"

        Dim cmdDeleteMapping As New SqlCommand(strSQL, cnn)
        cmdDeleteMapping.CommandType = CommandType.Text
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdDeleteMapping.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error deleting this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If txtQuan.Text = "" Then
            MsgBox("You Must Enter A Quantity")
            Exit Sub
        End If
        SaveMapping()
        txtQuan.Text = ""
        cboBulbs.SelectedIndex = 0
        blnIsLoadingM = True
        LoadMappedBulbs()

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim strGo As String
        If cboFixtures.SelectedValue.ToString = "" Then
            MsgBox("You must select a fixture!")
            Exit Sub
        End If

        If lbxMappedBulbs.SelectedValue.ToString = Nothing Then
            MsgBox("You must be a bulb mapped to the fixture!")
            Exit Sub
        End If

        strGo = MsgBox("Are you sure you want to delete this entry?", MsgBoxStyle.YesNo, "Delete Bulb To Fixture").ToString
        If strGo = "6" Then
            DeleteMapping()
            blnIsLoadingM = True
            LoadMappedBulbs()
            btnDelete.Visible = False
        End If
        '
    End Sub

    Private Sub lbxMappedBulbs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxMappedBulbs.SelectedIndexChanged

    End Sub

    Private Sub lbxMappedBulbs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbxMappedBulbs.Click
        btnDelete.Visible = True
    End Sub

    Private Sub cboFixtures_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFixtures.Enter
        cboFixtures.DroppedDown = True
    End Sub

    Private Sub cboBulbs_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBulbs.Enter
        cboBulbs.DroppedDown = True
    End Sub
End Class
