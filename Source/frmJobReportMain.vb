Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms

Public Class frmJobReportMain
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim strUnitsTotal As String
    Dim strCommonTotal As String
    Dim strSiteTotal As String
    Dim strClubhouseTotal As String
    Dim strAuxiliary As String
    Dim strAuxLabel As String
    Dim strPreTaxTotal As String
    Dim strTaxRate As String
    Dim strTaxTotal As String
    Dim strGrandTotal As String
    Dim strrptUnitCount As String
    Dim strrptBldgCount As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents cbxChangeOrders As System.Windows.Forms.CheckBox
    Friend WithEvents cbxShip As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBUTypes As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUnitsP As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUnitS As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCommonS As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClubHouseS As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAuxS As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTypeSummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTypeSummaryNoType As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSiteS As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUnitD As System.Windows.Forms.CheckBox
    Friend WithEvents cbxClubHouseD As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSiteD As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAuxD As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCommonD As System.Windows.Forms.CheckBox
    Friend WithEvents cbxBldgBOM As System.Windows.Forms.CheckBox
    Friend WithEvents cbxJobSummary As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTBD As System.Windows.Forms.CheckBox
    Friend WithEvents cbxComments As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbxBidComments As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.btnLoad = New System.Windows.Forms.Button
        Me.cbxComments = New System.Windows.Forms.CheckBox
        Me.cbxChangeOrders = New System.Windows.Forms.CheckBox
        Me.cbxShip = New System.Windows.Forms.CheckBox
        Me.cbxBUTypes = New System.Windows.Forms.CheckBox
        Me.cbxUnitsP = New System.Windows.Forms.CheckBox
        Me.cbxUnitS = New System.Windows.Forms.CheckBox
        Me.cbxCommonS = New System.Windows.Forms.CheckBox
        Me.cbxClubHouseS = New System.Windows.Forms.CheckBox
        Me.cbxAuxS = New System.Windows.Forms.CheckBox
        Me.cbxSiteS = New System.Windows.Forms.CheckBox
        Me.cbxUnitD = New System.Windows.Forms.CheckBox
        Me.cbxClubHouseD = New System.Windows.Forms.CheckBox
        Me.cbxSiteD = New System.Windows.Forms.CheckBox
        Me.cbxAuxD = New System.Windows.Forms.CheckBox
        Me.cbxCommonD = New System.Windows.Forms.CheckBox
        Me.cbxBldgBOM = New System.Windows.Forms.CheckBox
        Me.cbxJobSummary = New System.Windows.Forms.CheckBox
        Me.cbxTBD = New System.Windows.Forms.CheckBox
        Me.cbxTypeSummary = New System.Windows.Forms.CheckBox
        Me.cbxTypeSummaryNoType = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cbxBidComments = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(408, 52)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(76, 36)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.Visible = False
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(408, 288)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.TabIndex = 1
        Me.btnLoad.Text = "Show Report"
        '
        'cbxComments
        '
        Me.cbxComments.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxComments.Location = New System.Drawing.Point(20, 64)
        Me.cbxComments.Name = "cbxComments"
        Me.cbxComments.Size = New System.Drawing.Size(136, 16)
        Me.cbxComments.TabIndex = 2
        Me.cbxComments.Text = "Comments"
        '
        'cbxChangeOrders
        '
        Me.cbxChangeOrders.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxChangeOrders.Location = New System.Drawing.Point(20, 96)
        Me.cbxChangeOrders.Name = "cbxChangeOrders"
        Me.cbxChangeOrders.Size = New System.Drawing.Size(136, 16)
        Me.cbxChangeOrders.TabIndex = 3
        Me.cbxChangeOrders.Text = "Change Orders"
        '
        'cbxShip
        '
        Me.cbxShip.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxShip.Location = New System.Drawing.Point(20, 112)
        Me.cbxShip.Name = "cbxShip"
        Me.cbxShip.Size = New System.Drawing.Size(136, 16)
        Me.cbxShip.TabIndex = 4
        Me.cbxShip.Text = "Shipping Schedule"
        '
        'cbxBUTypes
        '
        Me.cbxBUTypes.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxBUTypes.Location = New System.Drawing.Point(20, 128)
        Me.cbxBUTypes.Name = "cbxBUTypes"
        Me.cbxBUTypes.Size = New System.Drawing.Size(136, 16)
        Me.cbxBUTypes.TabIndex = 5
        Me.cbxBUTypes.Text = "Bldg / Unit Types"
        '
        'cbxUnitsP
        '
        Me.cbxUnitsP.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUnitsP.Location = New System.Drawing.Point(20, 176)
        Me.cbxUnitsP.Name = "cbxUnitsP"
        Me.cbxUnitsP.Size = New System.Drawing.Size(136, 16)
        Me.cbxUnitsP.TabIndex = 6
        Me.cbxUnitsP.Text = "Units Per Bldg"
        '
        'cbxUnitS
        '
        Me.cbxUnitS.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUnitS.Location = New System.Drawing.Point(20, 224)
        Me.cbxUnitS.Name = "cbxUnitS"
        Me.cbxUnitS.Size = New System.Drawing.Size(136, 16)
        Me.cbxUnitS.TabIndex = 7
        Me.cbxUnitS.Text = "Unit Summary"
        '
        'cbxCommonS
        '
        Me.cbxCommonS.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCommonS.Location = New System.Drawing.Point(20, 240)
        Me.cbxCommonS.Name = "cbxCommonS"
        Me.cbxCommonS.Size = New System.Drawing.Size(136, 16)
        Me.cbxCommonS.TabIndex = 8
        Me.cbxCommonS.Text = "Common Summary"
        '
        'cbxClubHouseS
        '
        Me.cbxClubHouseS.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClubHouseS.Location = New System.Drawing.Point(20, 256)
        Me.cbxClubHouseS.Name = "cbxClubHouseS"
        Me.cbxClubHouseS.Size = New System.Drawing.Size(136, 16)
        Me.cbxClubHouseS.TabIndex = 9
        Me.cbxClubHouseS.Text = "Club House Summary"
        '
        'cbxAuxS
        '
        Me.cbxAuxS.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAuxS.Location = New System.Drawing.Point(20, 272)
        Me.cbxAuxS.Name = "cbxAuxS"
        Me.cbxAuxS.Size = New System.Drawing.Size(136, 16)
        Me.cbxAuxS.TabIndex = 10
        Me.cbxAuxS.Text = "Aux Summary"
        '
        'cbxSiteS
        '
        Me.cbxSiteS.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSiteS.Location = New System.Drawing.Point(20, 288)
        Me.cbxSiteS.Name = "cbxSiteS"
        Me.cbxSiteS.Size = New System.Drawing.Size(136, 16)
        Me.cbxSiteS.TabIndex = 11
        Me.cbxSiteS.Text = "Site Summary"
        '
        'cbxUnitD
        '
        Me.cbxUnitD.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUnitD.Location = New System.Drawing.Point(196, 64)
        Me.cbxUnitD.Name = "cbxUnitD"
        Me.cbxUnitD.Size = New System.Drawing.Size(136, 16)
        Me.cbxUnitD.TabIndex = 12
        Me.cbxUnitD.Text = "Unit Detail"
        '
        'cbxClubHouseD
        '
        Me.cbxClubHouseD.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClubHouseD.Location = New System.Drawing.Point(196, 96)
        Me.cbxClubHouseD.Name = "cbxClubHouseD"
        Me.cbxClubHouseD.Size = New System.Drawing.Size(136, 16)
        Me.cbxClubHouseD.TabIndex = 13
        Me.cbxClubHouseD.Text = "Club House Detail"
        '
        'cbxSiteD
        '
        Me.cbxSiteD.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSiteD.Location = New System.Drawing.Point(196, 128)
        Me.cbxSiteD.Name = "cbxSiteD"
        Me.cbxSiteD.Size = New System.Drawing.Size(136, 16)
        Me.cbxSiteD.TabIndex = 14
        Me.cbxSiteD.Text = "Site Detail"
        '
        'cbxAuxD
        '
        Me.cbxAuxD.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAuxD.Location = New System.Drawing.Point(196, 112)
        Me.cbxAuxD.Name = "cbxAuxD"
        Me.cbxAuxD.Size = New System.Drawing.Size(136, 16)
        Me.cbxAuxD.TabIndex = 15
        Me.cbxAuxD.Text = "Aux Detail"
        '
        'cbxCommonD
        '
        Me.cbxCommonD.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCommonD.Location = New System.Drawing.Point(196, 80)
        Me.cbxCommonD.Name = "cbxCommonD"
        Me.cbxCommonD.Size = New System.Drawing.Size(136, 16)
        Me.cbxCommonD.TabIndex = 16
        Me.cbxCommonD.Text = "Common Detail"
        '
        'cbxBldgBOM
        '
        Me.cbxBldgBOM.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxBldgBOM.Location = New System.Drawing.Point(196, 224)
        Me.cbxBldgBOM.Name = "cbxBldgBOM"
        Me.cbxBldgBOM.Size = New System.Drawing.Size(136, 12)
        Me.cbxBldgBOM.TabIndex = 17
        Me.cbxBldgBOM.Text = "Bldgs BOM"
        '
        'cbxJobSummary
        '
        Me.cbxJobSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxJobSummary.Location = New System.Drawing.Point(196, 236)
        Me.cbxJobSummary.Name = "cbxJobSummary"
        Me.cbxJobSummary.Size = New System.Drawing.Size(136, 12)
        Me.cbxJobSummary.TabIndex = 18
        Me.cbxJobSummary.Text = "Job Summary"
        '
        'cbxTBD
        '
        Me.cbxTBD.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTBD.Location = New System.Drawing.Point(196, 248)
        Me.cbxTBD.Name = "cbxTBD"
        Me.cbxTBD.Size = New System.Drawing.Size(136, 12)
        Me.cbxTBD.TabIndex = 19
        Me.cbxTBD.Text = "TBD"
        '
        'cbxTypeSummary
        '
        Me.cbxTypeSummary.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTypeSummary.Location = New System.Drawing.Point(20, 144)
        Me.cbxTypeSummary.Name = "cbxTypeSummary"
        Me.cbxTypeSummary.Size = New System.Drawing.Size(136, 16)
        Me.cbxTypeSummary.TabIndex = 20
        Me.cbxTypeSummary.Text = "Type Summary"
        '
        'cbxTypeSummaryNoType
        '
        Me.cbxTypeSummaryNoType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTypeSummaryNoType.Location = New System.Drawing.Point(20, 160)
        Me.cbxTypeSummaryNoType.Name = "cbxTypeSummaryNoType"
        Me.cbxTypeSummaryNoType.Size = New System.Drawing.Size(136, 16)
        Me.cbxTypeSummaryNoType.TabIndex = 21
        Me.cbxTypeSummaryNoType.Text = "Type Summary No Type"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(20, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(308, 12)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Select The Sections To Include"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(20, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 16)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Introduction"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(196, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 12)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Detail Sections"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(20, 204)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 12)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Summary Sections"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(196, 204)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 12)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Job Summary"
        '
        'cbxBidComments
        '
        Me.cbxBidComments.Location = New System.Drawing.Point(20, 80)
        Me.cbxBidComments.Name = "cbxBidComments"
        Me.cbxBidComments.Size = New System.Drawing.Size(104, 16)
        Me.cbxBidComments.TabIndex = 27
        Me.cbxBidComments.Text = "Bid Comments"
        '
        'frmJobReportMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 11)
        Me.ClientSize = New System.Drawing.Size(500, 326)
        Me.Controls.Add(Me.cbxBidComments)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxTypeSummaryNoType)
        Me.Controls.Add(Me.cbxTypeSummary)
        Me.Controls.Add(Me.cbxJobSummary)
        Me.Controls.Add(Me.cbxBldgBOM)
        Me.Controls.Add(Me.cbxSiteS)
        Me.Controls.Add(Me.cbxAuxS)
        Me.Controls.Add(Me.cbxClubHouseS)
        Me.Controls.Add(Me.cbxCommonS)
        Me.Controls.Add(Me.cbxUnitS)
        Me.Controls.Add(Me.cbxUnitsP)
        Me.Controls.Add(Me.cbxBUTypes)
        Me.Controls.Add(Me.cbxShip)
        Me.Controls.Add(Me.cbxChangeOrders)
        Me.Controls.Add(Me.cbxComments)
        Me.Controls.Add(Me.cbxTBD)
        Me.Controls.Add(Me.cbxCommonD)
        Me.Controls.Add(Me.cbxAuxD)
        Me.Controls.Add(Me.cbxSiteD)
        Me.Controls.Add(Me.cbxClubHouseD)
        Me.Controls.Add(Me.cbxUnitD)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.Name = "frmJobReportMain"
        Me.Text = "frmJobReportMain"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadReport()
        cnn.ConnectionString = frmMain.strConnect

        Dim curRpt As String

        curRpt = "r_main"
        Dim dsIntro As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "jr_main")

        curRpt = "rptComments"
        Dim dsComments As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "j_rptJobComments")

        curRpt = "rptComments"
        Dim dsBidComments As DataSet = CallRptSP(curRpt, cnn, frmJob.intOrigBidID.ToString, "b_rptBidComments")

        curRpt = "ChangeOrders"
        Dim dsCO As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "j_ChangeOrders")

        curRpt = "ShipDates"
        Dim dsShipDates As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "j_ShipDates")

        curRpt = "r_BldgTypes"
        Dim dsBldgTypes As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "jr_BldgTypes")

        curRpt = "UnitTypes"
        Dim dsUnitTypes As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "jr_UnitTypes")

        curRpt = "r_UnitsPerBldg"
        Dim dsUnitsPerBldgs As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "jr_UnitsPerBldg")

        curRpt = "TypeSummary"
        Dim dsTypeSummary As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "jr_TypeSummary")

        curRpt = "TypeSummaryNoType"
        Dim dsTypeSummaryNoType As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "jr_TypeSummaryNoType")

        Dim cmdRptSummary As New SqlCommand
        cmdRptSummary.Connection = cnn
        cmdRptSummary.CommandText = "UnitSummary"
        cmdRptSummary.CommandType = CommandType.StoredProcedure

        Dim rptProjectParameter As SqlParameter = cmdRptSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        rptProjectParameter.Value = frmMain.intJobID

        Dim rptUnitTypeParameter As SqlParameter
        rptUnitTypeParameter = cmdRptSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        rptUnitTypeParameter.Value = "U"

        Dim rptOrberByParameter As SqlParameter = cmdRptSummary.Parameters.Add("@OrderBy", SqlDbType.VarChar, 15)
        rptOrberByParameter.Value = "Location"

        Dim daUnitSummary As New SqlDataAdapter(cmdRptSummary)
        Dim dsUnit As New DataSet
        Try
            cnn.Open()
            daUnitSummary.Fill(dsUnit, "jr_UnitSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill jr_UnitSummary")
        Finally
            cnn.Close()
        End Try

        cmdRptSummary.Parameters.Clear()
        cmdRptSummary.Connection = cnn
        cmdRptSummary.CommandText = "UnitSummary"
        cmdRptSummary.CommandType = CommandType.StoredProcedure

        rptProjectParameter = cmdRptSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        rptProjectParameter.Value = frmMain.intJobID

        rptUnitTypeParameter = cmdRptSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        rptUnitTypeParameter.Value = "C"

        rptOrberByParameter = cmdRptSummary.Parameters.Add("@Orderby", SqlDbType.VarChar, 15)
        rptOrberByParameter.Value = "Location"

        daUnitSummary = New SqlDataAdapter(cmdRptSummary)
        Dim dsClubhouse As New DataSet
        Try
            cnn.Open()
            daUnitSummary.Fill(dsClubhouse, "jr_ClubhouseSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill jr_UnitSummary")
        Finally
            cnn.Close()
        End Try

        cmdRptSummary.Parameters.Clear()
        cmdRptSummary.Connection = cnn
        cmdRptSummary.CommandText = "CommonSummary"
        cmdRptSummary.CommandType = CommandType.StoredProcedure

        rptProjectParameter = cmdRptSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        rptProjectParameter.Value = frmMain.intJobID

        rptOrberByParameter = cmdRptSummary.Parameters.Add("@Order", SqlDbType.Char)
        rptOrberByParameter.Value = "L"
        daUnitSummary = New SqlDataAdapter(cmdRptSummary)
        Dim dsCommon As New DataSet
        Try
            cnn.Open()
            daUnitSummary.Fill(dsCommon, "jr_CommonSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill jr_CommonSummary")
        Finally
            cnn.Close()
        End Try

        cmdRptSummary.Parameters.Clear()
        cmdRptSummary.Connection = cnn
        cmdRptSummary.CommandText = "AuxillarySummary"
        cmdRptSummary.CommandType = CommandType.StoredProcedure

        rptProjectParameter = cmdRptSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        rptProjectParameter.Value = frmMain.intJobID

        rptOrberByParameter = cmdRptSummary.Parameters.Add("@Order", SqlDbType.Char, 2)
        rptOrberByParameter.Value = "L"
        daUnitSummary = New SqlDataAdapter(cmdRptSummary)
        Dim dsAux As New DataSet
        Try
            cnn.Open()
            daUnitSummary.Fill(dsAux, "jr_AltSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill jr_AltSummary")
        Finally
            cnn.Close()
        End Try

        cmdRptSummary.Parameters.Clear()
        cmdRptSummary.Connection = cnn
        cmdRptSummary.CommandText = "UnitSummary"
        cmdRptSummary.CommandType = CommandType.StoredProcedure

        rptProjectParameter = cmdRptSummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        rptProjectParameter.Value = frmMain.intJobID

        rptUnitTypeParameter = cmdRptSummary.Parameters.Add("@UnitType", SqlDbType.VarChar, 15)
        rptUnitTypeParameter.Value = "S"

        rptOrberByParameter = cmdRptSummary.Parameters.Add("@OrderBy", SqlDbType.VarChar, 15)
        rptOrberByParameter.Value = "Location"

        daUnitSummary = New SqlDataAdapter(cmdRptSummary)
        Dim dsSite As New DataSet
        Try
            cnn.Open()
            daUnitSummary.Fill(dsSite, "jr_SiteSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill jr_SiteSummary")
        Finally
            cnn.Close()
        End Try

        Dim dsUnitDetail As DataSet
        dsUnitDetail = JobReportingM.RunUnitTypeDetailSP("U", frmMain.intJobID.ToString, cnn, "jr_UnitDetail")
        Dim dsSiteDetail As DataSet
        dsSiteDetail = JobReportingM.RunUnitTypeDetailSP("S", frmMain.intJobID.ToString, cnn, "jr_SiteDetail")
        Dim dsClubHouseDetail As DataSet
        dsClubHouseDetail = JobReportingM.RunUnitTypeDetailSP("C", frmMain.intJobID.ToString, cnn, "jr_ClubHouseDetail")
        Dim dsAuxDetail As DataSet
        dsAuxDetail = JobReportingM.RunUnitTypeDetailSP("A", frmMain.intJobID.ToString, cnn, "jr_AuxDetail")
        Dim dsCommonDetail As DataSet
        dsCommonDetail = JobReportingM.RunUnitTypeDetailSP("X", frmMain.intJobID.ToString, cnn, "jr_CommonDetail")

        curRpt = "select Building.BldgName, Building.BldgNumber, Building.Type, Building.Description, Building.ShipdateR, " + _
         "Building.shipdate, LineItem.PartNumber, LineItem.RT, LineItem.Description as 'Desc', LineItem.Cost, LineItem.Sell, " + _
         "SUM(LineItem.Quantity) as 'Quan', LineItem.Location, LineItem.Type AS 'LI Type' from Building inner join LineItem " + _
         "on Building.BldgID = LineItem.BldgID where Building.ProjectID = " + frmMain.intJobID.ToString + "	and " + _
         "SUBSTRING(Building.Type, 1, 1) = 'B'group by Building.BldgName, Building.BldgNumber, Building.Type, " + _
         "Building.Description, Building.ShipdateR,	Building.shipdate, LineItem.PartNumber, LineItem.RT, LineItem.Description, " + _
         "LineItem.Cost, LineItem.Sell, LineItem.Location, LineItem.Type order by Building.BldgNumber"


        Dim dsBldgsBOM As New DataSet
        dsBldgsBOM = ExecuteSQL(curRpt, cnn, "jr_BldgBOM")


        curRpt = "rptJobSummary"
        Dim dsJobSummary As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "j_rptJobSummary")

        curRpt = "rptJobSummaryTBD"
        Dim dsJobSummaryTBD As DataSet = CallRptSP(curRpt, cnn, frmMain.intJobID.ToString, "j_rptJobSummaryTBD")

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        myReport = New jr_Main
        myReport.SetDataSource(dsIntro.Tables("jr_main"))
        myReport.OpenSubreport("jr_Comments.rpt").SetDataSource(dsComments)
        myReport.OpenSubreport("rptBidComments.rpt").SetDataSource(dsBidComments)
        myReport.OpenSubreport("jr_ChangeOrder.rpt").SetDataSource(dsCO)
        myReport.OpenSubreport("jr_ShipRT.rpt").SetDataSource(dsShipDates)
        myReport.OpenSubreport("jr_BldgTypes.rpt").SetDataSource(dsBldgTypes)
        myReport.OpenSubreport("jr_UnitTypes.rpt").SetDataSource(dsUnitTypes)
        myReport.OpenSubreport("jr_UnitsPerBldg.rpt").SetDataSource(dsUnitsPerBldgs)
        myReport.OpenSubreport("jr_TypeSummary.rpt").SetDataSource(dsTypeSummary)
        myReport.OpenSubreport("jr_TypeSummaryNoType.rpt").SetDataSource(dsTypeSummaryNoType)
        myReport.OpenSubreport("jrptUnitSummary.rpt").SetDataSource(dsUnit)
        myReport.OpenSubreport("jrptClubHouseSummary.rpt").SetDataSource(dsClubhouse)
        myReport.OpenSubreport("jrptCommonSummary.rpt").SetDataSource(dsCommon)
        myReport.OpenSubreport("jrptAuxSummary.rpt").SetDataSource(dsAux)
        myReport.OpenSubreport("jrptSiteSummary.rpt").SetDataSource(dsSite)
        myReport.OpenSubreport("jr_UnitDetail.rpt").SetDataSource(dsUnitDetail)
        myReport.OpenSubreport("jr_ClubHouseDetail.rpt").SetDataSource(dsClubHouseDetail)
        myReport.OpenSubreport("jr_SiteDetail.rpt").SetDataSource(dsSiteDetail)
        myReport.OpenSubreport("jr_AuxDetail.rpt").SetDataSource(dsAuxDetail)
        myReport.OpenSubreport("jr_CommonDetail.rpt").SetDataSource(dsCommonDetail)
        myReport.OpenSubreport("jr_BldgsBOM.rpt").SetDataSource(dsBldgsBOM)
        myReport.OpenSubreport("jr_JobSummary.rpt").SetDataSource(dsJobSummary)
        myReport.OpenSubreport("jr_JobSummaryTBD.rpt").SetDataSource(dsJobSummaryTBD)

        Dim SRAuxiliarylbl As CrystalDecisions.CrystalReports.Engine.ReportDocument
        SRAuxiliarylbl = myReport.OpenSubreport("jrptAuxSummary.rpt")
        Dim roAuxiliarylbl As CrystalDecisions.CrystalReports.Engine.ReportObject
        Dim foAuxiliarylbl As CrystalDecisions.CrystalReports.Engine.TextObject
        roAuxiliarylbl = SRAuxiliarylbl.ReportDefinition.ReportObjects.Item("txtAuxTitle")
        foAuxiliarylbl = CType(roAuxiliarylbl, CrystalDecisions.CrystalReports.Engine.TextObject)
        foAuxiliarylbl.Text = Trim(strAuxLabel) & " Summary"

        Dim SRAuxDetailLbl As CrystalDecisions.CrystalReports.Engine.ReportDocument
        SRAuxDetailLbl = myReport.OpenSubreport("jr_AuxDetail.rpt")
        Dim roAuxDetailLbl As CrystalDecisions.CrystalReports.Engine.ReportObject
        Dim foAuxDetailLbl As CrystalDecisions.CrystalReports.Engine.TextObject
        roAuxDetailLbl = SRAuxDetailLbl.ReportDefinition.ReportObjects.Item("Text12")
        foAuxDetailLbl = CType(roAuxDetailLbl, CrystalDecisions.CrystalReports.Engine.TextObject)
        foAuxDetailLbl.Text = Trim(strAuxLabel) & " Detail"

        Dim UnitsText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitsText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnits"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitsText.Text = strUnitsTotal

        Dim CommonText As CrystalDecisions.CrystalReports.Engine.TextObject
        CommonText = CType(myReport.ReportDefinition.ReportObjects.Item("txtCommon"), CrystalDecisions.CrystalReports.Engine.TextObject)
        CommonText.Text = strCommonTotal

        Dim SiteText As CrystalDecisions.CrystalReports.Engine.TextObject
        SiteText = CType(myReport.ReportDefinition.ReportObjects.Item("txtSite"), CrystalDecisions.CrystalReports.Engine.TextObject)
        SiteText.Text = strSiteTotal

        Dim ClubhouseText As CrystalDecisions.CrystalReports.Engine.TextObject
        ClubhouseText = CType(myReport.ReportDefinition.ReportObjects.Item("txtClubhouse"), CrystalDecisions.CrystalReports.Engine.TextObject)
        ClubhouseText.Text = strClubhouseTotal

        Dim AuxiliaryText As CrystalDecisions.CrystalReports.Engine.TextObject
        AuxiliaryText = CType(myReport.ReportDefinition.ReportObjects.Item("txtAuxiliary"), CrystalDecisions.CrystalReports.Engine.TextObject)
        AuxiliaryText.Text = strAuxiliary

        Dim Auxiliarylbl As CrystalDecisions.CrystalReports.Engine.TextObject
        Auxiliarylbl = CType(myReport.ReportDefinition.ReportObjects.Item("lblAuxiliary"), CrystalDecisions.CrystalReports.Engine.TextObject)
        Auxiliarylbl.Text = strAuxLabel

        Dim PreTaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        PreTaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtPreTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        PreTaxTotalText.Text = strPreTaxTotal

        Dim TaxRateText As CrystalDecisions.CrystalReports.Engine.TextObject
        TaxRateText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxRate"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TaxRateText.Text = FormatPercent(CDbl(strTaxRate), 2)

        Dim TaxTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        TaxTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtTaxTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        TaxTotalText.Text = strTaxTotal

        Dim GrandTotalText As CrystalDecisions.CrystalReports.Engine.TextObject
        GrandTotalText = CType(myReport.ReportDefinition.ReportObjects.Item("txtGrandTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
        GrandTotalText.Text = strGrandTotal
        'End If


        Dim UnitCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        UnitCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtUnitCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        UnitCountText.Text = strrptUnitCount

        Dim BldgCountText As CrystalDecisions.CrystalReports.Engine.TextObject
        BldgCountText = CType(myReport.ReportDefinition.ReportObjects.Item("txtBldgCount"), CrystalDecisions.CrystalReports.Engine.TextObject)
        BldgCountText.Text = strrptBldgCount

        If cbxComments.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section6")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxBidComments.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section26")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxChangeOrders.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section14")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxShip.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section12")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxBUTypes.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section15")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxTypeSummary.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section16")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxTypeSummaryNoType.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section17")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxUnitS.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section7")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxCommonS.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section9")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxClubHouseS.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section8")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxAuxS.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section13")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxSiteS.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section10")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxUnitD.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section18")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxClubHouseD.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section19")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxSiteD.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section20")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxAuxD.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section21")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxCommonD.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section22")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxBldgBOM.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section23")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxComments.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section6")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxJobSummary.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section11")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxTBD.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section24")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        If cbxUnitsP.Checked = False Then
            Dim MySectionTypeDef As CrystalDecisions.CrystalReports.Engine.Section
            MySectionTypeDef = myReport.ReportDefinition.Sections.Item("Section25")
            MySectionTypeDef.SectionFormat.EnableSuppress = True
        End If

        CrystalReportViewer1.ReportSource = myReport
    End Sub

    Private Sub LoadCounts()
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdUnitCount As New SqlCommand("rptUnitCount", cnn)
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmJobID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID
        Dim drUnitCount As SqlDataReader
        cnn.Open()
        drUnitCount = cmdUnitCount.ExecuteReader
        drUnitCount.Read()
        strrptUnitCount = drUnitCount.GetValue(0).ToString
        drUnitCount.Close()
        cnn.Close()

        Dim cmdBldgCount As New SqlCommand("rptBldgCount", cnn)
        cmdBldgCount.CommandType = CommandType.StoredProcedure
        Dim prmBldgJobID As SqlParameter = cmdBldgCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBldgJobID.Value = frmMain.intJobID
        Dim drBldgCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drBldgCount = cmdBldgCount.ExecuteReader
            drBldgCount.Read()
            strrptBldgCount = drBldgCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Bldg Count")
        Finally
            drBldgCount.Close()
            cnn.Close()
        End Try

        Dim strJobInfo As String
        strJobInfo = "SELECT AuxLabel FROM Project WHERE ProjectID = "
        strJobInfo = strJobInfo & frmMain.intJobID
        Dim cmdClientID As New SqlCommand(strJobInfo, cnn)
        cmdClientID.CommandType = CommandType.Text
        Dim drJob As SqlDataReader = Nothing
        Try
            cnn.Open()
            drJob = cmdClientID.ExecuteReader
            drJob.Read()

            If drJob.Item("AuxLabel").ToString = "" Then
                strAuxLabel = "Auxiliary"
            Else
                strAuxLabel = drJob.Item("AuxLabel").ToString
            End If
        Catch ex As Exception
            MsgBox("Error Loading Job Info")
        Finally
            drJob.Close()
            cnn.Close()
        End Try

    End Sub


    Private Sub LoadSubTotals()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdMargins As New SqlCommand("Margins", cnn)
        cmdMargins.CommandType = CommandType.StoredProcedure
        Dim prmJobID As SqlParameter = cmdMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmJobID.Value = frmMain.intJobID
        Dim drMargins As SqlDataReader
        cnn.Open()
        drMargins = cmdMargins.ExecuteReader

        'if these are not initialized an attempt to convert them into a number
        'may result in an exeception
        strUnitsTotal = "0"
        strClubhouseTotal = "0"
        strSiteTotal = "0"
        strCommonTotal = "0"
        strAuxiliary = "0"

        While drMargins.Read
            If drMargins.GetString(0) = "U" Then
                strUnitsTotal = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "C" Then
                strClubhouseTotal = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "S" Then
                strSiteTotal = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "X" Then
                strCommonTotal = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            ElseIf drMargins.GetString(0) = "A" Then
                strAuxiliary = CStr(FormatCurrency(drMargins.GetDecimal(2)))
            End If

        End While
        drMargins.Close()
        cnn.Close()

        strPreTaxTotal = Format(CDbl(strUnitsTotal) + CDbl(strSiteTotal) + CDbl(strClubhouseTotal) + CDbl(strCommonTotal) + CDbl(strAuxiliary), "C")

        Dim cmdSalesTaxRate As New SqlCommand("rptSalesTaxRate", cnn)
        cmdSalesTaxRate.CommandType = CommandType.StoredProcedure
        Dim prmSalesTaxJobID As SqlParameter = cmdSalesTaxRate.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmSalesTaxJobID.Value = frmMain.intJobID
        Dim drSalesTaxRate As SqlDataReader = Nothing
        cnn.Open()
        Try
            drSalesTaxRate = cmdSalesTaxRate.ExecuteReader
            drSalesTaxRate.Read()
            strTaxRate = drSalesTaxRate.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drSalesTaxRate.Close()
            cnn.Close()
        End Try

        strTaxTotal = Format(CDbl(strPreTaxTotal) * CDbl(strTaxRate), "C")
        strGrandTotal = Format(CDbl(strPreTaxTotal) + CDbl(strTaxTotal), "C")
    End Sub

    Private Sub HideAll()
        cbxComments.Visible = False
        cbxBidComments.Visible = False
        cbxChangeOrders.Visible = False
        cbxShip.Visible = False
        cbxBUTypes.Visible = False
        cbxTypeSummary.Visible = False
        cbxTypeSummaryNoType.Visible = False
        cbxUnitsP.Visible = False
        cbxUnitS.Visible = False
        cbxCommonS.Visible = False
        cbxClubHouseS.Visible = False
        cbxAuxS.Visible = False
        cbxSiteS.Visible = False
        cbxUnitD.Visible = False
        cbxClubHouseD.Visible = False
        cbxSiteD.Visible = False
        cbxAuxD.Visible = False
        cbxCommonD.Visible = False
        cbxBldgBOM.Visible = False
        cbxJobSummary.Visible = False
        cbxTBD.Visible = False

        Label1.Visible = False
        Label2.Visible = False
        Label3.Visible = False
        Label4.Visible = False
        Label5.Visible = False

        btnLoad.Visible = False
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        Me.Cursor = Cursors.WaitCursor
        LoadSubTotals()
        LoadCounts()
        LoadReport()
        HideAll()
        CrystalReportViewer1.Dock = DockStyle.Fill
        Me.Cursor = Cursors.Default
        CrystalReportViewer1.Visible = True
    End Sub

End Class
