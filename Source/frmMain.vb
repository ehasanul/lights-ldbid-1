Imports System.Data
Imports System.Data.SqlClient
Public Class frmMain
    Inherits System.Windows.Forms.Form

    Public Shared strConnect As String
    Public Shared intBidID As Integer
    Public Shared intUnitID As Integer
    Public Shared UnitType As String
    Public Shared intClientID As Integer
    Public Shared blnAdding As Boolean
    Public Shared strBlbPartNumber As String
    Public Shared intBlbQuan As Integer
    Public Shared strBlbLocation As String
    Public Shared strBlbRT As String
    Public Shared strBlbType As String
    Public Shared blnBidRunning As Boolean
    Public Shared comingFromVerbal As Boolean
    Public Shared comingFromBid As Boolean
    Public Shared ConvertFrom As String
    Public Shared ConvertTo As String
    Friend WithEvents mnuVerbal As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditVerbal As System.Windows.Forms.MenuItem
    Friend WithEvents mnuConvertBidtoVerbal As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalReports As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalShippingByDate As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalListing As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalPartsByDateRange As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalPartsByDateWithJobNum As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalPartsByDateWithInv As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalPartsStatistics As System.Windows.Forms.MenuItem
    Friend WithEvents mnuVerbalPartsSearch As System.Windows.Forms.MenuItem

    'Job Module Variables
    Public Shared intJobID As Integer


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuUtilities As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMainCatalog As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSOCatalog As System.Windows.Forms.MenuItem
    Friend WithEvents mnuClientsMaint As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMain As System.Windows.Forms.MainMenu
    Friend WithEvents mnuBids As System.Windows.Forms.MenuItem
    Friend WithEvents mnuJobs As System.Windows.Forms.MenuItem
    Friend WithEvents mnuProjConvert As System.Windows.Forms.MenuItem
    Friend WithEvents mnuProEdit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuBlb2Fixture As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopyDeleteBid As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchParts As System.Windows.Forms.MenuItem
    Friend WithEvents mnuJobReports As System.Windows.Forms.MenuItem
    Friend WithEvents mnuJobListing As System.Windows.Forms.MenuItem
    Friend WithEvents mnuJobPartsByDates As System.Windows.Forms.MenuItem
    Friend WithEvents mnujPartsSearch As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPartsByDateJN As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPartsMaxMin As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPartsByDateINV As System.Windows.Forms.MenuItem
    Friend WithEvents mnuBldgShipByDate As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewBid As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEditBid As System.Windows.Forms.MenuItem

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.mnuMain = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuBids = New System.Windows.Forms.MenuItem()
        Me.mnuNewBid = New System.Windows.Forms.MenuItem()
        Me.mnuEditBid = New System.Windows.Forms.MenuItem()
        Me.mnuCopyDeleteBid = New System.Windows.Forms.MenuItem()
        Me.mnuSearchParts = New System.Windows.Forms.MenuItem()
        Me.mnuExit = New System.Windows.Forms.MenuItem()
        Me.mnuVerbal = New System.Windows.Forms.MenuItem()
        Me.mnuEditVerbal = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalReports = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalListing = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalShippingByDate = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalPartsByDateRange = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalPartsByDateWithJobNum = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalPartsByDateWithInv = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalPartsStatistics = New System.Windows.Forms.MenuItem()
        Me.mnuVerbalPartsSearch = New System.Windows.Forms.MenuItem()
        Me.mnuConvertBidtoVerbal = New System.Windows.Forms.MenuItem()
        Me.mnuJobs = New System.Windows.Forms.MenuItem()
        Me.mnuProEdit = New System.Windows.Forms.MenuItem()
        Me.mnuJobReports = New System.Windows.Forms.MenuItem()
        Me.mnuJobListing = New System.Windows.Forms.MenuItem()
        Me.mnuBldgShipByDate = New System.Windows.Forms.MenuItem()
        Me.mnuJobPartsByDates = New System.Windows.Forms.MenuItem()
        Me.mnuPartsByDateJN = New System.Windows.Forms.MenuItem()
        Me.mnuPartsByDateINV = New System.Windows.Forms.MenuItem()
        Me.mnuPartsMaxMin = New System.Windows.Forms.MenuItem()
        Me.mnujPartsSearch = New System.Windows.Forms.MenuItem()
        Me.mnuProjConvert = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuUtilities = New System.Windows.Forms.MenuItem()
        Me.mnuMainCatalog = New System.Windows.Forms.MenuItem()
        Me.mnuSOCatalog = New System.Windows.Forms.MenuItem()
        Me.mnuClientsMaint = New System.Windows.Forms.MenuItem()
        Me.mnuBlb2Fixture = New System.Windows.Forms.MenuItem()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuBids, Me.mnuVerbal, Me.mnuJobs, Me.mnuUtilities})
        '
        'mnuBids
        '
        Me.mnuBids.Index = 0
        Me.mnuBids.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewBid, Me.mnuEditBid, Me.mnuCopyDeleteBid, Me.mnuSearchParts, Me.mnuExit})
        Me.mnuBids.Text = "Bids"
        '
        'mnuNewBid
        '
        Me.mnuNewBid.Index = 0
        Me.mnuNewBid.Text = "New Bid"
        '
        'mnuEditBid
        '
        Me.mnuEditBid.Index = 1
        Me.mnuEditBid.Text = "Edit Bid"
        '
        'mnuCopyDeleteBid
        '
        Me.mnuCopyDeleteBid.Index = 2
        Me.mnuCopyDeleteBid.Text = "Copy / Delete"
        '
        'mnuSearchParts
        '
        Me.mnuSearchParts.Index = 3
        Me.mnuSearchParts.Text = "Parts Search"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 4
        Me.mnuExit.Text = "Exit"
        '
        'mnuVerbal
        '
        Me.mnuVerbal.Index = 1
        Me.mnuVerbal.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEditVerbal, Me.mnuVerbalReports, Me.mnuVerbalPartsSearch, Me.mnuConvertBidtoVerbal})
        Me.mnuVerbal.Text = "Verbal"
        '
        'mnuEditVerbal
        '
        Me.mnuEditVerbal.Index = 0
        Me.mnuEditVerbal.Text = "View/Edit Verbals"
        '
        'mnuVerbalReports
        '
        Me.mnuVerbalReports.Index = 1
        Me.mnuVerbalReports.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuVerbalListing, Me.mnuVerbalShippingByDate, Me.mnuVerbalPartsByDateRange, Me.mnuVerbalPartsByDateWithJobNum, Me.mnuVerbalPartsByDateWithInv, Me.mnuVerbalPartsStatistics})
        Me.mnuVerbalReports.Text = "Reports"
        '
        'mnuVerbalListing
        '
        Me.mnuVerbalListing.Index = 0
        Me.mnuVerbalListing.Text = "Verbal Listing"
        '
        'mnuVerbalShippingByDate
        '
        Me.mnuVerbalShippingByDate.Index = 1
        Me.mnuVerbalShippingByDate.Text = "Shipping By Date"
        '
        'mnuVerbalPartsByDateRange
        '
        Me.mnuVerbalPartsByDateRange.Index = 2
        Me.mnuVerbalPartsByDateRange.Text = "Parts By Date Range"
        '
        'mnuVerbalPartsByDateWithJobNum
        '
        Me.mnuVerbalPartsByDateWithJobNum.Index = 3
        Me.mnuVerbalPartsByDateWithJobNum.Text = "Parts By Date Range / Job #"
        '
        'mnuVerbalPartsByDateWithInv
        '
        Me.mnuVerbalPartsByDateWithInv.Index = 4
        Me.mnuVerbalPartsByDateWithInv.Text = "Parts By Date Range / Inv"
        '
        'mnuVerbalPartsStatistics
        '
        Me.mnuVerbalPartsStatistics.Index = 5
        Me.mnuVerbalPartsStatistics.Text = "Parts Statistics"
        '
        'mnuVerbalPartsSearch
        '
        Me.mnuVerbalPartsSearch.Index = 2
        Me.mnuVerbalPartsSearch.Text = "Parts Search"
        '
        'mnuConvertBidtoVerbal
        '
        Me.mnuConvertBidtoVerbal.Index = 3
        Me.mnuConvertBidtoVerbal.Text = "Convert Bid to Verbal"
        '
        'mnuJobs
        '
        Me.mnuJobs.Index = 2
        Me.mnuJobs.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuProEdit, Me.mnuJobReports, Me.mnujPartsSearch, Me.mnuProjConvert, Me.MenuItem3})
        Me.mnuJobs.Text = "Jobs"
        '
        'mnuProEdit
        '
        Me.mnuProEdit.Index = 0
        Me.mnuProEdit.Text = "Job Details"
        '
        'mnuJobReports
        '
        Me.mnuJobReports.Index = 1
        Me.mnuJobReports.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuJobListing, Me.mnuBldgShipByDate, Me.mnuJobPartsByDates, Me.mnuPartsByDateJN, Me.mnuPartsByDateINV, Me.mnuPartsMaxMin})
        Me.mnuJobReports.Text = "Reports"
        '
        'mnuJobListing
        '
        Me.mnuJobListing.Index = 0
        Me.mnuJobListing.Text = "Job Listing"
        '
        'mnuBldgShipByDate
        '
        Me.mnuBldgShipByDate.Index = 1
        Me.mnuBldgShipByDate.Text = "Bldgs Shipping By Date Range"
        '
        'mnuJobPartsByDates
        '
        Me.mnuJobPartsByDates.Index = 2
        Me.mnuJobPartsByDates.Text = "Parts By Date Range"
        '
        'mnuPartsByDateJN
        '
        Me.mnuPartsByDateJN.Index = 3
        Me.mnuPartsByDateJN.Text = "Parts By Date Range / Job #"
        '
        'mnuPartsByDateINV
        '
        Me.mnuPartsByDateINV.Index = 4
        Me.mnuPartsByDateINV.Text = "Parts By Date Range / Inv"
        '
        'mnuPartsMaxMin
        '
        Me.mnuPartsMaxMin.Index = 5
        Me.mnuPartsMaxMin.Text = "Parts Statistics"
        '
        'mnujPartsSearch
        '
        Me.mnujPartsSearch.Index = 2
        Me.mnujPartsSearch.Text = "Parts Search"
        '
        'mnuProjConvert
        '
        Me.mnuProjConvert.Index = 3
        Me.mnuProjConvert.Text = "Convert Bid To Job"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 4
        Me.MenuItem3.Text = "Convert Verbal to Job"
        '
        'mnuUtilities
        '
        Me.mnuUtilities.Index = 3
        Me.mnuUtilities.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMainCatalog, Me.mnuSOCatalog, Me.mnuClientsMaint, Me.mnuBlb2Fixture})
        Me.mnuUtilities.Text = "Utilities"
        '
        'mnuMainCatalog
        '
        Me.mnuMainCatalog.Index = 0
        Me.mnuMainCatalog.Text = "Main Catalog"
        '
        'mnuSOCatalog
        '
        Me.mnuSOCatalog.Index = 1
        Me.mnuSOCatalog.Text = "Special Order Catalog"
        '
        'mnuClientsMaint
        '
        Me.mnuClientsMaint.Index = 2
        Me.mnuClientsMaint.Text = "Clients Maintenance"
        '
        'mnuBlb2Fixture
        '
        Me.mnuBlb2Fixture.Index = 3
        Me.mnuBlb2Fixture.Text = "Bulb To Fixture"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(584, 288)
        Me.MaximizeBox = False
        Me.Menu = Me.mnuMain
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lights Direct Bids and Projects"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'strConnect = "data source='LD-BID';user id=SA;password=Redacted;initial catalog=LD-BID"
        strConnect = My.Settings("ConnectionString").ToString
        blnBidRunning = False
    End Sub

    Private Sub CleanDatabase()
        Dim cnn3 As New SqlConnection
        cnn3.ConnectionString = frmMain.strConnect
        'DPS - I am trying to get rid of this Stored Procedure
        'Dim cmdDeleteOrphans As New SqlCommand("DeleteOrphans", cnn3)
        'cmdDeleteOrphans.CommandType = CommandType.StoredProcedure
        'cnn3.Open()
        'Try
        'cmdDeleteOrphans.ExecuteNonQuery()
        'Catch ex As Exception
        'MsgBox("Deletion Of Orphans Failed!!")
        'Finally
        'cnn3.Close()
        'End Try

        Dim strDupes As String
        strDupes = "SELECT ProjectID ,PartNumber, COUNT(PartNumber)AS Num FROM LineItem " & _
            "Group BY ProjectID, PartNumber HAVING(Count(PartNumber) > 1)"

        Dim cmdDupes As New SqlCommand(strDupes, cnn3)
        cmdDupes.CommandType = CommandType.Text

        Dim drDupes As SqlDataReader
        cnn3.Open()

        drDupes = cmdDupes.ExecuteReader

        While drDupes.Read()
            Dim strPartNum As String
            Dim intBidId As Integer
            Dim Go As Integer
            strPartNum = RTrim(drDupes.Item(1).ToString)
            intBidId = System.Convert.ToInt32(drDupes.Item(0))
            Go = MsgBox("Part Number: " & strPartNum & " From Bid:  " & intBidId.ToString & " Is Being Deleted!" & vbCrLf & "Do You Want To Continue?", MsgBoxStyle.YesNo)
            If Go = 6 Then
                Dim cnn2 As New SqlConnection
                cnn2.ConnectionString = frmMain.strConnect
                Dim strDeleteDupes As String
                strDeleteDupes = "DELETE From LineItem where ProjectID = " & intBidId & " AND PartNumber = '" & strPartNum & "'"
                Dim cmdDeleteDupes As New SqlCommand(strDeleteDupes, cnn2)
                cmdDeleteDupes.CommandType = CommandType.Text
                Try
                    cnn2.Open()
                    cmdDeleteDupes.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox("Error in Deleting Record")
                Finally
                    cnn2.Close()
                End Try
            End If

        End While
        If cnn3.State = ConnectionState.Open Then
            cnn3.Close()
        End If

    End Sub

    Private Sub mnuCatMain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMainCatalog.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim CatMain As New frmCatMain
        CatMain.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuCatSO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSOCatalog.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim CatSO As New frmCatSO
        CatSO.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuClients_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClientsMaint.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Clients As New frmEditClients
        Clients.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuBlb2Fixture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBlb2Fixture.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Blub2Fixture As New frmBulb2Fixture
        Blub2Fixture.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Me.Close()
    End Sub

    Private Sub mnuBidCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCopyDeleteBid.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim BidCopy As New frmCopyBid
        BidCopy.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuSearchParts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchParts.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim SearchParts As New frmProductSearch
        SearchParts.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuCleanDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CleanDatabase()
    End Sub

    Private Sub mnuProjConvert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProjConvert.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        ConvertFrom = "Bid"
        ConvertTo = "Job"
        Dim BidToJob As New frmBidToJob
        BidToJob.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuProEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProEdit.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim JobDetails As New frmJob
        JobDetails.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuJobListing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuJobListing.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim JobsReport As New frmVwJobsReport
        JobsReport.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuJobPartsByDates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuJobPartsByDates.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim PartsByDate As New frmVwJPartsByDates
        PartsByDate.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnujPartsSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnujPartsSearch.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim jPartsSearch As New frmj_PartsSearch
        jPartsSearch.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuPartsByDateJN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPartsByDateJN.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim PartsByDateJN As New frmVwJPartsByDateJN
        PartsByDateJN.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuPartsMaxMin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPartsMaxMin.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim PartsByDateJN As New frmVwJPartsHighLow
        PartsByDateJN.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuPartsByDateINV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPartsByDateINV.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = False
        Dim PartsByDateInv As New frmVwJPartsByDateInv
        PartsByDateInv.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuBldgShipByDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBldgShipByDate.Click
        frmMain.comingFromVerbal = False
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim PartsByDate As New frmVwJBldgShipByDate
        PartsByDate.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub NewBidClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewBid.Click
        Dim Bid As New frmBid
        Bid.LoadNewBidForm()
    End Sub

    Private Sub EditBidClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditBid.Click
        Dim Bid As New frmBid
        Bid.LoadEditBidForm()
    End Sub

    Private Sub ShowConvertBidToVerbal(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConvertBidtoVerbal.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        ConvertFrom = "Bid"
        ConvertTo = "Verbal"
        Dim BidToJob As New frmBidToJob
        BidToJob.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowEditVerbals(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditVerbal.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim JobDetails As New frmJob
        JobDetails.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowVerbalBldgShipByDate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalShippingByDate.Click
        frmMain.comingFromVerbal = True
        Dim PartsByDate As New frmVwJBldgShipByDate
        PartsByDate.ShowDialog()

    End Sub

    Private Sub ShowVerbalListingReport(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalListing.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim JobsReport As New frmVwJobsReport
        JobsReport.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowVerbalPartsByDateRangeReport(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalPartsByDateRange.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim PartsByDate As New frmVwJPartsByDates
        PartsByDate.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowDateJobNumReport(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalPartsByDateWithJobNum.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim PartsByDateJN As New frmVwJPartsByDateJN
        PartsByDateJN.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowPartsByDateWInv(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalPartsByDateWithInv.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim PartsByDateInv As New frmVwJPartsByDateInv
        PartsByDateInv.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowVerbalPartsStatistics(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalPartsStatistics.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim PartsByDateJN As New frmVwJPartsHighLow
        PartsByDateJN.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowVerbalPartsSearch(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVerbalPartsSearch.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        comingFromVerbal = True
        Dim jPartsSearch As New frmj_PartsSearch
        jPartsSearch.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ShowConvertVerbalToJob(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        ConvertFrom = "Verbal"
        ConvertTo = "Job"
        Dim BidToJob As New frmBidToJob
        BidToJob.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub
End Class
