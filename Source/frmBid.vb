Imports System.Data
Imports System.Data.SqlClient
Public Class frmBid
    Inherits System.Windows.Forms.Form

    Dim cnn As New SqlConnection ' Primary Connection
    Dim dsBids As New DataSet
    Dim blnStartBldg As Boolean 'Tests Startup Condition in LoadBldgs
    Dim intUnitEditing As Integer
    Dim intUnitDGRowCount As Integer
    Dim intUnitDGActiveRow As Integer
    Dim strUnitDGOrder As String
    Dim currentUnitCount As Integer
    Public Shared blnStartUnit As Boolean 'Tests Startup Condition in LoadUnits
    Public Shared blnUnitCopy As Boolean
    Public Shared blnUnitDelete As Boolean
    Public Shared strActiveUnit As String
    Public Shared blnBldgCopy As Boolean
    Public Shared strActiveBldg As String
    Public Shared blnAddSO As Boolean
    Public Shared strSOProductID As String
    Public Shared strSODesc As String
    Public Shared dblSOCost As String
    Public Shared intUnitID As Integer
    Public Shared blnUnitReturn As Boolean
    Public Shared strSPartNumber As String
    Public Shared strSType As String
    Friend WithEvents rwBldgID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwQuan As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwUnitType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents rwDescription As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents PartNumCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DescCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents LocCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents RTCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents TypeCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents CostCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents MarginCol As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgUnitLineItems As System.Windows.Forms.DataGrid
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents SellCol As System.Windows.Forms.DataGridTextBoxColumn
    Public Shared strSLocation As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tbClient As System.Windows.Forms.TabPage
    Friend WithEvents tbUnits As System.Windows.Forms.TabPage
    Friend WithEvents tbBuilding As System.Windows.Forms.TabPage
    Friend WithEvents tbSummary As System.Windows.Forms.TabPage
    Friend WithEvents tbAlternates As System.Windows.Forms.TabPage
    Friend WithEvents BidMainMenu As System.Windows.Forms.MainMenu
    Friend WithEvents btnNewUnit As System.Windows.Forms.Button
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents btnAddItem As System.Windows.Forms.Button
    Friend WithEvents cboClients As System.Windows.Forms.ComboBox
    Friend WithEvents txtCLName As System.Windows.Forms.TextBox
    Friend WithEvents txtCLContact As System.Windows.Forms.TextBox
    Friend WithEvents txtClAddr1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLAddr2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLCity As System.Windows.Forms.TextBox
    Friend WithEvents txtCLState As System.Windows.Forms.TextBox
    Friend WithEvents txtCLZip As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone1 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLFAX As System.Windows.Forms.TextBox
    Friend WithEvents txtCLPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCLemail As System.Windows.Forms.TextBox
    Friend WithEvents PhoneLabel As System.Windows.Forms.Label
    Friend WithEvents FaxLabel As System.Windows.Forms.Label
    Friend WithEvents EmailLabel As System.Windows.Forms.Label
    Friend WithEvents cboCatalogMain As System.Windows.Forms.ComboBox
    Friend WithEvents txtLISell As System.Windows.Forms.TextBox
    Friend WithEvents txtLIMargin As System.Windows.Forms.TextBox
    Friend WithEvents txtLICost As System.Windows.Forms.TextBox
    Friend WithEvents txtLIQuan As System.Windows.Forms.TextBox
    Friend WithEvents txtLIProductID As System.Windows.Forms.TextBox
    Friend WithEvents txtLIDesc As System.Windows.Forms.TextBox
    Friend WithEvents QuantityLabel As System.Windows.Forms.Label
    Friend WithEvents ProductIDLabel As System.Windows.Forms.Label
    Friend WithEvents UnitDescriptionLabel As System.Windows.Forms.Label
    Friend WithEvents CoLabel As System.Windows.Forms.Label
    Friend WithEvents MarginLabel As System.Windows.Forms.Label
    Friend WithEvents SellLabel As System.Windows.Forms.Label
    Friend WithEvents cboBid As System.Windows.Forms.ComboBox
    Friend WithEvents txtBLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtBCity As System.Windows.Forms.TextBox
    Friend WithEvents txtBState As System.Windows.Forms.TextBox
    Friend WithEvents txtBZip As System.Windows.Forms.TextBox
    Friend WithEvents txtBAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtBClientPM As System.Windows.Forms.TextBox
    Friend WithEvents ClientDataLabel As System.Windows.Forms.Label
    Friend WithEvents txtBLDPM As System.Windows.Forms.TextBox
    Friend WithEvents txtBNotes As System.Windows.Forms.TextBox
    Friend WithEvents NotesLabel As System.Windows.Forms.Label
    Friend WithEvents txtBUnitType As System.Windows.Forms.TextBox
    Friend WithEvents txtBUnitDescription As System.Windows.Forms.TextBox
    Friend WithEvents cboBldg As System.Windows.Forms.ComboBox
    Friend WithEvents btnAddBldg As System.Windows.Forms.Button
    Friend WithEvents txtbldgType As System.Windows.Forms.TextBox
    Friend WithEvents txtbldgDesc As System.Windows.Forms.TextBox
    Friend WithEvents dgBldg As System.Windows.Forms.DataGrid
    Friend WithEvents txtBldgShipDate As System.Windows.Forms.TextBox
    Friend WithEvents cboBldgUnits As System.Windows.Forms.ComboBox
    Friend WithEvents txtBldgUnitQuan As System.Windows.Forms.TextBox
    Friend WithEvents NumBuildingsLabel As System.Windows.Forms.Label
    Friend WithEvents dgBidUnitsperBldg As System.Windows.Forms.DataGrid
    Friend WithEvents dgUnitCnt As System.Windows.Forms.DataGrid
    Friend WithEvents dgBOM As System.Windows.Forms.DataGrid
    Friend WithEvents btnAddUnit As System.Windows.Forms.Button
    Friend WithEvents txtbDescription As System.Windows.Forms.TextBox
    Friend WithEvents DescriptionLabel As System.Windows.Forms.Label
    Friend WithEvents LocationLabel As System.Windows.Forms.Label
    Friend WithEvents AddressLabel As System.Windows.Forms.Label
    Friend WithEvents CityLabel As System.Windows.Forms.Label
    Friend WithEvents StateLabel As System.Windows.Forms.Label
    Friend WithEvents ZipLabel As System.Windows.Forms.Label
    Friend WithEvents ProjectLabel As System.Windows.Forms.Label
    Friend WithEvents btnSaveNewBid As System.Windows.Forms.Button
    Friend WithEvents TabCtrlBid As System.Windows.Forms.TabControl
    Friend WithEvents lblClientPM As System.Windows.Forms.Label
    Friend WithEvents lblLDPM As System.Windows.Forms.Label
    Friend WithEvents lblUnitType As System.Windows.Forms.Label
    Friend WithEvents lblUnitDesc As System.Windows.Forms.Label
    Friend WithEvents rbSO As System.Windows.Forms.RadioButton
    Friend WithEvents rbBid As System.Windows.Forms.RadioButton
    Friend WithEvents rbMain As System.Windows.Forms.RadioButton
    Friend WithEvents UnitLocation As System.Windows.Forms.Panel
    Friend WithEvents txtLIRT As System.Windows.Forms.TextBox
    Friend WithEvents lblRT As System.Windows.Forms.Label
    Friend WithEvents txtBid As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgNotes As System.Windows.Forms.TextBox
    Friend WithEvents lblBldgType As System.Windows.Forms.Label
    Friend WithEvents lblBldgLocation As System.Windows.Forms.Label
    Friend WithEvents lblBldgAddress As System.Windows.Forms.Label
    Friend WithEvents lblBldgNotes As System.Windows.Forms.Label
    Friend WithEvents lblBldgShipDate As System.Windows.Forms.Label
    Friend WithEvents lblBldgUnitQuan As System.Windows.Forms.Label
    Friend WithEvents lblBldgDesc As System.Windows.Forms.Label
    Friend WithEvents lblBldgSelUnit As System.Windows.Forms.Label
    Friend WithEvents lblBldgSelectBldg As System.Windows.Forms.Label
    Friend WithEvents btnAddUnitToBldg As System.Windows.Forms.Button
    Friend WithEvents btnAddSO As System.Windows.Forms.Button
    Friend WithEvents txtLILocation As System.Windows.Forms.TextBox
    Friend WithEvents UnitLocationLabel As System.Windows.Forms.Label
    Friend WithEvents tbLIBulkEdit As System.Windows.Forms.TabPage
    Friend WithEvents txtLIType As System.Windows.Forms.TextBox
    Friend WithEvents lblLIType As System.Windows.Forms.Label
    Friend WithEvents txtBldgNum As System.Windows.Forms.TextBox
    Friend WithEvents lblBldgNum As System.Windows.Forms.Label
    Friend WithEvents txtBldgShipSeq As System.Windows.Forms.TextBox
    Friend WithEvents lblBldgShipSeq As System.Windows.Forms.Label
    Friend WithEvents cboBEBidItems As System.Windows.Forms.ComboBox
    Friend WithEvents cboBECatalog As System.Windows.Forms.ComboBox
    Friend WithEvents rbBEMain As System.Windows.Forms.RadioButton
    Friend WithEvents rbBESO As System.Windows.Forms.RadioButton
    Friend WithEvents txtBEBProdID As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBRT As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBType As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBCost As System.Windows.Forms.TextBox
    Friend WithEvents txtBEBSell As System.Windows.Forms.TextBox
    Friend WithEvents BEProductIDLabel2 As System.Windows.Forms.Label
    Friend WithEvents BEDescription2 As System.Windows.Forms.Label
    Friend WithEvents RTLabel2 As System.Windows.Forms.Label
    Friend WithEvents BECostLabel2 As System.Windows.Forms.Label
    Friend WithEvents BESellLabel2 As System.Windows.Forms.Label
    Friend WithEvents txtBEBMargin As System.Windows.Forms.TextBox
    Friend WithEvents BEMarginLabel2 As System.Windows.Forms.Label
    Friend WithEvents cbxFSell As System.Windows.Forms.CheckBox
    Friend WithEvents CurrentBidLabel As System.Windows.Forms.Label
    Friend WithEvents cboBEType As System.Windows.Forms.ComboBox
    Friend WithEvents btnBEDelete As System.Windows.Forms.Button
    Friend WithEvents btnBERByPartNum As System.Windows.Forms.Button
    Friend WithEvents btnBERByType As System.Windows.Forms.Button
    Friend WithEvents TypesIncludedLabel As System.Windows.Forms.Label
    Friend WithEvents btnBEAdjustSell As System.Windows.Forms.Button
    Friend WithEvents txtBERProdID As System.Windows.Forms.TextBox
    Friend WithEvents txtBERDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtBERRT As System.Windows.Forms.TextBox
    Friend WithEvents txtBERType As System.Windows.Forms.TextBox
    Friend WithEvents txtBERCost As System.Windows.Forms.TextBox
    Friend WithEvents txtBERMargin As System.Windows.Forms.TextBox
    Friend WithEvents txtBERSell As System.Windows.Forms.TextBox
    Friend WithEvents BEProductIDLabel As System.Windows.Forms.Label
    Friend WithEvents BEDescriptionLabel As System.Windows.Forms.Label
    Friend WithEvents RTLabel As System.Windows.Forms.Label
    Friend WithEvents BETypeLabel As System.Windows.Forms.Label
    Friend WithEvents BECostLabel As System.Windows.Forms.Label
    Friend WithEvents BEMarginLabel As System.Windows.Forms.Label
    Friend WithEvents BESellLabel As System.Windows.Forms.Label
    Friend WithEvents lblBEBType As System.Windows.Forms.Label
    Friend WithEvents btnUpdatePrice As System.Windows.Forms.Button
    Friend WithEvents cbxClearOnAdd As System.Windows.Forms.CheckBox
    Friend WithEvents btnUpdateUnitLineItems As System.Windows.Forms.Button
    Friend WithEvents txtUnitEdQuan As System.Windows.Forms.TextBox
    Friend WithEvents txtUnitEdLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtUnitEdType As System.Windows.Forms.TextBox
    Friend WithEvents BubtnDeleteUnitLineItems As System.Windows.Forms.Button
    Friend WithEvents txtUnitEdOldLocation As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgEdUnitID As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgEDQuan As System.Windows.Forms.TextBox
    Friend WithEvents txtBldgEdBldgID As System.Windows.Forms.TextBox
    Friend WithEvents btnBldgLIUpdate As System.Windows.Forms.Button
    Friend WithEvents btnBldgLIDelete As System.Windows.Forms.Button
    Friend WithEvents TotalLabel As System.Windows.Forms.Label
    Friend WithEvents ProfitLabel As System.Windows.Forms.Label
    Friend WithEvents DataGridTableStyle2 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents clmPartNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents clmDescription As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents clmQuantity As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents clmCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents ClmTotalCost As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents clmSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents clmTotalSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents clmNet As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents btnCopyUnit As System.Windows.Forms.Button
    Friend WithEvents btnbEditBid As System.Windows.Forms.Button
    Friend WithEvents cboAltBidItems As System.Windows.Forms.ComboBox
    Friend WithEvents IIBLabel As System.Windows.Forms.Label
    Friend WithEvents cboAltCatMain As System.Windows.Forms.ComboBox
    Friend WithEvents lblAltCatMain As System.Windows.Forms.Label
    Friend WithEvents rbAltMain As System.Windows.Forms.RadioButton
    Friend WithEvents rbAltSO As System.Windows.Forms.RadioButton
    Friend WithEvents btnAltAdd As System.Windows.Forms.Button
    Friend WithEvents dgAlt As System.Windows.Forms.DataGrid
    Friend WithEvents txtAltCost As System.Windows.Forms.TextBox
    Friend WithEvents txtAltSell As System.Windows.Forms.TextBox
    Friend WithEvents lblAltCost As System.Windows.Forms.Label
    Friend WithEvents lblAltMargin As System.Windows.Forms.Label
    Friend WithEvents lblAltSell As System.Windows.Forms.Label
    Friend WithEvents txtAltMargin As System.Windows.Forms.TextBox
    Friend WithEvents dgStyleAlt As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgAltAltID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltOProdID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltODesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltOSell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltQuan As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltAProdID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltADesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgAltASell As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents btnAltDelete As System.Windows.Forms.Button
    Friend WithEvents mnuBidReport As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRptSummary As System.Windows.Forms.MenuItem
    Friend WithEvents btnCancelUnitLIneItem As System.Windows.Forms.Button
    Friend WithEvents btnBldgLICancel As System.Windows.Forms.Button
    Friend WithEvents btnAltCancel As System.Windows.Forms.Button
    Friend WithEvents lblMarUnits As System.Windows.Forms.Label
    Friend WithEvents lblMarUnitCost As System.Windows.Forms.Label
    Friend WithEvents lblMarUnitSell As System.Windows.Forms.Label
    Friend WithEvents lblMarUnitMargin As System.Windows.Forms.Label
    Friend WithEvents txtMarUnitSell As System.Windows.Forms.Label
    Friend WithEvents txtMarUnitMargin As System.Windows.Forms.Label
    Friend WithEvents txtMarUnitCost As System.Windows.Forms.Label
    Friend WithEvents lblMarCH As System.Windows.Forms.Label
    Friend WithEvents txtMarCHCost As System.Windows.Forms.Label
    Friend WithEvents txtMarCHSell As System.Windows.Forms.Label
    Friend WithEvents txtMarCHMargin As System.Windows.Forms.Label
    Friend WithEvents lblMarSite As System.Windows.Forms.Label
    Friend WithEvents txtMarSiteCost As System.Windows.Forms.Label
    Friend WithEvents txtMarSiteSell As System.Windows.Forms.Label
    Friend WithEvents txtMarSiteMargin As System.Windows.Forms.Label
    Friend WithEvents dgStyleBldgUnits As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgBldgUnitType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBldgUnitDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBldgUnitUType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgBldgUnitQuant As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents lblLDEstimator As System.Windows.Forms.Label
    Friend WithEvents lblBluePrintDate As System.Windows.Forms.Label
    Friend WithEvents LBLRevDate As System.Windows.Forms.Label
    Friend WithEvents txtLDEstimator As System.Windows.Forms.TextBox
    Friend WithEvents txtBluePrintDate As System.Windows.Forms.TextBox
    Friend WithEvents txtRevDate As System.Windows.Forms.TextBox
    Friend WithEvents txtSalesTaxRate As System.Windows.Forms.TextBox
    Friend WithEvents lblSalesTaxRate As System.Windows.Forms.Label
    Friend WithEvents txtSummaryTotalCost As System.Windows.Forms.Label
    Friend WithEvents txtSummaryTotalSell As System.Windows.Forms.Label
    Friend WithEvents txtSummaryProfit As System.Windows.Forms.Label
    Friend WithEvents txtSummaryMargin As System.Windows.Forms.Label
    Friend WithEvents txtBidBldgCnt As System.Windows.Forms.Label
    Friend WithEvents txtBidUnitCnt As System.Windows.Forms.Label
    Friend WithEvents NumUnitsLabel As System.Windows.Forms.Label
    Friend WithEvents mnuRptSnapshot As System.Windows.Forms.MenuItem
    Friend WithEvents CommonLabel As System.Windows.Forms.Label
    Friend WithEvents txtMarComCost As System.Windows.Forms.Label
    Friend WithEvents txtMarComSell As System.Windows.Forms.Label
    Friend WithEvents txtMarComMargin As System.Windows.Forms.Label
    Friend WithEvents ReplaceThisLabel As System.Windows.Forms.Label
    Friend WithEvents ReplaceWithLabel As System.Windows.Forms.Label
    Friend WithEvents cbxUFixedSell As System.Windows.Forms.CheckBox
    Friend WithEvents lblBContact As System.Windows.Forms.Label
    Friend WithEvents txtBBidContact As System.Windows.Forms.TextBox
    Friend WithEvents tbComments As System.Windows.Forms.TabPage
    Friend WithEvents dgComments As System.Windows.Forms.DataGrid
    Friend WithEvents txtCAuthor As System.Windows.Forms.TextBox
    Friend WithEvents txtCComment As System.Windows.Forms.TextBox
    Friend WithEvents lblCComment As System.Windows.Forms.Label
    Friend WithEvents lbCAuthor As System.Windows.Forms.Label
    Friend WithEvents btnSaveComment As System.Windows.Forms.Button
    Friend WithEvents tsComments As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents comCommentID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents comEntryDate As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents comAuthor As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents comComment As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents btnCommentEDit As System.Windows.Forms.Button
    Friend WithEvents btnCommentDelete As System.Windows.Forms.Button
    Friend WithEvents mnuUnitsBOM As System.Windows.Forms.MenuItem
    Friend WithEvents txtUnitEdOldType As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateUnitGrid As System.Windows.Forms.Button
    Friend WithEvents tbSearch As System.Windows.Forms.TabPage
    Friend WithEvents cboSLocation As System.Windows.Forms.ComboBox
    Friend WithEvents cboSType As System.Windows.Forms.ComboBox
    Friend WithEvents cboSPartNum As System.Windows.Forms.ComboBox
    Friend WithEvents SearchPartNumLabel As System.Windows.Forms.Label
    Friend WithEvents SearchTypeLabel As System.Windows.Forms.Label
    Friend WithEvents SearchLocationLabel As System.Windows.Forms.Label
    Friend WithEvents dgSearch As System.Windows.Forms.DataGrid
    Friend WithEvents btnLoadSearch As System.Windows.Forms.Button
    Friend WithEvents dgstSearch As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents dgSPartNumber As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsLocation As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsUnitType As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents dgsDesc As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents txtAltProdID As System.Windows.Forms.TextBox
    Friend WithEvents txtAltDesc As System.Windows.Forms.TextBox
    Friend WithEvents PartNumberLabel As System.Windows.Forms.Label
    Friend WithEvents AlternateDescriptionLabel As System.Windows.Forms.Label
    Friend WithEvents AlternateLabel As System.Windows.Forms.Label
    Friend WithEvents btnAltAddSO As System.Windows.Forms.Button
    Friend WithEvents lblMarAux As System.Windows.Forms.Label
    Friend WithEvents txtMarAuxCost As System.Windows.Forms.Label
    Friend WithEvents txtMarAuxSell As System.Windows.Forms.Label
    Friend WithEvents txtMarAuxMar As System.Windows.Forms.Label
    Friend WithEvents mnuEditAux As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUpdateCosts As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.MenuItem
    Friend WithEvents btnBERDByType As System.Windows.Forms.Button
    Friend WithEvents mnuUpdateDesc As System.Windows.Forms.MenuItem
    Friend WithEvents dgsQuantity As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents mnuAdjustMargins As System.Windows.Forms.MenuItem
    Friend WithEvents btnPrintSearch As System.Windows.Forms.Button
    Friend WithEvents mnuEditCosts As System.Windows.Forms.MenuItem
    Friend WithEvents mnuShipDates As System.Windows.Forms.MenuItem
    Friend WithEvents txtUnitCount As System.Windows.Forms.TextBox
    Friend WithEvents mnuCopyUnitOtherBid As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUtilities As System.Windows.Forms.MenuItem
    Friend WithEvents UnitCountLabel As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim QuanColumn As System.Windows.Forms.DataGridTextBoxColumn
        Dim bldgStyle As System.Windows.Forms.DataGridTableStyle
        Me.dgBldg = New System.Windows.Forms.DataGrid()
        Me.rwBldgID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.rwQuan = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.rwUnitType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.rwDescription = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgUnitLineItems = New System.Windows.Forms.DataGrid()
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle()
        Me.PartNumCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.DescCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.LocCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.RTCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.TypeCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.CostCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.MarginCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.SellCol = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.TabCtrlBid = New System.Windows.Forms.TabControl()
        Me.tbClient = New System.Windows.Forms.TabPage()
        Me.cboClients = New System.Windows.Forms.ComboBox()
        Me.txtBBidContact = New System.Windows.Forms.TextBox()
        Me.lblBContact = New System.Windows.Forms.Label()
        Me.lblSalesTaxRate = New System.Windows.Forms.Label()
        Me.txtSalesTaxRate = New System.Windows.Forms.TextBox()
        Me.txtRevDate = New System.Windows.Forms.TextBox()
        Me.txtBluePrintDate = New System.Windows.Forms.TextBox()
        Me.txtLDEstimator = New System.Windows.Forms.TextBox()
        Me.LBLRevDate = New System.Windows.Forms.Label()
        Me.lblBluePrintDate = New System.Windows.Forms.Label()
        Me.lblLDEstimator = New System.Windows.Forms.Label()
        Me.btnbEditBid = New System.Windows.Forms.Button()
        Me.ProjectLabel = New System.Windows.Forms.Label()
        Me.ZipLabel = New System.Windows.Forms.Label()
        Me.StateLabel = New System.Windows.Forms.Label()
        Me.CityLabel = New System.Windows.Forms.Label()
        Me.AddressLabel = New System.Windows.Forms.Label()
        Me.LocationLabel = New System.Windows.Forms.Label()
        Me.DescriptionLabel = New System.Windows.Forms.Label()
        Me.txtbDescription = New System.Windows.Forms.TextBox()
        Me.NotesLabel = New System.Windows.Forms.Label()
        Me.txtBNotes = New System.Windows.Forms.TextBox()
        Me.ClientDataLabel = New System.Windows.Forms.Label()
        Me.EmailLabel = New System.Windows.Forms.Label()
        Me.FaxLabel = New System.Windows.Forms.Label()
        Me.PhoneLabel = New System.Windows.Forms.Label()
        Me.txtCLemail = New System.Windows.Forms.TextBox()
        Me.txtCLPhone2 = New System.Windows.Forms.TextBox()
        Me.txtCLFAX = New System.Windows.Forms.TextBox()
        Me.txtCLPhone1 = New System.Windows.Forms.TextBox()
        Me.txtCLZip = New System.Windows.Forms.TextBox()
        Me.txtCLState = New System.Windows.Forms.TextBox()
        Me.txtCLCity = New System.Windows.Forms.TextBox()
        Me.txtCLAddr2 = New System.Windows.Forms.TextBox()
        Me.txtClAddr1 = New System.Windows.Forms.TextBox()
        Me.txtCLContact = New System.Windows.Forms.TextBox()
        Me.txtCLName = New System.Windows.Forms.TextBox()
        Me.txtBZip = New System.Windows.Forms.TextBox()
        Me.txtBAddress = New System.Windows.Forms.TextBox()
        Me.txtBCity = New System.Windows.Forms.TextBox()
        Me.txtBState = New System.Windows.Forms.TextBox()
        Me.txtBLocation = New System.Windows.Forms.TextBox()
        Me.tbUnits = New System.Windows.Forms.TabPage()
        Me.btnUpdateUnitGrid = New System.Windows.Forms.Button()
        Me.txtUnitEdOldType = New System.Windows.Forms.TextBox()
        Me.cbxUFixedSell = New System.Windows.Forms.CheckBox()
        Me.btnCancelUnitLIneItem = New System.Windows.Forms.Button()
        Me.btnCopyUnit = New System.Windows.Forms.Button()
        Me.txtUnitEdOldLocation = New System.Windows.Forms.TextBox()
        Me.BubtnDeleteUnitLineItems = New System.Windows.Forms.Button()
        Me.txtUnitEdType = New System.Windows.Forms.TextBox()
        Me.txtUnitEdLocation = New System.Windows.Forms.TextBox()
        Me.cbxClearOnAdd = New System.Windows.Forms.CheckBox()
        Me.lblLIType = New System.Windows.Forms.Label()
        Me.txtLIType = New System.Windows.Forms.TextBox()
        Me.UnitLocationLabel = New System.Windows.Forms.Label()
        Me.txtLILocation = New System.Windows.Forms.TextBox()
        Me.btnAddSO = New System.Windows.Forms.Button()
        Me.lblRT = New System.Windows.Forms.Label()
        Me.txtLIRT = New System.Windows.Forms.TextBox()
        Me.UnitLocation = New System.Windows.Forms.Panel()
        Me.rbSO = New System.Windows.Forms.RadioButton()
        Me.rbBid = New System.Windows.Forms.RadioButton()
        Me.rbMain = New System.Windows.Forms.RadioButton()
        Me.lblUnitDesc = New System.Windows.Forms.Label()
        Me.lblUnitType = New System.Windows.Forms.Label()
        Me.btnAddUnit = New System.Windows.Forms.Button()
        Me.txtBUnitDescription = New System.Windows.Forms.TextBox()
        Me.txtBUnitType = New System.Windows.Forms.TextBox()
        Me.SellLabel = New System.Windows.Forms.Label()
        Me.MarginLabel = New System.Windows.Forms.Label()
        Me.UnitDescriptionLabel = New System.Windows.Forms.Label()
        Me.CoLabel = New System.Windows.Forms.Label()
        Me.ProductIDLabel = New System.Windows.Forms.Label()
        Me.QuantityLabel = New System.Windows.Forms.Label()
        Me.cboCatalogMain = New System.Windows.Forms.ComboBox()
        Me.btnAddItem = New System.Windows.Forms.Button()
        Me.txtLISell = New System.Windows.Forms.TextBox()
        Me.txtLIMargin = New System.Windows.Forms.TextBox()
        Me.txtLICost = New System.Windows.Forms.TextBox()
        Me.txtLIQuan = New System.Windows.Forms.TextBox()
        Me.txtLIProductID = New System.Windows.Forms.TextBox()
        Me.btnNewUnit = New System.Windows.Forms.Button()
        Me.cboUnit = New System.Windows.Forms.ComboBox()
        Me.txtLIDesc = New System.Windows.Forms.TextBox()
        Me.btnUpdateUnitLineItems = New System.Windows.Forms.Button()
        Me.txtUnitEdQuan = New System.Windows.Forms.TextBox()
        Me.tbAlternates = New System.Windows.Forms.TabPage()
        Me.btnAltAddSO = New System.Windows.Forms.Button()
        Me.AlternateLabel = New System.Windows.Forms.Label()
        Me.AlternateDescriptionLabel = New System.Windows.Forms.Label()
        Me.PartNumberLabel = New System.Windows.Forms.Label()
        Me.txtAltDesc = New System.Windows.Forms.TextBox()
        Me.txtAltProdID = New System.Windows.Forms.TextBox()
        Me.btnAltCancel = New System.Windows.Forms.Button()
        Me.btnAltDelete = New System.Windows.Forms.Button()
        Me.lblAltSell = New System.Windows.Forms.Label()
        Me.lblAltMargin = New System.Windows.Forms.Label()
        Me.lblAltCost = New System.Windows.Forms.Label()
        Me.txtAltSell = New System.Windows.Forms.TextBox()
        Me.txtAltMargin = New System.Windows.Forms.TextBox()
        Me.txtAltCost = New System.Windows.Forms.TextBox()
        Me.dgAlt = New System.Windows.Forms.DataGrid()
        Me.dgStyleAlt = New System.Windows.Forms.DataGridTableStyle()
        Me.dgAltAltID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltOProdID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltODesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltOSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltQuan = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltAProdID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltADesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgAltASell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.btnAltAdd = New System.Windows.Forms.Button()
        Me.rbAltSO = New System.Windows.Forms.RadioButton()
        Me.rbAltMain = New System.Windows.Forms.RadioButton()
        Me.lblAltCatMain = New System.Windows.Forms.Label()
        Me.cboAltCatMain = New System.Windows.Forms.ComboBox()
        Me.IIBLabel = New System.Windows.Forms.Label()
        Me.cboAltBidItems = New System.Windows.Forms.ComboBox()
        Me.tbBuilding = New System.Windows.Forms.TabPage()
        Me.UnitCountLabel = New System.Windows.Forms.Label()
        Me.txtUnitCount = New System.Windows.Forms.TextBox()
        Me.btnBldgLICancel = New System.Windows.Forms.Button()
        Me.btnBldgLIDelete = New System.Windows.Forms.Button()
        Me.btnBldgLIUpdate = New System.Windows.Forms.Button()
        Me.txtBldgEdUnitID = New System.Windows.Forms.TextBox()
        Me.txtBldgEdBldgID = New System.Windows.Forms.TextBox()
        Me.txtBldgEDQuan = New System.Windows.Forms.TextBox()
        Me.lblBldgShipSeq = New System.Windows.Forms.Label()
        Me.txtBldgShipSeq = New System.Windows.Forms.TextBox()
        Me.lblBldgNum = New System.Windows.Forms.Label()
        Me.txtBldgNum = New System.Windows.Forms.TextBox()
        Me.lblBldgSelectBldg = New System.Windows.Forms.Label()
        Me.lblBldgSelUnit = New System.Windows.Forms.Label()
        Me.lblBldgShipDate = New System.Windows.Forms.Label()
        Me.lblBldgNotes = New System.Windows.Forms.Label()
        Me.lblBldgAddress = New System.Windows.Forms.Label()
        Me.lblBldgLocation = New System.Windows.Forms.Label()
        Me.lblBldgDesc = New System.Windows.Forms.Label()
        Me.lblBldgType = New System.Windows.Forms.Label()
        Me.txtBldgNotes = New System.Windows.Forms.TextBox()
        Me.txtBldgAddress = New System.Windows.Forms.TextBox()
        Me.txtBldgLocation = New System.Windows.Forms.TextBox()
        Me.btnAddUnitToBldg = New System.Windows.Forms.Button()
        Me.lblBldgUnitQuan = New System.Windows.Forms.Label()
        Me.txtBldgUnitQuan = New System.Windows.Forms.TextBox()
        Me.cboBldgUnits = New System.Windows.Forms.ComboBox()
        Me.txtBldgShipDate = New System.Windows.Forms.TextBox()
        Me.txtbldgDesc = New System.Windows.Forms.TextBox()
        Me.txtbldgType = New System.Windows.Forms.TextBox()
        Me.btnAddBldg = New System.Windows.Forms.Button()
        Me.cboBldg = New System.Windows.Forms.ComboBox()
        Me.tbLIBulkEdit = New System.Windows.Forms.TabPage()
        Me.btnBERDByType = New System.Windows.Forms.Button()
        Me.ReplaceWithLabel = New System.Windows.Forms.Label()
        Me.ReplaceThisLabel = New System.Windows.Forms.Label()
        Me.btnUpdatePrice = New System.Windows.Forms.Button()
        Me.BESellLabel = New System.Windows.Forms.Label()
        Me.BEMarginLabel = New System.Windows.Forms.Label()
        Me.BECostLabel = New System.Windows.Forms.Label()
        Me.BETypeLabel = New System.Windows.Forms.Label()
        Me.RTLabel = New System.Windows.Forms.Label()
        Me.BEDescriptionLabel = New System.Windows.Forms.Label()
        Me.BEProductIDLabel = New System.Windows.Forms.Label()
        Me.txtBERSell = New System.Windows.Forms.TextBox()
        Me.txtBERMargin = New System.Windows.Forms.TextBox()
        Me.txtBERCost = New System.Windows.Forms.TextBox()
        Me.txtBERType = New System.Windows.Forms.TextBox()
        Me.txtBERRT = New System.Windows.Forms.TextBox()
        Me.txtBERDesc = New System.Windows.Forms.TextBox()
        Me.txtBERProdID = New System.Windows.Forms.TextBox()
        Me.btnBEAdjustSell = New System.Windows.Forms.Button()
        Me.TypesIncludedLabel = New System.Windows.Forms.Label()
        Me.btnBERByType = New System.Windows.Forms.Button()
        Me.btnBERByPartNum = New System.Windows.Forms.Button()
        Me.btnBEDelete = New System.Windows.Forms.Button()
        Me.cboBEType = New System.Windows.Forms.ComboBox()
        Me.CurrentBidLabel = New System.Windows.Forms.Label()
        Me.cbxFSell = New System.Windows.Forms.CheckBox()
        Me.txtBEBMargin = New System.Windows.Forms.TextBox()
        Me.BEMarginLabel2 = New System.Windows.Forms.Label()
        Me.BESellLabel2 = New System.Windows.Forms.Label()
        Me.BECostLabel2 = New System.Windows.Forms.Label()
        Me.lblBEBType = New System.Windows.Forms.Label()
        Me.RTLabel2 = New System.Windows.Forms.Label()
        Me.BEDescription2 = New System.Windows.Forms.Label()
        Me.BEProductIDLabel2 = New System.Windows.Forms.Label()
        Me.txtBEBSell = New System.Windows.Forms.TextBox()
        Me.txtBEBCost = New System.Windows.Forms.TextBox()
        Me.txtBEBType = New System.Windows.Forms.TextBox()
        Me.txtBEBRT = New System.Windows.Forms.TextBox()
        Me.txtBEBDesc = New System.Windows.Forms.TextBox()
        Me.txtBEBProdID = New System.Windows.Forms.TextBox()
        Me.rbBESO = New System.Windows.Forms.RadioButton()
        Me.rbBEMain = New System.Windows.Forms.RadioButton()
        Me.cboBECatalog = New System.Windows.Forms.ComboBox()
        Me.cboBEBidItems = New System.Windows.Forms.ComboBox()
        Me.tbSummary = New System.Windows.Forms.TabPage()
        Me.txtMarAuxMar = New System.Windows.Forms.Label()
        Me.txtMarAuxSell = New System.Windows.Forms.Label()
        Me.txtMarAuxCost = New System.Windows.Forms.Label()
        Me.lblMarAux = New System.Windows.Forms.Label()
        Me.txtMarComMargin = New System.Windows.Forms.Label()
        Me.txtMarComSell = New System.Windows.Forms.Label()
        Me.txtMarComCost = New System.Windows.Forms.Label()
        Me.CommonLabel = New System.Windows.Forms.Label()
        Me.NumUnitsLabel = New System.Windows.Forms.Label()
        Me.txtBidUnitCnt = New System.Windows.Forms.Label()
        Me.txtBidBldgCnt = New System.Windows.Forms.Label()
        Me.txtSummaryMargin = New System.Windows.Forms.Label()
        Me.txtSummaryProfit = New System.Windows.Forms.Label()
        Me.txtSummaryTotalSell = New System.Windows.Forms.Label()
        Me.txtSummaryTotalCost = New System.Windows.Forms.Label()
        Me.txtMarSiteMargin = New System.Windows.Forms.Label()
        Me.txtMarSiteSell = New System.Windows.Forms.Label()
        Me.txtMarSiteCost = New System.Windows.Forms.Label()
        Me.lblMarSite = New System.Windows.Forms.Label()
        Me.txtMarCHMargin = New System.Windows.Forms.Label()
        Me.txtMarCHSell = New System.Windows.Forms.Label()
        Me.txtMarCHCost = New System.Windows.Forms.Label()
        Me.lblMarCH = New System.Windows.Forms.Label()
        Me.txtMarUnitCost = New System.Windows.Forms.Label()
        Me.txtMarUnitMargin = New System.Windows.Forms.Label()
        Me.txtMarUnitSell = New System.Windows.Forms.Label()
        Me.lblMarUnitMargin = New System.Windows.Forms.Label()
        Me.lblMarUnitSell = New System.Windows.Forms.Label()
        Me.lblMarUnitCost = New System.Windows.Forms.Label()
        Me.lblMarUnits = New System.Windows.Forms.Label()
        Me.ProfitLabel = New System.Windows.Forms.Label()
        Me.TotalLabel = New System.Windows.Forms.Label()
        Me.dgBOM = New System.Windows.Forms.DataGrid()
        Me.DataGridTableStyle2 = New System.Windows.Forms.DataGridTableStyle()
        Me.clmPartNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.clmDescription = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.clmQuantity = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.clmCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.ClmTotalCost = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.clmSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.clmTotalSell = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.clmNet = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgUnitCnt = New System.Windows.Forms.DataGrid()
        Me.dgBidUnitsperBldg = New System.Windows.Forms.DataGrid()
        Me.dgStyleBldgUnits = New System.Windows.Forms.DataGridTableStyle()
        Me.dgBldgUnitType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBldgUnitDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBldgUnitUType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgBldgUnitQuant = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.NumBuildingsLabel = New System.Windows.Forms.Label()
        Me.tbSearch = New System.Windows.Forms.TabPage()
        Me.btnPrintSearch = New System.Windows.Forms.Button()
        Me.btnLoadSearch = New System.Windows.Forms.Button()
        Me.dgSearch = New System.Windows.Forms.DataGrid()
        Me.dgstSearch = New System.Windows.Forms.DataGridTableStyle()
        Me.dgSPartNumber = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsDesc = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsQuantity = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsLocation = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.dgsUnitType = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.SearchLocationLabel = New System.Windows.Forms.Label()
        Me.SearchTypeLabel = New System.Windows.Forms.Label()
        Me.SearchPartNumLabel = New System.Windows.Forms.Label()
        Me.cboSPartNum = New System.Windows.Forms.ComboBox()
        Me.cboSType = New System.Windows.Forms.ComboBox()
        Me.cboSLocation = New System.Windows.Forms.ComboBox()
        Me.tbComments = New System.Windows.Forms.TabPage()
        Me.btnCommentDelete = New System.Windows.Forms.Button()
        Me.btnCommentEDit = New System.Windows.Forms.Button()
        Me.btnSaveComment = New System.Windows.Forms.Button()
        Me.lbCAuthor = New System.Windows.Forms.Label()
        Me.lblCComment = New System.Windows.Forms.Label()
        Me.txtCComment = New System.Windows.Forms.TextBox()
        Me.txtCAuthor = New System.Windows.Forms.TextBox()
        Me.dgComments = New System.Windows.Forms.DataGrid()
        Me.tsComments = New System.Windows.Forms.DataGridTableStyle()
        Me.comCommentID = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.comEntryDate = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.comAuthor = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.comComment = New System.Windows.Forms.DataGridTextBoxColumn()
        Me.BidMainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuBidReport = New System.Windows.Forms.MenuItem()
        Me.mnuRptSummary = New System.Windows.Forms.MenuItem()
        Me.mnuRptSnapshot = New System.Windows.Forms.MenuItem()
        Me.mnuUnitsBOM = New System.Windows.Forms.MenuItem()
        Me.mnuEditAux = New System.Windows.Forms.MenuItem()
        Me.mnuUtilities = New System.Windows.Forms.MenuItem()
        Me.mnuUpdateCosts = New System.Windows.Forms.MenuItem()
        Me.mnuUpdateDesc = New System.Windows.Forms.MenuItem()
        Me.mnuAdjustMargins = New System.Windows.Forms.MenuItem()
        Me.mnuEditCosts = New System.Windows.Forms.MenuItem()
        Me.mnuShipDates = New System.Windows.Forms.MenuItem()
        Me.mnuExport = New System.Windows.Forms.MenuItem()
        Me.mnuCopyUnitOtherBid = New System.Windows.Forms.MenuItem()
        Me.cboBid = New System.Windows.Forms.ComboBox()
        Me.txtBClientPM = New System.Windows.Forms.TextBox()
        Me.lblClientPM = New System.Windows.Forms.Label()
        Me.lblLDPM = New System.Windows.Forms.Label()
        Me.txtBLDPM = New System.Windows.Forms.TextBox()
        Me.btnSaveNewBid = New System.Windows.Forms.Button()
        Me.txtBid = New System.Windows.Forms.TextBox()
        QuanColumn = New System.Windows.Forms.DataGridTextBoxColumn()
        bldgStyle = New System.Windows.Forms.DataGridTableStyle()
        CType(Me.dgBldg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgUnitLineItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCtrlBid.SuspendLayout()
        Me.tbClient.SuspendLayout()
        Me.tbUnits.SuspendLayout()
        Me.UnitLocation.SuspendLayout()
        Me.tbAlternates.SuspendLayout()
        CType(Me.dgAlt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbBuilding.SuspendLayout()
        Me.tbLIBulkEdit.SuspendLayout()
        Me.tbSummary.SuspendLayout()
        CType(Me.dgBOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgUnitCnt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgBidUnitsperBldg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbSearch.SuspendLayout()
        CType(Me.dgSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbComments.SuspendLayout()
        CType(Me.dgComments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'QuanColumn
        '
        QuanColumn.Format = ""
        QuanColumn.FormatInfo = Nothing
        QuanColumn.HeaderText = "Quantity"
        QuanColumn.MappingName = "Quantity"
        QuanColumn.Width = 75
        '
        'bldgStyle
        '
        bldgStyle.DataGrid = Me.dgBldg
        bldgStyle.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.rwBldgID, Me.rwQuan, Me.rwUnitType, Me.rwDescription})
        bldgStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText
        bldgStyle.MappingName = "LineItems"
        bldgStyle.ReadOnly = True
        '
        'dgBldg
        '
        Me.dgBldg.DataMember = ""
        Me.dgBldg.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBldg.Location = New System.Drawing.Point(16, 96)
        Me.dgBldg.Name = "dgBldg"
        Me.dgBldg.ReadOnly = True
        Me.dgBldg.Size = New System.Drawing.Size(456, 240)
        Me.dgBldg.TabIndex = 11
        Me.dgBldg.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {bldgStyle})
        '
        'rwBldgID
        '
        Me.rwBldgID.Format = ""
        Me.rwBldgID.FormatInfo = Nothing
        Me.rwBldgID.MappingName = "BldgID"
        Me.rwBldgID.Width = 0
        '
        'rwQuan
        '
        Me.rwQuan.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.rwQuan.Format = ""
        Me.rwQuan.FormatInfo = Nothing
        Me.rwQuan.HeaderText = "Quantity"
        Me.rwQuan.MappingName = "Quantity"
        Me.rwQuan.NullText = ""
        Me.rwQuan.ReadOnly = True
        Me.rwQuan.Width = 50
        '
        'rwUnitType
        '
        Me.rwUnitType.Format = ""
        Me.rwUnitType.FormatInfo = Nothing
        Me.rwUnitType.HeaderText = "Unit Type"
        Me.rwUnitType.MappingName = "UnitType"
        Me.rwUnitType.NullText = ""
        Me.rwUnitType.ReadOnly = True
        Me.rwUnitType.Width = 125
        '
        'rwDescription
        '
        Me.rwDescription.Format = ""
        Me.rwDescription.FormatInfo = Nothing
        Me.rwDescription.HeaderText = "Description"
        Me.rwDescription.MappingName = "Description"
        Me.rwDescription.ReadOnly = True
        Me.rwDescription.Width = 250
        '
        'dgUnitLineItems
        '
        Me.dgUnitLineItems.AlternatingBackColor = System.Drawing.Color.LightGray
        Me.dgUnitLineItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgUnitLineItems.BackColor = System.Drawing.Color.DarkGray
        Me.dgUnitLineItems.CaptionBackColor = System.Drawing.Color.White
        Me.dgUnitLineItems.CaptionFont = New System.Drawing.Font("Verdana", 10.0!)
        Me.dgUnitLineItems.CaptionForeColor = System.Drawing.Color.Navy
        Me.dgUnitLineItems.DataMember = ""
        Me.dgUnitLineItems.ForeColor = System.Drawing.Color.Black
        Me.dgUnitLineItems.GridLineColor = System.Drawing.Color.Black
        Me.dgUnitLineItems.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None
        Me.dgUnitLineItems.HeaderBackColor = System.Drawing.Color.Silver
        Me.dgUnitLineItems.HeaderForeColor = System.Drawing.Color.Black
        Me.dgUnitLineItems.LinkColor = System.Drawing.Color.Navy
        Me.dgUnitLineItems.Location = New System.Drawing.Point(8, 120)
        Me.dgUnitLineItems.Name = "dgUnitLineItems"
        Me.dgUnitLineItems.ParentRowsBackColor = System.Drawing.Color.White
        Me.dgUnitLineItems.ParentRowsForeColor = System.Drawing.Color.Black
        Me.dgUnitLineItems.ReadOnly = True
        Me.dgUnitLineItems.SelectionBackColor = System.Drawing.Color.Navy
        Me.dgUnitLineItems.SelectionForeColor = System.Drawing.Color.White
        Me.dgUnitLineItems.Size = New System.Drawing.Size(696, 224)
        Me.dgUnitLineItems.TabIndex = 13
        Me.dgUnitLineItems.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle1})
        Me.dgUnitLineItems.TabStop = False
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.AlternatingBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.DataGridTableStyle1.DataGrid = Me.dgUnitLineItems
        Me.DataGridTableStyle1.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {QuanColumn, Me.PartNumCol, Me.DescCol, Me.LocCol, Me.RTCol, Me.TypeCol, Me.CostCol, Me.MarginCol, Me.SellCol})
        Me.DataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle1.MappingName = "LineItems"
        Me.DataGridTableStyle1.ReadOnly = True
        '
        'PartNumCol
        '
        Me.PartNumCol.Format = ""
        Me.PartNumCol.FormatInfo = Nothing
        Me.PartNumCol.HeaderText = "Part Number"
        Me.PartNumCol.MappingName = "PartNumber"
        Me.PartNumCol.Width = 75
        '
        'DescCol
        '
        Me.DescCol.Format = ""
        Me.DescCol.FormatInfo = Nothing
        Me.DescCol.HeaderText = "Description"
        Me.DescCol.MappingName = "LIDesc"
        Me.DescCol.Width = 75
        '
        'LocCol
        '
        Me.LocCol.Format = ""
        Me.LocCol.FormatInfo = Nothing
        Me.LocCol.HeaderText = "Location"
        Me.LocCol.MappingName = "Location"
        Me.LocCol.Width = 75
        '
        'RTCol
        '
        Me.RTCol.Format = ""
        Me.RTCol.FormatInfo = Nothing
        Me.RTCol.HeaderText = "R/T"
        Me.RTCol.MappingName = "RT"
        Me.RTCol.Width = 75
        '
        'TypeCol
        '
        Me.TypeCol.Format = ""
        Me.TypeCol.FormatInfo = Nothing
        Me.TypeCol.HeaderText = "Type"
        Me.TypeCol.MappingName = "Type"
        Me.TypeCol.Width = 75
        '
        'CostCol
        '
        Me.CostCol.Format = "c"
        Me.CostCol.FormatInfo = Nothing
        Me.CostCol.HeaderText = "Cost"
        Me.CostCol.MappingName = "Cost"
        Me.CostCol.Width = 75
        '
        'MarginCol
        '
        Me.MarginCol.Format = ""
        Me.MarginCol.FormatInfo = Nothing
        Me.MarginCol.HeaderText = "Margin"
        Me.MarginCol.MappingName = "Margin"
        Me.MarginCol.Width = 75
        '
        'SellCol
        '
        Me.SellCol.Format = "c"
        Me.SellCol.FormatInfo = Nothing
        Me.SellCol.HeaderText = "Sell"
        Me.SellCol.MappingName = "Sell"
        Me.SellCol.Width = 75
        '
        'TabCtrlBid
        '
        Me.TabCtrlBid.Controls.Add(Me.tbClient)
        Me.TabCtrlBid.Controls.Add(Me.tbUnits)
        Me.TabCtrlBid.Controls.Add(Me.tbAlternates)
        Me.TabCtrlBid.Controls.Add(Me.tbBuilding)
        Me.TabCtrlBid.Controls.Add(Me.tbLIBulkEdit)
        Me.TabCtrlBid.Controls.Add(Me.tbSummary)
        Me.TabCtrlBid.Controls.Add(Me.tbSearch)
        Me.TabCtrlBid.Controls.Add(Me.tbComments)
        Me.TabCtrlBid.Location = New System.Drawing.Point(8, 72)
        Me.TabCtrlBid.Name = "TabCtrlBid"
        Me.TabCtrlBid.SelectedIndex = 0
        Me.TabCtrlBid.Size = New System.Drawing.Size(720, 408)
        Me.TabCtrlBid.TabIndex = 0
        '
        'tbClient
        '
        Me.tbClient.Controls.Add(Me.cboClients)
        Me.tbClient.Controls.Add(Me.txtBBidContact)
        Me.tbClient.Controls.Add(Me.lblBContact)
        Me.tbClient.Controls.Add(Me.lblSalesTaxRate)
        Me.tbClient.Controls.Add(Me.txtSalesTaxRate)
        Me.tbClient.Controls.Add(Me.txtRevDate)
        Me.tbClient.Controls.Add(Me.txtBluePrintDate)
        Me.tbClient.Controls.Add(Me.txtLDEstimator)
        Me.tbClient.Controls.Add(Me.LBLRevDate)
        Me.tbClient.Controls.Add(Me.lblBluePrintDate)
        Me.tbClient.Controls.Add(Me.lblLDEstimator)
        Me.tbClient.Controls.Add(Me.btnbEditBid)
        Me.tbClient.Controls.Add(Me.ProjectLabel)
        Me.tbClient.Controls.Add(Me.ZipLabel)
        Me.tbClient.Controls.Add(Me.StateLabel)
        Me.tbClient.Controls.Add(Me.CityLabel)
        Me.tbClient.Controls.Add(Me.AddressLabel)
        Me.tbClient.Controls.Add(Me.LocationLabel)
        Me.tbClient.Controls.Add(Me.DescriptionLabel)
        Me.tbClient.Controls.Add(Me.txtbDescription)
        Me.tbClient.Controls.Add(Me.NotesLabel)
        Me.tbClient.Controls.Add(Me.txtBNotes)
        Me.tbClient.Controls.Add(Me.ClientDataLabel)
        Me.tbClient.Controls.Add(Me.EmailLabel)
        Me.tbClient.Controls.Add(Me.FaxLabel)
        Me.tbClient.Controls.Add(Me.PhoneLabel)
        Me.tbClient.Controls.Add(Me.txtCLemail)
        Me.tbClient.Controls.Add(Me.txtCLPhone2)
        Me.tbClient.Controls.Add(Me.txtCLFAX)
        Me.tbClient.Controls.Add(Me.txtCLPhone1)
        Me.tbClient.Controls.Add(Me.txtCLZip)
        Me.tbClient.Controls.Add(Me.txtCLState)
        Me.tbClient.Controls.Add(Me.txtCLCity)
        Me.tbClient.Controls.Add(Me.txtCLAddr2)
        Me.tbClient.Controls.Add(Me.txtClAddr1)
        Me.tbClient.Controls.Add(Me.txtCLContact)
        Me.tbClient.Controls.Add(Me.txtCLName)
        Me.tbClient.Controls.Add(Me.txtBZip)
        Me.tbClient.Controls.Add(Me.txtBAddress)
        Me.tbClient.Controls.Add(Me.txtBCity)
        Me.tbClient.Controls.Add(Me.txtBState)
        Me.tbClient.Controls.Add(Me.txtBLocation)
        Me.tbClient.Location = New System.Drawing.Point(4, 22)
        Me.tbClient.Name = "tbClient"
        Me.tbClient.Size = New System.Drawing.Size(712, 382)
        Me.tbClient.TabIndex = 0
        Me.tbClient.Text = "Project"
        '
        'cboClients
        '
        Me.cboClients.Location = New System.Drawing.Point(40, 24)
        Me.cboClients.Name = "cboClients"
        Me.cboClients.Size = New System.Drawing.Size(336, 21)
        Me.cboClients.TabIndex = 0
        '
        'txtBBidContact
        '
        Me.txtBBidContact.Location = New System.Drawing.Point(40, 160)
        Me.txtBBidContact.Name = "txtBBidContact"
        Me.txtBBidContact.Size = New System.Drawing.Size(280, 20)
        Me.txtBBidContact.TabIndex = 1
        '
        'lblBContact
        '
        Me.lblBContact.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBContact.Location = New System.Drawing.Point(40, 144)
        Me.lblBContact.Name = "lblBContact"
        Me.lblBContact.Size = New System.Drawing.Size(56, 16)
        Me.lblBContact.TabIndex = 36
        Me.lblBContact.Text = "Bid Contact"
        '
        'lblSalesTaxRate
        '
        Me.lblSalesTaxRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalesTaxRate.Location = New System.Drawing.Point(40, 360)
        Me.lblSalesTaxRate.Name = "lblSalesTaxRate"
        Me.lblSalesTaxRate.Size = New System.Drawing.Size(72, 20)
        Me.lblSalesTaxRate.TabIndex = 35
        Me.lblSalesTaxRate.Text = "Sales Tax Rate"
        '
        'txtSalesTaxRate
        '
        Me.txtSalesTaxRate.Location = New System.Drawing.Point(137, 352)
        Me.txtSalesTaxRate.Name = "txtSalesTaxRate"
        Me.txtSalesTaxRate.Size = New System.Drawing.Size(100, 20)
        Me.txtSalesTaxRate.TabIndex = 9
        Me.txtSalesTaxRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRevDate
        '
        Me.txtRevDate.Location = New System.Drawing.Point(509, 352)
        Me.txtRevDate.Name = "txtRevDate"
        Me.txtRevDate.Size = New System.Drawing.Size(100, 20)
        Me.txtRevDate.TabIndex = 11
        Me.txtRevDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBluePrintDate
        '
        Me.txtBluePrintDate.Location = New System.Drawing.Point(340, 352)
        Me.txtBluePrintDate.Name = "txtBluePrintDate"
        Me.txtBluePrintDate.Size = New System.Drawing.Size(104, 20)
        Me.txtBluePrintDate.TabIndex = 10
        Me.txtBluePrintDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLDEstimator
        '
        Me.txtLDEstimator.Location = New System.Drawing.Point(392, 24)
        Me.txtLDEstimator.Name = "txtLDEstimator"
        Me.txtLDEstimator.Size = New System.Drawing.Size(216, 20)
        Me.txtLDEstimator.TabIndex = 0
        '
        'LBLRevDate
        '
        Me.LBLRevDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLRevDate.Location = New System.Drawing.Point(456, 360)
        Me.LBLRevDate.Name = "LBLRevDate"
        Me.LBLRevDate.Size = New System.Drawing.Size(48, 20)
        Me.LBLRevDate.TabIndex = 30
        Me.LBLRevDate.Text = "Rev Date"
        '
        'lblBluePrintDate
        '
        Me.lblBluePrintDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBluePrintDate.Location = New System.Drawing.Point(252, 360)
        Me.lblBluePrintDate.Name = "lblBluePrintDate"
        Me.lblBluePrintDate.Size = New System.Drawing.Size(80, 20)
        Me.lblBluePrintDate.TabIndex = 29
        Me.lblBluePrintDate.Text = "Blue Print Date"
        '
        'lblLDEstimator
        '
        Me.lblLDEstimator.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLDEstimator.Location = New System.Drawing.Point(392, 8)
        Me.lblLDEstimator.Name = "lblLDEstimator"
        Me.lblLDEstimator.Size = New System.Drawing.Size(64, 16)
        Me.lblLDEstimator.TabIndex = 28
        Me.lblLDEstimator.Text = "LD Estimator"
        '
        'btnbEditBid
        '
        Me.btnbEditBid.Location = New System.Drawing.Point(624, 352)
        Me.btnbEditBid.Name = "btnbEditBid"
        Me.btnbEditBid.Size = New System.Drawing.Size(80, 24)
        Me.btnbEditBid.TabIndex = 12
        Me.btnbEditBid.Text = "Edit"
        '
        'ProjectLabel
        '
        Me.ProjectLabel.Location = New System.Drawing.Point(280, 144)
        Me.ProjectLabel.Name = "ProjectLabel"
        Me.ProjectLabel.Size = New System.Drawing.Size(160, 16)
        Me.ProjectLabel.TabIndex = 26
        Me.ProjectLabel.Text = "----------- Project ------------"
        '
        'ZipLabel
        '
        Me.ZipLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZipLabel.Location = New System.Drawing.Point(552, 216)
        Me.ZipLabel.Name = "ZipLabel"
        Me.ZipLabel.Size = New System.Drawing.Size(40, 16)
        Me.ZipLabel.TabIndex = 25
        Me.ZipLabel.Text = "ZIP"
        '
        'StateLabel
        '
        Me.StateLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StateLabel.Location = New System.Drawing.Point(500, 216)
        Me.StateLabel.Name = "StateLabel"
        Me.StateLabel.Size = New System.Drawing.Size(24, 16)
        Me.StateLabel.TabIndex = 24
        Me.StateLabel.Text = "ST"
        '
        'CityLabel
        '
        Me.CityLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CityLabel.Location = New System.Drawing.Point(344, 216)
        Me.CityLabel.Name = "CityLabel"
        Me.CityLabel.Size = New System.Drawing.Size(56, 16)
        Me.CityLabel.TabIndex = 23
        Me.CityLabel.Text = "City"
        '
        'AddressLabel
        '
        Me.AddressLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddressLabel.Location = New System.Drawing.Point(40, 216)
        Me.AddressLabel.Name = "AddressLabel"
        Me.AddressLabel.Size = New System.Drawing.Size(48, 16)
        Me.AddressLabel.TabIndex = 22
        Me.AddressLabel.Text = "Address"
        '
        'LocationLabel
        '
        Me.LocationLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocationLabel.Location = New System.Drawing.Point(344, 176)
        Me.LocationLabel.Name = "LocationLabel"
        Me.LocationLabel.Size = New System.Drawing.Size(48, 16)
        Me.LocationLabel.TabIndex = 21
        Me.LocationLabel.Text = "Location"
        '
        'DescriptionLabel
        '
        Me.DescriptionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionLabel.Location = New System.Drawing.Point(40, 176)
        Me.DescriptionLabel.Name = "DescriptionLabel"
        Me.DescriptionLabel.Size = New System.Drawing.Size(56, 16)
        Me.DescriptionLabel.TabIndex = 20
        Me.DescriptionLabel.Text = "Desc"
        '
        'txtbDescription
        '
        Me.txtbDescription.Location = New System.Drawing.Point(40, 192)
        Me.txtbDescription.Name = "txtbDescription"
        Me.txtbDescription.Size = New System.Drawing.Size(280, 20)
        Me.txtbDescription.TabIndex = 2
        '
        'NotesLabel
        '
        Me.NotesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NotesLabel.Location = New System.Drawing.Point(40, 256)
        Me.NotesLabel.Name = "NotesLabel"
        Me.NotesLabel.Size = New System.Drawing.Size(40, 15)
        Me.NotesLabel.TabIndex = 18
        Me.NotesLabel.Text = "Notes"
        '
        'txtBNotes
        '
        Me.txtBNotes.Location = New System.Drawing.Point(40, 272)
        Me.txtBNotes.Multiline = True
        Me.txtBNotes.Name = "txtBNotes"
        Me.txtBNotes.Size = New System.Drawing.Size(568, 80)
        Me.txtBNotes.TabIndex = 8
        '
        'ClientDataLabel
        '
        Me.ClientDataLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClientDataLabel.Location = New System.Drawing.Point(40, 8)
        Me.ClientDataLabel.Name = "ClientDataLabel"
        Me.ClientDataLabel.Size = New System.Drawing.Size(192, 16)
        Me.ClientDataLabel.TabIndex = 15
        Me.ClientDataLabel.Text = "Client Data"
        '
        'EmailLabel
        '
        Me.EmailLabel.Location = New System.Drawing.Point(338, 128)
        Me.EmailLabel.Name = "EmailLabel"
        Me.EmailLabel.Size = New System.Drawing.Size(41, 16)
        Me.EmailLabel.TabIndex = 14
        Me.EmailLabel.Text = "e-mail"
        '
        'FaxLabel
        '
        Me.FaxLabel.Location = New System.Drawing.Point(338, 108)
        Me.FaxLabel.Name = "FaxLabel"
        Me.FaxLabel.Size = New System.Drawing.Size(33, 16)
        Me.FaxLabel.TabIndex = 13
        Me.FaxLabel.Text = "FAX"
        '
        'PhoneLabel
        '
        Me.PhoneLabel.Location = New System.Drawing.Point(338, 82)
        Me.PhoneLabel.Name = "PhoneLabel"
        Me.PhoneLabel.Size = New System.Drawing.Size(41, 16)
        Me.PhoneLabel.TabIndex = 12
        Me.PhoneLabel.Text = "Phone"
        '
        'txtCLemail
        '
        Me.txtCLemail.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLemail.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLemail.Location = New System.Drawing.Point(392, 116)
        Me.txtCLemail.Name = "txtCLemail"
        Me.txtCLemail.ReadOnly = True
        Me.txtCLemail.Size = New System.Drawing.Size(216, 20)
        Me.txtCLemail.TabIndex = 11
        Me.txtCLemail.TabStop = False
        '
        'txtCLPhone2
        '
        Me.txtCLPhone2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone2.Location = New System.Drawing.Point(504, 76)
        Me.txtCLPhone2.Name = "txtCLPhone2"
        Me.txtCLPhone2.ReadOnly = True
        Me.txtCLPhone2.Size = New System.Drawing.Size(104, 20)
        Me.txtCLPhone2.TabIndex = 9
        Me.txtCLPhone2.TabStop = False
        '
        'txtCLFAX
        '
        Me.txtCLFAX.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLFAX.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLFAX.Location = New System.Drawing.Point(392, 96)
        Me.txtCLFAX.Name = "txtCLFAX"
        Me.txtCLFAX.ReadOnly = True
        Me.txtCLFAX.Size = New System.Drawing.Size(112, 20)
        Me.txtCLFAX.TabIndex = 10
        Me.txtCLFAX.TabStop = False
        '
        'txtCLPhone1
        '
        Me.txtCLPhone1.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLPhone1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLPhone1.Location = New System.Drawing.Point(392, 76)
        Me.txtCLPhone1.Name = "txtCLPhone1"
        Me.txtCLPhone1.ReadOnly = True
        Me.txtCLPhone1.Size = New System.Drawing.Size(112, 20)
        Me.txtCLPhone1.TabIndex = 8
        Me.txtCLPhone1.TabStop = False
        '
        'txtCLZip
        '
        Me.txtCLZip.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLZip.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLZip.Location = New System.Drawing.Point(216, 116)
        Me.txtCLZip.Name = "txtCLZip"
        Me.txtCLZip.ReadOnly = True
        Me.txtCLZip.Size = New System.Drawing.Size(80, 20)
        Me.txtCLZip.TabIndex = 6
        Me.txtCLZip.TabStop = False
        '
        'txtCLState
        '
        Me.txtCLState.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLState.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLState.Location = New System.Drawing.Point(176, 116)
        Me.txtCLState.Name = "txtCLState"
        Me.txtCLState.ReadOnly = True
        Me.txtCLState.Size = New System.Drawing.Size(40, 20)
        Me.txtCLState.TabIndex = 5
        Me.txtCLState.TabStop = False
        '
        'txtCLCity
        '
        Me.txtCLCity.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLCity.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLCity.Location = New System.Drawing.Point(40, 116)
        Me.txtCLCity.Name = "txtCLCity"
        Me.txtCLCity.ReadOnly = True
        Me.txtCLCity.Size = New System.Drawing.Size(136, 20)
        Me.txtCLCity.TabIndex = 4
        Me.txtCLCity.TabStop = False
        '
        'txtCLAddr2
        '
        Me.txtCLAddr2.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLAddr2.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLAddr2.Location = New System.Drawing.Point(40, 96)
        Me.txtCLAddr2.Name = "txtCLAddr2"
        Me.txtCLAddr2.ReadOnly = True
        Me.txtCLAddr2.Size = New System.Drawing.Size(256, 20)
        Me.txtCLAddr2.TabIndex = 3
        Me.txtCLAddr2.TabStop = False
        '
        'txtClAddr1
        '
        Me.txtClAddr1.BackColor = System.Drawing.SystemColors.Info
        Me.txtClAddr1.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtClAddr1.Location = New System.Drawing.Point(40, 76)
        Me.txtClAddr1.Name = "txtClAddr1"
        Me.txtClAddr1.ReadOnly = True
        Me.txtClAddr1.Size = New System.Drawing.Size(256, 20)
        Me.txtClAddr1.TabIndex = 2
        Me.txtClAddr1.TabStop = False
        '
        'txtCLContact
        '
        Me.txtCLContact.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLContact.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLContact.Location = New System.Drawing.Point(392, 56)
        Me.txtCLContact.Name = "txtCLContact"
        Me.txtCLContact.ReadOnly = True
        Me.txtCLContact.Size = New System.Drawing.Size(216, 20)
        Me.txtCLContact.TabIndex = 7
        Me.txtCLContact.TabStop = False
        '
        'txtCLName
        '
        Me.txtCLName.BackColor = System.Drawing.SystemColors.Info
        Me.txtCLName.ForeColor = System.Drawing.SystemColors.InfoText
        Me.txtCLName.Location = New System.Drawing.Point(40, 56)
        Me.txtCLName.Name = "txtCLName"
        Me.txtCLName.ReadOnly = True
        Me.txtCLName.Size = New System.Drawing.Size(256, 20)
        Me.txtCLName.TabIndex = 1
        Me.txtCLName.TabStop = False
        '
        'txtBZip
        '
        Me.txtBZip.Location = New System.Drawing.Point(536, 232)
        Me.txtBZip.Name = "txtBZip"
        Me.txtBZip.Size = New System.Drawing.Size(72, 20)
        Me.txtBZip.TabIndex = 7
        '
        'txtBAddress
        '
        Me.txtBAddress.Location = New System.Drawing.Point(40, 232)
        Me.txtBAddress.Name = "txtBAddress"
        Me.txtBAddress.Size = New System.Drawing.Size(288, 20)
        Me.txtBAddress.TabIndex = 4
        '
        'txtBCity
        '
        Me.txtBCity.Location = New System.Drawing.Point(344, 232)
        Me.txtBCity.Name = "txtBCity"
        Me.txtBCity.Size = New System.Drawing.Size(144, 20)
        Me.txtBCity.TabIndex = 5
        '
        'txtBState
        '
        Me.txtBState.Location = New System.Drawing.Point(496, 232)
        Me.txtBState.Name = "txtBState"
        Me.txtBState.Size = New System.Drawing.Size(32, 20)
        Me.txtBState.TabIndex = 6
        '
        'txtBLocation
        '
        Me.txtBLocation.Location = New System.Drawing.Point(344, 192)
        Me.txtBLocation.Name = "txtBLocation"
        Me.txtBLocation.Size = New System.Drawing.Size(264, 20)
        Me.txtBLocation.TabIndex = 3
        '
        'tbUnits
        '
        Me.tbUnits.Controls.Add(Me.btnUpdateUnitGrid)
        Me.tbUnits.Controls.Add(Me.txtUnitEdOldType)
        Me.tbUnits.Controls.Add(Me.cbxUFixedSell)
        Me.tbUnits.Controls.Add(Me.btnCancelUnitLIneItem)
        Me.tbUnits.Controls.Add(Me.btnCopyUnit)
        Me.tbUnits.Controls.Add(Me.txtUnitEdOldLocation)
        Me.tbUnits.Controls.Add(Me.BubtnDeleteUnitLineItems)
        Me.tbUnits.Controls.Add(Me.txtUnitEdType)
        Me.tbUnits.Controls.Add(Me.txtUnitEdLocation)
        Me.tbUnits.Controls.Add(Me.cbxClearOnAdd)
        Me.tbUnits.Controls.Add(Me.lblLIType)
        Me.tbUnits.Controls.Add(Me.txtLIType)
        Me.tbUnits.Controls.Add(Me.UnitLocationLabel)
        Me.tbUnits.Controls.Add(Me.txtLILocation)
        Me.tbUnits.Controls.Add(Me.btnAddSO)
        Me.tbUnits.Controls.Add(Me.lblRT)
        Me.tbUnits.Controls.Add(Me.txtLIRT)
        Me.tbUnits.Controls.Add(Me.UnitLocation)
        Me.tbUnits.Controls.Add(Me.lblUnitDesc)
        Me.tbUnits.Controls.Add(Me.lblUnitType)
        Me.tbUnits.Controls.Add(Me.btnAddUnit)
        Me.tbUnits.Controls.Add(Me.txtBUnitDescription)
        Me.tbUnits.Controls.Add(Me.txtBUnitType)
        Me.tbUnits.Controls.Add(Me.SellLabel)
        Me.tbUnits.Controls.Add(Me.MarginLabel)
        Me.tbUnits.Controls.Add(Me.UnitDescriptionLabel)
        Me.tbUnits.Controls.Add(Me.CoLabel)
        Me.tbUnits.Controls.Add(Me.ProductIDLabel)
        Me.tbUnits.Controls.Add(Me.QuantityLabel)
        Me.tbUnits.Controls.Add(Me.cboCatalogMain)
        Me.tbUnits.Controls.Add(Me.btnAddItem)
        Me.tbUnits.Controls.Add(Me.txtLISell)
        Me.tbUnits.Controls.Add(Me.txtLIMargin)
        Me.tbUnits.Controls.Add(Me.txtLICost)
        Me.tbUnits.Controls.Add(Me.txtLIQuan)
        Me.tbUnits.Controls.Add(Me.txtLIProductID)
        Me.tbUnits.Controls.Add(Me.dgUnitLineItems)
        Me.tbUnits.Controls.Add(Me.btnNewUnit)
        Me.tbUnits.Controls.Add(Me.cboUnit)
        Me.tbUnits.Controls.Add(Me.txtLIDesc)
        Me.tbUnits.Controls.Add(Me.btnUpdateUnitLineItems)
        Me.tbUnits.Controls.Add(Me.txtUnitEdQuan)
        Me.tbUnits.Location = New System.Drawing.Point(4, 22)
        Me.tbUnits.Name = "tbUnits"
        Me.tbUnits.Size = New System.Drawing.Size(712, 382)
        Me.tbUnits.TabIndex = 1
        Me.tbUnits.Text = "Units"
        '
        'btnUpdateUnitGrid
        '
        Me.btnUpdateUnitGrid.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateUnitGrid.Location = New System.Drawing.Point(8, 120)
        Me.btnUpdateUnitGrid.Name = "btnUpdateUnitGrid"
        Me.btnUpdateUnitGrid.Size = New System.Drawing.Size(56, 16)
        Me.btnUpdateUnitGrid.TabIndex = 50
        Me.btnUpdateUnitGrid.TabStop = False
        Me.btnUpdateUnitGrid.Text = "Update"
        '
        'txtUnitEdOldType
        '
        Me.txtUnitEdOldType.Enabled = False
        Me.txtUnitEdOldType.Location = New System.Drawing.Point(344, 345)
        Me.txtUnitEdOldType.Name = "txtUnitEdOldType"
        Me.txtUnitEdOldType.Size = New System.Drawing.Size(72, 20)
        Me.txtUnitEdOldType.TabIndex = 49
        Me.txtUnitEdOldType.TabStop = False
        Me.txtUnitEdOldType.Text = "TextBox1"
        Me.txtUnitEdOldType.Visible = False
        '
        'cbxUFixedSell
        '
        Me.cbxUFixedSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUFixedSell.Location = New System.Drawing.Point(304, 48)
        Me.cbxUFixedSell.Name = "cbxUFixedSell"
        Me.cbxUFixedSell.Size = New System.Drawing.Size(48, 24)
        Me.cbxUFixedSell.TabIndex = 48
        Me.cbxUFixedSell.TabStop = False
        Me.cbxUFixedSell.Text = "Fixed Sell"
        '
        'btnCancelUnitLIneItem
        '
        Me.btnCancelUnitLIneItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelUnitLIneItem.Location = New System.Drawing.Point(656, 349)
        Me.btnCancelUnitLIneItem.Name = "btnCancelUnitLIneItem"
        Me.btnCancelUnitLIneItem.Size = New System.Drawing.Size(48, 16)
        Me.btnCancelUnitLIneItem.TabIndex = 47
        Me.btnCancelUnitLIneItem.TabStop = False
        Me.btnCancelUnitLIneItem.Text = "Cancel"
        Me.btnCancelUnitLIneItem.Visible = False
        '
        'btnCopyUnit
        '
        Me.btnCopyUnit.Location = New System.Drawing.Point(640, 16)
        Me.btnCopyUnit.Name = "btnCopyUnit"
        Me.btnCopyUnit.Size = New System.Drawing.Size(64, 24)
        Me.btnCopyUnit.TabIndex = 16
        Me.btnCopyUnit.Text = "Copy Unit"
        Me.btnCopyUnit.Visible = False
        '
        'txtUnitEdOldLocation
        '
        Me.txtUnitEdOldLocation.Enabled = False
        Me.txtUnitEdOldLocation.Location = New System.Drawing.Point(72, 345)
        Me.txtUnitEdOldLocation.Name = "txtUnitEdOldLocation"
        Me.txtUnitEdOldLocation.Size = New System.Drawing.Size(120, 20)
        Me.txtUnitEdOldLocation.TabIndex = 44
        Me.txtUnitEdOldLocation.TabStop = False
        Me.txtUnitEdOldLocation.Text = "oldLocation"
        Me.txtUnitEdOldLocation.Visible = False
        '
        'BubtnDeleteUnitLineItems
        '
        Me.BubtnDeleteUnitLineItems.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BubtnDeleteUnitLineItems.Location = New System.Drawing.Point(608, 349)
        Me.BubtnDeleteUnitLineItems.Name = "BubtnDeleteUnitLineItems"
        Me.BubtnDeleteUnitLineItems.Size = New System.Drawing.Size(48, 16)
        Me.BubtnDeleteUnitLineItems.TabIndex = 18
        Me.BubtnDeleteUnitLineItems.TabStop = False
        Me.BubtnDeleteUnitLineItems.Text = "Delete"
        Me.BubtnDeleteUnitLineItems.Visible = False
        '
        'txtUnitEdType
        '
        Me.txtUnitEdType.Location = New System.Drawing.Point(422, 345)
        Me.txtUnitEdType.Name = "txtUnitEdType"
        Me.txtUnitEdType.Size = New System.Drawing.Size(66, 20)
        Me.txtUnitEdType.TabIndex = 17
        Me.txtUnitEdType.TabStop = False
        Me.txtUnitEdType.Visible = False
        '
        'txtUnitEdLocation
        '
        Me.txtUnitEdLocation.Location = New System.Drawing.Point(204, 345)
        Me.txtUnitEdLocation.Name = "txtUnitEdLocation"
        Me.txtUnitEdLocation.Size = New System.Drawing.Size(128, 20)
        Me.txtUnitEdLocation.TabIndex = 16
        Me.txtUnitEdLocation.TabStop = False
        Me.txtUnitEdLocation.Visible = False
        '
        'cbxClearOnAdd
        '
        Me.cbxClearOnAdd.Checked = True
        Me.cbxClearOnAdd.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxClearOnAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxClearOnAdd.Location = New System.Drawing.Point(648, 48)
        Me.cbxClearOnAdd.Name = "cbxClearOnAdd"
        Me.cbxClearOnAdd.Size = New System.Drawing.Size(56, 24)
        Me.cbxClearOnAdd.TabIndex = 14
        Me.cbxClearOnAdd.TabStop = False
        Me.cbxClearOnAdd.Text = "Clear On Add"
        '
        'lblLIType
        '
        Me.lblLIType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLIType.Location = New System.Drawing.Point(400, 72)
        Me.lblLIType.Name = "lblLIType"
        Me.lblLIType.Size = New System.Drawing.Size(40, 16)
        Me.lblLIType.TabIndex = 37
        Me.lblLIType.Text = "Type"
        '
        'txtLIType
        '
        Me.txtLIType.Location = New System.Drawing.Point(392, 88)
        Me.txtLIType.Name = "txtLIType"
        Me.txtLIType.Size = New System.Drawing.Size(56, 20)
        Me.txtLIType.TabIndex = 8
        '
        'UnitLocationLabel
        '
        Me.UnitLocationLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UnitLocationLabel.Location = New System.Drawing.Point(248, 72)
        Me.UnitLocationLabel.Name = "UnitLocationLabel"
        Me.UnitLocationLabel.Size = New System.Drawing.Size(72, 16)
        Me.UnitLocationLabel.TabIndex = 35
        Me.UnitLocationLabel.Text = "Location"
        '
        'txtLILocation
        '
        Me.txtLILocation.AccessibleName = ""
        Me.txtLILocation.Location = New System.Drawing.Point(248, 88)
        Me.txtLILocation.Name = "txtLILocation"
        Me.txtLILocation.Size = New System.Drawing.Size(112, 20)
        Me.txtLILocation.TabIndex = 6
        '
        'btnAddSO
        '
        Me.btnAddSO.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddSO.Location = New System.Drawing.Point(640, 80)
        Me.btnAddSO.Name = "btnAddSO"
        Me.btnAddSO.Size = New System.Drawing.Size(56, 16)
        Me.btnAddSO.TabIndex = 12
        Me.btnAddSO.Text = "+SO"
        Me.btnAddSO.Visible = False
        '
        'lblRT
        '
        Me.lblRT.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRT.Location = New System.Drawing.Point(360, 72)
        Me.lblRT.Name = "lblRT"
        Me.lblRT.Size = New System.Drawing.Size(24, 16)
        Me.lblRT.TabIndex = 32
        Me.lblRT.Text = "R/T"
        '
        'txtLIRT
        '
        Me.txtLIRT.Location = New System.Drawing.Point(360, 88)
        Me.txtLIRT.Name = "txtLIRT"
        Me.txtLIRT.Size = New System.Drawing.Size(32, 20)
        Me.txtLIRT.TabIndex = 7
        '
        'UnitLocation
        '
        Me.UnitLocation.Controls.Add(Me.rbSO)
        Me.UnitLocation.Controls.Add(Me.rbBid)
        Me.UnitLocation.Controls.Add(Me.rbMain)
        Me.UnitLocation.Location = New System.Drawing.Point(16, 48)
        Me.UnitLocation.Name = "UnitLocation"
        Me.UnitLocation.Size = New System.Drawing.Size(272, 24)
        Me.UnitLocation.TabIndex = 2
        '
        'rbSO
        '
        Me.rbSO.Location = New System.Drawing.Point(96, 0)
        Me.rbSO.Name = "rbSO"
        Me.rbSO.Size = New System.Drawing.Size(96, 24)
        Me.rbSO.TabIndex = 1
        Me.rbSO.Text = "Special Order"
        '
        'rbBid
        '
        Me.rbBid.Location = New System.Drawing.Point(208, 0)
        Me.rbBid.Name = "rbBid"
        Me.rbBid.Size = New System.Drawing.Size(72, 24)
        Me.rbBid.TabIndex = 2
        Me.rbBid.Text = "Bid Items"
        '
        'rbMain
        '
        Me.rbMain.Checked = True
        Me.rbMain.Location = New System.Drawing.Point(16, 0)
        Me.rbMain.Name = "rbMain"
        Me.rbMain.Size = New System.Drawing.Size(80, 24)
        Me.rbMain.TabIndex = 0
        Me.rbMain.TabStop = True
        Me.rbMain.Text = "Catalog"
        '
        'lblUnitDesc
        '
        Me.lblUnitDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitDesc.Location = New System.Drawing.Point(312, 16)
        Me.lblUnitDesc.Name = "lblUnitDesc"
        Me.lblUnitDesc.Size = New System.Drawing.Size(56, 16)
        Me.lblUnitDesc.TabIndex = 29
        Me.lblUnitDesc.Text = "Description"
        Me.lblUnitDesc.Visible = False
        '
        'lblUnitType
        '
        Me.lblUnitType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitType.Location = New System.Drawing.Point(424, 24)
        Me.lblUnitType.Name = "lblUnitType"
        Me.lblUnitType.Size = New System.Drawing.Size(48, 16)
        Me.lblUnitType.TabIndex = 28
        Me.lblUnitType.Text = "Unit Type"
        Me.lblUnitType.Visible = False
        '
        'btnAddUnit
        '
        Me.btnAddUnit.Location = New System.Drawing.Point(560, 16)
        Me.btnAddUnit.Name = "btnAddUnit"
        Me.btnAddUnit.Size = New System.Drawing.Size(75, 23)
        Me.btnAddUnit.TabIndex = 15
        Me.btnAddUnit.Text = "Add Unit"
        Me.btnAddUnit.Visible = False
        '
        'txtBUnitDescription
        '
        Me.txtBUnitDescription.Location = New System.Drawing.Point(312, 16)
        Me.txtBUnitDescription.Name = "txtBUnitDescription"
        Me.txtBUnitDescription.Size = New System.Drawing.Size(232, 20)
        Me.txtBUnitDescription.TabIndex = 2
        Me.txtBUnitDescription.TabStop = False
        Me.txtBUnitDescription.Text = "Unit Description"
        Me.txtBUnitDescription.Visible = False
        '
        'txtBUnitType
        '
        Me.txtBUnitType.Location = New System.Drawing.Point(408, 24)
        Me.txtBUnitType.Name = "txtBUnitType"
        Me.txtBUnitType.Size = New System.Drawing.Size(64, 20)
        Me.txtBUnitType.TabIndex = 1
        Me.txtBUnitType.TabStop = False
        Me.txtBUnitType.Text = "Unit Type"
        Me.txtBUnitType.Visible = False
        '
        'SellLabel
        '
        Me.SellLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SellLabel.Location = New System.Drawing.Point(600, 72)
        Me.SellLabel.Name = "SellLabel"
        Me.SellLabel.Size = New System.Drawing.Size(24, 16)
        Me.SellLabel.TabIndex = 21
        Me.SellLabel.Text = "Sell"
        '
        'MarginLabel
        '
        Me.MarginLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MarginLabel.Location = New System.Drawing.Point(520, 72)
        Me.MarginLabel.Name = "MarginLabel"
        Me.MarginLabel.Size = New System.Drawing.Size(40, 16)
        Me.MarginLabel.TabIndex = 20
        Me.MarginLabel.Text = "Margin"
        '
        'UnitDescriptionLabel
        '
        Me.UnitDescriptionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UnitDescriptionLabel.Location = New System.Drawing.Point(128, 72)
        Me.UnitDescriptionLabel.Name = "UnitDescriptionLabel"
        Me.UnitDescriptionLabel.Size = New System.Drawing.Size(64, 16)
        Me.UnitDescriptionLabel.TabIndex = 18
        Me.UnitDescriptionLabel.Text = "Description"
        '
        'CoLabel
        '
        Me.CoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CoLabel.Location = New System.Drawing.Point(464, 72)
        Me.CoLabel.Name = "CoLabel"
        Me.CoLabel.Size = New System.Drawing.Size(24, 16)
        Me.CoLabel.TabIndex = 19
        Me.CoLabel.Text = "Cost"
        '
        'ProductIDLabel
        '
        Me.ProductIDLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProductIDLabel.Location = New System.Drawing.Point(48, 72)
        Me.ProductIDLabel.Name = "ProductIDLabel"
        Me.ProductIDLabel.Size = New System.Drawing.Size(72, 16)
        Me.ProductIDLabel.TabIndex = 17
        Me.ProductIDLabel.Text = "Product ID"
        '
        'QuantityLabel
        '
        Me.QuantityLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QuantityLabel.Location = New System.Drawing.Point(16, 72)
        Me.QuantityLabel.Name = "QuantityLabel"
        Me.QuantityLabel.Size = New System.Drawing.Size(32, 16)
        Me.QuantityLabel.TabIndex = 16
        Me.QuantityLabel.Text = "Quan"
        '
        'cboCatalogMain
        '
        Me.cboCatalogMain.Location = New System.Drawing.Point(360, 48)
        Me.cboCatalogMain.Name = "cboCatalogMain"
        Me.cboCatalogMain.Size = New System.Drawing.Size(280, 21)
        Me.cboCatalogMain.TabIndex = 2
        Me.cboCatalogMain.Text = "Catalog"
        '
        'btnAddItem
        '
        Me.btnAddItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddItem.Location = New System.Drawing.Point(640, 96)
        Me.btnAddItem.Name = "btnAddItem"
        Me.btnAddItem.Size = New System.Drawing.Size(56, 16)
        Me.btnAddItem.TabIndex = 13
        Me.btnAddItem.Text = "+ Bid"
        '
        'txtLISell
        '
        Me.txtLISell.Location = New System.Drawing.Point(560, 88)
        Me.txtLISell.Name = "txtLISell"
        Me.txtLISell.Size = New System.Drawing.Size(72, 20)
        Me.txtLISell.TabIndex = 11
        Me.txtLISell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLIMargin
        '
        Me.txtLIMargin.Location = New System.Drawing.Point(520, 88)
        Me.txtLIMargin.Name = "txtLIMargin"
        Me.txtLIMargin.Size = New System.Drawing.Size(40, 20)
        Me.txtLIMargin.TabIndex = 10
        Me.txtLIMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLICost
        '
        Me.txtLICost.Location = New System.Drawing.Point(448, 88)
        Me.txtLICost.Name = "txtLICost"
        Me.txtLICost.Size = New System.Drawing.Size(72, 20)
        Me.txtLICost.TabIndex = 9
        Me.txtLICost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLIQuan
        '
        Me.txtLIQuan.Location = New System.Drawing.Point(8, 88)
        Me.txtLIQuan.Name = "txtLIQuan"
        Me.txtLIQuan.Size = New System.Drawing.Size(40, 20)
        Me.txtLIQuan.TabIndex = 3
        Me.txtLIQuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLIProductID
        '
        Me.txtLIProductID.Location = New System.Drawing.Point(48, 88)
        Me.txtLIProductID.MaxLength = 25
        Me.txtLIProductID.Name = "txtLIProductID"
        Me.txtLIProductID.Size = New System.Drawing.Size(80, 20)
        Me.txtLIProductID.TabIndex = 4
        '
        'btnNewUnit
        '
        Me.btnNewUnit.Location = New System.Drawing.Point(208, 15)
        Me.btnNewUnit.Name = "btnNewUnit"
        Me.btnNewUnit.Size = New System.Drawing.Size(64, 24)
        Me.btnNewUnit.TabIndex = 1
        Me.btnNewUnit.Text = "Add / Edit"
        '
        'cboUnit
        '
        Me.cboUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnit.Location = New System.Drawing.Point(8, 16)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(200, 21)
        Me.cboUnit.TabIndex = 0
        '
        'txtLIDesc
        '
        Me.txtLIDesc.Location = New System.Drawing.Point(128, 88)
        Me.txtLIDesc.Name = "txtLIDesc"
        Me.txtLIDesc.Size = New System.Drawing.Size(120, 20)
        Me.txtLIDesc.TabIndex = 5
        '
        'btnUpdateUnitLineItems
        '
        Me.btnUpdateUnitLineItems.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateUnitLineItems.Location = New System.Drawing.Point(552, 349)
        Me.btnUpdateUnitLineItems.Name = "btnUpdateUnitLineItems"
        Me.btnUpdateUnitLineItems.Size = New System.Drawing.Size(48, 16)
        Me.btnUpdateUnitLineItems.TabIndex = 19
        Me.btnUpdateUnitLineItems.TabStop = False
        Me.btnUpdateUnitLineItems.Text = "Update"
        Me.btnUpdateUnitLineItems.Visible = False
        '
        'txtUnitEdQuan
        '
        Me.txtUnitEdQuan.Location = New System.Drawing.Point(8, 345)
        Me.txtUnitEdQuan.Name = "txtUnitEdQuan"
        Me.txtUnitEdQuan.Size = New System.Drawing.Size(40, 20)
        Me.txtUnitEdQuan.TabIndex = 15
        Me.txtUnitEdQuan.TabStop = False
        Me.txtUnitEdQuan.Visible = False
        '
        'tbAlternates
        '
        Me.tbAlternates.Controls.Add(Me.btnAltAddSO)
        Me.tbAlternates.Controls.Add(Me.AlternateLabel)
        Me.tbAlternates.Controls.Add(Me.AlternateDescriptionLabel)
        Me.tbAlternates.Controls.Add(Me.PartNumberLabel)
        Me.tbAlternates.Controls.Add(Me.txtAltDesc)
        Me.tbAlternates.Controls.Add(Me.txtAltProdID)
        Me.tbAlternates.Controls.Add(Me.btnAltCancel)
        Me.tbAlternates.Controls.Add(Me.btnAltDelete)
        Me.tbAlternates.Controls.Add(Me.lblAltSell)
        Me.tbAlternates.Controls.Add(Me.lblAltMargin)
        Me.tbAlternates.Controls.Add(Me.lblAltCost)
        Me.tbAlternates.Controls.Add(Me.txtAltSell)
        Me.tbAlternates.Controls.Add(Me.txtAltMargin)
        Me.tbAlternates.Controls.Add(Me.txtAltCost)
        Me.tbAlternates.Controls.Add(Me.dgAlt)
        Me.tbAlternates.Controls.Add(Me.btnAltAdd)
        Me.tbAlternates.Controls.Add(Me.rbAltSO)
        Me.tbAlternates.Controls.Add(Me.rbAltMain)
        Me.tbAlternates.Controls.Add(Me.lblAltCatMain)
        Me.tbAlternates.Controls.Add(Me.cboAltCatMain)
        Me.tbAlternates.Controls.Add(Me.IIBLabel)
        Me.tbAlternates.Controls.Add(Me.cboAltBidItems)
        Me.tbAlternates.Location = New System.Drawing.Point(4, 22)
        Me.tbAlternates.Name = "tbAlternates"
        Me.tbAlternates.Size = New System.Drawing.Size(712, 382)
        Me.tbAlternates.TabIndex = 6
        Me.tbAlternates.Text = "Alternates"
        '
        'btnAltAddSO
        '
        Me.btnAltAddSO.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAltAddSO.Location = New System.Drawing.Point(672, 64)
        Me.btnAltAddSO.Name = "btnAltAddSO"
        Me.btnAltAddSO.Size = New System.Drawing.Size(32, 16)
        Me.btnAltAddSO.TabIndex = 21
        Me.btnAltAddSO.Text = "+ SO"
        '
        'AlternateLabel
        '
        Me.AlternateLabel.Location = New System.Drawing.Point(80, 64)
        Me.AlternateLabel.Name = "AlternateLabel"
        Me.AlternateLabel.Size = New System.Drawing.Size(64, 16)
        Me.AlternateLabel.TabIndex = 20
        Me.AlternateLabel.Text = "Alternate"
        '
        'AlternateDescriptionLabel
        '
        Me.AlternateDescriptionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AlternateDescriptionLabel.Location = New System.Drawing.Point(288, 48)
        Me.AlternateDescriptionLabel.Name = "AlternateDescriptionLabel"
        Me.AlternateDescriptionLabel.Size = New System.Drawing.Size(96, 16)
        Me.AlternateDescriptionLabel.TabIndex = 19
        Me.AlternateDescriptionLabel.Text = "Description"
        '
        'PartNumberLabel
        '
        Me.PartNumberLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PartNumberLabel.Location = New System.Drawing.Point(144, 48)
        Me.PartNumberLabel.Name = "PartNumberLabel"
        Me.PartNumberLabel.Size = New System.Drawing.Size(80, 16)
        Me.PartNumberLabel.TabIndex = 18
        Me.PartNumberLabel.Text = "Part Number"
        '
        'txtAltDesc
        '
        Me.txtAltDesc.Location = New System.Drawing.Point(288, 64)
        Me.txtAltDesc.Name = "txtAltDesc"
        Me.txtAltDesc.Size = New System.Drawing.Size(176, 20)
        Me.txtAltDesc.TabIndex = 3
        '
        'txtAltProdID
        '
        Me.txtAltProdID.Location = New System.Drawing.Point(144, 64)
        Me.txtAltProdID.Name = "txtAltProdID"
        Me.txtAltProdID.Size = New System.Drawing.Size(144, 20)
        Me.txtAltProdID.TabIndex = 2
        '
        'btnAltCancel
        '
        Me.btnAltCancel.Location = New System.Drawing.Point(608, 341)
        Me.btnAltCancel.Name = "btnAltCancel"
        Me.btnAltCancel.Size = New System.Drawing.Size(64, 19)
        Me.btnAltCancel.TabIndex = 15
        Me.btnAltCancel.Text = "Cancel"
        Me.btnAltCancel.Visible = False
        '
        'btnAltDelete
        '
        Me.btnAltDelete.Location = New System.Drawing.Point(544, 341)
        Me.btnAltDelete.Name = "btnAltDelete"
        Me.btnAltDelete.Size = New System.Drawing.Size(64, 19)
        Me.btnAltDelete.TabIndex = 14
        Me.btnAltDelete.Text = "Delete"
        Me.btnAltDelete.Visible = False
        '
        'lblAltSell
        '
        Me.lblAltSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltSell.Location = New System.Drawing.Point(640, 48)
        Me.lblAltSell.Name = "lblAltSell"
        Me.lblAltSell.Size = New System.Drawing.Size(32, 16)
        Me.lblAltSell.TabIndex = 13
        Me.lblAltSell.Text = "Sell"
        '
        'lblAltMargin
        '
        Me.lblAltMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltMargin.Location = New System.Drawing.Point(560, 48)
        Me.lblAltMargin.Name = "lblAltMargin"
        Me.lblAltMargin.Size = New System.Drawing.Size(48, 16)
        Me.lblAltMargin.TabIndex = 12
        Me.lblAltMargin.Text = "Margin"
        '
        'lblAltCost
        '
        Me.lblAltCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltCost.Location = New System.Drawing.Point(512, 48)
        Me.lblAltCost.Name = "lblAltCost"
        Me.lblAltCost.Size = New System.Drawing.Size(32, 16)
        Me.lblAltCost.TabIndex = 11
        Me.lblAltCost.Text = "Cost"
        '
        'txtAltSell
        '
        Me.txtAltSell.Location = New System.Drawing.Point(592, 64)
        Me.txtAltSell.Name = "txtAltSell"
        Me.txtAltSell.Size = New System.Drawing.Size(80, 20)
        Me.txtAltSell.TabIndex = 6
        Me.txtAltSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAltMargin
        '
        Me.txtAltMargin.Location = New System.Drawing.Point(544, 64)
        Me.txtAltMargin.Name = "txtAltMargin"
        Me.txtAltMargin.Size = New System.Drawing.Size(48, 20)
        Me.txtAltMargin.TabIndex = 5
        Me.txtAltMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAltCost
        '
        Me.txtAltCost.Location = New System.Drawing.Point(464, 64)
        Me.txtAltCost.Name = "txtAltCost"
        Me.txtAltCost.Size = New System.Drawing.Size(80, 20)
        Me.txtAltCost.TabIndex = 4
        Me.txtAltCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgAlt
        '
        Me.dgAlt.CaptionVisible = False
        Me.dgAlt.DataMember = ""
        Me.dgAlt.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgAlt.Location = New System.Drawing.Point(32, 96)
        Me.dgAlt.Name = "dgAlt"
        Me.dgAlt.ReadOnly = True
        Me.dgAlt.Size = New System.Drawing.Size(645, 240)
        Me.dgAlt.TabIndex = 7
        Me.dgAlt.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgStyleAlt})
        '
        'dgStyleAlt
        '
        Me.dgStyleAlt.DataGrid = Me.dgAlt
        Me.dgStyleAlt.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgAltAltID, Me.dgAltOProdID, Me.dgAltODesc, Me.dgAltOSell, Me.dgAltQuan, Me.dgAltAProdID, Me.dgAltADesc, Me.dgAltASell})
        Me.dgStyleAlt.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgStyleAlt.MappingName = "LineItems"
        Me.dgStyleAlt.ReadOnly = True
        '
        'dgAltAltID
        '
        Me.dgAltAltID.Format = ""
        Me.dgAltAltID.FormatInfo = Nothing
        Me.dgAltAltID.MappingName = "AltID"
        Me.dgAltAltID.Width = 0
        '
        'dgAltOProdID
        '
        Me.dgAltOProdID.Format = ""
        Me.dgAltOProdID.FormatInfo = Nothing
        Me.dgAltOProdID.HeaderText = "Org Part Num"
        Me.dgAltOProdID.MappingName = "OrgProductID"
        Me.dgAltOProdID.NullText = ""
        Me.dgAltOProdID.ReadOnly = True
        Me.dgAltOProdID.Width = 75
        '
        'dgAltODesc
        '
        Me.dgAltODesc.Format = ""
        Me.dgAltODesc.FormatInfo = Nothing
        Me.dgAltODesc.HeaderText = "Orig Description"
        Me.dgAltODesc.MappingName = "OrgDescription"
        Me.dgAltODesc.NullText = ""
        Me.dgAltODesc.ReadOnly = True
        Me.dgAltODesc.Width = 125
        '
        'dgAltOSell
        '
        Me.dgAltOSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgAltOSell.Format = "C"
        Me.dgAltOSell.FormatInfo = Nothing
        Me.dgAltOSell.HeaderText = "Orig Sell"
        Me.dgAltOSell.MappingName = "Sell"
        Me.dgAltOSell.NullText = ""
        Me.dgAltOSell.Width = 65
        '
        'dgAltQuan
        '
        Me.dgAltQuan.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgAltQuan.Format = ""
        Me.dgAltQuan.FormatInfo = Nothing
        Me.dgAltQuan.HeaderText = "Quantity"
        Me.dgAltQuan.MappingName = "QUAN"
        Me.dgAltQuan.NullText = ""
        Me.dgAltQuan.ReadOnly = True
        Me.dgAltQuan.Width = 75
        '
        'dgAltAProdID
        '
        Me.dgAltAProdID.Format = ""
        Me.dgAltAProdID.FormatInfo = Nothing
        Me.dgAltAProdID.HeaderText = "Alt Part Num"
        Me.dgAltAProdID.MappingName = "AltProductID"
        Me.dgAltAProdID.NullText = ""
        Me.dgAltAProdID.ReadOnly = True
        Me.dgAltAProdID.Width = 75
        '
        'dgAltADesc
        '
        Me.dgAltADesc.Format = ""
        Me.dgAltADesc.FormatInfo = Nothing
        Me.dgAltADesc.HeaderText = "Alt Description"
        Me.dgAltADesc.MappingName = "AltDescription"
        Me.dgAltADesc.NullText = ""
        Me.dgAltADesc.ReadOnly = True
        Me.dgAltADesc.Width = 125
        '
        'dgAltASell
        '
        Me.dgAltASell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.dgAltASell.Format = "C"
        Me.dgAltASell.FormatInfo = Nothing
        Me.dgAltASell.HeaderText = "Alt Sell"
        Me.dgAltASell.MappingName = "AltSell"
        Me.dgAltASell.NullText = ""
        Me.dgAltASell.ReadOnly = True
        Me.dgAltASell.Width = 65
        '
        'btnAltAdd
        '
        Me.btnAltAdd.Location = New System.Drawing.Point(624, 16)
        Me.btnAltAdd.Name = "btnAltAdd"
        Me.btnAltAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAltAdd.TabIndex = 7
        Me.btnAltAdd.Text = "Add Alt"
        '
        'rbAltSO
        '
        Me.rbAltSO.Location = New System.Drawing.Point(352, 16)
        Me.rbAltSO.Name = "rbAltSO"
        Me.rbAltSO.Size = New System.Drawing.Size(48, 24)
        Me.rbAltSO.TabIndex = 5
        Me.rbAltSO.Text = "SO"
        '
        'rbAltMain
        '
        Me.rbAltMain.Checked = True
        Me.rbAltMain.Location = New System.Drawing.Point(272, 16)
        Me.rbAltMain.Name = "rbAltMain"
        Me.rbAltMain.Size = New System.Drawing.Size(64, 24)
        Me.rbAltMain.TabIndex = 4
        Me.rbAltMain.TabStop = True
        Me.rbAltMain.Text = "Catalog"
        '
        'lblAltCatMain
        '
        Me.lblAltCatMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltCatMain.Location = New System.Drawing.Point(400, 0)
        Me.lblAltCatMain.Name = "lblAltCatMain"
        Me.lblAltCatMain.Size = New System.Drawing.Size(80, 16)
        Me.lblAltCatMain.TabIndex = 3
        Me.lblAltCatMain.Text = "Catalog Items"
        '
        'cboAltCatMain
        '
        Me.cboAltCatMain.Location = New System.Drawing.Point(400, 16)
        Me.cboAltCatMain.Name = "cboAltCatMain"
        Me.cboAltCatMain.Size = New System.Drawing.Size(208, 21)
        Me.cboAltCatMain.TabIndex = 1
        '
        'IIBLabel
        '
        Me.IIBLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IIBLabel.Location = New System.Drawing.Point(32, 0)
        Me.IIBLabel.Name = "IIBLabel"
        Me.IIBLabel.Size = New System.Drawing.Size(64, 16)
        Me.IIBLabel.TabIndex = 1
        Me.IIBLabel.Text = "Items In Bid"
        '
        'cboAltBidItems
        '
        Me.cboAltBidItems.Location = New System.Drawing.Point(32, 16)
        Me.cboAltBidItems.Name = "cboAltBidItems"
        Me.cboAltBidItems.Size = New System.Drawing.Size(224, 21)
        Me.cboAltBidItems.TabIndex = 0
        '
        'tbBuilding
        '
        Me.tbBuilding.Controls.Add(Me.UnitCountLabel)
        Me.tbBuilding.Controls.Add(Me.txtUnitCount)
        Me.tbBuilding.Controls.Add(Me.btnBldgLICancel)
        Me.tbBuilding.Controls.Add(Me.btnBldgLIDelete)
        Me.tbBuilding.Controls.Add(Me.btnBldgLIUpdate)
        Me.tbBuilding.Controls.Add(Me.txtBldgEdUnitID)
        Me.tbBuilding.Controls.Add(Me.txtBldgEdBldgID)
        Me.tbBuilding.Controls.Add(Me.txtBldgEDQuan)
        Me.tbBuilding.Controls.Add(Me.lblBldgShipSeq)
        Me.tbBuilding.Controls.Add(Me.txtBldgShipSeq)
        Me.tbBuilding.Controls.Add(Me.lblBldgNum)
        Me.tbBuilding.Controls.Add(Me.txtBldgNum)
        Me.tbBuilding.Controls.Add(Me.lblBldgSelectBldg)
        Me.tbBuilding.Controls.Add(Me.lblBldgSelUnit)
        Me.tbBuilding.Controls.Add(Me.lblBldgShipDate)
        Me.tbBuilding.Controls.Add(Me.lblBldgNotes)
        Me.tbBuilding.Controls.Add(Me.lblBldgAddress)
        Me.tbBuilding.Controls.Add(Me.lblBldgLocation)
        Me.tbBuilding.Controls.Add(Me.lblBldgDesc)
        Me.tbBuilding.Controls.Add(Me.lblBldgType)
        Me.tbBuilding.Controls.Add(Me.txtBldgNotes)
        Me.tbBuilding.Controls.Add(Me.txtBldgAddress)
        Me.tbBuilding.Controls.Add(Me.txtBldgLocation)
        Me.tbBuilding.Controls.Add(Me.btnAddUnitToBldg)
        Me.tbBuilding.Controls.Add(Me.lblBldgUnitQuan)
        Me.tbBuilding.Controls.Add(Me.txtBldgUnitQuan)
        Me.tbBuilding.Controls.Add(Me.cboBldgUnits)
        Me.tbBuilding.Controls.Add(Me.txtBldgShipDate)
        Me.tbBuilding.Controls.Add(Me.dgBldg)
        Me.tbBuilding.Controls.Add(Me.txtbldgDesc)
        Me.tbBuilding.Controls.Add(Me.txtbldgType)
        Me.tbBuilding.Controls.Add(Me.btnAddBldg)
        Me.tbBuilding.Controls.Add(Me.cboBldg)
        Me.tbBuilding.Location = New System.Drawing.Point(4, 22)
        Me.tbBuilding.Name = "tbBuilding"
        Me.tbBuilding.Size = New System.Drawing.Size(712, 382)
        Me.tbBuilding.TabIndex = 2
        Me.tbBuilding.Text = "Building"
        '
        'UnitCountLabel
        '
        Me.UnitCountLabel.Location = New System.Drawing.Point(16, 360)
        Me.UnitCountLabel.Name = "UnitCountLabel"
        Me.UnitCountLabel.Size = New System.Drawing.Size(64, 20)
        Me.UnitCountLabel.TabIndex = 30
        Me.UnitCountLabel.Text = "Unit Count"
        '
        'txtUnitCount
        '
        Me.txtUnitCount.Location = New System.Drawing.Point(88, 360)
        Me.txtUnitCount.Name = "txtUnitCount"
        Me.txtUnitCount.Size = New System.Drawing.Size(40, 20)
        Me.txtUnitCount.TabIndex = 29
        '
        'btnBldgLICancel
        '
        Me.btnBldgLICancel.Location = New System.Drawing.Point(416, 336)
        Me.btnBldgLICancel.Name = "btnBldgLICancel"
        Me.btnBldgLICancel.Size = New System.Drawing.Size(56, 23)
        Me.btnBldgLICancel.TabIndex = 28
        Me.btnBldgLICancel.Text = "Cancel"
        Me.btnBldgLICancel.Visible = False
        '
        'btnBldgLIDelete
        '
        Me.btnBldgLIDelete.Location = New System.Drawing.Point(304, 336)
        Me.btnBldgLIDelete.Name = "btnBldgLIDelete"
        Me.btnBldgLIDelete.Size = New System.Drawing.Size(56, 23)
        Me.btnBldgLIDelete.TabIndex = 10
        Me.btnBldgLIDelete.Text = "Delete"
        Me.btnBldgLIDelete.Visible = False
        '
        'btnBldgLIUpdate
        '
        Me.btnBldgLIUpdate.Location = New System.Drawing.Point(360, 336)
        Me.btnBldgLIUpdate.Name = "btnBldgLIUpdate"
        Me.btnBldgLIUpdate.Size = New System.Drawing.Size(56, 23)
        Me.btnBldgLIUpdate.TabIndex = 11
        Me.btnBldgLIUpdate.Text = "Update"
        Me.btnBldgLIUpdate.Visible = False
        '
        'txtBldgEdUnitID
        '
        Me.txtBldgEdUnitID.Location = New System.Drawing.Point(176, 336)
        Me.txtBldgEdUnitID.Name = "txtBldgEdUnitID"
        Me.txtBldgEdUnitID.Size = New System.Drawing.Size(64, 20)
        Me.txtBldgEdUnitID.TabIndex = 27
        Me.txtBldgEdUnitID.Text = "UnitID"
        Me.txtBldgEdUnitID.Visible = False
        '
        'txtBldgEdBldgID
        '
        Me.txtBldgEdBldgID.Location = New System.Drawing.Point(112, 336)
        Me.txtBldgEdBldgID.Name = "txtBldgEdBldgID"
        Me.txtBldgEdBldgID.Size = New System.Drawing.Size(48, 20)
        Me.txtBldgEdBldgID.TabIndex = 26
        Me.txtBldgEdBldgID.Text = "BldgID"
        Me.txtBldgEdBldgID.Visible = False
        '
        'txtBldgEDQuan
        '
        Me.txtBldgEDQuan.Location = New System.Drawing.Point(48, 336)
        Me.txtBldgEDQuan.Name = "txtBldgEDQuan"
        Me.txtBldgEDQuan.Size = New System.Drawing.Size(29, 20)
        Me.txtBldgEDQuan.TabIndex = 9
        Me.txtBldgEDQuan.Visible = False
        '
        'lblBldgShipSeq
        '
        Me.lblBldgShipSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgShipSeq.Location = New System.Drawing.Point(640, 48)
        Me.lblBldgShipSeq.Name = "lblBldgShipSeq"
        Me.lblBldgShipSeq.Size = New System.Drawing.Size(48, 16)
        Me.lblBldgShipSeq.TabIndex = 24
        Me.lblBldgShipSeq.Text = "Ship Seq"
        '
        'txtBldgShipSeq
        '
        Me.txtBldgShipSeq.Location = New System.Drawing.Point(648, 64)
        Me.txtBldgShipSeq.Name = "txtBldgShipSeq"
        Me.txtBldgShipSeq.Size = New System.Drawing.Size(32, 20)
        Me.txtBldgShipSeq.TabIndex = 8
        '
        'lblBldgNum
        '
        Me.lblBldgNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgNum.Location = New System.Drawing.Point(280, 48)
        Me.lblBldgNum.Name = "lblBldgNum"
        Me.lblBldgNum.Size = New System.Drawing.Size(16, 16)
        Me.lblBldgNum.TabIndex = 22
        Me.lblBldgNum.Text = "#"
        '
        'txtBldgNum
        '
        Me.txtBldgNum.Location = New System.Drawing.Point(272, 64)
        Me.txtBldgNum.Name = "txtBldgNum"
        Me.txtBldgNum.Size = New System.Drawing.Size(24, 20)
        Me.txtBldgNum.TabIndex = 4
        '
        'lblBldgSelectBldg
        '
        Me.lblBldgSelectBldg.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgSelectBldg.Location = New System.Drawing.Point(16, 8)
        Me.lblBldgSelectBldg.Name = "lblBldgSelectBldg"
        Me.lblBldgSelectBldg.Size = New System.Drawing.Size(100, 16)
        Me.lblBldgSelectBldg.TabIndex = 20
        Me.lblBldgSelectBldg.Text = "Select Building"
        '
        'lblBldgSelUnit
        '
        Me.lblBldgSelUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgSelUnit.Location = New System.Drawing.Point(304, 8)
        Me.lblBldgSelUnit.Name = "lblBldgSelUnit"
        Me.lblBldgSelUnit.Size = New System.Drawing.Size(56, 16)
        Me.lblBldgSelUnit.TabIndex = 19
        Me.lblBldgSelUnit.Text = "Select Unit"
        Me.lblBldgSelUnit.Visible = False
        '
        'lblBldgShipDate
        '
        Me.lblBldgShipDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgShipDate.Location = New System.Drawing.Point(544, 48)
        Me.lblBldgShipDate.Name = "lblBldgShipDate"
        Me.lblBldgShipDate.Size = New System.Drawing.Size(56, 16)
        Me.lblBldgShipDate.TabIndex = 18
        Me.lblBldgShipDate.Text = "Ship Date"
        '
        'lblBldgNotes
        '
        Me.lblBldgNotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgNotes.Location = New System.Drawing.Point(472, 96)
        Me.lblBldgNotes.Name = "lblBldgNotes"
        Me.lblBldgNotes.Size = New System.Drawing.Size(56, 16)
        Me.lblBldgNotes.TabIndex = 17
        Me.lblBldgNotes.Text = "Notes"
        '
        'lblBldgAddress
        '
        Me.lblBldgAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgAddress.Location = New System.Drawing.Point(432, 48)
        Me.lblBldgAddress.Name = "lblBldgAddress"
        Me.lblBldgAddress.Size = New System.Drawing.Size(64, 16)
        Me.lblBldgAddress.TabIndex = 16
        Me.lblBldgAddress.Text = "Address"
        '
        'lblBldgLocation
        '
        Me.lblBldgLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgLocation.Location = New System.Drawing.Point(304, 48)
        Me.lblBldgLocation.Name = "lblBldgLocation"
        Me.lblBldgLocation.Size = New System.Drawing.Size(72, 16)
        Me.lblBldgLocation.TabIndex = 15
        Me.lblBldgLocation.Text = "Location"
        '
        'lblBldgDesc
        '
        Me.lblBldgDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgDesc.Location = New System.Drawing.Point(120, 48)
        Me.lblBldgDesc.Name = "lblBldgDesc"
        Me.lblBldgDesc.Size = New System.Drawing.Size(80, 16)
        Me.lblBldgDesc.TabIndex = 14
        Me.lblBldgDesc.Text = "Description"
        '
        'lblBldgType
        '
        Me.lblBldgType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgType.Location = New System.Drawing.Point(16, 48)
        Me.lblBldgType.Name = "lblBldgType"
        Me.lblBldgType.Size = New System.Drawing.Size(80, 16)
        Me.lblBldgType.TabIndex = 13
        Me.lblBldgType.Text = "Type"
        '
        'txtBldgNotes
        '
        Me.txtBldgNotes.Location = New System.Drawing.Point(473, 112)
        Me.txtBldgNotes.Multiline = True
        Me.txtBldgNotes.Name = "txtBldgNotes"
        Me.txtBldgNotes.Size = New System.Drawing.Size(176, 224)
        Me.txtBldgNotes.TabIndex = 9
        '
        'txtBldgAddress
        '
        Me.txtBldgAddress.Location = New System.Drawing.Point(432, 64)
        Me.txtBldgAddress.Name = "txtBldgAddress"
        Me.txtBldgAddress.Size = New System.Drawing.Size(105, 20)
        Me.txtBldgAddress.TabIndex = 6
        '
        'txtBldgLocation
        '
        Me.txtBldgLocation.Location = New System.Drawing.Point(304, 64)
        Me.txtBldgLocation.Name = "txtBldgLocation"
        Me.txtBldgLocation.Size = New System.Drawing.Size(128, 20)
        Me.txtBldgLocation.TabIndex = 5
        '
        'btnAddUnitToBldg
        '
        Me.btnAddUnitToBldg.Location = New System.Drawing.Point(552, 24)
        Me.btnAddUnitToBldg.Name = "btnAddUnitToBldg"
        Me.btnAddUnitToBldg.Size = New System.Drawing.Size(104, 23)
        Me.btnAddUnitToBldg.TabIndex = 12
        Me.btnAddUnitToBldg.Text = "Add Units"
        '
        'lblBldgUnitQuan
        '
        Me.lblBldgUnitQuan.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBldgUnitQuan.Location = New System.Drawing.Point(496, 8)
        Me.lblBldgUnitQuan.Name = "lblBldgUnitQuan"
        Me.lblBldgUnitQuan.Size = New System.Drawing.Size(32, 16)
        Me.lblBldgUnitQuan.TabIndex = 8
        Me.lblBldgUnitQuan.Text = "Quan"
        Me.lblBldgUnitQuan.Visible = False
        '
        'txtBldgUnitQuan
        '
        Me.txtBldgUnitQuan.Location = New System.Drawing.Point(496, 24)
        Me.txtBldgUnitQuan.Name = "txtBldgUnitQuan"
        Me.txtBldgUnitQuan.Size = New System.Drawing.Size(40, 20)
        Me.txtBldgUnitQuan.TabIndex = 11
        Me.txtBldgUnitQuan.Visible = False
        '
        'cboBldgUnits
        '
        Me.cboBldgUnits.Location = New System.Drawing.Point(304, 24)
        Me.cboBldgUnits.Name = "cboBldgUnits"
        Me.cboBldgUnits.Size = New System.Drawing.Size(192, 21)
        Me.cboBldgUnits.TabIndex = 10
        Me.cboBldgUnits.Visible = False
        '
        'txtBldgShipDate
        '
        Me.txtBldgShipDate.Location = New System.Drawing.Point(544, 64)
        Me.txtBldgShipDate.Name = "txtBldgShipDate"
        Me.txtBldgShipDate.Size = New System.Drawing.Size(100, 20)
        Me.txtBldgShipDate.TabIndex = 7
        '
        'txtbldgDesc
        '
        Me.txtbldgDesc.Location = New System.Drawing.Point(120, 64)
        Me.txtbldgDesc.Name = "txtbldgDesc"
        Me.txtbldgDesc.Size = New System.Drawing.Size(152, 20)
        Me.txtbldgDesc.TabIndex = 3
        '
        'txtbldgType
        '
        Me.txtbldgType.Location = New System.Drawing.Point(16, 64)
        Me.txtbldgType.Name = "txtbldgType"
        Me.txtbldgType.Size = New System.Drawing.Size(100, 20)
        Me.txtbldgType.TabIndex = 2
        '
        'btnAddBldg
        '
        Me.btnAddBldg.Location = New System.Drawing.Point(240, 25)
        Me.btnAddBldg.Name = "btnAddBldg"
        Me.btnAddBldg.Size = New System.Drawing.Size(56, 22)
        Me.btnAddBldg.TabIndex = 1
        Me.btnAddBldg.Text = "Add/Edit"
        '
        'cboBldg
        '
        Me.cboBldg.Location = New System.Drawing.Point(16, 24)
        Me.cboBldg.Name = "cboBldg"
        Me.cboBldg.Size = New System.Drawing.Size(224, 21)
        Me.cboBldg.TabIndex = 0
        '
        'tbLIBulkEdit
        '
        Me.tbLIBulkEdit.Controls.Add(Me.btnBERDByType)
        Me.tbLIBulkEdit.Controls.Add(Me.ReplaceWithLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.ReplaceThisLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.btnUpdatePrice)
        Me.tbLIBulkEdit.Controls.Add(Me.BESellLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.BEMarginLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.BECostLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.BETypeLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.RTLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.BEDescriptionLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.BEProductIDLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERSell)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERMargin)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERCost)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERType)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERRT)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERDesc)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBERProdID)
        Me.tbLIBulkEdit.Controls.Add(Me.btnBEAdjustSell)
        Me.tbLIBulkEdit.Controls.Add(Me.TypesIncludedLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.btnBERByType)
        Me.tbLIBulkEdit.Controls.Add(Me.btnBERByPartNum)
        Me.tbLIBulkEdit.Controls.Add(Me.btnBEDelete)
        Me.tbLIBulkEdit.Controls.Add(Me.cboBEType)
        Me.tbLIBulkEdit.Controls.Add(Me.CurrentBidLabel)
        Me.tbLIBulkEdit.Controls.Add(Me.cbxFSell)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBMargin)
        Me.tbLIBulkEdit.Controls.Add(Me.BEMarginLabel2)
        Me.tbLIBulkEdit.Controls.Add(Me.BESellLabel2)
        Me.tbLIBulkEdit.Controls.Add(Me.BECostLabel2)
        Me.tbLIBulkEdit.Controls.Add(Me.lblBEBType)
        Me.tbLIBulkEdit.Controls.Add(Me.RTLabel2)
        Me.tbLIBulkEdit.Controls.Add(Me.BEDescription2)
        Me.tbLIBulkEdit.Controls.Add(Me.BEProductIDLabel2)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBSell)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBCost)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBType)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBRT)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBDesc)
        Me.tbLIBulkEdit.Controls.Add(Me.txtBEBProdID)
        Me.tbLIBulkEdit.Controls.Add(Me.rbBESO)
        Me.tbLIBulkEdit.Controls.Add(Me.rbBEMain)
        Me.tbLIBulkEdit.Controls.Add(Me.cboBECatalog)
        Me.tbLIBulkEdit.Controls.Add(Me.cboBEBidItems)
        Me.tbLIBulkEdit.Location = New System.Drawing.Point(4, 22)
        Me.tbLIBulkEdit.Name = "tbLIBulkEdit"
        Me.tbLIBulkEdit.Size = New System.Drawing.Size(712, 382)
        Me.tbLIBulkEdit.TabIndex = 5
        Me.tbLIBulkEdit.Text = "Bulk Edits"
        '
        'btnBERDByType
        '
        Me.btnBERDByType.Location = New System.Drawing.Point(292, 136)
        Me.btnBERDByType.Name = "btnBERDByType"
        Me.btnBERDByType.Size = New System.Drawing.Size(120, 24)
        Me.btnBERDByType.TabIndex = 44
        Me.btnBERDByType.Text = "Delete By Type"
        '
        'ReplaceWithLabel
        '
        Me.ReplaceWithLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReplaceWithLabel.Location = New System.Drawing.Point(24, 232)
        Me.ReplaceWithLabel.Name = "ReplaceWithLabel"
        Me.ReplaceWithLabel.Size = New System.Drawing.Size(200, 16)
        Me.ReplaceWithLabel.TabIndex = 43
        Me.ReplaceWithLabel.Text = "With This One -------------"
        '
        'ReplaceThisLabel
        '
        Me.ReplaceThisLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReplaceThisLabel.Location = New System.Drawing.Point(24, 56)
        Me.ReplaceThisLabel.Name = "ReplaceThisLabel"
        Me.ReplaceThisLabel.Size = New System.Drawing.Size(256, 23)
        Me.ReplaceThisLabel.TabIndex = 42
        Me.ReplaceThisLabel.Text = "Replace This Item -----------"
        '
        'btnUpdatePrice
        '
        Me.btnUpdatePrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 5.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdatePrice.Location = New System.Drawing.Point(680, 96)
        Me.btnUpdatePrice.Name = "btnUpdatePrice"
        Me.btnUpdatePrice.Size = New System.Drawing.Size(24, 16)
        Me.btnUpdatePrice.TabIndex = 7
        Me.btnUpdatePrice.Text = "Calc"
        '
        'BESellLabel
        '
        Me.BESellLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BESellLabel.Location = New System.Drawing.Point(640, 256)
        Me.BESellLabel.Name = "BESellLabel"
        Me.BESellLabel.Size = New System.Drawing.Size(24, 16)
        Me.BESellLabel.TabIndex = 40
        Me.BESellLabel.Text = "Sell"
        '
        'BEMarginLabel
        '
        Me.BEMarginLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEMarginLabel.Location = New System.Drawing.Point(520, 256)
        Me.BEMarginLabel.Name = "BEMarginLabel"
        Me.BEMarginLabel.Size = New System.Drawing.Size(48, 16)
        Me.BEMarginLabel.TabIndex = 39
        Me.BEMarginLabel.Text = "Margin"
        '
        'BECostLabel
        '
        Me.BECostLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BECostLabel.Location = New System.Drawing.Point(464, 256)
        Me.BECostLabel.Name = "BECostLabel"
        Me.BECostLabel.Size = New System.Drawing.Size(40, 16)
        Me.BECostLabel.TabIndex = 38
        Me.BECostLabel.Text = "Cost"
        '
        'BETypeLabel
        '
        Me.BETypeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BETypeLabel.Location = New System.Drawing.Point(416, 256)
        Me.BETypeLabel.Name = "BETypeLabel"
        Me.BETypeLabel.Size = New System.Drawing.Size(40, 16)
        Me.BETypeLabel.TabIndex = 37
        Me.BETypeLabel.Text = "Type"
        Me.BETypeLabel.Visible = False
        '
        'RTLabel
        '
        Me.RTLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTLabel.Location = New System.Drawing.Point(376, 256)
        Me.RTLabel.Name = "RTLabel"
        Me.RTLabel.Size = New System.Drawing.Size(40, 16)
        Me.RTLabel.TabIndex = 36
        Me.RTLabel.Text = "R/T"
        '
        'BEDescriptionLabel
        '
        Me.BEDescriptionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEDescriptionLabel.Location = New System.Drawing.Point(168, 256)
        Me.BEDescriptionLabel.Name = "BEDescriptionLabel"
        Me.BEDescriptionLabel.Size = New System.Drawing.Size(88, 16)
        Me.BEDescriptionLabel.TabIndex = 35
        Me.BEDescriptionLabel.Text = "Description"
        '
        'BEProductIDLabel
        '
        Me.BEProductIDLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEProductIDLabel.Location = New System.Drawing.Point(24, 256)
        Me.BEProductIDLabel.Name = "BEProductIDLabel"
        Me.BEProductIDLabel.Size = New System.Drawing.Size(104, 16)
        Me.BEProductIDLabel.TabIndex = 34
        Me.BEProductIDLabel.Text = "Product ID"
        '
        'txtBERSell
        '
        Me.txtBERSell.Location = New System.Drawing.Point(640, 272)
        Me.txtBERSell.Name = "txtBERSell"
        Me.txtBERSell.Size = New System.Drawing.Size(56, 20)
        Me.txtBERSell.TabIndex = 17
        Me.txtBERSell.TabStop = False
        '
        'txtBERMargin
        '
        Me.txtBERMargin.Location = New System.Drawing.Point(520, 272)
        Me.txtBERMargin.Name = "txtBERMargin"
        Me.txtBERMargin.Size = New System.Drawing.Size(48, 20)
        Me.txtBERMargin.TabIndex = 16
        Me.txtBERMargin.TabStop = False
        '
        'txtBERCost
        '
        Me.txtBERCost.Location = New System.Drawing.Point(464, 272)
        Me.txtBERCost.Name = "txtBERCost"
        Me.txtBERCost.Size = New System.Drawing.Size(56, 20)
        Me.txtBERCost.TabIndex = 15
        Me.txtBERCost.TabStop = False
        '
        'txtBERType
        '
        Me.txtBERType.Location = New System.Drawing.Point(416, 272)
        Me.txtBERType.Name = "txtBERType"
        Me.txtBERType.Size = New System.Drawing.Size(48, 20)
        Me.txtBERType.TabIndex = 14
        Me.txtBERType.Visible = False
        '
        'txtBERRT
        '
        Me.txtBERRT.Location = New System.Drawing.Point(376, 272)
        Me.txtBERRT.Name = "txtBERRT"
        Me.txtBERRT.Size = New System.Drawing.Size(40, 20)
        Me.txtBERRT.TabIndex = 11
        '
        'txtBERDesc
        '
        Me.txtBERDesc.Location = New System.Drawing.Point(168, 272)
        Me.txtBERDesc.Name = "txtBERDesc"
        Me.txtBERDesc.Size = New System.Drawing.Size(208, 20)
        Me.txtBERDesc.TabIndex = 12
        Me.txtBERDesc.TabStop = False
        '
        'txtBERProdID
        '
        Me.txtBERProdID.Location = New System.Drawing.Point(24, 272)
        Me.txtBERProdID.MaxLength = 25
        Me.txtBERProdID.Name = "txtBERProdID"
        Me.txtBERProdID.Size = New System.Drawing.Size(144, 20)
        Me.txtBERProdID.TabIndex = 0
        Me.txtBERProdID.TabStop = False
        '
        'btnBEAdjustSell
        '
        Me.btnBEAdjustSell.Location = New System.Drawing.Point(560, 137)
        Me.btnBEAdjustSell.Name = "btnBEAdjustSell"
        Me.btnBEAdjustSell.Size = New System.Drawing.Size(120, 23)
        Me.btnBEAdjustSell.TabIndex = 15
        Me.btnBEAdjustSell.Text = "Edit Desc, Sell,  RT"
        '
        'TypesIncludedLabel
        '
        Me.TypesIncludedLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TypesIncludedLabel.Location = New System.Drawing.Point(400, 16)
        Me.TypesIncludedLabel.Name = "TypesIncludedLabel"
        Me.TypesIncludedLabel.Size = New System.Drawing.Size(120, 16)
        Me.TypesIncludedLabel.TabIndex = 25
        Me.TypesIncludedLabel.Text = "Types Included In This Bid"
        '
        'btnBERByType
        '
        Me.btnBERByType.Location = New System.Drawing.Point(426, 137)
        Me.btnBERByType.Name = "btnBERByType"
        Me.btnBERByType.Size = New System.Drawing.Size(120, 23)
        Me.btnBERByType.TabIndex = 14
        Me.btnBERByType.Text = "Replace By Type"
        '
        'btnBERByPartNum
        '
        Me.btnBERByPartNum.Location = New System.Drawing.Point(158, 137)
        Me.btnBERByPartNum.Name = "btnBERByPartNum"
        Me.btnBERByPartNum.Size = New System.Drawing.Size(120, 23)
        Me.btnBERByPartNum.TabIndex = 13
        Me.btnBERByPartNum.Text = "Replace By Part #"
        '
        'btnBEDelete
        '
        Me.btnBEDelete.Location = New System.Drawing.Point(24, 137)
        Me.btnBEDelete.Name = "btnBEDelete"
        Me.btnBEDelete.Size = New System.Drawing.Size(120, 23)
        Me.btnBEDelete.TabIndex = 12
        Me.btnBEDelete.Text = "Delete By Part #"
        '
        'cboBEType
        '
        Me.cboBEType.Location = New System.Drawing.Point(400, 32)
        Me.cboBEType.Name = "cboBEType"
        Me.cboBEType.Size = New System.Drawing.Size(288, 21)
        Me.cboBEType.TabIndex = 1
        '
        'CurrentBidLabel
        '
        Me.CurrentBidLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CurrentBidLabel.Location = New System.Drawing.Point(24, 16)
        Me.CurrentBidLabel.Name = "CurrentBidLabel"
        Me.CurrentBidLabel.Size = New System.Drawing.Size(100, 16)
        Me.CurrentBidLabel.TabIndex = 20
        Me.CurrentBidLabel.Text = "Current Bid Items"
        '
        'cbxFSell
        '
        Me.cbxFSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFSell.Location = New System.Drawing.Point(574, 96)
        Me.cbxFSell.Name = "cbxFSell"
        Me.cbxFSell.Size = New System.Drawing.Size(48, 24)
        Me.cbxFSell.TabIndex = 5
        Me.cbxFSell.Text = "Fixed Sell"
        '
        'txtBEBMargin
        '
        Me.txtBEBMargin.Location = New System.Drawing.Point(520, 96)
        Me.txtBEBMargin.Name = "txtBEBMargin"
        Me.txtBEBMargin.Size = New System.Drawing.Size(48, 20)
        Me.txtBEBMargin.TabIndex = 4
        Me.txtBEBMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'BEMarginLabel2
        '
        Me.BEMarginLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEMarginLabel2.Location = New System.Drawing.Point(520, 80)
        Me.BEMarginLabel2.Name = "BEMarginLabel2"
        Me.BEMarginLabel2.Size = New System.Drawing.Size(40, 16)
        Me.BEMarginLabel2.TabIndex = 18
        Me.BEMarginLabel2.Text = "Margin"
        '
        'BESellLabel2
        '
        Me.BESellLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BESellLabel2.Location = New System.Drawing.Point(623, 80)
        Me.BESellLabel2.Name = "BESellLabel2"
        Me.BESellLabel2.Size = New System.Drawing.Size(40, 16)
        Me.BESellLabel2.TabIndex = 16
        Me.BESellLabel2.Text = "Sell"
        '
        'BECostLabel2
        '
        Me.BECostLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BECostLabel2.Location = New System.Drawing.Point(464, 80)
        Me.BECostLabel2.Name = "BECostLabel2"
        Me.BECostLabel2.Size = New System.Drawing.Size(32, 16)
        Me.BECostLabel2.TabIndex = 15
        Me.BECostLabel2.Text = "Cost"
        '
        'lblBEBType
        '
        Me.lblBEBType.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBEBType.Location = New System.Drawing.Point(416, 80)
        Me.lblBEBType.Name = "lblBEBType"
        Me.lblBEBType.Size = New System.Drawing.Size(40, 16)
        Me.lblBEBType.TabIndex = 14
        Me.lblBEBType.Text = "Type"
        '
        'RTLabel2
        '
        Me.RTLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTLabel2.Location = New System.Drawing.Point(376, 80)
        Me.RTLabel2.Name = "RTLabel2"
        Me.RTLabel2.Size = New System.Drawing.Size(32, 16)
        Me.RTLabel2.TabIndex = 13
        Me.RTLabel2.Text = "R/T"
        '
        'BEDescription2
        '
        Me.BEDescription2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEDescription2.Location = New System.Drawing.Point(168, 80)
        Me.BEDescription2.Name = "BEDescription2"
        Me.BEDescription2.Size = New System.Drawing.Size(100, 16)
        Me.BEDescription2.TabIndex = 12
        Me.BEDescription2.Text = "Description"
        '
        'BEProductIDLabel2
        '
        Me.BEProductIDLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEProductIDLabel2.Location = New System.Drawing.Point(24, 80)
        Me.BEProductIDLabel2.Name = "BEProductIDLabel2"
        Me.BEProductIDLabel2.Size = New System.Drawing.Size(100, 16)
        Me.BEProductIDLabel2.TabIndex = 11
        Me.BEProductIDLabel2.Text = "Product ID"
        '
        'txtBEBSell
        '
        Me.txtBEBSell.Location = New System.Drawing.Point(623, 96)
        Me.txtBEBSell.Name = "txtBEBSell"
        Me.txtBEBSell.Size = New System.Drawing.Size(56, 20)
        Me.txtBEBSell.TabIndex = 6
        Me.txtBEBSell.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBEBCost
        '
        Me.txtBEBCost.Location = New System.Drawing.Point(464, 96)
        Me.txtBEBCost.Name = "txtBEBCost"
        Me.txtBEBCost.Size = New System.Drawing.Size(56, 20)
        Me.txtBEBCost.TabIndex = 3
        Me.txtBEBCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBEBType
        '
        Me.txtBEBType.Enabled = False
        Me.txtBEBType.Location = New System.Drawing.Point(416, 96)
        Me.txtBEBType.Name = "txtBEBType"
        Me.txtBEBType.Size = New System.Drawing.Size(48, 20)
        Me.txtBEBType.TabIndex = 3
        Me.txtBEBType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtBEBRT
        '
        Me.txtBEBRT.Location = New System.Drawing.Point(376, 96)
        Me.txtBEBRT.Name = "txtBEBRT"
        Me.txtBEBRT.Size = New System.Drawing.Size(40, 20)
        Me.txtBEBRT.TabIndex = 2
        '
        'txtBEBDesc
        '
        Me.txtBEBDesc.Location = New System.Drawing.Point(168, 96)
        Me.txtBEBDesc.Name = "txtBEBDesc"
        Me.txtBEBDesc.Size = New System.Drawing.Size(208, 20)
        Me.txtBEBDesc.TabIndex = 3
        '
        'txtBEBProdID
        '
        Me.txtBEBProdID.Enabled = False
        Me.txtBEBProdID.Location = New System.Drawing.Point(24, 96)
        Me.txtBEBProdID.Name = "txtBEBProdID"
        Me.txtBEBProdID.Size = New System.Drawing.Size(144, 20)
        Me.txtBEBProdID.TabIndex = 2
        '
        'rbBESO
        '
        Me.rbBESO.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBESO.Location = New System.Drawing.Point(120, 176)
        Me.rbBESO.Name = "rbBESO"
        Me.rbBESO.Size = New System.Drawing.Size(72, 16)
        Me.rbBESO.TabIndex = 9
        Me.rbBESO.Text = "SO Catalog"
        '
        'rbBEMain
        '
        Me.rbBEMain.Checked = True
        Me.rbBEMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbBEMain.Location = New System.Drawing.Point(24, 176)
        Me.rbBEMain.Name = "rbBEMain"
        Me.rbBEMain.Size = New System.Drawing.Size(80, 16)
        Me.rbBEMain.TabIndex = 8
        Me.rbBEMain.TabStop = True
        Me.rbBEMain.Text = "Main Catalog"
        '
        'cboBECatalog
        '
        Me.cboBECatalog.Location = New System.Drawing.Point(24, 200)
        Me.cboBECatalog.Name = "cboBECatalog"
        Me.cboBECatalog.Size = New System.Drawing.Size(352, 21)
        Me.cboBECatalog.TabIndex = 10
        '
        'cboBEBidItems
        '
        Me.cboBEBidItems.Location = New System.Drawing.Point(24, 32)
        Me.cboBEBidItems.Name = "cboBEBidItems"
        Me.cboBEBidItems.Size = New System.Drawing.Size(344, 21)
        Me.cboBEBidItems.TabIndex = 0
        '
        'tbSummary
        '
        Me.tbSummary.Controls.Add(Me.txtMarAuxMar)
        Me.tbSummary.Controls.Add(Me.txtMarAuxSell)
        Me.tbSummary.Controls.Add(Me.txtMarAuxCost)
        Me.tbSummary.Controls.Add(Me.lblMarAux)
        Me.tbSummary.Controls.Add(Me.txtMarComMargin)
        Me.tbSummary.Controls.Add(Me.txtMarComSell)
        Me.tbSummary.Controls.Add(Me.txtMarComCost)
        Me.tbSummary.Controls.Add(Me.CommonLabel)
        Me.tbSummary.Controls.Add(Me.NumUnitsLabel)
        Me.tbSummary.Controls.Add(Me.txtBidUnitCnt)
        Me.tbSummary.Controls.Add(Me.txtBidBldgCnt)
        Me.tbSummary.Controls.Add(Me.txtSummaryMargin)
        Me.tbSummary.Controls.Add(Me.txtSummaryProfit)
        Me.tbSummary.Controls.Add(Me.txtSummaryTotalSell)
        Me.tbSummary.Controls.Add(Me.txtSummaryTotalCost)
        Me.tbSummary.Controls.Add(Me.txtMarSiteMargin)
        Me.tbSummary.Controls.Add(Me.txtMarSiteSell)
        Me.tbSummary.Controls.Add(Me.txtMarSiteCost)
        Me.tbSummary.Controls.Add(Me.lblMarSite)
        Me.tbSummary.Controls.Add(Me.txtMarCHMargin)
        Me.tbSummary.Controls.Add(Me.txtMarCHSell)
        Me.tbSummary.Controls.Add(Me.txtMarCHCost)
        Me.tbSummary.Controls.Add(Me.lblMarCH)
        Me.tbSummary.Controls.Add(Me.txtMarUnitCost)
        Me.tbSummary.Controls.Add(Me.txtMarUnitMargin)
        Me.tbSummary.Controls.Add(Me.txtMarUnitSell)
        Me.tbSummary.Controls.Add(Me.lblMarUnitMargin)
        Me.tbSummary.Controls.Add(Me.lblMarUnitSell)
        Me.tbSummary.Controls.Add(Me.lblMarUnitCost)
        Me.tbSummary.Controls.Add(Me.lblMarUnits)
        Me.tbSummary.Controls.Add(Me.ProfitLabel)
        Me.tbSummary.Controls.Add(Me.TotalLabel)
        Me.tbSummary.Controls.Add(Me.dgBOM)
        Me.tbSummary.Controls.Add(Me.dgUnitCnt)
        Me.tbSummary.Controls.Add(Me.dgBidUnitsperBldg)
        Me.tbSummary.Controls.Add(Me.NumBuildingsLabel)
        Me.tbSummary.Location = New System.Drawing.Point(4, 22)
        Me.tbSummary.Name = "tbSummary"
        Me.tbSummary.Size = New System.Drawing.Size(712, 382)
        Me.tbSummary.TabIndex = 3
        Me.tbSummary.Text = "Summary"
        '
        'txtMarAuxMar
        '
        Me.txtMarAuxMar.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarAuxMar.Location = New System.Drawing.Point(312, 72)
        Me.txtMarAuxMar.Name = "txtMarAuxMar"
        Me.txtMarAuxMar.Size = New System.Drawing.Size(72, 16)
        Me.txtMarAuxMar.TabIndex = 46
        Me.txtMarAuxMar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarAuxSell
        '
        Me.txtMarAuxSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarAuxSell.Location = New System.Drawing.Point(312, 52)
        Me.txtMarAuxSell.Name = "txtMarAuxSell"
        Me.txtMarAuxSell.Size = New System.Drawing.Size(72, 16)
        Me.txtMarAuxSell.TabIndex = 45
        Me.txtMarAuxSell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarAuxCost
        '
        Me.txtMarAuxCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarAuxCost.Location = New System.Drawing.Point(312, 32)
        Me.txtMarAuxCost.Name = "txtMarAuxCost"
        Me.txtMarAuxCost.Size = New System.Drawing.Size(72, 16)
        Me.txtMarAuxCost.TabIndex = 44
        Me.txtMarAuxCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMarAux
        '
        Me.lblMarAux.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarAux.Location = New System.Drawing.Point(304, 8)
        Me.lblMarAux.Name = "lblMarAux"
        Me.lblMarAux.Size = New System.Drawing.Size(112, 16)
        Me.lblMarAux.TabIndex = 43
        Me.lblMarAux.Text = "Auxiliary"
        Me.lblMarAux.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtMarComMargin
        '
        Me.txtMarComMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarComMargin.Location = New System.Drawing.Point(224, 72)
        Me.txtMarComMargin.Name = "txtMarComMargin"
        Me.txtMarComMargin.Size = New System.Drawing.Size(72, 16)
        Me.txtMarComMargin.TabIndex = 42
        Me.txtMarComMargin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarComSell
        '
        Me.txtMarComSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarComSell.Location = New System.Drawing.Point(224, 52)
        Me.txtMarComSell.Name = "txtMarComSell"
        Me.txtMarComSell.Size = New System.Drawing.Size(72, 16)
        Me.txtMarComSell.TabIndex = 41
        Me.txtMarComSell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarComCost
        '
        Me.txtMarComCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarComCost.Location = New System.Drawing.Point(224, 32)
        Me.txtMarComCost.Name = "txtMarComCost"
        Me.txtMarComCost.Size = New System.Drawing.Size(72, 16)
        Me.txtMarComCost.TabIndex = 40
        Me.txtMarComCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CommonLabel
        '
        Me.CommonLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommonLabel.Location = New System.Drawing.Point(224, 8)
        Me.CommonLabel.Name = "CommonLabel"
        Me.CommonLabel.Size = New System.Drawing.Size(72, 16)
        Me.CommonLabel.TabIndex = 39
        Me.CommonLabel.Text = "Common"
        Me.CommonLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NumUnitsLabel
        '
        Me.NumUnitsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumUnitsLabel.Location = New System.Drawing.Point(576, 32)
        Me.NumUnitsLabel.Name = "NumUnitsLabel"
        Me.NumUnitsLabel.Size = New System.Drawing.Size(64, 16)
        Me.NumUnitsLabel.TabIndex = 38
        Me.NumUnitsLabel.Text = "# of Units"
        '
        'txtBidUnitCnt
        '
        Me.txtBidUnitCnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBidUnitCnt.Location = New System.Drawing.Point(624, 32)
        Me.txtBidUnitCnt.Name = "txtBidUnitCnt"
        Me.txtBidUnitCnt.Size = New System.Drawing.Size(64, 16)
        Me.txtBidUnitCnt.TabIndex = 37
        Me.txtBidUnitCnt.Text = "UnitCnt"
        Me.txtBidUnitCnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBidBldgCnt
        '
        Me.txtBidBldgCnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBidBldgCnt.Location = New System.Drawing.Point(624, 52)
        Me.txtBidBldgCnt.Name = "txtBidBldgCnt"
        Me.txtBidBldgCnt.Size = New System.Drawing.Size(64, 16)
        Me.txtBidBldgCnt.TabIndex = 36
        Me.txtBidBldgCnt.Text = "BldgCnt"
        Me.txtBidBldgCnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSummaryMargin
        '
        Me.txtSummaryMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSummaryMargin.Location = New System.Drawing.Point(472, 72)
        Me.txtSummaryMargin.Name = "txtSummaryMargin"
        Me.txtSummaryMargin.Size = New System.Drawing.Size(88, 16)
        Me.txtSummaryMargin.TabIndex = 35
        Me.txtSummaryMargin.Text = "Margin"
        Me.txtSummaryMargin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSummaryProfit
        '
        Me.txtSummaryProfit.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSummaryProfit.Location = New System.Drawing.Point(624, 72)
        Me.txtSummaryProfit.Name = "txtSummaryProfit"
        Me.txtSummaryProfit.Size = New System.Drawing.Size(64, 16)
        Me.txtSummaryProfit.TabIndex = 34
        Me.txtSummaryProfit.Text = "Profit"
        Me.txtSummaryProfit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSummaryTotalSell
        '
        Me.txtSummaryTotalSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSummaryTotalSell.Location = New System.Drawing.Point(472, 52)
        Me.txtSummaryTotalSell.Name = "txtSummaryTotalSell"
        Me.txtSummaryTotalSell.Size = New System.Drawing.Size(88, 16)
        Me.txtSummaryTotalSell.TabIndex = 33
        Me.txtSummaryTotalSell.Text = "Sell"
        Me.txtSummaryTotalSell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSummaryTotalCost
        '
        Me.txtSummaryTotalCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSummaryTotalCost.Location = New System.Drawing.Point(472, 32)
        Me.txtSummaryTotalCost.Name = "txtSummaryTotalCost"
        Me.txtSummaryTotalCost.Size = New System.Drawing.Size(88, 16)
        Me.txtSummaryTotalCost.TabIndex = 32
        Me.txtSummaryTotalCost.Text = "Cost"
        Me.txtSummaryTotalCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarSiteMargin
        '
        Me.txtMarSiteMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarSiteMargin.Location = New System.Drawing.Point(392, 72)
        Me.txtMarSiteMargin.Name = "txtMarSiteMargin"
        Me.txtMarSiteMargin.Size = New System.Drawing.Size(64, 16)
        Me.txtMarSiteMargin.TabIndex = 31
        Me.txtMarSiteMargin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarSiteSell
        '
        Me.txtMarSiteSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarSiteSell.Location = New System.Drawing.Point(392, 52)
        Me.txtMarSiteSell.Name = "txtMarSiteSell"
        Me.txtMarSiteSell.Size = New System.Drawing.Size(64, 16)
        Me.txtMarSiteSell.TabIndex = 30
        Me.txtMarSiteSell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarSiteCost
        '
        Me.txtMarSiteCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarSiteCost.Location = New System.Drawing.Point(392, 32)
        Me.txtMarSiteCost.Name = "txtMarSiteCost"
        Me.txtMarSiteCost.Size = New System.Drawing.Size(64, 16)
        Me.txtMarSiteCost.TabIndex = 29
        Me.txtMarSiteCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMarSite
        '
        Me.lblMarSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarSite.Location = New System.Drawing.Point(392, 8)
        Me.lblMarSite.Name = "lblMarSite"
        Me.lblMarSite.Size = New System.Drawing.Size(64, 16)
        Me.lblMarSite.TabIndex = 25
        Me.lblMarSite.Text = "Site"
        Me.lblMarSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarCHMargin
        '
        Me.txtMarCHMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarCHMargin.Location = New System.Drawing.Point(136, 72)
        Me.txtMarCHMargin.Name = "txtMarCHMargin"
        Me.txtMarCHMargin.Size = New System.Drawing.Size(80, 16)
        Me.txtMarCHMargin.TabIndex = 24
        Me.txtMarCHMargin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarCHSell
        '
        Me.txtMarCHSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarCHSell.Location = New System.Drawing.Point(136, 52)
        Me.txtMarCHSell.Name = "txtMarCHSell"
        Me.txtMarCHSell.Size = New System.Drawing.Size(80, 16)
        Me.txtMarCHSell.TabIndex = 23
        Me.txtMarCHSell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarCHCost
        '
        Me.txtMarCHCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarCHCost.Location = New System.Drawing.Point(136, 32)
        Me.txtMarCHCost.Name = "txtMarCHCost"
        Me.txtMarCHCost.Size = New System.Drawing.Size(80, 16)
        Me.txtMarCHCost.TabIndex = 22
        Me.txtMarCHCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMarCH
        '
        Me.lblMarCH.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarCH.Location = New System.Drawing.Point(136, 8)
        Me.lblMarCH.Name = "lblMarCH"
        Me.lblMarCH.Size = New System.Drawing.Size(80, 16)
        Me.lblMarCH.TabIndex = 18
        Me.lblMarCH.Text = "Club House"
        Me.lblMarCH.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarUnitCost
        '
        Me.txtMarUnitCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarUnitCost.Location = New System.Drawing.Point(64, 32)
        Me.txtMarUnitCost.Name = "txtMarUnitCost"
        Me.txtMarUnitCost.Size = New System.Drawing.Size(64, 16)
        Me.txtMarUnitCost.TabIndex = 17
        Me.txtMarUnitCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarUnitMargin
        '
        Me.txtMarUnitMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarUnitMargin.Location = New System.Drawing.Point(64, 72)
        Me.txtMarUnitMargin.Name = "txtMarUnitMargin"
        Me.txtMarUnitMargin.Size = New System.Drawing.Size(64, 16)
        Me.txtMarUnitMargin.TabIndex = 16
        Me.txtMarUnitMargin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMarUnitSell
        '
        Me.txtMarUnitSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarUnitSell.Location = New System.Drawing.Point(64, 52)
        Me.txtMarUnitSell.Name = "txtMarUnitSell"
        Me.txtMarUnitSell.Size = New System.Drawing.Size(64, 16)
        Me.txtMarUnitSell.TabIndex = 15
        Me.txtMarUnitSell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMarUnitMargin
        '
        Me.lblMarUnitMargin.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarUnitMargin.Location = New System.Drawing.Point(24, 72)
        Me.lblMarUnitMargin.Name = "lblMarUnitMargin"
        Me.lblMarUnitMargin.Size = New System.Drawing.Size(48, 16)
        Me.lblMarUnitMargin.TabIndex = 14
        Me.lblMarUnitMargin.Text = "Margin"
        '
        'lblMarUnitSell
        '
        Me.lblMarUnitSell.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarUnitSell.Location = New System.Drawing.Point(24, 52)
        Me.lblMarUnitSell.Name = "lblMarUnitSell"
        Me.lblMarUnitSell.Size = New System.Drawing.Size(48, 16)
        Me.lblMarUnitSell.TabIndex = 13
        Me.lblMarUnitSell.Text = "Sell"
        '
        'lblMarUnitCost
        '
        Me.lblMarUnitCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarUnitCost.Location = New System.Drawing.Point(24, 32)
        Me.lblMarUnitCost.Name = "lblMarUnitCost"
        Me.lblMarUnitCost.Size = New System.Drawing.Size(48, 16)
        Me.lblMarUnitCost.TabIndex = 12
        Me.lblMarUnitCost.Text = "Cost"
        '
        'lblMarUnits
        '
        Me.lblMarUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarUnits.Location = New System.Drawing.Point(64, 8)
        Me.lblMarUnits.Name = "lblMarUnits"
        Me.lblMarUnits.Size = New System.Drawing.Size(64, 16)
        Me.lblMarUnits.TabIndex = 11
        Me.lblMarUnits.Text = "Units"
        Me.lblMarUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ProfitLabel
        '
        Me.ProfitLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProfitLabel.Location = New System.Drawing.Point(576, 72)
        Me.ProfitLabel.Name = "ProfitLabel"
        Me.ProfitLabel.Size = New System.Drawing.Size(64, 16)
        Me.ProfitLabel.TabIndex = 10
        Me.ProfitLabel.Text = "Profit"
        Me.ProfitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TotalLabel
        '
        Me.TotalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalLabel.Location = New System.Drawing.Point(472, 11)
        Me.TotalLabel.Name = "TotalLabel"
        Me.TotalLabel.Size = New System.Drawing.Size(88, 10)
        Me.TotalLabel.TabIndex = 6
        Me.TotalLabel.Text = "Total"
        Me.TotalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgBOM
        '
        Me.dgBOM.CaptionText = "Bid Line Item Summary"
        Me.dgBOM.DataMember = ""
        Me.dgBOM.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBOM.Location = New System.Drawing.Point(16, 224)
        Me.dgBOM.Name = "dgBOM"
        Me.dgBOM.ReadOnly = True
        Me.dgBOM.Size = New System.Drawing.Size(680, 136)
        Me.dgBOM.TabIndex = 4
        Me.dgBOM.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle2})
        '
        'DataGridTableStyle2
        '
        Me.DataGridTableStyle2.DataGrid = Me.dgBOM
        Me.DataGridTableStyle2.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.clmPartNumber, Me.clmDescription, Me.clmQuantity, Me.clmCost, Me.ClmTotalCost, Me.clmSell, Me.clmTotalSell, Me.clmNet})
        Me.DataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle2.MappingName = "LineItemSummary"
        '
        'clmPartNumber
        '
        Me.clmPartNumber.Format = ""
        Me.clmPartNumber.FormatInfo = Nothing
        Me.clmPartNumber.HeaderText = "Part Number"
        Me.clmPartNumber.MappingName = "PartNumber"
        Me.clmPartNumber.ReadOnly = True
        Me.clmPartNumber.Width = 75
        '
        'clmDescription
        '
        Me.clmDescription.Format = ""
        Me.clmDescription.FormatInfo = Nothing
        Me.clmDescription.HeaderText = "Description"
        Me.clmDescription.MappingName = "Description"
        Me.clmDescription.NullText = ""
        Me.clmDescription.ReadOnly = True
        Me.clmDescription.Width = 125
        '
        'clmQuantity
        '
        Me.clmQuantity.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.clmQuantity.Format = ""
        Me.clmQuantity.FormatInfo = Nothing
        Me.clmQuantity.HeaderText = "Quan"
        Me.clmQuantity.MappingName = "Quantity"
        Me.clmQuantity.NullText = ""
        Me.clmQuantity.ReadOnly = True
        Me.clmQuantity.Width = 50
        '
        'clmCost
        '
        Me.clmCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.clmCost.Format = "C"
        Me.clmCost.FormatInfo = Nothing
        Me.clmCost.HeaderText = "Cost"
        Me.clmCost.MappingName = "Cost"
        Me.clmCost.NullText = ""
        Me.clmCost.ReadOnly = True
        Me.clmCost.Width = 75
        '
        'ClmTotalCost
        '
        Me.ClmTotalCost.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.ClmTotalCost.Format = "C"
        Me.ClmTotalCost.FormatInfo = Nothing
        Me.ClmTotalCost.HeaderText = "Total cost"
        Me.ClmTotalCost.MappingName = "Total Cost"
        Me.ClmTotalCost.NullText = ""
        Me.ClmTotalCost.ReadOnly = True
        Me.ClmTotalCost.Width = 75
        '
        'clmSell
        '
        Me.clmSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.clmSell.Format = "c"
        Me.clmSell.FormatInfo = Nothing
        Me.clmSell.HeaderText = "Sell"
        Me.clmSell.MappingName = "Sell"
        Me.clmSell.NullText = ""
        Me.clmSell.Width = 75
        '
        'clmTotalSell
        '
        Me.clmTotalSell.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.clmTotalSell.Format = "c"
        Me.clmTotalSell.FormatInfo = Nothing
        Me.clmTotalSell.HeaderText = "Total Sell"
        Me.clmTotalSell.MappingName = "Total Sell"
        Me.clmTotalSell.NullText = ""
        Me.clmTotalSell.ReadOnly = True
        Me.clmTotalSell.Width = 75
        '
        'clmNet
        '
        Me.clmNet.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.clmNet.Format = "C"
        Me.clmNet.FormatInfo = Nothing
        Me.clmNet.HeaderText = "Net"
        Me.clmNet.MappingName = "Net"
        Me.clmNet.NullText = ""
        Me.clmNet.ReadOnly = True
        Me.clmNet.Width = 75
        '
        'dgUnitCnt
        '
        Me.dgUnitCnt.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgUnitCnt.CaptionText = "Unit Count"
        Me.dgUnitCnt.DataMember = ""
        Me.dgUnitCnt.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgUnitCnt.Location = New System.Drawing.Point(448, 96)
        Me.dgUnitCnt.Name = "dgUnitCnt"
        Me.dgUnitCnt.ReadOnly = True
        Me.dgUnitCnt.Size = New System.Drawing.Size(248, 120)
        Me.dgUnitCnt.TabIndex = 3
        '
        'dgBidUnitsperBldg
        '
        Me.dgBidUnitsperBldg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgBidUnitsperBldg.CaptionText = "Building - Unit Count"
        Me.dgBidUnitsperBldg.DataMember = ""
        Me.dgBidUnitsperBldg.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgBidUnitsperBldg.Location = New System.Drawing.Point(16, 96)
        Me.dgBidUnitsperBldg.Name = "dgBidUnitsperBldg"
        Me.dgBidUnitsperBldg.ReadOnly = True
        Me.dgBidUnitsperBldg.Size = New System.Drawing.Size(424, 120)
        Me.dgBidUnitsperBldg.TabIndex = 2
        Me.dgBidUnitsperBldg.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgStyleBldgUnits})
        '
        'dgStyleBldgUnits
        '
        Me.dgStyleBldgUnits.DataGrid = Me.dgBidUnitsperBldg
        Me.dgStyleBldgUnits.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgBldgUnitType, Me.dgBldgUnitDesc, Me.dgBldgUnitUType, Me.dgBldgUnitQuant})
        Me.dgStyleBldgUnits.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgStyleBldgUnits.MappingName = "UnitsPerBldg"
        '
        'dgBldgUnitType
        '
        Me.dgBldgUnitType.Format = ""
        Me.dgBldgUnitType.FormatInfo = Nothing
        Me.dgBldgUnitType.HeaderText = "Buildling"
        Me.dgBldgUnitType.MappingName = "BldgType"
        Me.dgBldgUnitType.Width = 75
        '
        'dgBldgUnitDesc
        '
        Me.dgBldgUnitDesc.Format = ""
        Me.dgBldgUnitDesc.FormatInfo = Nothing
        Me.dgBldgUnitDesc.HeaderText = "Description"
        Me.dgBldgUnitDesc.MappingName = "Description"
        Me.dgBldgUnitDesc.Width = 75
        '
        'dgBldgUnitUType
        '
        Me.dgBldgUnitUType.Format = ""
        Me.dgBldgUnitUType.FormatInfo = Nothing
        Me.dgBldgUnitUType.HeaderText = "UnitType"
        Me.dgBldgUnitUType.MappingName = "UnitType"
        Me.dgBldgUnitUType.Width = 75
        '
        'dgBldgUnitQuant
        '
        Me.dgBldgUnitQuant.Format = ""
        Me.dgBldgUnitQuant.FormatInfo = Nothing
        Me.dgBldgUnitQuant.HeaderText = "Quan"
        Me.dgBldgUnitQuant.MappingName = "Quantity"
        Me.dgBldgUnitQuant.Width = 75
        '
        'NumBuildingsLabel
        '
        Me.NumBuildingsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumBuildingsLabel.Location = New System.Drawing.Point(576, 52)
        Me.NumBuildingsLabel.Name = "NumBuildingsLabel"
        Me.NumBuildingsLabel.Size = New System.Drawing.Size(64, 16)
        Me.NumBuildingsLabel.TabIndex = 1
        Me.NumBuildingsLabel.Text = "# of Buildings"
        '
        'tbSearch
        '
        Me.tbSearch.Controls.Add(Me.btnPrintSearch)
        Me.tbSearch.Controls.Add(Me.btnLoadSearch)
        Me.tbSearch.Controls.Add(Me.dgSearch)
        Me.tbSearch.Controls.Add(Me.SearchLocationLabel)
        Me.tbSearch.Controls.Add(Me.SearchTypeLabel)
        Me.tbSearch.Controls.Add(Me.SearchPartNumLabel)
        Me.tbSearch.Controls.Add(Me.cboSPartNum)
        Me.tbSearch.Controls.Add(Me.cboSType)
        Me.tbSearch.Controls.Add(Me.cboSLocation)
        Me.tbSearch.Location = New System.Drawing.Point(4, 22)
        Me.tbSearch.Name = "tbSearch"
        Me.tbSearch.Size = New System.Drawing.Size(712, 382)
        Me.tbSearch.TabIndex = 8
        Me.tbSearch.Text = "Search"
        '
        'btnPrintSearch
        '
        Me.btnPrintSearch.Location = New System.Drawing.Point(592, 52)
        Me.btnPrintSearch.Name = "btnPrintSearch"
        Me.btnPrintSearch.Size = New System.Drawing.Size(84, 20)
        Me.btnPrintSearch.TabIndex = 8
        Me.btnPrintSearch.Text = "Print"
        Me.btnPrintSearch.Visible = False
        '
        'btnLoadSearch
        '
        Me.btnLoadSearch.Location = New System.Drawing.Point(592, 28)
        Me.btnLoadSearch.Name = "btnLoadSearch"
        Me.btnLoadSearch.Size = New System.Drawing.Size(84, 20)
        Me.btnLoadSearch.TabIndex = 7
        Me.btnLoadSearch.Text = "Load "
        '
        'dgSearch
        '
        Me.dgSearch.DataMember = ""
        Me.dgSearch.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgSearch.Location = New System.Drawing.Point(28, 84)
        Me.dgSearch.Name = "dgSearch"
        Me.dgSearch.Size = New System.Drawing.Size(664, 268)
        Me.dgSearch.TabIndex = 6
        Me.dgSearch.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.dgstSearch})
        '
        'dgstSearch
        '
        Me.dgstSearch.DataGrid = Me.dgSearch
        Me.dgstSearch.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.dgSPartNumber, Me.dgsDesc, Me.dgsQuantity, Me.dgsLocation, Me.dgsType, Me.dgsUnitType})
        Me.dgstSearch.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgstSearch.MappingName = "lineitems"
        Me.dgstSearch.ReadOnly = True
        '
        'dgSPartNumber
        '
        Me.dgSPartNumber.Format = ""
        Me.dgSPartNumber.FormatInfo = Nothing
        Me.dgSPartNumber.HeaderText = "Part Number"
        Me.dgSPartNumber.MappingName = "PartNumber"
        Me.dgSPartNumber.NullText = ""
        Me.dgSPartNumber.ReadOnly = True
        Me.dgSPartNumber.Width = 140
        '
        'dgsDesc
        '
        Me.dgsDesc.Format = ""
        Me.dgsDesc.FormatInfo = Nothing
        Me.dgsDesc.HeaderText = "Unit Description"
        Me.dgsDesc.MappingName = "Description"
        Me.dgsDesc.NullText = ""
        Me.dgsDesc.ReadOnly = True
        Me.dgsDesc.Width = 150
        '
        'dgsQuantity
        '
        Me.dgsQuantity.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgsQuantity.Format = ""
        Me.dgsQuantity.FormatInfo = Nothing
        Me.dgsQuantity.HeaderText = "Quan"
        Me.dgsQuantity.MappingName = "Quantity"
        Me.dgsQuantity.NullText = "0"
        Me.dgsQuantity.ReadOnly = True
        Me.dgsQuantity.Width = 50
        '
        'dgsLocation
        '
        Me.dgsLocation.Format = ""
        Me.dgsLocation.FormatInfo = Nothing
        Me.dgsLocation.HeaderText = "Location"
        Me.dgsLocation.MappingName = "Location"
        Me.dgsLocation.NullText = ""
        Me.dgsLocation.ReadOnly = True
        Me.dgsLocation.Width = 125
        '
        'dgsType
        '
        Me.dgsType.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.dgsType.Format = ""
        Me.dgsType.FormatInfo = Nothing
        Me.dgsType.HeaderText = "Type"
        Me.dgsType.MappingName = "Type"
        Me.dgsType.NullText = ""
        Me.dgsType.ReadOnly = True
        Me.dgsType.Width = 75
        '
        'dgsUnitType
        '
        Me.dgsUnitType.Format = ""
        Me.dgsUnitType.FormatInfo = Nothing
        Me.dgsUnitType.HeaderText = "Unit Type"
        Me.dgsUnitType.MappingName = "UnitType"
        Me.dgsUnitType.NullText = ""
        Me.dgsUnitType.ReadOnly = True
        Me.dgsUnitType.Width = 75
        '
        'SearchLocationLabel
        '
        Me.SearchLocationLabel.Location = New System.Drawing.Point(352, 12)
        Me.SearchLocationLabel.Name = "SearchLocationLabel"
        Me.SearchLocationLabel.Size = New System.Drawing.Size(96, 16)
        Me.SearchLocationLabel.TabIndex = 5
        Me.SearchLocationLabel.Text = "Location"
        '
        'SearchTypeLabel
        '
        Me.SearchTypeLabel.Location = New System.Drawing.Point(224, 8)
        Me.SearchTypeLabel.Name = "SearchTypeLabel"
        Me.SearchTypeLabel.Size = New System.Drawing.Size(100, 12)
        Me.SearchTypeLabel.TabIndex = 4
        Me.SearchTypeLabel.Text = "Type"
        '
        'SearchPartNumLabel
        '
        Me.SearchPartNumLabel.Location = New System.Drawing.Point(36, 12)
        Me.SearchPartNumLabel.Name = "SearchPartNumLabel"
        Me.SearchPartNumLabel.Size = New System.Drawing.Size(84, 12)
        Me.SearchPartNumLabel.TabIndex = 3
        Me.SearchPartNumLabel.Text = "Part Number"
        '
        'cboSPartNum
        '
        Me.cboSPartNum.Location = New System.Drawing.Point(36, 28)
        Me.cboSPartNum.Name = "cboSPartNum"
        Me.cboSPartNum.Size = New System.Drawing.Size(180, 21)
        Me.cboSPartNum.TabIndex = 2
        '
        'cboSType
        '
        Me.cboSType.Location = New System.Drawing.Point(224, 28)
        Me.cboSType.Name = "cboSType"
        Me.cboSType.Size = New System.Drawing.Size(124, 21)
        Me.cboSType.TabIndex = 1
        '
        'cboSLocation
        '
        Me.cboSLocation.Location = New System.Drawing.Point(352, 28)
        Me.cboSLocation.Name = "cboSLocation"
        Me.cboSLocation.Size = New System.Drawing.Size(232, 21)
        Me.cboSLocation.TabIndex = 0
        '
        'tbComments
        '
        Me.tbComments.Controls.Add(Me.btnCommentDelete)
        Me.tbComments.Controls.Add(Me.btnCommentEDit)
        Me.tbComments.Controls.Add(Me.btnSaveComment)
        Me.tbComments.Controls.Add(Me.lbCAuthor)
        Me.tbComments.Controls.Add(Me.lblCComment)
        Me.tbComments.Controls.Add(Me.txtCComment)
        Me.tbComments.Controls.Add(Me.txtCAuthor)
        Me.tbComments.Controls.Add(Me.dgComments)
        Me.tbComments.Location = New System.Drawing.Point(4, 22)
        Me.tbComments.Name = "tbComments"
        Me.tbComments.Size = New System.Drawing.Size(712, 382)
        Me.tbComments.TabIndex = 7
        Me.tbComments.Text = "Comments"
        '
        'btnCommentDelete
        '
        Me.btnCommentDelete.Location = New System.Drawing.Point(612, 352)
        Me.btnCommentDelete.Name = "btnCommentDelete"
        Me.btnCommentDelete.Size = New System.Drawing.Size(84, 20)
        Me.btnCommentDelete.TabIndex = 7
        Me.btnCommentDelete.Text = "Delete"
        Me.btnCommentDelete.Visible = False
        '
        'btnCommentEDit
        '
        Me.btnCommentEDit.Location = New System.Drawing.Point(528, 352)
        Me.btnCommentEDit.Name = "btnCommentEDit"
        Me.btnCommentEDit.Size = New System.Drawing.Size(84, 20)
        Me.btnCommentEDit.TabIndex = 6
        Me.btnCommentEDit.Text = "Edit"
        Me.btnCommentEDit.Visible = False
        '
        'btnSaveComment
        '
        Me.btnSaveComment.Location = New System.Drawing.Point(584, 8)
        Me.btnSaveComment.Name = "btnSaveComment"
        Me.btnSaveComment.Size = New System.Drawing.Size(112, 24)
        Me.btnSaveComment.TabIndex = 2
        Me.btnSaveComment.Text = "Save Comment"
        '
        'lbCAuthor
        '
        Me.lbCAuthor.Location = New System.Drawing.Point(12, 4)
        Me.lbCAuthor.Name = "lbCAuthor"
        Me.lbCAuthor.Size = New System.Drawing.Size(64, 16)
        Me.lbCAuthor.TabIndex = 4
        Me.lbCAuthor.Text = "Entered By"
        '
        'lblCComment
        '
        Me.lblCComment.Location = New System.Drawing.Point(12, 23)
        Me.lblCComment.Name = "lblCComment"
        Me.lblCComment.Size = New System.Drawing.Size(128, 12)
        Me.lblCComment.TabIndex = 3
        Me.lblCComment.Text = "Comment"
        '
        'txtCComment
        '
        Me.txtCComment.Location = New System.Drawing.Point(12, 40)
        Me.txtCComment.MaxLength = 1000
        Me.txtCComment.Multiline = True
        Me.txtCComment.Name = "txtCComment"
        Me.txtCComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCComment.Size = New System.Drawing.Size(684, 64)
        Me.txtCComment.TabIndex = 1
        '
        'txtCAuthor
        '
        Me.txtCAuthor.Location = New System.Drawing.Point(76, 4)
        Me.txtCAuthor.Name = "txtCAuthor"
        Me.txtCAuthor.Size = New System.Drawing.Size(60, 20)
        Me.txtCAuthor.TabIndex = 0
        '
        'dgComments
        '
        Me.dgComments.DataMember = ""
        Me.dgComments.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgComments.Location = New System.Drawing.Point(8, 120)
        Me.dgComments.Name = "dgComments"
        Me.dgComments.Size = New System.Drawing.Size(692, 224)
        Me.dgComments.TabIndex = 0
        Me.dgComments.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.tsComments})
        Me.dgComments.TabStop = False
        '
        'tsComments
        '
        Me.tsComments.DataGrid = Me.dgComments
        Me.tsComments.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.comCommentID, Me.comEntryDate, Me.comAuthor, Me.comComment})
        Me.tsComments.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.tsComments.MappingName = "Comments"
        '
        'comCommentID
        '
        Me.comCommentID.Format = ""
        Me.comCommentID.FormatInfo = Nothing
        Me.comCommentID.MappingName = "CommentID"
        Me.comCommentID.Width = 0
        '
        'comEntryDate
        '
        Me.comEntryDate.Format = "d"
        Me.comEntryDate.FormatInfo = Nothing
        Me.comEntryDate.HeaderText = "Added"
        Me.comEntryDate.MappingName = "EntryDate"
        Me.comEntryDate.NullText = ""
        Me.comEntryDate.Width = 75
        '
        'comAuthor
        '
        Me.comAuthor.Format = ""
        Me.comAuthor.FormatInfo = Nothing
        Me.comAuthor.HeaderText = "By"
        Me.comAuthor.MappingName = "Author"
        Me.comAuthor.NullText = ""
        Me.comAuthor.Width = 25
        '
        'comComment
        '
        Me.comComment.Format = ""
        Me.comComment.FormatInfo = Nothing
        Me.comComment.HeaderText = "Comment"
        Me.comComment.MappingName = "Comment"
        Me.comComment.NullText = ""
        Me.comComment.Width = 550
        '
        'BidMainMenu
        '
        Me.BidMainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuBidReport, Me.mnuUtilities})
        '
        'mnuBidReport
        '
        Me.mnuBidReport.Index = 0
        Me.mnuBidReport.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuRptSummary, Me.mnuRptSnapshot, Me.mnuUnitsBOM, Me.mnuEditAux})
        Me.mnuBidReport.Text = "Reports"
        Me.mnuBidReport.Visible = False
        '
        'mnuRptSummary
        '
        Me.mnuRptSummary.Index = 0
        Me.mnuRptSummary.Text = " Bid"
        '
        'mnuRptSnapshot
        '
        Me.mnuRptSnapshot.Index = 1
        Me.mnuRptSnapshot.Text = "Snapshot"
        '
        'mnuUnitsBOM
        '
        Me.mnuUnitsBOM.Index = 2
        Me.mnuUnitsBOM.Text = "Units BOM"
        '
        'mnuEditAux
        '
        Me.mnuEditAux.Index = 3
        Me.mnuEditAux.Text = "Edit Aux Label"
        '
        'mnuUtilities
        '
        Me.mnuUtilities.Index = 1
        Me.mnuUtilities.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUpdateCosts, Me.mnuUpdateDesc, Me.mnuAdjustMargins, Me.mnuEditCosts, Me.mnuShipDates, Me.mnuExport, Me.mnuCopyUnitOtherBid})
        Me.mnuUtilities.Text = "Utilites"
        Me.mnuUtilities.Visible = False
        '
        'mnuUpdateCosts
        '
        Me.mnuUpdateCosts.Index = 0
        Me.mnuUpdateCosts.Text = "Update Costs"
        Me.mnuUpdateCosts.Visible = False
        '
        'mnuUpdateDesc
        '
        Me.mnuUpdateDesc.Index = 1
        Me.mnuUpdateDesc.Text = "Update Part Desc."
        Me.mnuUpdateDesc.Visible = False
        '
        'mnuAdjustMargins
        '
        Me.mnuAdjustMargins.Index = 2
        Me.mnuAdjustMargins.Text = "Adjust Margins"
        Me.mnuAdjustMargins.Visible = False
        '
        'mnuEditCosts
        '
        Me.mnuEditCosts.Index = 3
        Me.mnuEditCosts.Text = "Compare Costs"
        Me.mnuEditCosts.Visible = False
        '
        'mnuShipDates
        '
        Me.mnuShipDates.Index = 4
        Me.mnuShipDates.Text = "Enter Ship Dates"
        Me.mnuShipDates.Visible = False
        '
        'mnuExport
        '
        Me.mnuExport.Index = 5
        Me.mnuExport.Text = "Export XML Files"
        '
        'mnuCopyUnitOtherBid
        '
        Me.mnuCopyUnitOtherBid.Index = 6
        Me.mnuCopyUnitOtherBid.Text = "Copy Unit From Other Bid"
        '
        'cboBid
        '
        Me.cboBid.Location = New System.Drawing.Point(16, 8)
        Me.cboBid.Name = "cboBid"
        Me.cboBid.Size = New System.Drawing.Size(328, 21)
        Me.cboBid.TabIndex = 1
        Me.cboBid.Text = "Select Bid "
        '
        'txtBClientPM
        '
        Me.txtBClientPM.Location = New System.Drawing.Point(72, 40)
        Me.txtBClientPM.Name = "txtBClientPM"
        Me.txtBClientPM.Size = New System.Drawing.Size(144, 20)
        Me.txtBClientPM.TabIndex = 6
        '
        'lblClientPM
        '
        Me.lblClientPM.Location = New System.Drawing.Point(16, 40)
        Me.lblClientPM.Name = "lblClientPM"
        Me.lblClientPM.Size = New System.Drawing.Size(56, 20)
        Me.lblClientPM.TabIndex = 7
        Me.lblClientPM.Text = "Cilent PM"
        '
        'lblLDPM
        '
        Me.lblLDPM.Location = New System.Drawing.Point(232, 40)
        Me.lblLDPM.Name = "lblLDPM"
        Me.lblLDPM.Size = New System.Drawing.Size(40, 20)
        Me.lblLDPM.TabIndex = 8
        Me.lblLDPM.Text = "LD PM"
        '
        'txtBLDPM
        '
        Me.txtBLDPM.Location = New System.Drawing.Point(280, 40)
        Me.txtBLDPM.Name = "txtBLDPM"
        Me.txtBLDPM.Size = New System.Drawing.Size(136, 20)
        Me.txtBLDPM.TabIndex = 9
        '
        'btnSaveNewBid
        '
        Me.btnSaveNewBid.Location = New System.Drawing.Point(640, 8)
        Me.btnSaveNewBid.Name = "btnSaveNewBid"
        Me.btnSaveNewBid.Size = New System.Drawing.Size(88, 23)
        Me.btnSaveNewBid.TabIndex = 10
        Me.btnSaveNewBid.Text = "Save New Bid"
        Me.btnSaveNewBid.Visible = False
        '
        'txtBid
        '
        Me.txtBid.BackColor = System.Drawing.SystemColors.Control
        Me.txtBid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBid.Location = New System.Drawing.Point(17, 13)
        Me.txtBid.Name = "txtBid"
        Me.txtBid.Size = New System.Drawing.Size(327, 13)
        Me.txtBid.TabIndex = 11
        Me.txtBid.TabStop = False
        Me.txtBid.Visible = False
        '
        'frmBid
        '
        Me.AcceptButton = Me.btnAddUnitToBldg
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(736, 489)
        Me.Controls.Add(Me.txtBid)
        Me.Controls.Add(Me.txtBLDPM)
        Me.Controls.Add(Me.txtBClientPM)
        Me.Controls.Add(Me.btnSaveNewBid)
        Me.Controls.Add(Me.lblLDPM)
        Me.Controls.Add(Me.lblClientPM)
        Me.Controls.Add(Me.cboBid)
        Me.Controls.Add(Me.TabCtrlBid)
        Me.MaximizeBox = False
        Me.Menu = Me.BidMainMenu
        Me.Name = "frmBid"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lights Direct Project Bid"
        CType(Me.dgBldg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgUnitLineItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCtrlBid.ResumeLayout(False)
        Me.tbClient.ResumeLayout(False)
        Me.tbClient.PerformLayout()
        Me.tbUnits.ResumeLayout(False)
        Me.tbUnits.PerformLayout()
        Me.UnitLocation.ResumeLayout(False)
        Me.tbAlternates.ResumeLayout(False)
        Me.tbAlternates.PerformLayout()
        CType(Me.dgAlt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbBuilding.ResumeLayout(False)
        Me.tbBuilding.PerformLayout()
        Me.tbLIBulkEdit.ResumeLayout(False)
        Me.tbLIBulkEdit.PerformLayout()
        Me.tbSummary.ResumeLayout(False)
        CType(Me.dgBOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgUnitCnt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgBidUnitsperBldg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbSearch.ResumeLayout(False)
        CType(Me.dgSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbComments.ResumeLayout(False)
        Me.tbComments.PerformLayout()
        CType(Me.dgComments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmBid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HideAll()
    End Sub

    Public Sub HideAll()
        'On Load to present Clean Form - Rqrs Menu Action
        TabCtrlBid.Visible = False
        cboBid.Visible = False
        txtBClientPM.Visible = False
        txtBLDPM.Visible = False
        lblClientPM.Visible = False
        lblLDPM.Visible = False
        btnSaveNewBid.Visible = False
        txtBid.Visible = False
        mnuBidReport.Visible = False
        mnuUpdateCosts.Visible = False
        mnuExport.Visible = False
    End Sub

    'Called By Main Form
    Public Sub LoadNewBidForm()
        LoadClients()
        Show()
        NewBidHide()
        btnSaveNewBid.Visible = True
        TabCtrlBid.Visible = True
    End Sub

    'Called By Main Form
    Public Sub LoadEditBidForm()
        Show()
        HideAll()
        cboBid.Visible = True
        LoadBids()
        mnuUtilities.Visible = False
    End Sub

    Sub LoadClients()
        'On Load Binds Clients to cboClients on Main Page
        cnn.ConnectionString = frmMain.strConnect

        Dim strClientSelect As String
        strClientSelect = "Select * from Clients Order By Name"
        Dim daClients As New SqlDataAdapter(strClientSelect, cnn)
        Dim dsClients As New DataSet
        Try
            daClients.Fill(dsClients, "ClientList")
        Catch ex As Exception
            MsgBox("Error Loading Clients")
        End Try
        cboClients.DataSource = dsClients.Tables("ClientList")
        cboClients.DisplayMember = "Name"
        cboClients.ValueMember = "ClientID"
    End Sub

    Sub LoadClientDetailsFromSavedID()
        Dim strClientID As String
        strClientID = frmMain.intClientID.ToString
        If Len(strClientID) > 4 Then
            Exit Sub
        End If

        Dim spClientDetails As String = "ClientDetails"
        Dim cmdClientDetails As New SqlCommand(spClientDetails, cnn)
        cmdClientDetails.CommandType = CommandType.StoredProcedure
        cmdClientDetails.Parameters.AddWithValue("@ClientID", strClientID)
        Dim drClientDetails As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        Try
            drClientDetails = cmdClientDetails.ExecuteReader
            drClientDetails.Read()
            txtCLName.Text = drClientDetails.Item("Name").ToString
            txtClAddr1.Text = drClientDetails.Item("Address1").ToString
            txtCLAddr2.Text = drClientDetails.Item("Address2").ToString
            txtCLCity.Text = drClientDetails.Item("City").ToString
            txtCLState.Text = drClientDetails.Item("State").ToString
            txtCLZip.Text = drClientDetails.Item("Zip").ToString
            txtCLPhone1.Text = drClientDetails.Item("Phone1").ToString
            txtCLPhone2.Text = drClientDetails.Item("Phone2").ToString
            txtCLFAX.Text = drClientDetails.Item("FAX").ToString
            txtCLemail.Text = drClientDetails.Item("email").ToString
            txtCLContact.Text = drClientDetails.Item("contact").ToString

        Catch ex As Exception
            MsgBox("Error Loading Client Details: " + ex.ToString)
        Finally
            drClientDetails.Close()
            cnn.Close()
        End Try
    End Sub

    Sub LoadClientDetails()

        If Len(cboClients.SelectedValue.ToString) > 4 Then
            Exit Sub
        End If
        Dim strClientID As String
        strClientID = cboClients.SelectedValue.ToString

        Dim spClientDetails As String = "ClientDetails"
        Dim cmdClientDetails As New SqlCommand(spClientDetails, cnn)
        cmdClientDetails.CommandType = CommandType.StoredProcedure
        cmdClientDetails.Parameters.AddWithValue("@ClientID", cboClients.SelectedValue.ToString)
        Dim drClientDetails As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        Try
            drClientDetails = cmdClientDetails.ExecuteReader
            drClientDetails.Read()
            txtCLName.Text = drClientDetails.Item("Name").ToString
            txtClAddr1.Text = drClientDetails.Item("Address1").ToString
            txtCLAddr2.Text = drClientDetails.Item("Address2").ToString
            txtCLCity.Text = drClientDetails.Item("City").ToString
            txtCLState.Text = drClientDetails.Item("State").ToString
            txtCLZip.Text = drClientDetails.Item("Zip").ToString
            txtCLPhone1.Text = drClientDetails.Item("Phone1").ToString
            txtCLPhone2.Text = drClientDetails.Item("Phone2").ToString
            txtCLFAX.Text = drClientDetails.Item("FAX").ToString
            txtCLemail.Text = drClientDetails.Item("email").ToString
            txtCLContact.Text = drClientDetails.Item("contact").ToString

        Catch ex As Exception
            MsgBox("Error Loading Client Details")
        Finally
            drClientDetails.Close()
            cnn.Close()
        End Try
    End Sub

    Sub LoadCatalogMaincbo()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String = Nothing
        If rbMain.Checked = True Then
            strCatalogMain = "CatMainItems"
        ElseIf rbSO.Checked = True Then
            strCatalogMain = "CatSOItem"
        ElseIf rbBid.Checked = True Then
            LoadBidItemcbo()
            Exit Sub
        End If

        Dim daCatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading CatalogMain CBO" + ex.ToString)
        End Try
        cboCatalogMain.DataSource = dsCatalogMain.Tables("Catalog")
        cboCatalogMain.DisplayMember = "Item"
        cboCatalogMain.ValueMember = "ProductID"
    End Sub

    Private Sub LoadcboAltCatMain()
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String = Nothing
        If rbAltMain.Checked = True Then
            strCatalogMain = "CatMainItems"
        ElseIf rbAltSO.Checked = True Then
            strCatalogMain = "CatSOItem"
        End If

        Dim daCatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading Alternate CBO")
        End Try

        cboAltCatMain.DataSource = dsCatalogMain.Tables("Catalog")
        cboAltCatMain.DisplayMember = "Item"
        cboAltCatMain.ValueMember = "ProductID"
    End Sub

    Private Sub LoadBECatalogCBO()
        'Bulk Edits
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String = Nothing
        If rbBEMain.Checked = True Then
            strCatalogMain = "CatMainItems"
        ElseIf rbBESO.Checked = True Then
            strCatalogMain = "CatSOItem"
        ElseIf rbBid.Checked = True Then
            LoadBidItemcbo()
            Exit Sub
        End If

        Dim daBECatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsBECatalogMain As New DataSet
        Try
            daBECatalogMain.Fill(dsBECatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading BECatalog")
        End Try

        cboBECatalog.DataSource = dsBECatalogMain.Tables("Catalog")
        cboBECatalog.DisplayMember = "Item"
        cboBECatalog.ValueMember = "ProductID"
    End Sub

    Private Sub LoadBEBidItemcbo()
        'Bulk Edit
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadBidItems As New SqlCommand("CatItems", cnn)
        cmdLoadBidItems.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBidItems.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBECatalogMain As New SqlDataAdapter(cmdLoadBidItems)

        Dim dsBECatalogMain As New DataSet

        Try
            daBECatalogMain.Fill(dsBECatalogMain, "Catalog")
            'Populate First Row of CBO with Select Item prompt
            Dim dtCatalog As DataTable = dsBECatalogMain.Tables("Catalog")
            Dim dr As DataRow = dtCatalog.NewRow
            dr("Item") = " - Please Select An Item"
            dr("ProductID") = "---"
            dtCatalog.Rows.InsertAt(dr, 0)
        Catch ex As Exception
            MsgBox("Error Loading BEBidItemCBO")
        End Try

        cboBEBidItems.DataSource = dsBECatalogMain.Tables("Catalog")
        cboBEBidItems.DisplayMember = "Item"
        cboBEBidItems.ValueMember = "ProductID"
    End Sub

    Private Sub LoadBETypeCBO()
        'Bulk Edit
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadBidType As New SqlCommand("LineItemTypes", cnn)
        cmdLoadBidType.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBidType.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBEBidType As New SqlDataAdapter(cmdLoadBidType)

        Dim dsBEBidType As New DataSet

        Try
            daBEBidType.Fill(dsBEBidType, "Type")
            Dim dtType As DataTable = dsBEBidType.Tables("Type")
            Dim dr As DataRow = dtType.NewRow
            dr("Item") = " - Please Select An Item"
            dr("Type") = "---"
            dtType.Rows.InsertAt(dr, 0)
        Catch ex As Exception
            MsgBox("Error Loading BETypeCBO")
        End Try

        cboBEType.DataSource = dsBEBidType.Tables("Type")
        cboBEType.DisplayMember = "Item"
        cboBEType.ValueMember = "Type"
    End Sub

    Private Sub LoadBidItemcbo()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadBidItems As New SqlCommand("CatItems", cnn)
        cmdLoadBidItems.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBidItems.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daCatalogMain As New SqlDataAdapter(cmdLoadBidItems)

        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")

            Dim dtCatalog As DataTable = dsCatalogMain.Tables("Catalog")
            Dim dr As DataRow = dtCatalog.NewRow
            dr("Item") = " - Please Select An Item"
            dr("ProductID") = "---"
            dtCatalog.Rows.InsertAt(dr, 0)
        Catch ex As Exception
            MsgBox("Error Loading BidItemcbo")
        End Try

        cboCatalogMain.DataSource = dsCatalogMain.Tables("Catalog")
        cboCatalogMain.DisplayMember = "Item"
        cboCatalogMain.ValueMember = "ProductID"
    End Sub

    Sub LoadAltBidItems()
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLoadBidItems As New SqlCommand("CatItems", cnn)
        cmdLoadBidItems.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBidItems.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daCatalogMain As New SqlDataAdapter(cmdLoadBidItems)

        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")

            Dim dtCatalog As DataTable = dsCatalogMain.Tables("Catalog")
            Dim dr As DataRow = dtCatalog.NewRow
            dr("Item") = " - Please Select An Item"
            dr("ProductID") = "---"
            dtCatalog.Rows.InsertAt(dr, 0)
        Catch ex As Exception
            MsgBox("Error Loading AltBidItemsCBO")
        End Try

        cboAltBidItems.DataSource = dsCatalogMain.Tables("Catalog")
        cboAltBidItems.DisplayMember = "Item"
        cboAltBidItems.ValueMember = "ProductID"
        cboAltBidItems.SelectedIndex = 0
    End Sub

    Sub LoadCatMainLineItem()
        'Units
        If cboCatalogMain.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboCatalogMain.SelectedValue.ToString

        Dim spCatMainLineItem As String = "CatMainLineItem"
        If rbMain.Checked = True Then
            spCatMainLineItem = "CatMainLineItem"
        ElseIf rbSO.Checked = True Then
            spCatMainLineItem = "CatSOLineItem"
        ElseIf rbBid.Checked = True Then
            spCatMainLineItem = "CatLineItem"
        End If

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboCatalogMain.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            txtLIProductID.Text = drCatMainLineItem.Item("ProductID").ToString
            txtLIDesc.Text = drCatMainLineItem.Item("Description").ToString
            txtLICost.Text = drCatMainLineItem.Item("Cost").ToString
        Catch ex As Exception
            MsgBox("Error Loading CatMainLineItem")
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try
        If txtLIQuan.Text = "" Then
            txtLIQuan.Text = "1"
        End If
        If txtLIQuan.Text = "0" Then
            txtLIQuan.Text = "1"
        End If
        txtLIMargin.Text = "30"
        txtLISell.Text = Format(CDbl(txtLICost.Text) / (1 - (CDbl(txtLIMargin.Text) / 100)), "$###,###.#0")
        txtLIRT.Text = "T"
    End Sub

    Private Sub cboClient_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClients.SelectedIndexChanged
        'Client
        LoadClientDetails()
    End Sub

    Private Sub cboCatalogMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCatalogMain.SelectedIndexChanged
        'Units
        LoadCatMainLineItem()
    End Sub

    Private Sub NewBidHide()
        btnbEditBid.Visible = False
        mnuUtilities.Visible = False
        cboBid.Visible = False
        txtBClientPM.Visible = False
        txtBLDPM.Visible = False
        lblClientPM.Visible = False
        lblLDPM.Visible = False
        TabCtrlBid.TabPages(1).Enabled = False
        TabCtrlBid.TabPages(2).Enabled = False
        TabCtrlBid.TabPages(3).Enabled = False
        TabCtrlBid.TabPages(4).Enabled = False
        TabCtrlBid.TabPages(5).Enabled = False
        TabCtrlBid.TabPages(6).Enabled = False
        TabCtrlBid.TabPages(7).Enabled = False
    End Sub

    Private Sub NewBidShow()
        cboBid.Visible = True
    End Sub

    Private Sub AddNewBid()

        Dim AddNewBid As New SqlCommand("AddProject", cnn)
        AddNewBid.CommandType = CommandType.StoredProcedure

        Dim prmClientID As SqlParameter = AddNewBid.Parameters.Add("@ClientID", SqlDbType.Int)
        prmClientID.Value = cboClients.SelectedValue

        Dim LDID As Integer
        Dim LDIDQuery As String = "SELECT MAX(LDID) as LDID FROM Project"
        Dim LDIDQU As New SqlCommand(LDIDQuery, cnn)
        LDIDQU.CommandType = CommandType.Text
        Dim LDIDDR As SqlDataReader = Nothing
        Try
            cnn.Open()
            LDIDDR = LDIDQU.ExecuteReader
            LDIDDR.Read()
            LDID = System.Convert.ToInt32(LDIDDR.Item("LDID")) + 1
        Catch ex As Exception
            MsgBox("Error obtaining LDID: " + ex.ToString)
        Finally
            cnn.Close()
        End Try

        AddNewBid.Parameters.AddWithValue("@LDID", LDID)

        Dim prmEntryDate As SqlParameter = AddNewBid.Parameters.Add("@ProjectDate", SqlDbType.DateTime)
        prmEntryDate.Value = Now()

        Dim prmDescription As SqlParameter = AddNewBid.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbDescription.Text

        Dim prmLocation As SqlParameter = AddNewBid.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtBLocation.Text

        Dim prmAddress As SqlParameter = AddNewBid.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtBAddress.Text

        Dim prmCity As SqlParameter = AddNewBid.Parameters.Add("@City", SqlDbType.NVarChar, 50)
        prmCity.Value = txtBCity.Text

        Dim prmState As SqlParameter = AddNewBid.Parameters.Add("@State", SqlDbType.Char, 2)
        prmState.Value = txtBState.Text

        Dim prmZip As SqlParameter = AddNewBid.Parameters.Add("@Zip", SqlDbType.Char, 10)
        prmZip.Value = txtBZip.Text

        Dim prmProjectType As SqlParameter = AddNewBid.Parameters.Add("@ProjectType", SqlDbType.VarChar, 25)
        prmProjectType.Value = "Bid"

        Dim prmNotes As SqlParameter = AddNewBid.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtBNotes.Text

        Dim prmLDEstimator As SqlParameter = AddNewBid.Parameters.Add("@LDEstimator", SqlDbType.NVarChar, 50)
        prmLDEstimator.Value = txtLDEstimator.Text

        Dim prmBluePrintDate As SqlParameter = AddNewBid.Parameters.Add("@BluePrintDate", SqlDbType.DateTime)
        prmBluePrintDate.Value = txtBluePrintDate.Text

        Dim prmRevDate As SqlParameter = AddNewBid.Parameters.Add("@RevDate", SqlDbType.DateTime)
        If Not IsDate(txtRevDate.Text) Then
            prmRevDate.Value = Now()
        Else
            prmRevDate.Value = txtRevDate.Text
        End If

        Dim prmSalesTaxRate As SqlParameter = AddNewBid.Parameters.Add("@SalesTaxRate", SqlDbType.Float)
        If IsNumeric(txtSalesTaxRate.Text) Then
            prmSalesTaxRate.Value = CDbl(txtSalesTaxRate.Text)
        Else
            prmSalesTaxRate.Value = 0
        End If

        Dim prmBidContact As SqlParameter = AddNewBid.Parameters.Add("@ProjectContact", SqlDbType.NVarChar, 50)
        prmBidContact.Value = txtBBidContact.Text

        Dim prmNewBidID As SqlParameter = AddNewBid.Parameters.Add("@NewProjectID", SqlDbType.Int)
        prmNewBidID.Direction = ParameterDirection.Output

        Dim prmContractDate As SqlParameter = AddNewBid.Parameters.Add("@ContractDate", SqlDbType.DateTime)
        prmContractDate.Value = DBNull.Value

        Dim prmContractAmount As SqlParameter = AddNewBid.Parameters.Add("@ContractAmount", SqlDbType.Money)
        prmContractAmount.Value = DBNull.Value

        Dim prmClientPM As SqlParameter = AddNewBid.Parameters.Add("@ClientPM", SqlDbType.NVarChar, 50)
        prmClientPM.Value = DBNull.Value

        Dim prmClientMobile As SqlParameter = AddNewBid.Parameters.Add("@ClientMobilePhone", SqlDbType.Char, 14)
        prmClientMobile.Value = DBNull.Value

        Dim prmLDPM As SqlParameter = AddNewBid.Parameters.Add("@LDPM", SqlDbType.NVarChar, 50)
        prmLDPM.Value = DBNull.Value

        Dim prmClosed As SqlParameter = AddNewBid.Parameters.Add("@Closed", SqlDbType.Bit, 50)
        prmClosed.Value = DBNull.Value

        Dim prmAuxLabel As SqlParameter = AddNewBid.Parameters.Add("@AuxLabel", SqlDbType.Char, 25)
        prmAuxLabel.Value = DBNull.Value

        Dim intBidID As Integer
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            AddNewBid.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        'Gets New Bid ID As Output Parameter
        intBidID = System.Convert.ToInt32(prmNewBidID.Value)
        frmMain.intBidID = intBidID
        btnSaveNewBid.Visible = False
        btnbEditBid.Visible = True
        'LoadBidInfo()
        mnuBidReport.Visible = True
    End Sub

    Private Sub ShowNewUnit()
        txtBUnitType.Visible = True
        txtBUnitDescription.Visible = True
        btnAddUnit.Visible = True
        lblUnitType.Visible = True
        lblUnitDesc.Visible = True
    End Sub

    Private Sub HideNewUnit()
        txtBUnitType.Visible = False
        txtBUnitDescription.Visible = False
        btnAddUnit.Visible = False
        lblUnitType.Visible = False
        lblUnitDesc.Visible = False
        txtBUnitType.Text = ""
        txtBUnitDescription.Text = ""
    End Sub

    Private Sub LoadUnits()
        If frmMain.intBidID = vbNull Then
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadUnits As New SqlCommand("Units", cnn)
        cmdLoadUnits.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadUnits.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daUnits As New SqlDataAdapter(cmdLoadUnits)
        Dim dsUnits As New DataSet
        Try
            daUnits.Fill(dsUnits, "Units")
        Catch ex As Exception
            MsgBox("Error Loading Units: " + ex.ToString)
        End Try

        cboUnit.DataSource = dsUnits.Tables("Units")
        cboUnit.DisplayMember = "U"
        cboUnit.ValueMember = "UnitType"
    End Sub

    Private Sub LoadBldgUnits()
        'Bldg
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadUnits As New SqlCommand("Units", cnn)
        cmdLoadUnits.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadUnits.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daUnits As New SqlDataAdapter(cmdLoadUnits)
        Dim dsUnits As New DataSet
        Try
            daUnits.Fill(dsUnits, "Units")
        Catch ex As Exception
            MsgBox("Error Loading BldgUnits")
        End Try

        cboBldgUnits.DataSource = dsUnits.Tables("Units")
        cboBldgUnits.DisplayMember = "U"
        cboBldgUnits.ValueMember = "UnitType"
    End Sub

    Public Sub LoadBids()
        cnn.ConnectionString = frmMain.strConnect
        Dim strBids As String
        strBids = "SELECT ProjectID,'LD-'+ RTRIM(CAST(LDID AS char))+ ': '+ISNULL(Description, '')+' '+ISNull(Location, " + _
            "'') AS Proj FROM Project Where ProjectType = 'bid' order by LDID"
        Dim daBids As New SqlDataAdapter(strBids, cnn)
        Dim dsBids As New DataSet
        daBids.Fill(dsBids, "Project")
        cboBid.DisplayMember = "Proj"
        cboBid.ValueMember = "ProjectID"
        cboBid.DataSource = dsBids.Tables("Project")
    End Sub

    Private Sub GetClientId()
        cnn.ConnectionString = frmMain.strConnect

        Dim strClientID As String
        strClientID = "SELECT ClientID FROM Project WHERE ProjectID = "
        strClientID = strClientID & frmMain.intBidID.ToString
        Dim cmdClientID As New SqlCommand(strClientID, cnn)
        cmdClientID.CommandType = CommandType.Text
        cnn.Open()
        strClientID = cmdClientID.ExecuteScalar.ToString
        cnn.Close()
        frmMain.intClientID = System.Convert.ToInt32(strClientID)
        cboClients.SelectedValue = frmMain.intClientID
    End Sub

    Private Sub ClearLineItemFields()
        'Units - Init to Clean Form
        txtLIProductID.Text = ""
        txtLIDesc.Text = ""
        txtLILocation.Text = ""
        txtLICost.Text = ""
        txtLIQuan.Text = ""
        txtLIMargin.Text = ""
        txtLISell.Text = ""
        txtLIRT.Text = ""
        txtLIType.Text = ""
    End Sub

    Private Sub LoadLineItemDG()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlText As String
        strSqlText = "Select distinct Quantity, PartNumber, LineItem.Description LIDesc, Location, RT, Type," + _
        "Cost, Margin, Sell FROM LineItem inner join Unit on LineItem.UnitID = Unit.UnitID " + _
        "WHERE Unit.UnitType = '"
        strSqlText = strSqlText + cboUnit.SelectedValue.ToString + "' and LineItem.ProjectID = " + frmMain.intBidID.ToString
        strSqlText = strSqlText + " ORDER BY  Location, Type"

        Dim cmdUnitLineItems As New SqlCommand(strSqlText, cnn)
        cmdUnitLineItems.CommandType = CommandType.Text

        If cboUnit.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim daUnitLineItems As New SqlDataAdapter(cmdUnitLineItems)
        Dim dsUnitLineItems As New DataSet
        Try
            daUnitLineItems.Fill(dsUnitLineItems, "LineItems")
        Catch ex As Exception
            MsgBox("Error Loading Unit Line Item Datagrid: " + ex.ToString)
        End Try

        dgUnitLineItems.DataSource = dsUnitLineItems.Tables("LineItems")
        intUnitDGRowCount = dsUnitLineItems.Tables("LineItems").Rows.Count
        'Init Line Item Edit Fields
        txtUnitEdQuan.Text = ""
        txtUnitEdLocation.Text = ""
        txtUnitEdType.Text = ""
    End Sub

    Private Sub LoadBidInfo()
        Dim BidNum As String = ""
        cnn.ConnectionString = frmMain.strConnect
        Dim strBidInfo As String
        strBidInfo = "SELECT LDID, Description, Location, Address, City, State, Zip, Notes, LDEstimator, BluePrintDate, RevDate, SalesTaxRate, ProjectDate, ProjectContact, AuxLabel FROM Project WHERE ProjectID = "
        strBidInfo = strBidInfo & frmMain.intBidID.ToString
        Dim cmdClientID As New SqlCommand(strBidInfo, cnn)
        cmdClientID.CommandType = CommandType.Text
        Dim drBid As SqlDataReader = Nothing
        Dim dtEntryDate As Date
        Try
            cnn.Open()
            drBid = cmdClientID.ExecuteReader
            drBid.Read()
            txtbDescription.Text = drBid.Item("Description").ToString
            txtBLocation.Text = drBid.Item("Location").ToString
            txtBAddress.Text = drBid.Item("Address").ToString
            txtBCity.Text = drBid.Item("City").ToString
            txtBState.Text = drBid.Item("State").ToString
            txtBZip.Text = drBid.Item("Zip").ToString
            txtBNotes.Text = drBid.Item("Notes").ToString
            txtLDEstimator.Text = drBid.Item("LDEstimator").ToString
            txtBluePrintDate.Text = Format(drBid.Item("BluePrintDate"), "d")
            txtRevDate.Text = Format(drBid.Item("RevDate"), "d")
            txtSalesTaxRate.Text = CStr(drBid.Item("SalesTaxRate"))
            dtEntryDate = System.Convert.ToDateTime(drBid.Item("ProjectDate"))
            txtBBidContact.Text = drBid.Item("ProjectContact").ToString
            If drBid.Item("AuxLabel").ToString = "" Then
                lblMarAux.Text = "Auxiliary"
            Else
                lblMarAux.Text = drBid.Item("AuxLabel").ToString
            End If
            BidNum = "LD-" & Format(dtEntryDate, "yy")
            BidNum = BidNum & "-" & drBid.Item("LDID").ToString
        Catch ex As Exception
            MsgBox("Error Loading Bid Info: " + ex.ToString)
        Finally
            drBid.Close()
            cnn.Close()
        End Try
        Me.Text = "Lights Direct Bid  -----    " & BidNum
    End Sub

    Private Sub EditBid()
        If txtbDescription.Text = "" Then
            MsgBox("You Must Enter A Bid Description!")
            txtbDescription.Focus()
            Exit Sub
        End If

        If txtBLocation.Text = "" Then
            MsgBox("You Must Enter A Bid Location!")
            txtBLocation.Focus()
            Exit Sub
        End If

        If Not IsDate(txtBluePrintDate.Text) Then
            MsgBox("You must enter a valid Blue Print Date")
            Exit Sub
        End If

        Dim EditBid As New SqlCommand("EditProject", cnn)
        EditBid.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = EditBid.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmDescription As SqlParameter = EditBid.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtbDescription.Text

        Dim prmLocation As SqlParameter = EditBid.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
        prmLocation.Value = txtBLocation.Text

        Dim prmAddress As SqlParameter = EditBid.Parameters.Add("@Address", SqlDbType.NVarChar, 50)
        prmAddress.Value = txtBAddress.Text

        Dim prmCity As SqlParameter = EditBid.Parameters.Add("@City", SqlDbType.NVarChar, 50)
        prmCity.Value = txtBCity.Text

        Dim prmState As SqlParameter = EditBid.Parameters.Add("@State", SqlDbType.Char, 2)
        prmState.Value = txtBState.Text

        Dim prmZip As SqlParameter = EditBid.Parameters.Add("@Zip", SqlDbType.Char, 10)
        prmZip.Value = txtBZip.Text

        Dim prmClientPM As SqlParameter = EditBid.Parameters.Add("@ClientPM", SqlDbType.NVarChar, 50)
        prmClientPM.Value = txtBClientPM.Text

        Dim prmClientMobilePhone As SqlParameter = EditBid.Parameters.Add("@ClientMobilePhone", SqlDbType.Char, 14)
        ' Not defined in Bid Form Yet
        prmClientMobilePhone.Value = ""

        Dim prmLDPM As SqlParameter = EditBid.Parameters.Add("@LDPM", SqlDbType.NVarChar, 50)
        prmLDPM.Value = txtBLDPM.Text

        Dim prmNotes As SqlParameter = EditBid.Parameters.Add("@Notes", SqlDbType.NVarChar, 250)
        prmNotes.Value = txtBNotes.Text

        Dim prmLDEstimator As SqlParameter = EditBid.Parameters.Add("@LDEstimator", SqlDbType.NVarChar, 50)
        prmLDEstimator.Value = txtLDEstimator.Text

        Dim prmBluePrintDate As SqlParameter = EditBid.Parameters.Add("@BluePrintDate", SqlDbType.DateTime)

        prmBluePrintDate.Value = txtBluePrintDate.Text

        Dim prmRevDate As SqlParameter = EditBid.Parameters.Add("@RevDate", SqlDbType.DateTime)
        If Not IsDate(txtRevDate.Text) Then
            prmRevDate.Value = Now()
        Else
            prmRevDate.Value = txtRevDate.Text
        End If

        Dim prmSalesTaxRate As SqlParameter = EditBid.Parameters.Add("@SalesTaxRate", SqlDbType.Float)
        If IsNumeric(txtSalesTaxRate.Text) Then
            prmSalesTaxRate.Value = CDbl(txtSalesTaxRate.Text)
        Else
            prmSalesTaxRate.Value = 0
        End If

        Dim prmBidContact As SqlParameter = EditBid.Parameters.Add("@ProjectContact", SqlDbType.NVarChar, 50)
        prmBidContact.Value = txtBBidContact.Text

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            EditBid.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record:" + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try


    End Sub

    Private Sub AddBulbsToUnitLineItem()
        '    'Units
        '    'Multiple connections used to support nested processing on 
        '    'DataReaders
        Dim cnn3 As New SqlConnection
        cnn3.ConnectionString = frmMain.strConnect
        Dim cmdGetBulbs As New SqlCommand
        Dim strSelect As String
        'If bulbs are defined in bulb2fixture table - build datareader
        strSelect = "SELECT Partnumber, bulb2fixture.BulbNum, Quan, CatalogMain.StandardCost FROM Bulb2Fixture " & _
        "INNER JOIN CatalogMain ON Bulb2Fixture.BulbNum = CatalogMain.ProductID WHERE Partnumber = '"
        strSelect = strSelect & txtLIProductID.Text & "'"
        cmdGetBulbs.Connection = cnn3
        cmdGetBulbs.CommandText = strSelect
        Dim drBulbs As SqlDataReader = Nothing
        Try
            If cnn3.State = ConnectionState.Closed Then
                cnn3.Open()
            End If

            drBulbs = cmdGetBulbs.ExecuteReader

            If drBulbs.HasRows Then
                Dim Bulbs As New frmBulbs
                frmMain.strBlbPartNumber = txtLIProductID.Text
                frmMain.intBlbQuan = CInt(txtLIQuan.Text)
                frmMain.strBlbLocation = txtLILocation.Text
                frmMain.strBlbRT = txtLIRT.Text
                frmMain.strBlbType = txtLIType.Text.ToString & "L"
                frmMain.UnitType = cboUnit.SelectedValue.ToString
                Bulbs.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error in Calling frmBulbs: " + ex.ToString)
        Finally
            drBulbs.Close()
            cnn3.Close()
        End Try
        LoadLineItemDG()
    End Sub

    Private Sub AddUnitLineItemsFromCatalog()

        If Not IsNumeric(txtLICost.Text) Then
            MsgBox("Could not convert cost to monetary value: " + txtLICost.Text)
            Exit Sub
        End If
        If Not IsNumeric(txtLIMargin.Text) Then
            MsgBox("Could not convert Margin to Numerical value: " + txtLIMargin.Text)
            Exit Sub
        End If
        If Not IsNumeric(txtLISell.Text) Then
            MsgBox("Could not convert Sell to monetary value: " + txtLISell.Text)
            Exit Sub
        End If

        Dim RecordSource As String

        If rbMain.Checked = True Then
            RecordSource = "CatMain"
        ElseIf rbSO.Checked = True Then
            RecordSource = "CatSO"
        Else
            RecordSource = ""
        End If

        AddLineItemToUnitType(cboUnit.SelectedValue.ToString, frmMain.intBidID, CInt(txtLIQuan.Text), txtLILocation.Text, _
                                 RecordSource, txtLIProductID.Text, txtLIRT.Text, txtLIType.Text, _
                                 txtLIDesc.Text, txtLICost.Text, txtLISell.Text, cnn, _
                                 txtLIMargin.Text)

        LoadLineItemDG()
    End Sub

    Private Sub tbUnits_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbUnits.Enter
        If tbUnits.Enabled Then
            LoadUnits()
        End If
    End Sub

    Private Sub HideBldgUnits()
        'Bldg
        'Init To Add Units
        cboBldgUnits.Visible = False
        lblBldgSelUnit.Visible = False
        lblBldgUnitQuan.Visible = False
        txtBldgUnitQuan.Visible = False
        btnAddUnitToBldg.Text = "Add Units"
    End Sub

    Private Sub HideBldgDetails()
        'bldg
        lblBldgType.Visible = False
        txtbldgType.Visible = False
        lblBldgDesc.Visible = False
        txtbldgDesc.Visible = False
        lblBldgLocation.Visible = False
        txtBldgLocation.Visible = False
        lblBldgAddress.Visible = False
        txtBldgAddress.Visible = False
        lblBldgShipDate.Visible = False
        txtBldgShipDate.Visible = False
        lblBldgNotes.Visible = False
        txtBldgNotes.Visible = False
    End Sub

    Private Sub LoadBldgs()
        'bldg
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadBldg As New SqlCommand("Buildings", cnn)
        cmdLoadBldg.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadBldg.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim daBldg As New SqlDataAdapter(cmdLoadBldg)
        Dim dsBldg As New DataSet
        Try
            daBldg.Fill(dsBldg, "Bldg")
        Catch ex As Exception
            MsgBox("Error Loading Bldgs: " + ex.ToString)
        End Try

        cboBldg.DataSource = dsBldg.Tables("Bldg")
        cboBldg.DisplayMember = "BLDG"
        cboBldg.ValueMember = "BldgID"
        blnStartBldg = False
        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If

    End Sub

    Private Sub ClearBldgFields()
        txtbldgType.Text = ""
        txtbldgDesc.Text = ""
        txtBldgLocation.Text = ""
        txtBldgAddress.Text = ""
        txtBldgShipDate.Text = ""
        txtBldgNotes.Text = ""
        txtBldgNum.Text = ""
    End Sub

    Private Sub tbBuilding_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbBuilding.Enter
        If tbBuilding.Enabled Then
            LoadBldgs()
        End If
    End Sub

    Private Sub LoadBldgDetails()
        cnn.ConnectionString = frmMain.strConnect

        Dim strBldgInfo As String
        strBldgInfo = "SELECT BldgNumber, Type, Description, Location, Address, Notes, shipdate, shipsequence FROM Building WHERE BldgID = "
        strBldgInfo = strBldgInfo & cboBldg.SelectedValue.ToString
        Dim cmdBldgInfo As New SqlCommand(strBldgInfo, cnn)
        cmdBldgInfo.CommandType = CommandType.Text
        Dim drBldgInfo As SqlDataReader = Nothing
        Try
            cnn.Open()
            drBldgInfo = cmdBldgInfo.ExecuteReader
            drBldgInfo.Read()
            txtbldgType.Text = drBldgInfo.Item("Type").ToString
            txtbldgDesc.Text = drBldgInfo.Item("Description").ToString
            txtBldgNum.Text = drBldgInfo.Item("BldgNumber").ToString
            txtBldgLocation.Text = drBldgInfo.Item("Location").ToString
            txtBldgAddress.Text = drBldgInfo.Item("Address").ToString
            txtBldgNotes.Text = drBldgInfo.Item("Notes").ToString
            If IsDBNull(drBldgInfo.Item("shipdate")) Then
                txtBldgShipDate.Text = ""
            Else
                txtBldgShipDate.Text = FormatDateTime(System.Convert.ToDateTime(drBldgInfo.Item("shipdate")), DateFormat.ShortDate)
            End If
            txtBldgShipSeq.Text = drBldgInfo.Item("shipsequence").ToString
        Catch ex As Exception
            MsgBox("Error Loading Bldg Details: " + ex.ToString)
        Finally
            drBldgInfo.Close()
            cnn.Close()
        End Try
    End Sub

    Private Sub BldgUnitCount()
        cnn.ConnectionString = frmMain.strConnect
        Dim strSql As String
        strSql = "Select count(*) from Unit where BldgID = " & _
               cboBldg.SelectedValue.ToString & " AND (SUBSTRING(UnitType, 1, 1) = '"
        Dim strSwitch As String
        strSwitch = txtbldgType.Text.Substring(0, 1)
        Select Case strSwitch
            Case "B"
                strSql = strSql & "U')"
            Case "C"
                strSql = strSql & "C')"
            Case "S"
                strSql = strSql & "S')"
            Case "A"
                strSql = strSql & "A')"
            Case Else
                Exit Sub
        End Select
        Dim cmdBldgUnits As New SqlCommand(strSql, cnn)
        cmdBldgUnits.CommandType = CommandType.Text

        Dim daBldgUnits As New SqlDataAdapter(cmdBldgUnits)
        Dim dsBldgUnits As New DataSet
        daBldgUnits.Fill(dsBldgUnits, "LineItems")

        Dim intUnitCnt As Integer
        If IsDBNull(dsBldgUnits.Tables("LineItems").Rows(0).Item(0)) Then
            intUnitCnt = 0
        Else
            intUnitCnt = System.Convert.ToInt32(dsBldgUnits.Tables("LineItems").Rows(0).Item(0))
        End If

        txtUnitCount.Text = (intUnitCnt.ToString)
    End Sub

    Private Sub LoadBldgUnitsDG()
        'Bldg
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdBldgUnitCnt As New SqlCommand("BldgUnitCnt", cnn)
        cmdBldgUnitCnt.CommandType = CommandType.StoredProcedure
        Dim prmBldgID As SqlParameter = cmdBldgUnitCnt.Parameters.Add("@BldgId", SqlDbType.Int)
        prmBldgID.Value = cboBldg.SelectedValue
        Dim prmProjectID As SqlParameter = cmdBldgUnitCnt.Parameters.Add("@ProjectId", SqlDbType.Int)
        prmProjectID.Value = frmMain.intBidID
        Dim daBldgUnitCnt As New SqlDataAdapter(cmdBldgUnitCnt)
        Dim dsBldgUnitCnt As New DataSet
        Try
            daBldgUnitCnt.Fill(dsBldgUnitCnt, "LineItems")
        Catch ex As Exception
            MsgBox("Error Loading Bldg Units DataGrid: " + ex.ToString)
        End Try

        dgBldg.DataSource = dsBldgUnitCnt.Tables("LineItems")
        'BldgUnitCount()
    End Sub

    Private Sub LoadComments()
        Dim strSQL As String
        strSQL = "SELECT CommentID, EntryDate, Author, Comment FROM Comment WHERE ProjectId = " & _
        frmMain.intBidID
        Dim cmdComments As New SqlCommand(strSQL, cnn)
        cmdComments.CommandType = CommandType.Text
        Dim daComments As New SqlDataAdapter(cmdComments)
        Dim dsComments As New DataSet
        Try
            daComments.Fill(dsComments, "Comments")
        Catch ex As Exception
            MsgBox("Error Loading Bldg Units DataGrid: " + ex.ToString)
        End Try

        dgComments.DataSource = dsComments.Tables("Comments")
    End Sub

    Private Sub rbBid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbBid.CheckedChanged
        'Units
        'Load with Bid Items
        LoadCatalogMaincbo()
        btnAddSO.Visible = False
    End Sub

    Private Sub rbSO_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSO.CheckedChanged
        'Units
        'Load with SO Items 
        LoadCatalogMaincbo()
        btnAddSO.Visible = True
    End Sub

    Private Sub rbMain_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbMain.CheckedChanged
        'Units
        'Load with Main Items
        LoadCatalogMaincbo()
        btnAddSO.Visible = False
    End Sub

    Private Sub AddToSO()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAdd2SO As New SqlCommand("AddCatalogSO", cnn)
        cmdAdd2SO.CommandType = CommandType.StoredProcedure

        Dim prmProdID As SqlParameter = cmdAdd2SO.Parameters.Add("@ProdID", SqlDbType.NVarChar, 255)
        prmProdID.Value = txtLIProductID.Text

        Dim prmDesc As SqlParameter = cmdAdd2SO.Parameters.Add("@Desc", SqlDbType.NVarChar, 255)
        prmDesc.Value = txtLIDesc.Text

        Dim prmCost As SqlParameter = cmdAdd2SO.Parameters.Add("@Cost", SqlDbType.Float)
        prmCost.Value = CDbl(txtLICost.Text)
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAdd2SO.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding this record:" + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub LoadLineItemSummary()
        'Summary
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdLISummary As New SqlCommand("LineItemSummary", cnn)
        cmdLISummary.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdLISummary.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daLineItemSummary As New SqlDataAdapter(cmdLISummary)
        Dim dsLineItemSummary As New DataSet
        Try
            daLineItemSummary.Fill(dsLineItemSummary, "LineItemSummary")
        Catch ex As Exception
            MsgBox("Error in LoadLineItemSummary: " + ex.ToString)
        End Try

        dgBOM.DataSource = dsLineItemSummary.Tables("LineItemSummary")
        Dim decTotalCost As Decimal
        decTotalCost = 0
        Dim decTotalSell As Decimal
        decTotalSell = 0
        Dim intRowCount As Integer
        For intRowCount = 0 To dsLineItemSummary.Tables(0).Rows.Count - 1
            decTotalCost = decTotalCost + System.Convert.ToDecimal(dsLineItemSummary.Tables(0).Rows(intRowCount).Item(5))
            decTotalSell = decTotalSell + System.Convert.ToDecimal(dsLineItemSummary.Tables(0).Rows(intRowCount).Item(6))
        Next
        'Format as Currency
        txtSummaryTotalCost.Text = (Format(decTotalCost, "C"))
        txtSummaryTotalSell.Text = (Format(decTotalSell, "C"))
        Try
            txtSummaryMargin.Text = CStr(FormatPercent(CDbl(1 - System.Convert.ToDouble((txtSummaryTotalCost.Text)) / CDbl(txtSummaryTotalSell.Text)), 2))
        Catch ex As Exception
            txtSummaryMargin.Text = ""
        End Try
        txtSummaryProfit.Text = Format((decTotalSell - decTotalCost), "C")
    End Sub

    Private Sub LoadUnitsPerBldg()
        'Summary
        cnn.ConnectionString = frmMain.strConnect
        Dim searchStr As String = "SELECT CONVERT(Varchar,BldgNumber) + Type + Building.Description as BldgType , Building.Description, Unit.UnitType," _
                        + " COUNT(UnitID) As Quantity from Building inner join Unit on Building.BldgID = Unit.BldgID where Building.ProjectID" _
                        + "= " + frmMain.intBidID.ToString + " and Unit.ProjectID = " + frmMain.intBidID.ToString + " AND LEFT(Unit.UnitType,1) = 'U'" + _
                        " Group BY CONVERT(Varchar,BldgNumber) + Type + Building.Description , Building.Description, Unit.UnitType"
        Dim cmdUnitsPerBldg As New SqlCommand(searchStr, cnn)
        cmdUnitsPerBldg.CommandType = CommandType.Text
        Dim daUnitsPerBldg As New SqlDataAdapter(cmdUnitsPerBldg)
        Dim dsUnitsPerBldg As New DataSet
        Try
            daUnitsPerBldg.Fill(dsUnitsPerBldg, "UnitsPerBldg")
        Catch ex As Exception
            MsgBox("Error in LoadUnitsPerBldg: " + ex.ToString)
        End Try

        dgBidUnitsperBldg.DataSource = dsUnitsPerBldg.Tables("UnitsPerBldg")
    End Sub

    Private Sub LoadMargins()

        txtMarUnitCost.Text = ""
        txtMarUnitSell.Text = ""
        txtMarUnitMargin.Text = ""
        txtMarCHCost.Text = ""
        txtMarCHSell.Text = ""
        txtMarCHMargin.Text = ""
        txtMarSiteCost.Text = ""
        txtMarSiteSell.Text = ""
        txtMarSiteMargin.Text = ""
        txtMarComCost.Text = ""
        txtMarComSell.Text = ""
        txtMarComMargin.Text = ""
        txtMarAuxCost.Text = ""
        txtMarAuxSell.Text = ""
        txtMarAuxMar.Text = ""

        cnn.ConnectionString = frmMain.strConnect

        Dim cmdMargins As New SqlCommand("Margins", cnn)
        cmdMargins.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drMargins As SqlDataReader
        cnn.Open()
        drMargins = cmdMargins.ExecuteReader

        While drMargins.Read
            If drMargins.GetString(0) = "U" Then
                txtMarUnitCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtMarUnitSell.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                Try
                    txtMarUnitMargin.Text = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2)))
                Catch ex As Exception
                    txtMarUnitMargin.Text = ""
                End Try

            ElseIf drMargins.GetString(0) = "C" Then
                txtMarCHCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtMarCHSell.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                Try
                    txtMarCHMargin.Text = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2)))
                Catch ex As Exception
                    txtMarCHMargin.Text = ""
                End Try
            ElseIf drMargins.GetString(0) = "S" Then
                txtMarSiteCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtMarSiteSell.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                Try
                    txtMarSiteMargin.Text = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2)))
                Catch ex As Exception
                    txtMarSiteMargin.Text = ""
                End Try

            ElseIf drMargins.GetString(0) = "X" Then
                txtMarComCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtMarComSell.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                Try
                    txtMarComMargin.Text = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2)))
                Catch ex As Exception
                    txtMarComMargin.Text = ""
                End Try
            ElseIf drMargins.GetString(0) = "A" Then
                txtMarAuxCost.Text = CStr(FormatCurrency(drMargins.GetDecimal(1)))
                txtMarAuxSell.Text = CStr(FormatCurrency(drMargins.GetDecimal(2)))
                Try
                    txtMarAuxMar.Text = CStr(FormatPercent(1 - drMargins.GetDecimal(1) / drMargins.GetDecimal(2)))
                Catch ex As Exception
                    txtMarAuxMar.Text = ""
                End Try
            End If
        End While
        drMargins.Close()
        cnn.Close()
        If CInt(txtSummaryTotalSell.Text) = 0 Then
            txtSummaryMargin.Text = "N/A"
        Else
            txtSummaryMargin.Text = CStr(FormatPercent(1 - CInt(txtSummaryTotalCost.Text) / CInt(txtSummaryTotalSell.Text)))
        End If

    End Sub

    Private Sub LoadUnitCount()
        'Summary
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdUnitCount As New SqlCommand("ProjectUnitCount", cnn)
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daUnitCount As New SqlDataAdapter(cmdUnitCount)
        Dim dsUnitCount As New DataSet
        Try
            daUnitCount.Fill(dsUnitCount, "UnitCount")
        Catch ex As Exception
            MsgBox("Error In LoadUnitCount: " + ex.ToString)
        End Try
        dgUnitCnt.DataSource = dsUnitCount.Tables("UnitCount")
    End Sub

    Private Sub rbBEMain_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBEMain.CheckedChanged
        'Bulk Edit
        LoadBECatalogCBO()
    End Sub

    Private Sub tbLIBulkEdit_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbLIBulkEdit.Enter
        If tbLIBulkEdit.Enabled Then
            LoadBEBidItemcbo()
            LoadBETypeCBO()
            LoadBECatalogCBO()
        End If
    End Sub

    Private Sub LoadBELIbyType()
        'Bulk Edit
        'Replace All Items in Units By Type
        If cboBEType.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strType As String
        strType = cboBEType.SelectedValue.ToString

        Dim spCatMainLineItem As String = "BELIbyType"

        Dim cmdBEMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdBEMainLineItem.CommandType = CommandType.StoredProcedure
        cmdBEMainLineItem.Parameters.AddWithValue("@ProjectID", frmMain.intBidID)
        cmdBEMainLineItem.Parameters.AddWithValue("@Type", cboBEType.SelectedValue.ToString)
        Dim drBEMainLineItem As SqlDataReader = Nothing
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            drBEMainLineItem = cmdBEMainLineItem.ExecuteReader
            drBEMainLineItem.Read()
            txtBEBProdID.Text = drBEMainLineItem.Item("PartNumber").ToString
            txtBEBDesc.Text = drBEMainLineItem.Item("Description").ToString
            txtBEBRT.Text = drBEMainLineItem.Item("RT").ToString
            txtBEBType.Text = drBEMainLineItem.Item("Type").ToString
            txtBEBCost.Text = drBEMainLineItem.Item("Cost").ToString
            txtBEBMargin.Text = drBEMainLineItem.Item("Margin").ToString
            txtBEBSell.Text = drBEMainLineItem.Item("Sell").ToString
        Catch ex As Exception
            MsgBox("Error In LoadBELIByType")
        Finally
            drBEMainLineItem.Close()
            cnn.Close()
        End Try
        lblBEBType.Visible = True
        txtBEBType.Visible = True
        cboBEBidItems.SelectedIndex = 0
    End Sub

    Private Sub LoadBELIbyPart()
        If cboBEBidItems.SelectedIndex <> 0 Then
            Dim strProductID As String
            strProductID = cboBEBidItems.SelectedValue.ToString

            Dim spCatMainLineItem As String = "BELIbyPart"
            Dim cmdBEMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
            cmdBEMainLineItem.CommandType = CommandType.StoredProcedure
            cmdBEMainLineItem.Parameters.AddWithValue("@ProjectID", frmMain.intBidID)
            cmdBEMainLineItem.Parameters.AddWithValue("@PartNumber", cboBEBidItems.SelectedValue.ToString)
            Dim drBEMainLineItem As SqlDataReader = Nothing
            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If

                drBEMainLineItem = cmdBEMainLineItem.ExecuteReader
                drBEMainLineItem.Read()
                txtBEBProdID.Text = drBEMainLineItem.Item("PartNumber").ToString
                txtBEBDesc.Text = drBEMainLineItem.Item("Description").ToString
                txtBEBRT.Text = drBEMainLineItem.Item("RT").ToString
                txtBEBCost.Text = CDbl(drBEMainLineItem.Item("Cost")).ToString("C")
                txtBEBMargin.Text = drBEMainLineItem.Item("Margin").ToString
                txtBEBSell.Text = Format(CDbl(drBEMainLineItem.Item("Sell")), "$###,###.#0")
            Catch ex As Exception
                MsgBox("Error in LoadBELIByPart: " + ex.ToString)
            Finally
                drBEMainLineItem.Close()
                cnn.Close()
            End Try

            lblBEBType.Visible = False
            txtBEBType.Visible = False
            cboBEType.SelectedIndex = 0
        End If
    End Sub

    Private Sub LoadCatBERLineItem()
        'Bulk Edit
        If cboBECatalog.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboBECatalog.SelectedValue.ToString

        Dim spCatMainLineItem As String = Nothing
        If rbBEMain.Checked = True Then
            spCatMainLineItem = "CatMainLineItem"
        ElseIf rbBESO.Checked = True Then
            spCatMainLineItem = "CatSOLineItem"
        End If

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboBECatalog.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader
        'Try
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
        drCatMainLineItem.Read()
        txtBERProdID.Text = drCatMainLineItem.Item("ProductID").ToString
        txtBERDesc.Text = drCatMainLineItem.Item("Description").ToString
        txtBERMargin.Text = "30"
        txtBERCost.Text = drCatMainLineItem.Item("Cost").ToString
        txtBERRT.Text = "T"
        drCatMainLineItem.Close()
        cnn.Close()
    End Sub

    Private Sub cboBEType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBEType.SelectedIndexChanged
        'Bulk Edit
        'Shows Items By Type
        LoadBELIbyType()
    End Sub

    Private Sub cboBECatalog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBECatalog.SelectedIndexChanged
        'Bulk Edit
        LoadCatBERLineItem()
    End Sub

    Private Sub BECalcSell()
        'Bulk Edit
        If txtBEBSell.Text = "" Then
            Exit Sub
        End If
        If txtBEBCost.Text = "" Then
            Exit Sub
        End If
        If cbxFSell.Checked = True Then
            txtBEBMargin.Text = CInt((1 - (CDbl(txtBEBCost.Text) / CDbl(txtBEBSell.Text))) * 100).ToString
        Else
            txtBEBSell.Text = Format(CDbl(txtBEBCost.Text) / (1 - (CDbl(txtBEBMargin.Text) / 100)), "$###,###.#0")
        End If
    End Sub

    Private Sub LICalcSell()
        'Bulk Edit
        If txtLISell.Text = "" Then
            Exit Sub
        End If
        If txtLICost.Text = "" Then
            Exit Sub
        End If
        If IsNumeric(txtLICost.Text) And IsNumeric(txtLIMargin.Text) Then
            txtLISell.Text = Format((CDbl(txtLICost.Text) / (1 - (CDbl(txtLIMargin.Text) / 100))), "$###,###.##")
        End If

    End Sub

    Private Sub txtLICost_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLICost.TextChanged
        'Units
        'LICalcSell()
        UnitCalcSell()
    End Sub

    Private Sub txtLIMargin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLIMargin.TextChanged
        'Units
        'LICalcSell()
        UnitCalcSell()
    End Sub

    Private Sub txtLIQuan_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtLIQuan.Validating
        'Units
        If txtLIQuan.Text = "" Then
            MsgBox("You Must Enter A Quantity!!")
            e.Cancel = True
            Exit Sub
        End If

        If IsNumeric(txtLIQuan.Text) = False Then
            MsgBox("You Must Enter A Number for the Quantity")
            txtLIQuan.Clear()
            e.Cancel = True
        End If

    End Sub

    Private Sub txtBEBCost_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBEBCost.Leave
        'Bulk Edit
        'Format Cost On Exit
        If Not IsNumeric(txtBEBCost.Text) Then
            Exit Sub
        End If
        txtBEBCost.Text = CDbl(txtBEBCost.Text).ToString("C")
    End Sub

    Private Sub LoadUnitLIEdit()
        'Units
        Dim AR As Integer
        AR = dgUnitLineItems.CurrentRowIndex
        intUnitDGActiveRow = AR
        If AR > -1 Then
            txtUnitEdQuan.Text = dgUnitLineItems.Item(AR, 0).ToString
            txtUnitEdLocation.Text = dgUnitLineItems.Item(AR, 3).ToString
            txtUnitEdType.Text = dgUnitLineItems.Item(AR, 5).ToString
            txtUnitEdOldType.Text = dgUnitLineItems.Item(AR, 5).ToString
            txtUnitEdOldLocation.Text = dgUnitLineItems.Item(AR, 3).ToString
            txtUnitEdQuan.Visible = True
            txtUnitEdLocation.Visible = True
            txtUnitEdType.Visible = True
            txtUnitEdOldType.Visible = True
            txtUnitEdOldLocation.Visible = True
            btnUpdateUnitLineItems.Visible = True
        End If
    End Sub

    Private Sub LoadBldgLIEdit()
        currentUnitCount = CInt(dgBldg.Item(dgBldg.CurrentRowIndex, 1))
        txtBldgEDQuan.Text = currentUnitCount.ToString
    End Sub

    Private Sub ClearBulkEditFields()
        'Bulk Edit
        txtBEBProdID.Text = ""
        txtBEBDesc.Text = ""
        txtBEBRT.Text = ""
        txtBEBType.Text = ""
        txtBEBCost.Text = ""
        txtBEBMargin.Text = ""
        txtBEBSell.Text = ""
        txtBERProdID.Text = ""
        txtBERDesc.Text = ""
        txtBERRT.Text = ""
        txtBERType.Text = ""
        txtBERCost.Text = ""
        txtBERMargin.Text = ""
        txtBERSell.Text = ""
        cbxFSell.Checked = False
        cboBEBidItems.SelectedIndex = 0
        cboBEType.SelectedIndex = 0
        cboBECatalog.SelectedIndex = 0
    End Sub

    Private Sub txtLIRT_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLIRT.KeyPress
        'Process keystroke - accept r,R,t,T - convert to upper
        Dim ch As Char
        ch = e.KeyChar
        ch = Char.ToUpper(ch)
        Select Case ch
            Case "R"c
                txtLIRT.Text = ch
            Case "T"c
                txtLIRT.Text = ch
        End Select
        e.Handled = True
    End Sub

    Private Sub frmBid_Activated(ByVal sender As Object, ByVal e As System.EventArgs)
        'If blnBldgCopy = True Then
        '    blnStartBldg = True
        '    LoadBldgs()
        'End If

        'If blnUnitReturn = True Then
        '    blnStartUnit = True
        '    LoadUnits()
        'End If

        'If blnUnitCopy = True Then
        '    cboUnit.SelectedIndex = cboUnit.FindString(strActiveUnit)
        '    LoadLineItemDG()
        'End If

        'If blnUnitDelete = True Then
        '    LoadLineItemDG()
        'End If

        ''TODO - Find out which of these is really necessary
        'LoadAltBidItems()
        'LoadAltDG()
        'LoadLineItemSummary()
        'LoadUnitsPerBldg()
        'LoadUnitCount()
        'LoadBldgCnt()
        'LoadMargins()
        'LoadCatalogMaincbo()
        'LoadcboAltCatMain()
    End Sub

    Private Sub tbAlternates_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbAlternates.Enter
        If tbAlternates.Enabled Then
            LoadAltBidItems()
            LoadAltDG()
        End If
    End Sub

    Private Sub rbAltMain_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAltMain.CheckedChanged
        LoadcboAltCatMain()
        btnAltAdd.Visible = True
    End Sub

    Private Sub rbAltSO_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAltSO.CheckedChanged
        LoadcboAltCatMain()
        btnAltAdd.Visible = True
    End Sub

    Private Sub LoadAlternateItems()
        If cboAltCatMain.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboAltCatMain.SelectedValue.ToString

        Dim spCatMainLineItem As String = Nothing
        If rbAltMain.Checked = True Then
            spCatMainLineItem = "SELECT ProductID, Description, StandardCost FROM CatalogMain WHERE ProductID = '" & strProductID & "'"
        ElseIf rbAltSO.Checked = True Then
            spCatMainLineItem = "SELECT ProductID, Description, StandardCost FROM CatalogSO WHERE ProductID = '" & strProductID & "'"
        End If

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            txtAltProdID.Text = drCatMainLineItem.Item("ProductID").ToString
            txtAltDesc.Text = drCatMainLineItem.Item("Description").ToString
            txtAltCost.Text = Format(drCatMainLineItem.Item("StandardCost"), "C")
        Catch ex As Exception
            MsgBox("Error in LoadAlternateItems")
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try
        txtAltMargin.Text = "30"
        txtAltSell.Text = Format(CDbl(txtAltCost.Text) / (1 - (CDbl(txtAltMargin.Text) / 100)), "$###,###.#0")

    End Sub

    Private Sub AddAlternateItem()
        Dim strOrgProductID As String = Nothing
        Dim strOrgDescription As String = Nothing
        Dim dblOrgCost As Double
        Dim intOrgMargin As Integer
        Dim dblOrgSell As Double
        Dim strAltProductID As String
        Dim strAltDescription As String
        Dim dblAltCost As Double
        Dim intAltMargin As Integer
        Dim dblAltSell As Double
        Dim strAltRecordSource As String

        If txtAltProdID.Text = "" Then
            MsgBox("You must enter a valid Part Number")
            Exit Sub
        End If

        If txtAltDesc.Text = "" Then
            MsgBox("You must enter a valid Description")
            Exit Sub
        End If

        If Not IsNumeric(txtAltCost.Text) Then
            MsgBox("The Cost must be positive number")
            Exit Sub
        End If

        If Not IsNumeric(txtAltMargin.Text) Then
            MsgBox("The margin must be positive number")
            Exit Sub
        End If

        If Not IsNumeric(txtAltSell.Text) Then
            MsgBox("The Selling Price must be positive number")
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect

        Dim strSQLOriginal As String
        strSQLOriginal = "SELECT PartNumber, Description, Cost, Margin, Sell FROM LineItem WHERE ProjectID = "
        strSQLOriginal = strSQLOriginal + CStr(frmMain.intBidID) + " AND PartNumber = '"
        strSQLOriginal = strSQLOriginal + RTrim(cboAltBidItems.SelectedValue.ToString) + "'"
        Dim cmdOriginal As New SqlCommand(strSQLOriginal, cnn)

        Dim drOriginal As SqlDataReader = Nothing
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            drOriginal = cmdOriginal.ExecuteReader
            drOriginal.Read()
            strOrgProductID = drOriginal.Item("PartNumber").ToString
            strOrgDescription = drOriginal.Item("Description").ToString
            dblOrgCost = System.Convert.ToDouble(drOriginal.Item("Cost"))
            intOrgMargin = System.Convert.ToInt32(drOriginal.Item("Margin"))
            dblOrgSell = System.Convert.ToDouble(drOriginal.Item("Sell"))
        Catch ex As Exception
            MsgBox("Error In AddAlternateItem Getting Data")
        Finally
            drOriginal.Close()
            cnn.Close()
        End Try

        Dim spCatMainLineItem As String = "CatMainLineItem"
        If rbAltMain.Checked = True Then
            spCatMainLineItem = "CatMainLineItem"
            strAltRecordSource = "CatMain"
        ElseIf rbAltSO.Checked = True Then
            spCatMainLineItem = "CatSOLineItem"
            strAltRecordSource = "CatSO"
        End If

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboAltCatMain.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            strAltProductID = drCatMainLineItem.Item("ProductID").ToString
            strAltDescription = drCatMainLineItem.Item("Description").ToString
            dblAltCost = System.Convert.ToDouble(drCatMainLineItem.Item("Cost"))
        Catch ex As Exception
            MsgBox("Error in AddAlternateItem spCatMainLineItem")
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try
        intAltMargin = 30
        dblAltSell = (CDbl(dblAltCost) / (1 - (CDbl(intAltMargin) / 100)))

        Dim cmdAddAlternate As New SqlCommand("Alternate", cnn)
        cmdAddAlternate.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAddAlternate.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmOrgProductID As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgProductID", SqlDbType.NVarChar, 255)
        prmOrgProductID.Value = strOrgProductID

        Dim prmOrgDescription As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgDescription", SqlDbType.NVarChar, 25)
        prmOrgDescription.Value = strOrgDescription

        Dim prmOrgCost As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgCost", SqlDbType.Money)
        prmOrgCost.Value = CDec(dblOrgCost)

        Dim prmOrgMargin As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgMargin", SqlDbType.Int)
        prmOrgMargin.Value = CInt(intOrgMargin)

        Dim prmOrgSell As SqlParameter = cmdAddAlternate.Parameters.Add("@OrgSell", SqlDbType.Money)
        prmOrgSell.Value = CDec(dblOrgSell)

        Dim prmAltProductID As SqlParameter = cmdAddAlternate.Parameters.Add("@AltProductID", SqlDbType.NVarChar, 255)
        prmAltProductID.Value = txtAltProdID.Text

        Dim prmAltDescription As SqlParameter = cmdAddAlternate.Parameters.Add("@AltDescription", SqlDbType.NVarChar, 25)
        prmAltDescription.Value = txtAltDesc.Text

        Dim prmAltCost As SqlParameter = cmdAddAlternate.Parameters.Add("@AltCost", SqlDbType.Money)
        If Not IsNumeric(txtAltCost.Text) Then
            prmAltCost.Value() = 0
        Else
            prmAltCost.Value = CDec(txtAltCost.Text)
        End If

        Dim prmAltMargin As SqlParameter = cmdAddAlternate.Parameters.Add("@AltMargin", SqlDbType.Int)
        If Not IsNumeric(txtAltMargin.Text) Then
            prmAltMargin.Value = 0
        Else
            prmAltMargin.Value = CInt(txtAltMargin.Text)
        End If

        Dim prmAltSell As SqlParameter = cmdAddAlternate.Parameters.Add("@AltSell", SqlDbType.Money)
        If Not IsNumeric(txtAltSell.Text) Then
            prmAltSell.Value = 0
        Else
            prmAltSell.Value = CDec(txtAltSell.Text)
        End If

        Dim prmAltRecordSource As SqlParameter = cmdAddAlternate.Parameters.Add("@AltRecordSource", SqlDbType.Char, 10)
        If rbAltMain.Checked = True Then
            prmAltRecordSource.Value = "CatMain"
        ElseIf rbAltSO.Checked = True Then
            prmAltRecordSource.Value = "CatSO"
        Else
            prmAltRecordSource.Value = ""
        End If

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAddAlternate.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error adding in AddAlternateLineItem!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub cboAltCatMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAltCatMain.SelectedIndexChanged
        LoadAlternateItems()
    End Sub

    Private Sub LoadAltDG()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAltSummary As New SqlCommand("AlternateSummary", cnn)
        cmdAltSummary.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdAltSummary.Parameters.Add("@ProjectId", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daAltSummary As New SqlDataAdapter(cmdAltSummary)
        Dim dsAltSummary As New DataSet
        Try
            daAltSummary.Fill(dsAltSummary, "LineItems")
            dgAlt.DataSource = dsAltSummary.Tables("LineItems")
        Catch ex As Exception
            MsgBox("Error in LoadAltDB")
        End Try

    End Sub

    Private Sub LoadBldgCnt()
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdUnitCount As New SqlCommand("rptUnitCount", cnn)
        cmdUnitCount.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdUnitCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim drUnitCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drUnitCount = cmdUnitCount.ExecuteReader
            drUnitCount.Read()
            txtBidUnitCnt.Text = drUnitCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count:" + ex.ToString)
        Finally
            drUnitCount.Close()
            cnn.Close()
        End Try

        Dim cmdBldgCount As New SqlCommand("rptBldgCount", cnn)
        cmdBldgCount.CommandType = CommandType.StoredProcedure
        Dim prmBldgBidID As SqlParameter = cmdBldgCount.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBldgBidID.Value = frmMain.intBidID
        Dim drBldgCount As SqlDataReader = Nothing
        cnn.Open()
        Try
            drBldgCount = cmdBldgCount.ExecuteReader
            drBldgCount.Read()
            txtBidBldgCnt.Text = drBldgCount.GetValue(0).ToString
        Catch ex As Exception
            MsgBox("Error in processing Unit Count")
        Finally
            drBldgCount.Close()
            cnn.Close()
        End Try
    End Sub

    Private Sub ShowUnitEdit()
        BubtnDeleteUnitLineItems.Visible = True
        btnCancelUnitLIneItem.Visible = True
    End Sub

    Private Sub HideUnitEdit()
        BubtnDeleteUnitLineItems.Visible = False
        btnCancelUnitLIneItem.Visible = False
    End Sub

    Private Sub ShowBldgEdit()
        txtBldgEDQuan.Visible = True
        btnBldgLIDelete.Visible = True
        btnBldgLIUpdate.Visible = True
        btnBldgLICancel.Visible = True
    End Sub

    Private Sub HideBldgEdit()
        txtBldgEDQuan.Visible = False
        btnBldgLIDelete.Visible = False
        btnBldgLIUpdate.Visible = False
        btnBldgLICancel.Visible = False
    End Sub

    Private Sub txtLICost_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLICost.Leave
        If txtLICost.Text = "" Then
            Exit Sub
        End If
        txtLIMargin.Text = "30"
        txtLISell.Text = Format((CDbl(txtLICost.Text) / (1 - (CDbl(txtLIMargin.Text) / 100))), "$###,###.##")
    End Sub

    Private Sub tbUnits_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbUnits.GotFocus
        LoadUnits()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub UnitCalcSell()
        'Bulk Edit
        If txtLISell.Text = "" Then
            Exit Sub
        End If
        If txtLICost.Text = "" Then
            Exit Sub
        End If
        If cbxUFixedSell.Checked = True Then
            txtLIMargin.Text = System.Convert.ToString(CInt((1 - (CDbl(txtLICost.Text) / CDbl(txtLISell.Text))) * 100))
        Else
            txtLISell.Text = Format(CDbl(txtLICost.Text) / (1 - (CDbl(txtLIMargin.Text) / 100)), "$###,###.#0")
        End If
    End Sub

    Private Sub txtLISell_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLISell.TextChanged
        UnitCalcSell()
    End Sub

    Private Sub txtBERRT_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBERRT.KeyPress
        'Process keystroke - accept r,R,t,T - convert to upper
        Dim ch As Char
        ch = e.KeyChar
        ch = Char.ToUpper(ch)
        Select Case ch
            Case "R"c
                txtBERRT.Text = ch
            Case "T"c
                txtBERRT.Text = ch
        End Select
        e.Handled = True
    End Sub

    Private Sub LoadCBOSParts()

        cnn.ConnectionString = frmMain.strConnect

        Dim strSPartNumber As String
        strSPartNumber = "SELECT DISTINCT(PartNumber) FROM LineItem WHERE ProjectId = " & frmMain.intBidID
        Dim daSParts As New SqlDataAdapter(strSPartNumber, cnn)
        Dim dsSParts As New DataSet
        Try
            daSParts.Fill(dsSParts, "Parts")
        Catch ex As Exception
            MsgBox("Error Loading SParts")
        End Try

        Dim dtSParts As DataTable = dsSParts.Tables("Parts")
        Dim dr As DataRow = dtSParts.NewRow
        'Initialize DataSet First Row
        dr("PartNumber") = ""

        dtSParts.Rows.InsertAt(dr, 0)

        cboSPartNum.DataSource = dsSParts.Tables("Parts")
        cboSPartNum.DisplayMember = "PartNumber"

    End Sub

    Private Sub LoadCBOSLocations()

        cnn.ConnectionString = frmMain.strConnect

        Dim strSLocation As String
        strSLocation = "SELECT DISTINCT(Location) FROM LineItem WHERE ProjectId = " & frmMain.intBidID
        Dim daSLocations As New SqlDataAdapter(strSLocation, cnn)
        Dim dsSLocations As New DataSet
        Try
            daSLocations.Fill(dsSLocations, "Locations")
        Catch ex As Exception
            MsgBox("Error Loading SLocations")
        End Try

        Dim dtSLocations As DataTable = dsSLocations.Tables("Locations")
        Dim dr As DataRow = dtSLocations.NewRow
        'Initialize DataSet First Row
        dr("Location") = ""

        dtSLocations.Rows.InsertAt(dr, 0)

        cboSLocation.DataSource = dsSLocations.Tables("Locations")
        cboSLocation.DisplayMember = "Location"

    End Sub

    Private Sub LoadCBOSTypes()
        cnn.ConnectionString = frmMain.strConnect

        Dim strSType As String
        strSType = "SELECT DISTINCT(Type) FROM LineItem WHERE ProjectId = " & frmMain.intBidID
        strSType = strSType & " ORDER BY Type"
        Dim daSTypes As New SqlDataAdapter(strSType, cnn)
        Dim dsSTypes As New DataSet
        Try
            daSTypes.Fill(dsSTypes, "Types")
        Catch ex As Exception
            MsgBox("Error Loading STypes")
        End Try

        Dim dtSTypes As DataTable = dsSTypes.Tables("Types")
        Dim dr As DataRow = dtSTypes.NewRow
        'Initialize DataSet First Row
        dr("Type") = ""

        dtSTypes.Rows.InsertAt(dr, 0)

        cboSType.DataSource = dsSTypes.Tables("Types")
        cboSType.DisplayMember = "Type"
    End Sub

    Private Sub UpdateCosts()
        Dim UpdateCosts As New SqlCommand("UpdateProjectCost", cnn)
        UpdateCosts.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = UpdateCosts.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            UpdateCosts.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error Updating The Costs:" + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub UpdateDescriptions()
        Dim UpdateDesc As New SqlCommand("UpdateProjectItemDesc", cnn)
        UpdateDesc.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = UpdateDesc.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            UpdateDesc.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error Updating The Descriptions: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub txtBEBRT_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBEBRT.KeyPress
        Dim ch As Char
        ch = e.KeyChar
        ch = Char.ToUpper(ch)
        Select Case ch
            Case "R"c
                txtBEBRT.Text = ch
            Case "T"c
                txtBEBRT.Text = ch
        End Select
        e.Handled = True
    End Sub

    Private Sub txtBERCost_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBERCost.TextChanged
        If txtBERCost.Text = "" Then
            Exit Sub

        End If
        If IsNumeric(txtBERCost.Text) Then
            txtBERSell.Text = Format(CDbl(txtBERCost.Text) / (1 - (CDbl(txtBERMargin.Text) / 100)), "$###,###.#0")
        End If
    End Sub

    Private Sub frmBid_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        frmMain.blnBidRunning = False
        blnUnitCopy = False
        blnBldgCopy = False

    End Sub

    Private Sub frmBid_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs)
        blnUnitCopy = False
        blnBldgCopy = False
        blnUnitReturn = False
    End Sub

    Private Sub mnuAdjustMargins_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAdjustMargins.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Snapshot As New frmAdjustMargins
        Snapshot.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnPrintSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintSearch.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim PrintSearch As New frmPrintSearch
        PrintSearch.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub


    Private Sub mnuEditCosts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditCosts.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim CompareCosts As New frmCompareBidCosts
        CompareCosts.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuShipDates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuShipDates.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim ShipDates As New frmBulkShipDate
        ShipDates.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub


    Private Sub mnuCopyUnitOtherBid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopyUnitOtherBid.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim CopyUnitOB As New frmCopyUnitFromOtherBid
        CopyUnitOB.ShowDialog()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.WaitCursor
        LoadCatalogMaincbo()
        blnStartUnit = True
        blnStartBldg = True
        blnUnitReturn = False
        blnUnitCopy = False
        blnBldgCopy = False
        HideBldgUnits()
        LoadBldgs()
        LoadUnits()

        dgUnitLineItems.DataSource = Nothing
        TabCtrlBid.SelectedIndex = 0
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnSaveNewBid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveNewBid.Click
        If txtbDescription.Text = "" Then
            MsgBox("You Must Enter A Bid Description!")
            txtbDescription.Focus()
            Exit Sub
        End If

        If txtBLocation.Text = "" Then
            MsgBox("You Must Enter A Bid Location!")
            txtBLocation.Focus()
            Exit Sub
        End If
        If cboClients.SelectedIndex = 0 Then
            MsgBox("You Must Select A Client'")
            Exit Sub
        End If

        If Not IsDate(txtBluePrintDate.Text) Then
            MsgBox("You must enter a valid Blue Print Date")
            Exit Sub
        End If
        AddNewBid()
        'Set Tab To Units
        LoadEditBidForm()
        GoToBid()
    End Sub

    Private Sub btnNewUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewUnit.Click
        'ShowNewUnit()
        Dim Unit As New frmUnit
        Unit.ShowDialog()
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Cursor = Cursors.WaitCursor
        HideAll()
        cboBid.Visible = True
        LoadBids()
        mnuUtilities.Visible = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItem.Click
        If txtLIProductID.Text = "" Then
            MsgBox("There must be a Product ID Entered!")
            Exit Sub
        End If
        If txtLIDesc.Text = "" Then
            MsgBox("There must be a Product Description Entered!")
            Exit Sub
        End If
        If txtLILocation.Text = "" Then
            MsgBox("There must be a Location Entered!")
            Exit Sub
        End If
        If txtLIQuan.Text = "" Then
            MsgBox("There must be a Quantity Entered!")
            Exit Sub
        End If
        If System.Convert.ToInt32(txtLIMargin.Text) < 0 Then
            Dim confirm As DialogResult = MessageBox.Show("Margin is negative. Is this O.K.?", "Caption", _
            MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
            If confirm = DialogResult.No Then
                Exit Sub
            End If
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim strItem As String
        strItem = "SELECT LineItem.ItemID,  LineItem.PartNumber, LineItem.UnitID, LineItem.Location, LineItem.Type " + _
            "FROM LineItem inner join Unit on LineItem.UnitID = Unit.UnitID"
        strItem = strItem + " WHERE(Unit.UnitType = '" + cboUnit.SelectedValue.ToString + "') AND (LineItem.Location = '" + txtLILocation.Text + "' )"
        strItem = strItem + " AND (LineItem.Type = '" + txtLIType.Text + "') AND (LineItem.PartNumber = '" + txtLIProductID.Text + "')" + _
            " AND  LineItem.ProjectID = " + frmMain.intBidID.ToString

        Dim cmdItem As New SqlCommand(strItem, cnn)
        Dim drItem As SqlDataReader

        cnn.Open()
        drItem = cmdItem.ExecuteReader

        If drItem.HasRows Then
            MsgBox("There is already an entry for this part in the location of this type!")
            drItem.Close()
            cnn.Close()
            LoadLineItemDG()
            Exit Sub
        End If
        drItem.Close()
        cnn.Close()

        AddUnitLineItemsFromCatalog()
        AddBulbsToUnitLineItem()
        If cbxClearOnAdd.Checked = True Then
            ClearLineItemFields()
        End If


    End Sub

    Private Sub btnAddBldg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBldg.Click
        'bldg
        Dim EditBldg As New frmBldgs
        EditBldg.ShowDialog()
        If tbBuilding.Enabled Then
            LoadBldgs()
        End If
    End Sub

    Private Sub AddUnitToBldg(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddUnitToBldg.Click
        'Bldg
        'Function based on caption
        If btnAddUnitToBldg.Text = "Add Units" Then
            LoadBldgUnits()
            lblBldgSelUnit.Visible = True
            lblBldgUnitQuan.Visible = True
            cboBldgUnits.Visible = True
            txtBldgUnitQuan.Visible = True
            btnAddUnitToBldg.Text = "Save"
        Else
            If Not IsNumeric(txtBldgUnitQuan.Text) Then
                MsgBox("You Must Enter A Whole Number In The Quantity Box!")
                txtBldgUnitQuan.Focus()
                Exit Sub
            End If
            AddUnitsToBldg()
            LoadBldgUnitsDG()
            'txtBldgUnitQuan.Text = ""
        End If
        cboBldgUnits.Focus()
    End Sub

    Private Sub AddUnitsToBldg()
        'Check to see if the unit hsa already been added to the building
        'If not add that many unit rows
        'we also need to make new copies of each line item associated with that unit
        Dim checkString As String
        Dim BldgID As Integer
        Dim UnitType As String
        Dim Quantity As Integer

        If cnn.State = ConnectionState.Open Then
            cnn.Close()
        End If
        cnn.ConnectionString = frmMain.strConnect
        BldgID = System.Convert.ToInt32(cboBldg.SelectedValue)
        UnitType = cboBldgUnits.SelectedValue.ToString
        Quantity = System.Convert.ToInt32(txtBldgUnitQuan.Text)
        checkString = "Select * from Unit where BldgID = " + BldgID.ToString + " and UnitType = '" + UnitType + "' and ProjectId = " + frmMain.intBidID.ToString
        Dim cmdCheckUnitExists As New SqlCommand(checkString, cnn)
        Dim CUEDR As SqlDataReader

        cnn.Open()
        CUEDR = cmdCheckUnitExists.ExecuteReader

        If Not CUEDR.HasRows Then
            For i As Integer = 1 To Quantity
                CommonDataAccess.AddUnitToBldg(CStr(BldgID), UnitType, frmMain.intBidID.ToString, cnn, UnitType.Trim + "-" + i.ToString)
            Next
        Else
            MsgBox("That unit has already been added to the building. If you would like to add more please use the control below.")
        End If
        cnn.Close()

    End Sub

    Private Sub btnAddSO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSO.Click
        'Units
        blnAddSO = True
        strSOProductID = txtLIProductID.Text
        strSODesc = txtLIDesc.Text
        If IsNumeric(txtLICost.Text) Then
            dblSOCost = CDbl(txtLICost.Text).ToString
        Else
            MsgBox("Cost must be entered as a number!")
            Exit Sub
        End If
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim CatSO As New frmCatSO
        CatSO.ShowDialog()
        Me.Cursor = Cursors.Default
        'AddToSO()
        'LoadCatalogMaincbo()
    End Sub

    Private Sub btnLoadSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Summary
        LoadLineItemSummary()
        LoadUnitsPerBldg()
        LoadUnitCount()
        LoadBldgCnt()
        LoadMargins()
    End Sub

    Private Sub btnBEDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBEDelete.Click
        'Bulk Edit
        Dim strprompt As String
        strprompt = "You are about to Delete All Instances of --- " & RTrim(txtBEBProdID.Text.ToString) & "!!" & vbCrLf & _
        "                                  CONTINUE????"
        Dim strGo As String
        strGo = MsgBox(strprompt, MsgBoxStyle.OkCancel, "Delete Part Number").ToString
        If strGo = "1" Then
            Dim cmdBEDelete As New SqlCommand("DeleteLineItems", cnn)
            cmdBEDelete.CommandType = CommandType.StoredProcedure
            cmdBEDelete.Parameters.AddWithValue("@ProjectID", frmMain.intBidID)
            cmdBEDelete.Parameters.AddWithValue("@PartNumber", txtBEBProdID.Text.ToString)

            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            Try
                cmdBEDelete.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Error In btnEDelete_Click")
            Finally
                cnn.Close()

            End Try
        End If

        LoadBEBidItemcbo()
        LoadBETypeCBO()
        LoadLineItemDG()
        ClearBulkEditFields()
    End Sub

    Private Sub btnBERByPartNum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBERByPartNum.Click

        Dim intBidID As Integer
        Dim strOPartNumber As String
        Dim strNPartNumber As String
        Dim strNDescription As String
        Dim dblNCost As Double
        Dim intNMargin As Integer
        Dim dblNSell As Double
        Dim Conn As New SqlConnection(frmMain.strConnect)

        If txtBEBProdID.Text = "" Then
            MsgBox("Invalid Original Part Number")
            Exit Sub
        End If

        If txtBERProdID.Text = "" Then
            MsgBox("Invalid Replacement Part Number")
            Exit Sub
        End If

        If txtBEBCost.Text = "" Then
            MsgBox("Original Cost Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBERCost.Text) Then
            MsgBox("Replacement Cost Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBEBMargin.Text) Then
            MsgBox("Original Margin Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBERMargin.Text) Then
            MsgBox("Replacement Margin Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBEBSell.Text) Then
            MsgBox("Original Sell Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBERSell.Text) Then
            MsgBox("Replacement Sell Must Be Numeric")
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect

        intBidID = frmMain.intBidID
        strOPartNumber = txtBEBProdID.Text
        strNPartNumber = txtBERProdID.Text
        strNDescription = txtBERDesc.Text
        dblNCost = CDbl(txtBERCost.Text)
        intNMargin = CInt(txtBERMargin.Text)
        If IsNumeric(txtBERSell.Text) Then
            dblNSell = CDbl(txtBERSell.Text)
        Else
            dblNSell = 0
        End If

        Dim strprompt As String
        strprompt = "You are about to Replace --- " & strOPartNumber & vbCrLf & vbCrLf & _
         "With  --- " & strNPartNumber & "!" & vbCrLf & vbCrLf & _
        "CONTINUE????"
        If MsgBox(strprompt, MsgBoxStyle.OkCancel, "Replace By Part Number") = MsgBoxResult.Ok Then
            'Update all lineitems in the project with that partnumber, description, R/T, cost, margin and sell
            Dim updateStr As String = "Update LineItem set PartNumber = '" + strNPartNumber + "', Description = '" + _
                strNDescription + "', RT = '" + txtBEBRT.Text + "', Cost = " + dblNCost.ToString + ", Sell = " + dblNSell.ToString + _
                " where ProjectID = " + frmMain.intBidID.ToString + " and PartNumber = '" + strOPartNumber + "'"
            ExecuteQueryText(updateStr, Conn)
        End If

        LoadBEBidItemcbo()
        LoadBETypeCBO()
        ClearBulkEditFields()
    End Sub

    Private Sub btnBERByType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBERByType.Click
        'Bulk Edit
        If txtBEBProdID.Text = "" Then
            MsgBox("Invalid Original Part Number")
            Exit Sub
        End If

        If txtBERProdID.Text = "" Then
            MsgBox("Invalid Replacement Part Number")
            Exit Sub
        End If

        If txtBEBCost.Text = "" Then
            MsgBox("Original Cost Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBERCost.Text) Then
            MsgBox("Replacement Cost Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBEBMargin.Text) Then
            MsgBox("Original Margin Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBERMargin.Text) Then
            MsgBox("Replacement Margin Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBEBSell.Text) Then
            MsgBox("Original Sell Must Be Numeric")
            Exit Sub
        End If

        If Not IsNumeric(txtBERSell.Text) Then
            MsgBox("Replacement Sell Must Be Numeric")
            Exit Sub
        End If
        Dim strprompt As String
        strprompt = "You are about to Replace All Instances of Type " & RTrim(txtBEBType.Text.ToString) & vbCrLf & _
            " With -- " & RTrim(txtBERProdID.Text.ToString) & "!!" & vbCrLf & _
            "      CONTINUE????"

        If MsgBox(strprompt, MsgBoxStyle.OkCancel, "Delete Part Number") = MsgBoxResult.Ok Then
            ' Check for Dupe Parts of Same Type
            Dim strSQL As String
            'Populate Client TextBoxes From Stored Proecedure And DataReader
            strSQL = "SELECT Distinct PartNumber, Type From lineitem " & _
                "where Projectid = " & frmMain.intBidID & " AND Type = '" & txtBEBType.Text & "'"
            Dim cmdDupeType As New SqlCommand(strSQL, cnn)
            cmdDupeType.CommandType = CommandType.Text
            Dim daDupeType As New SqlDataAdapter(cmdDupeType)
            Dim dsDupeType As New DataSet
            daDupeType.Fill(dsDupeType, "Types")
            If dsDupeType.Tables(0).Rows.Count > 1 Then
                MsgBox("This Type refers to multiple Part Numbers in this bid. It cannot be used in this procedure!!")
                Exit Sub
            End If

            Dim cmdBEUpdateByType As New SqlCommand("UpdateLineItemByType", cnn)
            cmdBEUpdateByType.CommandType = CommandType.StoredProcedure

            Dim prmType As SqlParameter = cmdBEUpdateByType.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
            prmType.Value = txtBEBType.Text

            cmdBEUpdateByType.Parameters.AddWithValue("@ProjectID", frmMain.intBidID)

            Dim prmRecordSource As SqlParameter = cmdBEUpdateByType.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
            If rbBEMain.Checked = True Then
                prmRecordSource.Value = "CatMain"
            ElseIf rbBESO.Checked = True Then
                prmRecordSource.Value = "CatSO"
            Else
                prmRecordSource.Value = ""
            End If

            Dim prmPartNumber As SqlParameter = cmdBEUpdateByType.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
            prmPartNumber.Value = txtBERProdID.Text

            Dim prmRT As SqlParameter = cmdBEUpdateByType.Parameters.Add("@RT", SqlDbType.Char, 1)
            prmRT.Value = txtBERRT.Text

            Dim prmDescription As SqlParameter = cmdBEUpdateByType.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
            prmDescription.Value = txtBERDesc.Text

            Dim prmCost As SqlParameter = cmdBEUpdateByType.Parameters.Add("@Cost", SqlDbType.Money)
            If IsNumeric(txtBERCost.Text) Then
                prmCost.Value = CDbl(txtBERCost.Text)
            Else
                prmCost.Value = 0
            End If

            Dim prmMargin As SqlParameter = cmdBEUpdateByType.Parameters.Add("@Margin", SqlDbType.Int)
            If IsNumeric(txtBERMargin.Text) Then
                prmMargin.Value = CInt(txtBERMargin.Text)
            Else
                prmMargin.Value = 30
            End If


            Dim prmSell As SqlParameter = cmdBEUpdateByType.Parameters.Add("@Sell", SqlDbType.Money)
            If IsNumeric(txtBERSell.Text) Then
                prmSell.Value = CDbl(txtBERSell.Text)
            Else
                prmSell.Value = 0
            End If

            Dim prmOldPartNumber As SqlParameter = cmdBEUpdateByType.Parameters.Add("@OldPartNumber", SqlDbType.Char, 25)
            prmOldPartNumber.Value = txtBEBProdID.Text


            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            Try
                cmdBEUpdateByType.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Error Replacing By Type: " + ex.ToString)
            Finally
                cnn.Close()
            End Try
        End If
        LoadBEBidItemcbo()
        LoadBETypeCBO()
        ClearBulkEditFields()
    End Sub

    Private Sub btnUpdatePrice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdatePrice.Click
        'Bulk Edit
        BECalcSell()
    End Sub

    Private Sub btnBEAdjustSell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBEAdjustSell.Click

        Dim cmdBEUpdatePrice As New SqlCommand("UpdatePrice", cnn)
        cmdBEUpdatePrice.CommandType = CommandType.StoredProcedure
        cmdBEUpdatePrice.Parameters.AddWithValue("@ProjectID", frmMain.intBidID)
        cmdBEUpdatePrice.Parameters.AddWithValue("@PartNumber", txtBEBProdID.Text.ToString)

        Dim prmCost As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Cost", SqlDbType.Money)
        If IsNumeric(txtBEBCost.Text) Then
            prmCost.Value = CDbl(txtBEBCost.Text)
        Else
            MsgBox("You must enter a Number For Cost!!")
            Exit Sub
        End If

        Dim prmMargin As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Margin", SqlDbType.Int)
        If IsNumeric(txtBEBMargin.Text) Then
            prmMargin.Value = CInt(txtBEBMargin.Text)
        Else
            MsgBox("You must enter a Whole Number For Margin!!")
            Exit Sub
        End If

        Dim prmSell As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Sell", SqlDbType.Money)
        If IsNumeric(txtBEBSell.Text) Then
            prmSell.Value = CDbl(txtBEBSell.Text)
        Else
            MsgBox("You must enter a Whole Number For Margin!!")
            Exit Sub
        End If

        Dim prmDesc As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDesc.Value = txtBEBDesc.Text

        Dim prmRT As SqlParameter = cmdBEUpdatePrice.Parameters.Add("@RT", SqlDbType.Char, 1)
        prmRT.Value = txtBEBRT.Text

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            cmdBEUpdatePrice.ExecuteNonQuery()
        Catch ex As Exception
        Finally
            cnn.Close()
        End Try
        ClearBulkEditFields()
    End Sub

    Private Sub btnUpdateUnitLineItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateUnitLineItems.Click
        'Units
        'intUnitEditing = cboUnit.SelectedIndex
        If txtUnitEdQuan.Text = "" Then
            MsgBox("There must be a Quantity Entered!")
            Exit Sub
        End If
        If txtUnitEdLocation.Text = "" Then
            MsgBox("There must be a Location Entered!")
            Exit Sub
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdUpdateLineItemQuan As New SqlCommand
        Dim strCommandText As String
        strCommandText = "Update LineItem Set Quantity = "
        strCommandText = strCommandText & txtUnitEdQuan.Text
        strCommandText = strCommandText & ", Location = '"
        strCommandText = strCommandText & txtUnitEdLocation.Text
        strCommandText = strCommandText & "', Type = '"
        strCommandText = strCommandText & txtUnitEdType.Text
        strCommandText = strCommandText & "' WHERE (ProjectID = "
        strCommandText = strCommandText & frmMain.intBidID
        strCommandText = strCommandText & ") AND (UnitID in (select UnitID from Unit where UnitType = '" + _
        cboUnit.SelectedValue.ToString + "' and ProjectID = " + frmMain.intBidID.ToString + " ))"
        strCommandText = strCommandText & " AND (Location = '"
        strCommandText = strCommandText & txtUnitEdOldLocation.Text
        strCommandText = strCommandText & "') AND (Type = '"
        strCommandText = strCommandText & txtUnitEdOldType.Text
        strCommandText = strCommandText & "')"

        cmdUpdateLineItemQuan.CommandText = strCommandText
        cmdUpdateLineItemQuan.Connection = cnn
        Try
            cnn.Open()
            cmdUpdateLineItemQuan.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error in btnUpdateUnitLineItems: " + ex.ToString)
        Finally
            cnn.Close()
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        HideUnitEdit()
        LoadLineItemDG()
        dgUnitLineItems.CurrentRowIndex = intUnitDGActiveRow
    End Sub

    Private Sub dgUnitLineItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgUnitLineItems.Click
        ShowUnitEdit()
        LoadUnitLIEdit()
    End Sub

    Private Sub BubtnDeleteUnitLineItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BubtnDeleteUnitLineItems.Click
        Dim Row As Integer
        Dim PN As String
        Dim Desc As String
        Dim Loc As String
        Dim RT As String
        Dim Type As String
        Dim Cost As String
        Dim Margin As String
        Dim Sell As String
        Dim cmdDeleteLineItem As New SqlCommand
        Dim strCommandText As String

        Row = dgUnitLineItems.CurrentCell.RowNumber
        PN = dgUnitLineItems.Item(Row, 1).ToString
        Desc = dgUnitLineItems.Item(Row, 2).ToString
        Loc = dgUnitLineItems.Item(Row, 3).ToString
        RT = dgUnitLineItems.Item(Row, 4).ToString
        Type = dgUnitLineItems.Item(Row, 5).ToString
        Cost = dgUnitLineItems.Item(Row, 6).ToString
        Margin = dgUnitLineItems.Item(Row, 7).ToString
        Sell = dgUnitLineItems.Item(Row, 8).ToString

        strCommandText = "Delete from LineItem where PartNumber = '" + PN + "' and Description = '" + Desc + "' and Location ='" + _
            Loc + "' and RT = '" + RT + "' and Type = '" + Type + "' and Cost = " + Cost + " and Margin = " + Margin + " and Sell = " + Sell _
            + " and ProjectID = " + frmMain.intBidID.ToString + " and UnitID in (Select UnitID from Unit where UnitType = '" + cboUnit.SelectedValue.ToString + _
            "' and ProjectID = " + frmMain.intBidID.ToString + ")"

        cmdDeleteLineItem.CommandText = strCommandText
        cmdDeleteLineItem.Connection = cnn
        Try
            cnn.Open()
            cmdDeleteLineItem.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error Deleting LineItem: " + ex.ToString)
        Finally
            cnn.Close()

        End Try


        HideUnitEdit()
        LoadLineItemDG()
        If System.Convert.ToDouble(intUnitDGActiveRow) = 0 Then
            dgUnitLineItems.CurrentRowIndex = intUnitDGActiveRow
        Else
            dgUnitLineItems.CurrentRowIndex = intUnitDGActiveRow - 1
        End If

    End Sub

    Private Sub MainBid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Clients As New frmRptMain
        Clients.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub dgBldg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgBldg.Click
        LoadBldgLIEdit()
        ShowBldgEdit()
    End Sub

    Private Sub btnBldgLIUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBldgLIUpdate.Click
        If Not IsNumeric(txtBldgEDQuan.Text) Then
            MsgBox("You must enter a Whole Number for the Quantity")
            Exit Sub
        End If
        Dim UnitType As String = dgBldg.Item(dgBldg.CurrentRowIndex, 2).ToString
        If CInt(txtBldgEDQuan.Text) > currentUnitCount Then
            'We are trying to increase the the number of units
            For I As Integer = currentUnitCount + 1 To CInt(txtBldgEDQuan.Text)
                CommonDataAccess.AddUnitToBldg(cboBldg.SelectedValue.ToString, UnitType, frmMain.intBidID.ToString, cnn, dgBldg.Item(dgBldg.CurrentRowIndex, 2).ToString + I.ToString)
            Next
        Else
            'Lower them by deleting 
            Dim delLI As String = "delete from LineItem where unitid in ( select top " + CStr(currentUnitCount - CInt(txtBldgEDQuan.Text)) + " UnitID from Unit where BldgID = " + cboBldg.SelectedValue.ToString + _
                " and ProjectID = " + frmMain.intBidID.ToString + " and UnitType = '" + UnitType + "' order by unitname desc) and ProjectID = " + frmMain.intBidID.ToString
            ExecuteQueryText(delLI, cnn)

            Dim strDelete As String = "delete Units from( select top " + CStr(currentUnitCount - CInt(txtBldgEDQuan.Text)) + " UnitName from Unit where BldgID = " + cboBldg.SelectedValue.ToString + _
                " and ProjectID = " + frmMain.intBidID.ToString + " and UnitType = '" + UnitType + "' order by unitname desc) units"
            ExecuteQueryText(strDelete, cnn)
        End If

        HideBldgEdit()
        LoadBldgUnitsDG()
        txtBldgEDQuan.Text = ""
        dgBldg.Focus()
    End Sub

    Private Sub btnBldgLIDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBldgLIDelete.Click
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdDeleteUnitsFromBldg As New SqlCommand
        Dim strCommandText As String

        strCommandText = "delete from lineitem where bldgid = " + cboBldg.SelectedValue.ToString + " and ProjectID = " + frmMain.intBidID.ToString _
            + " and UnitID in (select UnitID from Unit where unittype = '" + dgBldg.Item(dgBldg.CurrentRowIndex, 2).ToString + "' and bldgid = " + _
            cboBldg.SelectedValue.ToString + " and ProjectID = " + frmMain.intBidID.ToString + ")"
        ExecuteQueryText(strCommandText, cnn)

        strCommandText = "delete from unit where bldgid = " + cboBldg.SelectedValue.ToString + " and ProjectID = " + frmMain.intBidID.ToString _
            + " and UnitType = '" + dgBldg.Item(dgBldg.CurrentRowIndex, 2).ToString + "'"
        ExecuteQueryText(strCommandText, cnn)

        HideBldgEdit()
        LoadBldgUnitsDG()
        dgBldg.Focus()
    End Sub

    Private Sub btnCopyUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyUnit.Click
        'Me.Cursor = Cursors.WaitCursor
        Dim CopyUnit As New frmCopyUnit
        CopyUnit.ShowDialog()
    End Sub

    Private Sub btnbEditBid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbEditBid.Click
        If btnbEditBid.Text = "Save" Then
            EditBid()
            btnbEditBid.Text = "Edit"
        Else
            btnbEditBid.Text = "Save"
        End If

    End Sub

    Private Sub btnAltAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltAdd.Click
        If cboAltBidItems.SelectedIndex = 0 Then
            MsgBox("You Must Select An Item From The Bid!")
            Exit Sub
        End If
        If cboAltCatMain.SelectedIndex = 0 Then
            MsgBox("You Must Select An Alternate Item!")
            Exit Sub
        End If
        If Len(txtAltCost.Text) = 0 Then
            txtAltCost.Text = "0"
        End If
        If Not IsNumeric(txtAltCost.Text) Then
            MsgBox("Cost must be a Number!")
            Exit Sub
        End If
        If Len(txtAltSell.Text) = 0 Then
            txtAltSell.Text = "0"
        End If
        If Not IsNumeric(txtAltSell.Text) Then
            MsgBox("Selling Price must be a Number!")
            Exit Sub
        End If
        If Len(txtAltMargin.Text) = 0 Then
            txtAltMargin.Text = "0"
        End If
        If Not IsNumeric(txtAltMargin.Text) Then
            MsgBox("Margin must be a Whole Number!")
            Exit Sub
        End If
        AddAlternateItem()
        LoadAltDG()
        cboAltBidItems.SelectedIndex = 0
        cboAltCatMain.SelectedIndex = 0
        txtAltCost.Text = ""
        txtAltMargin.Text = ""
        txtAltSell.Text = ""
        txtAltProdID.Text = ""
        txtAltDesc.Text = ""
    End Sub

    Private Sub btnAltDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltDelete.Click
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdDeleteAlternate As New SqlCommand
        Dim strCommandText As String
        strCommandText = "DELETE FROM  Alternates "
        strCommandText = strCommandText & " WHERE (AltID = "

        strCommandText = strCommandText & CInt(dgAlt.Item(dgAlt.CurrentRowIndex, 0).ToString)
        strCommandText = strCommandText & ")"

        cmdDeleteAlternate.CommandText = strCommandText
        cmdDeleteAlternate.Connection = cnn
        Try
            cnn.Open()
            cmdDeleteAlternate.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error in Processing")
        Finally
            cnn.Close()
        End Try
        btnAltDelete.Visible = False
        btnAltCancel.Visible = False
        LoadAltDG()
    End Sub

    Private Sub mnuFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub mnuRptSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRptSummary.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Clients As New frmRptBidSummary
        Clients.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnCancelUnitLIneItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelUnitLIneItem.Click
        HideUnitEdit()
        txtUnitEdLocation.Text = ""
        txtUnitEdType.Text = ""
        txtUnitEdQuan.Text = ""
        cboUnit.Focus()
    End Sub

    Private Sub btnBldgLICancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBldgLICancel.Click
        HideBldgEdit()
        txtBldgEDQuan.Text = ""
        dgBldg.Focus()
    End Sub

    Private Sub btnAltCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltCancel.Click
        btnAltDelete.Visible = False
        btnAltCancel.Visible = False
    End Sub

    Private Sub dgAlt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAlt.Click
        btnAltDelete.Visible = True
        btnAltCancel.Visible = True
    End Sub

    Private Sub mnuRptSnapshot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRptSnapshot.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Snapshot As New frmRptSSMain
        Snapshot.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnSaveComment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveComment.Click
        If btnSaveComment.Text = "Save Comment" Then
            If txtCAuthor.Text = "" Then
                MsgBox("You must enter the initials of the Author of this comment!!")
                Exit Sub
            End If
            Dim AddBidComment As New SqlCommand("AddComment", cnn)
            AddBidComment.CommandType = CommandType.StoredProcedure

            Dim prmBidID As SqlParameter = AddBidComment.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmBidID.Value = frmMain.intBidID

            Dim prmEntryDate As SqlParameter = AddBidComment.Parameters.Add("@EntryDate", SqlDbType.DateTime)
            prmEntryDate.Value = Now()

            Dim prmAuthor As SqlParameter = AddBidComment.Parameters.Add("@Author", SqlDbType.NVarChar, 30)
            prmAuthor.Value = txtCAuthor.Text

            Dim prmComment As SqlParameter = AddBidComment.Parameters.Add("@Comment", SqlDbType.NVarChar, 1000)
            prmComment.Value = txtCComment.Text

            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                AddBidComment.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                MsgBox("There was an error adding this record: " + ex.ToString)
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
            End Try

        ElseIf btnSaveComment.Text = "Update Comment" Then
            Dim strEditSql As String
            strEditSql = "Update Comments Set Comment = '" & txtCComment.Text & _
            "' WHERE CommentID = " & dgComments.Item(dgComments.CurrentRowIndex, 0).ToString
            Dim EditBidComment As New SqlCommand(strEditSql.ToString, cnn)
            EditBidComment.CommandType = CommandType.Text


            Try
                If cnn.State = ConnectionState.Closed Then
                    cnn.Open()
                End If
                EditBidComment.ExecuteNonQuery()
                cnn.Close()
            Catch ex As Exception
                MsgBox("There was an error editing this record!!")
            Finally
                If cnn.State = ConnectionState.Open Then
                    cnn.Close()
                End If
                btnSaveComment.Text = "Save Comment"
                btnCommentEDit.Visible = False
                btnCommentDelete.Visible = False
            End Try
        End If

        txtCAuthor.Text = ""
        txtCComment.Text = ""
        LoadComments()
    End Sub

    Private Sub dgComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgComments.Click
        btnCommentEDit.Visible = True
        btnCommentDelete.Visible = True
    End Sub

    Private Sub btnCommentEDit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommentEDit.Click
        btnSaveComment.Text = "Update Comment"
        txtCComment.Text = dgComments.Item(dgComments.CurrentRowIndex, 3).ToString

    End Sub

    Private Sub btnCommentDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommentDelete.Click
        Dim strDeleteSql As String
        strDeleteSql = "DELETE FROM Comments " & _
        "WHERE CommentID = " & dgComments.Item(dgComments.CurrentRowIndex, 0).ToString
        Dim DeleteBidComment As New SqlCommand(strDeleteSql.ToString, cnn)
        DeleteBidComment.CommandType = CommandType.Text


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            DeleteBidComment.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error editing this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If

        End Try

        btnCommentEDit.Visible = False
        btnCommentDelete.Visible = False
        btnSaveComment.Text = "Save Comment"
        txtCAuthor.Text = ""
        txtCComment.Text = ""
        LoadComments()

    End Sub

    Private Sub UnitsBOM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnitsBOM.Click
        MsgBox("Unit Bom report is currently disabled")
        'DPS Recieved Broken as per Suzanne
        'Me.Refresh()
        'Me.Cursor = Cursors.WaitCursor
        'Dim UnitBOM As New frmRptUnitDetails
        'UnitBOM.ShowDialog()
        'Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnUpdateUnitGrid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateUnitGrid.Click

        cnn.ConnectionString = frmMain.strConnect

        Dim i As Integer
        For i = 0 To intUnitDGRowCount - 1

            Try
                Dim intItemID As Integer
                Dim intQuanID As Long
                Dim strLocation As String
                Dim strType As String

                intItemID = System.Convert.ToInt32(dgUnitLineItems.Item(i, 0))
                intQuanID = System.Convert.ToInt64(dgUnitLineItems.Item(i, 11))
                strLocation = dgUnitLineItems.Item(i, 5).ToString
                strType = dgUnitLineItems.Item(i, 7).ToString
                If Len(strType) > 10 Then
                    MsgBox(strType & " must be less than 10 characters." & vbCrLf & "Changes after this line will not be saved!", MsgBoxStyle.Critical, "Unit Type Too Long")

                    Exit For
                End If


                Dim cmdUpdateLineItemQuan As New SqlCommand
                Dim strCommandText As String
                strCommandText = "Update LineItem Set Quantity = "
                strCommandText = strCommandText & dgUnitLineItems.Item(i, 2).ToString
                strCommandText = strCommandText & ", Location = '" & strLocation
                strCommandText = strCommandText & "', Type = '" & strType

                cmdUpdateLineItemQuan.CommandText = strCommandText
                cmdUpdateLineItemQuan.Connection = cnn

                Try
                    cnn.Open()
                    cmdUpdateLineItemQuan.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox("Error in btnUpdateUnitLineItems")
                Finally
                    cnn.Close()
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try
            Catch ex As Exception

            End Try

        Next
        HideUnitEdit()
        LoadLineItemDG()
    End Sub

    Private Sub btnLoadSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadSearch.Click
        If cboSPartNum.Text = "" And cboSType.Text = "" And cboSLocation.Text = "" Then
            MsgBox("You must select at least one item to search for!")
            Exit Sub
        End If
        cnn.ConnectionString = frmMain.strConnect
        Dim strSearch As String
        strSearch = "SELECT distinct LineItem.PartNumber,LineItem.Quantity, LineItem.Location, "
        strSearch = strSearch & "LineItem.Type, Unit.UnitType, Unit.Description "
        strSearch = strSearch & " FROM LineItem Inner Join Unit on LineItem.UnitId = Unit.UnitID"
        strSearch = strSearch & " WHERE (LineItem.ProjectID = " & frmMain.intBidID.ToString & ")"
        If cboSPartNum.Text <> "" Then
            strSearch = strSearch & " AND (LineItem.PartNumber = '" & cboSPartNum.Text & "')"
        End If
        If cboSType.Text <> "" Then
            strSearch = strSearch & " AND (LineItem.Type = '" & cboSType.Text & "')"
        End If
        If cboSLocation.Text <> "" Then
            strSearch = strSearch & " AND (LineItem.Location = '" & cboSLocation.Text & "')"
        End If

        Dim cmdSearch As New SqlCommand(strSearch, cnn)
        cmdSearch.CommandType = CommandType.Text

        Dim daSearch As New SqlDataAdapter(cmdSearch)
        Dim dsSearch As New DataSet
        Try
            daSearch.Fill(dsSearch, "LineItems")
        Catch ex As Exception
            MsgBox("Error Loading Search DataGrid: " + ex.ToString)
        End Try

        dgSearch.DataSource = dsSearch.Tables("LineItems")
        btnPrintSearch.Visible = True
        strSPartNumber = cboSPartNum.Text
        strSType = cboSType.Text
        strSLocation = cboSLocation.Text
    End Sub

    Private Sub btnAltAddSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltAddSO.Click
        If txtAltProdID.Text = "" Then
            MsgBox("You must enter a valid Part Number")
            Exit Sub
        End If

        If txtAltDesc.Text = "" Then
            MsgBox("You must enter a valid Description")
            Exit Sub
        End If

        If Not IsNumeric(txtAltCost.Text) Then
            MsgBox("The Cost must be positive number")
            Exit Sub
        End If

        blnAddSO = True
        strSOProductID = txtAltProdID.Text
        strSODesc = txtAltDesc.Text
        If IsNumeric(txtAltCost.Text) Then
            dblSOCost = CDbl(txtAltCost.Text).ToString
        Else
            MsgBox("Cost must be entered as a number!")
            Exit Sub
        End If
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim CatSO As New frmCatSO
        CatSO.ShowDialog()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub mnuEditAux_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditAux.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Snapshot As New frmEditAuxLabel
        Snapshot.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuUpdateCosts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuUpdateCosts.Click
        Dim strPrompt As String
        strPrompt = "You are about to update all costs from the Main Catalog!!"
        strPrompt = strPrompt & vbCrLf & vbCrLf & "Please run a Snapshot Report and confirm that you have a current backup."
        strPrompt = strPrompt & vbCrLf & vbCrLf & "Are you sure you want to proceed?"
        If MsgBox(strPrompt, MsgBoxStyle.YesNo, "Update Costs???") = MsgBoxResult.Yes Then
            UpdateCosts()
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        Me.Refresh()
        Me.Cursor = Cursors.WaitCursor
        Dim Snapshot As New frmBidExportBOM
        Snapshot.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub mnuUnits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 1
    End Sub

    Private Sub mnuProject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 0
    End Sub

    Private Sub mnuAlternates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 2
    End Sub

    Private Sub BidBuilding_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 3
    End Sub

    Private Sub BidBulkEdits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 4
    End Sub

    Private Sub BidSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 5
    End Sub

    Private Sub BidSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 6
    End Sub

    Private Sub BidComments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TabCtrlBid.SelectedIndex = 7
    End Sub

    Private Sub btnBERDByType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBERDByType.Click
        'Bulk Edit
        If txtBEBType.Text = "" Then
            MsgBox("Invalid Original Type")
            Exit Sub
        End If

        Dim strprompt As String
        strprompt = "You are about to Delete All Instances of Type " & RTrim(txtBEBType.Text.ToString) & _
              "!!" & vbCrLf & _
            "      CONTINUE????"

        If MsgBox(strprompt, MsgBoxStyle.OkCancel, "Delete Type") = MsgBoxResult.Ok Then
            ' Check for Dupe Parts of Same Type
            Dim strSQL As String
            'Populate Client TextBoxes From Stored Proecedure And DataReader
            strSQL = "SELECT Distinct PartNumber, Type From LineItem " & _
                "where Projectid = " & frmMain.intBidID & " AND Type = '" & txtBEBType.Text & "'"
            Dim cmdDupeType As New SqlCommand(strSQL, cnn)
            cmdDupeType.CommandType = CommandType.Text
            Dim daDupeType As New SqlDataAdapter(cmdDupeType)
            Dim dsDupeType As New DataSet
            daDupeType.Fill(dsDupeType, "Types")
            If dsDupeType.Tables(0).Rows.Count > 1 Then
                MsgBox("This Type refers to multiple Part Numbers in this bid. It cannot be used in this procedure!!")
                Exit Sub
            End If

            Dim cmdBEDelete As New SqlCommand("DeleteLIByType", cnn)
            cmdBEDelete.CommandType = CommandType.StoredProcedure
            cmdBEDelete.Parameters.AddWithValue("@ProjectID", frmMain.intBidID)
            cmdBEDelete.Parameters.AddWithValue("@Type", txtBEBType.Text.ToString)

            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            Try
                cmdBEDelete.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Error In Deleting By Type")
            Finally
                cnn.Close()

            End Try
        End If

        LoadBEBidItemcbo()
        LoadBETypeCBO()
        ClearBulkEditFields()
    End Sub

    Private Sub MnuUpdateDesc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdateDesc.Click
        Dim strPrompt As String
        strPrompt = "You are about to update all Part Descriptions from the Main Catalog!!"
        strPrompt = strPrompt & vbCrLf & vbCrLf & "Please run a Snapshot Report and confirm that you have a current backup."
        strPrompt = strPrompt & vbCrLf & vbCrLf & "Are you sure you want to proceed?"
        If MsgBox(strPrompt, MsgBoxStyle.YesNo, "Update Descriptions???") = MsgBoxResult.Yes Then
            UpdateDescriptions()
        End If
    End Sub

    Private Sub cboBid_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBid.SelectionChangeCommitted
        frmMain.intBidID = System.Convert.ToInt32(cboBid.SelectedValue)
        GoToBid()
    End Sub

    Private Sub GoToBid()
        TabCtrlBid.Visible = True
        cboClients.Visible = False
        LoadBidInfo()
        GetClientId()
        LoadClientDetailsFromSavedID()
        cboBid.Visible = False
        txtBid.Text = cboBid.Text
        txtBid.Visible = True
        mnuUtilities.Visible = True
        mnuBidReport.Visible = True
        mnuUpdateCosts.Visible = True
        mnuUpdateDesc.Visible = True
        mnuExport.Visible = True
        mnuAdjustMargins.Visible = True
        mnuEditCosts.Visible = True
        mnuShipDates.Visible = True
        TabCtrlBid.TabPages(1).Enabled = True
        TabCtrlBid.TabPages(2).Enabled = True
        TabCtrlBid.TabPages(3).Enabled = True
        TabCtrlBid.TabPages(4).Enabled = True
        TabCtrlBid.TabPages(5).Enabled = True
        TabCtrlBid.TabPages(6).Enabled = True
        TabCtrlBid.TabPages(7).Enabled = True
    End Sub

    Private Sub cboUnit_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectionChangeCommitted
        Try
            LoadLineItemDG()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub frmBid_Activated_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If TabCtrlBid.SelectedIndex = 1 Then
            LoadUnits()
        End If
    End Sub

    Private Sub cboBldg_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBldg.SelectionChangeCommitted
        LoadBldgDetails()
        LoadBldgUnitsDG()
        BldgUnitCount()
    End Sub

    Private Sub cboBEBidItems_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBEBidItems.SelectionChangeCommitted
        LoadBELIbyPart()
    End Sub

    Private Sub tbSummary_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbSummary.Enter
        LoadUnitsPerBldg()
        LoadUnitCount()
        LoadLineItemSummary()
        LoadBldgCnt()
        LoadMargins()
    End Sub

    Private Sub tbSearch_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbSearch.Enter
        LoadCBOSParts()
        LoadCBOSTypes()
        LoadCBOSLocations()
    End Sub
End Class
