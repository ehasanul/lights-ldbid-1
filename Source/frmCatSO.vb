Imports System.Data
Imports System.Data.SqlClient
Public Class frmCatSO
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuNew As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEdit As System.Windows.Forms.MenuItem
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents lblAlternatePartNum As System.Windows.Forms.Label
    Friend WithEvents lblAlternateVendor As System.Windows.Forms.Label
    Friend WithEvents txtAlternatePartNum As System.Windows.Forms.TextBox
    Friend WithEvents txtAlternateVendor As System.Windows.Forms.TextBox
    Friend WithEvents lblLeadTime As System.Windows.Forms.Label
    Friend WithEvents lblCommittedUnits As System.Windows.Forms.Label
    Friend WithEvents lblOnOrderUnits As System.Windows.Forms.Label
    Friend WithEvents lblAvailableUnits As System.Windows.Forms.Label
    Friend WithEvents lblStandardCost As System.Windows.Forms.Label
    Friend WithEvents lblAverageCost As System.Windows.Forms.Label
    Friend WithEvents lblVendorPartNo As System.Windows.Forms.Label
    Friend WithEvents lblVendorCode As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblProductID As System.Windows.Forms.Label
    Friend WithEvents txtLeadTime As System.Windows.Forms.TextBox
    Friend WithEvents txtCommittedUnits As System.Windows.Forms.TextBox
    Friend WithEvents txtOnOrderUnits As System.Windows.Forms.TextBox
    Friend WithEvents txtAvailableUnits As System.Windows.Forms.TextBox
    Friend WithEvents txtStandardCost As System.Windows.Forms.TextBox
    Friend WithEvents txtAverageCost As System.Windows.Forms.TextBox
    Friend WithEvents txtVendorPartNo As System.Windows.Forms.TextBox
    Friend WithEvents txtVendorCode As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtProductID As System.Windows.Forms.TextBox
    Friend WithEvents cboCatalogMain As System.Windows.Forms.ComboBox
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.mnuNew = New System.Windows.Forms.MenuItem
        Me.mnuEdit = New System.Windows.Forms.MenuItem
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.lblAlternatePartNum = New System.Windows.Forms.Label
        Me.lblAlternateVendor = New System.Windows.Forms.Label
        Me.txtAlternatePartNum = New System.Windows.Forms.TextBox
        Me.txtAlternateVendor = New System.Windows.Forms.TextBox
        Me.lblLeadTime = New System.Windows.Forms.Label
        Me.lblCommittedUnits = New System.Windows.Forms.Label
        Me.lblOnOrderUnits = New System.Windows.Forms.Label
        Me.lblAvailableUnits = New System.Windows.Forms.Label
        Me.lblStandardCost = New System.Windows.Forms.Label
        Me.lblAverageCost = New System.Windows.Forms.Label
        Me.lblVendorPartNo = New System.Windows.Forms.Label
        Me.lblVendorCode = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblProductID = New System.Windows.Forms.Label
        Me.txtLeadTime = New System.Windows.Forms.TextBox
        Me.txtCommittedUnits = New System.Windows.Forms.TextBox
        Me.txtOnOrderUnits = New System.Windows.Forms.TextBox
        Me.txtAvailableUnits = New System.Windows.Forms.TextBox
        Me.txtStandardCost = New System.Windows.Forms.TextBox
        Me.txtAverageCost = New System.Windows.Forms.TextBox
        Me.txtVendorPartNo = New System.Windows.Forms.TextBox
        Me.txtVendorCode = New System.Windows.Forms.TextBox
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtProductID = New System.Windows.Forms.TextBox
        Me.cboCatalogMain = New System.Windows.Forms.ComboBox
        Me.txtNotes = New System.Windows.Forms.TextBox
        Me.lblNotes = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNew, Me.mnuEdit})
        '
        'mnuNew
        '
        Me.mnuNew.Index = 0
        Me.mnuNew.Text = "New"
        '
        'mnuEdit
        '
        Me.mnuEdit.Index = 1
        Me.mnuEdit.Text = "Edit"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(420, 264)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(52, 16)
        Me.btnSave.TabIndex = 14
        Me.btnSave.Text = "Save"
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(364, 264)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(56, 16)
        Me.btnCancel.TabIndex = 15
        Me.btnCancel.Text = "Cancel"
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(312, 264)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(52, 16)
        Me.btnDelete.TabIndex = 16
        Me.btnDelete.Text = "Delete"
        '
        'lblAlternatePartNum
        '
        Me.lblAlternatePartNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternatePartNum.Location = New System.Drawing.Point(252, 112)
        Me.lblAlternatePartNum.Name = "lblAlternatePartNum"
        Me.lblAlternatePartNum.Size = New System.Drawing.Size(176, 12)
        Me.lblAlternatePartNum.TabIndex = 56
        Me.lblAlternatePartNum.Text = "Alternate Part Number"
        '
        'lblAlternateVendor
        '
        Me.lblAlternateVendor.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternateVendor.Location = New System.Drawing.Point(8, 116)
        Me.lblAlternateVendor.Name = "lblAlternateVendor"
        Me.lblAlternateVendor.Size = New System.Drawing.Size(104, 12)
        Me.lblAlternateVendor.TabIndex = 55
        Me.lblAlternateVendor.Text = "Alternate Vendor"
        '
        'txtAlternatePartNum
        '
        Me.txtAlternatePartNum.Location = New System.Drawing.Point(252, 128)
        Me.txtAlternatePartNum.Name = "txtAlternatePartNum"
        Me.txtAlternatePartNum.Size = New System.Drawing.Size(224, 20)
        Me.txtAlternatePartNum.TabIndex = 6
        Me.txtAlternatePartNum.Text = ""
        '
        'txtAlternateVendor
        '
        Me.txtAlternateVendor.Location = New System.Drawing.Point(8, 128)
        Me.txtAlternateVendor.Name = "txtAlternateVendor"
        Me.txtAlternateVendor.Size = New System.Drawing.Size(228, 20)
        Me.txtAlternateVendor.TabIndex = 5
        Me.txtAlternateVendor.Text = ""
        '
        'lblLeadTime
        '
        Me.lblLeadTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeadTime.Location = New System.Drawing.Point(420, 148)
        Me.lblLeadTime.Name = "lblLeadTime"
        Me.lblLeadTime.Size = New System.Drawing.Size(60, 12)
        Me.lblLeadTime.TabIndex = 54
        Me.lblLeadTime.Text = "Lead Time"
        '
        'lblCommittedUnits
        '
        Me.lblCommittedUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommittedUnits.Location = New System.Drawing.Point(340, 148)
        Me.lblCommittedUnits.Name = "lblCommittedUnits"
        Me.lblCommittedUnits.Size = New System.Drawing.Size(76, 12)
        Me.lblCommittedUnits.TabIndex = 53
        Me.lblCommittedUnits.Text = "Committed Units"
        '
        'lblOnOrderUnits
        '
        Me.lblOnOrderUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOnOrderUnits.Location = New System.Drawing.Point(276, 148)
        Me.lblOnOrderUnits.Name = "lblOnOrderUnits"
        Me.lblOnOrderUnits.Size = New System.Drawing.Size(68, 12)
        Me.lblOnOrderUnits.TabIndex = 52
        Me.lblOnOrderUnits.Text = "Units On Order"
        '
        'lblAvailableUnits
        '
        Me.lblAvailableUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAvailableUnits.Location = New System.Drawing.Point(204, 148)
        Me.lblAvailableUnits.Name = "lblAvailableUnits"
        Me.lblAvailableUnits.Size = New System.Drawing.Size(68, 12)
        Me.lblAvailableUnits.TabIndex = 51
        Me.lblAvailableUnits.Text = "Available Units"
        '
        'lblStandardCost
        '
        Me.lblStandardCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStandardCost.Location = New System.Drawing.Point(116, 148)
        Me.lblStandardCost.Name = "lblStandardCost"
        Me.lblStandardCost.Size = New System.Drawing.Size(80, 12)
        Me.lblStandardCost.TabIndex = 50
        Me.lblStandardCost.Text = "Standard Cost"
        '
        'lblAverageCost
        '
        Me.lblAverageCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAverageCost.Location = New System.Drawing.Point(8, 148)
        Me.lblAverageCost.Name = "lblAverageCost"
        Me.lblAverageCost.Size = New System.Drawing.Size(76, 12)
        Me.lblAverageCost.TabIndex = 49
        Me.lblAverageCost.Text = "Average Cost"
        '
        'lblVendorPartNo
        '
        Me.lblVendorPartNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendorPartNo.Location = New System.Drawing.Point(252, 80)
        Me.lblVendorPartNo.Name = "lblVendorPartNo"
        Me.lblVendorPartNo.Size = New System.Drawing.Size(68, 12)
        Me.lblVendorPartNo.TabIndex = 48
        Me.lblVendorPartNo.Text = "Vendor Part Number"
        '
        'lblVendorCode
        '
        Me.lblVendorCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendorCode.Location = New System.Drawing.Point(8, 80)
        Me.lblVendorCode.Name = "lblVendorCode"
        Me.lblVendorCode.Size = New System.Drawing.Size(80, 12)
        Me.lblVendorCode.TabIndex = 47
        Me.lblVendorCode.Text = "Vendor Code"
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(252, 44)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(124, 12)
        Me.lblDescription.TabIndex = 46
        Me.lblDescription.Text = "Description"
        '
        'lblProductID
        '
        Me.lblProductID.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProductID.Location = New System.Drawing.Point(8, 44)
        Me.lblProductID.Name = "lblProductID"
        Me.lblProductID.Size = New System.Drawing.Size(116, 12)
        Me.lblProductID.TabIndex = 44
        Me.lblProductID.Text = "Part Number"
        '
        'txtLeadTime
        '
        Me.txtLeadTime.Location = New System.Drawing.Point(408, 164)
        Me.txtLeadTime.Name = "txtLeadTime"
        Me.txtLeadTime.Size = New System.Drawing.Size(68, 20)
        Me.txtLeadTime.TabIndex = 12
        Me.txtLeadTime.Text = ""
        Me.txtLeadTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCommittedUnits
        '
        Me.txtCommittedUnits.Location = New System.Drawing.Point(340, 164)
        Me.txtCommittedUnits.Name = "txtCommittedUnits"
        Me.txtCommittedUnits.Size = New System.Drawing.Size(68, 20)
        Me.txtCommittedUnits.TabIndex = 11
        Me.txtCommittedUnits.Text = ""
        Me.txtCommittedUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtOnOrderUnits
        '
        Me.txtOnOrderUnits.Location = New System.Drawing.Point(272, 164)
        Me.txtOnOrderUnits.Name = "txtOnOrderUnits"
        Me.txtOnOrderUnits.Size = New System.Drawing.Size(68, 20)
        Me.txtOnOrderUnits.TabIndex = 10
        Me.txtOnOrderUnits.Text = ""
        Me.txtOnOrderUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtAvailableUnits
        '
        Me.txtAvailableUnits.Location = New System.Drawing.Point(204, 164)
        Me.txtAvailableUnits.Name = "txtAvailableUnits"
        Me.txtAvailableUnits.Size = New System.Drawing.Size(68, 20)
        Me.txtAvailableUnits.TabIndex = 9
        Me.txtAvailableUnits.Text = ""
        Me.txtAvailableUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtStandardCost
        '
        Me.txtStandardCost.Location = New System.Drawing.Point(108, 164)
        Me.txtStandardCost.Name = "txtStandardCost"
        Me.txtStandardCost.Size = New System.Drawing.Size(96, 20)
        Me.txtStandardCost.TabIndex = 8
        Me.txtStandardCost.Text = ""
        Me.txtStandardCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAverageCost
        '
        Me.txtAverageCost.Location = New System.Drawing.Point(8, 164)
        Me.txtAverageCost.Name = "txtAverageCost"
        Me.txtAverageCost.TabIndex = 7
        Me.txtAverageCost.Text = ""
        Me.txtAverageCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVendorPartNo
        '
        Me.txtVendorPartNo.Location = New System.Drawing.Point(252, 92)
        Me.txtVendorPartNo.Name = "txtVendorPartNo"
        Me.txtVendorPartNo.Size = New System.Drawing.Size(224, 20)
        Me.txtVendorPartNo.TabIndex = 4
        Me.txtVendorPartNo.Text = ""
        '
        'txtVendorCode
        '
        Me.txtVendorCode.Location = New System.Drawing.Point(8, 92)
        Me.txtVendorCode.Name = "txtVendorCode"
        Me.txtVendorCode.Size = New System.Drawing.Size(228, 20)
        Me.txtVendorCode.TabIndex = 3
        Me.txtVendorCode.Text = ""
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(252, 60)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(224, 20)
        Me.txtDescription.TabIndex = 2
        Me.txtDescription.Text = ""
        '
        'txtProductID
        '
        Me.txtProductID.Location = New System.Drawing.Point(8, 60)
        Me.txtProductID.MaxLength = 13
        Me.txtProductID.Name = "txtProductID"
        Me.txtProductID.Size = New System.Drawing.Size(228, 20)
        Me.txtProductID.TabIndex = 1
        Me.txtProductID.Text = ""
        '
        'cboCatalogMain
        '
        Me.cboCatalogMain.Location = New System.Drawing.Point(8, 12)
        Me.cboCatalogMain.Name = "cboCatalogMain"
        Me.cboCatalogMain.Size = New System.Drawing.Size(280, 21)
        Me.cboCatalogMain.TabIndex = 0
        Me.cboCatalogMain.Text = "Catalog"
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(8, 204)
        Me.txtNotes.MaxLength = 1000
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNotes.Size = New System.Drawing.Size(468, 52)
        Me.txtNotes.TabIndex = 13
        Me.txtNotes.Text = ""
        '
        'lblNotes
        '
        Me.lblNotes.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotes.Location = New System.Drawing.Point(8, 188)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(68, 12)
        Me.lblNotes.TabIndex = 58
        Me.lblNotes.Text = "Notes"
        '
        'frmCatSO
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(492, 285)
        Me.Controls.Add(Me.lblNotes)
        Me.Controls.Add(Me.txtNotes)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblAlternatePartNum)
        Me.Controls.Add(Me.lblAlternateVendor)
        Me.Controls.Add(Me.txtAlternatePartNum)
        Me.Controls.Add(Me.txtAlternateVendor)
        Me.Controls.Add(Me.lblLeadTime)
        Me.Controls.Add(Me.lblCommittedUnits)
        Me.Controls.Add(Me.lblOnOrderUnits)
        Me.Controls.Add(Me.lblAvailableUnits)
        Me.Controls.Add(Me.lblStandardCost)
        Me.Controls.Add(Me.lblAverageCost)
        Me.Controls.Add(Me.lblVendorPartNo)
        Me.Controls.Add(Me.lblVendorCode)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblProductID)
        Me.Controls.Add(Me.txtLeadTime)
        Me.Controls.Add(Me.txtCommittedUnits)
        Me.Controls.Add(Me.txtOnOrderUnits)
        Me.Controls.Add(Me.txtAvailableUnits)
        Me.Controls.Add(Me.txtStandardCost)
        Me.Controls.Add(Me.txtAverageCost)
        Me.Controls.Add(Me.txtVendorPartNo)
        Me.Controls.Add(Me.txtVendorCode)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.txtProductID)
        Me.Controls.Add(Me.cboCatalogMain)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmCatSO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Special Order Catalog"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim cnn As New SqlConnection
    Dim strFunction As String

    Sub LoadCatalogMaincbo()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String
        strCatalogMain = "CatSOItem"

        Dim daCatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading CatalogMain CBO")
        End Try
        cboCatalogMain.DataSource = dsCatalogMain.Tables("Catalog")
        cboCatalogMain.DisplayMember = "Item"
        cboCatalogMain.ValueMember = "ProductID"
    End Sub

    Private Sub HideBtns()
        btnDelete.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False

    End Sub

    Private Sub HideFields()
        txtProductID.Visible = False
        txtDescription.Visible = False
        txtVendorCode.Visible = False
        txtVendorPartNo.Visible = False
        txtAlternateVendor.Visible = False
        txtAlternatePartNum.Visible = False
        txtAverageCost.Visible = False
        txtStandardCost.Visible = False
        txtAvailableUnits.Visible = False
        txtOnOrderUnits.Visible = False
        txtCommittedUnits.Visible = False
        txtLeadTime.Visible = False
        txtNotes.Visible = False

        lblProductID.Visible = False
        lblDescription.Visible = False
        lblVendorCode.Visible = False
        lblVendorPartNo.Visible = False
        lblAlternateVendor.Visible = False
        lblAlternatePartNum.Visible = False
        lblAverageCost.Visible = False
        lblStandardCost.Visible = False
        lblAvailableUnits.Visible = False
        lblOnOrderUnits.Visible = False
        lblCommittedUnits.Visible = False
        lblLeadTime.Visible = False
        lblNotes.Visible = False

        cboCatalogMain.Visible = False


    End Sub

    Private Sub showfields()
        txtProductID.Visible = True
        txtDescription.Visible = True
        txtVendorCode.Visible = True
        txtVendorPartNo.Visible = True
        txtAlternateVendor.Visible = True
        txtAlternatePartNum.Visible = True
        txtAverageCost.Visible = True
        txtStandardCost.Visible = True
        txtAvailableUnits.Visible = True
        txtOnOrderUnits.Visible = True
        txtCommittedUnits.Visible = True
        txtLeadTime.Visible = True
        txtNotes.Visible = True

        lblProductID.Visible = True
        lblDescription.Visible = True
        lblVendorCode.Visible = True
        lblVendorPartNo.Visible = True
        lblAlternateVendor.Visible = True
        lblAlternatePartNum.Visible = True
        lblAverageCost.Visible = True
        lblStandardCost.Visible = True
        lblAvailableUnits.Visible = True
        lblOnOrderUnits.Visible = True
        lblCommittedUnits.Visible = True
        lblLeadTime.Visible = True
        lblNotes.Visible = True
    End Sub

    Private Sub ClearFields()
        txtProductID.Text = ""
        txtDescription.Text = ""
        txtVendorCode.Text = ""
        txtVendorPartNo.Text = ""
        txtAlternateVendor.Text = ""
        txtAlternatePartNum.Text = ""
        txtAverageCost.Text = ""
        txtStandardCost.Text = ""
        txtAvailableUnits.Text = ""
        txtOnOrderUnits.Text = ""
        txtCommittedUnits.Text = ""
        txtLeadTime.Text = ""
        txtNotes.Text = ""
    End Sub

    Private Sub frmCatSO_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadCatalogMaincbo()
        HideFields()
        HideBtns()
        If frmBid.blnAddSO = True Then
            ClearFields()
            btnSave.Visible = True
            btnCancel.Visible = True
            showfields()
            cboCatalogMain.Visible = False
            strFunction = "Add"
            txtProductID.Text = frmBid.strSOProductID
            txtDescription.Text = frmBid.strSODesc
            txtAverageCost.Text = frmBid.dblSOCost.ToString
            txtStandardCost.Text = frmBid.dblSOCost.ToString
            'frmBid.blnAddSO = False
        End If
    End Sub

    Private Sub LoadItemDetails()
        'cboCatalogMain.SelectedValue.ToString
        If cboCatalogMain.SelectedIndex = 0 Then
            ClearFields()
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboCatalogMain.SelectedValue.ToString

        Dim spCatMainLineItem As String
        spCatMainLineItem = "CatSOItemsSP"

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboCatalogMain.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            txtProductID.Text = cboCatalogMain.SelectedValue.ToString
            txtDescription.Text = drCatMainLineItem.Item("Description").ToString
            If IsDBNull(drCatMainLineItem.Item("VendorCode")) Then
                txtVendorCode.Text = ""
            Else
                txtVendorCode.Text = drCatMainLineItem.Item("VendorCode").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("VendorPartNo")) Then
                txtVendorPartNo.Text = ""
            Else
                txtVendorPartNo.Text = drCatMainLineItem.Item("VendorPartNo").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("AlternateVendor")) Then
                txtAlternateVendor.Text = ""
            Else
                txtAlternateVendor.Text = drCatMainLineItem.Item("AlternateVendor").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("AlternatePartNum")) Then
                txtAlternatePartNum.Text = ""
            Else
                txtAlternatePartNum.Text = drCatMainLineItem.Item("AlternatePartNum").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("AverageCost")) Then
                txtAverageCost.Text = "0"
            Else
                txtAverageCost.Text = Format(drCatMainLineItem.Item("AverageCost"), "C")
            End If
            If IsDBNull(drCatMainLineItem.Item("StandardCost")) Then
                txtStandardCost.Text = "0"
            Else
                txtStandardCost.Text = Format(drCatMainLineItem.Item("StandardCost"), "C")
            End If
            If IsDBNull(drCatMainLineItem.Item("AvailableUnits")) Then
                txtAvailableUnits.Text = "0"
            Else
                txtAvailableUnits.Text = drCatMainLineItem.Item("AvailableUnits").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("OnOrderUnits")) Then
                txtOnOrderUnits.Text = "0"
            Else
                txtOnOrderUnits.Text = drCatMainLineItem.Item("OnOrderUnits").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("CommittedUnits")) Then
                txtCommittedUnits.Text = "0"
            Else
                txtCommittedUnits.Text = drCatMainLineItem.Item("CommittedUnits").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("LeadTime")) Then
                txtLeadTime.Text = ""
            Else
                txtLeadTime.Text = drCatMainLineItem.Item("LeadTime").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("Notes")) Then
                txtNotes.Text = ""
            Else
                txtNotes.Text = drCatMainLineItem.Item("Notes").ToString
            End If

        Catch ex As Exception
            MsgBox("Error Loading CatMainLineItem")
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try

    End Sub

    Private Sub cboCatalogMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCatalogMain.SelectedIndexChanged
        LoadItemDetails()
        showfields()
        btnSave.Visible = True
        btnCancel.Visible = True
        btnDelete.Visible = True
    End Sub

    Private Sub EditCatalog()
        If txtDescription.Text = "" Then
            MsgBox("A Description Is Required.")
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim EditCatalog As New SqlCommand("EditCatalogSO", cnn)
        EditCatalog.CommandType = CommandType.StoredProcedure

        Dim prmProductID As SqlParameter = EditCatalog.Parameters.Add("@ProductID", SqlDbType.NVarChar, 255)
        prmProductID.Value = cboCatalogMain.SelectedValue

        Dim prmDescription As SqlParameter = EditCatalog.Parameters.Add("@Description", SqlDbType.NVarChar, 255)
        prmDescription.Value = txtDescription.Text

        Dim prmVendorCode As SqlParameter = EditCatalog.Parameters.Add("@VendorCode", SqlDbType.NVarChar, 255)
        prmVendorCode.Value = txtVendorCode.Text

        Dim prmVendorPartNo As SqlParameter = EditCatalog.Parameters.Add("@VendorPartNo", SqlDbType.NVarChar, 255)
        prmVendorPartNo.Value = txtVendorPartNo.Text

        Dim prmAlternateVendor As SqlParameter = EditCatalog.Parameters.Add("@AlternateVendor", SqlDbType.NVarChar, 255)
        prmAlternateVendor.Value = txtAlternateVendor.Text

        Dim prmAlternatePartNum As SqlParameter = EditCatalog.Parameters.Add("@AlternatePartNum", SqlDbType.NVarChar, 255)
        prmAlternatePartNum.Value = txtAlternatePartNum.Text

        Dim prmAverageCost As SqlParameter = EditCatalog.Parameters.Add("@AverageCost", SqlDbType.Float)
        If IsNumeric(txtAverageCost.Text) Then
            prmAverageCost.Value = CDbl(txtAverageCost.Text)
        Else
            MsgBox("You must enter a numeric Cost.")
            Exit Sub
        End If


        Dim prmStandardCost As SqlParameter = EditCatalog.Parameters.Add("@StandardCost", SqlDbType.Float)
        If IsNumeric(txtStandardCost.Text) Then
            prmStandardCost.Value = CDbl(txtStandardCost.Text)
        Else
            MsgBox("You must enter a numeric Cost.")
            Exit Sub
        End If


        Dim prmAvailableUnits As SqlParameter = EditCatalog.Parameters.Add("@AvailableUnits", SqlDbType.Float)
        If IsNumeric(txtAvailableUnits.Text) Then
            prmAvailableUnits.Value = CDbl(txtAvailableUnits.Text)
        Else
            prmAvailableUnits.Value = 0
        End If


        Dim prmOnOrderUnits As SqlParameter = EditCatalog.Parameters.Add("@OnOrderUnits", SqlDbType.Float)
        If IsNumeric(txtOnOrderUnits.Text) Then
            prmOnOrderUnits.Value = CDbl(txtOnOrderUnits.Text)
        Else
            prmOnOrderUnits.Value = 0
        End If

        Dim prmCommittedUnits As SqlParameter = EditCatalog.Parameters.Add("@CommittedUnits", SqlDbType.Float)
        If IsNumeric(txtCommittedUnits.Text) Then
            prmCommittedUnits.Value = CDbl(txtCommittedUnits.Text)
        Else
            prmCommittedUnits.Value = 0
        End If

        Dim prmLeadTime As SqlParameter = EditCatalog.Parameters.Add("@LeadTime", SqlDbType.NVarChar, 255)
        prmLeadTime.Value = txtLeadTime.Text

        Dim prmNotes As SqlParameter = EditCatalog.Parameters.Add("@Notes", SqlDbType.NVarChar, 1000)
        prmNotes.Value = txtNotes.Text


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            EditCatalog.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub AddItem()
        Dim strTestProductID As String = "SELECT ProductID FROM CatalogSO WHERE ProductID = '"
        strTestProductID = strTestProductID & txtProductID.Text & "'"
        Dim cmdTestProductID As New SqlCommand(strTestProductID, cnn)
        cmdTestProductID.CommandType = CommandType.Text

        Dim drTestProductID As SqlDataReader
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        drTestProductID = cmdTestProductID.ExecuteReader
        drTestProductID.Read()
        If drTestProductID.HasRows Then
            MsgBox("You Must Enter A Unique Part Number!!!")
            drTestProductID.Close()
            cnn.Close()
            Exit Sub
        End If

        drTestProductID.Close()
        cnn.Close()


        If txtProductID.Text = "" Then
            MsgBox("A Unique Part Number Is Required.")
            Exit Sub
        End If
        If txtDescription.Text = "" Then
            MsgBox("A Description Is Required.")
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim AddItem As New SqlCommand("NewCatalogSO", cnn)
        AddItem.CommandType = CommandType.StoredProcedure

        Dim prmProductID As SqlParameter = AddItem.Parameters.Add("@ProductID", SqlDbType.NVarChar, 255)
        prmProductID.Value = txtProductID.Text

        Dim prmDescription As SqlParameter = AddItem.Parameters.Add("@Description", SqlDbType.NVarChar, 255)
        prmDescription.Value = txtDescription.Text

        Dim prmVendorCode As SqlParameter = AddItem.Parameters.Add("@VendorCode", SqlDbType.NVarChar, 255)
        prmVendorCode.Value = txtVendorCode.Text

        Dim prmVendorPartNo As SqlParameter = AddItem.Parameters.Add("@VendorPartNo", SqlDbType.NVarChar, 255)
        prmVendorPartNo.Value = txtVendorPartNo.Text

        Dim prmAlternateVendor As SqlParameter = AddItem.Parameters.Add("@AlternateVendor", SqlDbType.NVarChar, 255)
        prmAlternateVendor.Value = txtAlternateVendor.Text

        Dim prmAlternatePartNum As SqlParameter = AddItem.Parameters.Add("@AlternatePartNum", SqlDbType.NVarChar, 255)
        prmAlternatePartNum.Value = txtAlternatePartNum.Text

        Dim prmAverageCost As SqlParameter = AddItem.Parameters.Add("@AverageCost", SqlDbType.Float)
        If IsNumeric(txtAverageCost.Text) Then
            prmAverageCost.Value = CDbl(txtAverageCost.Text)
        Else
            MsgBox("You must enter a numeric Cost.")
            Exit Sub
        End If


        Dim prmStandardCost As SqlParameter = AddItem.Parameters.Add("@StandardCost", SqlDbType.Float)
        If IsNumeric(txtStandardCost.Text) Then
            prmStandardCost.Value = CDbl(txtStandardCost.Text)
        Else
            MsgBox("You must enter a numeric Cost.")
            Exit Sub
        End If


        Dim prmAvailableUnits As SqlParameter = AddItem.Parameters.Add("@AvailableUnits", SqlDbType.Float)
        If IsNumeric(txtAvailableUnits.Text) Then
            prmAvailableUnits.Value = CDbl(txtAvailableUnits.Text)
        Else
            prmAvailableUnits.Value = 0
        End If


        Dim prmOnOrderUnits As SqlParameter = AddItem.Parameters.Add("@OnOrderUnits", SqlDbType.Float)
        If IsNumeric(txtOnOrderUnits.Text) Then
            prmOnOrderUnits.Value = CDbl(txtOnOrderUnits.Text)
        Else
            prmOnOrderUnits.Value = 0
        End If

        Dim prmCommittedUnits As SqlParameter = AddItem.Parameters.Add("@CommittedUnits", SqlDbType.Float)
        If IsNumeric(txtCommittedUnits.Text) Then
            prmCommittedUnits.Value = CDbl(txtCommittedUnits.Text)
        Else
            prmCommittedUnits.Value = 0
        End If

        Dim prmLeadTime As SqlParameter = AddItem.Parameters.Add("@LeadTime", SqlDbType.NVarChar, 255)
        prmLeadTime.Value = txtLeadTime.Text

        Dim prmNotes As SqlParameter = AddItem.Parameters.Add("@Notes", SqlDbType.NVarChar, 1000)
        prmNotes.Value = txtNotes.Text


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            AddItem.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub DeleteItem()
        cnn.ConnectionString = frmMain.strConnect
        Dim DeleteItem As New SqlCommand("DeleteCatalogSO", cnn)
        DeleteItem.CommandType = CommandType.StoredProcedure

        Dim prmProductID As SqlParameter = DeleteItem.Parameters.Add("@ProductID", SqlDbType.NVarChar, 255)
        prmProductID.Value = cboCatalogMain.SelectedValue

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            DeleteItem.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error Deleting this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtVendorCode.Text = "" Then
            MsgBox("You Must Enter A Valid Vendor Code")
            Exit Sub
        End If

        If txtVendorPartNo.Text = "" Then
            MsgBox("You Must Enter A Valid Vendor Part Number")
            Exit Sub
        End If


        If strFunction = "Edit" Then
            'MsgBox("Edit")
            EditCatalog()
        ElseIf strFunction = "Add" Then
            'MsgBox("Add")
            AddItem()
            If frmBid.blnAddSO = True Then
                Me.Close()
            End If
        End If
        LoadCatalogMaincbo()
        HideBtns()
        ClearFields()
        HideFields()

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim strResult As String
        Dim strPrompt As String
        strPrompt = "Do you want to Delete " & cboCatalogMain.SelectedValue.ToString & "?"
        strResult = MsgBox(strPrompt, MsgBoxStyle.YesNo, "Delete??").ToString
        If strResult = "6" Then
            DeleteItem()
        End If
        LoadCatalogMaincbo()
        ClearFields()
        HideFields()

        
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        HideFields()
        HideBtns()
    End Sub

    Private Sub txtAlternatePartNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlternatePartNum.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtAlternateVendor_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlternateVendor.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtDescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtProductID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtProductID.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtVendorPartNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtVendorPartNo.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtVendorCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtVendorCode.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub mnuNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        ClearFields()
        btnSave.Visible = True
        btnCancel.Visible = True
        showfields()
        strFunction = "Add"
    End Sub

    Private Sub mnuEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEdit.Click
        'btnSave.Visible = True
        'btnCancel.Visible = True
        'btnDelete.Visible = True
        'showfields()
        cboCatalogMain.Visible = True
        strFunction = "Edit"
    End Sub

    Private Sub frmCatSO_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        frmBid.blnAddSO = False
    End Sub
End Class
