Imports System.Data
Imports System.Data.SqlClient
Public Class frmAdjustMargins
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDelta As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnAdjustMargins As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtDelta = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnAdjustMargins = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtDelta
        '
        Me.txtDelta.Location = New System.Drawing.Point(208, 104)
        Me.txtDelta.Name = "txtDelta"
        Me.txtDelta.TabIndex = 0
        Me.txtDelta.Text = ""
        Me.txtDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(300, 32)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "This from will increase or decrease Margins on all items in this Bid by the amoun" & _
        "t you enter."
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(12, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(300, 36)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "The entry must be a whole number and may be positive to increase the Margins or n" & _
        "egative to decrease the Margins."
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(140, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Increment"
        '
        'btnAdjustMargins
        '
        Me.btnAdjustMargins.Location = New System.Drawing.Point(188, 132)
        Me.btnAdjustMargins.Name = "btnAdjustMargins"
        Me.btnAdjustMargins.Size = New System.Drawing.Size(124, 23)
        Me.btnAdjustMargins.TabIndex = 4
        Me.btnAdjustMargins.Text = "Adjust Margins"
        '
        'frmAdjustMargins
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(336, 194)
        Me.Controls.Add(Me.btnAdjustMargins)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDelta)
        Me.Name = "frmAdjustMargins"
        Me.Text = "Adjust Margins"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub AdjustMargins()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim cmdAdjustMargins As New SqlCommand("AdjustMargins", cnn)
        cmdAdjustMargins.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAdjustMargins.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmDelta As SqlParameter = cmdAdjustMargins.Parameters.Add("@Delta", SqlDbType.Int)
        prmDelta.Value = CInt(txtDelta.Text)
        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If
            cmdAdjustMargins.ExecuteNonQuery()
            cnn.Close()
        Catch ex As Exception
            MsgBox("There was an error modifying the margins: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub btnAdjustMargins_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdjustMargins.Click
        If Not IsNumeric(txtDelta.Text) Then
            MsgBox("You must enter a Whole Number in the Increment Box!!", MsgBoxStyle.Exclamation, "Get it Right")
            Exit Sub
        Else
            AdjustMargins()

            Me.Close()
        End If
    End Sub
End Class
