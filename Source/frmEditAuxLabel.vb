Imports System.Data
Imports System.Data.SqlClient
Public Class frmEditAuxLabel
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAuxLabel As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtAuxLabel = New System.Windows.Forms.TextBox
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Enter Label For Auxillary Field"
        '
        'txtAuxLabel
        '
        Me.txtAuxLabel.Location = New System.Drawing.Point(4, 44)
        Me.txtAuxLabel.MaxLength = 25
        Me.txtAuxLabel.Name = "txtAuxLabel"
        Me.txtAuxLabel.Size = New System.Drawing.Size(160, 20)
        Me.txtAuxLabel.TabIndex = 1
        Me.txtAuxLabel.Text = ""
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(172, 44)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(92, 20)
        Me.btnUpdate.TabIndex = 2
        Me.btnUpdate.Text = "Update"
        '
        'frmEditAuxLabel
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(288, 78)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.txtAuxLabel)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmEditAuxLabel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Auxiliary Label"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        cnn.ConnectionString = frmMain.strConnect

        Dim strSqlCommand As String
        strSqlCommand = "UPDATE Project SET  "
        strSqlCommand = strSqlCommand & "AuxLabel =  '" & Trim(txtAuxLabel.Text)
        strSqlCommand = strSqlCommand & "' Where ProjectID = " & frmMain.intBidID
        Dim cmdAuxEdit As New SqlCommand(strSqlCommand, cnn)
        Try
            cnn.Open()
            cmdAuxEdit.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("There was an error modifying this record!")
        Finally
            cnn.Close()
        End Try
    End Sub
    
End Class
