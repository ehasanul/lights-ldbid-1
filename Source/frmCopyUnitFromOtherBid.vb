Imports System.Data
Imports System.Data.SqlClient

Public Class frmCopyUnitFromOtherBid
    Inherits System.Windows.Forms.Form
    Dim cnn As New SqlConnection
    Dim intNewUnitID As Integer
    Dim intOldUnitID As Integer
    Dim blnStartUnit As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboBid As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtBUnitType As System.Windows.Forms.TextBox
    Friend WithEvents txtBUnitDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboBid = New System.Windows.Forms.ComboBox()
        Me.cboUnit = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBUnitType = New System.Windows.Forms.TextBox()
        Me.txtBUnitDescription = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cboBid
        '
        Me.cboBid.Location = New System.Drawing.Point(12, 56)
        Me.cboBid.Name = "cboBid"
        Me.cboBid.Size = New System.Drawing.Size(196, 21)
        Me.cboBid.TabIndex = 0
        '
        'cboUnit
        '
        Me.cboUnit.Location = New System.Drawing.Point(220, 56)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(240, 21)
        Me.cboUnit.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(252, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Select Bid and Unit to Copy From"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(12, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Bid"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(220, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Unit"
        '
        'txtBUnitType
        '
        Me.txtBUnitType.Location = New System.Drawing.Point(132, 144)
        Me.txtBUnitType.MaxLength = 10
        Me.txtBUnitType.Name = "txtBUnitType"
        Me.txtBUnitType.Size = New System.Drawing.Size(100, 20)
        Me.txtBUnitType.TabIndex = 5
        '
        'txtBUnitDescription
        '
        Me.txtBUnitDescription.Location = New System.Drawing.Point(132, 172)
        Me.txtBUnitDescription.MaxLength = 50
        Me.txtBUnitDescription.Name = "txtBUnitDescription"
        Me.txtBUnitDescription.Size = New System.Drawing.Size(236, 20)
        Me.txtBUnitDescription.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Type"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(16, 172)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 20)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Description"
        '
        'btnCopy
        '
        Me.btnCopy.Location = New System.Drawing.Point(332, 200)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(132, 24)
        Me.btnCopy.TabIndex = 9
        Me.btnCopy.Text = "Copy Unit"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(16, 92)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(428, 36)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Enter New Unit Type and Description - The Type Must Be Unique To Your Current Bid" & _
            ""
        '
        'frmCopyUnitFromOtherBid
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(472, 234)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnCopy)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBUnitDescription)
        Me.Controls.Add(Me.txtBUnitType)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboUnit)
        Me.Controls.Add(Me.cboBid)
        Me.Name = "frmCopyUnitFromOtherBid"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Copy A Unit From Another Bid"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub LoadBids()
        'frmBid
        cnn.ConnectionString = frmMain.strConnect
        Dim cmdProjectList As New SqlCommand("ProjectList", cnn)
        cmdProjectList.CommandType = CommandType.StoredProcedure
        cmdProjectList.Parameters.AddWithValue("@ProjectType", "Bid")
        Dim daBids As New SqlDataAdapter(cmdProjectList)
        Dim dsBids As New DataSet
        daBids.Fill(dsBids, "Project")
        cboBid.DataSource = dsBids.Tables("Project")
        cboBid.DisplayMember = "Proj"
        cboBid.ValueMember = "ProjectID"
    End Sub

    Private Sub LoadUnits()
        'Units
        If blnStartUnit = False Then
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim cmdLoadUnits As New SqlCommand("Units", cnn)
        cmdLoadUnits.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdLoadUnits.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = cboBid.SelectedValue

        Dim daUnits As New SqlDataAdapter(cmdLoadUnits)
        Dim dsUnits As New DataSet
        Try
            daUnits.Fill(dsUnits, "Units")
        Catch ex As Exception
            MsgBox("Error Loading Units")
        End Try

        cboUnit.DataSource = dsUnits.Tables("Units")
        cboUnit.DisplayMember = "U"
        cboUnit.ValueMember = "UnitType"
    End Sub

    Private Sub frmCopyUnitFromOtherBid_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        blnStartUnit = True
        LoadBids()
    End Sub

    Private Sub AddNewUnit()
        Dim cmdAddNewUnit As New SqlCommand("AddUnit", cnn)
        cmdAddNewUnit.CommandType = CommandType.StoredProcedure

        Dim prmBidID As SqlParameter = cmdAddNewUnit.Parameters.Add("@ProjectID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID

        Dim prmUnitType As SqlParameter = cmdAddNewUnit.Parameters.Add("@UnitType", SqlDbType.Char, 10)
        prmUnitType.Value = txtBUnitType.Text

        Dim prmDescription As SqlParameter = cmdAddNewUnit.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
        prmDescription.Value = txtBUnitDescription.Text

        Dim prmNewUnitID As SqlParameter = cmdAddNewUnit.Parameters.Add("@NewUnitID", SqlDbType.Int)
        prmNewUnitID.Direction = ParameterDirection.Output


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            cmdAddNewUnit.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
        ' Gets New Unit ID From @@Identity
        intNewUnitID = System.Convert.ToInt32(prmNewUnitID.Value)
        frmBid.blnUnitReturn = True
        frmBid.blnUnitCopy = True
        frmBid.strActiveUnit = txtBUnitType.Text
    End Sub

    Private Sub CopyUnit()
        frmBid.blnUnitCopy = True
        frmBid.strActiveUnit = txtBUnitType.Text
        Dim strNewUnitDesc As String

        strNewUnitDesc = txtBUnitDescription.Text

        cnn.ConnectionString = frmMain.strConnect
        Dim strSqlCommand As String
        strSqlCommand = "SELECT distinct RecordSource, PartNumber, RT, Description, " & _
         "Cost, Margin, Sell, Quantity, Location, Type FROM LineItem " & _
            " WHERE UnitID in (Select UnitID from Unit where UnitType = '" & cboUnit.SelectedValue.ToString & "') " + _
            "and LineItem.ProjectID = " + cboBid.SelectedValue.ToString
        Dim cmdItems As New SqlCommand(strSqlCommand, cnn)
        Dim drItems As SqlDataReader
        cnn.Open()
        drItems = cmdItems.ExecuteReader

        While drItems.Read
            Dim cnn1 As New SqlConnection
            cnn1.ConnectionString = frmMain.strConnect
            Dim cmdAddUnitLineItem As New SqlCommand("AddLineItem", cnn1)
            cmdAddUnitLineItem.CommandType = CommandType.StoredProcedure

            Dim prmBidID As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@ProjectID", SqlDbType.Int)
            prmBidID.Value = frmMain.intBidID

            Dim prmUnitID As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@UnitID", SqlDbType.Int)
            prmUnitID.Value = intNewUnitID

            Dim prmQuantity As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Quantity", SqlDbType.Int)
            prmQuantity.Value = drItems("Quantity")

            Dim prmLocation As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Location", SqlDbType.NVarChar, 50)
            prmLocation.Value = drItems("Location")

            Dim prmRecordSource As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@RecordSource", SqlDbType.Char, 10)
            prmRecordSource.Value = drItems("RecordSource")

            Dim prmPartNumber As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@PartNumber", SqlDbType.Char, 25)
            prmPartNumber.Value = drItems("PartNumber")

            Dim prmRT As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@RT", SqlDbType.Char, 1)
            prmRT.Value = drItems("RT")

            Dim prmType As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Type", SqlDbType.NVarChar, 10)
            prmType.Value = drItems("Type")

            Dim prmdescription As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Description", SqlDbType.NVarChar, 50)
            prmdescription.Value = drItems("Description")

            Dim prmCost As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Cost", SqlDbType.Money)
            prmCost.Value = drItems("Cost")

            Dim prmMargin As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Margin", SqlDbType.Int)
            prmMargin.Value = drItems("Margin")

            Dim prmSell As SqlParameter = cmdAddUnitLineItem.Parameters.Add("@Sell", SqlDbType.Money)
            prmSell.Value = drItems("Sell")

            Try
                If cnn1.State = ConnectionState.Closed Then
                    cnn1.Open()
                End If
                cmdAddUnitLineItem.ExecuteNonQuery()
                cnn1.Close()
            Catch ex As Exception
                MsgBox("There was an error adding this record: " + ex.ToString)
            Finally
                If cnn1.State = ConnectionState.Open Then
                    cnn1.Close()
                End If
            End Try
        End While
        drItems.Close()
        cnn.Close()
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        If txtBUnitType.Text = "" Then
            MsgBox("You Must Enter A Unit Type!")
            Exit Sub
        End If

        If txtBUnitDescription.Text = "" Then
            MsgBox("You Must Enter A Unit Description!")
            Exit Sub
        End If
        AddNewUnit()
        CopyUnit()
        
    End Sub

    Private Sub SelectedBidCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBid.SelectionChangeCommitted
        If cboBid.SelectedIndex = 0 Then
            Exit Sub
        End If
        LoadUnits()
    End Sub
End Class
