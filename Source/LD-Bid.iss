[Setup]
AppName=LD-Bid
AppVersion=1.9
DefaultDirName={pf}\Lights Direct
DefaultGroupName=LD-Bid
UninstallDisplayIcon={app}\LD-Bid.exe
Compression=lzma2
SolidCompression=yes
OutputDir=.\Builds

[Files]
Source: ".\bin\x86\Release\LD-Bid.exe"; DestDir: "{app}"
Source: ".\bin\x86\Release\LD-Bid.exe.config"; DestDir: "{app}"; Flags: onlyifdoesntexist uninsneveruninstall

[Icons]
Name: "{group}\LD-Bid"; FileName: "{app}\LD-Bid.exe"