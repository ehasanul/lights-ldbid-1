Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmCompareBidCosts
    Inherits System.Windows.Forms.Form
    Dim strBids As String
    Dim strSql As String
    Dim cnn As New SqlConnection


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtBidList As System.Windows.Forms.TextBox
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents lblList As System.Windows.Forms.Label
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtBidList = New System.Windows.Forms.TextBox
        Me.btnLoad = New System.Windows.Forms.Button
        Me.lblList = New System.Windows.Forms.Label
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'txtBidList
        '
        Me.txtBidList.Location = New System.Drawing.Point(28, 52)
        Me.txtBidList.Name = "txtBidList"
        Me.txtBidList.Size = New System.Drawing.Size(488, 20)
        Me.txtBidList.TabIndex = 0
        Me.txtBidList.Text = ""
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(420, 300)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(60, 20)
        Me.btnLoad.TabIndex = 1
        Me.btnLoad.Text = "Load"
        '
        'lblList
        '
        Me.lblList.Location = New System.Drawing.Point(28, 32)
        Me.lblList.Name = "lblList"
        Me.lblList.Size = New System.Drawing.Size(488, 20)
        Me.lblList.TabIndex = 2
        Me.lblList.Text = "Enter a list of Bids Separated By Commas (i.e. 143, 151, 162)"
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(536, 346)
        Me.CrystalReportViewer1.TabIndex = 3
        Me.CrystalReportViewer1.Visible = False
        '
        'frmCompareBidCosts
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(536, 346)
        Me.Controls.Add(Me.lblList)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.txtBidList)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmCompareBidCosts"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compare Bid Costs"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub BuildSql()
        If txtBidList.Text = "" Then
            MsgBox("You must enter a list of Bids to Compare with!")
            Exit Sub
        Else
            strBids = frmMain.intBidID.ToString & ", " & txtBidList.Text
        End If

        strSql = "SELECT PartNumber, Cost, Sell, ProjectID as BidID FROM LineItem WHERE " & _
            "PartNumber IN (SELECT Partnumber FROM LineItem WHERE ProjectID = " & frmMain.intBidID.ToString & ")" & _
            " AND ProjectID IN (" & strBids & ")" & " GROUP BY PartNumber, Cost, Sell, ProjectID"

    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        BuildSql()
        HideAll()
        LoadReport()

    End Sub

    Private Sub HideAll()
        txtBidList.Visible = False
        btnLoad.Visible = False
        lblList.Visible = False
        CrystalReportViewer1.Visible = True
    End Sub

    Private Sub showall()
        txtBidList.Visible = True
        btnLoad.Visible = True
        lblList.Visible = True
        CrystalReportViewer1.Visible = False
    End Sub

    Private Sub LoadReport()
        cnn.ConnectionString = frmMain.strConnect

        Dim strCommand As String
        strCommand = strSql
        Dim cmdCommand As New SqlCommand
        cmdCommand.Connection = cnn
        cmdCommand.CommandText = strCommand
        cmdCommand.CommandType = CommandType.Text
        Dim daIntro As New SqlDataAdapter(cmdCommand)
        Dim dsIntro As New DataSet
        Try
            daIntro.Fill(dsIntro, "b_CompPrices")
        Catch ex As Exception
            MsgBox("There was an error getting your data. Please check your Bid List!!", MsgBoxStyle.Critical, "Check Your Bid List!")
            showall()
            Exit Sub
        End Try

        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        myReport = New rpt_CompPrices
        myReport.SetDataSource(dsIntro.Tables("b_CompPrices"))
        CrystalReportViewer1.ReportSource = myReport
    End Sub
End Class
