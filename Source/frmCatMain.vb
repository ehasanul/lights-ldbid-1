Imports System.Data
Imports System.Data.SqlClient
Public Class frmCatMain
    Inherits System.Windows.Forms.Form

    Dim cnn As New SqlConnection
    Dim strFunction As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboCatalogMain As System.Windows.Forms.ComboBox
    Friend WithEvents txtProductID As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtVendorCode As System.Windows.Forms.TextBox
    Friend WithEvents txtVendorPartNo As System.Windows.Forms.TextBox
    Friend WithEvents txtStandardCost As System.Windows.Forms.TextBox
    Friend WithEvents txtAvailableUnits As System.Windows.Forms.TextBox
    Friend WithEvents txtOnOrderUnits As System.Windows.Forms.TextBox
    Friend WithEvents txtCommittedUnits As System.Windows.Forms.TextBox
    Friend WithEvents txtLeadTime As System.Windows.Forms.TextBox
    Friend WithEvents txtAlternateVendor As System.Windows.Forms.TextBox
    Friend WithEvents txtAlternatePartNum As System.Windows.Forms.TextBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblProductID As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblVendorCode As System.Windows.Forms.Label
    Friend WithEvents lblStandardCost As System.Windows.Forms.Label
    Friend WithEvents lblAvailableUnits As System.Windows.Forms.Label
    Friend WithEvents lblOnOrderUnits As System.Windows.Forms.Label
    Friend WithEvents lblCommittedUnits As System.Windows.Forms.Label
    Friend WithEvents lblLeadTime As System.Windows.Forms.Label
    Friend WithEvents lblAlternateVendor As System.Windows.Forms.Label
    Friend WithEvents lblAlternatePartNum As System.Windows.Forms.Label
    Friend WithEvents lblVendorPartNo As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboCatalogMain = New System.Windows.Forms.ComboBox
        Me.txtProductID = New System.Windows.Forms.TextBox
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtVendorCode = New System.Windows.Forms.TextBox
        Me.txtVendorPartNo = New System.Windows.Forms.TextBox
        Me.txtStandardCost = New System.Windows.Forms.TextBox
        Me.txtAvailableUnits = New System.Windows.Forms.TextBox
        Me.txtOnOrderUnits = New System.Windows.Forms.TextBox
        Me.txtCommittedUnits = New System.Windows.Forms.TextBox
        Me.txtLeadTime = New System.Windows.Forms.TextBox
        Me.lblProductID = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblVendorCode = New System.Windows.Forms.Label
        Me.lblVendorPartNo = New System.Windows.Forms.Label
        Me.lblStandardCost = New System.Windows.Forms.Label
        Me.lblAvailableUnits = New System.Windows.Forms.Label
        Me.lblOnOrderUnits = New System.Windows.Forms.Label
        Me.lblCommittedUnits = New System.Windows.Forms.Label
        Me.lblLeadTime = New System.Windows.Forms.Label
        Me.txtAlternateVendor = New System.Windows.Forms.TextBox
        Me.txtAlternatePartNum = New System.Windows.Forms.TextBox
        Me.lblAlternateVendor = New System.Windows.Forms.Label
        Me.lblAlternatePartNum = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboCatalogMain
        '
        Me.cboCatalogMain.Location = New System.Drawing.Point(20, 12)
        Me.cboCatalogMain.Name = "cboCatalogMain"
        Me.cboCatalogMain.Size = New System.Drawing.Size(280, 21)
        Me.cboCatalogMain.TabIndex = 0
        Me.cboCatalogMain.Text = "Catalog"
        '
        'txtProductID
        '
        Me.txtProductID.Location = New System.Drawing.Point(20, 60)
        Me.txtProductID.MaxLength = 13
        Me.txtProductID.Name = "txtProductID"
        Me.txtProductID.Size = New System.Drawing.Size(228, 20)
        Me.txtProductID.TabIndex = 1
        Me.txtProductID.Text = ""
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(264, 60)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(224, 20)
        Me.txtDescription.TabIndex = 2
        Me.txtDescription.Text = ""
        '
        'txtVendorCode
        '
        Me.txtVendorCode.Location = New System.Drawing.Point(20, 92)
        Me.txtVendorCode.Name = "txtVendorCode"
        Me.txtVendorCode.Size = New System.Drawing.Size(228, 20)
        Me.txtVendorCode.TabIndex = 3
        Me.txtVendorCode.Text = ""
        '
        'txtVendorPartNo
        '
        Me.txtVendorPartNo.Location = New System.Drawing.Point(264, 92)
        Me.txtVendorPartNo.Name = "txtVendorPartNo"
        Me.txtVendorPartNo.Size = New System.Drawing.Size(224, 20)
        Me.txtVendorPartNo.TabIndex = 4
        Me.txtVendorPartNo.Text = ""
        '
        'txtStandardCost
        '
        Me.txtStandardCost.Location = New System.Drawing.Point(20, 164)
        Me.txtStandardCost.Name = "txtStandardCost"
        Me.txtStandardCost.Size = New System.Drawing.Size(96, 20)
        Me.txtStandardCost.TabIndex = 8
        Me.txtStandardCost.Text = ""
        Me.txtStandardCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAvailableUnits
        '
        Me.txtAvailableUnits.Location = New System.Drawing.Point(116, 164)
        Me.txtAvailableUnits.Name = "txtAvailableUnits"
        Me.txtAvailableUnits.Size = New System.Drawing.Size(68, 20)
        Me.txtAvailableUnits.TabIndex = 9
        Me.txtAvailableUnits.Text = ""
        Me.txtAvailableUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtOnOrderUnits
        '
        Me.txtOnOrderUnits.Location = New System.Drawing.Point(184, 164)
        Me.txtOnOrderUnits.Name = "txtOnOrderUnits"
        Me.txtOnOrderUnits.Size = New System.Drawing.Size(68, 20)
        Me.txtOnOrderUnits.TabIndex = 10
        Me.txtOnOrderUnits.Text = ""
        Me.txtOnOrderUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCommittedUnits
        '
        Me.txtCommittedUnits.Location = New System.Drawing.Point(252, 164)
        Me.txtCommittedUnits.Name = "txtCommittedUnits"
        Me.txtCommittedUnits.Size = New System.Drawing.Size(68, 20)
        Me.txtCommittedUnits.TabIndex = 11
        Me.txtCommittedUnits.Text = ""
        Me.txtCommittedUnits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtLeadTime
        '
        Me.txtLeadTime.Location = New System.Drawing.Point(320, 164)
        Me.txtLeadTime.Name = "txtLeadTime"
        Me.txtLeadTime.Size = New System.Drawing.Size(68, 20)
        Me.txtLeadTime.TabIndex = 12
        Me.txtLeadTime.Text = ""
        Me.txtLeadTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblProductID
        '
        Me.lblProductID.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProductID.Location = New System.Drawing.Point(20, 44)
        Me.lblProductID.Name = "lblProductID"
        Me.lblProductID.Size = New System.Drawing.Size(116, 12)
        Me.lblProductID.TabIndex = 15
        Me.lblProductID.Text = "Part Number"
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(264, 44)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(124, 12)
        Me.lblDescription.TabIndex = 16
        Me.lblDescription.Text = "Description"
        '
        'lblVendorCode
        '
        Me.lblVendorCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendorCode.Location = New System.Drawing.Point(20, 80)
        Me.lblVendorCode.Name = "lblVendorCode"
        Me.lblVendorCode.Size = New System.Drawing.Size(80, 12)
        Me.lblVendorCode.TabIndex = 17
        Me.lblVendorCode.Text = "Vendor Code"
        '
        'lblVendorPartNo
        '
        Me.lblVendorPartNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendorPartNo.Location = New System.Drawing.Point(264, 80)
        Me.lblVendorPartNo.Name = "lblVendorPartNo"
        Me.lblVendorPartNo.Size = New System.Drawing.Size(68, 12)
        Me.lblVendorPartNo.TabIndex = 18
        Me.lblVendorPartNo.Text = "Vendor Part Number"
        '
        'lblStandardCost
        '
        Me.lblStandardCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStandardCost.Location = New System.Drawing.Point(28, 148)
        Me.lblStandardCost.Name = "lblStandardCost"
        Me.lblStandardCost.Size = New System.Drawing.Size(80, 12)
        Me.lblStandardCost.TabIndex = 20
        Me.lblStandardCost.Text = "Standard Cost"
        '
        'lblAvailableUnits
        '
        Me.lblAvailableUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAvailableUnits.Location = New System.Drawing.Point(116, 148)
        Me.lblAvailableUnits.Name = "lblAvailableUnits"
        Me.lblAvailableUnits.Size = New System.Drawing.Size(68, 12)
        Me.lblAvailableUnits.TabIndex = 21
        Me.lblAvailableUnits.Text = "Available Units"
        '
        'lblOnOrderUnits
        '
        Me.lblOnOrderUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOnOrderUnits.Location = New System.Drawing.Point(188, 148)
        Me.lblOnOrderUnits.Name = "lblOnOrderUnits"
        Me.lblOnOrderUnits.Size = New System.Drawing.Size(68, 12)
        Me.lblOnOrderUnits.TabIndex = 22
        Me.lblOnOrderUnits.Text = "Units On Order"
        '
        'lblCommittedUnits
        '
        Me.lblCommittedUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommittedUnits.Location = New System.Drawing.Point(252, 148)
        Me.lblCommittedUnits.Name = "lblCommittedUnits"
        Me.lblCommittedUnits.Size = New System.Drawing.Size(76, 12)
        Me.lblCommittedUnits.TabIndex = 23
        Me.lblCommittedUnits.Text = "Committed Units"
        '
        'lblLeadTime
        '
        Me.lblLeadTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeadTime.Location = New System.Drawing.Point(332, 148)
        Me.lblLeadTime.Name = "lblLeadTime"
        Me.lblLeadTime.Size = New System.Drawing.Size(60, 12)
        Me.lblLeadTime.TabIndex = 24
        Me.lblLeadTime.Text = "Lead Time"
        '
        'txtAlternateVendor
        '
        Me.txtAlternateVendor.Location = New System.Drawing.Point(20, 128)
        Me.txtAlternateVendor.Name = "txtAlternateVendor"
        Me.txtAlternateVendor.Size = New System.Drawing.Size(228, 20)
        Me.txtAlternateVendor.TabIndex = 5
        Me.txtAlternateVendor.Text = ""
        '
        'txtAlternatePartNum
        '
        Me.txtAlternatePartNum.Location = New System.Drawing.Point(264, 128)
        Me.txtAlternatePartNum.Name = "txtAlternatePartNum"
        Me.txtAlternatePartNum.Size = New System.Drawing.Size(224, 20)
        Me.txtAlternatePartNum.TabIndex = 6
        Me.txtAlternatePartNum.Text = ""
        '
        'lblAlternateVendor
        '
        Me.lblAlternateVendor.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternateVendor.Location = New System.Drawing.Point(20, 116)
        Me.lblAlternateVendor.Name = "lblAlternateVendor"
        Me.lblAlternateVendor.Size = New System.Drawing.Size(104, 12)
        Me.lblAlternateVendor.TabIndex = 27
        Me.lblAlternateVendor.Text = "Alternate Vendor"
        '
        'lblAlternatePartNum
        '
        Me.lblAlternatePartNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternatePartNum.Location = New System.Drawing.Point(264, 112)
        Me.lblAlternatePartNum.Name = "lblAlternatePartNum"
        Me.lblAlternatePartNum.Size = New System.Drawing.Size(176, 12)
        Me.lblAlternatePartNum.TabIndex = 28
        Me.lblAlternatePartNum.Text = "Alternate Part Number"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "New"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Edit"
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(328, 192)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(52, 16)
        Me.btnDelete.TabIndex = 15
        Me.btnDelete.Text = "Delete"
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(380, 192)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(56, 16)
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "Cancel"
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(436, 192)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(52, 16)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "Save"
        '
        'frmCatMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(508, 229)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.lblAlternatePartNum)
        Me.Controls.Add(Me.lblAlternateVendor)
        Me.Controls.Add(Me.txtAlternatePartNum)
        Me.Controls.Add(Me.txtAlternateVendor)
        Me.Controls.Add(Me.txtLeadTime)
        Me.Controls.Add(Me.txtCommittedUnits)
        Me.Controls.Add(Me.txtOnOrderUnits)
        Me.Controls.Add(Me.txtAvailableUnits)
        Me.Controls.Add(Me.txtStandardCost)
        Me.Controls.Add(Me.txtVendorPartNo)
        Me.Controls.Add(Me.txtVendorCode)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.txtProductID)
        Me.Controls.Add(Me.lblLeadTime)
        Me.Controls.Add(Me.lblCommittedUnits)
        Me.Controls.Add(Me.lblOnOrderUnits)
        Me.Controls.Add(Me.lblAvailableUnits)
        Me.Controls.Add(Me.lblStandardCost)
        Me.Controls.Add(Me.lblVendorPartNo)
        Me.Controls.Add(Me.lblVendorCode)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblProductID)
        Me.Controls.Add(Me.cboCatalogMain)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmCatMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Main Catalog"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Sub LoadCatalogMaincbo()
        'Units
        cnn.ConnectionString = frmMain.strConnect

        Dim strCatalogMain As String
        'If rbMain.Checked = True Then
        strCatalogMain = "CatMainItems"
        'ElseIf rbSO.Checked = True Then
        '    strCatalogMain = "b_CatSOItem"
        'ElseIf rbBid.Checked = True Then
        '    LoadBidItemcbo()
        '    Exit Sub
        'End If

        Dim daCatalogMain As New SqlDataAdapter(strCatalogMain, cnn)
        Dim dsCatalogMain As New DataSet
        Try
            daCatalogMain.Fill(dsCatalogMain, "Catalog")
        Catch ex As Exception
            MsgBox("Error Loading CatalogMain CBO")
        End Try
        cboCatalogMain.DataSource = dsCatalogMain.Tables("Catalog")
        cboCatalogMain.DisplayMember = "Item"
        cboCatalogMain.ValueMember = "ProductID"
    End Sub

    Private Sub frmCatMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadCatalogMaincbo()
        showfields()
        HideBtns()
    End Sub

    Private Sub HideBtns()
        btnDelete.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False

    End Sub

    Private Sub HideFields()
        txtProductID.Visible = False
        txtDescription.Visible = False
        txtVendorCode.Visible = False
        txtVendorPartNo.Visible = False
        txtAlternateVendor.Visible = False
        txtAlternatePartNum.Visible = False
        'txtAverageCost.Visible = False
        txtStandardCost.Visible = False
        txtAvailableUnits.Visible = False
        txtOnOrderUnits.Visible = False
        txtCommittedUnits.Visible = False
        txtLeadTime.Visible = False

        lblProductID.Visible = False
        lblDescription.Visible = False
        lblVendorCode.Visible = False
        lblVendorPartNo.Visible = False
        lblAlternateVendor.Visible = False
        lblAlternatePartNum.Visible = False
        'lblAverageCost.Visible = False
        lblStandardCost.Visible = False
        lblAvailableUnits.Visible = False
        lblOnOrderUnits.Visible = False
        lblCommittedUnits.Visible = False
        lblLeadTime.Visible = False


    End Sub

    Private Sub showfields()
        txtProductID.Visible = True
        txtDescription.Visible = True
        txtVendorCode.Visible = True
        txtVendorPartNo.Visible = True
        txtAlternateVendor.Visible = True
        txtAlternatePartNum.Visible = True
        'txtAverageCost.Visible = True
        txtStandardCost.Visible = True
        txtAvailableUnits.Visible = True
        txtOnOrderUnits.Visible = True
        txtCommittedUnits.Visible = True
        txtLeadTime.Visible = True

        lblProductID.Visible = True
        lblDescription.Visible = True
        lblVendorCode.Visible = True
        lblVendorPartNo.Visible = True
        lblAlternateVendor.Visible = True
        lblAlternatePartNum.Visible = True
        'lblAverageCost.Visible = True
        lblStandardCost.Visible = True
        lblAvailableUnits.Visible = True
        lblOnOrderUnits.Visible = True
        lblCommittedUnits.Visible = True
        lblLeadTime.Visible = True
    End Sub

    Private Sub ClearFields()
        txtProductID.Text = ""
        txtDescription.Text = ""
        txtVendorCode.Text = ""
        txtVendorPartNo.Text = ""
        txtAlternateVendor.Text = ""
        txtAlternatePartNum.Text = ""
        'txtAverageCost.Text = ""
        txtStandardCost.Text = ""
        txtAvailableUnits.Text = ""
        txtOnOrderUnits.Text = ""
        txtCommittedUnits.Text = ""
        txtLeadTime.Text = ""
    End Sub

    Private Sub LoadItemDetails()
        'cboCatalogMain.SelectedValue.ToString
        If cboCatalogMain.SelectedIndex = 0 Then
            ClearFields()
            Exit Sub
        End If
        Dim strProductID As String
        strProductID = cboCatalogMain.SelectedValue.ToString

        Dim spCatMainLineItem As String
        spCatMainLineItem = "CatMainItem"

        Dim cmdCatMainLineItem As New SqlCommand(spCatMainLineItem, cnn)
        cmdCatMainLineItem.CommandType = CommandType.StoredProcedure
        cmdCatMainLineItem.Parameters.AddWithValue("@ProductID", cboCatalogMain.SelectedValue.ToString)
        Dim drCatMainLineItem As SqlDataReader = Nothing
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If
        Try
            drCatMainLineItem = cmdCatMainLineItem.ExecuteReader
            drCatMainLineItem.Read()
            txtProductID.Text = cboCatalogMain.SelectedValue.ToString
            txtDescription.Text = drCatMainLineItem.Item("Description").ToString
            If IsDBNull(drCatMainLineItem.Item("VendorCode")) Then
                txtVendorCode.Text = ""
            Else
                txtVendorCode.Text = drCatMainLineItem.Item("VendorCode").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("VendorPartNo")) Then
                txtVendorPartNo.Text = ""
            Else
                txtVendorPartNo.Text = drCatMainLineItem.Item("VendorPartNo").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("AlternateVendor")) Then
                txtAlternateVendor.Text = ""
            Else
                txtAlternateVendor.Text = drCatMainLineItem.Item("AlternateVendor").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("AlternatePartNum")) Then
                txtAlternatePartNum.Text = ""
            Else
                txtAlternatePartNum.Text = drCatMainLineItem.Item("AlternatePartNum").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("StandardCost")) Then
                txtStandardCost.Text = "0"
            Else
                txtStandardCost.Text = Format(drCatMainLineItem.Item("StandardCost"), "C")
            End If
            If IsDBNull(drCatMainLineItem.Item("AvailableUnits")) Then
                txtAvailableUnits.Text = "0"
            Else
                txtAvailableUnits.Text = drCatMainLineItem.Item("AvailableUnits").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("OnOrderUnits")) Then
                txtOnOrderUnits.Text = "0"
            Else
                txtOnOrderUnits.Text = drCatMainLineItem.Item("OnOrderUnits").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("CommittedUnits")) Then
                txtCommittedUnits.Text = "0"
            Else
                txtCommittedUnits.Text = drCatMainLineItem.Item("CommittedUnits").ToString
            End If
            If IsDBNull(drCatMainLineItem.Item("LeadTime")) Then
                txtLeadTime.Text = ""
            Else
                txtLeadTime.Text = drCatMainLineItem.Item("LeadTime").ToString
            End If

        Catch ex As Exception
            MsgBox("Error Loading CatMainLineItem: " + ex.ToString)
        Finally
            drCatMainLineItem.Close()
            cnn.Close()
        End Try

    End Sub

    Private Sub cboCatalogMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCatalogMain.SelectedIndexChanged
        LoadItemDetails()
        showfields()
    End Sub

    Private Sub EditCatalog()
        If txtDescription.Text = "" Then
            MsgBox("A Description Is Required.")
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim EditCatalog As New SqlCommand("EditCatalogMain", cnn)
        EditCatalog.CommandType = CommandType.StoredProcedure

        Dim prmProductID As SqlParameter = EditCatalog.Parameters.Add("@ProductID", SqlDbType.NVarChar, 255)
        prmProductID.Value = cboCatalogMain.SelectedValue

        Dim prmDescription As SqlParameter = EditCatalog.Parameters.Add("@Description", SqlDbType.NVarChar, 255)
        prmDescription.Value = txtDescription.Text

        Dim prmVendorCode As SqlParameter = EditCatalog.Parameters.Add("@VendorCode", SqlDbType.NVarChar, 255)
        prmVendorCode.Value = txtVendorCode.Text

        Dim prmVendorPartNo As SqlParameter = EditCatalog.Parameters.Add("@VendorPartNo", SqlDbType.NVarChar, 255)
        prmVendorPartNo.Value = txtVendorPartNo.Text

        Dim prmAlternateVendor As SqlParameter = EditCatalog.Parameters.Add("@AlternateVendor", SqlDbType.NVarChar, 255)
        prmAlternateVendor.Value = txtAlternateVendor.Text

        Dim prmAlternatePartNum As SqlParameter = EditCatalog.Parameters.Add("@AlternatePartNum", SqlDbType.NVarChar, 255)
        prmAlternatePartNum.Value = txtAlternatePartNum.Text

        Dim prmStandardCost As SqlParameter = EditCatalog.Parameters.Add("@StandardCost", SqlDbType.Float)
        If IsNumeric(txtStandardCost.Text) Then
            prmStandardCost.Value = CDbl(txtStandardCost.Text)
        Else
            MsgBox("You must enter a numeric Cost.")
            Exit Sub
        End If

        Dim prmAvailableUnits As SqlParameter = EditCatalog.Parameters.Add("@AvailableUnits", SqlDbType.Float)
        If IsNumeric(txtAvailableUnits.Text) Then
            prmAvailableUnits.Value = CDbl(txtAvailableUnits.Text)
        Else
            prmAvailableUnits.Value = 0
        End If

        Dim prmOnOrderUnits As SqlParameter = EditCatalog.Parameters.Add("@OnOrderUnits", SqlDbType.Float)
        If IsNumeric(txtOnOrderUnits.Text) Then
            prmOnOrderUnits.Value = CDbl(txtOnOrderUnits.Text)
        Else
            prmOnOrderUnits.Value = 0
        End If

        Dim prmCommittedUnits As SqlParameter = EditCatalog.Parameters.Add("@CommittedUnits", SqlDbType.Float)
        If IsNumeric(txtCommittedUnits.Text) Then
            prmCommittedUnits.Value = CDbl(txtCommittedUnits.Text)
        Else
            prmCommittedUnits.Value = 0
        End If

        Dim prmLeadTime As SqlParameter = EditCatalog.Parameters.Add("@LeadTime", SqlDbType.NVarChar, 255)
        prmLeadTime.Value = txtLeadTime.Text


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            EditCatalog.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error editing this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub AddItem()
        Dim strTestProductID As String = "SELECT ProductID FROM CatalogMain WHERE ProductID = '"
        strTestProductID = strTestProductID & txtProductID.Text & "'"
        Dim cmdTestProductID As New SqlCommand(strTestProductID, cnn)
        cmdTestProductID.CommandType = CommandType.Text

        Dim drTestProductID As SqlDataReader
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
        End If

        drTestProductID = cmdTestProductID.ExecuteReader
        drTestProductID.Read()
        If drTestProductID.HasRows Then
            MsgBox("You Must Enter A Unique Part Number!!!")
            drTestProductID.Close()
            cnn.Close()
            Exit Sub
        End If

        drTestProductID.Close()
        cnn.Close()


        If txtProductID.Text = "" Then
            MsgBox("A Unique Part Number Is Required.")
            Exit Sub
        End If
        If txtDescription.Text = "" Then
            MsgBox("A Description Is Required.")
            Exit Sub
        End If

        cnn.ConnectionString = frmMain.strConnect
        Dim AddItem As New SqlCommand("NewCatalogMain", cnn)
        AddItem.CommandType = CommandType.StoredProcedure

        Dim prmProductID As SqlParameter = AddItem.Parameters.Add("@ProductID", SqlDbType.NVarChar, 255)
        prmProductID.Value = txtProductID.Text

        Dim prmDescription As SqlParameter = AddItem.Parameters.Add("@Description", SqlDbType.NVarChar, 255)
        prmDescription.Value = txtDescription.Text

        Dim prmVendorCode As SqlParameter = AddItem.Parameters.Add("@VendorCode", SqlDbType.NVarChar, 255)
        prmVendorCode.Value = txtVendorCode.Text

        Dim prmVendorPartNo As SqlParameter = AddItem.Parameters.Add("@VendorPartNo", SqlDbType.NVarChar, 255)
        prmVendorPartNo.Value = txtVendorPartNo.Text

        Dim prmAlternateVendor As SqlParameter = AddItem.Parameters.Add("@AlternateVendor", SqlDbType.NVarChar, 255)
        prmAlternateVendor.Value = txtAlternateVendor.Text

        Dim prmAlternatePartNum As SqlParameter = AddItem.Parameters.Add("@AlternatePartNum", SqlDbType.NVarChar, 255)
        prmAlternatePartNum.Value = txtAlternatePartNum.Text

        Dim prmStandardCost As SqlParameter = AddItem.Parameters.Add("@StandardCost", SqlDbType.Float)
        If IsNumeric(txtStandardCost.Text) Then
            prmStandardCost.Value = CDbl(txtStandardCost.Text)
        Else
            MsgBox("You must enter a numeric Cost.")
            Exit Sub
        End If


        Dim prmAvailableUnits As SqlParameter = AddItem.Parameters.Add("@AvailableUnits", SqlDbType.Float)
        If IsNumeric(txtAvailableUnits.Text) Then
            prmAvailableUnits.Value = CDbl(txtAvailableUnits.Text)
        Else
            prmAvailableUnits.Value = 0
        End If


        Dim prmOnOrderUnits As SqlParameter = AddItem.Parameters.Add("@OnOrderUnits", SqlDbType.Float)
        If IsNumeric(txtOnOrderUnits.Text) Then
            prmOnOrderUnits.Value = CDbl(txtOnOrderUnits.Text)
        Else
            prmOnOrderUnits.Value = 0
        End If

        Dim prmCommittedUnits As SqlParameter = AddItem.Parameters.Add("@CommittedUnits", SqlDbType.Float)
        If IsNumeric(txtCommittedUnits.Text) Then
            prmCommittedUnits.Value = CDbl(txtCommittedUnits.Text)
        Else
            prmCommittedUnits.Value = 0
        End If

        Dim prmLeadTime As SqlParameter = AddItem.Parameters.Add("@LeadTime", SqlDbType.NVarChar, 255)
        prmLeadTime.Value = txtLeadTime.Text


        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            AddItem.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error adding this record: " + ex.ToString)
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub

    Private Sub DeleteItem()
        cnn.ConnectionString = frmMain.strConnect
        Dim DeleteItem As New SqlCommand("DeleteCatalogMain", cnn)
        DeleteItem.CommandType = CommandType.StoredProcedure

        Dim prmProductID As SqlParameter = DeleteItem.Parameters.Add("@ProductID", SqlDbType.NVarChar, 255)
        prmProductID.Value = cboCatalogMain.SelectedValue

        Try
            If cnn.State = ConnectionState.Closed Then
                cnn.Open()
            End If

            DeleteItem.ExecuteNonQuery()
            cnn.Close()

        Catch ex As Exception
            MsgBox("There was an error Deleting this record!!")
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try
    End Sub


    Private Sub MenuItem1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        ClearFields()
        btnSave.Visible = True
        btnCancel.Visible = True
        showfields()
        strFunction = "Add"
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        btnSave.Visible = True
        btnCancel.Visible = True
        btnDelete.Visible = True
        showfields()
        strFunction = "Edit"
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtVendorCode.Text = "" Then
            MsgBox("You Must Enter A Valid Vendor Code")
            Exit Sub
        End If

        If txtVendorPartNo.Text = "" Then
            MsgBox("You Must Enter A Valid Vendor Part Number")
            Exit Sub
        End If
        If strFunction = "Edit" Then
            'MsgBox("Edit")
            EditCatalog()
        ElseIf strFunction = "Add" Then
            MsgBox("Add")
            AddItem()
        End If
        HideBtns()
        ClearFields()
        HideFields()
        LoadCatalogMaincbo()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim strPrompt As String
        strPrompt = "Do you want to Delete " & cboCatalogMain.SelectedValue.ToString & "?"
        If MsgBox(strPrompt, MsgBoxStyle.YesNo, "Delete??") = MsgBoxResult.Yes Then
            DeleteItem()
        End If
        ClearFields()
        HideFields()
        LoadCatalogMaincbo()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        HideFields()
        HideBtns()
    End Sub

    Private Sub txtAlternatePartNum_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlternatePartNum.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtAlternateVendor_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAlternateVendor.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtDescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtProductID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtProductID.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtVendorPartNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtVendorPartNo.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtVendorCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtVendorCode.KeyPress
        If e.KeyChar.ToString = "'" Then
            e.Handled = True
        End If
    End Sub
End Class
