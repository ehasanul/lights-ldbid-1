Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmRptMainSummary
    Inherits System.Windows.Forms.Form

    Dim cnn As New SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(584, 314)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'frmRptMainSummary
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(584, 314)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmRptMainSummary"
        Me.Text = "Summary Report"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadReport()
        'cnn.ConnectionString = "workstation id=""CO-TECH-LT-003"";user id=SA;data source=""CO-TECH-LT-003"";initial catalog=LD-BID;password=vw9110"
        cnn.ConnectionString = frmMain.strConnect
        Dim strCommand As String
        strCommand = "b_rptBidIntro"
        Dim cmdCommand As New SqlCommand
        cmdCommand.Connection = cnn
        cmdCommand.CommandText = strCommand
        cmdCommand.CommandType = CommandType.StoredProcedure
        Dim prmBidID As SqlParameter = cmdCommand.Parameters.Add("@BidID", SqlDbType.Int)
        prmBidID.Value = frmMain.intBidID
        Dim daBid As New SqlDataAdapter(cmdCommand)
        'daBid.SelectCommand = cmdCommand

        Dim dsBid As New DataSet
        daBid.Fill(dsBid, "Bids")
        'dgMain.DataSource = dsTeacherClasses
        'dgMain.DataMember = "TeacherClasses"
        Dim strSummary As String
        strSummary = "b_LineItemSummary"
        Dim cmdSummary As New SqlCommand
        cmdSummary.Connection = cnn
        cmdSummary.CommandText = strSummary
        cmdSummary.CommandType = CommandType.StoredProcedure
        Dim prmSumBidID As SqlParameter = cmdSummary.Parameters.Add("@BidID", SqlDbType.Int)
        prmSumBidID.Value = frmMain.intBidID
        Dim daSummary As New SqlDataAdapter(cmdSummary)
        'daBid.SelectCommand = cmdCommand
        Dim dsSummary As New DataSet
        Try
            daSummary.Fill(dsSummary, "b_LineItemSummary")
        Catch ex As Exception
            MsgBox("Failed to fill Summary Dataset")
        End Try
        

        'Dim strUnitsPerBldg As String
        'strUnitsPerBldg = "b_UnitsPerBuilding"
        'Dim cmdUnitsPerBldg As New SqlCommand
        'cmdUnitsPerBldg.Connection = cnn
        'cmdUnitsPerBldg.CommandText = strUnitsPerBldg
        'cmdUnitsPerBldg.CommandType = CommandType.StoredProcedure
        'Dim prmUnitPerBldgBidID As SqlParameter = cmdUnitsPerBldg.Parameters.Add("@BidID", SqlDbType.Int)
        'prmUnitPerBldgBidID.Value = frmMain.intBidID
        'Dim daUnitsPerBldg As New SqlDataAdapter(cmdUnitsPerBldg)


        'Dim dsUnitsPerBldg As New DataSet
        'daUnitsPerBldg.Fill(dsUnitsPerBldg, "b_UnitsPerBuilding")

        'Dim strUnitDetail As String
        'strUnitDetail = "b_UnitRptDetail"
        'Dim cmdUnitDetail As New SqlCommand
        'cmdUnitDetail.Connection = cnn
        'cmdUnitDetail.CommandText = strUnitDetail
        'cmdUnitDetail.CommandType = CommandType.StoredProcedure
        'Dim prmUnitDetailBidID As SqlParameter = cmdUnitDetail.Parameters.Add("@BidID", SqlDbType.Int)
        'prmUnitDetailBidID.Value = frmMain.intBidID
        'Dim daUnitDetail As New SqlDataAdapter(cmdUnitDetail)


        'Dim dsUnitDetail As New DataSet
        'daUnitDetail.Fill(dsUnitDetail, "b_UnitRptDetail")

        Dim strAlternates As String
        strAlternates = "b_AlternateSummary"
        Dim cmdAlternates As New SqlCommand
        cmdAlternates.Connection = cnn
        cmdAlternates.CommandText = strAlternates
        cmdAlternates.CommandType = CommandType.StoredProcedure
        Dim prmAlternatesBidID As SqlParameter = cmdAlternates.Parameters.Add("@BidID", SqlDbType.Int)
        prmAlternatesBidID.Value = frmMain.intBidID
        Dim daAlternates As New SqlDataAdapter(cmdAlternates)
        Dim dsAlternates As New DataSet
        Try

            daAlternates.Fill(dsAlternates, "b_AlternateSummary")
        Catch ex As Exception
            MsgBox("Failed to Fill Alternates Dataset")
        End Try
        


        Dim myReport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Try
            myReport = New rptBidMainSummary
            myReport.SetDataSource(dsBid)
            'myReport.OpenSubreport("rptUnitsPerBldg.rpt").SetDataSource(dsUnitsPerBldg)
            myReport.OpenSubreport("rptBidSummary.rpt").SetDataSource(dsSummary)
            'myReport.OpenSubreport("rptUnitRptDetail.rpt").SetDataSource(dsUnitDetail)
            myReport.OpenSubreport("rptBidAlternates.rpt").SetDataSource(dsAlternates)
        Catch ex As Exception
            MsgBox("Failed to assign Data to Reports")
        End Try

        CrystalReportViewer1.ReportSource = myReport
    End Sub

    Private Sub frmRptMainSummary_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadReport()
    End Sub
End Class
